package com.agriiprince.models;

import com.agriiprince.model.MandiModel;
import com.agriiprince.mvvm.model.crop.Crop;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Manu Sajjan on 21-12-2018.
 *
 * @version 1.2.2
 */
public class FarmerInfoTabModel {
    public Crop crop;

    public List<HistoricPriceModel> historicPriceModels;

    public List<MandiModel> mandis;

    public int selectedGrade = 0;

    public int selectedVariety = 0;

    public long selectedTime;

    public String getDay() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        Date date = new Date(selectedTime);
        String string = simpleDateFormat.format(date);
        return string.split("-")[0];
    }

    public String getMonth() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        Date date = new Date(selectedTime);
        String string = simpleDateFormat.format(date);
        return string.split("-")[1];
    }
}
