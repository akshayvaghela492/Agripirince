package com.agriiprince.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Manu Sajjan on 15-12-2018.
 */
public class CropDiseaseInfoModel {

    @SerializedName("id")
    private String id;
    @SerializedName("crop_name")
    private String cropName;
    @SerializedName("crop_diseases")
    private String diseaseName;
    @SerializedName("crop_diseases_scientific_name")
    private String scientificName;
    @SerializedName("crop_diseases_category")
    private String category;
    @SerializedName("crop_causes")
    private String cause;
    @SerializedName("crop_diseases_management")
    private String management;
    @SerializedName("crop_symptom")
    private String symptom;
    @SerializedName("comments")
    private String comments;

    @SerializedName("crop_images")
    private String images;


    public String getId() {
        return id;
    }

    public String getCropName() {
        return cropName;
    }

    public String getDiseaseName() {
        return diseaseName;
    }

    public String getScientificName() {
        return scientificName;
    }

    public String getCategory() {
        return category;
    }

    public String getCause() {
        return cause;
    }

    public String getManagement() {
        return management;
    }

    public String getSymptom() {
        return symptom;
    }

    public String getComments() {
        return comments;
    }

    public String getImages() {
        return images;
    }
}
