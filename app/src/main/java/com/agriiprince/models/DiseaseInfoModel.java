package com.agriiprince.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.agriiprince.R;
import com.google.gson.annotations.SerializedName;

public class DiseaseInfoModel implements Parcelable {

    @SerializedName("id")
    private String id;

    @SerializedName("crop_name")
    private String cropName;
    @SerializedName("disease_name")
    private String diseaseName;
    @SerializedName("crop_diseases_scientific_name")
    private String scientificName;
    @SerializedName("crop_diseases_category")
    private String category;
    @SerializedName("cause")
    private String cause;
    @SerializedName("crop_diseases_management")
    private String management;
    @SerializedName("symptoms")
    private String symptom;
    @SerializedName("comments")
    private String comments;

    @SerializedName("crop_images")
    private String images;


    @SerializedName("crop_name")
    private String cropNameBi;
    @SerializedName("disease_name")
    private String diseaseNameBi;
    @SerializedName("crop_diseases_scientific_name")
    private String scientificNameBi;
    @SerializedName("crop_diseases_category")
    private String categoryBi;
    @SerializedName("cause")
    private String causeBi;
    @SerializedName("crop_diseases_management")
    private String managementBi;
    @SerializedName("symptoms")
    private String symptomBi;
    @SerializedName("comments")
    private String commentsBi;

    public DiseaseInfoModel() {

    }

    private DiseaseInfoModel(Parcel parcel) {
        this.id = parcel.readString();

        this.cropName = parcel.readString();
        this.diseaseName = parcel.readString();
        this.scientificName = parcel.readString();
        this.category = parcel.readString();
        this.cause = parcel.readString();
        this.management = parcel.readString();
        this.symptom = parcel.readString();
        this.comments = parcel.readString();

        this.images = parcel.readString();

        this.cropNameBi = parcel.readString();
        this.diseaseNameBi = parcel.readString();
        this.scientificNameBi = parcel.readString();
        this.categoryBi = parcel.readString();
        this.causeBi = parcel.readString();
        this.managementBi = parcel.readString();
        this.symptomBi = parcel.readString();
        this.commentsBi = parcel.readString();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(cropName);
        dest.writeString(diseaseName);
        dest.writeString(scientificName);
        dest.writeString(category);
        dest.writeString(cause);
        dest.writeString(management);
        dest.writeString(symptom);
        dest.writeString(comments);
        dest.writeString(images);

        dest.writeString(cropNameBi);
        dest.writeString(diseaseNameBi);
        dest.writeString(scientificNameBi);
        dest.writeString(categoryBi);
        dest.writeString(causeBi);
        dest.writeString(managementBi);
        dest.writeString(symptomBi);
        dest.writeString(commentsBi);
    }

    public static final Parcelable.Creator<DiseaseInfoModel> CREATOR = new Creator<DiseaseInfoModel>() {
        @Override
        public DiseaseInfoModel createFromParcel(Parcel source) {
            return new DiseaseInfoModel(source);
        }

        @Override
        public DiseaseInfoModel[] newArray(int size) {
            return new DiseaseInfoModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCropName() {
        return cropName;
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }

    public String getScientificName() {
        return scientificName;
    }

    public void setScientificName(String scientificName) {
        this.scientificName = scientificName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getManagement() {
        return management;
    }

    public void setManagement(String management) {
        this.management = management;
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getCropNameBi() {
        return cropNameBi;
    }

    public void setCropNameBi(String cropNameBi) {
        this.cropNameBi = cropNameBi;
    }

    public String getDiseaseNameBi() {
        return diseaseNameBi;
    }

    public void setDiseaseNameBi(String diseaseNameBi) {
        this.diseaseNameBi = diseaseNameBi;
    }

    public String getScientificNameBi() {
        return scientificNameBi;
    }

    public void setScientificNameBi(String scientificNameBi) {
        this.scientificNameBi = scientificNameBi;
    }

    public String getCategoryBi() {
        return categoryBi;
    }

    public void setCategoryBi(String categoryBi) {
        this.categoryBi = categoryBi;
    }

    public String getCauseBi() {
        return causeBi;
    }

    public void setCauseBi(String causeBi) {
        this.causeBi = causeBi;
    }

    public String getManagementBi() {
        return managementBi;
    }

    public void setManagementBi(String managementBi) {
        this.managementBi = managementBi;
    }

    public String getSymptomBi() {
        return symptomBi;
    }

    public void setSymptomBi(String symptomBi) {
        this.symptomBi = symptomBi;
    }

    public String getCommentsBi() {
        return commentsBi;
    }

    public void setCommentsBi(String commentsBi) {
        this.commentsBi = commentsBi;
    }

    public static int getCropNameTitle() {
        return R.string.crop_name;
    }

    public static int getDiseaseNameTitle() {
        return R.string.disease;
    }

    public static int getScientificNameTitle() {
        return R.string.scientific_name;
    }

    public static int getCategoryTitle() {
        return R.string.category;
    }

    public static int getCauseTitle() {
        return R.string.cause;
    }

    public static int getManagementTitle() {
        return R.string.management;
    }

    public static int getSymptomTitle() {
        return R.string.symptom;
    }

    public static int getCommentsTitle() {
        return R.string.comments;
    }
}
