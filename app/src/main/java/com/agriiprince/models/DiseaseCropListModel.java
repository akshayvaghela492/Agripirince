package com.agriiprince.models;

import com.google.gson.annotations.SerializedName;

public class DiseaseCropListModel {

    @SerializedName("crop_diseases_id")
    private String diseaseId;
    @SerializedName("crop_name")
    private String crop_name;
    @SerializedName("crop_name_bi")
    private String crop_name_bi;


    public void setDiseaseId(String diseaseId) {
        this.diseaseId = diseaseId;
    }

    public void setCrop_name(String crop_name) {
        this.crop_name = crop_name;
    }


    public void setCrop_name_bi(String crop_name_bi) {
        this.crop_name_bi = crop_name_bi;
    }


    public String getDiseaseId() {
        return diseaseId;
    }

    public String getCrop_name() {
        return crop_name;
    }


    public String getCrop_name_bi() {
        return crop_name_bi;
    }

}
