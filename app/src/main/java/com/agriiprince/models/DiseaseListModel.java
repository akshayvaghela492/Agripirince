package com.agriiprince.models;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.agriiprince.mvvm.applevel.constants.Config;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DiseaseListModel {

    @SerializedName("id")
    private String disease_id;
    @SerializedName("disease_name")
    private String disease_name;
    @SerializedName("crop_diseases_hindi")
    private String disease_name_bi;
    @SerializedName("disease_symptoms")
    private String disease_symptoms;
    @SerializedName("disease_symptoms_hindi")
    private String disease_symptoms_bi;
    @SerializedName("crop_images")
    private String crop_images;

    public void setDisease_id(String disease_id) {
        this.disease_id = disease_id;
    }

    public void setDisease_name(String disease_name) {
        this.disease_name = disease_name;
    }

    public void setDisease_name_bi(String disease_name_bi) {
        this.disease_name_bi = disease_name_bi;
    }

    public void setDisease_symptoms(String disease_symptoms) {
        this.disease_symptoms = disease_symptoms;
    }

    public void setDisease_symptoms_bi(String disease_symptoms_bi) {
        this.disease_symptoms_bi = disease_symptoms_bi;
    }

    public void setCrop_images(String crop_images) {
        this.crop_images = crop_images;
    }

    public String getDisease_id() {
        return disease_id;
    }

    public String getDisease_name() {
        return disease_name;
    }

    public String getDisease_name_bi() {
        return disease_name_bi;
    }

    public String getDisease_symptoms() {
        return disease_symptoms;
    }

    public String getDisease_symptoms_bi() {
        return disease_symptoms_bi;
    }

    public String getCrop_images() {
        return crop_images;
    }

    public static List<String> getImageUrls(@NonNull String value) {
        List<String> urls = new ArrayList<>();
        String[] images = value.split(",");
        for (String image : images) {
            String query = Uri.encode(image.trim());
            String url = Config.DISEASE_IMAGE_BASE_URL + query;
            //url = url.replace("(", "%28").replace(")", "%29");
            url = url.replace(" ", "_").replace("#", "%23").replace("(","_").replace(")","_").replace(",","_").replace("%20","_");
            if (!url.endsWith(".jpg")) url = url + ".jpg";
            urls.add(url);
            Log.d("disease_url", url);
        }

        return urls;
    }
}
