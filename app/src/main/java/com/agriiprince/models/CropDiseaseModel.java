package com.agriiprince.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Manu Sajjan on 15-12-2018.
 */
public class CropDiseaseModel {

    @SerializedName("error_code")
    private int error_code;
    @SerializedName("message")
    private String message;
    @SerializedName("data_primary")
    private List<CropDiseaseInfoModel> data_primary;
    @SerializedName("data_secondary")
    private List<CropDiseaseInfoModel> data_secondary;

    public int getError_code() {
        return error_code;
    }

    public String getMessage() {
        return message;
    }

    public List<CropDiseaseInfoModel> getData_primary() {
        return data_primary;
    }

    public List<CropDiseaseInfoModel> getData_secondary() {
        return data_secondary;
    }
}
