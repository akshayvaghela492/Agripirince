package com.agriiprince.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.agriiprince.utils.MathUtils;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Manu Sajjan on 21-12-2018.
 *
 * @version 1.2.2
 */
public class HistoricPriceModel implements Parcelable {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

    @SerializedName("arrival")
    private String date;
    @SerializedName("district")
    private String district;
    @SerializedName("state")
    private String state;
    @SerializedName("market")
    private String market;
    @SerializedName("commodity")
    private String crop;
    @SerializedName("variety")
    private String variety;
    @SerializedName("grade")
    private String grade;
    @SerializedName("minP")
    private String minPrice;
    @SerializedName("modP")
    private String modalPrice;
    @SerializedName("maxP")
    private String maxPrice;

    public HistoricPriceModel() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        dest.writeString(district);
        dest.writeString(state);
        dest.writeString(market);
        dest.writeString(crop);
        dest.writeString(variety);
        dest.writeString(grade);
        dest.writeString(minPrice);
        dest.writeString(modalPrice);
        dest.writeString(maxPrice);
    }

    private HistoricPriceModel(Parcel parcel) {
        this.date = parcel.readString();
        this.district = parcel.readString();
        this.state = parcel.readString();
        this.market = parcel.readString();
        this.crop = parcel.readString();
        this.variety = parcel.readString();
        this.grade = parcel.readString();
        this.minPrice = parcel.readString();
        this.modalPrice = parcel.readString();
        this.maxPrice = parcel.readString();
    }

    public final static Parcelable.Creator<HistoricPriceModel> CREATOR = new Creator<HistoricPriceModel>() {
        @Override
        public HistoricPriceModel createFromParcel(Parcel source) {
            return new HistoricPriceModel(source);
        }

        @Override
        public HistoricPriceModel[] newArray(int size) {
            return new HistoricPriceModel[size];
        }
    };

    public String getDate() {
        return date;
    }


    /**
     * returns the date in milliseconds if fail then returns current milliseconds
     *
     * @return long default current milliseconds
     */
    public long getDateInMilliSeconds() {
        try {
            return dateFormat.parse(date).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return Calendar.getInstance().getTimeInMillis();
        }
    }

    public String getDistrict() {
        return district;
    }

    public String getState() {
        return state;
    }

    public String getMarket() {
        return market;
    }

    public String getCrop() {
        return crop;
    }

    public String getVariety() {
        return variety;
    }

    public String getGrade() {
        return grade;
    }

    public String getMinPrice() {
        return minPrice;
    }

    public String getModalPrice() {
        return modalPrice;
    }

    public String getMaxPrice() {
        return maxPrice;
    }

    public String getMinPricePerKg() {
        return String.valueOf(MathUtils.roundDecimal(getPricePerKg(minPrice), MathUtils.DoubleFormat.ONE_DECIMAL));
    }

    public String getModalPricePerKg() {
        return String.valueOf(MathUtils.roundDecimal(getPricePerKg(modalPrice), MathUtils.DoubleFormat.ONE_DECIMAL));
    }

    public String getMaxPricePerKg() {
        return String.valueOf(MathUtils.roundDecimal(getPricePerKg(maxPrice), MathUtils.DoubleFormat.ONE_DECIMAL));
    }

    public static String getApiDate(long timeInMilliSeconds) {
        Date date = new Date(timeInMilliSeconds);
        return dateFormat.format(date);
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public void setCrop(String crop) {
        this.crop = crop;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }

    public void setModalPrice(String modalPrice) {
        this.modalPrice = modalPrice;
    }

    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }

    private static double getPricePerKg(String value) {
        return Double.valueOf(value) / 100;
    }
}
