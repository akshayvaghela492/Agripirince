package com.agriiprince.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationModel {

    @SerializedName("error_code")
    String error_code;
    @SerializedName("message")
    String message;
    @SerializedName("unreaddata")
    List<NotificationData> notificationDataList;

    @SerializedName("readdata")
    List<NotificationData> notificationDataListRead;

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NotificationData> getNotificationDataList() {
        return notificationDataList;
    }

    public void setNotificationDataList(List<NotificationData> notificationDataList) {
        this.notificationDataList = notificationDataList;
    }

    public List<NotificationData> getNotificationDataListRead() {
        return notificationDataListRead;
    }

    public void setNotificationDataListRead(List<NotificationData> notificationDataListRead) {
        this.notificationDataListRead = notificationDataListRead;
    }

    public class NotificationData {
        @SerializedName("read_status")
        private String read_status;
        @SerializedName("delivered_date_time")
        private String delivered_date_time;
        @SerializedName("deliver_status")
        private String deliver_status;
        @SerializedName("receiver")
        private String receiver;
        @SerializedName("created_on")
        private String created_on;
        @SerializedName("receiver_type")
        private String receiver_type;
        @SerializedName("sender_type")
        private String sender_type;
        @SerializedName("medium")
        private String medium;
        @SerializedName("city")
        private String city;
        @SerializedName("commodity_id")
        private String commodity_id;
        @SerializedName("pincode")
        private String pincode;
        @SerializedName("id")
        private String id;
        @SerializedName("message_title")
        private String message_title;
        @SerializedName("message")
        private String message;
        @SerializedName("sender")
        private String sender;
        @SerializedName("read_date_time")
        private String read_date_time;
        @SerializedName("chat_type")
        private String chat_type;
        @SerializedName("message_type")
        private String message_type;

        public String getRead_status() {
            return read_status;
        }

        public void setRead_status(String read_status) {
            this.read_status = read_status;
        }

        public String getDelivered_date_time() {
            return delivered_date_time;
        }

        public void setDelivered_date_time(String delivered_date_time) {
            this.delivered_date_time = delivered_date_time;
        }

        public String getDeliver_status() {
            return deliver_status;
        }

        public void setDeliver_status(String deliver_status) {
            this.deliver_status = deliver_status;
        }

        public String getReceiver() {
            return receiver;
        }

        public void setReceiver(String receiver) {
            this.receiver = receiver;
        }

        public String getCreated_on() {
            return created_on;
        }

        public void setCreated_on(String created_on) {
            this.created_on = created_on;
        }

        public String getReceiver_type() {
            return receiver_type;
        }

        public void setReceiver_type(String receiver_type) {
            this.receiver_type = receiver_type;
        }

        public String getSender_type() {
            return sender_type;
        }

        public void setSender_type(String sender_type) {
            this.sender_type = sender_type;
        }

        public String getMedium() {
            return medium;
        }

        public void setMedium(String medium) {
            this.medium = medium;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCommodity_id() {
            return commodity_id;
        }

        public void setCommodity_id(String commodity_id) {
            this.commodity_id = commodity_id;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMessage_title() {
            return message_title;
        }

        public void setMessage_title(String message_title) {
            this.message_title = message_title;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getSender() {
            return sender;
        }

        public void setSender(String sender) {
            this.sender = sender;
        }

        public String getRead_date_time() {
            return read_date_time;
        }

        public void setRead_date_time(String read_date_time) {
            this.read_date_time = read_date_time;
        }

        public String getChat_type() {
            return chat_type;
        }

        public void setChat_type(String chat_type) {
            this.chat_type = chat_type;
        }

        public String getMessage_type() {
            return message_type;
        }

        public void setMessage_type(String message_type) {
            this.message_type = message_type;
        }
    }
}
