package com.agriiprince.helper;

import android.app.Activity;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.List;

public class UserPermission {

    private OnPermissionListener mListener;

    private Activity mActivity;

    public UserPermission(Activity activity) {
        this.mActivity = activity;
    }

    public void checkPermission(String permission, OnPermissionListener listener) {
        checkPermission(new String[]{permission}, listener);
    }


    public void checkPermission(String[] permissions, OnPermissionListener listener) {
        this.mListener = listener;

        TedPermission.with(mActivity)
                .setPermissionListener(permissionListener)
                .setRationaleTitle("Permissions")
                .setRationaleMessage("You need to allow following permissions for this app to work properly.")
                .setDeniedTitle("Permission denied")
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setGotoSettingButtonText("Permission")
                .setPermissions(permissions)
                .check();
    }

    private PermissionListener permissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            mListener.onPermissionGranted();
        }

        @Override
        public void onPermissionDenied(List<String> deniedPermissions) {
            mListener.onPermissionDenied(deniedPermissions);
        }
    };

    public interface OnPermissionListener {
        void onPermissionGranted();

        void onPermissionDenied(List<String> permissions);
    }


}
