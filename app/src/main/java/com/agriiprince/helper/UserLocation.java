package com.agriiprince.helper;

import android.Manifest;
import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.List;

public class UserLocation extends UserPermission implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private final static int REQUEST_CHECK_SETTINGS = 2000;

    private OnUserLocationResults mListener;

    private Activity mActivity;

    private GoogleApiClient mGoogleApiClient;

    public UserLocation(Activity activity) {
        super(activity);
        this.mActivity = activity;
    }

    public void getLastKnowLocation(OnUserLocationResults listener) {
        mListener = listener;

        checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, new OnPermissionListener() {
            @Override
            public void onPermissionGranted() {
                getLastKnowLocation();
            }

            @Override
            public void onPermissionDenied(List<String> permissions) {
                mListener.onLocationFail();
            }
        });
    }

    public void getCurrentLocation(OnUserLocationResults listener) {
        mListener = listener;

        checkPermission(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                new OnPermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        buildGoogleApiClient();
                    }

                    @Override
                    public void onPermissionDenied(List<String> permissions) {
                        mListener.onLocationFail();
                    }
                });
    }


    @SuppressWarnings("MissingPermission")
    private void getLastKnowLocation() {
        LocationServices.getFusedLocationProviderClient(mActivity)
                .getLastLocation()
                .addOnSuccessListener(
                        new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                if (location != null) mListener.onLocationResult(location);
                                else mListener.onLocationFail();
                            }
                        }
                )
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                e.printStackTrace();

                                mListener.onLocationFail();
                            }
                        }
                );
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

        mGoogleApiClient.connect();

        final LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        Task<LocationSettingsResponse> task = LocationServices.getSettingsClient(mActivity).checkLocationSettings(builder.build());

        task.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                getLastKnowLocation();
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(mActivity, "Connection Failed", Toast.LENGTH_SHORT).show();

    }

    public interface OnUserLocationResults {

        void onLocationResult(Location lastLocation);

        void onLocationFail();

    }
}
