package com.agriiprince.helper;

import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

//Statistical Helper Class
public class PriceDistribution<T> {

    private final static String TAG = PriceDistribution.class.getSimpleName();

    private int MAX_INTERVAL = 7;
    private int INTERVAL = 0;

    private int mUnitsPerInterval = 0;
    private int mUnitsOverflow = 0;

    private Distribute<T> mListener;

    private Comparator<T> mComparator;

    private List<T> mList;

    private List<Double> mIntervals;

    public PriceDistribution(List<T> list, Comparator<T> comparator, Distribute<T> listener) {
        this.mList = list;
        this.mComparator = comparator;
        this.mListener = listener;
        this.mIntervals = new ArrayList<>();
    }

    public void removeListener() {
        mListener = null;
    }

    public void distribute(List<T> list) {
        this.mList = list;
        if (mList != null && mList.size() > 0 && mListener != null) {
//            Log.d(TAG, "" + mList.size());
            mIntervals.clear();

            INTERVAL = mList.size() < MAX_INTERVAL ? mList.size() : MAX_INTERVAL; // max interval must be less than total data points or equal

            // intervals are calculated from index 0, subtract -1
            mUnitsPerInterval = (mList.size() - 1) / INTERVAL; // find the number of data points fall in each interval
            mUnitsOverflow = (mList.size() - 1) % INTERVAL; // number of data points remaining after distribution equally

//            Log.d(TAG, "unitsPerInterval : " + mUnitsPerInterval + " unitsOverFlow : " + mUnitsOverflow);

            if (INTERVAL > 0) // list is empty don't run the distribution
                new CalculateInterval<>(
                        MAX_INTERVAL, INTERVAL, mUnitsPerInterval,
                        mUnitsOverflow, mComparator, mIntervals, mList, mListener
                ).execute();

            else
                mListener.failure();
        }
    }

    public void setMaxInterval(int maxInterval) {
        if (maxInterval > 0) {
            MAX_INTERVAL = maxInterval;
        } else {
            throw new IllegalArgumentException("max interval must be greater than 0");
        }
    }

    public int getInterval() {
        return INTERVAL;
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {

        }
    };


    private static class CalculateInterval<E> extends AsyncTask<Void, Void, Boolean> {

        private int MAX_INTERVAL;
        private int INTERVAL;
        private int mUnitsPerInterval;
        private int mUnitsOverflow;

        private Distribute<E> mListener;

        private Comparator<E> mComparator;

        private List<Double> mIntervals;
        private List<E> mList;


        private CalculateInterval(int maxInterval, int interval, int unitsPerInterval, int unitsOverFlow, Comparator<E> comparator,
                                  List<Double> intervals, List<E> list, Distribute<E> listener) {

            this.MAX_INTERVAL = maxInterval;
            this.INTERVAL = interval;
            this.mUnitsPerInterval = unitsPerInterval;
            this.mUnitsOverflow = unitsOverFlow;
            this.mIntervals = intervals;
            this.mComparator = comparator;
            this.mList = list;
            this.mListener = listener;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            Collections.sort(mList, mComparator); // sort collection in ascending order

            int index = 0;
            int intervalOffset = 0;
            for (int i = 0; i <= INTERVAL; i++) {

                double value = mListener.getIntervalPrice(mList.get(index));

                Log.d(TAG, "index : " + index + "   value : " + value);

                index += mUnitsPerInterval;
                if (mUnitsOverflow > 0) {
                    index++; // add one index until left out data points are included
                    mUnitsOverflow--;
//                    Log.d(TAG, "unitsOverFlow : " + mUnitsOverflow);
                }

                int currentIntervalSize = mIntervals.size();
                if (currentIntervalSize > 0 && mIntervals.get(currentIntervalSize - 1) == value) {
                    intervalOffset++;
                    continue;
                }

                mIntervals.add(value);

            }

            INTERVAL = INTERVAL - intervalOffset;

            for (int i = mIntervals.size(); i <= MAX_INTERVAL; i++) {
                mIntervals.add(mIntervals.get(i - 1));
                INTERVAL++;
            }
            /*
            if (mIntervals.size() > MAX_INTERVAL) {
                mIntervals.subList(MAX_INTERVAL + 1, mIntervals.size()).clear();
                INTERVAL = mIntervals.size();
            }
*/
/*
            for (int i = mIntervals.size() - 1; i > 0; i--) {
                if (INTERVAL > 1 && mIntervals.get(i).equals(mIntervals.get(i - 1))) {
                    mIntervals.remove(i);
                    --INTERVAL;
                }
            }
*/

            return true;
        }

        @Override
        protected void onPostExecute(Boolean boo) {
            if (boo) {
                Log.d(TAG, "INTERVAL: " + INTERVAL + "  mIntervals: " + mIntervals);
                mListener.onSuccess(INTERVAL, mIntervals, mList);

            } else {
                mListener.failure();
            }
        }
    }

    public interface Distribute<T> {

        double getIntervalPrice(T o);

        void onSuccess(int interval, List<Double> intervals, List<T> list);

        void failure();
    }

}
