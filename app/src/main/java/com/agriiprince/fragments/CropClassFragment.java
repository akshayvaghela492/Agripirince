package com.agriiprince.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.adapter.CropClassAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.databinding.FragmentCropClassBinding;
import com.agriiprince.mvvm.model.crop.CropClass;
import com.agriiprince.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnCropClassInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CropClassFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CropClassFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnCropClassInteractionListener mListener;

    private StringUtils stringUtils;

    public CropClassFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CropClassFragment.
     */
    public static CropClassFragment newInstance() {
        CropClassFragment fragment = new CropClassFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_crop_class, container, false);
    }


    FragmentCropClassBinding cropClassBinding;
    CropClassAdapter adapter;
    List<CropClass> classList;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cropClassBinding = DataBindingUtil.bind(view);

        stringUtils = AppController.getInstance().getStringUtils();
        classList = new ArrayList<>();
        adapter = new CropClassAdapter(getContext(), classList, cropClassListener);

        cropClassBinding.cropClassMain.setOnClickListener(onClickOutside);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        cropClassBinding.selectCropClass.setText(stringUtils.getLocalizedString(R.string.select_crop_class));

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        cropClassBinding.cropClassRecycler.setLayoutManager(gridLayoutManager);
        cropClassBinding.cropClassRecycler.setAdapter(adapter);

        getCropClass();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCropClassInteractionListener) {
            mListener = (OnCropClassInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnCropClassInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void getCropClass() {
        adapter.addAll(CropClass.getCropClassList(getContext()));
    }

    private View.OnClickListener onClickOutside = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            popFragmentBack();
        }
    };

    private void popFragmentBack() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    private CropClassAdapter.CropClassListener cropClassListener = new CropClassAdapter.CropClassListener() {
        @Override
        public void onClickItem(int position) {
            CropClass cropClass = adapter.getItem(position);
            CropClassFragment.this.mListener.onSelectCropClass(cropClass.getCrop_class());
        }
    };

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnCropClassInteractionListener {

        void onSelectCropClass(String cropClass);
    }
}
