package com.agriiprince.fragments;


import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.agriiprince.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PredictedPriceChartsBarsFragment extends Fragment {


    public PredictedPriceChartsBarsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_predicted_price_charts_bars, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button btnStart = view.findViewById(R.id.btnStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Builder(getContext())
                        .setMessage("You have not subscribed to this feature")
                        .setPositiveButton("Okay", new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //FarmerPesticideVendorChatActivity.super.onBackPressed();
                            }
                        }).setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        //FarmerPesticideVendorChatActivity.super.onBackPressed();
                    }
                }).show();
            }
        });
    }
}
