package com.agriiprince.fragments.rss;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.text.InputType;
import android.text.TextUtils;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agriiprince.R;
import com.bumptech.glide.Glide;

public class RssDetailsFragment extends Fragment {

    private static final String ARG_SOURCE = "source";
    private static final String ARG_TITLE = "title";
    private static final String ARG_LINK = "link";
    private static final String ARG_DATE = "date";
    private static final String ARG_IMAGE_URL = "url";
    private static final String ARG_DESCRIPTION = "description";

    private String source;
    private String title;
    private String link;
    private String date;
    private String url;
    private String description;

    private AppCompatTextView mTvSource;
    private TextView mTvTitle;
    private TextView mTvLink;
    private TextView mTvDate;
    private TextView mTvDescription;

    private ImageView mImage;

    private OnFragmentInteractionListener mListener;

    public RssDetailsFragment() {
        // Required empty public constructor
    }

    public static RssDetailsFragment newInstance(String source, String title, String link,
                                                 String date, String url, String description) {

        RssDetailsFragment fragment = new RssDetailsFragment();
        Bundle args = new Bundle();

        args.putString(ARG_SOURCE, source);
        args.putString(ARG_TITLE, title);
        args.putString(ARG_LINK, link);
        args.putString(ARG_DATE, date);
        args.putString(ARG_IMAGE_URL, url);
        args.putString(ARG_DESCRIPTION, description);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            source = getArguments().getString(ARG_SOURCE);
            title = getArguments().getString(ARG_TITLE);
            link = getArguments().getString(ARG_LINK);
            date = getArguments().getString(ARG_DATE);
            url = getArguments().getString(ARG_IMAGE_URL);
            description = getArguments().getString(ARG_DESCRIPTION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rss_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mTvSource = view.findViewById(R.id.rss_details_source);
        mTvTitle = view.findViewById(R.id.rss_details_title);
        mTvLink = view.findViewById(R.id.rss_details_link);
        mTvDate = view.findViewById(R.id.rss_details_date);
        mTvDescription = view.findViewById(R.id.rss_details_description);

        mImage = view.findViewById(R.id.rss_details_image);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (!TextUtils.isEmpty(url)) {
            Glide.with(getContext())
                    .load(url)
                    .into(mImage);
        }

        mTvSource.setText(source);
        mTvTitle.setText(title);
        mTvLink.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
        mTvDate.setText(date);
        mTvDescription.setText(description);

        if (!TextUtils.isEmpty(link)) {
            mTvLink.setText(link);
            Linkify.addLinks(mTvLink, Linkify.WEB_URLS);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*
        if (context instanceof OnInfoTabInteractionListener) {
            mListener = (OnInfoTabInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnInfoTabInteractionListener");
        }
        */
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

    }
}
