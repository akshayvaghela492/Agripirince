package com.agriiprince.fragments.rss;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.agriiprince.R;
import com.agriiprince.activities.farmer.rss.RssActivity;
import com.agriiprince.adapter.rss.RssArrangeByTimeAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.FragmentRssArrangeByTimeBinding;
import com.agriiprince.dataservice.DataResponse;
import com.agriiprince.dataservice.RssDataService;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.RssFeedModel;
import com.agriiprince.model.RssModel;
import com.agriiprince.utils.DateConvertUtils;
import com.agriiprince.utils.StringUtils;
import com.android.volley.VolleyError;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.agriiprince.utils.Utils.showLog;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link RssArrangeByTime#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RssArrangeByTime extends Fragment {
    private final String TAG = RssArrangeByTime.class.getSimpleName();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    FragmentRssArrangeByTimeBinding binding;

    RssArrangeByTimeAdapter adapter;

    private SparseBooleanArray mSelectedRssTopics;
    List<RssFeedModel> mFeedModelList;
    List<RssModel> rssModels;

    Calendar calendar;
    Date today;

    DateConvertUtils dateConvertUtils;

    StringUtils stringUtils;
    UserProfile userProfile;

    public RssArrangeByTime() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RssArrangeByTimeAdapter.
     */
    // TODO: Rename and change types and number of parameters
    public static RssArrangeByTime newInstance() {
        return new RssArrangeByTime();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rss_arrange_by_time, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = DataBindingUtil.bind(view);

        stringUtils = AppController.getInstance().getStringUtils();
        userProfile = new UserProfile(getContext());

        mSelectedRssTopics = new SparseBooleanArray();

        dateConvertUtils = new DateConvertUtils();
        mFeedModelList = new ArrayList<>();

        calendar = Calendar.getInstance();
        today = calendar.getTime();
        adapter = new RssArrangeByTimeAdapter(mFeedModelList, clickRssTimeListener, calendar);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        binding.rvRssTime.setLayoutManager(layoutManager);
        binding.rvRssTime.setAdapter(adapter);

        binding.preDay.setOnClickListener(prevDayListener);
        binding.nextDay.setOnClickListener(nextDayListener);
        binding.datePick.setOnClickListener(pickDateListener);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        hideEmptyText();
        showProgressBar();
        PrefManager prefManager = new PrefManager(getContext());

        String rss = prefManager.getRSStopics();
        if (TextUtils.isEmpty(rss)) {
            getActivity().getSupportFragmentManager().popBackStack();

            showSubscribeToRss();
            hideProgressBar();
        } else {
            String[] topics = prefManager.getRSStopics().split(",");
            for (String value : topics) {
                try {
                    mSelectedRssTopics.put(Integer.parseInt(value), true);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
            RssDataService.get(dataResponse, userProfile);
        }

        setDate();
    }

    private void setDate() {
        SimpleDateFormat format = new SimpleDateFormat("dd  MMM yyyy", Locale.ENGLISH);
        String date = format.format(calendar.getTime());
        binding.date.setText(date);
    }

    private View.OnClickListener nextDayListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            hideProgressBar();
            Date date = calendar.getTime();
            if (isDateBeforeToday(date)) {
                calendar.add(Calendar.DATE, 1);
                setDate();
                adapter.setDate(calendar);
                checkFeedList();
            }
        }
    };

    private View.OnClickListener prevDayListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            hideProgressBar();
            calendar.add(Calendar.DATE, -1);
            setDate();
            adapter.setDate(calendar);
            checkFeedList();
        }
    };

    private View.OnClickListener pickDateListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            hideProgressBar();
            showDatePicker();
        }
    };

    private boolean isDateBeforeToday(Date date) {
        return date.before(today);
    }


    private void showDatePicker() {
        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        Calendar cal = Calendar.getInstance();
                        cal.set(year, monthOfYear, dayOfMonth);
                        if (isDateBeforeToday(cal.getTime())) {
                            calendar.set(year, monthOfYear, dayOfMonth);
                            setDate();
                            adapter.setDate(calendar);
                            checkFeedList();
                        }

                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "Okay", datePickerDialog);
        datePickerDialog.show();
    }


    private void checkFeedList() {
        if (adapter.getItemCount() > 0) hideEmptyText();
        else showEmptyText();

        Log.d(TAG, "" + adapter.getItemCount());
    }

    private void showProgressBar() {
        binding.progress.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        binding.progress.setVisibility(View.GONE);
    }

    private void showTryLater() {
        showEmptyText(R.string.rss_try_later);
    }

    private void showSubscribeToRss() {
        showEmptyText(R.string.rss_please_subscribe);
    }

    private void showEmptyText() {
        showEmptyText(R.string.no_news_available_now);
    }

    private void showEmptyText(@StringRes int stringId) {
        binding.emptyText.setText(stringUtils.getLocalizedString(stringId));
        binding.emptyText.setVisibility(View.VISIBLE);
    }

    private void hideEmptyText() {
        binding.emptyText.setVisibility(View.GONE);

    }

    private void getSubscribedRssFeeds() {
        for (RssModel model : rssModels) {
            if (mSelectedRssTopics.get(model.id)) {
                new FetchFeedTask(model.rss_feed_description, model.rss_feed_link).execute();

            }
        }
    }


    private DataResponse<List<RssModel>> dataResponse = new DataResponse<List<RssModel>>() {
        @Override
        public void onSuccessResponse(List<RssModel> response) {
            RssArrangeByTime.this.rssModels = response;
            hideProgressBar();
            hideEmptyText();
            getSubscribedRssFeeds();
        }

        @Override
        public void onParseError() {
            showTryLater();
            hideProgressBar();
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            showTryLater();
            hideProgressBar();
        }
    };


    private class FetchFeedTask extends AsyncTask<Void, Void, List<RssFeedModel>> {

        String TAG = "rss-log";
        String source;

        private String urlLink;

        public FetchFeedTask(String s, String link) {
            source = s;
            urlLink = link;
        }

        @Override
        protected List<RssFeedModel> doInBackground(Void... voids) {
            try {
                if (!urlLink.startsWith("http://") && !urlLink.startsWith("https://"))
                    urlLink = "http://" + urlLink;

                showLog("Fetching feed from " + source);
                URL url = new URL(urlLink);
                InputStream inputStream = url.openConnection().getInputStream();
                showLog("Fetch complete. Begin Parsing");

                List<RssFeedModel> ls = dateConvertUtils.parseFeed(source, inputStream);
                showLog("parsing finsihed. Size of parsed items : " + ls.size());
                if (ls.size() > 0) {
                    for (RssFeedModel model : ls) {
                        for (RssModel rssModel : rssModels) {
                            if (rssModel.rss_feed_description.equalsIgnoreCase(model.source)) {
                                model.setTopics(rssModel.categories);
                                break;
                            }
                        }
                    }

                    inputStream.close();
                    return ls;
                }

                Log.d(TAG, source + "   :   true");

                inputStream.close();
                return null;
            } catch (IOException e) {
                Log.e(TAG, "Error", e);
            } catch (XmlPullParserException e) {
                Log.e(TAG, "Error", e);
            }
            Log.d(TAG, source + "   :   false");
            return null;
        }

        @Override
        protected void onPostExecute(List<RssFeedModel> result) {
            if (result != null) {
                adapter.addAll(result);
            }
            if (adapter.getItemCount() == 0) showEmptyText();
            else {
                hideProgressBar();
                hideEmptyText();
            }
        }
    }


    private RssArrangeByTimeAdapter.OnClickRssTimeListener clickRssTimeListener = new RssArrangeByTimeAdapter.OnClickRssTimeListener() {
        @Override
        public void onClickRssTimeItem(int position) {
            RssFeedModel rssFeedModel = adapter.get(position);

            Intent intent = new Intent(getContext(), RssActivity.class);
            intent.putExtra(RssActivity.RSS_FEED_MODEL, rssFeedModel);
            startActivity(intent);
        }
    };
}
