package com.agriiprince.fragments.rss;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.adapter.RssSubscriptionAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.dataservice.FarmerProfileDataService;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.FarmerProfileModel;
import com.agriiprince.model.RssModel;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class RssSubscribeFragment extends Fragment
        implements View.OnClickListener, RssSubscriptionAdapter.OnClickRssSubscriptionListener {

    private static final String TAG = RssSubscribeFragment.class.getName();

    private OnRssSubscribeInteractionListener mListener;

    private UserProfile mUserProfile;

    private RssSubscriptionAdapter mAdapter;

    private SparseBooleanArray mSelectedRssTopics;

    private List<RssModel> mRssModels;
    private List<String> mRssCategories;

    private StringUtils stringUtils;

    public RssSubscribeFragment() {
        // Required empty public constructor
    }

    public static RssSubscribeFragment newInstance() {
        RssSubscribeFragment fragment = new RssSubscribeFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onSubscribe(int rssId, int position) {
        mSelectedRssTopics.put(rssId, !mSelectedRssTopics.get(rssId));
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_rss_subscribe, container, false);
        setHasOptionsMenu(false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);
        stringUtils = AppController.getInstance().getStringUtils();
        mUserProfile = new UserProfile(getContext());

        mRssCategories = new ArrayList<>();
        mRssModels = new ArrayList<>();
        mSelectedRssTopics = new SparseBooleanArray();

        mAdapter = new RssSubscriptionAdapter(getContext());
        mAdapter.addOnClickListener(this);

        AppCompatTextView textView = view.findViewById(R.id.rss_subscription_next);
        textView.setOnClickListener(this);
        textView.setText(stringUtils.getLocalizedString(R.string.save_my_choices));

        ((TextView) view.findViewById(R.id.text_head_2)).setText(stringUtils.getLocalizedString(R.string.subscribe_to_news_topics));
        ((TextView) view.findViewById(R.id.text_head_1)).setText(stringUtils.getLocalizedString(R.string.to_begin_with));

        RecyclerView recyclerView = view.findViewById(R.id.rss_subscription_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setNestedScrollingEnabled(false);

        recyclerView.setAdapter(mAdapter);

        getFarmerSubscriptions();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mListener != null)
            mListener.onChangeLabel(stringUtils.getLocalizedString(R.string.news).toString());
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.addOnClickListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mAdapter.removeOnClickListener();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mListener != null) mListener.onChangeLabel(null);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRssSubscribeInteractionListener) {
            mListener = (OnRssSubscribeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnRssSubscribeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.rss_subscription_next) {
            List<Integer> topics = new ArrayList<>();

            for (int i = 0; i < mSelectedRssTopics.size(); i++) {
                if (mSelectedRssTopics.valueAt(i)) {
                    topics.add(mSelectedRssTopics.keyAt(i));
                }
            }

            if (topics.size() == 0) {
                Toast.makeText(getContext(), stringUtils.getLocalizedString(R.string.please_select_topic_to_continue), Toast.LENGTH_SHORT).show();
                return;
            }

            String rssTopics = TextUtils.join(",", topics);

            // in case of empty string pass non empty string to server
            if (TextUtils.isEmpty(rssTopics)) rssTopics = " ";

            PrefManager prefManager = new PrefManager(getContext());
            prefManager.setRSStopics(rssTopics);

            Log.d(TAG, prefManager.getRSStopics());

            subscribeRssTopics(rssTopics);
        }
    }

    private void getFarmerSubscriptions() {
        FarmerProfileDataService dataService = new FarmerProfileDataService(getActivity());

        dataService.getFarmerProfile(new FarmerProfileDataService.OnFarmerProfileResult() {
            @Override
            public void onSuccess(FarmerProfileModel farmerProfileModel) {
                String rssTopic = farmerProfileModel.interested_rss_topics.trim();

                String[] rssTopics = rssTopic.split(",");

                for (String rssId : rssTopics) {
                    rssId = rssId.trim();

                    if (!TextUtils.isEmpty(rssId))
                        mSelectedRssTopics.put(Integer.parseInt(rssId), true);
                }

                mAdapter.addSelectedRss(mSelectedRssTopics);

                getRssTopics();
            }

            @Override
            public void onResultError() {

            }
        });
    }

    private void getRssTopics() {
        if (mListener != null)
            mListener.showProgress();
        StringRequest request = new StringRequest(Request.Method.POST, Config.POST_RSS_LIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY", "get Rss subscribe fragment"+response);
                        try {
/*
                            JSONArray responseArray = new JSONArray(response);
                            JSONObject responseObject = responseArray.getJSONObject(0);
                            if (responseObject.getInt("error_code") == 100) {
                                HashSet<String> category = new HashSet<>();
                                String data = responseObject.getString("data");
                                mRssModels = new Gson().fromJson(data, new TypeToken<List<RssModel>>() {
                                }.getType());
*/
                            JSONObject Object = new JSONObject(response);
                            if (Object.getInt("code") == 200) {
                                Log.d("TESTING_VOLLEY", "get Rss subscribe fragment code "+Object.getInt("code"));
                                HashSet<String> category = new HashSet<>();
                                JSONObject object=Object.getJSONObject("data");
                                List<RssModel> mRssModels = new Gson().fromJson(String.valueOf(object.getJSONArray("data")),
                                        new TypeToken<List<RssModel>>() {
                                        }.getType());
                                Log.d("TESTING_VOLLEY", "get Rss subscribe fragment list "+mRssModels);
                                for (RssModel model : mRssModels) {
                                    String[] categories = model.categories.split(",");
                                    for (String value : categories) {
                                        value = value.trim();
                                        category.add(value);
                                    }
                                }


                                mRssCategories.addAll(category);
                                mAdapter.addRssList(mRssModels);
                                mAdapter.addAllCategory(mRssCategories);
                                mAdapter.notifyDataSetChanged();

                            } else {
                                Toast.makeText(getContext(), R.string.please_try_again, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Log.d("TESTING_VOLLEY", "get Rss subscribe fragment exception  "+e.getMessage());
                            e.printStackTrace();
                        } finally {
                            if (mListener != null)
                                mListener.dismissProgress();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mListener != null)
                            mListener.dismissProgress();
                        Log.d("TESTING_VOLLEY", "get Rss subscribe fragment error   "+error.getMessage());
                        Utils.showVolleyError(getContext(), error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication", mUserProfile.getApiToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(request);
    }


    private void subscribeRssTopics(final String rssIds) {
        if (mListener != null)
            mListener.showProgress();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.POST_RSS_UPDATE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY",response);
                        try {
                            //JSONArray jsonArray = new JSONArray(response);
                            //JSONObject jsonObject = jsonArray.getJSONObject(0);
                            JSONObject object=new JSONObject(response);
                            int code = object.getInt("code");
                            Log.d("TESTING_VOLLEY", String.valueOf(code));
                            //int error_code = jsonObject.getInt("error_code");
                            //if (error_code == 100 || error_code == 107)
                            if (code == 200 || code == 107)
                            {
                                if (mListener != null)
                                    mListener.onSuccess();

                                Toast.makeText(getContext(), stringUtils.getLocalizedString(R.string.updated),
                                        Toast.LENGTH_SHORT).show();

                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                fragmentManager.popBackStack();
                            }
                            //else if (error_code == 101)
                            else if (code == 101)
                            {
                                Toast.makeText(getContext(),
                                        stringUtils.getLocalizedString(R.string.something_went_wrong),
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            if (mListener != null) mListener.dismissProgress();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mListener != null) mListener.dismissProgress();

                        error.printStackTrace();
                        Log.d(TAG, error.toString());
                        Utils.showVolleyError(getContext(), error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());

                params.put("user_type", mUserProfile.getUserType());
                params.put("user_id", mUserProfile.getUserId());
                params.put("rss_feed_id", rssIds);

                Log.d(TAG, "RSS update params : " + params.toString());
                return params;

            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication", mUserProfile.getApiToken());
                return header;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }


    public interface OnRssSubscribeInteractionListener {

        void showProgress();

        void dismissProgress();

        void onChangeLabel(String label);

        void onSuccess();
    }
}
