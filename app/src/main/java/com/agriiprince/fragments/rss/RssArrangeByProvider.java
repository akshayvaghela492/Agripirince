package com.agriiprince.fragments.rss;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.activities.farmer.rss.RssActivity;
import com.agriiprince.adapter.rss.RssArrangeByProviderAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.FragmentRssArrangeByProviderBinding;
import com.agriiprince.dataservice.DataResponse;
import com.agriiprince.dataservice.RssDataService;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.RssModel;
import com.agriiprince.utils.StringUtils;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

public class RssArrangeByProvider extends Fragment {

    public static RssArrangeByProvider newInstance() {
        return new RssArrangeByProvider();
    }

    FragmentRssArrangeByProviderBinding binding;

    List<RssModel> rssModels;

    private SparseBooleanArray mSelectedRssTopics;

    StringUtils stringUtils;

    UserProfile userProfile;

    RssArrangeByProviderAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        stringUtils = AppController.getInstance().getStringUtils();
        userProfile = new UserProfile(getContext());
        return inflater.inflate(R.layout.fragment_rss_arrange_by_provider, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = DataBindingUtil.bind(view);

        rssModels = new ArrayList<>();

        adapter = new RssArrangeByProviderAdapter(stringUtils, rssModels, listener);

        mSelectedRssTopics = new SparseBooleanArray();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        removeEmptyText();

        binding.rssProviderRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rssProviderRecycler.setAdapter(adapter);

        showProgressBar();
        PrefManager prefManager = new PrefManager(getContext());

        String rss = prefManager.getRSStopics();
        if (TextUtils.isEmpty(rss)) {
            getActivity().getSupportFragmentManager().popBackStack();

            showSubscribeToRss();
            hideProgressBar();
        } else {
            String[] topics = prefManager.getRSStopics().split(",");
            for (String value : topics) {
                try {
                    mSelectedRssTopics.put(Integer.parseInt(value), true);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
            RssDataService.get(dataResponse, userProfile);
        }

    }

    private void showProgressBar() {
        binding.progress.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        binding.progress.setVisibility(View.GONE);
    }

    private void showTryLater() {
        showEmptyText(R.string.rss_try_later);
    }

    private void showSubscribeToRss() {
        showEmptyText(R.string.rss_please_subscribe);
    }

    private void showEmptyText(@StringRes int stringId) {
        binding.emptyText.setText(stringUtils.getLocalizedString(stringId));
        binding.emptyText.setVisibility(View.VISIBLE);
    }

    private void removeEmptyText() {
        binding.emptyText.setVisibility(View.GONE);

    }


    private void getSubscribedRssFeeds(List<RssModel> mRssModels) {
        for (RssModel model : mRssModels) {
            if (mSelectedRssTopics.get(model.id)) {
                adapter.add(model);
            }
        }
    }

    private RssArrangeByProviderAdapter.OnClickRssProviderListener listener = new RssArrangeByProviderAdapter.OnClickRssProviderListener() {
        @Override
        public void OnClickRssProvider(int position) {
            RssModel rssModel = adapter.get(position);

            Intent intent = new Intent(getContext(), RssActivity.class);
            intent.putExtra(RssActivity.RSS_MODEL, rssModel);
            startActivity(intent);
        }
    };


    private DataResponse<List<RssModel>> dataResponse = new DataResponse<List<RssModel>>() {
        @Override
        public void onSuccessResponse(List<RssModel> response) {
            hideProgressBar();
            removeEmptyText();
            getSubscribedRssFeeds(response);
        }

        @Override
        public void onParseError() {
            showTryLater();
            hideProgressBar();
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            showTryLater();
            hideProgressBar();
        }
    };
}
