package com.agriiprince.fragments.rss;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.adapter.RssFeedListAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.RssFeedModel;
import com.agriiprince.model.RssModel;
import com.agriiprince.utils.DateConvertUtils;
import com.agriiprince.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.agriiprince.utils.Utils.showLog;

public class RssFeedFragment extends Fragment {

    private static final String TAG = RssFeedFragment.class.getName();

    private OnRssFeedInteractionListener mListener;

    private UserProfile mUserProfile;

    private SparseBooleanArray mSelectedRssTopics;

    private List<RssModel> mRssModels;

    private boolean isTopicsLoaded = false;

    private TextView mStatus;

    RecyclerView rss_rv;

    DateConvertUtils dateConvertUtils;

    RssFeedListAdapter rssFeedListAdapter;

    ArrayList<RssFeedModel> mFeedModelList;

    public RssFeedFragment() {
        // Required empty public constructor
    }

    public static RssFeedFragment newInstance() {
        RssFeedFragment fragment = new RssFeedFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_rss_feed, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        if (mListener != null)
            mListener.onChangeLabel(getString(R.string.news));

        //ImageView iv_edit = (ImageView) getView().findViewById(R.id.menu_rss_subscription);
        //iv_edit.setVisibility(View.INVISIBLE);

        mUserProfile = new UserProfile(getContext());

        dateConvertUtils = new DateConvertUtils();
        mSelectedRssTopics = new SparseBooleanArray();

        mStatus = view.findViewById(R.id.rss_feed_status);

        rss_rv = view.findViewById(R.id.rss_feed_list);
        rss_rv.setLayoutManager(new LinearLayoutManager(getContext()));

        mFeedModelList = new ArrayList<>();
        rssFeedListAdapter = new RssFeedListAdapter(getContext(), mFeedModelList);
        rss_rv.setAdapter(rssFeedListAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        PrefManager prefManager = new PrefManager(getContext());

        String rss = prefManager.getRSStopics();
        if (TextUtils.isEmpty(rss)) {
            getActivity().getSupportFragmentManager().popBackStack();

            startRssSubscribe();
        } else {
            String[] topics = prefManager.getRSStopics().split(",");

            for (String value : topics) {
                try {
                    mSelectedRssTopics.put(Integer.parseInt(value), true);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
            getRssTopics();
        }

        rssFeedListAdapter.addOnClickListener(new RssFeedListAdapter.OnClickRssFeedItemListener() {
            @Override
            public void onClickItem(int position) {
                RssFeedModel rssFeedModel = rssFeedListAdapter.get(position);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
                String date = simpleDateFormat.format(rssFeedModel.getDate());

                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.farmer_home_frame,
                                RssDetailsFragment.newInstance(
                                        rssFeedModel.getSource(),
                                        rssFeedModel.getTitle(),
                                        rssFeedModel.getLink(),
                                        date,
                                        rssFeedModel.getImg(),
                                        rssFeedModel.getDescription()))
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_rss, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_rss_subscription) {
            startRssSubscribe();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mListener != null)
            mListener.onChangeLabel(null);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRssFeedInteractionListener) {
            mListener = (OnRssFeedInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnRssFeedInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (isTopicsLoaded) {
            PrefManager prefManager = new PrefManager(getContext());
            String[] topics = prefManager.getRSStopics().split(",");

            if (TextUtils.isEmpty(topics[0])) {
                startRssSubscribe();
            } else {
                for (String value : topics) {
                    mSelectedRssTopics.put(Integer.parseInt(value), true);
                }

                getSubscribedRssFeeds();
            }
        }
    }


    private void startRssSubscribe() {
        getActivity().getSupportFragmentManager().beginTransaction()
                .add(R.id.farmer_home_frame, RssSubscribeFragment.newInstance(), RssSubscribeFragment.class.getSimpleName())
                .addToBackStack(RssSubscribeFragment.class.getSimpleName())
                .commit();
    }

    private void getRssTopics() {
        if (mListener != null)
            mListener.showProgress();

        StringRequest request = new StringRequest(Request.Method.POST, Config.POST_RSS_LIST,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY", "get Rss feed "+response);
                        try {

           /*                 JSONArray responseArray = new JSONArray(response);
                            JSONObject responseObject = responseArray.getJSONObject(0);
                            if (responseObject.getInt("error_code") == 100) {
                                String data = responseObject.getString("data");
                                mRssModels = new Gson().fromJson(data, new TypeToken<List<RssModel>>() {
                                }.getType());
            */                 JSONObject Object = new JSONObject(response);
                            if (Object.getInt("code") == 200) {
                                Log.d("TESTING_VOLLEY", "get rss feed code"+Object.getInt("code"));
                                JSONObject object=Object.getJSONObject("data");
                                mRssModels = new Gson().fromJson(String.valueOf(object.getJSONArray("data")),
                                        new TypeToken<List<RssModel>>() {
                                        }.getType());
                                isTopicsLoaded = true;
                                Log.d("TESTING_VOLLEY", "get rss feed topics"+mRssModels);
                                getSubscribedRssFeeds();

                            } else {
                                Toast.makeText(getContext(), "Please Try again", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("TESTING_VOLLEY", "get rss feed exception "+e.getMessage());
                        } finally {
                            if (mListener != null)
                                mListener.dismissProgress();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mListener != null)
                            mListener.dismissProgress();
                        Log.d("TESTING_VOLLEY", "get rss feed error "+error.getMessage());
                        Utils.showVolleyError(getContext(), error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication", mUserProfile.getApiToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(request);
    }

    private void getSubscribedRssFeeds() {
        for (RssModel model : mRssModels) {
            if (mSelectedRssTopics.get(model.id)) {
                new FetchFeedTask(model.rss_feed_description, model.rss_feed_link).execute((Void) null);

            }
        }
    }

    private class FetchFeedTask extends AsyncTask<Void, Void, Boolean> {

        String TAG = "rss-log";
        String source;

        private String urlLink;

        public FetchFeedTask(String s, String link) {
            source = s;
            urlLink = link;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                if (!urlLink.startsWith("http://") && !urlLink.startsWith("https://"))
                    urlLink = "http://" + urlLink;

                showLog("Fetching feed from " + source);
                URL url = new URL(urlLink);
                InputStream inputStream = url.openConnection().getInputStream();
                showLog("Fetch complete. Begin Parsing");

                List<RssFeedModel> ls = dateConvertUtils.parseFeed(source, inputStream);
                showLog("parsing finsihed. Size of parsed items : " + ls.size());
                if (ls.size() > 0) {
                    mFeedModelList.addAll(ls);
                }

                Log.d(TAG, source + "   :   true");

                return true;
            } catch (IOException e) {
                Log.e(TAG, "Error", e);
            } catch (XmlPullParserException e) {
                Log.e(TAG, "Error", e);
            }
            Log.d(TAG, source + "   :   false");
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (mListener != null) mListener.dismissProgress();
            if (success) {
                // Fill RecyclerView
                rssFeedListAdapter.notifyDataSetChanged();
            }
            if (rssFeedListAdapter.getItemCount() > 0) {
                mStatus.setVisibility(View.GONE);
            } else {
                mStatus.setText(R.string.no_news_available_now);
            }
        }
    }

    public interface OnRssFeedInteractionListener {

        void showProgress();

        void dismissProgress();

        void onChangeLabel(String label);
    }
}
