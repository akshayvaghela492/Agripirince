package com.agriiprince.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.adapter.NotificationAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.databinding.FragmentNotificaitonBinding;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.models.NotificationModel;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.Utils;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.agriiprince.utils.Utils.showLog;

public class NotificationFragment extends Fragment {

    public static NotificationFragment newInstance() {
        return new NotificationFragment();
    }

    OnNotificationInteractionListener mListener;

    UserProfile userProfile;

    StringUtils utils;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        userProfile = new UserProfile(getActivity());
        utils = AppController.getInstance().getStringUtils();
        setHasOptionsMenu(false);
        return inflater.inflate(R.layout.fragment_notificaiton, container, false);
    }

    FragmentNotificaitonBinding binding;
    private NotificationAdapter mAdapter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = DataBindingUtil.bind(view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.rvNotification.setLayoutManager(linearLayoutManager);

        // hidden progress


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mListener.onChangeLabel(null);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnNotificationInteractionListener) {
            mListener = (OnNotificationInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnInfoTabInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    void showEmptyText() {
        binding.emptyNotification.setText(utils.getLocalizedString(R.string.no_notificaion_available));
        binding.emptyNotification.setVisibility(View.VISIBLE);
    }

    void hideEmptyText() {
        binding.emptyNotification.setVisibility(View.GONE);
    }

    public void getNotifications() {
        hideEmptyText();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_UNREAD_NOTIFICATIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Utils.showLog("notification---->" + response);
                        hideProgressBar();
                        try {
                            JSONArray responseArray = new JSONArray(response);
                            JSONObject responseObject = responseArray.getJSONObject(0);
                            if (responseObject != null) {
                                NotificationModel notificationModel = new Gson().fromJson(responseObject.toString(), NotificationModel.class);
                                if (notificationModel != null && notificationModel.getError_code().equals("100")) {
                                    List<NotificationModel.NotificationData> notificationData = notificationModel.getNotificationDataList();
                                    mAdapter = new NotificationAdapter(getActivity(), notificationData);
                                    binding.rvNotification.setAdapter(mAdapter);
                                } else {
                                    showEmptyText();
                                }
                            } else {
                                showEmptyText();
                            }
                        } catch (Exception e) {
                            showEmptyText();
                            Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressBar();
                        Utils.showVolleyError(getActivity(), error);
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", userProfile.getUserId());
                params.put("user_type", userProfile.getUserType());
                params.put("api_token", userProfile.getApiToken());

                //todo update this token and delete else where (on server)
                showLog("Logot params " + params.toString());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);

    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            getNotifications();
            if (mListener != null)
                mListener.onChangeLabel(utils.getLocalizedString(R.string.notification).toString());
        }
    }

    public void showProgressBar() {
        binding.progressNotification.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        binding.progressNotification.setVisibility(View.GONE);
    }

    public interface OnNotificationInteractionListener {

        void onChangeLabel(String label);
    }
}
