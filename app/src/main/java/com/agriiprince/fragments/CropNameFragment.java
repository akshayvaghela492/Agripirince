package com.agriiprince.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.adapter.SortedStringSimpleAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.dataservice.CropDataService;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CropNameFragment.OnCropTypeInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CropNameFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CropNameFragment extends Fragment implements View.OnClickListener, TextWatcher,
        SortedStringSimpleAdapter.ItemClickListener {

    private static final String TAG = CropNameFragment.class.getName();

    private static final String ARGS_CROP_CLASS = "class";
    private static final String ARGS_CROP_LIST = "crops";

    private OnCropTypeInteractionListener mListener;

    private String mCropClass;

    private List<Crop> mCrops;
    private List<String> mCropNames;
    private List<String> mSearchList;

    private AppCompatImageView mBack;
    private TextView mClassLabel;

    private TextInputEditText mSearchBar;
    private TextView mCancelSearch;

    private SortedStringSimpleAdapter mAdapter;

    private RecyclerView mRecycler;

    private StringUtils utils;

    public CropNameFragment() {
        // Required empty public constructor
    }

    public static CropNameFragment newInstance(String cropClass) {
        CropNameFragment fragment = new CropNameFragment();
        Bundle args = new Bundle();

        args.putString(ARGS_CROP_CLASS, cropClass);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClickSimpleItem(int position) {
        String cropName = mAdapter.get(position);

        mListener.onSelectCropType(cropName);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCropClass = getArguments().getString(ARGS_CROP_CLASS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_crop_type, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        utils = AppController.getInstance().getStringUtils();
        mSearchList = new ArrayList<>();

        view.findViewById(R.id.crop_type_back).setOnClickListener(this);

        mSearchBar = view.findViewById(R.id.crop_type_search_edit);
        mSearchBar.setHint(utils.getLocalizedString(R.string.search_for_crops, true));
        mSearchBar.addTextChangedListener(this);

        mCancelSearch = view.findViewById(R.id.crop_type_cancel_search);
        mCancelSearch.setOnClickListener(this);

        mRecycler = view.findViewById(R.id.crop_type_recycler);

        mClassLabel = view.findViewById(R.id.crop_type_class_label);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAdapter = new SortedStringSimpleAdapter(this);

        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecycler.setAdapter(mAdapter);

        if (TextUtils.isEmpty(mCropClass)) {
            mClassLabel.setTextColor(Color.BLACK);
            mClassLabel.setText(utils.getLocalizedString(R.string.all));

        } else {
/*
            Log.d(TAG, mCropClass + CropClass.getClassColor(mCropClass));
            mClassLabel.setTextColor(getResources().getColor(CropClass.getClassColor(mCropClass)));
*/

            mCropClass = mCropClass.substring(0, 1).toUpperCase() + mCropClass.substring(1);
            mClassLabel.setText(mCropClass);
        }

        getCrops();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.crop_type_back:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.crop_type_cancel_search:
                mSearchBar.getText().clear();
                mCancelSearch.setVisibility(View.GONE);
                break;

        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() == 1) mCancelSearch.setVisibility(View.VISIBLE);
        else if (s.length() == 0) mCancelSearch.setVisibility(View.GONE);

        filterSearch(s.toString());

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCropTypeInteractionListener) {
            mListener = (OnCropTypeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnRssSubscribeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private String getSelectedCropClass() {
        return mCropClass;
    }

    private void filterSearch(String s) {
        mSearchList.clear();
        for (String value : mCropNames) {
            if (value.toLowerCase().contains(s.toLowerCase())) mSearchList.add(value);
        }

        mAdapter.replaceAll(mSearchList);
    }

    private List<Crop> filterForSelectedCropClass(List<Crop> crops, String cropClass) {
        List<Crop> filter = new ArrayList<>();
        for (Crop crop : crops) {
            if (crop.cropClass.toLowerCase().equalsIgnoreCase(cropClass.toLowerCase()))
                filter.add(crop);
        }

        return filter;
    }

    private void setAdapter() {
        CropNameFragment.this.mSearchList.addAll(mCropNames);
        mAdapter.replaceAll(mCropNames);
    }

    private void getCrops() {
        CropDataService cropDataService = new CropDataService(getContext());
        cropDataService.getCrops(new CropDataService.OnCropDataRequest() {
            @Override
            public void onCropDataResults(List<Crop> crops) {
                CropNameFragment.this.mCrops = filterForSelectedCropClass(crops, getSelectedCropClass());
                CropNameFragment.this.mCropNames = Crop.getBilingualNames(utils, mCrops);
                setAdapter();
            }

            @Override
            public void onCropDataError() {

            }
        });
    }

    public interface OnCropTypeInteractionListener {

        void onSelectCropType(String cropType);
    }
}
