package com.agriiprince.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.adapter.SortedStringSimpleAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.FragmentFarmerCaBinding;
import com.agriiprince.dataservice.CropDataService;
import com.agriiprince.dataservice.MandiDataService;
import com.agriiprince.helper.UserLocation;
import com.agriiprince.model.MandiModel;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.mvvm.model.crop.CropClass;
import com.agriiprince.utils.StringUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class FarmerCaFragment extends Fragment
        implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnCameraMoveStartedListener,
        View.OnClickListener, SortedStringSimpleAdapter.ItemClickListener {

    private static final String TAG = FarmerCaFragment.class.getName();

    private static final float MAP_ZOOM = 11f;

    private static final float MAX_DISTANCE = 500 * 1000; // max radius distance between user location and mandi

    private OnFarmerCaInteractionListener mListener;

    private PrefManager mPrefManager;

    private SupportMapFragment mMapFragment;

    private SortedStringSimpleAdapter mAdapter;

    private AtomicBoolean isMandiLoaded;
    private AtomicBoolean isCropLoaded;

    private List<CropClass> mCropClassList;
    private List<Crop> mCropsList;
    private List<MandiModel> mMandiesList;

    private List<String> mMandiCityNames;
    private List<String> mCropNames;

    private GoogleMap mGoogleMap;

    private CardView mSearchLayout;
    private CardView mMandiSearchLayout;

    private ImageView mSearchDropDown;

    private TextView mTvSearchMandi;

    private AppCompatImageView mImgBgAddCropClass;
    private AppCompatImageView mImgBgAddCropType;
    private CircleImageView mImgCropClass;
    private CircleImageView mImgCropType;
    private CircleImageView mImgCropClassRemove;
    private CircleImageView mImgCropTypeRemove;

    private AppCompatButton mSearchButton;

    private TextView mCropClassName;
    private TextView mCropTypeName;

    private TextInputEditText mSearchMandiInput;
    private RecyclerView mSearchMandiRecycler;

    private boolean isSearchOpen = true;
    private boolean isMandiSearchOpen = false;

    private String mUserCity;
    private LatLng mUserLatLng;

    private String mSelectedMandi;
    private String mSelectedMandiCity;
    private String mSelectedCropClass;
    private String mSelectedCropType;

    private StringUtils utils;

    private FragmentFarmerCaBinding binding;

    public FarmerCaFragment() {
        // Required empty public constructor
    }

    public static FarmerCaFragment newInstance() {
        FarmerCaFragment fragment = new FarmerCaFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClickSimpleItem(int position) {
        String mandiName = mAdapter.get(position);
        MandiModel mandiModel = MandiModel.getByMandiCity(getContext(), mMandiesList, mandiName);

        if (mandiModel != null) {
            mSelectedMandi = mandiModel.mandi_name_en;
            mSelectedMandiCity = mandiModel.mandi_city_en;
            mTvSearchMandi.setText(utils.getBilingualStringForApi(mandiModel.mandi_name_en, mandiModel.mandi_name_bi));
        }
        closeSearchMandi();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_farmer_ca, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = DataBindingUtil.bind(view);

        utils = AppController.getInstance().getStringUtils();
        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_farmer_ca);
        mMapFragment.getMapAsync(this);

        mPrefManager = new PrefManager(getContext());

        mAdapter = new SortedStringSimpleAdapter(this);

        mSearchMandiInput = view.findViewById(R.id.farmer_ca_search_mandi);
        mSearchMandiRecycler = view.findViewById(R.id.farmer_ca_search_mandi_recycler);

        mSearchLayout = view.findViewById(R.id.farmer_ca_search_layout);
        mMandiSearchLayout = view.findViewById(R.id.farmer_ca_search_mandi_layout);

        mSearchDropDown = view.findViewById(R.id.farmer_ca_search_arrow);

        mTvSearchMandi = view.findViewById(R.id.farmer_ca_search_mandi_et);

        mImgBgAddCropClass = view.findViewById(R.id.farmer_ca_add_crop_class);
        mImgBgAddCropType = view.findViewById(R.id.farmer_ca_add_crop_type);
        mImgCropClassRemove = view.findViewById(R.id.farmer_ca_crop_class_remove);
        mImgCropTypeRemove = view.findViewById(R.id.farmer_ca_crop_type_remove);

        mImgCropClass = view.findViewById(R.id.farmer_ca_crop_class_image);
        mImgCropType = view.findViewById(R.id.farmer_ca_crop_type_image);

        mCropClassName = view.findViewById(R.id.farmer_ca_crop_class_name);
        mCropTypeName = view.findViewById(R.id.farmer_ca_crop_type_name);

        mSearchButton = view.findViewById(R.id.farmer_ca_find_button);

        setTexts(view);
    }

    private void setTexts(View view) {
        ((TextView) view.findViewById(R.id.text_mandi)).setText(utils.getLocalizedString(R.string.mandi));
        ((TextView) view.findViewById(R.id.farmer_ca_search_mandi_et)).setHint(utils.getLocalizedString(R.string.search_for_mandi, true));
        ((TextView) view.findViewById(R.id.crop_class)).setText(utils.getLocalizedString(R.string.crop_category));
        ((TextView) view.findViewById(R.id.crop_type)).setText(utils.getLocalizedString(R.string.crop_name));
        ((AppCompatButton) view.findViewById(R.id.farmer_ca_find_button)).setText(utils.getLocalizedString(R.string.find));

        ((TextInputEditText) view.findViewById(R.id.farmer_ca_search_mandi)).setHint(utils.getLocalizedString(R.string.search_for_mandi, true));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListener.onChangeLabel(utils.getBilingualString(R.string.commission_agents_directory));

        isMandiLoaded = new AtomicBoolean(false);
        isCropLoaded = new AtomicBoolean(false);

        mUserCity = mPrefManager.getCity();

        mSearchDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSearchViewVisibility(isSearchOpen ? View.GONE : View.VISIBLE);
            }
        });

        mTvSearchMandi.setOnClickListener(this);

        mImgBgAddCropClass.setOnClickListener(this);
        mImgCropClassRemove.setOnClickListener(this);

        mImgBgAddCropType.setOnClickListener(this);
        mImgCropTypeRemove.setOnClickListener(this);

        mSearchButton.setOnClickListener(this);

        mSearchMandiRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mSearchMandiRecycler.setAdapter(mAdapter);

        mSearchMandiInput.addTextChangedListener(mandiSearchWatcher);

        new UserLocation(getActivity()).getLastKnowLocation(onUserLocationResults);

        getMandiList();

        getCropList();

        getCropClass();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mListener.onChangeLabel(null);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFarmerCaInteractionListener) {
            mListener = (OnFarmerCaInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFarmerCaInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.farmer_ca_add_crop_class:
                startCropClassFragment();
                break;
            case R.id.farmer_ca_add_crop_type:
                startCropTypeFragment();
                break;
            case R.id.farmer_ca_crop_class_remove:
                removeCropClass();
                break;
            case R.id.farmer_ca_crop_type_remove:
                removeCropType();
                break;
            case R.id.farmer_ca_search_mandi_et:
                openSearchMandi();
                break;
            case R.id.farmer_ca_find_button:
                startCaListFragment();
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng LatLngNewDelhi = new LatLng(28.7041, 77.1025);

        mGoogleMap = googleMap;
        try {
            if (getContext() != null)
                mGoogleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.map_style));
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.setOnMapClickListener(this);
        mGoogleMap.setOnCameraMoveStartedListener(this);

        mGoogleMap.setOnInfoWindowClickListener(onInfoWindowClickListener);


        if (mUserLatLng == null) {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLngNewDelhi, MAP_ZOOM));

        } else {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mUserLatLng, MAP_ZOOM));

        }
    }

    @Override
    public void onCameraMoveStarted(int i) {
        if (i == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            setSearchViewVisibility(View.GONE);

        } else if (i == GoogleMap.OnCameraMoveStartedListener.REASON_API_ANIMATION) {


        } else if (i == GoogleMap.OnCameraMoveStartedListener.REASON_DEVELOPER_ANIMATION) {

        }
    }


    @Override
    public void onMapClick(LatLng latLng) {
        setSearchViewVisibility(View.GONE);
    }

    private void startCropClassFragment() {
        if (isCropLoaded.get() && getActivity() != null) {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.farmer_ca_frame, CropClassFragment.newInstance())
                    .setCustomAnimations(R.anim.slide_up_out, R.anim.slide_up_in)
                    .addToBackStack(null)
                    .commit();
        }
    }

    private void startCropTypeFragment() {
        if (TextUtils.isEmpty(mSelectedCropClass)) {
            Toast.makeText(getContext(), utils.getLocalizedString(R.string.please_select_crop_class), Toast.LENGTH_SHORT).show();
            mImgBgAddCropClass.performClick();
        } else if (getActivity() != null) {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.farmer_ca_frame,
                            CropNameFragment.newInstance(mSelectedCropClass))
                    .setCustomAnimations(R.anim.slide_up_out, R.anim.slide_up_in)
                    .addToBackStack(null)
                    .commit();
        }
    }

    private void startCaListFragment() {
        if (validateSearch() && getActivity() != null) {
            Log.d(TAG, mSelectedMandi + "   " + mSelectedCropClass + "   " + mSelectedCropType);
            setSearchViewVisibility(View.GONE);

            String cropName = null;
            if (!TextUtils.isEmpty(mSelectedCropType))
                cropName = Crop.getCropByBilingualCropName(utils, mCropsList, mSelectedCropType).cropName;

            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(
                            R.id.layout_farmer_ca,
                            CaListFragment.newInstance(mSelectedMandi, mSelectedMandiCity, mSelectedCropClass, cropName),
                            CaListFragment.class.getSimpleName())
                    .addToBackStack(null)
                    .commit();
        }

    }

    private void plotMandiOnMap() {
        Log.d(TAG, "plotMarker");

        Location userLocation = new Location("user");
        userLocation.setLatitude(mUserLatLng.latitude);
        userLocation.setLongitude(mUserLatLng.longitude);

        Location mandiLocation = new Location("mandi");

        for (MandiModel model : mMandiesList) {

            try {

                mandiLocation.setLatitude(Double.parseDouble(model.mandi_lat));
                mandiLocation.setLongitude(Double.parseDouble(model.mandi_lng));

                if (getDistance(userLocation, mandiLocation) < MAX_DISTANCE) {
                    addMarker(model.mandi_name_en, model.mandi_state_en, model.mandi_district_en,
                            Double.parseDouble(model.mandi_lat), Double.parseDouble(model.mandi_lng));

                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
    }

    private float getDistance(Location location1, Location location2) {
        return location1.distanceTo(location2);
    }

    private void addMarker(String name, String state, String city, double lat, double lng) {
        mGoogleMap.addMarker(new MarkerOptions().snippet(state + ", " + city)
                .title(name)
                .position(new LatLng(lat, lng))
                .icon(getMarkerIcon()));
    }

    public BitmapDescriptor getMarkerIcon() {
        return BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pointer);
    }

    private void setSearchViewVisibility(int visibility) {
        if (isMandiSearchOpen) {
            closeSearchMandi();
            return;
        }

        if (visibility == View.GONE || visibility == View.INVISIBLE) {
            isSearchOpen = false;
            mSearchLayout.setVisibility(View.GONE);
            mSearchDropDown.setImageResource(R.drawable.ic_keyboard_arrow_down);


        } else if (visibility == View.VISIBLE) {
            isSearchOpen = true;
            mSearchLayout.setVisibility(View.VISIBLE);
            mSearchDropDown.setImageResource(R.drawable.ic_keyboard_arrow_up);

            CaListFragment fragment = (CaListFragment) getActivity().getSupportFragmentManager()
                    .findFragmentByTag(CaListFragment.class.getSimpleName());

            if (fragment != null) getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    private boolean validateSearch() {
        if (TextUtils.isEmpty(mSelectedMandi)) {
            Toast.makeText(getContext(), utils.getLocalizedString(R.string.please_select_mandi), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public void setCropClass(String name) {
        Log.d(TAG, name);

        mSelectedCropClass = name;

        if (getActivity() != null)
            getActivity().getSupportFragmentManager().popBackStack();

        mCropClassName.setText(name);

        binding.farmerCaAddCropClass.setVisibility(View.GONE);
        mImgCropClassRemove.setVisibility(View.VISIBLE);
        mImgBgAddCropClass.setClickable(false);

        for (CropClass cropClass : mCropClassList) {
            if (cropClass.getCrop_class().equalsIgnoreCase(name)) {
                mCropClassName.setText(utils.getBilingualString(cropClass.getCrop_class(), cropClass.getCrop_class_bi()));
                break;
            }
        }

        String url = CropClass.getImageUrl(name);
        Glide.with(mImgCropClass)
                .setDefaultRequestOptions(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.ap_logo_final))
                .load(url)
                .into(mImgCropClass);
    }

    private void removeCropClass() {
        Log.d(TAG, "remove class");

        mSelectedCropClass = null;

        mImgCropClass.setImageResource(0);
        mImgBgAddCropClass.setImageResource(0);

        binding.farmerCaAddCropClass.setVisibility(View.VISIBLE);
        mImgCropClassRemove.setVisibility(View.GONE);
        mImgBgAddCropClass.setClickable(true);

        mCropClassName.setText("");

        removeCropType();
    }

    public void setCropType(String cropName) {
        Log.d(TAG, cropName);

        hideSoftInputMethod();

        mSelectedCropType = cropName;

        mCropTypeName.setText(cropName);

        if (getActivity() != null)
            getActivity().getSupportFragmentManager().popBackStack();

        binding.farmerCaAddCropType.setVisibility(View.GONE);
        mImgCropTypeRemove.setVisibility(View.VISIBLE);
        mImgBgAddCropType.setClickable(false);

        for (Crop crop : mCropsList) {
            if (crop.cropName.equalsIgnoreCase(cropName)) {

                mCropTypeName.setText(utils.getBilingualString(crop.cropName, crop.cropName_bi));

                Glide.with(mImgCropType)
                        .setDefaultRequestOptions(new RequestOptions()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .placeholder(R.drawable.ap_logo_final))
                        .load(crop.getImageUrl())
                        .into(mImgCropType);

                break;
            }
        }
    }

    private void removeCropType() {
        Log.d(TAG, "remove class");

        mSelectedCropType = null;

        mImgCropType.setImageResource(0);
        mImgBgAddCropType.setImageResource(0);

        binding.farmerCaAddCropType.setVisibility(View.VISIBLE);
        mImgCropTypeRemove.setVisibility(View.GONE);
        mImgBgAddCropType.setClickable(true);

        mCropTypeName.setText("");
    }


    private void openSearchMandi() {
        mMandiSearchLayout.setVisibility(View.VISIBLE);
        mListener.onSearchMandiOpen();
        isMandiSearchOpen = true;

        if (getActivity() != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(mSearchMandiInput, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public void closeSearchMandi() {
        mMandiSearchLayout.setVisibility(View.GONE);
        mListener.onSearchMandiClose();
        isMandiSearchOpen = false;
        if (mSearchMandiInput.getText() != null) mSearchMandiInput.getText().clear();

        hideSoftInputMethod();
    }

    private void getCropClass() {
        mCropClassList = CropClass.getCropClassList(getContext());
    }

    private void getCropList() {
        CropDataService cropDataService = new CropDataService(getContext());
        cropDataService.getCrops(new CropDataService.OnCropDataRequest() {
            @Override
            public void onCropDataResults(List<Crop> crops) {
                mCropsList = crops;

                mCropNames = Crop.getNames(crops);
                Collections.sort(mCropNames);

                isCropLoaded.set(true);
            }

            @Override
            public void onCropDataError() {
                mCropsList = null;
                Toast.makeText(getContext(), "Could not retrieve data, try again", Toast.LENGTH_SHORT).show();
                //getActivity().getSupportFragmentManager().popBackStack();
            }
        });
    }

    private void getMandiList() {
        MandiDataService mandiDataService = new MandiDataService(getContext());
        mandiDataService.getMandis(new MandiDataService.OnMandiResponse() {
            @Override
            public void onMandiSuccess(List<MandiModel> mandies) {
                mMandiesList = mandies;
                if (getContext() != null) {
                    mMandiCityNames = MandiModel.getMandiCityNames(getContext(), mMandiesList);

                    mAdapter.addAll(mMandiCityNames);
                    isMandiLoaded.set(true);

                    if (mUserLatLng != null && mGoogleMap != null) plotMandiOnMap();
                }
            }

            @Override
            public void onMandiFailure() {
                mMandiesList = null;

                if (getContext() != null) {
                    Toast.makeText(getContext(), "Could not retrieve data, try again", Toast.LENGTH_SHORT).show();
                    //getActivity().getSupportFragmentManager().popBackStack();
                }

            }
        });
    }

    private TextWatcher mandiSearchWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            Log.d(TAG, s.toString());
            mAdapter.getFilter().filter(s);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private GoogleMap.OnInfoWindowClickListener onInfoWindowClickListener = new GoogleMap.OnInfoWindowClickListener() {
        @Override
        public void onInfoWindowClick(Marker marker) {
            MandiModel mandiModel = MandiModel.getByMandiName(mMandiesList, marker.getTitle());
            if (mandiModel != null) {
                mSelectedMandi = mandiModel.mandi_name_en;
                mSelectedMandiCity = mandiModel.mandi_city_combi_en;
                mTvSearchMandi.setText("");
                removeCropClass();
                removeCropType();
                startCaListFragment();
            }
        }
    };


    private UserLocation.OnUserLocationResults onUserLocationResults = new UserLocation.OnUserLocationResults() {
        @Override
        public void onLocationResult(Location lastLocation) {
            FarmerCaFragment.this.mUserLatLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());

            if (mGoogleMap != null) {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mUserLatLng, MAP_ZOOM));

                if (mMandiesList != null) plotMandiOnMap();
            }
        }

        @Override
        public void onLocationFail() {
            try {
                Geocoder geocoder = new Geocoder(getContext(), Locale.ENGLISH);
                List<Address> addresses = geocoder.getFromLocationName(mUserCity, 5);
                if (addresses.size() > 0) {
                    Address address = addresses.get(0);
                    mUserLatLng = new LatLng(address.getLatitude(), address.getLongitude());
                    if (mGoogleMap != null) {
                        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mUserLatLng, MAP_ZOOM));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                mUserLatLng = null;
            }
        }
    };

    private void hideSoftInputMethod() {
        if (getActivity() != null && getActivity().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);

            if (inputMethodManager != null)
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }


    public interface OnFarmerCaInteractionListener {

        void onChangeLabel(String label);

        void onSearchMandiOpen();

        void onSearchMandiClose();

        void showProgress();

        void dismissProgress();
    }
}
