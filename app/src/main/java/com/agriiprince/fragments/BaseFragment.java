package com.agriiprince.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.agriiprince.R;

/**
 * Created by Manu Sajjan on 19-12-2018.
 */
public class BaseFragment extends Fragment {

    private ProgressDialog mProgressDialog;

    /**
     * hide the soft input layout (keyboard)
     *
     * @param view pass the view which has the focus
     */
    protected void hideSoftInputLayout(View view) {
        if (getContext() == null) return;
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null)
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected void showSnackBar(@StringRes int id) {
        if (getContext() != null) showSnackBar(getContext().getResources().getString(id));
    }

    protected void showSnackBar(String message) {
        if (getActivity() != null)
            showSnackBar(getActivity().findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT);
    }

    protected void showSnackBar(String message, int duration) {
        if (getActivity() != null)
            showSnackBar(getActivity().findViewById(android.R.id.content), message, duration);
    }

    protected void showSnackBar(View parentLayout, String message, int duration) {
        Snackbar.make(parentLayout, message, duration).show();
    }


    protected void showProgressDialog() {
        if (getActivity() == null) return;
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setMessage(getString(R.string.please_wait));
        }

        if (!mProgressDialog.isShowing()) {
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
    }

    protected void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }
}
