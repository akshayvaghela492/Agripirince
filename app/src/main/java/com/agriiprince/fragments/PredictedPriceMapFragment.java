package com.agriiprince.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PredictedPriceMapFragment extends Fragment {


    public PredictedPriceMapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_predicted_price_map, container, false);
    }

}
