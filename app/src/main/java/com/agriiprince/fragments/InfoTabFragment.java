package com.agriiprince.fragments;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.db.DatabaseManager;
import com.agriiprince.R;
import com.agriiprince.mvvm.ui.registration.FarmerProfileActivity;
import com.agriiprince.mvvm.ui.weather.WeatherActivityFinal;
import com.agriiprince.mvvm.ui.homescreen.FarmerHomeScreenActivity;
import com.agriiprince.activities.checkprice.CheckPricingActivity;
import com.agriiprince.activities.farmer.rss.RssMainActivity;
import com.agriiprince.adapter.farmer.FarmerInfoAdapter;
import com.agriiprince.mvvm.applevel.analytics.FarmerRealtimeDBAnalytics;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.chat.ChatContainerActivity;
import com.agriiprince.databinding.FragmentInfoTabBinding;
import com.agriiprince.dataservice.CropDataService;
import com.agriiprince.dataservice.DataResponse;
import com.agriiprince.dataservice.FarmerInfoTabPriceDataService;
import com.agriiprince.dataservice.MandiDataService;
import com.agriiprince.dataservice.NotificationDataService;
import com.agriiprince.mvvm.data.repository.WeatherDataService;
import com.agriiprince.db.InfoTabPricesDao;
import com.agriiprince.mvvm.ui.customwidgets.DatePickerDialog;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.MandiModel;
import com.agriiprince.mvvm.model.WeatherModel;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.models.FarmerInfoTabModel;
import com.agriiprince.models.HistoricPriceModel;
import com.agriiprince.models.NotificationModel;
import com.agriiprince.utils.DateUtil;
import com.agriiprince.utils.StringUtils;
import com.android.volley.VolleyError;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.agriiprince.activities.checkprice.CheckPricingActivity.KEY_CROP_GRADE;
import static com.agriiprince.activities.checkprice.CheckPricingActivity.KEY_CROP_NAME;
import static com.agriiprince.activities.checkprice.CheckPricingActivity.KEY_CROP_VARIETY;
import static com.agriiprince.activities.checkprice.CheckPricingActivity.KEY_TIME_MILLI;
import static com.agriiprince.mvvm.applevel.analytics.FarmerFirebaseAnalytics.logEventFarmerClicked;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnInfoTabInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InfoTabFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InfoTabFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = InfoTabFragment.class.getSimpleName();

    private static final int MESSAGE_CROP_ID_MATCHED = 0;
    private static final int MESSAGE_MANDI_ID_MATCHED = 1;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    private OnInfoTabInteractionListener mListener;

    private UserProfile mUserProfile;
    DatabaseReference mFarmersRef;
    DatabaseReference mNumRef;
    String mUserId;
    String mMobile;
    String mName;
    String mCity;
    String mPin;
    Context mContext;

    private PrefManager prefManager;
    private FarmerInfoAdapter mAdapter;

    private List<FarmerInfoTabModel> farmerInfoTabModels;
    List<MandiModel> mMandis;
    private List<Crop> mCrops;

    private List<Crop> mInterestedCrops;
    private List<MandiModel> mInterestedMandis;

    private AtomicBoolean isCropLoaded;
    private AtomicBoolean isMandiLoaded;

    private ProgressBar progressBar;

    private LinearLayout weatherStatusLayout;

    private RelativeLayout weatherInfoLayout;

    private NestedScrollView nestedScrollView;

    private FrameLayout notificationIcon;
    private FrameLayout newsIcon;
    private FrameLayout chatIcon;

    private TextView tvWeatherStatus;

    private TextView tvDate;
    private TextView tvMinTemp;
    private TextView tvMaxTemp;
    private TextView tvSatus;

    private ImageView imgWeather, notificationicon;
    private CardView weather_card;

    private StringUtils utils;
    private boolean firstTime = true;

    private Handler uiHandler;

    private Handler handler;

    private FarmerInfoTabPriceDataService farmerInfoTabPriceDataService;

    private FragmentInfoTabBinding binding;

    public InfoTabFragment() {
        // Required empty public constructor
    }

    public static InfoTabFragment newInstance(String param1, String param2) {
        InfoTabFragment fragment = new InfoTabFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        farmerInfoTabModels = new ArrayList<>();
        DatabaseManager databaseManager = AppController.getInstance().getDataBase();
        utils = AppController.getInstance().getStringUtils();
        mUserProfile = new UserProfile(getContext());
        prefManager = new PrefManager(getContext());
        mInterestedCrops = new ArrayList<>();
        mInterestedMandis = new ArrayList<>();

        mFarmersRef = ((FarmerHomeScreenActivity)getActivity()).getmFarmersRef();
        mNumRef = ((FarmerHomeScreenActivity)getActivity()).getmNumRef();
        mUserId = mUserProfile.getUserId();
        mMobile = mUserProfile.getUserMobile();
        mName = mUserProfile.getUserName();
        mCity = mUserProfile.getUserCity();
        mPin = mUserProfile.getUserPincode();
        mContext = getContext();

        isCropLoaded = new AtomicBoolean(false);
        isMandiLoaded = new AtomicBoolean(false);

        InfoTabPricesDao infoTabPricesDao = new InfoTabPricesDao(databaseManager);
        farmerInfoTabPriceDataService = new FarmerInfoTabPriceDataService(mUserProfile,
                infoTabPricesDao, onFarmerInfoTabPriceDataService);

        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();

        uiHandler = new Handler(Looper.getMainLooper(), uiHandlerCallback);
        handler = new Handler(handlerThread.getLooper());

        return inflater.inflate(R.layout.fragment_info_tab, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = DataBindingUtil.bind(view);

        progressBar = view.findViewById(R.id.farmer_landing_weather_progress);

        weatherStatusLayout = view.findViewById(R.id.farmer_landing_weather_status_layout);
        weatherInfoLayout = view.findViewById(R.id.farmer_landing_weather_info_layout);
        weather_card = view.findViewById(R.id.weather_card);
        tvWeatherStatus = view.findViewById(R.id.farmer_landing_weather_status);

        tvDate = view.findViewById(R.id.tv_farmer_landing_weather_date);
        tvMinTemp = view.findViewById(R.id.tv_farmer_landing_min_temp);
        tvMaxTemp = view.findViewById(R.id.tv_farmer_landing_max_temp);
        tvSatus = view.findViewById(R.id.tv_farmer_landing_weather_status);

        imgWeather = view.findViewById(R.id.img_farmer_landing_weather_icon);
        notificationicon = view.findViewById(R.id.notificationicon);

        notificationIcon = view.findViewById(R.id.farmer_home_notification_icon);
        newsIcon = view.findViewById(R.id.farmer_home_news_icon);
        chatIcon = view.findViewById(R.id.farmer_home_chat_icon);

        notificationIcon.setOnClickListener(this);
        newsIcon.setOnClickListener(this);
        chatIcon.setOnClickListener(this);
        weather_card.setOnClickListener(this);

        nestedScrollView = view.findViewById(R.id.farmer_landing_scroll_view);
        final RecyclerView recyclerView = view.findViewById(R.id.farmer_landing_recycler);

        ViewCompat.setNestedScrollingEnabled(recyclerView, false); // to support api below 21 use this instead in XML

        mAdapter = new FarmerInfoAdapter(getContext(), onInfoItemClickListener, firstTime);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        NotificationDataService notificationDataService = new NotificationDataService(notificationModelDataResponse);
        notificationDataService.getNotifications(mUserProfile.getUserId(), mUserProfile.getUserType(), mUserProfile.getApiToken());
        WeatherDataService weatherDataService = new WeatherDataService(getActivity(), weatherCallback);

        getCrops();
        getMandis();
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            mListener.onChangeLabel(utils.getLocalizedString(R.string.information_tab).toString());
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_info_tab, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refreshInfoTab();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private Handler.Callback uiHandlerCallback = new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            Log.d(TAG, "uiHandler >>>> handleMessage");
            switch (msg.what) {
                case MESSAGE_CROP_ID_MATCHED:
                    getInfoTabPrices();
                    return true;
                case MESSAGE_MANDI_ID_MATCHED:
                    getInfoTabPrices();
                    return  true;
            }
            return false;
        }
    };

    private void refreshInfoTab() {
        Log.d(TAG, "refreshInfoTab");
        Log.d("TESTING", "refreshInfoTab");
        if (mInterestedMandis.size() > 0 && mInterestedCrops.size() > 0) {

            List<String> cropNames = Crop.getCropNames(mInterestedCrops);
            List<String> mandiNames = MandiModel.getMandiNames(mInterestedMandis);

            farmerInfoTabPriceDataService.refreshData(cropNames,
                    mandiNames, Calendar.getInstance().getTimeInMillis());
        }

        Toast.makeText(getContext(), "Successfully updated.",
                Toast.LENGTH_SHORT).show();
    }

    private void getInfoTabPrices() {
        Log.d(TAG, "getInfoTabPrices");
        Log.d("TESTING", "get infotab price");
        if (mInterestedMandis.size() > 0 && mInterestedCrops.size() > 0) {
            List<String> cropNames = Crop.getCropNames(mInterestedCrops);
            List<String> mandiNames = MandiModel.getMandiNames(mInterestedMandis);
            Log.d("TESTING", "mandi and crop size > 0");
            farmerInfoTabPriceDataService.getHistoricPrices(cropNames,
                    mandiNames, Calendar.getInstance().getTimeInMillis());
        }
    }

    private void getMandis() {
        MandiDataService mandiDataService = new MandiDataService(getContext());
        mandiDataService.getMandis(new MandiDataService.OnMandiResponse() {
            @Override
            public void onMandiSuccess(List<MandiModel> mandies) {
                Log.d(TAG, "onMandiSuccess: " + mandies.size());
                InfoTabFragment.this.mMandis = mandies;
                isMandiLoaded.set(true);
                if (isCropLoaded.get() && isMandiLoaded.get()) {
                    getInterestedLists();
                }
            }

            @Override
            public void onMandiFailure() {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnInfoTabInteractionListener) {
            mListener = (OnInfoTabInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnInfoTabInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.farmer_home_notification_icon) {

            Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                dataSnapshot, "click_infotab_notification", mNumRef);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });

            logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_infotab_notification");

            mListener.onSelectNotification();
        } else if (v.getId() == R.id.farmer_home_news_icon) {

            Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                dataSnapshot, "click_infotab_news", mNumRef);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });

            logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_infotab_news");

            startActivity(new Intent(getContext(), RssMainActivity.class));
        } else if (v.getId() == R.id.farmer_home_chat_icon) {

            Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                dataSnapshot, "click_infotab_chat", mNumRef);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });

            logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_infotab_chat");

            if (getActivity() != null)
                getActivity().startActivity(new Intent(getContext(), ChatContainerActivity.class));
        } else if (v.getId() == R.id.weather_card) {

            Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                dataSnapshot, "click_infotab_weather", mNumRef);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });

            logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_infotab_weather");

            startActivity(new Intent(getActivity(), WeatherActivityFinal.class));
        }
    }


    private void getCrops() {
        CropDataService cropDataService = new CropDataService(getContext());
        cropDataService.getCrops(new CropDataService.OnCropDataRequest() {
            @Override
            public void onCropDataResults(List<Crop> crops) {
                InfoTabFragment.this.mCrops = crops;
                isCropLoaded.set(true);

                if (isCropLoaded.get() && isMandiLoaded.get()) {
                    getInterestedLists();
                }
            }

            @Override
            public void onCropDataError() {

            }
        });
    }

    private void getInterestedLists() {
        Log.d(TAG, "getInterestedLists: ");
        final String[] cropsList = mUserProfile.getInterestedCropIds()
                .replace("[", "").replace("]", "").split(",");
        final String[] mandiIds = mUserProfile.getInterestedMandiIds()
                .replace("[", "").replace("]", "").split(",");

        findCropNameByCropId(cropsList);
        findMandiNameByMandiId(mandiIds);
    }

    private void findCropNameByCropId(final String[] cropIds) {
        Runnable cropNameRunnable = new Runnable() {
            @Override
            public void run() {
                List<String> ids = new ArrayList<>();
                for (String id : cropIds) {
                    ids.add(id.trim());
                }
                mInterestedCrops = Crop.getCropsByIds(mCrops, ids);
                Log.d(TAG, "findCropNameByCropId:   " + ids.toString());

                uiHandler.sendEmptyMessage(MESSAGE_CROP_ID_MATCHED);
            }
        };
        handler.post(cropNameRunnable);
    }

    private void findMandiNameByMandiId(final String[] mandiIds) {
        Runnable mandiNameRunnable = new Runnable() {
            @Override
            public void run() {
                List<String> ids = new ArrayList<>();
                for (String id : mandiIds) {
                    ids.add(id.trim());
                }
                mInterestedMandis = MandiModel.getMandisByIds(mMandis, ids);
                Log.d(TAG, "findMandiNameByMandiId:     " + ids.toString());

                uiHandler.sendEmptyMessage(MESSAGE_MANDI_ID_MATCHED);
            }
        };
        handler.post(mandiNameRunnable);
    }

    private void populateInfoTabRecycler() {
        mAdapter.removeAll();
        long time = Calendar.getInstance().getTimeInMillis();
        for (Crop crop : mInterestedCrops) {
            FarmerInfoTabModel farmerInfoTabModel = new FarmerInfoTabModel();
            farmerInfoTabModel.crop = crop;
            farmerInfoTabModel.mandis = mInterestedMandis;
            farmerInfoTabModel.selectedTime = time;

            mAdapter.add(farmerInfoTabModel);

            getHistoricData(farmerInfoTabModel);
        }
    }

    private void getHistoricData(FarmerInfoTabModel farmerInfoTabModel) {
        farmerInfoTabPriceDataService.getHistoricPricesForCrop(
                farmerInfoTabModel.crop.cropName,
                farmerInfoTabModel.crop.getVariety(farmerInfoTabModel.selectedVariety).varietyName,
                farmerInfoTabModel.crop.getVariety(farmerInfoTabModel.selectedVariety)
                        .getGrade(farmerInfoTabModel.selectedGrade).gradeName,
                farmerInfoTabModel.selectedTime
        );
    }

    private void showInfoTabDatePicker(final int position) {
        Calendar selectedCalender = Calendar.getInstance();
        selectedCalender.setTimeInMillis(mAdapter.get(position).selectedTime);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new android.app.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        FarmerInfoTabModel farmerInfoTabModel = mAdapter.get(position);
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, month, dayOfMonth);
                        farmerInfoTabModel.selectedTime = calendar.getTimeInMillis();
                        mAdapter.notifyItemChanged(position, farmerInfoTabModel);

                        getHistoricData(farmerInfoTabModel);
                    }
                }, selectedCalender);
        DatePicker datePicker = datePickerDialog.getDatePicker();
        datePicker.setMaxDate(Calendar.getInstance().getTimeInMillis());

        datePickerDialog.show();
    }

    private FarmerInfoAdapter.OnInfoItemClickListener onInfoItemClickListener = new FarmerInfoAdapter.OnInfoItemClickListener() {
        @Override
        public void onChangeVariety(int recyclerPosition, int varietyPosition) {
            FarmerInfoTabModel farmerInfoTabModel = mAdapter.get(recyclerPosition);
            if (farmerInfoTabModel.selectedVariety != varietyPosition) {
                farmerInfoTabModel.selectedVariety = varietyPosition;
                farmerInfoTabModel.selectedGrade = 0;
                mAdapter.notifyItemChanged(recyclerPosition, farmerInfoTabModel);

                getHistoricData(farmerInfoTabModel);

            }
        }

        @Override
        public void onChangeGrade(int recyclerPosition, int gradePosition) {
            FarmerInfoTabModel farmerInfoTabModel = mAdapter.get(recyclerPosition);
            if (farmerInfoTabModel.selectedGrade != gradePosition) {
                farmerInfoTabModel.selectedGrade = gradePosition;
                mAdapter.notifyItemChanged(recyclerPosition, farmerInfoTabModel);

                getHistoricData(farmerInfoTabModel);

            }
        }

        @Override
        public void onClickNextDay(int position) {
            FarmerInfoTabModel farmerInfoTabModel = mAdapter.get(position);
            Calendar time = Calendar.getInstance();
            time.setTimeInMillis(farmerInfoTabModel.selectedTime);
            time.add(Calendar.DATE, 1);

            if (DateUtil.isTimeBeforeToday(time)) {
                farmerInfoTabModel.selectedTime = time.getTimeInMillis();
                mAdapter.notifyItemChanged(position, farmerInfoTabModel);

                getHistoricData(farmerInfoTabModel);

            }
        }

        @Override
        public void onClickPrevDay(int position) {
            FarmerInfoTabModel farmerInfoTabModel = mAdapter.get(position);

            Calendar time = Calendar.getInstance();
            time.setTimeInMillis(farmerInfoTabModel.selectedTime);
            time.add(Calendar.DATE, -1);

            farmerInfoTabModel.selectedTime = time.getTimeInMillis();
            mAdapter.notifyItemChanged(position, farmerInfoTabModel);

            getHistoricData(farmerInfoTabModel);

        }

        @Override
        public void onClickCalender(int position) {
            showInfoTabDatePicker(position);
        }

        @Override
        public void onClickMap(int position) {
            FarmerInfoTabModel farmerInfoTabModel = mAdapter.get(position);
            String variety = farmerInfoTabModel.crop.getVariety(farmerInfoTabModel.selectedVariety).varietyName;
            String grade = farmerInfoTabModel.crop.getVariety(farmerInfoTabModel.selectedVariety)
                    .getGrade(farmerInfoTabModel.selectedGrade).gradeName;

            Intent intent = new Intent(getContext(), CheckPricingActivity.class);
            intent.putExtra(KEY_TIME_MILLI, farmerInfoTabModel.selectedTime);
            intent.putExtra(KEY_CROP_NAME, farmerInfoTabModel.crop.cropName.trim());
            intent.putExtra(KEY_CROP_VARIETY, variety);
            intent.putExtra(KEY_CROP_GRADE, grade);
            startActivity(intent);
        }

        @Override
        public void onClickProfile(int position) {
            startActivity(new Intent(getActivity(), FarmerProfileActivity.class));
        }
    };

    private FarmerInfoTabPriceDataService.OnFarmerInfoTabPriceDataService onFarmerInfoTabPriceDataService =
            new FarmerInfoTabPriceDataService.OnFarmerInfoTabPriceDataService() {

                @Override
                public void onRefresh() {
                    Log.d(TAG, "infoTabDataService: onRefresh");
                    populateInfoTabRecycler();
                }

                @Override
                public void onGetHistoricPrices(List<HistoricPriceModel> list) {
                    Log.d(TAG, "infoTabDataService: " + list.size());
                    if (list.size() > 0) {
                        HistoricPriceModel model = list.get(0);
                        for (int i = 0; i < mAdapter.getItemCount(); i++) {
                            FarmerInfoTabModel farmerInfoTabModel = mAdapter.get(i);
                            if (farmerInfoTabModel.crop.cropName.equalsIgnoreCase(model.getCrop())) {
                                farmerInfoTabModel.historicPriceModels = list;
                                mAdapter.notifyItemChanged(i, farmerInfoTabModel);
                                Log.d(TAG, "infoTabDataService: " + farmerInfoTabModel.crop.cropName);
                                break;
                            }
                        }
                    }
                }

                @Override
                public void onResultError() {

                }
            };

    private DataResponse<NotificationModel> notificationModelDataResponse = new DataResponse<NotificationModel>() {
        @Override
        public void onSuccessResponse(NotificationModel response) {
            if (getContext() != null && response != null && response.getError_code().equals("100")) {
                if (response.getNotificationDataList().size() > 0) {
                    DrawableCompat.setTint(notificationicon.getDrawable(), ContextCompat.getColor(getContext(), R.color.rangeRed));
                } else
                    DrawableCompat.setTint(notificationicon.getDrawable(), ContextCompat.getColor(getContext(), R.color.colorPrimaryFarmer));
            }
        }

        @Override
        public void onParseError() {

        }

        @Override
        public void onErrorResponse(VolleyError error) {

        }
    };

    private WeatherDataService.OnWeatherCallback weatherCallback = new WeatherDataService.OnWeatherCallback() {
        @Override
        public void onWeatherUpdate(List<WeatherModel> weatherModelList) {

        }

        @Override
        public void onDayWeatherUpdate(WeatherModel weatherModel) {
            tvDate.setText(weatherModel.getDate());

            double maxTemp = (5.0 / 9.0) * (weatherModel.getTempMax() - 32);
            double minTemp = (5.0 / 9.0) * (weatherModel.getTempMin() - 32);

            String maxTemperature = String.format(Locale.getDefault(), "%.1f", maxTemp);
            String minTemperature = String.format(Locale.getDefault(), "%.1f", minTemp);

            tvMinTemp.setText(minTemperature);
            tvMaxTemp.setText(maxTemperature);

            int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);

            // check for day or night
            if (hour >= 6 && hour <= 18) {
                tvSatus.setText(weatherModel.getDayCondition());
                imgWeather.setImageResource(weatherModel.getDayIcon());
            } else {
                tvSatus.setText(weatherModel.getNightCondition());
                imgWeather.setImageResource(weatherModel.getNightIcon());
            }

            progressBar.setVisibility(View.GONE);
            weatherStatusLayout.setVisibility(View.GONE);
            weatherInfoLayout.setVisibility(View.VISIBLE);

        }

        @Override
        public void onWeatherUpdateFail() {
            tvWeatherStatus.setText(utils.getLocalizedString(R.string.weather_not_available));

            progressBar.setVisibility(View.GONE);
            weatherInfoLayout.setVisibility(View.INVISIBLE);
            weatherStatusLayout.setVisibility(View.VISIBLE);
        }
    };

    public interface OnInfoTabInteractionListener {
        void onChangeLabel(String label);

        void onSelectNotification();
    }
}
