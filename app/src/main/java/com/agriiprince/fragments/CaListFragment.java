package com.agriiprince.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.adapter.CAListAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.ca.CAlist;
import com.agriiprince.mvvm.retrofit.model.ca.CaListModel;
import com.agriiprince.mvvm.retrofit.service.CA;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CALL_PHONE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CaListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CaListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CaListFragment extends Fragment implements CAListAdapter.OnClickListener, View.OnClickListener {

    private static final String ARG_MANDI_NAME = "mandi";
    private static final String ARG_MANDI_CITY = "city";
    private static final String ARG_CROP_CLASS = "class";
    private static final String ARG_CROP_NAME = "name";

    private OnFragmentInteractionListener mListener;

    private UserProfile mUserProfile;
    private StringUtils mStringUtils;

    private CAListAdapter mCAListAdapter;

    private String mMandiName;
    private String mMandiCity;
    private String mCropClass;
    private String mCropName;

    private TextView mTvMandi;

    private TextView mEmptyText;


    public static CaListFragment newInstance(String mandiName, String mandi_city, String cropCalss, String cropName) {
        CaListFragment fragment = new CaListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_MANDI_NAME, mandiName);
        args.putString(ARG_MANDI_CITY, mandi_city);
        args.putString(ARG_CROP_CLASS, cropCalss);
        args.putString(ARG_CROP_NAME, cropName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClickCall(CaListModel caItem, int index) {
        Intent i = new Intent();
        i.setAction(Intent.ACTION_CALL);
        i.setData(Uri.parse("tel:" + caItem.getCaContact().split(",")[index]));
        if (getContext() != null && ActivityCompat.checkSelfPermission(getContext(), CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            if (mListener != null)
                mListener.requestPermission(CALL_PHONE);
        } else {
            startActivity(i);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mMandiName = getArguments().getString(ARG_MANDI_NAME);
            mMandiCity = getArguments().getString(ARG_MANDI_CITY);
            mCropClass = getArguments().getString(ARG_CROP_CLASS);
            mCropName = getArguments().getString(ARG_CROP_NAME);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mUserProfile = new UserProfile(getContext());
        mStringUtils = AppController.getInstance().getStringUtils();
        return inflater.inflate(R.layout.fragment_ca_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.ca_list_back).setOnClickListener(this);

        mTvMandi = view.findViewById(R.id.ca_list_mandi_name);
        mEmptyText = view.findViewById(R.id.empty_text);

        mCAListAdapter = new CAListAdapter(getContext());
        mCAListAdapter.setOnItemClickListener(this);

        RecyclerView recyclerView = view.findViewById(R.id.ca_list_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mCAListAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
/*

        if (! TextUtils.isEmpty(mCropClass))
            mCropClass = CropClass.getParamString(mCropClass);
*/

        mTvMandi.setText(mMandiName);

        getCAList();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ca_list_back:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnRssSubscribeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void setEmptyText() {
        mEmptyText.setText(mStringUtils.getLocalizedString(R.string.no_commission_agents_found));
        mEmptyText.setVisibility(View.VISIBLE);
    }

    private void getCAList() {
        if (mListener != null)
            mListener.showProgress();

/*        final String caListURL = Config.GET_CA_LIST_BI;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, caListURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (mListener != null)
                            mListener.dismissProgress();
                        Log.d("TESTING_VOLLEY", response);

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            //JSONArray array = new JSONArray(response);
                            //JSONObject object = array.getJSONObject(0);
                            //int error = object.getInt("error_code");
                            int error = jsonObject.getInt("code");
                            //if (error == 100) {
                            if (error == 200) {
                                JSONObject dataobj=jsonObject.getJSONObject("data");
                                JSONArray dataArray=dataobj.getJSONArray("data");
                                List<CaListModel> caList = new Gson().fromJson(String.valueOf(dataArray),
                                        new TypeToken<List<CaListModel>>() {
                                        }.getType());

                                if (caList == null || caList.size() == 0) {
                                    setEmptyText();
                                } else {
                                    mEmptyText.setVisibility(View.GONE);
                                    mCAListAdapter.setData(caList);
                                }
                            } else {
                                setEmptyText();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mListener != null)
                    mListener.dismissProgress();
                GeneralUtils.showVolleyError(getContext(), error);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());

                PrefManager prefManager = new PrefManager(getContext());
                String primaryLocale = prefManager.getLocale();
                String secondaryLocale = prefManager.getSecondaryLocale();

                if (TextUtils.isEmpty(secondaryLocale)) {
                    secondaryLocale = primaryLocale.equals("en") ? "hi" : primaryLocale;
                } else {
                    secondaryLocale = primaryLocale.equals("en") ? secondaryLocale : primaryLocale;
                }

                params.put("primary_language", "en");
                params.put("secondary_language", secondaryLocale);

                params.put("user_id", mUserProfile.getUserId());
                params.put("user_type", mUserProfile.getUserType());
                params.put("mandi_name", mMandiName);

                if (!TextUtils.isEmpty(mMandiCity))
                    params.put("mandi_city", mMandiCity);

                if (!TextUtils.isEmpty(mCropClass))
                    params.put("crop_class", mCropClass);

                if (!TextUtils.isEmpty(mCropName))
                    params.put("crop_name", mCropName);

                Log.d("params", ":" + params);
                return params;
            }
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUserProfile.getApiToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
        */
        HashMap<String, String> params = new HashMap<>();
        params.put("api_token", mUserProfile.getApiToken());

        PrefManager prefManager = new PrefManager(getContext());
        String primaryLocale = prefManager.getLocale();
        String secondaryLocale = prefManager.getSecondaryLocale();

        if (TextUtils.isEmpty(secondaryLocale)) {
            secondaryLocale = primaryLocale.equals("en") ? "hi" : primaryLocale;
        } else {
            secondaryLocale = primaryLocale.equals("en") ? secondaryLocale : primaryLocale;
        }

        params.put("primaryLanguage", "en");
        params.put("secondaryLanguage", secondaryLocale);

        params.put("userId", mUserProfile.getUserId());
        params.put("userType", mUserProfile.getUserType());
        params.put("mandiName", mMandiName);

        if (!TextUtils.isEmpty(mMandiCity))
            params.put("mandiCity", mMandiCity);

        if (!TextUtils.isEmpty(mCropClass))
            params.put("cropClass", mCropClass);

        if (!TextUtils.isEmpty(mCropName))
            params.put("cropName", mCropName);

        API_Manager api_manager = new API_Manager();
        CA retrofit_interface=api_manager.getClient11().create(CA.class);
        Call<CAlist> call = retrofit_interface.CA_LIST(mUserProfile.getApiToken(),params);
        call.enqueue(new Callback<CAlist>() {
            @Override
            public void onResponse(Call<CAlist> call, Response<CAlist> response) {
                Logs.d("CA_CheckStatus", "onResponse: CA "+response);
                if (mListener != null)
                    mListener.dismissProgress();
                Logs.d("CA_CheckStatus", "onResponse: CA body "+response.body());
                Logs.d("CA_CheckStatus", "onResponse: CA status "+response.body().getStatus());
                Logs.d("CA_CheckStatus", "onResponse: CA message "+response.body().getMessage());
                    if (response.body()!=null && response.body().getStatus() == 200) {
                        List<CaListModel> caList = response.body().getData().getCaRating();
                        if (caList == null || caList.size() == 0) {
                            setEmptyText();
                        } else {
                            mEmptyText.setVisibility(View.GONE);
                            mCAListAdapter.setData(caList);
                        }
                    } else {
                        setEmptyText();
                    }

            }
            @Override
            public void onFailure(Call<CAlist> call, Throwable t) {
                Log.d("CA_CheckStatus", "fail CA"+t.getMessage());
                if (mListener != null)
                    mListener.dismissProgress();
                Utils.showToast(Objects.requireNonNull(getActivity()), t.getMessage());
            }
        });

    }


    public interface OnFragmentInteractionListener {

        void showProgress();

        void dismissProgress();

        void requestPermission(String permission);
    }
}
