package com.agriiprince.mvvm.ui.registration.infostepsfragments;

import android.content.Context;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.helper.UserLocation;
import com.agriiprince.utils.StringUtils;

import java.io.IOException;
import java.util.Locale;

public class InfoLocationFragment extends Fragment {

    private OnInfoLocationInteractionListener mListener;

    private UserLocation userLocation;
    private Location mLocation;

    private TextInputEditText etLocation;

    private StringUtils utils;

    public InfoLocationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_info_location, container, false);

        utils = AppController.getInstance().getStringUtils();
        userLocation = new UserLocation(getActivity());

        ((TextView) view.findViewById(R.id.title)).setText(utils.getLocalizedString(R.string.for_best_performance_of_app_enable_gps));
        ((AppCompatButton) view.findViewById(R.id.info_next)).setText(utils.getLocalizedString(R.string.next));

        etLocation = view.findViewById(R.id.info_location_input);
        etLocation.setHint(utils.getLocalizedString(R.string.location));
        etLocation.setFocusable(false);

        etLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserLastLocation();
            }
        });

        AppCompatButton enableLocation = view.findViewById(R.id.info_location_enable);
        enableLocation.setText(utils.getLocalizedString(R.string.enable_location));
        enableLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserLastLocation();
            }
        });

        view.findViewById(R.id.info_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onInfoLocationSuccess(mLocation);
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getUserLocation();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnInfoLocationInteractionListener) {
            mListener = (OnInfoLocationInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnInfoLocationInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void getUserLocation() {
        userLocation.getLastKnowLocation(new UserLocation.OnUserLocationResults() {
            @Override
            public void onLocationResult(Location lastLocation) {
                if (lastLocation != null)
                    mLocation = lastLocation;
            }

            @Override
            public void onLocationFail() {

            }
        });
    }

    private void getUserLastLocation() {
        userLocation.getLastKnowLocation(mUserLocationCallback);
        if (mLocation == null) {
            userLocation.getCurrentLocation(mUserLocationCallback);
        }
    }

    private UserLocation.OnUserLocationResults mUserLocationCallback = new UserLocation.OnUserLocationResults() {
        @Override
        public void onLocationResult(Location lastLocation) {
            if (lastLocation == null) {
                return;
            }

            InfoLocationFragment.this.mLocation = lastLocation;

            try {
                Geocoder geocoder = new Geocoder(getContext(), Locale.ENGLISH);
                if (geocoder.getFromLocation(lastLocation.getLatitude(), lastLocation.getLongitude(), 1)
                        .get(0).getLocality() != null) {
                    String city = geocoder.getFromLocation(lastLocation.getLatitude(), lastLocation.getLongitude(), 1)
                            .get(0).getLocality();
                    if (city != null) etLocation.setText(city);
                }

            } catch (IOException e) {
                e.printStackTrace();

                Toast.makeText(getContext(), utils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onLocationFail() {
            Toast.makeText(getContext(), utils.getLocalizedString(R.string.please_enable_the_gps_to_continue), Toast.LENGTH_SHORT).show();
        }
    };


    public interface OnInfoLocationInteractionListener {

        void onInfoLocationSuccess(Location location);
    }
}
