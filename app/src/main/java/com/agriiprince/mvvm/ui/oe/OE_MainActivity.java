package com.agriiprince.mvvm.ui.oe;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.activities.CommunicateToAP;
import com.agriiprince.activities.NotificationActivity;
import com.agriiprince.activities.checkprice.CheckPricingActivity;
import com.agriiprince.activities.oe.OE_MandiSelection;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.ActivityOperationsExecutiveBinding;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.SelectLanguageModel;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.login.Logout;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.mvvm.ui.splash.SplashActivity;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.utils.StringUtils;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.agriiprince.appcontroller.AppController.loggedOut;
import static com.agriiprince.utils.Utils.showToast;

public class OE_MainActivity extends AppCompatActivity implements View.OnClickListener {

    String name;
    String mobileNo;
    ProgressDialog progressDialog;
    private PrefManager pref;

    private StringUtils utils;
    private UserProfile mUserProfile;

    private ActivityOperationsExecutiveBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_operations_executive);

        pref = new PrefManager(this);

        utils = AppController.getInstance().getStringUtils();
        mUserProfile = new UserProfile(this);
        name = pref.getUserName();
        mobileNo = pref.getMobileNo();

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(utils.getLocalizedString(R.string.please_wait));

        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setTitle(utils.getLocalizedString(R.string.operative_executive));

        findViewById(R.id.btnCommunicateToAP).setOnClickListener(this);
        findViewById(R.id.btnNotification).setOnClickListener(this);
        findViewById(R.id.btnShowTransactions).setOnClickListener(this);
        findViewById(R.id.btnShowManualCollections).setOnClickListener(this);


        findViewById(R.id.btnOEUpdateProfile).setOnClickListener(this);
        findViewById(R.id.btnUpdateCommodityPrice).setOnClickListener(this);

        mBinding.btnCheckPrice.setOnClickListener(this);

        mBinding.btnOEUpdateProfile.setText(utils.getLocalizedString(R.string.profile));
        mBinding.btnCheckPrice.setText(utils.getLocalizedString(R.string.check_prices_and_profits));
        mBinding.btnUpdateCommodityPrice.setText(utils.getLocalizedString(R.string.update_commodity_prices));
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCommunicateToAP:
                startActivity(new Intent(OE_MainActivity.this, CommunicateToAP.class));
                break;
            case R.id.btnNotification:
                startActivity(new Intent(OE_MainActivity.this, NotificationActivity.class));
                break;
            //case R.id.btnShowTransactions:
            //    startActivity(new Intent(OE_MainActivity.this, ShowConsignmentsActivity.class));
            //    break;
            case R.id.btnShowManualCollections:
                startActivity(new Intent(OE_MainActivity.this, OE_MandiSelection.class));
                break;
            case R.id.btnOEUpdateProfile:
                startActivity(new Intent(OE_MainActivity.this, OeProfileActivity.class));
                break;
            case R.id.btnUpdateCommodityPrice:
                startActivity(new Intent(OE_MainActivity.this, OePriceUpdateActivity.class));
                break;
            case R.id.btnCheckPrice:
                startActivity(new Intent(OE_MainActivity.this, CheckPricingActivity.class));
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_operations_executive, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
/*

            case R.id.profile:

                startActivity(new Intent(getApplicationContext(), OpExecProfileActivity.class));
                return true;

            case R.id.terms_conditions:

                checkStatus();
                return true;

            case R.id.invite_user:

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                LayoutInflater inflater = this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.custom_dialog_op_exec, null);

                EditText sendMobileNo = dialogView.findViewById(R.id.enter_number);
                builder.setView(dialogView)
                        .setTitle("Invite user")
                        .setMessage("Enter mobile no.")
                        .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //apk link sent to mobile no.
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        })
                        .create()
                        .show();
                return true;

            case R.id.request_code:

                //request for code
                return true;

*/
            case R.id.language:
                showBilingualSelectionDialog();
                return true;
            case R.id.logout:

                fnLogout();
                return true;

        }

        return super.onOptionsItemSelected(item);

    }

    API_Manager apiManager=new API_Manager();
    UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);

    private void fnLogout() {
        //progressDialog.show();
        //local server hosted on my laptop

            Call<Logout> call = retrofit_interface.logout(mUserProfile.getUserMobile(),mUserProfile.getUserId(),mUserProfile.getUserType());
            call.enqueue(new Callback<Logout>() {
                @Override
                public void onResponse(Call<Logout> call, retrofit2.Response<Logout> response) {
                    Logs.d("CheckStatus", "onResponse: logout Success");
                    if (response.isSuccessful() && response.body() != null) {
                        Logs.d("CheckStatus",response.body().getStatus().toString());
                        Logs.d("CheckStatus",response.body().getMessage());

                        pref.logOut();
                        mUserProfile.logOut();

                        loggedOut = 1;

                        startActivity(new Intent(OE_MainActivity.this, SplashActivity.class));
                        finish();
                    }

                    else
                    {
                        Toast.makeText(OE_MainActivity.this,
                                utils.getLocalizedString(R.string.sorry_something_went_wrong_unable_to_logout_currently_please_try_after_sometime),
                                Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Logout> call, Throwable t) {
                    //progressDialog.dismiss();
                    Log.d("CheckStatus", "fail entry");
                    if (t instanceof TimeoutError) {
                        showToast(OE_MainActivity.this, "Slow internet...Kindly check internet to proceed seamlessly.");
                    } else if (t instanceof NoConnectionError) {
                        showToast(OE_MainActivity.this, "No internet connection ");
                    }
                }
            });

    }

    private void showBilingualSelectionDialog() {
        final List<SelectLanguageModel> mLanguageModelList = new ArrayList<>();
        SelectLanguageModel model = new SelectLanguageModel();
        model.languageTitle = "English";
        model.languageLetter = "E";
        model.languageCode = "en";
        mLanguageModelList.add(model);
        model = new SelectLanguageModel();
        model.languageTitle = "हिंदी";
        model.languageLetter = "हि";
        model.languageCode = "hi";
        mLanguageModelList.add(model);
        model = new SelectLanguageModel();
        model.languageTitle = "ಕನ್ನಡ";
        model.languageLetter = "ಕ";
        model.languageCode = "kn";
        mLanguageModelList.add(model);

        PrefManager prefManager = new PrefManager(this);
        String primaryLocale = prefManager.getLocale();

        List<String> languages = new ArrayList<>();

        if (primaryLocale.equalsIgnoreCase("en")) {
            for (SelectLanguageModel selectLanguageModel : mLanguageModelList) {
                if (!selectLanguageModel.languageCode.equalsIgnoreCase(primaryLocale)) {
                    languages.add(selectLanguageModel.languageTitle);
                }
            }
        } else {
            for (SelectLanguageModel selectLanguageModel : mLanguageModelList) {
                if (selectLanguageModel.languageCode.equalsIgnoreCase("en")) {
                    languages.add(selectLanguageModel.languageTitle);
                }
            }
        }

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_for_bilingual_selection, null);
        ((TextView) dialogView.findViewById(R.id.text_second)).setText(utils.getLocalizedString(R.string.select_second_language_to_show));
        ((Button) dialogView.findViewById(R.id.btnDisable)).setText(utils.getLocalizedString(R.string.disable));
        ((Button) dialogView.findViewById(R.id.btnSubmit)).setText(utils.getLocalizedString(R.string.submit));

        dialogBuilder.setView(dialogView);
        final AlertDialog alert = dialogBuilder.create();
        final Spinner spinnerLanguages = dialogView.findViewById(R.id.spinner_language_list);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, languages);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLanguages.setAdapter(adapter);

        dialogView.findViewById(R.id.btnDisable).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                utils.getPref().setSecondaryLocale(null);
                updateLanguageSelection(" ");
                refreshScreen();
            }
        });

        dialogView.findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String selectedLanguage = (String) spinnerLanguages.getSelectedItem();

                for (SelectLanguageModel languageModel : mLanguageModelList) {
                    if (languageModel.languageTitle.equalsIgnoreCase(selectedLanguage)) {
                        utils.getPref().setSecondaryLocale(languageModel.languageCode);
                        updateLanguageSelection(languageModel.languageCode);
                        break;
                    }
                }
            }
        });
        alert.show();
    }

    private void updateLanguageSelection(final String secondaryLanguage) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_OE_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Logs.d("CheckStatus_oe", "onResponse: update profile Success"+response);
                        refreshScreen();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Logs.d("CheckStatus_oe", "update profile fail "+error.getMessage());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());
                //Mandatory
                params.put("user_id", mUserProfile.getUserId());
                params.put("password", mUserProfile.getUserPassword());

                params.put("secondary_language", secondaryLanguage);
                Log.d("secondary lan:  ", params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUserProfile.getApiToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void refreshScreen() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
}
