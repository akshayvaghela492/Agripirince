package com.agriiprince.mvvm.ui.disease;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.databinding.FragmentDiseaseInformationBinding;
import com.agriiprince.models.DiseaseInfoModel;
import com.agriiprince.utils.StringUtils;

import org.jetbrains.annotations.NotNull;

public class DiseaseInformationFragment extends Fragment {

    private static final String TAG = "DiseaseInformationFragm";

    private static final String ARG_DISEASE_INFO = "disease_info";

    private StringUtils stringUtils;

    private DiseaseInfoModel diseaseInfoModel;

    private FragmentDiseaseInformationBinding binding;

    public DiseaseInformationFragment() {
        // Required empty public constructor
    }

    public static DiseaseInformationFragment newInstance(DiseaseInfoModel diseaseInfoModel) {
        DiseaseInformationFragment fragment = new DiseaseInformationFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_DISEASE_INFO, diseaseInfoModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            diseaseInfoModel = getArguments().getParcelable(ARG_DISEASE_INFO);
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_disease_information, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = DataBindingUtil.bind(view);
        stringUtils = AppController.getInstance().getStringUtils();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (diseaseInfoModel != null) {
            if (!TextUtils.isEmpty(diseaseInfoModel.getCropName())) {
                Log.d(TAG, "onActivityCreated: " + diseaseInfoModel.getCropNameBi());
                binding.cropNameLabel.setText(stringUtils
                        .getLocalizedString(DiseaseInfoModel.getCropNameTitle(), true));
                binding.cropName.setText(stringUtils
                        .getBilingualStringForApi(diseaseInfoModel.getCropName(), diseaseInfoModel.getCropNameBi()));
            }

            if (!TextUtils.isEmpty(diseaseInfoModel.getDiseaseName())) {
                binding.diseaseNameLabel.setText(stringUtils
                        .getLocalizedString(DiseaseInfoModel.getDiseaseNameTitle(), true));
                binding.diseaseName.setText(stringUtils
                        .getBilingualStringForApi(diseaseInfoModel.getDiseaseName(), diseaseInfoModel.getDiseaseNameBi()));
            }

            if (!TextUtils.isEmpty(diseaseInfoModel.getScientificName())) {
                binding.scientificNameLabel.setText(stringUtils
                        .getLocalizedString(DiseaseInfoModel.getScientificNameTitle(), true));
                binding.scientificName.setText(stringUtils
                        .getBilingualStringForApi(diseaseInfoModel.getScientificName(), diseaseInfoModel.getScientificNameBi()));
            }

            if (!TextUtils.isEmpty(diseaseInfoModel.getCategory())) {
                binding.categoryLabel.setText(stringUtils
                        .getLocalizedString(DiseaseInfoModel.getCategoryTitle(), true));
                binding.category.setText(stringUtils
                        .getBilingualStringForApi(diseaseInfoModel.getCategory(), diseaseInfoModel.getCategoryBi()));
            }

            if (!TextUtils.isEmpty(diseaseInfoModel.getCause())) {
                binding.causesLabel.setText(stringUtils
                        .getLocalizedString(DiseaseInfoModel.getCauseTitle(), true));
                binding.causes.setText(stringUtils
                        .getBilingualStringForApi(diseaseInfoModel.getCause(), diseaseInfoModel.getCauseBi()));
            }
        }
    }
}
