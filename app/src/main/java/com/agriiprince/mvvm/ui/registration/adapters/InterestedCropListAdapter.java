package com.agriiprince.mvvm.ui.registration.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agriiprince.db.DatabaseManager;
import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.mvvm.model.crop.Variety;
import com.agriiprince.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class InterestedCropListAdapter extends RecyclerView.Adapter<InterestedCropListAdapter.ViewHolder> {

    private static final String TAG = "InterestedCropListAdapt";

    private int mDisplaySize;
    private boolean mIsEditable;
    private Context context;
    private List<Crop> mCrops;
    private StringUtils utils;
    private List<Crop> mInterestedCropsList;
    private List<String> ids;

    private OnItemClickListener mOnItemClickListener;
    DatabaseManager mDatabase;

    public InterestedCropListAdapter() {
        utils = AppController.getInstance().getStringUtils();
        this.mInterestedCropsList = new ArrayList<>();

    }

    public interface OnItemClickListener {
        void onItemClick(String crop);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    public InterestedCropListAdapter(int displaySize, boolean isEdit, Context context) {
        mIsEditable = isEdit;
        mDisplaySize = displaySize;
        utils = AppController.getInstance().getStringUtils();
        this.mInterestedCropsList = new ArrayList<>();
        this.context = context;
    }

    public InterestedCropListAdapter(int displaySize, boolean isEdit, Context context, List<Crop> mCrops) {
        mIsEditable = isEdit;
        mDisplaySize = displaySize;
        utils = AppController.getInstance().getStringUtils();
        this.mInterestedCropsList = new ArrayList<>();
        this.context = context;
        this.mCrops = mCrops;
    }

    public void setData(List<Crop> list, List<String> ids) {
        mInterestedCropsList = list;
        this.ids = ids;
        notifyDataSetChanged();
    }

/*
    public void addData(String crop) {
        mInterestedCropsList.add(crop);
        notifyDataSetChanged();
    }

    public List<String> getCropsList() {
        return mInterestedCropsList;
    }
*/

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_interested_crop, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Crop crop = mInterestedCropsList.get(position);
        List<String> varietyList = new ArrayList<>();
        for (String id : ids) {
            for (Variety variety : crop.getVarieties()) {
                if (id.trim().equalsIgnoreCase(variety.id)) {
//                    Log.d(TAG, crop.cropName);
                    varietyList.add(utils.getBilingualStringForApi(variety.varietyName, variety.varietyName_bi, true));
                }
            }
        }

        String text = utils.getBilingualStringForApi(crop.cropName, crop.cropName_bi, true) + "\n" + varietyList;

        Log.d(TAG, "onBindViewHolder: " + text);
        holder.textViewCropName.setText(text);
    }

    @Override
    public int getItemCount() {
        return mInterestedCropsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewCropName;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewCropName = itemView.findViewById(R.id.tv_crop_name);
        }
    }
}
