package com.agriiprince.mvvm.ui.login;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class LoginFragmentBindingModule {

    @ContributesAndroidInjector
    public abstract LoginFragment contributesLoginFragmentInjector();

    @ContributesAndroidInjector
    public abstract RegisterFragment contributesRegisterFragmentInjector();
}
