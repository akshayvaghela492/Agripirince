package com.agriiprince.mvvm.ui.disease;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.databinding.FragmentDiseaseControlBinding;
import com.agriiprince.databinding.ItemDiseaseControlBinding;
import com.agriiprince.utils.StringUtils;

import org.jetbrains.annotations.NotNull;

public class DiseaseControlFragment extends Fragment {
    private static final String ARG_CHEMICAL_CONTROL = "chemical";
    private static final String ARG_BIOLOGICAL_CONTROL = "biological";

    private String mChemicalText;
    private String mBiologicalText;

    private StringUtils stringUtils;

    private FragmentDiseaseControlBinding binding;

    public DiseaseControlFragment() {
        // Required empty public constructor
    }

    public static DiseaseControlFragment newInstance(String chemicalText, String biologicalText) {
        DiseaseControlFragment fragment = new DiseaseControlFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CHEMICAL_CONTROL, chemicalText);
        args.putString(ARG_BIOLOGICAL_CONTROL, biologicalText);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mChemicalText = getArguments().getString(ARG_CHEMICAL_CONTROL);
            mBiologicalText = getArguments().getString(ARG_BIOLOGICAL_CONTROL);
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_disease_control, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = DataBindingUtil.bind(view);
        stringUtils = AppController.getInstance().getStringUtils();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (!TextUtils.isEmpty(mChemicalText))
            binding.scrollChildLayout.addView(getControlView(
                stringUtils.getLocalizedString(R.string.chemical_control).toString(), mChemicalText));

        if (!TextUtils.isEmpty(mBiologicalText))
            binding.scrollChildLayout.addView(getControlView(
                stringUtils.getLocalizedString(R.string.biological_control).toString(), mBiologicalText));
    }

    private View getControlView(String title, String body) {
        View view = getLayoutInflater().inflate(R.layout.item_disease_control, null);
        final ItemDiseaseControlBinding binding = DataBindingUtil.bind(view);

        if (binding != null) {
            binding.controlLabel.setText(title);
            binding.controlBody.setText(body);
            binding.divider.setVisibility(View.GONE);
            binding.controlBody.setVisibility(View.GONE);

            binding.controlLabel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (binding.controlBody.getVisibility() == View.VISIBLE) {
                        binding.controlBody.setVisibility(View.GONE);
                        binding.divider.setVisibility(View.GONE);
                        binding.controlLabel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_up, 0);
                    } else {
                        binding.controlBody.setVisibility(View.VISIBLE);
                        binding.divider.setVisibility(View.VISIBLE);
                        binding.controlLabel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down, 0);
                    }
                }
            });
        }
        return view;
    }
}
