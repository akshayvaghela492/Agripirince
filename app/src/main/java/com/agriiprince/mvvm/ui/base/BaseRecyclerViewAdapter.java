package com.agriiprince.mvvm.ui.base;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerViewAdapter<T, VH extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<VH> implements RecyclerViewUseCase<T>{

    protected List<T> mList;

    public BaseRecyclerViewAdapter() {
        this.mList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void addItem(T item) {
        int index = getItemCount();
        mList.add(item);
        notifyItemInserted(index);
    }

    @Override
    public void addAllItem(List<T> items) {
        int index = getItemCount();
        mList.addAll(items);
        notifyItemRangeInserted(index, items.size());
    }

    @Override
    public void replaceAllItem(List<T> items) {
        mList.clear();
        mList.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public T getItem(int position) {
        return mList.get(position);
    }
}
