package com.agriiprince.mvvm.ui.registration;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.db.DatabaseManager;
import com.agriiprince.R;
import com.agriiprince.mvvm.retrofit.model.login.Profile;
import com.agriiprince.mvvm.ui.registration.adapters.InterestedCropListAdapter;
import com.agriiprince.mvvm.ui.registration.adapters.InterestedMandisAdapter;
import com.agriiprince.mvvm.applevel.analytics.FarmerRealtimeDBAnalytics;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.dataservice.CropDataService;
import com.agriiprince.dataservice.MandiDataService;
import com.agriiprince.mvvm.ui.registration.infostepsfragments.InfoMandiFragment;
import com.agriiprince.mvvm.ui.registration.infostepsfragments.InfoMandiInterestedFragment;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.FarmerProfileModel;
import com.agriiprince.model.MandiModel;
import com.agriiprince.model.SelectLanguageModel;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.profile.UserUpdate;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.utils.StringUtils;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import retrofit2.Call;
import retrofit2.Callback;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.agriiprince.mvvm.applevel.analytics.FarmerFirebaseAnalytics.logEventFarmerClicked;
import static com.agriiprince.utils.Utils.showToast;

public class FarmerProfileActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "FarmerProfileActivity";

    public static final int LIST_DISPLAY_SIZE = 6;
    private final static int REQUEST_CHECK_SETTINGS = 2000;
    private final String DEFAULT_LANGUAGE_CODE = "en";
    private final String DEFAULT_LANGUAGE = "English";
    List<MandiModel> mMandis;
    List<Crop> mCrops;
    List<Crop> interestedCropList = new ArrayList<>();
    List<String> interestedMandiNameList = new ArrayList<>();
    List<String> interestedMandiIdList = new ArrayList<>();

    private AtomicBoolean isMandiLoaded;
    private AtomicBoolean isCropsLoaded;

    private UserProfile mUserProfile;
    private String primaryLanguageCode;
    private String secondaryLanguageCode;
    private TextView mTextViewMobile;
    private EditText mEditTextFullName;
    private EditText mEditTextAddress;
    private EditText mTextViewLocality;
    private EditText mTextViewPinCode;
    private List<SelectLanguageModel> mLanguageModelList = new ArrayList<>();
    private Spinner mSpinnerPrimaryLanguage;
    private RadioButton mRadioButtonBilingualYes;
    private RadioButton mRadioButtonBilingualNo;
    private LinearLayout mLinearLayoutBilingual;
    private Spinner mSpinnerSecondaryLanguage;
    private RadioButton mRadioButtonBankYes;
    private RadioButton mRadioButtonBankNo;
    private LinearLayout mLinearLayoutBankDetails;
    private EditText mEditTextBankName;
    private EditText mEditTextBankAccountNumber;
    private RadioButton mRBHasNetBankingYes;
    private RadioButton mRBHasNetBankingNo;
    private RadioButton mRBHasPaytmYes;
    private RadioButton mRBHasPaytmNo;
    private RadioButton mRBHasBhimYes;
    private RadioButton mRBHasBhimNo;
    private RadioButton mRBScreenSaverEnabledYes;
    private RadioButton mRBScreenSaverEnabledNo;
    private EditText mEtTotalLandAreaInAcres;
    private Spinner mEtAvgAnnualIncome;
    private Spinner mEtPriceMeasuringUnit;
    private Spinner mEtAreaMeasuringUnit;
    private TextView mTvNearestMandi;
    private TextView mTvMoreInterestedMandisCount;
    private RecyclerView mRvInterestedMandisList;
    private InterestedMandisAdapter mInterestedMandisAdapter;
    private TextView mTvMoreInterestedCropsCount;
    private RecyclerView mRvInterestedCropsList;
    private InterestedCropListAdapter mInterestedCropsAdapter;
    private ProgressDialog mProgressDialog;
    private Boolean mForLatLng = false;
    private GoogleApiClient mGoogleApiClient;
    private double latitude, longitude;
    private MandiModel mNearestMandi;
    private Location mLastLocation;
    private String mNearestMandiId;
    private String mNearestMandiName;
    private StringUtils mStringUtils;
    private RadioGroup radioGroupBilingual;
    DatabaseManager mDatabase;
    FirebaseDatabase mFDatabase;
    DatabaseReference mRef;
    DatabaseReference mFarmersRef;
    DatabaseReference mNumRef;
    long mTimeStart;
    String mUserId;
    String mMobile;
    String mName;
    String mCity;
    String mPin;
    Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mStringUtils = AppController.getInstance().getStringUtils();
        mUserProfile = new UserProfile(this);

        isMandiLoaded = new AtomicBoolean(false);
        isCropsLoaded = new AtomicBoolean(false);

        // ANALYTICS DATABASES

        mFDatabase = FirebaseDatabase.getInstance();
        mRef = mFDatabase.getReference("analytics");
        mFarmersRef = mRef.child("farmers");
        mNumRef = mFarmersRef.child(mUserProfile.getUserMobile());
        mUserId = mUserProfile.getUserId();
        mMobile = mUserProfile.getUserMobile();
        mName = mUserProfile.getUserName();
        mCity = mUserProfile.getUserCity();
        mPin = mUserProfile.getUserPincode();
        mContext = getApplicationContext();

        //------------------------

        setContentView(R.layout.activity_farmer_profile);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(mStringUtils.getLocalizedString(R.string.update_profile));

        getMandis();
        getCrops();
        mDatabase = new DatabaseManager(this);
        mTextViewMobile = findViewById(R.id.tv_mobile);
        mEditTextFullName = findViewById(R.id.et_full_name);
        mEditTextAddress = findViewById(R.id.et_address);
        mTextViewLocality = findViewById(R.id.tv_locality);
        mTextViewPinCode = findViewById(R.id.tv_pincode);
        mTextViewMobile.requestFocus();

        // For getting user location
        mForLatLng = true;
        buildGoogleApiClient();
        mForLatLng = false;

        TextView textViewUseCurrentLocation = findViewById(R.id.tv_current_location);
        textViewUseCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
                q.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                    dataSnapshot, "click_prof_update_location", mNumRef);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_prof_update_location");

                buildGoogleApiClient();
            }
        });

        SelectLanguageModel model = new SelectLanguageModel();
        model.languageTitle = "English";
        model.languageLetter = "E";
        model.languageCode = "en";
        mLanguageModelList.add(model);
        model = new SelectLanguageModel();
        model.languageTitle = "हिंदी";
        model.languageLetter = "हि";
        model.languageCode = "hi";
        mLanguageModelList.add(model);
        model = new SelectLanguageModel();
        model.languageTitle = "ಕನ್ನಡ";
        model.languageLetter = "ಕ";
        model.languageCode = "kn";
        mLanguageModelList.add(model);

        mSpinnerPrimaryLanguage = findViewById(R.id.spinner_primary_langauge);
        mSpinnerSecondaryLanguage = findViewById(R.id.spinner_second_language);

        mRadioButtonBilingualYes = findViewById(R.id.rb_bilingual_yes);
        mRadioButtonBilingualNo = findViewById(R.id.rb_bilingual_no);
        mLinearLayoutBilingual = findViewById(R.id.ll_bilingual);

        radioGroupBilingual = findViewById(R.id.rg_bilingual);
        radioGroupBilingual.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_bilingual_yes:
                        mLinearLayoutBilingual.setVisibility(View.VISIBLE);
                        setSecondaryLanguageAdapter(secondaryLanguageCode);
                        break;
                    case R.id.rb_bilingual_no:
                        mLinearLayoutBilingual.setVisibility(View.GONE);
                        break;
                }
            }
        });

        mRadioButtonBankYes = findViewById(R.id.rb_has_bank_yes);
        mRadioButtonBankNo = findViewById(R.id.rb_has_bank_no);
        mLinearLayoutBankDetails = findViewById(R.id.ll_bank_details);
        mEditTextBankName = findViewById(R.id.et_bank_name);
        mEditTextBankAccountNumber = findViewById(R.id.et_bank_account_number);
        RadioGroup radioGroupBankEnable = findViewById(R.id.rg_bank);
        radioGroupBankEnable.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_has_bank_yes:
                        mLinearLayoutBankDetails.setVisibility(View.VISIBLE);
                        break;
                    case R.id.rb_has_bank_no:
                        mLinearLayoutBankDetails.setVisibility(View.GONE);
                        break;
                }
            }
        });

        mRBHasNetBankingYes = findViewById(R.id.rb_net_banking_yes);
        mRBHasNetBankingNo = findViewById(R.id.rb_net_banking_no);
        mRBHasPaytmYes = findViewById(R.id.rb_paytm_yes);
        mRBHasPaytmNo = findViewById(R.id.rb_paytm_no);
        mRBHasBhimYes = findViewById(R.id.rb_bhim_yes);
        mRBHasBhimNo = findViewById(R.id.rb_bhim_no);

        mRBScreenSaverEnabledYes = findViewById(R.id.rb_screensaver_yes);
        mRBScreenSaverEnabledNo = findViewById(R.id.rb_screensaver_no);

        mEtTotalLandAreaInAcres = findViewById(R.id.et_total_land_area);
        mEtAvgAnnualIncome = findViewById(R.id.et_annual_income);
        mEtPriceMeasuringUnit = findViewById(R.id.et_price_measuring_unit);
        mEtAreaMeasuringUnit = findViewById(R.id.et_area_unit);

        mTvNearestMandi = findViewById(R.id.tv_nearest_mandi);
        /*mTvNearestMandi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NearestMandiFragment fragment = new NearestMandiFragment();
                fragment.setOnItemClickListener(new NearestMandiFragment.OnItemmClickListener() {
                    @Override
                    public void onItemClick(MandiModel mandi) {
                        mNearestMandi = mandi;
                        mTvNearestMandi.setText(mNearestMandi == null ? "" : mNearestMandi.mandi_name_en);
                    }
                });
                fragment.setCancelable(false);
                fragment.show(getSupportFragmentManager(), null);
            }
        });*/

        mRvInterestedMandisList = findViewById(R.id.rv_interested_mandis);
        mRvInterestedMandisList.setLayoutManager(new GridLayoutManager(this, 2));
        mRvInterestedMandisList.setNestedScrollingEnabled(false);
        mInterestedMandisAdapter = new InterestedMandisAdapter();
        //mInterestedMandisAdapter.setData(interestedMandiNameList != null ? interestedMandiNameList : new ArrayList<String>());
        mRvInterestedMandisList.setAdapter(mInterestedMandisAdapter);
        mTvMoreInterestedMandisCount = findViewById(R.id.tv_more_interested_mandis_count);

        TextView tvNearestMandis = findViewById(R.id.tv_nearest_mandi);
        tvNearestMandis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
                q.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                    dataSnapshot, "click_prof_nearest_mandi", mNumRef);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_prof_nearest_mandi");

                getSupportFragmentManager().beginTransaction()
                        .add(android.R.id.content, InfoMandiFragment.newInstance(mNearestMandiId, latitude, longitude, true))
                        .addToBackStack(null)
                        .commit();
            }
        });

        TextView tvModifyInterestedMandis = findViewById(R.id.tv_modify_interested_mandis);
        tvModifyInterestedMandis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
                q.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                    dataSnapshot, "click_prof_interested_mandi", mNumRef);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_prof_interested_mandi");

                getSupportFragmentManager().beginTransaction()
                        .add(android.R.id.content, InfoMandiInterestedFragment.newInstance(mNearestMandiId,
                                (ArrayList<String>) interestedMandiIdList, latitude, longitude, true))
                        .addToBackStack(null)
                        .commit();
            }
        });

        mRvInterestedCropsList = findViewById(R.id.rv_interested_crops);
        mRvInterestedCropsList.setLayoutManager(new GridLayoutManager(this, 2));
        mRvInterestedCropsList.setNestedScrollingEnabled(false);
        mInterestedCropsAdapter = new InterestedCropListAdapter(LIST_DISPLAY_SIZE, false, this, mCrops);
        mRvInterestedCropsList.setAdapter(mInterestedCropsAdapter);
        mTvMoreInterestedCropsCount = findViewById(R.id.tv_more_interested_crops_count);
        TextView tvModifyInterestedCrops = findViewById(R.id.tv_modify_interested_crops);
        tvModifyInterestedCrops.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
                q.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                    dataSnapshot, "click_prof_interested_crops", mNumRef);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_prof_interested_crops");

                /*getSupportFragmentManager().beginTransaction()
                        .add(android.R.id.content, InfoCropsFragment.newInstance((ArrayList<String>) interestedCropList, true))
                        .addToBackStack(null)
                        .commit();*/
                //finish();
                Intent mIntent = new Intent(FarmerProfileActivity.this, SelectCropActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean("fromProfile", true);
                mIntent.putExtras(mBundle);
                startActivity(mIntent);

            }
        });

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getResources().getString(R.string.please_wait));

        findViewById(R.id.button_update_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    updateProfile();
                }
            }
        });
        setTextViews();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mTimeStart = System.currentTimeMillis();
    }

    @Override
    protected void onStop() {
        super.onStop();
        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - mTimeStart;
        final int elapsedSeconds = (int)(tDelta / 1000.0);

        Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    FarmerRealtimeDBAnalytics.saveEventFarmerTimeSpent(
                            dataSnapshot, "time_spent_profile", mNumRef, elapsedSeconds);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setTextViews() {
        ((TextView) findViewById(R.id.general_info)).setText(mStringUtils.getLocalizedString(R.string.general_info));
        ((TextView) findViewById(R.id.other)).setText(mStringUtils.getLocalizedString(R.string.other));

        ((TextView) findViewById(R.id.address)).setText(mStringUtils.getLocalizedString(R.string.address));
        ((TextView) findViewById(R.id.tv_current_location)).setText(mStringUtils.getLocalizedString(R.string.use_current_location));
        ((TextView) findViewById(R.id.pref_lan)).setText(mStringUtils.getLocalizedString(R.string.preferred_language));
        ((TextView) findViewById(R.id.enable_second_language)).setText(mStringUtils.getLocalizedString(R.string.enable_second_language));
        ((TextView) findViewById(R.id.select_second_language)).setText(mStringUtils.getLocalizedString(R.string.select_second_language));
        ((TextView) findViewById(R.id.have_online_bank_account)).setText(mStringUtils.getLocalizedString(R.string.have_online_bank_account));
        ((TextView) findViewById(R.id.bank_details)).setText(mStringUtils.getLocalizedString(R.string.bank_details));
        ((TextView) findViewById(R.id.do_you_have_net_banking)).setText(mStringUtils.getLocalizedString(R.string.do_you_have_net_banking));
        ((TextView) findViewById(R.id.do_you_have_paytm)).setText(mStringUtils.getLocalizedString(R.string.do_you_have_paytm));
        ((TextView) findViewById(R.id.do_you_have_bhim)).setText(mStringUtils.getLocalizedString(R.string.do_you_have_bhim));
        ((TextView) findViewById(R.id.enable_screen_saver)).setText(mStringUtils.getLocalizedString(R.string.enable_screen_saver));
        ((TextView) findViewById(R.id.average_annual_income)).setText(mStringUtils.getLocalizedString(R.string.average_annual_income));
        ((TextView) findViewById(R.id.total_land_area_n_in_acres)).setText(mStringUtils.getLocalizedString(R.string.total_land_area_n_in_acres));
        ((TextView) findViewById(R.id.price_measuring_unit)).setText(mStringUtils.getLocalizedString(R.string.price_measuring_unit));
        ((TextView) findViewById(R.id.nearest_mandi)).setText(mStringUtils.getLocalizedString(R.string.nearest_mandi));
        ((TextView) findViewById(R.id.interested_mandis)).setText(mStringUtils.getLocalizedString(R.string.interested_mandis));
        ((TextView) findViewById(R.id.tv_modify_interested_mandis)).setText(mStringUtils.getLocalizedString(R.string.add_modify_interests));
        ((TextView) findViewById(R.id.interested_crops)).setText(mStringUtils.getLocalizedString(R.string.interested_crops));
        ((TextView) findViewById(R.id.tv_modify_interested_crops)).setText(mStringUtils.getLocalizedString(R.string.add_modify_interests));
        ((TextView) findViewById(R.id.interested_rss)).setText(mStringUtils.getLocalizedString(R.string.interested_rss));
        ((TextView) findViewById(R.id.tv_modify_rss)).setText(mStringUtils.getLocalizedString(R.string.add_modify_rss));

        ((Button) findViewById(R.id.button_update_profile)).setText(mStringUtils.getLocalizedString(R.string.update_profile));

        //Clone/Echo effect for blank values
        //((AppCompatEditText) findViewById(R.id.et_full_name)).setHint(stringUtils.getLocalizedString(R.string.enter_full_name, true));
        //((AppCompatEditText) findViewById(R.id.tv_mobile)).setHint(stringUtils.getLocalizedString(R.string.mobile, true));
        //((AppCompatEditText) findViewById(R.id.et_address)).setHint(stringUtils.getLocalizedString(R.string.enter_address, true));
        //((AppCompatEditText) findViewById(R.id.tv_locality)).setHint(stringUtils.getLocalizedString(R.string.locality_city, true));
        //((AppCompatEditText) findViewById(R.id.tv_pincode)).setText(stringUtils.getLocalizedString(R.string.pincode, true));
        ((EditText) findViewById(R.id.et_bank_name)).setHint(mStringUtils.getLocalizedString(R.string.bank_name, true));
        ((EditText) findViewById(R.id.et_bank_account_number)).setHint(mStringUtils.getLocalizedString(R.string.bank_account_number, true));
        ((EditText) findViewById(R.id.et_total_land_area)).setHint(mStringUtils.getLocalizedString(R.string.enter_total_land_area, true));


    }


    private void getProfileDetails() {
        mProgressDialog.show();
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST, Config.GET_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        Log.d("TESTING_VOLLEY", response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            //JSONArray responseArray = new JSONArray(response);
                            //JSONObject responseObject = responseArray.getJSONObject(0);
                            //int errorCode = responseObject.getInt("error_code");
                            int errorCode = jsonObject.getInt("code");
                            //if (errorCode == 100)
                            if (errorCode == 200)
                            {
                                //String data = responseObject.getString("user_details");
                                JSONObject dataobj = jsonObject.getJSONObject("data");
                                String data = dataobj.getString("user_details");
                                FarmerProfileModel profile = new Gson().fromJson(data, new TypeToken<FarmerProfileModel>() {
                                }.getType());
                                setProfileData(profile);
                            } else {
                                Toast.makeText(FarmerProfileActivity.this, mStringUtils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();
                        if (error instanceof TimeoutError) {
                            showToast(FarmerProfileActivity.this, mStringUtils.getLocalizedString(R.string.slow_internet_kindly_check_internet_to_proceed_seamlessly).toString());
                        } else if (error instanceof NoConnectionError) {
                            showToast(FarmerProfileActivity.this, mStringUtils.getLocalizedString(R.string.no_internet_connection_try_later).toString());
                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());

                params.put("user_id", mUserProfile.getUserId());
                params.put("user_type", mStringUtils.getPref().getUserType());
                System.out.println("Get profile body: " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> header = new HashMap<>();
                header.put("api_token", mUserProfile.getApiToken());
                return header;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);

    }

    private List<String> getPrimaryLanguageTitles() {
        List<String> languages = new ArrayList<>();
        for (SelectLanguageModel model : mLanguageModelList)
            languages.add(model.languageTitle);

        return languages;
    }

    private List<String> getSecondaryLanguageTitles() {
        List<String> languages = new ArrayList<>();

        // if primary language is english then user can select any other languages
        if (primaryLanguageCode.equalsIgnoreCase("en")) {
            for (SelectLanguageModel model : mLanguageModelList)
                if (!model.languageCode.equals(primaryLanguageCode))
                    languages.add(model.languageTitle);
        } else {
            // if primary language is not english then user can only select english
            for (SelectLanguageModel model : mLanguageModelList) {
                if (model.languageCode.equals("en")) {
                    languages.add(model.languageTitle);
                    break;
                }
            }
        }

        return languages;
    }

    private void setPrimaryLanguageAdapter(String selectedLanguageCode) {
        List<String> languages = getPrimaryLanguageTitles();
        String title = getLanguageTitleByCode(selectedLanguageCode);

        int index = 0;
        for (int i = 0; i < languages.size(); i++) {
            if (languages.get(i).equals(title)) {
                index = i;
                break;
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, languages);
        mSpinnerPrimaryLanguage.setAdapter(adapter);
        mSpinnerPrimaryLanguage.setSelection(index);

        mSpinnerPrimaryLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String language = parent.getAdapter().getItem(position).toString();
                primaryLanguageCode = getLanguageCodeByTitle(language);
                if (primaryLanguageCode.equals(secondaryLanguageCode))
                    setSecondaryLanguageAdapter(null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setSecondaryLanguageAdapter(@Nullable String selectedLanguageCode) {
        if (mRadioButtonBilingualNo.isChecked()) return;

        List<String> languages = getSecondaryLanguageTitles();
        String title = getLanguageTitleByCode(selectedLanguageCode);

        int index = 0;
        if (!TextUtils.isEmpty(selectedLanguageCode)) {
            for (int i = 0; i < languages.size(); i++) {
                if (languages.get(i).equals(title)) {
                    index = i;
                    break;
                }
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, languages);
        mSpinnerSecondaryLanguage.setAdapter(adapter);
        mSpinnerSecondaryLanguage.setSelection(index);

        mSpinnerSecondaryLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String language = parent.getAdapter().getItem(position).toString();
                secondaryLanguageCode = getLanguageCodeByTitle(language);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private String getLanguageCodeByTitle(String title) {
        for (SelectLanguageModel model : mLanguageModelList)
            if (model.languageTitle.equals(title))
                return model.languageCode;

        return DEFAULT_LANGUAGE_CODE;
    }

    private String getLanguageTitleByCode(String code) {
        for (SelectLanguageModel model : mLanguageModelList)
            if (model.languageCode.equals(code))
                return model.languageTitle;

        return DEFAULT_LANGUAGE;
    }


    private void setProfileData(FarmerProfileModel profile) {
        mTextViewMobile.setText(profile.farmer_contact);
        mEditTextFullName.setText(profile.farmer_name);
        mEditTextAddress.setText(profile.farmer_address);
        mTextViewLocality.setText(profile.farmer_location);
        //mTextViewLocality.setText(mStringUtils.getPref().getCity());
        mTextViewPinCode.setText(profile.farmer_pincode);
        //mTextViewPinCode.setText(mStringUtils.getPref().getPinCode());

        primaryLanguageCode = TextUtils.isEmpty(profile.preferred_language.trim()) ?
                mStringUtils.getPref().getLocale() : profile.preferred_language.trim();

        secondaryLanguageCode = TextUtils.isEmpty(profile.secondary_language.trim()) ?
                mStringUtils.getPref().getSecondaryLocale() : profile.secondary_language.trim();

        if (TextUtils.isEmpty(secondaryLanguageCode))
            radioGroupBilingual.check(mRadioButtonBilingualNo.getId());
        else radioGroupBilingual.check(mRadioButtonBilingualYes.getId());

        setPrimaryLanguageAdapter(primaryLanguageCode);
        setSecondaryLanguageAdapter(secondaryLanguageCode);


        if (TextUtils.isEmpty(profile.bank_name)) {
            mRadioButtonBankNo.setChecked(true);
            mLinearLayoutBankDetails.setVisibility(View.GONE);
        } else {
            mRadioButtonBankYes.setChecked(true);
            mLinearLayoutBankDetails.setVisibility(View.VISIBLE);
            mEditTextBankName.setText(profile.bank_name);
            mEditTextBankAccountNumber.setText(profile.bank_account_number);
        }

        if (profile.has_net_banking.equalsIgnoreCase("Y")) {
            mRBHasNetBankingYes.setChecked(true);
        } else {
            mRBHasNetBankingNo.setChecked(true);
        }
        if (profile.has_paytm.equalsIgnoreCase("Y")) {
            mRBHasPaytmYes.setChecked(true);
        } else {
            mRBHasPaytmNo.setChecked(true);
        }
        if (profile.has_bhim.equalsIgnoreCase("Y")) {
            mRBHasBhimYes.setChecked(true);
        } else {
            mRBHasBhimNo.setChecked(true);
        }

        if (profile.screen_saver_enabled.equalsIgnoreCase("Y")) {
            mRBScreenSaverEnabledYes.setChecked(true);
        } else {
            mRBScreenSaverEnabledNo.setChecked(true);
        }

        mEtTotalLandAreaInAcres.setText(profile.land_in_acres);

        List<String> avgAnnualIncome = new ArrayList<>(Arrays.asList(FarmerProfileModel.incomeRange));
        String income = FarmerProfileModel.getIncomeByOneDigit(profile.average_annual_income);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, avgAnnualIncome);
        mEtAvgAnnualIncome.setAdapter(adapter);
        mEtAvgAnnualIncome.setSelection(adapter.getPosition(income));

        List<String> priceMeasuringUnit = new ArrayList<>(Arrays.asList(FarmerProfileModel.priceUnits));
        String priceUnit = profile.price_measuring_unit;
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, priceMeasuringUnit);
        mEtPriceMeasuringUnit.setAdapter(adapter2);
        mEtPriceMeasuringUnit.setSelection(adapter2.getPosition(priceUnit));


        List<String> preferredMeasuringUnit = new ArrayList<>(Arrays.asList(FarmerProfileModel.areaUnits));
        String areaUnit = profile.area_measuring_unit;
        ArrayAdapter<String> adapter3 = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, preferredMeasuringUnit);
        mEtAreaMeasuringUnit.setAdapter(adapter3);
        mEtAreaMeasuringUnit.setSelection(adapter3.getPosition(areaUnit));

        if (TextUtils.isEmpty(profile.nearest_mandi_name)) {
            mTvNearestMandi.setText(mStringUtils.getLocalizedString(R.string.set_nearest_mandi));
        } else {
            mNearestMandi = new MandiModel();
            mNearestMandi.mandi_name_en = MandiModel.getMandiNameById(FarmerProfileActivity.this, mMandis, profile.nearest_mandi_name.toString());
            mNearestMandi.mandi_lat = profile.nearest_mandi_lat;
            mNearestMandi.mandi_lng = profile.nearest_mandi_log;

            //MandiModel mandiModel = MandiModel.getByMandiName(mMandis, profile.nearest_mandi_name);
            //if (mandiModel != null)
            //mTvNearestMandi.setText(mStringUtils.getBilingualStringForApi(mandiModel.mandi_city_combi_en, mandiModel.mandi_city_combi_bi));

            mNearestMandiId = profile.nearest_mandi_name;
            mNearestMandiName = MandiModel.getMandiNameById(FarmerProfileActivity.this, mMandis, mNearestMandiId);
            mTvNearestMandi.setText(mNearestMandiName);
        }

        if (!TextUtils.isEmpty(profile.interested_mandi_list) && mMandis != null) {
            String[] mandiIds = profile.interested_mandi_list.replace("[", "").replace("]", "").split(",");
            for (String mandiId : mandiIds) {
//                Log.d("Profile: ", mandiId);
                //MandiModel mandiModel = MandiModel.getByMandiName(mMandis, mandiId.trim());
                mandiId = mandiId.trim();
                interestedMandiIdList.add(mandiId);
                String mandiName = MandiModel.getMandiNameById(FarmerProfileActivity.this, mMandis, mandiId);
                interestedMandiNameList.add(mandiName);
            }
        }
        if (interestedMandiNameList == null || interestedMandiNameList.size() == 0) {
            mRvInterestedMandisList.setVisibility(View.GONE);
            mTvMoreInterestedMandisCount.setVisibility(View.VISIBLE);
            mTvMoreInterestedMandisCount.setText(mStringUtils.getLocalizedString(R.string.no_mandis_added));
        } else {
            mRvInterestedMandisList.setVisibility(View.VISIBLE);
            mTvMoreInterestedMandisCount.setVisibility(View.GONE);
/*
            if (interestedMandiNameList.size() <= LIST_DISPLAY_SIZE) {
            } else {
                mTvMoreInterestedMandisCount.setVisibility(View.VISIBLE);
                mTvMoreInterestedMandisCount.setText(interestedMandiNameList.size() - LIST_DISPLAY_SIZE + " more");
            }
*/
        }
        mInterestedMandisAdapter.setData(interestedMandiNameList != null ? interestedMandiNameList : new ArrayList<String>());

        if (!TextUtils.isEmpty(profile.interested_crop_list) && mCrops != null) {
            String[] cropIds = profile.interested_crop_list
                    .replace(" ", "")
                    .replace("[", "")
                    .replace("]", "").split(",");

            //have to find good solution for this shitty code.....
            try {
/*
                mDatabase.deleteInterestedCropEntries();
                for (String id : cropIds) {
//                    Log.d("Profile Crops", "" + crop.trim());
                    for (Crop crop : mCrops) {
                        for (Variety variety : crop.getVarieties()) {
                            if (id.trim().equalsIgnoreCase(variety.id)) {
                                mDatabase.addToCropTable(variety.id, variety.varietyName, crop.cropName);
                                if (! contains(interestedCropList, crop.cropName)) {
//                                        Log.d(TAG, variety.varietyName);
                                    interestedCropList.add(crop.cropName);
                                }
                            }
                        }
                    }
                }
*/

                interestedCropList = Crop.getCropsByIds(mCrops, cropIds);
                Log.d(TAG, "" + interestedCropList.size() + "   " + TextUtils.join(",", interestedCropList));
                mInterestedCropsAdapter.setData(interestedCropList, Arrays.asList(cropIds));

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                mDatabase.close();
            }
        }
        if (interestedCropList == null || interestedCropList.size() == 0) {
            mRvInterestedCropsList.setVisibility(View.GONE);
            mTvMoreInterestedCropsCount.setVisibility(View.VISIBLE);
            mTvMoreInterestedCropsCount.setText(mStringUtils.getLocalizedString(R.string.no_crops_added));
        } else {
            mRvInterestedCropsList.setVisibility(View.VISIBLE);
            mTvMoreInterestedCropsCount.setVisibility(View.GONE);
/*
            if (interestedCropList.size() <= LIST_DISPLAY_SIZE) {
                mTvMoreInterestedCropsCount.setVisibility(View.GONE);
            } else {
                mTvMoreInterestedCropsCount.setVisibility(View.VISIBLE);
//                mTvMoreInterestedCropsCount.setText(interestedCropList.size() - LIST_DISPLAY_SIZE + mStringUtils.getLocalizedString(R.string.more).toString());
            }
*/
        }

        if (profile.interested_rss_topics != null) {
            //TODO: Aditya
        }
    }

    boolean contains(List<String> list, String name) {
        for (String item : list) {
            if (item.equals(name)) {
                return true;
            }
        }
        return false;
    }

    private void updateProfile() {
        mProgressDialog.show();
  /*      StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_FARMER_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        System.out.println("update_farmer_profile_response: " + response);
                        try {
                            JSONArray responseArray = new JSONArray(response);
                            JSONObject responseObject = responseArray.getJSONObject(0);
                            int errorCode = responseObject.getInt("error_code");
                            if (errorCode == 100) {

                                mUserProfile.setUserName(mEditTextFullName.getText().toString());
                                mStringUtils.getPref().setPrimaryLocale(primaryLanguageCode);
                                if (mRadioButtonBilingualYes.isChecked()) {
                                    mStringUtils.getPref().setSecondaryLocale(secondaryLanguageCode);
                                } else {
                                    mStringUtils.getPref().setSecondaryLocale(null);
                                }
                                Toast.makeText(FarmerProfileActivity.this, mStringUtils.getLocalizedString(R.string.successfully_updated), Toast.LENGTH_SHORT).show();
                                onBackPressed();
//                                mStringUtils.getPref().setFarmerProfile(new Gson().toJson(response));
                            } else {
                                Toast.makeText(FarmerProfileActivity.this, mStringUtils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();
                        if (error instanceof TimeoutError) {
                            showToast(FarmerProfileActivity.this, mStringUtils.getLocalizedString(R.string.slow_internet_kindly_check_internet_to_proceed_seamlessly).toString());
                        } else if (error instanceof NoConnectionError) {
                            showToast(FarmerProfileActivity.this, mStringUtils.getLocalizedString(R.string.no_internet_connection_try_later).toString());
                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("api_token", mUserProfile.getApiToken());

                //Mandatory
                params.put("user_id", mStringUtils.getPref().getUserId());
                params.put("password", mStringUtils.getPref().getUserPassword());

                //Optional
                params.put("farmer_contact", mStringUtils.getPref().getMobileNo());
                params.put("farmer_name", mEditTextFullName.getText().toString());
                params.put("farmer_address", mEditTextAddress.getText().toString());
                //params.put("farmer_location", mStringUtils.getPref().getCity());
                //params.put("farmer_pincode", mStringUtils.getPref().getPinCode());

                params.put("farmer_location", mTextViewLocality.getText().toString());
                params.put("farmer_pincode", mTextViewPinCode.getText().toString());

                params.put("preferred_language", primaryLanguageCode);
                params.put("secondary_language", mRadioButtonBilingualYes.isSelected() ? secondaryLanguageCode : " ");

                if (mRadioButtonBilingualYes.isChecked()) {
                    mStringUtils.getPref()
                            .setSecondaryLocale(mLanguageModelList.get(mSpinnerSecondaryLanguage.getSelectedItemPosition()).languageCode);
                } else {
                    mStringUtils.getPref().setSecondaryLocale(null);
                }


                if (mRadioButtonBankYes.isSelected()) {
                    params.put("bank_name", mEditTextBankName.getText().toString());
                    params.put("bank_account_number", mEditTextBankAccountNumber.getText().toString());
                } else {
                    params.put("bank_name", "");
                    params.put("bank_account_number", "");
                }

                params.put("has_net_banking", mRBHasNetBankingYes.isSelected() ? "Y" : "N");
                params.put("has_paytm", mRBHasPaytmYes.isSelected() ? "Y" : "N");
                params.put("has_bhim", mRBHasBhimYes.isSelected() ? "Y" : "N");

                params.put("screen_saver_enabled", mRBScreenSaverEnabledYes.isSelected() ? "Y" : "N");

                int position = mEtAvgAnnualIncome.getSelectedItemPosition();
                String theSlectedItemValue = FarmerProfileModel.getOneDigitIncome(position);

                params.put("average_annual_income", theSlectedItemValue);
                params.put("land_in_acres", mEtTotalLandAreaInAcres.getText().toString());
                params.put("price_measuring_unit", mEtPriceMeasuringUnit.getSelectedItem().toString());
                params.put("area_measuring_unit", mEtAreaMeasuringUnit.getSelectedItem().toString());

                /*if (mNearestMandi == null) {
                    params.put("nearest_mandi_name", "");
                    params.put("nearest_mandi_lat", "0");
                    params.put("nearest_mandi_log", "0");
                } else {
                    params.put("nearest_mandi_name", mNearestMandi.mandi_name_en);
                    params.put("nearest_mandi_lat", mNearestMandi.mandi_lat);
                    params.put("nearest_mandi_log", mNearestMandi.mandi_lng);
                }

                params.put("interested_mandi_list", "Chail Chowk,Ambala City(Subji Mandi),4th Mile Dimapur");
                params.put("interested_crop_list", "Pumpkin,Mousambi(Sweet Lime),Potato,Green Chilli,Banana - Green,Apple,Tomato,Wheat Atta,Paddy(Dhan)");
                //TODO
                //                params.put("interested_rss_topics", "");*/

        //System.out.println("update_farmer_profile_request: " + params.toString());
        //return params;}};
        //AppController.getInstance().addToRequestQueue(stringRequest);


        Profile params = new Profile();
        //params.setFarmerId("UKYVLUT5");
        //params.setFarmerContact("1478523691");
        params.setFarmerId(mUserId);
        params.setFarmerContact(mMobile);
        params.setFarmerName(mEditTextFullName.getText().toString());
        params.setFarmerAddress(mEditTextAddress.getText().toString());
        params.setFarmerLocation(mTextViewLocality.getText().toString());
        params.setFarmerPincode(mTextViewPinCode.getText().toString());
        params.setPreferredLanguage(primaryLanguageCode);
        params.setSecondaryLanguage(mRadioButtonBilingualYes.isSelected() ? secondaryLanguageCode : " ");
        if (mRadioButtonBilingualYes.isChecked()) {
            mStringUtils.getPref()
                    .setSecondaryLocale(mLanguageModelList.get(mSpinnerSecondaryLanguage.getSelectedItemPosition()).languageCode);
        } else {
            mStringUtils.getPref().setSecondaryLocale(null);
        }

        if (mRadioButtonBankYes.isSelected()) {
            params.setBankName(mEditTextBankName.getText().toString());
            params.setBankAccountNumber(mEditTextBankAccountNumber.getText().toString());
        } else {
            params.setBankName(" ");
            params.setBankAccountNumber(" ");
        }

        params.setHasNetBanking(mRBHasNetBankingYes.isSelected() ? "Y" : "N");
        params.setHasPaytm(mRBHasPaytmYes.isSelected() ? "Y" : "N");
        params.setHasBhim(mRBHasBhimYes.isSelected() ? "Y" : "N");

        params.setScreenSaverEnabled(mRBScreenSaverEnabledYes.isSelected() ? "Y" : "N");

        int position = mEtAvgAnnualIncome.getSelectedItemPosition();
        String theSlectedItemValue = FarmerProfileModel.getOneDigitIncome(position);

        params.setAverageAnnualIncome(Integer.parseInt(theSlectedItemValue));
        params.setLandInAcres((Integer.parseInt(mEtTotalLandAreaInAcres.getText().toString())));
        params.setPriceMeasuringUnit(mEtPriceMeasuringUnit.getSelectedItem().toString());
        params.setAreaMeasuringUnit(mEtAreaMeasuringUnit.getSelectedItem().toString());
        //System.out.println("update_farmer_profile_request: " + params.toString());

        API_Manager apiManager=new API_Manager();
        UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);
        Call<UserUpdate> call=retrofit_interface.updateprofile(mUserProfile.getApiToken(),params);
        call.enqueue(new Callback<UserUpdate>() {
            @Override
            public void onResponse(Call<UserUpdate> call, retrofit2.Response<UserUpdate> response) {
                mProgressDialog.dismiss();
                //Log.e("farmer_profile","profile response"+response.body());

                //System.out.println("update_farmer_profile_response: " + response);
                try {
                    Log.e("farmer_profile","profile response"+response.body());
                    Log.e("farmer_profile","profile response"+response.message());
                    Log.e("farmer_profile","profile response"+response.code());

                    if (response.body().getCode() == 200) {

                        mUserProfile.setUserName(mEditTextFullName.getText().toString());
                        mStringUtils.getPref().setPrimaryLocale(primaryLanguageCode);
                        if (mRadioButtonBilingualYes.isChecked()) {
                            mStringUtils.getPref().setSecondaryLocale(secondaryLanguageCode);
                        } else {
                            mStringUtils.getPref().setSecondaryLocale(null);
                        }
                        Toast.makeText(FarmerProfileActivity.this, mStringUtils.getLocalizedString(R.string.successfully_updated), Toast.LENGTH_SHORT).show();
                        onBackPressed();
//                                mStringUtils.getPref().setFarmerProfile(new Gson().toJson(response));
                    } else {
                        Toast.makeText(FarmerProfileActivity.this, mStringUtils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<UserUpdate> call, Throwable t) {
                mProgressDialog.dismiss();
                Log.d("farmer_profile","profile fail"+t.getMessage());
                if (t instanceof TimeoutError) {
                    showToast(FarmerProfileActivity.this, mStringUtils.getLocalizedString(R.string.slow_internet_kindly_check_internet_to_proceed_seamlessly).toString());
                } else if (t instanceof NoConnectionError) {
                    showToast(FarmerProfileActivity.this, mStringUtils.getLocalizedString(R.string.no_internet_connection_try_later).toString());
                }
            }
        });
    }

    private void getMandis() {
        MandiDataService mandiDataService = new MandiDataService(this);
        mandiDataService.getMandis(new MandiDataService.OnMandiResponse() {
            @Override
            public void onMandiSuccess(List<MandiModel> mandies) {
                FarmerProfileActivity.this.mMandis = mandies;
                isMandiLoaded.set(true);
                if (isCropsLoaded.get() && isMandiLoaded.get()) getProfileDetails();
            }

            @Override
            public void onMandiFailure() {

            }
        });
    }

    private void getCrops() {
        CropDataService cropDataService = new CropDataService(this);
        cropDataService.getCrops(new CropDataService.OnCropDataRequest() {
            @Override
            public void onCropDataResults(List<Crop> crops) {
                FarmerProfileActivity.this.mCrops = crops;
                isCropsLoaded.set(true);
                if (isCropsLoaded.get() && isMandiLoaded.get()) getProfileDetails();
            }

            @Override
            public void onCropDataError() {

            }
        });
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

        mGoogleApiClient.connect();

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location requests here
                        getLocation();
                        Log.e("location", "getLocation");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(FarmerProfileActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }


    private void getLocation() {
        Log.e("getLocation", "inside");
        //if (isPermissionGranted) {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                try {
                    mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    Log.e("getLocation", "inside" + mLastLocation);
                    latitude = mLastLocation.getLatitude();
                    longitude = mLastLocation.getLongitude();
                    try {
                        if (mForLatLng == false) getAddress();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (SecurityException | NullPointerException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(FarmerProfileActivity.this, "Permission Denied\n" + deniedPermissions.toString(),
                        Toast.LENGTH_SHORT).show();
                FarmerProfileActivity.super.onBackPressed();
            }
        };
        TedPermission.with(FarmerProfileActivity.this)
                .setPermissionListener(permissionlistener)
                .setRationaleTitle(mStringUtils.getLocalizedString(R.string.permissions))
                .setRationaleMessage(
                        mStringUtils.getLocalizedString(R.string.you_need_to_allow_following_permissions_for_this_app_to_work_properly))
                .setDeniedTitle("Permission denied")
                .setDeniedMessage(
                        "If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setGotoSettingButtonText("Permission")
                .setPermissions(ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION)
                .check();
    }

    public Address getAddress(double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.ENGLISH);

        try {
            addresses = geocoder.getFromLocation(latitude, longitude,
                    1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            return addresses.get(0);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void getAddress() {
        Log.e("getAddress", "inside");
        Address locationAddress = getAddress(latitude, longitude);
        try {
            if (locationAddress != null) {
                String address = locationAddress.getAddressLine(0);
                String address1 = locationAddress.getAddressLine(1);
                String city = locationAddress.getLocality();
                String state = locationAddress.getAdminArea();
                String country = locationAddress.getCountryName();
                String postalCode = locationAddress.getPostalCode();
                mEditTextAddress.setText(address);
                if (city != null && TextUtils.isEmpty(mTextViewLocality.getText()))
                    mTextViewLocality.setText(city);
                if (postalCode != null && TextUtils.isEmpty(mTextViewPinCode.getText()))
                    mTextViewPinCode.setText(postalCode);

                mTextViewLocality.setEnabled(false);
                mTextViewPinCode.setEnabled(false);

                String currentLocation;

                if (!TextUtils.isEmpty(address)) {
                    currentLocation = address;

                    if (!TextUtils.isEmpty(address1)) {

                        currentLocation += "\n" + address1;
                    }

                    if (!TextUtils.isEmpty(city)) {
                        currentLocation += "\n" + city;

                        if (!TextUtils.isEmpty(postalCode)) {
                            currentLocation += " - " + postalCode;
                        }
                    } else {
                        if (!TextUtils.isEmpty(postalCode)) {

                            currentLocation += "\n" + postalCode;
                        }
                    }

                    if (!TextUtils.isEmpty(state)) {
                        currentLocation += "\n" + state;
                    }
                    if (!TextUtils.isEmpty(country)) {
                        currentLocation += "\n" + country;
                    }
                    Log.e("getAddress", "currentLocation" + currentLocation);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection Failed", Toast.LENGTH_SHORT).show();
    }

    private boolean isValid() {
        boolean isValid = true;
        if (TextUtils.isEmpty(mTextViewLocality.getText()) && TextUtils.isEmpty(mTextViewPinCode.getText())) {
            showToast(FarmerProfileActivity.this, mStringUtils.getLocalizedString(R.string.please_enter_city_or_pincode).toString());
            isValid = false;
        } else if (mRadioButtonBilingualYes.isSelected() && mSpinnerSecondaryLanguage.getSelectedItemPosition() == mSpinnerPrimaryLanguage.getSelectedItemPosition()) {
            showToast(FarmerProfileActivity.this, mStringUtils.getLocalizedString(R.string.preferred_language_and_secondary_language_cannot_be_same).toString());
            isValid = false;
        } else if (mRadioButtonBankYes.isSelected() &&
                (TextUtils.isEmpty(mEditTextBankName.getText().toString()) || TextUtils.isEmpty(mEditTextBankAccountNumber.getText().toString()))) {
            showToast(FarmerProfileActivity.this, mStringUtils.getLocalizedString(R.string.enter_bank_details).toString());
            isValid = false;
        }
        return isValid;
    }

}
