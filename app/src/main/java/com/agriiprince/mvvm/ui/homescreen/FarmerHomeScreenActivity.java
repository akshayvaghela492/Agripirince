package com.agriiprince.mvvm.ui.homescreen;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.BuildConfig;
import com.agriiprince.activities.TutorialActivity;
import com.agriiprince.activities.oe.OeInfoActivity;
import com.agriiprince.db.DatabaseManager;
import com.agriiprince.R;
import com.agriiprince.activities.CommunicateToAP;
import com.agriiprince.mvvm.model.User;
import com.agriiprince.mvvm.retrofit.dto.tutorial.TutorialResponse;
import com.agriiprince.mvvm.retrofit.model.login.Profile;
import com.agriiprince.mvvm.retrofit.model.tutorial.SubSection;
import com.agriiprince.mvvm.retrofit.service.Tutorial;
import com.agriiprince.mvvm.ui.login.LoginActivity;
import com.agriiprince.mvvm.ui.registration.FarmerInfoActivity;
import com.agriiprince.mvvm.ui.registration.FarmerProfileActivity;
import com.agriiprince.activities.TrackYourCropFarmer;
import com.agriiprince.adapter.ViewPagerAdapter;
import com.agriiprince.mvvm.applevel.analytics.FarmerRealtimeDBAnalytics;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.fragments.InfoTabFragment;
import com.agriiprince.fragments.FarmerCaFragment;
import com.agriiprince.fragments.NotificationFragment;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.SelectLanguageModel;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.login.Logout;
import com.agriiprince.mvvm.retrofit.dto.profile.UserUpdate;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.mvvm.retrofit.test.TestRetrofit;
import com.agriiprince.mvvm.ui.splash.SplashActivity;
import com.agriiprince.mvvm.ui.subscription.SubscriptionActivity;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.utils.StringUtils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_SMS;
import static com.agriiprince.appcontroller.AppController.gTutorialSectionsHi;
import static com.agriiprince.appcontroller.AppController.loggedOut;
import static com.agriiprince.mvvm.applevel.analytics.FarmerFirebaseAnalytics.logEventFarmerClicked;
import static com.agriiprince.mvvm.applevel.analytics.FarmerFirebaseAnalytics.logUserId;

public class FarmerHomeScreenActivity extends FarmerHomeScreen implements
        NavigationView.OnNavigationItemSelectedListener {

    private UserProfile mUserProfile;
    PrefManager mPrefmanager;

    private ViewPager mViewPager;

    private ViewPagerAdapter viewPagerAdapter;

    StringUtils utils;
    DatabaseManager mDatabase;
    FirebaseDatabase mFDatabase;
    DatabaseReference mRef;
    DatabaseReference mFarmersRef;
    DatabaseReference mNumRef;
    DatabaseReference mKeyRef;
    long mTimeStart;
    String mUserId;
    String mMobile;
    String mName;
    String mCity;
    String mPin;
    Context mContext;
    private int selectedTab = 1;

    private LinearLayout infoTab;
    private LinearLayout homeTab;
    private LinearLayout notificationTab;
    private LinearLayout languageTab;
    private int MY_PERMISSIONS_REQUEST_SEND_SMS = 1111;

    @Override
    public void onChangeLabel(@NonNull String label) {
        if (!TextUtils.isEmpty(label)) {
            setTitleLabel(label);

        } else {
            setTitleLabel();
        }
    }

    @Override
    public void onSelectNotification() {
        notificationTab.performClick();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_home_screen);

        TestRetrofit retrofit=new TestRetrofit();
        retrofit.testing();

        // ANALYTICS DATABASES

        mPrefmanager = new PrefManager(this);

        mFDatabase = FirebaseDatabase.getInstance();
        mRef = mFDatabase.getReference("analytics");
        mFarmersRef = mRef.child("farmers");

        //------------------------

        mUserProfile = new UserProfile(this);
        mUserId = mUserProfile.getUserId();
        mMobile = mUserProfile.getUserMobile();
        mName = mUserProfile.getUserName();
        mCity = mUserProfile.getUserCity();
        mPin = mUserProfile.getUserPincode();
        mContext = getApplicationContext();

        utils = AppController.getInstance().getStringUtils();
        mDatabase = new DatabaseManager(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(utils.getLocalizedString(R.string.home));

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        TextView textView = navigationView.findViewById(R.id.nav_logout);
        textView.setText(utils.getLocalizedString(R.string.log_out));
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fnLogout();
            }
        });

        View navigationHeader = navigationView.getHeaderView(0);

        TextView profileName = navigationHeader.findViewById(R.id.nav_profile_name);
        profileName.setText(mPrefmanager.getUserName());
        TextView profileNumber = navigationHeader.findViewById(R.id.nav_profile_mobile);
        profileNumber.setText(mUserProfile.getUserMobile());

        //navigationHeader.setOnClickListener(navigationHeaderClick);

        mViewPager = findViewById(R.id.pager);

        setupBottomNavBar();
        setUpViewPager(mViewPager);

        setTexts();

        // ANALYTICS DATABASES

        mNumRef = mFarmersRef.child(mMobile);
        mNumRef.child("_id").setValue(mUserId);
        mNumRef.child("_name").setValue(mName);
        mNumRef.child("_city").setValue(mCity);
        mNumRef.child("_pin").setValue(mPin);

        logUserId(mContext, mMobile);
        logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "entered_homescreen");

        //------------------------

        ImageView imgview_help =  findViewById(R.id.iv_help);

        imgview_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentByTag("testID");
                if (test != null && test.isVisible()) {
                    //DO STUFF
                }
                else {
                    //Whatever
                }*/

                //viewPagerAdapter.getPageTitle(mViewPager.getCurrentItem());
                int pos = mViewPager.getCurrentItem();
                Log.e("Currrent Position: ", " -----> " + pos);

                if(pos == 0)
                {
                    Intent intent = new Intent(FarmerHomeScreenActivity.this, TutorialActivity.class);
                    intent.putExtra("code", "INFOT");
                    startActivity(intent);
                }
                else if(pos == 2)
                {
                    Intent intent = new Intent(FarmerHomeScreenActivity.this, TutorialActivity.class);
                    intent.putExtra("code", "NOTIF");
                    startActivity(intent);
                }
                else
                {
                    FarmerCaFragment caFragment = (FarmerCaFragment) getSupportFragmentManager().findFragmentByTag(FarmerCaFragment.class.getSimpleName());
                    if (caFragment != null && caFragment.isVisible()) {
                        Intent intent = new Intent(FarmerHomeScreenActivity.this, TutorialActivity.class);
                        intent.putExtra("code", "COMMAD");
                        startActivity(intent);
                    }
                    else {
                        Intent intent = new Intent(FarmerHomeScreenActivity.this, TutorialActivity.class);
                        intent.putExtra("code", "HOMES");
                        startActivity(intent);
                    }
                }

            }
        });

        //getTutorialEn();
        //getTutorialHi();

        askForSubscription();
    }


    private void setTexts() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        Menu menu = navigationView.getMenu();

        menu.findItem(R.id.nav_subscription).setTitle(utils.getLocalizedString(R.string.subscription, true).toString());
        menu.findItem(R.id.nav_profile).setTitle(utils.getLocalizedString(R.string.profile, true).toString());
        menu.findItem(R.id.nav_refer_friend).setTitle(utils.getLocalizedString(R.string.refer_a_friend, true).toString());
        menu.findItem(R.id.nav_contact_us).setTitle(utils.getLocalizedString(R.string.contact_us, true).toString());
        menu.findItem(R.id.nav_terms_conditions).setTitle(utils.getLocalizedString(R.string.terms_and_condition, true).toString());
    }


    private void setUpViewPager(ViewPager viewPager) {

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new InfoTabFragment(), utils.getLocalizedString(R.string.information_tab).toString());
        viewPagerAdapter.addFragment(new HomeFragment(), utils.getLocalizedString(R.string.home).toString());
        viewPagerAdapter.addFragment(NotificationFragment.newInstance(), utils.getLocalizedString(R.string.notification).toString());

        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(1);
        viewPager.setOffscreenPageLimit(2);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTimeStart = System.currentTimeMillis();
    }

    @Override
    protected void onStop() {
        super.onStop();
        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - mTimeStart;
        final int elapsedSeconds = (int)(tDelta / 1000.0);

        Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    //FarmerRealtimeDBAnalytics.saveEventFarmerTimeSpent(       CREATING CRASH - LOOK LATER
                            //dataSnapshot, "time_spent_homescreen", mNumRef, elapsedSeconds);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    View.OnClickListener navigationHeaderClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            openUserProfile();
        }
    };

    private void openUserProfile() {
        Intent i = new Intent(this, FarmerProfileActivity.class);
        startActivity(i);
    }


    private void setupBottomNavBar() {
        infoTab = findViewById(R.id.farmer_nav_bar_info_tab);
        homeTab = findViewById(R.id.farmer_nav_bar_home);
        notificationTab = findViewById(R.id.farmer_nav_bar_notification);
        languageTab = findViewById(R.id.farmer_nav_bar_language);

        TextView info = findViewById(R.id.text_info_tab);
        info.setText(utils.getLocalizedString(R.string.infotab));
        TextView home = findViewById(R.id.text_home);
        home.setText(utils.getLocalizedString(R.string.home));
        TextView notification = findViewById(R.id.text_notification);
        notification.setText(utils.getLocalizedString(R.string.notification));
        TextView language = findViewById(R.id.text_language);
        language.setText(utils.getLocalizedString(R.string.language));

        final ValueEventListener vl = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                            dataSnapshot, "click_bottom_infotab", mNumRef);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        };

        final ValueEventListener v2 = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                            dataSnapshot, "click_bottom_home", mNumRef);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        final ValueEventListener v3 = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                            dataSnapshot, "click_bottom_notification", mNumRef);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        final ValueEventListener v4 = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                            dataSnapshot, "click_bottom_language", mNumRef);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        infoTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
                q.addListenerForSingleValueEvent(vl);

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_bottom_infotab");

                mViewPager.setCurrentItem(0);
                clearBackStack();
            }
        });


        homeTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
                q.addListenerForSingleValueEvent(v2);

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_bottom_home");

                mViewPager.setCurrentItem(1);
                clearBackStack();

            }
        });

        notificationTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
                q.addListenerForSingleValueEvent(v3);

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_bottom_notification");

                mViewPager.setCurrentItem(2);
                clearBackStack();
            }
        });

        languageTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
                q.addListenerForSingleValueEvent(v4);

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_bottom_language");

                showBilingualSelectionDialog();
            }
        });
    }

    private void clearBackStack() {
        FragmentManager fragmentManager = getSupportFragmentManager();

        for (int i = 0; i < fragmentManager.getBackStackEntryCount(); i++) {
            fragmentManager.popBackStack();
        }
    }

    private void setTitleLabel() {
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(viewPagerAdapter.getPageTitle(mViewPager.getCurrentItem()));
    }

    private void setTitleLabel(String label) {
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(label);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String permissions[], @NonNull final int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getApplicationContext(), utils.getLocalizedString(R.string.permission_granted_now_you_can_proceed), Toast.LENGTH_LONG).show();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CALL_PHONE)) {
                                showAlertDialog(utils.getLocalizedString(R.string.you_need_to_allow_access_to_the_permissions).toString(),
                                        "OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermission(permissions[0]);
                                                }
                                            }
                                        },
                                        "Cancel", null, true);
                                return;
                            }
                        }
                    }
                }
                break;
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }

        if (isCaSearchMandiVisible) {
            FarmerCaFragment farmerCaFragment = (FarmerCaFragment) getSupportFragmentManager()
                    .findFragmentByTag(FarmerCaFragment.class.getSimpleName());

            if (farmerCaFragment != null) farmerCaFragment.closeSearchMandi();

        } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onBackPressed();
        } else if (mViewPager.getCurrentItem() != 1) {
            mViewPager.setCurrentItem(1);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.farmer_home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }


    private void askForSubscription() {
        if (mUserProfile.isNewUser()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle(utils.getLocalizedString(R.string.subscription));
            alertDialog.setMessage(utils.getLocalizedString(R.string.dialog_activate_subscription));
            alertDialog.setPositiveButton(utils.getLocalizedString(R.string.btn_ok), onClickSubscribe);
            alertDialog.setNegativeButton(utils.getLocalizedString(R.string.later), null);
            alertDialog.show();
            mUserProfile.setNewUser(false);
        }
    }

    private DialogInterface.OnClickListener onClickSubscribe = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            startActivity(new Intent(FarmerHomeScreenActivity.this, SubscriptionActivity.class));
        }
    };

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_subscription) {

            Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                dataSnapshot, "click_nav_subscription", mNumRef);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });

            logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_nav_subscription");

            startActivity(new Intent(FarmerHomeScreenActivity.this, SubscriptionActivity.class));

        } else if (id == R.id.nav_profile) {

            Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                dataSnapshot, "click_nav_profile", mNumRef);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });

            logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_nav_profile");

            if(mPrefmanager.getFilledDetails())
                openUserProfile();
            else
            {
                        if (BuildConfig.USER_TYPE.equals(User.Type.FARMER)) {
                            Intent intent = new Intent(this, FarmerInfoActivity.class);
                            intent.putExtra(FarmerInfoActivity.MOBILE_NUMBER, mUserProfile.getUserMobile());
                            intent.putExtra(FarmerInfoActivity.PASSWORD, mUserProfile.getUserPassword());
                            Logs.d("HomeScreenValues", mUserProfile.getUserMobile() + " " + mUserProfile.getUserPassword());
                            startActivity(intent);
                        } else if (BuildConfig.USER_TYPE.equals(User.Type.OPERATIVE_EXECUTIVE)) {
                            Intent intent = new Intent(this, OeInfoActivity.class);
                            intent.putExtra(OeInfoActivity.MOBILE_NUMBER, mUserProfile.getUserMobile());
                            intent.putExtra(OeInfoActivity.PASSWORD, mUserProfile.getUserPassword());
                            startActivity(intent);
                        }
            }

        } else if (id == R.id.nav_refer_friend) {

            Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                dataSnapshot, "click_nav_refer_friend", mNumRef);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });

            logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_nav_refer_friend");

            invite();

        } else if (id == R.id.nav_contact_us) {

            Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                dataSnapshot, "click_nav_contact_us", mNumRef);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });

            logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_nav_contact_us");

            showAlertForContactUs();

        } else if (id == R.id.nav_terms_conditions) {

            Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                dataSnapshot, "click_nav_terms_conditions", mNumRef);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });

            logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_nav_terms_conditions");

            termAndCondition();
        } else if (id == R.id.nav_track_crop) {
            startActivity(new Intent(FarmerHomeScreenActivity.this, TrackYourCropFarmer.class));
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void termAndCondition() {
        String url = Config.TERMS_AND_CONDITION;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }


    private void invite() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_op_exec, null);

        final EditText sendMobileNo = dialogView.findViewById(R.id.enter_number);
        final TextView messageFree = dialogView.findViewById(R.id.refer_message);
        messageFree.setText(utils.getLocalizedString(R.string.refer_free_subscription));
        builder.setView(dialogView)
                .setTitle(utils.getLocalizedString(R.string.refer_a_friend))
                .setMessage(utils.getLocalizedString(R.string.enter_mobile_no))
                .setPositiveButton(utils.getLocalizedString(R.string.send), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //apk link sent to mobile no.
                        if (TextUtils.isEmpty(sendMobileNo.getText()) || sendMobileNo.getText().length() < 10) {
                            Toast.makeText(FarmerHomeScreenActivity.this, utils.getLocalizedString(R.string.enter_valid_number),
                                    Toast.LENGTH_SHORT).show();
                        } else {

                            String message = getString(R.string.hey_there_i_am_using_agriprince_app) + "\n" +
                                    getString(R.string.download_app_from_link) + " " + Config.APP_LINK_FARMER;

                            sendSMS(sendMobileNo.getText().toString(), message);
                            uploadReferredNumber(sendMobileNo.getText().toString());
                        }
                    }
                })
                .setNegativeButton(utils.getLocalizedString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                })
                .create()
                .show();
    }


    private void uploadReferredNumber(final String number) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.POST_UPDATE_REFERREL_NUMBER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY", "referred:   " + response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());
                params.put("user_id", mUserProfile.getUserId());
                params.put("user_type", mUserProfile.getUserType());
                params.put("referred_number", number);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUserProfile.getApiToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void sendSMS(String phoneNumber, String message) {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.SEND_SMS, READ_SMS, RECEIVE_SMS},
                    MY_PERMISSIONS_REQUEST_SEND_SMS);

            return;
        }

        // Intent Filter Tags for SMS SEND and DELIVER
        String strSmsSent = "SMS_SENT";
        String strSmsDelivered = "SMS_DELIVERED";

        // Send PendingIntent
        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                strSmsSent), 0);

        // Deliver PendingIntent
        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(strSmsDelivered), 0);

        // Send BroadcastReceiver
        BroadcastReceiver sendSMS = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), utils.getLocalizedString(R.string.sms_sent),
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };

        // Delivery BroadcastReceiver
        BroadcastReceiver deliverSMS = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), utils.getLocalizedString(R.string.sms_delivered),
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), utils.getLocalizedString(R.string.sms_not_delivered),
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };

        // Notify when the SMS has been sent
        registerReceiver(sendSMS, new IntentFilter(strSmsSent));

        // Notify when the SMS has been delivered
        registerReceiver(deliverSMS, new IntentFilter(strSmsDelivered));

        SmsManager sms = SmsManager.getDefault();
        //smsManager.sendTextMessage("numbersToSend,"sourceAddress","Message","intent sent","intent delivered")
//        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);

        ArrayList<String> parts = sms.divideMessage(message);

        sms.sendMultipartTextMessage(phoneNumber, null, parts, null, null);

    }


    private void showAlertForContactUs() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setMessage(utils.getLocalizedString(R.string.contact_us_detail));
        dialogBuilder.setPositiveButton(utils.getLocalizedString(R.string.send_message), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(FarmerHomeScreenActivity.this, CommunicateToAP.class));
            }
        });
        dialogBuilder.setNegativeButton(utils.getLocalizedString(R.string.dismiss), null);
        AlertDialog alert = dialogBuilder.create();
        alert.show();
    }

    private void updateLanguageConfig() {
        Locale locale = new Locale(utils.getPref().getLocale());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        Resources resources = getResources();
        resources.updateConfiguration(config, resources.getDisplayMetrics());
    }

    private void showBilingualSelectionDialog() {
        final List<SelectLanguageModel> mLanguageModelList = new ArrayList<>();
        SelectLanguageModel model = new SelectLanguageModel();
        model.languageTitle = "English";
        model.languageLetter = "E";
        model.languageCode = "en";
        mLanguageModelList.add(model);
        model = new SelectLanguageModel();
        model.languageTitle = "हिंदी";
        model.languageLetter = "हि";
        model.languageCode = "hi";
        mLanguageModelList.add(model);
        model = new SelectLanguageModel();
        model.languageTitle = "ಕನ್ನಡ";
        model.languageLetter = "ಕ";
        model.languageCode = "kn";
        mLanguageModelList.add(model);

        PrefManager prefManager = new PrefManager(this);
        String primaryLocale = prefManager.getLocale();

        List<String> languages = new ArrayList<>();

        if (primaryLocale.equalsIgnoreCase("en")) {
            for (SelectLanguageModel selectLanguageModel : mLanguageModelList) {
                if (!selectLanguageModel.languageCode.equalsIgnoreCase(primaryLocale)) {
                    languages.add(selectLanguageModel.languageTitle);
                }
            }
        } else {
            for (SelectLanguageModel selectLanguageModel : mLanguageModelList) {
                if (selectLanguageModel.languageCode.equalsIgnoreCase("en")) {
                    languages.add(selectLanguageModel.languageTitle);
                }
            }
        }

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_for_bilingual_selection, null);
        ((TextView) dialogView.findViewById(R.id.text_second)).setText(utils.getLocalizedString(R.string.select_second_language_to_show));
        ((Button) dialogView.findViewById(R.id.btnDisable)).setText(utils.getLocalizedString(R.string.disable));
        ((Button) dialogView.findViewById(R.id.btnSubmit)).setText(utils.getLocalizedString(R.string.submit));

        dialogBuilder.setView(dialogView);
        final AlertDialog alert = dialogBuilder.create();
        final Spinner spinnerLanguages = dialogView.findViewById(R.id.spinner_language_list);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, languages);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLanguages.setAdapter(adapter);

        dialogView.findViewById(R.id.btnDisable).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                utils.getPref().setSecondaryLocale(null);
                updateLanguageSelection(" ");
                refreshScreen();
            }
        });

        dialogView.findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String selectedLanguage = (String) spinnerLanguages.getSelectedItem();

                for (SelectLanguageModel languageModel : mLanguageModelList) {
                    if (languageModel.languageTitle.equalsIgnoreCase(selectedLanguage)) {
                        utils.getPref().setSecondaryLocale(languageModel.languageCode);
                        updateLanguageSelection(languageModel.languageCode);
                        break;
                    }
                }
            }
        });
        alert.show();
    }

    private void refreshScreen() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }


    private void updateLanguageSelection(final String secondaryLanguage) {

        Profile params = new Profile();

        params.setFarmerId(mUserProfile.getUserId());
        params.setFarmerContact(mMobile);
        params.setSecondaryLanguage(secondaryLanguage);

        API_Manager apiManager=new API_Manager();
        UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);
        Call<UserUpdate> call=retrofit_interface.updateprofile(mUserProfile.getApiToken(),params);
        call.enqueue(new Callback<UserUpdate>() {
            @Override
            public void onResponse(Call<UserUpdate> call, retrofit2.Response<UserUpdate> response) {
                refreshScreen();
                Log.d("farmer_profile","home response"+response.body());
            }
            @Override
            public void onFailure(Call<UserUpdate> call, Throwable t) {
               t.printStackTrace();
                Log.d("farmer_profile","home  fail"+ t.getMessage());
            }
        });
    }

    API_Manager apiManager=new API_Manager();
    UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);

    private void fnLogout() {
        showProgressDialog();
        mDatabase.deleteInterestedCropEntries();
        //local server hosted on my laptop

            Call<Logout> call = retrofit_interface.logout(mUserProfile.getUserMobile(),mUserProfile.getUserId(),mUserProfile.getUserType());
            call.enqueue(new Callback<Logout>() {
                @Override
                public void onResponse(Call<Logout> call, retrofit2.Response<Logout> response) {
                    Logs.d("CheckStatus", "onResponse: logout Success");
                    if (response.isSuccessful() && response.body() != null) {
                        Logs.d("CheckStatus",response.body().getStatus().toString());
                        Logs.d("CheckStatus",response.body().getMessage());

                        mPrefmanager.logOut();
                        mUserProfile.logOut();
                        loggedOut = 1;

                        Intent intent = new Intent(FarmerHomeScreenActivity.this, SplashActivity.class);

                        /*Intent intent = new Intent(FarmerHomeScreenActivity.this, LoginActivity.class);
                        intent.putExtra("WHO_CALLED", "Logout");*/
                        startActivity(intent);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<Logout> call, Throwable t) {
                    Log.d("CheckStatus", "fail entry");
                    dismissProgressDialog();
                    Toast.makeText(FarmerHomeScreenActivity.this,
                            utils.getLocalizedString(R.string.sorry_something_went_wrong_unable_to_logout_currently_please_try_after_sometime),
                            Toast.LENGTH_LONG).show();
                }
            });
    }

    private void showAlertDialog(String message, String positiveButton, DialogInterface.OnClickListener positiveButtonClickListener,
                                 String negativeButton, DialogInterface.OnClickListener negativeButtonClickListener,
                                 boolean isCancellable) {
        android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(this);
        alert.setCancelable(isCancellable);
        alert.setMessage(message)
                .setPositiveButton(positiveButton, positiveButtonClickListener)
                .setNegativeButton(negativeButton, negativeButtonClickListener)
                .create()
                .show();
    }

    public DatabaseReference getmFarmersRef() {
        return mFarmersRef;
    }

    public DatabaseReference getmNumRef() {
        return mNumRef;
    }

    public UserProfile getmUserProfile() {
        return mUserProfile;
    }
    
}
