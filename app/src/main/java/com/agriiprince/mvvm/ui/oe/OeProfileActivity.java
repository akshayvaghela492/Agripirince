package com.agriiprince.mvvm.ui.oe;

import android.app.ProgressDialog;
import android.content.IntentSender;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.mvvm.ui.registration.adapters.InterestedCropListAdapter;
import com.agriiprince.mvvm.ui.registration.adapters.InterestedMandisAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.ActivityOeprofileBinding;
import com.agriiprince.dataservice.CropDataService;
import com.agriiprince.dataservice.MandiDataService;
import com.agriiprince.mvvm.ui.registration.AddInterestedCropFragment;
import com.agriiprince.mvvm.ui.registration.infostepsfragments.InfoCropsFragment;
import com.agriiprince.mvvm.ui.registration.infostepsfragments.InfoMandiInterestedFragment;
import com.agriiprince.helper.UserLocation;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.MandiModel;
import com.agriiprince.model.OEProfileModel;
import com.agriiprince.model.SelectLanguageModel;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.Utils;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.agriiprince.utils.Utils.showToast;

public class OeProfileActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        InfoMandiInterestedFragment.OnInfoMandiInteractionListener, AddInterestedCropFragment.OnInfoCropsInteractionListener {

    private static final String TAG = OeProfileActivity.class.getSimpleName();

    public static final int LIST_DISPLAY_SIZE = 6;

    private List<SelectLanguageModel> mLanguageModelList = new ArrayList<>();

    private ProgressDialog mProgressDialog;

    private GoogleApiClient mGoogleApiClient;
    private double latitude, longitude;

    private StringUtils mStringUtils;

    private UserProfile mUserProfile;


    private AtomicBoolean isMandiLoaded;
    private AtomicBoolean isCropsLoaded;


    private String primaryLanguageCode;
    private String secondaryLanguageCode;

    List<MandiModel> mMandis;
    List<Crop> mCrops;
    List<String> interestedMandiNameList = new ArrayList<>();
    List<String> interestedMandiIdList = new ArrayList<>();
    List<String> interestedCropIdList = new ArrayList<>();

    private InterestedMandisAdapter mInterestedMandisAdapter;
    private InterestedCropListAdapter mInterestedCropsAdapter;

    private Location mLocaion;

    private final static int REQUEST_CHECK_SETTINGS = 2000;

    private ActivityOeprofileBinding mBinding;

    private final String DEFAULT_LANGUAGE_CODE = "en";
    private final String DEFAULT_LANGUAGE = "English";


    @Override
    public void onInfoMandiInterestedSuccess(String nearestMandi, List<String> selectedMandies) {
        Log.d(TAG, "onInfoMandiInterestedSuccess: " + selectedMandies);
        setMandiList(TextUtils.join(",", selectedMandies));
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onInfoCropSuccess(List<String> ids) {
        Log.d(TAG, "onInfoCropSuccess: " + ids);
        setCropList(TextUtils.join(",", ids));
        getSupportFragmentManager().popBackStack();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_oeprofile);
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mUserProfile = new UserProfile(this);
        mStringUtils = AppController.getInstance().getStringUtils();
        mLocaion = new Location("user");

        isMandiLoaded = new AtomicBoolean(false);
        isCropsLoaded = new AtomicBoolean(false);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getResources().getString(R.string.please_wait));

        mInterestedMandisAdapter = new InterestedMandisAdapter();
        mInterestedCropsAdapter = new InterestedCropListAdapter(LIST_DISPLAY_SIZE, false, this);

        mBinding.rvInterestedCrops.setLayoutManager(new GridLayoutManager(this, 2));
        mBinding.rvInterestedCrops.setAdapter(mInterestedCropsAdapter);
        mBinding.rvInterestedMandis.setLayoutManager(new GridLayoutManager(this, 2));
        mBinding.rvInterestedMandis.setAdapter(mInterestedMandisAdapter);

        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setTitle(mStringUtils.getLocalizedString(R.string.update_profile));

        mBinding.base.tvCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buildGoogleApiClient();
            }
        });

        List<String> languageNamesList = new ArrayList<>();
        SelectLanguageModel model = new SelectLanguageModel();
        model.languageTitle = "English";
        model.languageLetter = "E";
        model.languageCode = "en";
        mLanguageModelList.add(model);
        languageNamesList.add(model.languageTitle);
        model = new SelectLanguageModel();
        model.languageTitle = "हिंदी";
        model.languageLetter = "हि";
        model.languageCode = "hi";
        mLanguageModelList.add(model);
        languageNamesList.add(model.languageTitle);
        model = new SelectLanguageModel();
        model.languageTitle = "ಕನ್ನಡ";
        model.languageLetter = "ಕ";
        model.languageCode = "kn";
        mLanguageModelList.add(model);
        languageNamesList.add(model.languageTitle);

        mBinding.base.rbBilingualYes.setEnabled(false);
        mBinding.base.rbBilingualNo.setEnabled(false);
        mBinding.base.spinnerPrimaryLangauge.setEnabled(false);
        mBinding.base.spinnerSecondLanguage.setEnabled(false);


        getMandis();
        getCrops();

        findViewById(R.id.button_update_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateValues(mBinding.base.etFullName.getText().toString(),mBinding.base.tvMobile.getText().toString(),mBinding.base.etAddress.getText().toString(), mBinding.base.tvLocality.getText().toString(), mBinding.base.tvPincode.getText().toString()))
                    updateProfile();
            }
        });

        mBinding.base.rgBilingual.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_bilingual_yes:
                        mBinding.base.llBilingual.setVisibility(View.VISIBLE);
                        setSecondaryLanguageAdapter(secondaryLanguageCode);
                        break;
                    case R.id.rb_bilingual_no:
                        mBinding.base.llBilingual.setVisibility(View.GONE);
                        break;
                }
            }
        });

        mBinding.tvModifyInterestedMandis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double lat;
                double lng;

                if (mLocaion != null) {
                    lat = mLocaion.getLatitude();
                    lng = mLocaion.getLongitude();
                } else {
                    lat = 0;
                    lng = 0;
                }

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(android.R.id.content,
                        InfoMandiInterestedFragment.newInstance("-1",
                                (ArrayList<String>) interestedMandiIdList, lat, lng, false, true
                        ));
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        mBinding.tvModifyInterestedCrops.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(android.R.id.content, InfoCropsFragment.newInstance(false, true));
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        setTextFields();

        getUserLocation();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTextFields() {
        mBinding.base.generalInfo.setText(mStringUtils.getLocalizedString(R.string.general_info));
        mBinding.base.address.setText(mStringUtils.getLocalizedString(R.string.address));
        mBinding.base.other.setText(mStringUtils.getLocalizedString(R.string.other));
        mBinding.base.tvCurrentLocation.setText(mStringUtils.getLocalizedString(R.string.use_current_location));

        mBinding.base.fullNameInput.setHint(mStringUtils.getLocalizedString(R.string.enter_full_name, true));
        mBinding.base.mobileInput.setHint(mStringUtils.getLocalizedString(R.string.mobile, true));
        mBinding.base.addressInput.setHint(mStringUtils.getLocalizedString(R.string.enter_address, true));
        mBinding.base.localityInput.setHint(mStringUtils.getLocalizedString(R.string.locality_city, true));
        mBinding.base.pincodeInput.setHint(mStringUtils.getLocalizedString(R.string.pincode, true));

        mBinding.base.prefLan.setText(mStringUtils.getLocalizedString(R.string.preferred_language));
        mBinding.base.enableSecondLanguage.setText(mStringUtils.getLocalizedString(R.string.enable_second_language));

        mBinding.base.selectSecondLanguage.setText(mStringUtils.getLocalizedString(R.string.select_second_language));

        mBinding.interestedMandiLabel.setText(mStringUtils.getLocalizedString(R.string.interested_mandis));
        mBinding.interestedCropLabel.setText(mStringUtils.getLocalizedString(R.string.interested_crops));

        mBinding.tvModifyInterestedMandis.setText(mStringUtils.getLocalizedString(R.string.add_modify_interests));
        mBinding.tvModifyInterestedCrops.setText(mStringUtils.getLocalizedString(R.string.add_modify_interests));
    }

    private void getUserLocation() {
        UserLocation userLocation = new UserLocation(this);
        userLocation.getLastKnowLocation(new UserLocation.OnUserLocationResults() {
            @Override
            public void onLocationResult(Location lastLocation) {
                OeProfileActivity.this.mLocaion = lastLocation;
            }

            @Override
            public void onLocationFail() {
            }
        });
    }


    private void getProfileDetails() {

        mProgressDialog.show();
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST, Config.GET_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        Log.d(TAG, "TESTING_VOLLEY" + response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            //JSONArray responseArray = new JSONArray(response);
                            //JSONObject responseObject = responseArray.getJSONObject(0);
                            //int errorCode = responseObject.getInt("error_code");
                            int errorCode = jsonObject.getInt("code");
                            //if (errorCode == 100)
                            if (errorCode == 200)
                            {
                                //String data = responseObject.getString("user_details");
                                JSONObject dataobj = jsonObject.getJSONObject("data");
                                Log.d("OEProfileModel", " ----> " + dataobj.toString());

                                String data = dataobj.getString("user_details");

                                Log.d("OEProfileModel", " ----> " + data.toString());
                                OEProfileModel profile = new Gson().fromJson(data, new TypeToken<OEProfileModel>() {}.getType());

                                Log.d("OEProfileModel", " ----> " + profile.oe_name);
                                Log.d("OEProfileModel", " ----> " + profile.pincode);

                                setProfileData(profile);
                                mBinding.base.rbBilingualYes.setEnabled(true);
                                mBinding.base.rbBilingualNo.setEnabled(true);
                                mBinding.base.spinnerPrimaryLangauge.setEnabled(true);
                                mBinding.base.spinnerSecondLanguage.setEnabled(true);
                            } else {
                                Toast.makeText(OeProfileActivity.this, mStringUtils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();
                        Utils.showVolleyError(OeProfileActivity.this, error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());
                params.put("user_id", mUserProfile.getUserId());
                params.put("user_type", mUserProfile.getUserType());
                System.out.println("Get profile body: " + params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> header = new HashMap<>();
                header.put("api_token", mUserProfile.getApiToken());
                return header;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);

    }

    private String getLanguageCodeByTitle(String title) {
        for (SelectLanguageModel model : mLanguageModelList)
            if (model.languageTitle.equals(title))
                return model.languageCode;

        return DEFAULT_LANGUAGE_CODE;
    }

    private String getLanguageTitleByCode(String code) {
        for (SelectLanguageModel model : mLanguageModelList)
            if (model.languageCode.equals(code))
                return model.languageTitle;

        return DEFAULT_LANGUAGE;
    }

    private List<String> getPrimaryLanguageTitles() {
        List<String> languages = new ArrayList<>();
        for (SelectLanguageModel model : mLanguageModelList)
            languages.add(model.languageTitle);

        return languages;
    }

    private List<String> getSecondaryLanguageTitles() {
        List<String> languages = new ArrayList<>();

        // if primary language is english then user can select any other languages
        if (primaryLanguageCode.equalsIgnoreCase("en")) {
            for (SelectLanguageModel model : mLanguageModelList)
                if (!model.languageCode.equals(primaryLanguageCode))
                    languages.add(model.languageTitle);
        } else {
            // if primary language is not english then user can only select english
            for (SelectLanguageModel model : mLanguageModelList) {
                if (model.languageCode.equals("en")) {
                    languages.add(model.languageTitle);
                    break;
                }
            }
        }

        return languages;
    }

    private void setPrimaryLanguageAdapter(String selectedLanguageCode) {
        List<String> languages = getPrimaryLanguageTitles();
        String title = getLanguageTitleByCode(selectedLanguageCode);

        int index = 0;
        for (int i = 0; i < languages.size(); i++) {
            if (languages.get(i).equals(title)) {
                index = i;
                break;
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, languages);
        mBinding.base.spinnerPrimaryLangauge.setAdapter(adapter);
        mBinding.base.spinnerPrimaryLangauge.setSelection(index);

        mBinding.base.spinnerPrimaryLangauge.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String language = parent.getAdapter().getItem(position).toString();
                primaryLanguageCode = getLanguageCodeByTitle(language);
                if (primaryLanguageCode.equals(secondaryLanguageCode))
                    setSecondaryLanguageAdapter(null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setSecondaryLanguageAdapter(@Nullable String selectedLanguageCode) {
        if (mBinding.base.rbBilingualNo.isChecked()) return;

        List<String> languages = getSecondaryLanguageTitles();
        String title = getLanguageTitleByCode(selectedLanguageCode);

        int index = 0;
        if (!TextUtils.isEmpty(selectedLanguageCode)) {
            for (int i = 0; i < languages.size(); i++) {
                if (languages.get(i).equals(title)) {
                    index = i;
                    break;
                }
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, languages);
        mBinding.base.spinnerSecondLanguage.setAdapter(adapter);
        mBinding.base.spinnerSecondLanguage.setSelection(index);

        mBinding.base.spinnerSecondLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String language = parent.getAdapter().getItem(position).toString();
                secondaryLanguageCode = getLanguageCodeByTitle(language);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setProfileData(OEProfileModel profile) {
        mBinding.base.tvMobile.setText(profile.oe_contact);
        mBinding.base.etFullName.setText(profile.oe_name);
        mBinding.base.etAddress.setText(profile.oe_address);
        mBinding.base.tvLocality.setText(profile.oe_location);
        mBinding.base.tvPincode.setText(profile.pincode);

        if(profile.secondary_language.equals(""))
            mBinding.base.rbBilingualNo.setChecked(true);

        primaryLanguageCode = TextUtils.isEmpty(profile.preferred_language.trim()) ?
                mStringUtils.getPref().getLocale() : profile.preferred_language.trim();

        secondaryLanguageCode = TextUtils.isEmpty(profile.secondary_language.trim()) ?
                mStringUtils.getPref().getSecondaryLocale() : profile.secondary_language.trim();

        if (TextUtils.isEmpty(secondaryLanguageCode))
            mBinding.base.rgBilingual.check(mBinding.base.rbBilingualNo.getId());
        else mBinding.base.rgBilingual.check(mBinding.base.rbBilingualYes.getId());

        setPrimaryLanguageAdapter(primaryLanguageCode);
        setSecondaryLanguageAdapter(secondaryLanguageCode);

        setMandiList(profile.interested_mandi_list);
        setCropList(profile.interested_crop_list);
    }

    private void setMandiList(String interestedMandiList) {
        if (!TextUtils.isEmpty(interestedMandiList) && mMandis != null) {
            String[] mandiIds = interestedMandiList
                    .replace(" ", "")
                    .replace("[", "")
                    .replace("]", "").split(",");

            interestedMandiIdList.clear();
            interestedMandiIdList.addAll(Arrays.asList(mandiIds));
            List<String> mandiNames = MandiModel.getMandiNamesByIds(mMandis, mandiIds);
            interestedMandiNameList.clear();
            interestedMandiNameList.addAll(mandiNames);
            Log.d(TAG, "setMandiList: " + mandiNames.size() + "     " + interestedMandiIdList.size());
            mInterestedMandisAdapter.setData(interestedMandiNameList);

            if (mandiNames.size() > 0) {
                mBinding.tvMoreInterestedMandisCount.setVisibility(View.GONE);
            } else {
                mBinding.tvMoreInterestedMandisCount.setText(mStringUtils.getLocalizedString(R.string.no_mandis_added));
                mBinding.tvMoreInterestedMandisCount.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setCropList(String interestedCropList) {
        if (!TextUtils.isEmpty(interestedCropList) && mCrops != null) {
            String[] cropIds = interestedCropList
                    .replace(" ", "")
                    .replace("[", "")
                    .replace("]", "").split(",");

            interestedCropIdList.clear();
            interestedCropIdList.addAll(Arrays.asList(cropIds));

            List<Crop> interestedCrops = Crop.getCropsByIds(mCrops, cropIds);
            Log.d(TAG, "setCropList: " + interestedCrops.size() + " " + interestedCropIdList.size());
            mInterestedCropsAdapter.setData(interestedCrops, interestedCropIdList);

            if (interestedCrops.size() > 0) {
                mBinding.tvMoreInterestedCropsCount.setVisibility(View.GONE);
            } else {
                mBinding.tvMoreInterestedCropsCount.setText(mStringUtils.getLocalizedString(R.string.no_crops_added));
                mBinding.tvMoreInterestedCropsCount.setVisibility(View.VISIBLE);
            }

        }
    }


    private void updateProfile() {
        mProgressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_OE_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        Logs.d("CheckStatus_oe", "onResponse: update profile Success"+response);
                        try {
                            //JSONArray responseArray = new JSONArray(response);
                            //JSONObject responseObject = responseArray.getJSONObject(0);
                            //int errorCode = responseObject.getInt("error_code");
                            //if (errorCode == 100)
                            JSONObject jsonObject=new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            if(code==200)
                            {
                                Logs.d("CheckStatus_oe", "onResponse: update profile code"+code);
                                PrefManager prefs = new PrefManager(OeProfileActivity.this);
                                prefs.setInterestedCropIds(TextUtils.join(",", interestedCropIdList));
                                prefs.setInterestedMandiIds(TextUtils.join(",", interestedMandiIdList));

                                prefs.setUserName(mBinding.base.etFullName.getText().toString());
                                prefs.setPinCode(mBinding.base.tvPincode.getText().toString());


                                mStringUtils.getPref().setPrimaryLocale(primaryLanguageCode);
                                if (mBinding.base.rbBilingualYes.isChecked()) {
                                    mStringUtils.getPref().setSecondaryLocale(secondaryLanguageCode);
                                } else {
                                    mStringUtils.getPref().setSecondaryLocale(null);
                                }
                                Toast.makeText(OeProfileActivity.this, "Successfully updated", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            } else {
                                Toast.makeText(OeProfileActivity.this, "Sorry something went wrong. Please try after sometime.", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();
                        Logs.d("CheckStatus_oe", "update profile fail "+error.getMessage());
                        if (error instanceof TimeoutError) {
                            showToast(OeProfileActivity.this, "Slow internet...Kindly check internet to proceed seamlessly.");
                        } else if (error instanceof NoConnectionError) {
                            showToast(OeProfileActivity.this, "No internet connection ");
                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("api_token", mUserProfile.getApiToken());

                //Mandatory
                params.put("user_id", mUserProfile.getUserId());
                params.put("password", mUserProfile.getUserPassword());

                //Optional
                params.put("oe_contact", mStringUtils.getPref().getMobileNo());
                params.put("oe_name", mBinding.base.etFullName.getText().toString());
                params.put("oe_address", mBinding.base.etAddress.getText().toString());

                params.put("oe_location", mBinding.base.tvLocality.getText().toString());
                params.put("pincode", mBinding.base.tvPincode.getText().toString());
                //params.put("oe_pincode", mStringUtils.getPref().getPinCode());

                params.put("preferred_language", primaryLanguageCode);
                params.put("secondary_language",
                        mBinding.base.rgBilingual.getCheckedRadioButtonId() == mBinding.base.rbBilingualYes.getId() ?
                                secondaryLanguageCode : " ");

                JSONArray cropArray = new JSONArray();
                for (String item : interestedMandiIdList) {
                    Log.d("Mandi:   ", item);
                    if (item != null) cropArray.put(item.trim());
                }

                params.put("interested_mandi_list", "" + cropArray);

                JSONArray jsonArray = new JSONArray();
                for (String item : interestedCropIdList) {
                    Log.d("Mandi:   ", item);
                    if (item != null) jsonArray.put(item.trim());
                }

                params.put("interested_crop_list", "" + jsonArray);

                Log.d(TAG, "getParams: " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUserProfile.getApiToken());
                return header;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

        mGoogleApiClient.connect();

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location requests here
                        getLocation();
                        Log.e("location", "getLocation");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(OeProfileActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }

    private void getLocation() {
        Log.e("getLocation", "inside");
        //if (isPermissionGranted) {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                try {
                    Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    Log.e("getLocation", "inside" + mLastLocation);
                    latitude = mLastLocation.getLatitude();
                    longitude = mLastLocation.getLongitude();
                    getAddress();
                } catch (SecurityException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(OeProfileActivity.this, "Permission Denied\n" + deniedPermissions.toString(),
                        Toast.LENGTH_SHORT).show();
                OeProfileActivity.super.onBackPressed();
            }
        };
        TedPermission.with(OeProfileActivity.this)
                .setPermissionListener(permissionlistener)
                .setRationaleTitle("Permissions")
                .setRationaleMessage(
                        "You need to allow following permissions for this app to work properly.")
                .setDeniedTitle("Permission denied")
                .setDeniedMessage(
                        "If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setGotoSettingButtonText("Permission")
                .setPermissions(ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION)
                .check();
    }

    public Address getAddress(double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.ENGLISH);

        try {
            addresses = geocoder.getFromLocation(latitude, longitude,
                    1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            return addresses.get(0);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void getAddress() {
        Log.e("getAddress", "inside");
        Address locationAddress = getAddress(latitude, longitude);

        if (locationAddress != null) {
            String address = locationAddress.getAddressLine(0);
            String address1 = locationAddress.getAddressLine(1);
            String city = locationAddress.getLocality();
            String state = locationAddress.getAdminArea();
            String country = locationAddress.getCountryName();
            String postalCode = locationAddress.getPostalCode();

            String currentLocation;

            if (!TextUtils.isEmpty(address)) {
                currentLocation = address;

                if (!TextUtils.isEmpty(address1)) {
                    currentLocation += "\n" + address1;
                }

                if (!TextUtils.isEmpty(city)) {
                    currentLocation += "\n" + city;
                    if (!TextUtils.isEmpty(postalCode)) {
                        currentLocation += " - " + postalCode;
                    }
                } else {
                    if (!TextUtils.isEmpty(postalCode)) {
                        currentLocation += "\n" + postalCode;
                    }
                }

                if (!TextUtils.isEmpty(state)) {
                    currentLocation += "\n" + state;
                }
                if (!TextUtils.isEmpty(country)) {
                    currentLocation += "\n" + country;
                }
                Log.e("getAddress", "currentLocation" + currentLocation);
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection Failed", Toast.LENGTH_SHORT).show();
    }

    private boolean isValid() {
        boolean isValid = true;
        if (TextUtils.isEmpty(mBinding.base.tvLocality.getText()) && TextUtils.isEmpty(mBinding.base.tvPincode.getText())) {
            showToast(OeProfileActivity.this, "Please enter City or Pincode");
            isValid = false;
        } else if (mBinding.base.rbBilingualYes.isSelected() &&
                mBinding.base.spinnerSecondLanguage.getSelectedItemPosition() == mBinding.base.spinnerPrimaryLangauge.getSelectedItemPosition()) {
            showToast(OeProfileActivity.this, "Preferred Language and Secondary Language cannot be same");
            isValid = false;
        }
        return isValid;
    }

    private boolean validateValues(String name, String mobile, String address, String city, String pinCode) {
        if (TextUtils.isEmpty(name)) {
            showToast(OeProfileActivity.this, getString(R.string.enter_your_name));
            return false;
        } else if (TextUtils.isEmpty(mobile) || mobile.length() != 10) {
            showToast(OeProfileActivity.this, getString(R.string.enter_valid_number));
            return false;
        } else if (TextUtils.isEmpty(address)) {
            showToast(OeProfileActivity.this, getString(R.string.enter_your_address));
            return false;
        } else if (TextUtils.isEmpty(city)) {
            showToast(OeProfileActivity.this, getString(R.string.enter_your_city));
            return false;
        } else if (TextUtils.isEmpty(pinCode) || pinCode.length() != 6) {
            showToast(OeProfileActivity.this, getString(R.string.enter_your_pincode));
            return false;
        }

        return true;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    private void getMandis() {
        MandiDataService mandiDataService = new MandiDataService(this);
        mandiDataService.getMandis(new MandiDataService.OnMandiResponse() {
            @Override
            public void onMandiSuccess(List<MandiModel> mandies) {
                OeProfileActivity.this.mMandis = mandies;
                isMandiLoaded.set(true);
                if (isCropsLoaded.get() && isMandiLoaded.get()) getProfileDetails();
            }

            @Override
            public void onMandiFailure() {

            }
        });
    }

    private void getCrops() {
        CropDataService cropDataService = new CropDataService(this);
        cropDataService.getCrops(new CropDataService.OnCropDataRequest() {
            @Override
            public void onCropDataResults(List<Crop> crops) {
                OeProfileActivity.this.mCrops = crops;
                isCropsLoaded.set(true);
                if (isCropsLoaded.get() && isMandiLoaded.get()) getProfileDetails();

            }

            @Override
            public void onCropDataError() {

            }
        });
    }

    public interface OnBackClickListener {
        boolean onBackClick();
    }

    private OnBackClickListener onBackClickListener;

    public void setOnBackClickListener(OnBackClickListener onBackClickListener) {
        this.onBackClickListener = onBackClickListener;
    }

    @Override
    public void onBackPressed() {
        if (onBackClickListener != null && onBackClickListener.onBackClick()) {
            return;
        }
        super.onBackPressed();
    }
}
