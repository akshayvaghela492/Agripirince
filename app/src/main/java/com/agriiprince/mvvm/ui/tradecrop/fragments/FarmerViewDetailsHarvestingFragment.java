package com.agriiprince.mvvm.ui.tradecrop.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.databinding.FragmentCropdetailHarvestingBinding;
import com.agriiprince.mvvm.data.prefs.UserProfile;

public class FarmerViewDetailsHarvestingFragment extends Fragment {

    private FragmentCropdetailHarvestingBinding binding;

    private static final String ARG_SECTION_NUMBER = "section_number";

    private UserProfile mUserProfile;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserProfile = new UserProfile(this.getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cropdetail_harvesting, container, false);
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_cropdetail_harvesting, container, false);

        return binding.getRoot();
    }

    public static FarmerViewDetailsHarvestingFragment newInstance(int index) {
        FarmerViewDetailsHarvestingFragment fragment = new FarmerViewDetailsHarvestingFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }
}
