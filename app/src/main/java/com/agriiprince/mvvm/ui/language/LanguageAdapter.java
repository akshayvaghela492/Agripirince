package com.agriiprince.mvvm.ui.language;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.databinding.ItemLanguageBinding;
import com.agriiprince.mvvm.model.Language;
import com.agriiprince.mvvm.ui.base.BaseRecyclerViewAdapter;

import java.util.List;

import javax.inject.Inject;

public class LanguageAdapter extends BaseRecyclerViewAdapter<Language, LanguageAdapter.LanguageViewHolder> {

    private OnLanguageItemListener mListener;

    @Inject
    public LanguageAdapter() {

    }

    public void addListener(OnLanguageItemListener listener) {
        this.mListener = listener;
    }

    public void removeListener() {
        this.mListener = null;
    }

    public int getSelectedLanguagePosition() {
        for (int i = 0; i < mList.size(); i++) {
            Language language = mList.get(i);
            if (language.isSelected()) return i;
        }
        return 0;
    }

    public Language getSelectedLanguage() {
        for (Language language : mList) {
            if (language.isSelected()) return language;
        }
        return null;
    }

    @NonNull
    @Override
    public LanguageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_language, viewGroup, false);
        return new LanguageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LanguageViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.size() > 0 && payloads.get(0) instanceof Language) {
            Language language = (Language) payloads.get(0);
            holder.bind(language);
        } else {
            super.onBindViewHolder(holder, position, payloads);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull LanguageViewHolder holder, int i) {
        Language language = getItem(i);
        holder.bind(language);
    }

    class LanguageViewHolder extends RecyclerView.ViewHolder {

        private ItemLanguageBinding mBinding;

        private LanguageViewHolder(View view) {
            super(view);
            mBinding = DataBindingUtil.bind(view);
        }

        private void bind(Language item) {
            mBinding.textViewLanguageLetter.setText(item.getLanguageLetter());
            mBinding.textViewLanguageTitle.setText(item.getLanguageTitle());

            if (item.isSelected()) mBinding.rootLayout.setBackgroundResource(R.drawable.shape_rectangle_border_selected);
            else mBinding.rootLayout.setBackgroundResource(R.drawable.shape_rectangle_border);

            mBinding.rootLayout.setOnClickListener(onClickItem);
        }

        private View.OnClickListener onClickItem = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) mListener.onClickItem(getAdapterPosition());
            }
        };
    }

    /**
     * listener for item interaction events
     */
    public interface OnLanguageItemListener {

        void onClickItem(int position);
    }
}
