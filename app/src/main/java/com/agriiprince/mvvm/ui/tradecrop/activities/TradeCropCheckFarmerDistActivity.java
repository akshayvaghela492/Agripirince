package com.agriiprince.mvvm.ui.tradecrop.activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.agriiprince.R;
import com.agriiprince.databinding.ActivityFigCheckFarmerDistBinding;
import com.agriiprince.mvvm.data.prefs.UserProfile;

public class TradeCropCheckFarmerDistActivity extends AppCompatActivity {

    private ActivityFigCheckFarmerDistBinding binding;
    private UserProfile mUserProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_fig_check_farmer_dist);
        mUserProfile = new UserProfile(this);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void onCheckClicked(View view) {
        Context context = view.getContext();
        Intent intent = new Intent(context, TradeCropCheckFarmerDistCheckActivity.class);
        context.startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
