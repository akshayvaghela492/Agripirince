package com.agriiprince.mvvm.ui.tradecrop.tabs;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.agriiprince.R;
import com.agriiprince.mvvm.ui.tradecrop.fragments.FarmerActiveCropsFragment;
import com.agriiprince.mvvm.ui.tradecrop.fragments.FarmerReadyToHarvestFragment;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class ManageCropSectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.trade_crop_tab_text_1, R.string.trade_crop_tab_text_2};
    private final Context mContext;

    public ManageCropSectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        if(position==0)
            return FarmerActiveCropsFragment.newInstance(position + 1);
        else
            return FarmerReadyToHarvestFragment.newInstance(position + 1);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 2;
    }
}