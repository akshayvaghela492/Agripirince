package com.agriiprince.mvvm.ui.weather;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.activities.TutorialActivity;
import com.agriiprince.mvvm.applevel.analytics.FarmerRealtimeDBAnalytics;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.model.WeatherModel;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.weather.WeatherResponse;
import com.agriiprince.mvvm.retrofit.model.weather.DailyForecast;
import com.agriiprince.mvvm.retrofit.service.WeatherForecast;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherActivityFinal extends AppCompatActivity {

    PrefManager prefManager;
    String locationID;
    WeatherModel weatherModel;
    RecyclerView weatherRecyclerView;
    WeatherForecastAdapter weatherForecastAdapter;
    ArrayList<WeatherModel> weatherForecastList = new ArrayList<>();
    private ProgressDialog progressDialog;

    private UserProfile mUserProfile;
    FirebaseDatabase mFDatabase;
    DatabaseReference mRef;
    DatabaseReference mFarmersRef;
    DatabaseReference mNumRef;
    DatabaseReference mKeyRef;
    long mTimeStart;
    String mUserId;

    private TextView emptyText;

    StringUtils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_final);
        mUserProfile = new UserProfile(this);
        prefManager = new PrefManager(this);
        progressDialog = new ProgressDialog(this);
        utils = AppController.getInstance().getStringUtils();

        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        //getSupportActionBar().setTitle(utils.getLocalizedString(R.string.weather));

        mUserId = mUserProfile.getUserId();

        mFDatabase = FirebaseDatabase.getInstance();
        mRef = mFDatabase.getReference("analytics");
        mFarmersRef = mRef.child("farmers");
        mNumRef = mFarmersRef.child(mUserProfile.getUserMobile());

        progressDialog.setCancelable(false);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));

        emptyText = findViewById(R.id.empty_text);

        weatherRecyclerView = findViewById(R.id.weather_recycler_view);
        weatherForecastAdapter = new WeatherForecastAdapter(weatherForecastList,
                WeatherActivityFinal.this);

        weatherRecyclerView.setAdapter(weatherForecastAdapter);
        weatherRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        ImageView imgview_help =  findViewById(R.id.iv_help);

        imgview_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(WeatherActivityFinal.this, TutorialActivity.class);
                intent.putExtra("code", "WEAT");
                startActivity(intent);
            }
        });

        //getLocationID(place);
        getLocationWeatherDetails("", "" + prefManager.getCity());

    }


    void showEmptyText() {
        emptyText.setText(utils.getLocalizedString(R.string.weather_not_available_try_later));
        emptyText.setVisibility(View.VISIBLE);
    }

    void hideEmptyText() {
        emptyText.setVisibility(View.GONE);
    }

    private void getLocationWeatherDetails(String locationID, final String place) {
        progressDialog.show();
        hideEmptyText();

        /* final String weatherURL ="http://dataservice.accuweather.com/forecasts/v1/daily/5day/" + locationID+ "?apikey=MgAv8HtR0GnN6H8RcCaVRIkMDS19ve0d&details=true";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_WEATHER_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            parseResponse(response, place);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showEmptyText();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                GeneralUtils.showVolleyError(WeatherActivityFinal.this, error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                Calendar calendar = Calendar.getInstance();

                params.put("api_token", mUserProfile.getApiToken());

                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                String abhiKiDate = sdf.format(calendar.getTime());
                params.put("date", "" + abhiKiDate);
                params.put("city", prefManager.getCity());

                Log.d("weather params", params.toString());
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
        */
        API_Manager api_manager=new API_Manager();
        WeatherForecast retrofit_interface = api_manager.getClient9().create(WeatherForecast.class);
        Calendar calendar = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String abhiKiDate = sdf.format(calendar.getTime());

        Call<WeatherResponse> call = retrofit_interface.WeatherResponse(mUserProfile.getApiToken(),abhiKiDate, prefManager.getCity());
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                Logs.d("CheckStatus", "onResponse: "+response);
                progressDialog.dismiss();
                try {
                    parseResponse(response, place);
                } catch (JSONException e) {
                    e.printStackTrace();
                    showEmptyText();
                }

            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                progressDialog.dismiss();
                Utils.showToast(WeatherActivityFinal.this, t.getMessage());
            }
        });
    }

    private void parseResponse(Response<WeatherResponse> response, String place) throws JSONException {

    /*    Log.d("WeatherForecast", "" + response);
          JSONObject responseObject = new JSONObject(response.body().getMessage());
          JSONArray forecastArray = responseObject.getJSONArray("DailyForecasts");
            for (int i = 0; i <= 4; i++) {
            weatherModel = new WeatherModel();
            JSONObject forecastObject = forecastArray.getJSONObject(i);
            JSONObject temperatureObject = forecastObject.getJSONObject("Temperature");
            JSONObject maxTempObject = temperatureObject.getJSONObject("Maximum");
            JSONObject minTempObject = temperatureObject.getJSONObject("Minimum");
            JSONObject dayObject = forecastObject.getJSONObject("Day");
            JSONObject nightObject = forecastObject.getJSONObject("Night");
            String dateStamp = forecastObject.getString("Date");
            double minTemp = (double) minTempObject.getInt("Value");
            double maxTemp = (double) maxTempObject.getInt("Value");
*/
                for (int i = 0; i <= 4; i++) {
                    weatherModel = new WeatherModel();

            List<DailyForecast> dailyForecasts = response.body().getData().getDailyForecasts();
            String dateStamp = dailyForecasts.get(i).getDate();
            String[] date = dateStamp.split("T");
            weatherModel.setDate(date[0]);
            weatherModel.setCity(place);
            weatherModel.setTempMax(dailyForecasts.get(i).getTemperature().getMaximum().getValue());
            weatherModel.setTempMin(dailyForecasts.get(i).getTemperature().getMinimum().getValue());
            weatherModel.setDayIcon(getIcon(dailyForecasts.get(i).getDay().getIcon(), 0));
            weatherModel.setDayCondition(dailyForecasts.get(i).getDay().getShortPhrase());
            weatherModel.setNightIcon(getIcon(dailyForecasts.get(i).getNight().getIcon(), 1));
            weatherModel.setNightCondition(dailyForecasts.get(i).getNight().getShortPhrase());

            weatherForecastList.add(weatherModel);

        }

        weatherForecastAdapter.notifyDataSetChanged();

    }

    private int getIcon(int icon, int timeOfDay) {

        switch (icon) {

            case 1:
                return R.drawable.img1;

            case 24:
                return R.drawable.img22;

            case 2:
                return R.drawable.img2;

            case 3:
                return R.drawable.img3;

            case 4:
                return R.drawable.img4;

            case 5:
                return R.drawable.img5;

            case 6:
                return R.drawable.img6;

            case 7:
                return R.drawable.img7;

            case 8:
                return R.drawable.img8;

            case 11:
                if (timeOfDay == 0) {
                    return R.drawable.img11_day;
                } else {
                    return R.drawable.img11_night;
                }

            case 12:
                return R.drawable.img12;

            case 13:
                return R.drawable.img13;

            case 14:
                return R.drawable.img14;

            case 15:
                return R.drawable.img15;

            case 16:
                return R.drawable.img16;

            case 17:
                return R.drawable.img17;

            case 18:
                return R.drawable.img18;

            case 19:
                return R.drawable.img19;

            case 20:
                return R.drawable.img20;

            case 21:
                return R.drawable.img21;

            case 22:
                return R.drawable.img22;

            case 23:
                return R.drawable.img23;

            case 25:
                return R.drawable.img25;

            case 26:
                return R.drawable.img26;

            case 29:
                return R.drawable.img29;

            case 33:
                return R.drawable.img33;

            case 34:
                return R.drawable.img34;

            case 35:
                return R.drawable.img35;

            case 36:
                return R.drawable.img36;

            case 37:
                return R.drawable.img37;

            case 38:
                return R.drawable.img38;

            case 39:
                return R.drawable.img39;

            case 40:
                return R.drawable.img40;

            case 41:
                return R.drawable.img41;

            case 42:
                return R.drawable.img42;

            case 43:
                return R.drawable.img43;

            case 44:
                return R.drawable.img44;

            default:
                return R.drawable.sunny1;

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        mTimeStart = System.currentTimeMillis();
    }

    @Override
    protected void onStop() {
        super.onStop();
        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - mTimeStart;
        final int elapsedSeconds = (int)(tDelta / 1000.0);

        Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    FarmerRealtimeDBAnalytics.saveEventFarmerTimeSpent(
                            dataSnapshot, "time_spent_weather", mNumRef, elapsedSeconds);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
}
