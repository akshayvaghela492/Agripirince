package com.agriiprince.mvvm.ui.registration;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.mvvm.retrofit.dto.profile.UserUpdate;
import com.agriiprince.mvvm.retrofit.model.login.Profile;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.dataservice.CropDataService;
import com.agriiprince.dataservice.MandiDataService;
import com.agriiprince.mvvm.ui.registration.infostepsfragments.InfoBasicFragment;
import com.agriiprince.mvvm.ui.registration.infostepsfragments.InfoCropsFragment;
import com.agriiprince.mvvm.ui.registration.infostepsfragments.InfoFarmSizeFragment;
import com.agriiprince.mvvm.ui.registration.infostepsfragments.InfoIncomeFragment;
import com.agriiprince.mvvm.ui.registration.infostepsfragments.InfoLocationFragment;
import com.agriiprince.mvvm.ui.registration.infostepsfragments.InfoMandiFragment;
import com.agriiprince.mvvm.ui.registration.infostepsfragments.InfoMandiInterestedFragment;
import com.agriiprince.mvvm.ui.registration.infostepsfragments.InfoPriceFragment;
import com.agriiprince.model.MandiModel;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import retrofit2.Call;
import retrofit2.Callback;

public class FarmerInfoActivity extends AppCompatActivity implements InfoLocationFragment.OnInfoLocationInteractionListener,
        InfoBasicFragment.OnInfoBasicInteractionListener, AddInterestedCropFragment.OnInfoCropsInteractionListener,
        InfoFarmSizeFragment.OnInfoFarmSizeInteractionListener, InfoPriceFragment.OnInfoPriceInteractionListener,
        InfoMandiFragment.OnInfoMandiInteractionListener, InfoMandiInterestedFragment.OnInfoMandiInteractionListener,
        InfoIncomeFragment.OnInfoIncomeInteractionListener {

    private static final String TAG = FarmerInfoActivity.class.getSimpleName();

    private final int REQUEST_CODE_CROP_SELECTION = 999;

    public static final String MOBILE_NUMBER = "mobile_number";
    public static final String PASSWORD = "mPassword";

    private ProgressDialog mProgressDialog;

    PrefManager mPrefManager;

    //private UserProfile mUserProfile;

    private List<Crop> mCrops;
    private List<MandiModel> mMandies;
    private List<MandiModel> mFullMandiList;


    private AtomicBoolean isCropsLoaded;
    private AtomicBoolean isMandiesLoaded;

    private String mPhoneNumber;
    private String mPassword;

    private Location mLocation;

    private String mName;

    private String mStreet;
    private String mCity;
    private String mState;
    private String mPinCode;

    private List<String> mSelectedCrops;
    private List<String> mSelectedMandies;

    private String mAreaUnit;
    private int mFarmSize;

    private String mPriceUnit;

    private String mNearestMandi;

    private int mIncome;

    private String mUserType = "farmer"; // farmer profile activity, so only farmer :)

    private String mLanguage;

    private String mUserId;
    private StringUtils utils;

    @Override
    public void onInfoLocationSuccess(Location location) {
        hideSoftInputMethod();

        if (location != null) {
            this.mLocation = location;
            try {
                Geocoder geocoder = new Geocoder(this);
                List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 5);

                for (Address address : addresses) {
                    if (TextUtils.isEmpty(address.getLocality()))
                        continue;

                    this.mStreet = address.getAddressLine(0).split(address.getLocality())[0].trim(); // remove address after city name
                    this.mCity = address.getLocality();
                    this.mState = address.getAdminArea();
                    this.mPinCode = address.getPostalCode();

                    break;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.d(TAG, mStreet + " " + mCity + " " + mState + " " + mPinCode);

        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, InfoBasicFragment.newInstance(mStreet, mCity, mState, mPinCode))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onInfoBasicSuccess(String name, String street, String city, String state, String pinCode) {
        hideSoftInputMethod();
        Log.d(TAG, name + " " + street + " " + city + " " + state + " " + pinCode);
        this.mName = name;
        this.mStreet = street;
        this.mCity = city;
        this.mState = state;
        this.mPinCode = pinCode;

        if (mLocation == null) {
            try {
                String search = city + ", " + state;
                Geocoder geocoder = new Geocoder(this);
                List<Address> addresses = geocoder.getFromLocationName(search, 5);

                for (Address address : addresses) {
                    if (address.getLocality() == null) continue;

                    this.mLocation = new Location("user");
                    mLocation.setLatitude(address.getLatitude());
                    mLocation.setLongitude(address.getLongitude());
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Log.d(TAG, mStreet + " " + mCity + " " + mState + " " + mPinCode);
        if (mLocation != null) Log.d(TAG, mLocation.getLatitude() + " " + mLocation.getLongitude());


//                        Intent mIntent = new Intent(FarmerInfoActivity.this, SelectCropActivity.class);
//                        Bundle mBundle = new Bundle();
//                        mBundle.putBoolean("fromProfile", false);
//                        mIntent.putExtras(mBundle);
//                        startActivity(mIntent);
        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, InfoCropsFragment.newInstance(false, false))
                .addToBackStack(null)
                .commit();

//        Intent mIntent = new Intent(FarmerInfoActivity.this, SelectCropActivity.class);
//        Bundle mBundle = new Bundle();
//        mBundle.putBoolean("fromProfile", false);
//        mIntent.putExtras(mBundle);
//        startActivityForResult(mIntent, REQUEST_CODE_CROP_SELECTION);
    }

    @Override
    public void onInfoCropSuccess(List<String> crops) {
        hideSoftInputMethod();
        if (this.mSelectedCrops == null) this.mSelectedCrops = new ArrayList<>();

        this.mSelectedCrops.clear();
        for (String crop : crops) {
            //Crop crop = Crop.getCropByBilingualCropName(FarmerInfoActivity.this, mCrops, value);
            if (crop != null)
                this.mSelectedCrops.add(crop);
        }

        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, InfoFarmSizeFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onInfoFarmSizeSuccess(String unit, int size) {
        hideSoftInputMethod();

        this.mAreaUnit = unit;
        this.mFarmSize = size;

        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, InfoPriceFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onInfoPriceSuccess(String unit) {
        hideSoftInputMethod();
        this.mPriceUnit = unit;

        showProgressDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!isMandiesLoaded.get()) {
                    // wait till the data loads
                    // replace this if you find better solution
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dismissProgressDialog();
                        double lat = mLocation.getLatitude();
                        double lng = mLocation.getLongitude();
                        getSupportFragmentManager().beginTransaction()
                                .add(android.R.id.content, InfoMandiFragment.newInstance("", lat, lng, false))
                                .addToBackStack(null)
                                .commit();
                    }
                });
            }
        }).start();
    }

    @Override
    public void onInfoMandiSuccess(final String nearestMandi) {
        showProgressDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!isMandiesLoaded.get()) {
                    // wait till the data loads
                    // replace this if you find better solution
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        List<String> emptyInterestedMandiList = null;
                        dismissProgressDialog();
                        double lat = mLocation.getLatitude();
                        double lng = mLocation.getLongitude();
                        getSupportFragmentManager().beginTransaction()
                                .add(android.R.id.content, InfoMandiInterestedFragment.newInstance(nearestMandi,
                                        (ArrayList<String>) emptyInterestedMandiList, lat, lng, false))
                                .addToBackStack(null)
                                .commit();
                    }
                });
            }
        }).start();
    }

    @Override
    public void onInfoMandiInterestedSuccess(String nearestMandi, List<String> selectedMandies) {
        hideSoftInputMethod();
        Log.d(TAG, selectedMandies.toString());
        this.mNearestMandi = nearestMandi;
        //this.mNearestMandi = MandiModel.getMandiNameByMandiCity(FarmerInfoActivity.this, mMandies, nearestMandi);

        if (this.mSelectedMandies == null) this.mSelectedMandies = new ArrayList<>();

        this.mSelectedMandies.clear();
        this.mSelectedMandies = selectedMandies;
        //for (String value : selectedMandies) {
        //    this.mSelectedMandies.add(MandiModel.getMandiNameByMandiCity(FarmerInfoActivity.this, mMandies, value));
        //}

        Log.d("FarmerInfoActivity", "NEAREST MANDI - " + nearestMandi + " | INTEREST MANDIES - " + selectedMandies.toString());

        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, InfoIncomeFragment.newInstance())
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onInfoIncomeSuccess(int value) {
        hideSoftInputMethod();

        this.mIncome = value;
        Log.e("Income_Update: ", "steps finished with income : " + value);

        updateProfile();
        //userRegistration();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiling);
        utils = AppController.getInstance().getStringUtils();
        PrefManager prefManager = new PrefManager(this);
        mLanguage = prefManager.getLocale();

        mPrefManager = new PrefManager(this);
        //mUserProfile = new UserProfile(this);

        isCropsLoaded = new AtomicBoolean(false);
        isMandiesLoaded = new AtomicBoolean(false);

        mProgressDialog = new ProgressDialog(this);

        Intent intent = getIntent();

        mPhoneNumber = intent.getStringExtra(MOBILE_NUMBER);
        mPassword = intent.getStringExtra(PASSWORD);

        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new InfoLocationFragment())
                .commit();

        getCrops();

        getMandies();
    }

    @Override
    protected void onPause() {
        super.onPause();

        dismissProgressDialog();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "results from activity");

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_CROP_SELECTION) {
            Log.d(TAG, "results from selection of crops activity");

            if (data != null) {
                if (data.getData() != null) {
                    try {
                        String result = data.getData().toString();

                        JSONArray jsonArray = new JSONArray(result);

                        List<String> selectedCrops = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            selectedCrops.add(jsonArray.getString(i));
                        }

                        if (this.mSelectedCrops == null) this.mSelectedCrops = new ArrayList<>();

                        this.mSelectedCrops.addAll(selectedCrops);

                        Log.d(TAG, "selected crops: " + this.mSelectedCrops.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            getSupportFragmentManager().beginTransaction()
                    .add(android.R.id.content, InfoFarmSizeFragment.newInstance())
                    .addToBackStack(null)
                    .commit();
        }
    }

    private void getCrops() {

        CropDataService cropDataService = new CropDataService(this);
        cropDataService.getCrops(new CropDataService.OnCropDataRequest() {
            @Override
            public void onCropDataResults(List<Crop> crops) {
                FarmerInfoActivity.this.mCrops = crops;

                isCropsLoaded.set(true);
            }

            @Override
            public void onCropDataError() {
                FarmerInfoActivity.this.mCrops = null;

            }
        });
    }

    public interface OnBackClickListener {
        boolean onBackClick();
    }

    private OnBackClickListener onBackClickListener;

    public void setOnBackClickListener(OnBackClickListener onBackClickListener) {
        this.onBackClickListener = onBackClickListener;
    }

    @Override
    public void onBackPressed() {
        if (onBackClickListener != null && onBackClickListener.onBackClick()) {
            return;
        }
        super.onBackPressed();
    }

    private void getMandies() {

        MandiDataService mandiDataService = new MandiDataService(this);
        mandiDataService.getMandis(new MandiDataService.OnMandiResponse() {
            @Override
            public void onMandiSuccess(List<MandiModel> mandies) {
                FarmerInfoActivity.this.mMandies = mandies;

                isMandiesLoaded.set(true);
            }

            @Override
            public void onMandiFailure() {
                FarmerInfoActivity.this.mMandies = null;
            }
        });
    }


    /*private void userRegistration() {

        Call<UserRegistration> call = retrofit_interface.registration(mUserType.toLowerCase(),mName,mPassword,mCity,
                mPinCode,mPhoneNumber,mLanguage," ");
        Logs.d("CheckStatus", mUserType.toLowerCase() + " " + mName + " " + mPassword + " " + mCity
                + " " + mPinCode + " " + mPhoneNumber + " " + mLanguage);
        call.enqueue(new Callback<UserRegistration>() {
            @Override
            public void onResponse(Call<UserRegistration> call, retrofit2.Response<UserRegistration> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code registration ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus", response.body().getMessage());
                    Logs.d("CheckStatus", response.body().getData().getUserId());

                    int error_code = response.body().getCode();

                    if (error_code == 200) {

                        //mUserId = response.body().getData().getUserId();

                        mPrefManager.setCreatedOn(Calendar.getInstance().getTimeInMillis());

                        onSignUpSuccess();

                        generateToken();

                    } else {
                        Toast.makeText(FarmerInfoActivity.this, utils.getLocalizedString(R.string.something_went_wrong_try_again), Toast
                                .LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<UserRegistration> call, Throwable t) {
                Log.d("CheckStatus", "UserRegistration "+ t.getMessage());
            }
        });


        /*StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.registration_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "UserRegistration response " + response);

                        JSONArray jsonArray = null;

                        try {
                            jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            int error_code = jsonObject.getInt("error_code");
                            if (error_code == 100) {

                                mUserId = jsonObject.getString("user_id");

                                mUserProfile.setCreatedOn(Calendar.getInstance().getTimeInMillis());

                                onSignUpSuccess();

                                generateToken();

                            } else {
                                Toast.makeText(FarmerInfoActivity.this, utils.getLocalizedString(R.string.something_went_wrong_try_again), Toast
                                        .LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                GeneralUtils.showVolleyError(FarmerInfoActivity.this, error);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", mName);
                params.put("password", mPassword);
                params.put("mobileno", mPhoneNumber);
                params.put("user_type", mUserType);
                params.put("city", mCity);
                params.put("language", mLanguage);
                params.put("secondary_language", mLanguage);
                params.put("fcm_token", " ");
                Log.d(TAG, "UserRegistration params : " + params.toString());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }*/

    /*private void uploadFcmToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        String token = instanceIdResult.getToken();

                        new FcmTokenUpdateDataService(mPrefManager.getUserId(), mPrefManager.getUserType(), mPrefManager.getAccessToken(), token);
                    }
                });
    }*/

    private void updateProfile() {
        //mProgressDialog.show();
      /*  StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_FARMER_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        Log.d(TAG, response);
                        try {
                            JSONArray responseArray = new JSONArray(response);
                            JSONObject responseObject = responseArray.getJSONObject(0);
                            int errorCode = responseObject.getInt("error_code");
                            if (errorCode == 100) {

                                startActivity(new Intent(FarmerInfoActivity.this, FarmerHomeScreenActivity.class));
                                FarmerInfoActivity.this.finish();

                            } else {
                                Toast.makeText(FarmerInfoActivity.this, utils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();
                        GeneralUtils.showVolleyError(FarmerInfoActivity.this, error);
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("api_token", mUserProfile.getApiToken());

                //Mandatory
                params.put("user_id", mUserId);
                params.put("password", mPassword);

                params.put("farmer_address", mStreet);
                params.put("farmer_pincode", mPinCode);
                params.put("preferred_language", mLanguage);
                params.put("secondary_language", mLanguage);
                params.put("average_annual_income", mIncome);
                params.put("land_in_acres", mFarmSize);
                params.put("price_measuring_unit", mPriceUnit);
                params.put("area_measuring_unit", mAreaUnit);

                for (MandiModel model : mMandies) {
                    if (model.mandi_id.equals(mNearestMandi)) {
                        Log.e("nearest_mandi_name", "--------->" + model.mandi_id);
                        params.put("nearest_mandi_name", model.mandi_id);
                        params.put("nearest_mandi_lat", model.mandi_lat);
                        params.put("nearest_mandi_log", model.mandi_lng);
                    }
                }


                JSONArray jsonArray = new JSONArray();
                for (String item : mSelectedCrops) {
                    jsonArray.put(item);
                }
                params.put("interested_crop_list", "" + jsonArray);

                JSONArray jsonArray2 = new JSONArray();
                for (String item : mSelectedMandies) {
                    jsonArray2.put(item);
                }
                Log.e("interested_mandi_list", "--------->" + jsonArray2);
                params.put("interested_mandi_list", "" + jsonArray2);

                Log.d(TAG, params.toString());
                return params;

            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
        */

        Log.d("farmer_profile"," ----->1");

        Profile params = new Profile();

        params.setFarmerId(mPrefManager.getUserId());
        params.setFarmerContact(mPhoneNumber);
        //params.setFarmerPassword(mPassword);
        //params.setFarmerId("UKYVLUT5");
        //params.setFarmerContact("1478523691");

        params.setFarmerAddress(mStreet);
        params.setFarmerPincode(mPinCode);
        params.setPreferredLanguage(mLanguage);
        //params.setSecondaryLanguage(mLanguage);
        Log.e("Income_Update"," ----->2" + mIncome);
        params.setAverageAnnualIncome(mIncome);
        Log.d("farmer_profile"," ----->3" + mFarmSize);
        params.setLandInAcres(mFarmSize);
        Log.d("farmer_profile"," ----->4");
        params.setPriceMeasuringUnit(mPriceUnit);
        params.setAreaMeasuringUnit(mAreaUnit);

        for (MandiModel model : mMandies) {
            if (model.mandi_id.equals(mNearestMandi)) {
                Log.d("farmer_profile ", " nearest_mandi_name --------->" + model.mandi_id);
                params.setNearestMandiName(model.mandi_id);
                //params.setNearestMandiLat(model.mandi_lat);
                //params.setNearestMandiLog(model.mandi_lng);
            }
        }
        params.setInterestedCropList(mSelectedCrops);
        params.setInterestedMandiList(mSelectedMandies);

        Log.d(TAG, params.toString());

        Log.e("farmer_profile"," ----->>>>>>>>>>>>>>>>>> " + params.getFarmerId() + params.getFarmerPassword()
                + params.getFarmerContact() + params.getFarmerAddress() + params.getFarmerPincode() + params.getPreferredLanguage()
                + params.getAverageAnnualIncome() + params.getLandInAcres() + params.getPriceMeasuringUnit() + params.getAreaMeasuringUnit()
                + params.getNearestMandiName() + params.getInterestedCropList() + params.getInterestedMandiList() + mPrefManager.getAccessToken() );
        API_Manager apiManager=new API_Manager();
        UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);
        Call<UserUpdate> call=retrofit_interface.updateprofile(mPrefManager.getAccessToken(),params);
        call.enqueue(new Callback<UserUpdate>() {
            @Override
            public void onResponse(Call<UserUpdate> call, retrofit2.Response<UserUpdate> response) {
                try {
                    Log.d("farmer_profile","info response"+response.body());
                    Log.d("farmer_profile","info response"+response.body().getStatus());
                    Log.e("farmer_profile","info response"+response.body().getCode());
                    Log.e("farmer_profile","info response"+response.body().getMessage());
                    if (response.body().getCode() == 200) {

                        mPrefManager.setFilledDetails(true);
                        //startActivity(new Intent(FarmerInfoActivity.this, FarmerHomeScreenActivity.class));
                        Toast.makeText(FarmerInfoActivity.this,  utils.getLocalizedString(R.string.successfully_updated), Toast.LENGTH_LONG).show();
                        FarmerInfoActivity.this.finish();

                    } else {
                        Toast.makeText(FarmerInfoActivity.this, "UpdateProfile" + utils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<UserUpdate> call, Throwable t) {
                //mProgressDialog.dismiss();
                Log.e("farmer_profile","info fail"+t.getMessage());
                Utils.showToast(FarmerInfoActivity.this, t.getLocalizedMessage());
            }
        });
    }

    /*private void onSignUpSuccess() {

        PrefManager mPrefManager = new PrefManager(this);

        mPrefManager.setUserName(mName);
        mPrefManager.setUserId(mUserId);
        mPrefManager.setUserPassword(mPassword);
        mPrefManager.setUserType(mUserType);
        mPrefManager.setCity(mCity);
        mPrefManager.setPinCode(mPinCode);
        mPrefManager.setMobileNo(mPhoneNumber);
        mPrefManager.setNewUser(true);

    }

    API_Manager apiManager=new API_Manager();
    UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);

    private void generateToken() {//

        Call<TokenGenerate> call = retrofit_interface.generateToken(mPrefManager.getUserId(), mPrefManager.getUserType().toLowerCase(), mPrefManager.getUserPassword());
        call.enqueue(new Callback<TokenGenerate>() {
            @Override
            public void onResponse(Call<TokenGenerate> call, retrofit2.Response<TokenGenerate> response) {
                try {

                    Logs.d("CheckStatus", "onResponse: code token " );
                    Logs.d("CheckStatus",String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getData().getToken());

                    int errorCode = response.body().getCode();

                    Log.d("NEW FARMERINFO -> ", errorCode+"");
                    if (errorCode == 200) {

                        mPrefManager.setAccessToken(response.body().getData().getToken());

                        uploadFcmToken();

                        updateProfile();
                    } else if (errorCode == 101) {
                        Toast.makeText(FarmerInfoActivity.this, utils.getLocalizedString(R.string.login_to_continue), Toast.LENGTH_SHORT).show();


                        startActivity(new Intent(FarmerInfoActivity.this, LoginActivity.class));
                        FarmerInfoActivity.this.finish();
                    }

                }catch (Exception e){
                    e.getMessage();
                    e.printStackTrace();
                    Toast.makeText(FarmerInfoActivity.this, utils.getLocalizedString(R.string.login_to_continue), Toast.LENGTH_SHORT).show();

                    startActivity(new Intent(FarmerInfoActivity.this, LoginActivity.class));
                    FarmerInfoActivity.this.finish();
                }
            }

            @Override
            public void onFailure(Call<TokenGenerate> call, Throwable t) {
                Log.d("CheckStatus", "token "+ t.getMessage());
            }
        });


        /*StringRequest request = new StringRequest(Request.Method.POST, Config.POST_GENERATE_TOKEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);

                        try {

                            JSONArray responseArray = new JSONArray(response);
                            JSONObject responseObject = responseArray.getJSONObject(0);
                            int errorCode = responseObject.getInt("error_code");

                            if (errorCode == 100) {

                                mUserProfile.setApiToken(responseObject.getString("token"));

                                uploadFcmToken();

                                updateProfile();


                            } else if (errorCode == 101) {
                                Toast.makeText(FarmerInfoActivity.this, utils.getLocalizedString(R.string.login_to_continue), Toast.LENGTH_SHORT).show();


                                startActivity(new Intent(FarmerInfoActivity.this, LoginActivity.class));
                                FarmerInfoActivity.this.finish();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(FarmerInfoActivity.this, utils.getLocalizedString(R.string.login_to_continue), Toast.LENGTH_SHORT).show();

                            startActivity(new Intent(FarmerInfoActivity.this, LoginActivity.class));
                            FarmerInfoActivity.this.finish();

                        }

                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        GeneralUtils.showVolleyError(FarmerInfoActivity.this, error);

                        Toast.makeText(FarmerInfoActivity.this, utils.getLocalizedString(R.string.login_to_continue), Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(FarmerInfoActivity.this, LoginActivity.class));
                        FarmerInfoActivity.this.finish();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("user_id", mUserProfile.getUserId());
                params.put("user_type", mUserProfile.getUserType());
                params.put("password", mUserProfile.getUserPassword());

                Log.d(TAG, params.toString());

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(request);

        showProgressDialog();
    }*/

    private void hideSoftInputMethod() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

            if (inputMethodManager != null)
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.please_wait));
        }

        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }
}
