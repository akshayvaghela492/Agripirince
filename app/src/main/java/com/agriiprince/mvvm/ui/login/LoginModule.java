package com.agriiprince.mvvm.ui.login;


import android.arch.lifecycle.ViewModel;

import com.agriiprince.mvvm.model.User;
import com.agriiprince.mvvm.data.repository.LoginRepository;
import com.agriiprince.mvvm.data.repository.LoginRepositoryUseCase;
import com.agriiprince.mvvm.retrofit.service.LoginService;
import com.agriiprince.mvvm.di.ViewModelKey;
import com.agriiprince.mvvm.viewmodel.LoginViewModel;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import retrofit2.Retrofit;

@Module
public class LoginModule {

    @Provides
    LoginService providesLoginService(Retrofit retrofit) {
        return retrofit.create(LoginService.class);
    }

    @Provides
    LoginRepositoryUseCase providesLoginRepository(LoginService loginService, User user) {
        return new LoginRepository(loginService, user);
    }

    @Module(
            includes = {
                    LoginModule.class
            }
    )
    public interface BindLoginViewModel {

        @Binds
        @IntoMap
        @Singleton
        @ViewModelKey(LoginViewModel.class)
        ViewModel bindsLoginViewModel(LoginViewModel loginViewModel);
    }
}
