package com.agriiprince.mvvm.ui.subscription;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.agriiprince.mvvm.model.Subscription;
import com.android.databinding.library.baseAdapters.BR;

public class SubscriptionViewState extends BaseObservable {

    private final int EXPIRE_VALIDITY_DAYS = 0;
    private final int WARNING_VALIDITY_DAYS = 10;
    private final int VALIDITY_DAYS = 365;
/*

/**
     * value will be true if api calls are made and waiting for response.
     * indicate the user about progress
     *//*

    private boolean isLoading;
*/

    /**
     * when user clicks on to check the active features the remember the state of visibility.
     */
    private boolean isFeaturesExpanded;

    /**
     * max validity days of subscription
     */
    private int maxValidityDays = VALIDITY_DAYS;

    /**
     * number of days the subscription is valid
     * initial validity days is 0
     */
    private int validityDays;

    /**
     * coupon code entered by the user
     */
    private String couponCode;

    /**
     * subscription type can be basic or premium
     */
    private String subscriptionTypeName = "None";


    /**
     * subscription type can be basic or premium
     */
    private int subscriptionType = Subscription.NONE;


    /**
     * date on which user activated the subscription pack
     */
    private String activationDate = "NA";

    /**
     * date on which subscription pack expires
     */
    private String expiryDate = "NA";

    /**
     * subscription state based on validity days
     */
    private State subscriptionState = State.EXPIRED;

    public void onClickFeatureCard() {
        setFeaturesExpanded(!isFeaturesExpanded);
    }

    @Bindable
    public boolean isFeaturesExpanded() {
        return isFeaturesExpanded;
    }

    public void setFeaturesExpanded(boolean featuresExpanded) {
        isFeaturesExpanded = featuresExpanded;
        notifyPropertyChanged(BR.featuresExpanded);
    }

    @Bindable
    public int getValidityDays() {
        return validityDays;
    }

    public void setValidityDays(int validityDays) {
        this.validityDays = validityDays;
        onChangeValidity(validityDays);
        notifyPropertyChanged(BR.validityDays);
    }

    @Bindable
    public int getMaxValidityDays() {
        return maxValidityDays;
    }

    public void setMaxValidityDays(int maxValidityDays) {
        this.maxValidityDays = maxValidityDays;
        notifyPropertyChanged(BR.maxValidityDays);
    }

    @Bindable
    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    @Bindable
    public String getSubscriptionTypeName() {
        return subscriptionTypeName;
    }

    public void setSubscriptionTypeName(String subscriptionTypeName) {
        this.subscriptionTypeName = subscriptionTypeName;
        notifyPropertyChanged(BR.subscriptionTypeName);
    }

    public int getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(int subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    @Bindable
    public String getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(String activationDate) {
        this.activationDate = activationDate;
        notifyPropertyChanged(BR.activationDate);
    }

    @Bindable
    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
        notifyPropertyChanged(BR.expiryDate);
    }

    private void onChangeValidity(int validityDays) {
        if (validityDays < 0) {
            setValidityDays(0);
            return;
        }
        if (validityDays == EXPIRE_VALIDITY_DAYS) {
            subscriptionState = State.EXPIRED;
        } else if (validityDays <= WARNING_VALIDITY_DAYS) subscriptionState = State.WARNING;
        else subscriptionState = State.VALID;
    }


    public State getSubscriptionState() {
        return subscriptionState;
    }


    public void setDefault() {
        activationDate = "NA";
        expiryDate = "NA";

        subscriptionState = State.EXPIRED;

        subscriptionTypeName = "None";
        subscriptionType = Subscription.NONE;

        validityDays = 0;
        maxValidityDays = VALIDITY_DAYS;
    }

    public enum State {
        EXPIRED,
        WARNING,
        VALID
    }
}
