package com.agriiprince.mvvm.ui.homescreen;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.BuildConfig;
import com.agriiprince.R;
import com.agriiprince.activities.TutorialActivity;
import com.agriiprince.activities.checkprice.CheckPricingActivity;
import com.agriiprince.activities.farmer.makeprofit.MakeProfitActivity;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.data.prefs.PreferenceManager;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.profile.UserUpdate;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.UpdateDeleteUser;
import com.agriiprince.mvvm.retrofit.model.login.FarmerData;
import com.agriiprince.mvvm.retrofit.model.login.LoginUserDetails;
import com.agriiprince.mvvm.retrofit.model.login.Profile;
import com.agriiprince.mvvm.retrofit.model.login.ProfileDataDetails;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.mvvm.ui.disease.DiseaseInfoActivity;
import com.agriiprince.mvvm.ui.login.LoginActivity;
import com.agriiprince.mvvm.ui.registration.FarmerInfoActivity;
import com.agriiprince.activities.farmer.rss.RssMainActivity;
import com.agriiprince.activities.farmer.trackcrop.TrackCropActivity;
import com.agriiprince.activities.oe.OeInfoActivity;
import com.agriiprince.adapter.TipsPagerAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.coldstorage.MapsActivityColdStorage;
import com.agriiprince.fragments.FarmerCaFragment;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.FarmerTip;
import com.agriiprince.mvvm.model.User;
import com.agriiprince.mvvm.room.database.AppDatabase;
import com.agriiprince.mvvm.room.entity.TradeCropDetailEntity;
import com.agriiprince.mvvm.applevel.threadmanagement.AppExecutors;
import com.agriiprince.mvvm.applevel.analytics.FarmerRealtimeDBAnalytics;
import com.agriiprince.mvvm.ui.disease.DiseaseCropNamesActivity;
import com.agriiprince.mvvm.ui.subscription.SubscriptionActivity;
import com.agriiprince.mvvm.ui.tradecrop.activities.FIGFarmerListActivity;
import com.agriiprince.mvvm.ui.tradecrop.activities.FPOFigOperationsActivity;
import com.agriiprince.mvvm.ui.tradecrop.activities.TradeCropCheckFarmerDistActivity;
import com.agriiprince.mvvm.ui.tradecrop.activities.TradeCropManageActivity;
import com.agriiprince.mvvm.ui.weather.WeatherActivityFinal;
import com.agriiprince.mvvm.util.GeneralUtils;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.mvvm.viewmodel.TradeCropViewModel;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.mvvm.ui.customwidgets.WrapViewPager;
import com.agriiprince.utils.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.agriiprince.mvvm.applevel.analytics.FarmerFirebaseAnalytics.logEventFarmerClicked;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener,
        ViewPager.OnPageChangeListener, TipsPagerAdapter.OnClickTipListener {

    // Member variable for the Database
    private AppDatabase mDb;

    private TipsPagerAdapter mPagerAdapter;

    private OnHomeFragmentInteractionListener mListener;

    private LinearLayout commissionAgent;
    private LinearLayout checkPrice;
    private LinearLayout makeProfit;
    private LinearLayout weather;
    private LinearLayout pesticidesAndDiseases;
    private LinearLayout moversAndPackers;
    private LinearLayout warehouses;
    private LinearLayout news;
    private LinearLayout trackCrop;
    private LinearLayout checkSubscription;
    private LinearLayout tradeCrop;
    private LinearLayout farmerList;
    private LinearLayout checkFarmerDist;
    private LinearLayout fpoOperations;


    private TextView mTipNumberIndicator;

    UserProfile mUserProfile;
    DatabaseReference mFarmerRef;
    DatabaseReference mNumRef;
    String mUserId;
    String mMobile;
    String mName;
    String mCity;
    String mPin;
    Context mContext;

    Button mRegistrationButton;
    UserProfile userProfile;
    PrefManager mPrefManager;

    View mView;

    private StringUtils stringUtils;

    @Override
    public void onClickTry(int position) {
        Log.d("home", String.valueOf(position));
    }

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(false);
        return inflater.inflate(R.layout.fragment_farmer_home_screen, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDb = AppDatabase.getInstance(getActivity().getApplicationContext());

        mUserProfile = new UserProfile(getContext());
        //mUserProfile = ((FarmerHomeScreenActivity)getActivity()).getmUserProfile();
        mFarmerRef = ((FarmerHomeScreenActivity)getActivity()).getmFarmersRef();
        mNumRef = ((FarmerHomeScreenActivity)getActivity()).getmNumRef();
        mUserId = mUserProfile.getUserId();
        mMobile = mUserProfile.getUserMobile();
        mName = mUserProfile.getUserName();
        mCity = mUserProfile.getUserCity();
        mPin = mUserProfile.getUserPincode();
        mContext = getContext();

        stringUtils = AppController.getInstance().getStringUtils();

        mPagerAdapter = new TipsPagerAdapter();
        mPagerAdapter.setOnTipClickListener(this);

        WrapViewPager viewPager = view.findViewById(R.id.farmer_home_view_pager);
        viewPager.setAdapter(mPagerAdapter);
        viewPager.addOnPageChangeListener(this);

        TabLayout tabLayout = view.findViewById(R.id.farmer_home_tab);
        tabLayout.setupWithViewPager(viewPager);

        mTipNumberIndicator = view.findViewById(R.id.farmer_home_tip_number);

        commissionAgent = view.findViewById(R.id.farmer_home_commission_agent);
        commissionAgent.setOnClickListener(this);

        checkPrice = view.findViewById(R.id.farmer_home_check_price);
        checkPrice.setOnClickListener(this);

        makeProfit = view.findViewById(R.id.farmer_home_make_profit);
        makeProfit.setOnClickListener(this);

        weather = view.findViewById(R.id.farmer_home_weather);
        weather.setOnClickListener(this);

        pesticidesAndDiseases = view.findViewById(R.id.farmer_home_pesticides);
        pesticidesAndDiseases.setOnClickListener(this);

        moversAndPackers = view.findViewById(R.id.farmer_home_movers);
        moversAndPackers.setOnClickListener(this);

        warehouses = view.findViewById(R.id.farmer_home_warehouse);
        warehouses.setOnClickListener(this);

        news = view.findViewById(R.id.farmer_home_news);
        news.setOnClickListener(this);

        trackCrop = view.findViewById(R.id.farmer_home_track_your_crop);
        trackCrop.setOnClickListener(this);

        tradeCrop = view.findViewById(R.id.farmer_home_trade_crop);
        tradeCrop.setOnClickListener(this);

        farmerList = view.findViewById(R.id.farmer_home_farmer_list);
        farmerList.setOnClickListener(this);

        checkFarmerDist = view.findViewById(R.id.farmer_home_farmer_distribution);
        checkFarmerDist.setOnClickListener(this);

        fpoOperations = view.findViewById(R.id.farmer_home_fig_operations);
        fpoOperations.setOnClickListener(this);

        checkSubscription = view.findViewById(R.id.farmer_home_check_subscription);
        checkSubscription.setOnClickListener(this);

        userProfile = new UserProfile(getContext());

        mPrefManager = new PrefManager(getContext());



        mRegistrationButton = view.findViewById(R.id.start_registration_button);

        mRegistrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (BuildConfig.USER_TYPE.equals(User.Type.FARMER)) {
                    Intent intent = new Intent(getActivity(), FarmerInfoActivity.class);
                    intent.putExtra(FarmerInfoActivity.MOBILE_NUMBER, userProfile.getUserMobile());
                    intent.putExtra(FarmerInfoActivity.PASSWORD, userProfile.getUserPassword());
                    Logs.d("HomeScreenValues", userProfile.getUserMobile() + " " + userProfile.getUserPassword());
                    startActivity(intent);
                } else if (BuildConfig.USER_TYPE.equals(User.Type.OPERATIVE_EXECUTIVE)) {
                    Intent intent = new Intent(getActivity(), OeInfoActivity.class);
                    intent.putExtra(OeInfoActivity.MOBILE_NUMBER, userProfile.getUserMobile());
                    intent.putExtra(OeInfoActivity.PASSWORD, userProfile.getUserPassword());
                    startActivity(intent);
                }
            }
        });

        mView = view;

        //testing
        /*if (BuildConfig.DEBUG)
        {
            testTradeCropDBDeleteAll();
            testTradeCropDBInsert();
            testTradeCropDBInsert();
            testTradeCropDBInsert();
            //testTradeCropDBLoadAll();
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            setupViewModel();   //Forced
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            testTradeCropDBInsert();
            testTradeCropDBInsert();
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            testTradeCropDBInsert();
            testTradeCropDBInsert();
            //setupViewModelObserve();
        }*/
    }

    private void setupViewModel() {
        TradeCropViewModel viewModel = ViewModelProviders.of(this).get(TradeCropViewModel.class);
        viewModel.getCropDetails().observe(this, new Observer<List<TradeCropDetailEntity>>() {
            @Override
            public void onChanged(@Nullable List<TradeCropDetailEntity> taskEntries) {
                Log.e("ROOMTEST ===> ", "Updating list of tasks from LiveData in ViewModel");
                for (TradeCropDetailEntity tmp : taskEntries) {
                    Log.e("ROOMPRINT ===> ", tmp.getFarmer_id());
                }
            }
        });
    }

    private void testTradeCropDBInsert() {
        String randomfarmerid = GeneralUtils.getAlphaNumericString(20);
        final TradeCropDetailEntity dummyCropDetails = new TradeCropDetailEntity(randomfarmerid, "bbb", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.tradeCropDetailDao().insertCropDetail(dummyCropDetails);
                Log.e("ROOMTEST ===> ", "insert success");
            }
        });
    }

    private void testTradeCropDBLoadAll() {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                ArrayList<TradeCropDetailEntity> dummyCropList = new ArrayList<>();
                dummyCropList = (ArrayList) mDb.tradeCropDetailDao().loadAllCropDetails();
                Log.e("ROOMTEST ===> ", "load success");
                Log.e("ROOMTEST ===> ", dummyCropList.toString());
                for (TradeCropDetailEntity tmp : dummyCropList) {
                    Log.e("ROOMPRINT ===> ", tmp.getFarmer_id());
                }
            }
        });
    }

    private void testTradeCropDBDeleteAll() {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.tradeCropDetailDao().nukeTable();
                Log.e("ROOMTEST ===> ", "delete success");
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            mListener.onChangeLabel(stringUtils.getLocalizedString(R.string.home).toString());
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        setTexts(mView);
        setHintsViewPager(mView);

        Log.d("farmer_profile","login response jjj");
        checkIfIncomeFilled();

        if(mPrefManager.getFilledDetails())
            mRegistrationButton.setVisibility(View.GONE);
    }

    private void checkIfIncomeFilled() {
        // mProgressDialog.show();

        Profile params= new Profile();

        Log.e("farmer_profile","login response   1" + (mPrefManager.getUserId() + mPrefManager.getUserPassword() + mPrefManager.getMobileNo()));
        params.setFarmerId(mPrefManager.getUserId());
        params.setFarmerContact(mPrefManager.getMobileNo());

        API_Manager apiManager=new API_Manager();
        UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);
        Call<UserUpdate> call=retrofit_interface.updateprofile(mPrefManager.getAccessToken(),params);
        call.enqueue(new Callback<UserUpdate>() {
            @Override
            public void onResponse(Call<UserUpdate> call, retrofit2.Response<UserUpdate> response) {
                try {
                    Log.e("farmer_profile","login response"+response.body().getStatus());
                    Log.e("farmer_profile","login response"+response.body().getCode());
                    Log.e("farmer_profile","login response"+response.body().getMessage());

                    if (response.body().getCode() == 200 || response.body().getCode().toString().equals("200")) {
                        Log.e("farmer_profile","login response --> a");
                        FarmerData farmerData = response.body().getData();
                        List<ProfileDataDetails> profileDataDetails = farmerData.getData();
                        Log.e("farmer_profile","login response --> b");
                        int income = profileDataDetails.get(0).getAverageAnnualIncome();
                        Log.e("farmer_profile","login response --> " + income);
                        Log.e("farmer_profile","login response --> " + profileDataDetails.get(0));

                        if (income > 0){
                            //if(mPrefManager.getFilledDetails())
                                mRegistrationButton.setVisibility(View.GONE);
                                mPrefManager.setFilledDetails(true);
                        }

                    } else {
                        //Toast.makeText(LoginActivity.this, utils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<UserUpdate> call, Throwable t) {
                Log.d("farmer_profile","login fail"+ t.getMessage());
                //mProgressDialog.dismiss();
                //Utils.showToast(LoginActivity.this, t.getLocalizedMessage());
            }
        });
    }

    private void setTexts(View view) {

        ((TextView) view.findViewById(R.id.text_tips)).setText(stringUtils.getLocalizedString(R.string.tips));
        ((TextView) view.findViewById(R.id.text_features)).setText(stringUtils.getLocalizedString(R.string.available_services));

        ((TextView) view.findViewById(R.id.text_features)).setText(stringUtils.getLocalizedString(R.string.available_services));

        ((TextView) view.findViewById(R.id.text_ca)).setText(stringUtils.getLocalizedString(R.string.commission_agents_directory));
        ((TextView) view.findViewById(R.id.text_check_price)).setText(stringUtils.getLocalizedString(R.string.check_prices_and_profits));
        ((TextView) view.findViewById(R.id.text_make_profit)).setText(stringUtils.getLocalizedString(R.string.make_profit));
        ((TextView) view.findViewById(R.id.text_weather)).setText(stringUtils.getLocalizedString(R.string.weather));
        ((TextView) view.findViewById(R.id.text_disease)).setText(stringUtils.getLocalizedString(R.string.pesticides_crop_diseases));
        ((TextView) view.findViewById(R.id.text_warehouse)).setText(stringUtils.getLocalizedString(R.string.search_cold_storage_around_you));
        ((TextView) view.findViewById(R.id.text_news)).setText(stringUtils.getLocalizedString(R.string.daily_farming_news_advisory));
        ((TextView) view.findViewById(R.id.text_track_your_crop)).setText(stringUtils.getLocalizedString(R.string.track_your_crop));
        ((TextView) view.findViewById(R.id.text_check_subscription)).setText(stringUtils.getLocalizedString(R.string.check_subscription));

    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        String text = (i + 1) + "/" + mPagerAdapter.getCount();
        mTipNumberIndicator.setText(text);
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnHomeFragmentInteractionListener) {
            mListener = (OnHomeFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnInfoTabInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.farmer_home_commission_agent:

                Query q  = mFarmerRef.orderByChild("_id").equalTo(mUserId);
                q.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                    dataSnapshot, "click_home_commission_agent", mNumRef);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_home_commission_agent");

                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.farmer_home_frame, FarmerCaFragment.newInstance(), FarmerCaFragment.class.getSimpleName())
                        .addToBackStack(FarmerCaFragment.class.getSimpleName())
                        .commit();

                mListener.onChangeLabel(getString(R.string.commission_agents_directory));

                break;
            case R.id.farmer_home_news:

                Query q2  = mFarmerRef.orderByChild("_id").equalTo(mUserId);
                q2.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                    dataSnapshot, "click_home_news", mNumRef);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_home_news");

                startRssActivity();

                break;
            case R.id.farmer_home_check_price:

                Query q3  = mFarmerRef.orderByChild("_id").equalTo(mUserId);
                q3.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                    dataSnapshot, "click_home_check_price", mNumRef);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_home_check_price");

                startActivity(new Intent(getContext(), CheckPricingActivity.class));
                //          mListener.onChangeLabel(getString(R.string.commission_agent));


                break;
            case R.id.farmer_home_make_profit:

                Query q4  = mFarmerRef.orderByChild("_id").equalTo(mUserId);
                q4.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                    dataSnapshot, "click_home_make_profit", mNumRef);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_home_make_profit");

                startActivity(new Intent(getContext(), MakeProfitActivity.class));
                //            mListener.onChangeLabel(getString(R.string.commission_agent));

                break;
            case R.id.farmer_home_track_your_crop:

                Query q5  = mFarmerRef.orderByChild("_id").equalTo(mUserId);
                q5.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                    dataSnapshot, "click_home_track_your_crop", mNumRef);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_home_track_your_crop");

                startActivity(new Intent(getContext(), TrackCropActivity.class));
                //            mListener.onChangeLabel(getString(R.string.commission_agent));

                break;

            case R.id.farmer_home_trade_crop:

                startActivity(new Intent(getContext(), TradeCropManageActivity.class));
                //            mListener.onChangeLabel(getString(R.string.commission_agent));

                break;

            case R.id.farmer_home_farmer_list:

                startActivity(new Intent(getContext(), FIGFarmerListActivity.class));
                //            mListener.onChangeLabel(getString(R.string.commission_agent));

                break;

            case R.id.farmer_home_farmer_distribution:

                startActivity(new Intent(getContext(), TradeCropCheckFarmerDistActivity.class));
                //            mListener.onChangeLabel(getString(R.string.commission_agent));

                break;

            case R.id.farmer_home_fig_operations:

                startActivity(new Intent(getContext(), FPOFigOperationsActivity.class));
                //            mListener.onChangeLabel(getString(R.string.commission_agent));

                break;

            case R.id.farmer_home_check_subscription:

                Query q6  = mFarmerRef.orderByChild("_id").equalTo(mUserId);
                q6.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                    dataSnapshot, "click_home_subscription", mNumRef);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_home_subscription");

                startActivity(new Intent(getContext(), SubscriptionActivity.class));
                //            mListener.onChangeLabel(getString(R.string.commission_agent));

                break;
            case R.id.farmer_home_weather:

                Query q7  = mFarmerRef.orderByChild("_id").equalTo(mUserId);
                q7.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                    dataSnapshot, "click_home_weather", mNumRef);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_home_weather");

                startActivity(new Intent(getContext(), WeatherActivityFinal.class));
                //              mListener.onChangeLabel(getString(R.string.commission_agent));

                break;
            case R.id.farmer_home_pesticides:

                Query q8  = mFarmerRef.orderByChild("_id").equalTo(mUserId);
                q8.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                    dataSnapshot, "click_home_pesticides", mNumRef);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_home_pesticides");

                startActivity(new Intent(getContext(), DiseaseCropNamesActivity.class));
//                mListener.onChangeLabel(getString(R.string.commission_agent));

                break;
            case R.id.farmer_home_movers:

                mListener.onChangeLabel(getString(R.string.mover_packers));

                break;
            case R.id.farmer_home_warehouse:

                Query q9  = mFarmerRef.orderByChild("_id").equalTo(mUserId);
                q9.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            FarmerRealtimeDBAnalytics.saveEventFarmerClicks(
                                    dataSnapshot, "click_home_warehouse", mNumRef);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });

                logEventFarmerClicked(mContext, mMobile, mUserId, mName, mCity, mPin, "click_home_warehouse");

                startActivity(new Intent(getContext(), MapsActivityColdStorage.class));


                break;
        }
    }

    private void startRssActivity() {
/*
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.farmer_home_frame, RssFeedFragment.newInstance(), RssFeedFragment.class.getSimpleName())
                .addToBackStack(RssFeedFragment.class.getSimpleName())
                .commit();
*/

        startActivity(new Intent(getContext(), RssMainActivity.class));
    }


    private void setHintsViewPager(View view) {

        //String[] title = getResources().getStringArray(R.array.farmer_tips_title);
        //String[] body = getResources().getStringArray(R.array.farmer_tips_body);

        //TextView test = new TextView(getContext());
        //test.setText(stringUtils.getLocalizedString(R.string.tips));
        //Log.d("TIPS_TEXT ---> ", test.getText().toString());

        TextView tv = new TextView(getContext());

        ArrayList<SpannableString> titlesSpannableStringList = new ArrayList<>();
        titlesSpannableStringList = stringUtils.getLocalizedStringArray(R.array.farmer_tips_title);

        ArrayList<String> titlesStringList = new ArrayList<>();
        for(int i = 0; i < titlesSpannableStringList.size(); i++) {
            tv.setText(titlesSpannableStringList.get(i));
            Log.d("TIPS_TEXT ---> ", tv.getText().toString());
            titlesStringList.add(tv.getText().toString());
        }

        String[] title = titlesStringList.toArray(new String[titlesStringList.size()]);


        ArrayList<SpannableString> bodySpannableStringList = new ArrayList<>();
        bodySpannableStringList = stringUtils.getLocalizedStringArray(R.array.farmer_tips_body);

        ArrayList<String> bodyStringList = new ArrayList<>();
        for(int i = 0; i < bodySpannableStringList.size(); i++) {
            tv.setText(bodySpannableStringList.get(i));
            Log.d("TIPS_TEXT ---> ", tv.getText().toString());
            bodyStringList.add(tv.getText().toString());
        }

        String[] body = bodyStringList.toArray(new String[bodyStringList.size()]);


        int[][] tipBackgroundGradientColors = new int[][]{
                {Color.parseColor("#36BA89"), Color.parseColor("#A55EEA")},
                {Color.parseColor("#36BA89"), Color.parseColor("#F39C12")},
                {Color.parseColor("#2E86DE"), Color.parseColor("#36BA89")},
                {Color.parseColor("#A55EEA"), Color.parseColor("#3C8FE1")},
                {Color.parseColor("#FA8231"), Color.parseColor("#EB3B5A")},
                {Color.parseColor("#B33771"), Color.parseColor("#2E86DE")},
                {Color.parseColor("#2E86DE"), Color.parseColor("#EB3B5A")},
                {Color.parseColor("#D35400"), Color.parseColor("#8854D0")},
                {Color.parseColor("#EB3B5A"), Color.parseColor("#8854D0")},
                //    {Color.parseColor("#A55EEA"), Color.parseColor("#EB3B5A")},
        };

        int[] images = new int[]{
                R.drawable.ic_renew,
                R.drawable.ic_commission_agent,
                R.drawable.ic_pricecheck,
                R.drawable.ic_make_profit,
                R.drawable.ic_weather,
                R.drawable.ic_pesticides_diseases,
                R.drawable.ic_warehouses,
                R.drawable.ic_crop,
//                R.drawable.ic_movers_white,
        };

        if(mPagerAdapter.getCount()>0) {
            try {
                for (int i = 0; i < 8; i++) {
                    FarmerTip farmerTip = new FarmerTip(title[i], body[i], images[i], tipBackgroundGradientColors[i]);
                    mPagerAdapter.addFarmerTipByPos(farmerTip,i);

                    Log.d("home", title[i] + "   " + tipBackgroundGradientColors[i][0] + "   " + tipBackgroundGradientColors[i][1]);
                }
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
        else
        {
            try {
                for (int i = 0; i < 8; i++) {
                    FarmerTip farmerTip = new FarmerTip(title[i], body[i], images[i], tipBackgroundGradientColors[i]);
                    mPagerAdapter.addFarmerTip(farmerTip);

                    Log.d("home", title[i] + "   " + tipBackgroundGradientColors[i][0] + "   " + tipBackgroundGradientColors[i][1]);
                }
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }

        String text = 1 + "/" + mPagerAdapter.getCount();
        mTipNumberIndicator.setText(text);

    }

    public interface OnHomeFragmentInteractionListener {

        void onChangeLabel(String label);
    }
}
