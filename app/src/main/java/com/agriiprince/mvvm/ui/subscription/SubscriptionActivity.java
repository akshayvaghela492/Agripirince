package com.agriiprince.mvvm.ui.subscription;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.activities.TutorialActivity;
import com.agriiprince.databinding.ActivitySubscriptionBinding;
import com.agriiprince.mvvm.retrofit.dto.subscription.CouponVerification;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionInfo;
import com.agriiprince.mvvm.retrofit.model.subscription.SubscriptionDetailList;
import com.agriiprince.mvvm.ui.base.BaseActivity;
import com.agriiprince.mvvm.ui.refer.ReferFriendsActivity;
import com.agriiprince.mvvm.ui.LiveDataObserver;
import com.agriiprince.mvvm.viewmodel.SubscriptionViewModel;
import com.agriiprince.mvvm.viewmodel.SubscriptionViewModelUseCase;

import java.util.List;

import javax.inject.Inject;

import static com.agriiprince.mvvm.model.Subscription.BASIC;
import static com.agriiprince.mvvm.model.Subscription.NONE;
import static com.agriiprince.mvvm.model.Subscription.PREMIUM;

public class SubscriptionActivity extends BaseActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, SubscriptionActivity.class);
    }

    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    private SubscriptionViewModelUseCase mViewModel;

    private ActivitySubscriptionBinding mBinding;

    @Override
    protected void setViewAndPerformDataBinding() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_subscription);
    }

    @Override
    protected void initControl() {
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(SubscriptionViewModel.class);
    }

    @Override
    protected void initViewControl() {
        setupToolbar(mBinding.toolbar, true, null);
        mBinding.setLifecycleOwner(this);

    }

    @Override
    protected void initTextViews() {

        setBilingualText(mBinding.titleMessage, R.string.your_subscription_is_expired);
        setBilingualText(mBinding.daysLabel, R.string.days);

        setBilingualText(mBinding.subscriptionTypeLabel, R.string.subscription_type);
        setBilingualText(mBinding.activationDateLabel, R.string.activation_date);
        setBilingualText(mBinding.expiryDateLabel, R.string.expiry_date);

        setBilingualText(mBinding.activeFeatureLabel, R.string.view_active_features);
        setBilingualText(mBinding.features.caLabel, R.string.commission_agent);
        setBilingualText(mBinding.features.caFeature, R.string.ca_feature);
        setBilingualText(mBinding.features.checkPriceLabel, R.string.check_price);
        setBilingualText(mBinding.features.checkPriceFeature, R.string.check_price_feature);
        setBilingualText(mBinding.features.weatherLabel, R.string.weather);
        setBilingualText(mBinding.features.weatherFeature, R.string.weather_feature);
        setBilingualText(mBinding.features.warehousesLabel, R.string.warehouses);
        setBilingualText(mBinding.features.warehousesFeature, R.string.warehouses_feature);

        setBilingualText(mBinding.referLabel, R.string.refer_friend__get_subscription_label);
        setBilingualText(mBinding.referBtn, R.string.refer);

        setBilingualText(mBinding.couponLabel, R.string.have_a_coupon_code);
        setBilingualText(mBinding.couponBtn, R.string.apply);
        setBilingualHint(mBinding.couponInput, R.string.enter_coupon_code);

        setBilingualText(mBinding.poorFarmerLabel, R.string.poor_farmer);
        setBilingualText(mBinding.poorFarmer, R.string.poor_farmer_apply_description);
        setBilingualText(mBinding.poorFarmerBtn, R.string.apply);



        setBilingualText(mBinding.basic.label, R.string.basic_subscription);
        setBilingualText(mBinding.basic.featureLabel, R.string.basic_features);
        setBilingualText(mBinding.basic.activate, R.string.activate);
        setBilingualText(mBinding.basic.tvPerYear, R.string.per_year, true);
        setBilingualText(mBinding.basic.learnMore, R.string.learn_more);

        setBilingualText(mBinding.premium.label, R.string.premium_subscription);
        setBilingualText(mBinding.premium.featureLabel, R.string.exclusive_features);
        setBilingualText(mBinding.premium.activate, R.string.activate);
        setBilingualText(mBinding.premium.tvPerYear, R.string.per_year, true);
        setBilingualText(mBinding.premium.learnMore, R.string.learn_more);

        ImageView imgview_help =  findViewById(R.id.iv_help);

        imgview_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SubscriptionActivity.this, TutorialActivity.class);
                intent.putExtra("code", "SUBSD");
                startActivity(intent);
            }
        });
    }

    @Override
    protected void initListener() {
        mBinding.poorFarmerBtn.setOnClickListener(onClickPoorFarmer);
        mBinding.referBtn.setOnClickListener(onClickReferFriends);

        mBinding.basic.activate.setOnClickListener(onClickActivateBasic);
        mBinding.premium.activate.setOnClickListener(onClickActivatePremium);
        mBinding.couponBtn.setOnClickListener(onClickCouponApply);

        mViewModel.getViewState().observe(this, viewStateObserver);

        getUserSubscription();

        mViewModel.getSubscriptions().observe(this, subscriptionObserver);
    }


    private View.OnClickListener onClickPoorFarmer = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(mBinding.root.getId(), BplCardFragment.newInstance());
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    };


    private View.OnClickListener onClickActivateBasic = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(SubscriptionActivity.this, SubscriptionPaymentActivity.class);
            startActivity(i);
        }
    };

    private View.OnClickListener onClickActivatePremium = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(SubscriptionActivity.this, SubscriptionPaymentActivity.class);
            startActivity(i);
        }
    };


    private View.OnClickListener onClickReferFriends = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(ReferFriendsActivity.newIntent(SubscriptionActivity.this));
        }
    };

    private View.OnClickListener onClickCouponApply = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mBinding.couponInput.getText() != null &&
                    !TextUtils.isEmpty(mBinding.couponInput.getText().toString())) {

                applyCouponCode(mBinding.couponInput.getText().toString());

            } else {
                mBinding.couponInput.setError(getBilingualString(R.string.enter_coupon_code));
                mBinding.couponInput.requestFocus();
            }
        }
    };

    private Observer<SubscriptionViewState> viewStateObserver = new Observer<SubscriptionViewState>() {
        @Override
        public void onChanged(@Nullable SubscriptionViewState subscriptionViewState) {
            if (subscriptionViewState != null) onViewStateChange(subscriptionViewState);
        }
    };


    private LiveDataObserver<List<SubscriptionDetailList>> subscriptionObserver = new LiveDataObserver<List<SubscriptionDetailList>>() {
        @Override
        protected void onLoading() {
            showProgressDialog();
        }

        @Override
        protected void onSuccess(List<SubscriptionDetailList> subscriptions) {
            hideProgressDialog();
            if (subscriptions != null && subscriptions.size() > 0) {
                for (SubscriptionDetailList subscription : subscriptions) {
                    if (subscription.isBasic()) {
                        mBinding.basic.setSubscription(subscription);
                    } else {
                        mBinding.premium.setSubscription(subscription);
                    }
                }

                mBinding.subscriptionCards.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onFail(Throwable t) {
            hideProgressDialog();

        }

        @Override
        protected void onNetworkFail() {
            hideProgressDialog();

        }
    };

    private void getUserSubscription() {
        mViewModel.getUserSubscription()
                .observe(this, new LiveDataObserver<SubscriptionInfo>() {
                    @Override
                    protected void onLoading() {
                        showProgressDialog();
                    }

                    @Override
                    protected void onSuccess(SubscriptionInfo data) {
                        hideProgressDialog();
                    }
                    @Override
                    protected void onFail(Throwable t) {
                        hideProgressDialog();
                    }

                    @Override
                    protected void onNetworkFail() {
                        hideProgressDialog();
                    }
                });
    }

    private void applyCouponCode(@NonNull String couponCode) {
        mViewModel.applyCoupon(couponCode)
                .observe(this, new LiveDataObserver<CouponVerification>() {
                    @Override
                    protected void onLoading() {
                        showProgressDialog();
                    }

                    @Override
                    protected void onSuccess(CouponVerification data) {
                        hideProgressDialog();

                        if (data != null) {
                           // if (data.getErrorCode() == 100) {
                            if (data.getCode() == 200) {
                                //if (data.isValidCoupon()) {
                                if (data.getData().getValidCoupon()) {

                                    //if (data.isTrialActivated()) {
                                    if (data.getData().getTrialActivated()) {
                                        getUserSubscription();
                                    } else {
                                        // TODO
                                    }

                                } else {
                                    showDefaultSnackBar(getBilingualString(R.string.invalid_coupon_dialog_message));
                                }
                           // } else if (data.getErrorCode() == 101 && !data.isValidCoupon()) {
                            } else if (data.getCode() == 400 && !data.getData().getValidCoupon()) {
                                showDefaultSnackBar(getBilingualString(R.string.invalid_coupon_dialog_message));
                            }
                        }
                    }

                    @Override
                    protected void onFail(Throwable t) {
                        hideProgressDialog();
                        Toast.makeText(SubscriptionActivity.this, getBilingualString(getErrorMessageId()), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    protected void onNetworkFail() {
                        hideProgressDialog();
                        Toast.makeText(SubscriptionActivity.this, getBilingualString(getErrorMessageId()), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void onViewStateChange(SubscriptionViewState viewState) {
        switch (viewState.getSubscriptionState()) {
            case VALID:
                mBinding.progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.circular_progress_bar_green));
                mBinding.validityDays.setTextColor(getResources().getColor(R.color.colorPrimaryFarmer));
                mBinding.daysLabel.setTextColor(getResources().getColor(R.color.colorPrimaryFarmer));
                setBilingualText(mBinding.titleMessage, R.string.your_subscription_ends_in);
                break;
            case WARNING:
                mBinding.progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.circular_progress_bar_orange));
                mBinding.validityDays.setTextColor(getResources().getColor(R.color.colorAccentFarmer));
                mBinding.daysLabel.setTextColor(getResources().getColor(R.color.colorAccentFarmer));
                setBilingualText(mBinding.titleMessage, R.string.your_subscription_ends_in);
                break;
            case EXPIRED:
                mBinding.progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.circular_progress_bar_orange));
                mBinding.validityDays.setTextColor(getResources().getColor(R.color.red));
                mBinding.daysLabel.setTextColor(getResources().getColor(R.color.red));
                setBilingualText(mBinding.titleMessage, R.string.your_subscription_is_expired);
                break;
            default:
                mBinding.progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.circular_progress_bar_orange));
                mBinding.validityDays.setTextColor(getResources().getColor(R.color.red));
                mBinding.daysLabel.setTextColor(getResources().getColor(R.color.red));
                setBilingualText(mBinding.titleMessage, R.string.your_subscription_is_expired);
                break;

        }

        switch (viewState.getSubscriptionType()) {
            case NONE:
                viewState.setSubscriptionTypeName(getBilingualString(R.string.none));
                break;
            case BASIC:
                viewState.setSubscriptionTypeName(getBilingualString(R.string.basic));
                if (viewState.getSubscriptionState() == SubscriptionViewState.State.WARNING)
                    setBilingualText(mBinding.basic.activate, R.string.pay_now);
                break;
            case PREMIUM:
                viewState.setSubscriptionTypeName(getBilingualString(R.string.premium));
                if (viewState.getSubscriptionState() == SubscriptionViewState.State.WARNING)
                    setBilingualText(mBinding.premium.activate, R.string.pay_now);
                break;
            default:
                viewState.setSubscriptionTypeName(getBilingualString(R.string.none));
                break;
        }

        mBinding.setViewState(viewState);
        mBinding.executePendingBindings();
    }


}
