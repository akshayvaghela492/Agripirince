package com.agriiprince.mvvm.ui.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.utils.StringUtils;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public abstract class BaseActivity extends AppCompatActivity implements
        HasSupportFragmentInjector, BaseFragmentInteraction {

    @Inject
    DispatchingAndroidInjector<Fragment> mFragmentDispatchingAndroidInjector;

    @Inject
    PrefManager mPrefManager;

    @Inject
    StringUtils mStringUtils;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        performDependencyInjection();
        super.onCreate(savedInstanceState);
        setViewAndPerformDataBinding();
        initControl();
        initViewControl();
        initTextViews();
        initListener();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return mFragmentDispatchingAndroidInjector;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Perform dependency injection.
     */
    private void performDependencyInjection() {
        AndroidInjection.inject(this);
    }

    /**
     * set content view and bind the layout
     */
    protected abstract void setViewAndPerformDataBinding();

    /**
     * Init control.
     */
    protected abstract void initControl();

    /**
     * Init view control.
     */
    protected abstract void initViewControl();

    /**
     * set texts for all the views
     */
    protected abstract void initTextViews();

    /**
     * Init listener.
     */
    protected abstract void initListener();

    /**
     * set the support toolbar and home icon, and the tile
     * @param toolbar
     * @param enableHomeAsUp
     * @param title
     */
    protected void setupToolbar(Toolbar toolbar, boolean enableHomeAsUp, String title) {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(enableHomeAsUp);
            getSupportActionBar().setTitle(title);
        }
    }

    /**
     * shows the progress dialog. dialog cannot be dismissed on touch screen
     */
    @Override
    public void showProgressDialog() {
        if (mProgressDialog == null) { // if dialog is not created then create
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(mStringUtils.getLocalizedString(R.string.please_wait));
        }

        // check if progress dialog is showing then no need show show again
        if (! mProgressDialog.isShowing()) mProgressDialog.show();
    }

    /**
     * hide the progress dialog
     */
    @Override
    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    @Override
    public void showDefaultSnackBar(@NonNull String message) {
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT).show();
    }


    /**
     * hides soft input keyboard
     */
    @Override
    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) hideKeyboard(view);
    }

    /**
     * hides soft input keyboard
     *
     * @param view view which has the focus
     */
    @Override
    public void hideKeyboard(@NonNull View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }




















    /**
     * returns bilingual string for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected String getBilingualString(@StringRes int id){
        return mStringUtils.getLocalizedString(id).toString();
    }

    /**
     * set bilingual string to the text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualText(TextView textView, @StringRes int id){
        this.setBilingualText(textView, id, false);
    }

    /**
     * set bilingual string to the text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualText(TextView textView, @StringRes int id, boolean isSingleLine){
        textView.setText(mStringUtils.getLocalizedString(id, isSingleLine));
    }

    /**
     * set bilingual string to the text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualText(AppCompatTextView textView, @StringRes int id){
        this.setBilingualText(textView, id, false);
    }

    /**
     * set bilingual string to the text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualText(AppCompatTextView textView, @StringRes int id, boolean isSingleLine){
        textView.setText(mStringUtils.getLocalizedString(id, isSingleLine));
    }

    /**
     * set bilingual string to the edit text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualText(EditText editText, @StringRes int id){
        this.setBilingualText(editText, id, false);
    }

    /**
     * set bilingual string to the edit text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualText(EditText editText, @StringRes int id, boolean isSingleLine){
        editText.setText(mStringUtils.getLocalizedString(id, isSingleLine));
    }

    /**
     * set bilingual string to the edit text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualText(AppCompatEditText editText, @StringRes int id){
        this.setBilingualText(editText, id, false);
    }

    /**
     * set bilingual string to the edit text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualText(AppCompatEditText editText, @StringRes int id, boolean isSingleLine){
        editText.setText(mStringUtils.getLocalizedString(id, isSingleLine));
    }

    /**
     * set bilingual hint to the edit text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualHint(EditText editText, @StringRes int id){
        this.setBilingualHint(editText, id, false);
    }

    /**
     * set bilingual hint to the edit text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualHint(EditText editText, @StringRes int id, boolean isSingleLine){
        editText.setHint(mStringUtils.getLocalizedString(id, isSingleLine));
    }

    /**
     * set bilingual hint to the edit text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualHint(AppCompatEditText editText, @StringRes int id){
        this.setBilingualHint(editText, id, false);
    }

    /**
     * set bilingual hint to the edit text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualHint(AppCompatEditText editText, @StringRes int id, boolean isSingleLine){
        editText.setHint(mStringUtils.getLocalizedString(id, isSingleLine));
    }
}
