package com.agriiprince.mvvm.ui.login;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.agriiprince.BuildConfig;
import com.agriiprince.R;
import com.agriiprince.databinding.FragmentLoginBinding;
import com.agriiprince.mvvm.model.User;
import com.agriiprince.mvvm.ui.base.BaseFragment;
import com.agriiprince.mvvm.util.DisplayUtils;
import com.agriiprince.mvvm.viewmodel.LoginViewModel;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.mvvm.util.UserUtils;

import java.util.List;

import javax.inject.Inject;

import static com.agriiprince.appcontroller.AppController.loggedOut;

public class LoginFragment extends BaseFragment {

    private static final String TAG = "LoginFragment";

    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    StringUtils mStringUtils;

    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    private LoginViewModel mLoginViewModel;

    private FragmentLoginBinding mBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_login;
    }

    @Override
    protected void performDataBinding(View view) {
        mBinding = DataBindingUtil.bind(view);
    }

    @Override
    protected void initControl() {
        mLoginViewModel = ViewModelProviders.of(getActivity(), mViewModelFactory).get(LoginViewModel.class);
    }

    @Override
    protected void initViewControl() {
        mBinding.setLoginViewModel(mLoginViewModel);
        mBinding.executePendingBindings();

        if (mBinding.loginPassword.getText() != null) mBinding.loginPassword.getText().clear();

        mBinding.loginPasswordLayout.findViewById(R.id.text_input_password_toggle).performClick();

        if (getContext() != null)
            mBinding.loginOtp.setMinHeight(DisplayUtils.dpToPx(getContext(), 36));

        setUserTypeAdapter();
    }

    @Override
    protected void initTextViews() {
        setBilingualText(mBinding.signIn, R.string.sign_in);
        setBilingualText(mBinding.dontHaveAccount, R.string.don_t_have_an_account);
        setBilingualText(mBinding.loginBtn, R.string.login);
        setBilingualText(mBinding.loginOtp, R.string.login_with_otp);
        setBilingualText(mBinding.loginRegister, R.string.register);
    }

    @Override
    protected void initListener() {
        mBinding.loginRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginViewModel.onClickSingUpFragment();
            }
        });

        mBinding.loginOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loggedOut = 0;

                String userType = UserUtils.getLoginUserType(mBinding.loginUserType.getSelectedItemPosition());

                String phoneNumber = null;
                if (mBinding.mobile.getText() != null) phoneNumber = mBinding.mobile.getText().toString();

                if (validateOtpLoginDetails(phoneNumber, userType)) {
                    mLoginViewModel.verifyMobile();
                }
            }
        });

        mBinding.loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loggedOut = 0;

                String userType = UserUtils.getLoginUserType(mBinding.loginUserType.getSelectedItemPosition());

                String phoneNumber = null;
                String password = null;
                if (mBinding.mobile.getText() != null) phoneNumber = mBinding.mobile.getText().toString();
                if (mBinding.loginPassword.getText() != null) password = mBinding.loginPassword.getText().toString();

                if (validateLoginDetails(phoneNumber, password, userType)) {
                    mLoginViewModel.loginByPassword();
                }
            }
        });
    }

    private void setUserTypeAdapter() {
        if (getContext() == null) return;

        List<String> mUserTypes = UserUtils.getUserTypeNames(mStringUtils);

        ArrayAdapter<String> userTypeAdapter =
                new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, mUserTypes);
        userTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mBinding.loginUserType.setAdapter(userTypeAdapter);

        if (BuildConfig.USER_TYPE.equals(User.Type.FARMER))
            mBinding.loginUserType.setSelection(2); // default farmer
        else if (BuildConfig.USER_TYPE.equals(User.Type.OPERATIVE_EXECUTIVE))
            mBinding.loginUserType.setSelection(4); // default oe

        mBinding.loginUserType.setEnabled(false);
        mBinding.loginUserType.setClickable(false);

        mBinding.loginUserType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String userType = UserUtils.getLoginUserType(position);
                mLoginViewModel.onChangeLoginUserType(userType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private boolean validateLoginDetails(String phoneNumber, String password, String userType) {
        if (TextUtils.isEmpty(phoneNumber) || phoneNumber.length() < 10) {
            mBinding.mobile.setError(getString(R.string.enter_valid_number));
            mBinding.mobile.requestFocus();
            return false;

        } else if (TextUtils.isEmpty(password)) {
            mBinding.loginPassword.setError(getString(R.string.enter_password));
            mBinding.loginPassword.requestFocus();
            return false;

        } else if (password.contains(" ")) {
            mBinding.loginPassword.setError(getString(R.string.password_no_space));
            mBinding.loginPassword.requestFocus();
            return false;

        } else if (TextUtils.isEmpty(userType)) {
            if (getContext() != null)
                Toast.makeText(getContext(), getString(R.string.enter_user_type), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateOtpLoginDetails(String phoneNumber, String userType) {
        if (TextUtils.isEmpty(phoneNumber) || phoneNumber.length() < 10) {
            mBinding.mobile.setError(getString(R.string.enter_valid_number));
            mBinding.mobile.requestFocus();
            return false;

        } else if (TextUtils.isEmpty(userType)) {
            Toast.makeText(getContext(), getString(R.string.enter_user_type), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
