package com.agriiprince.mvvm.ui.registration.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.model.SelectedCropModel;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.mvvm.retrofit.model.croplist.CropByClass;
import com.agriiprince.utils.StringUtils;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dtrah on 11/21/2018.
 */

class VarietyAdapter extends RecyclerView.Adapter<VarietyAdapter.MyHolder> {

    Context c;
    ArrayList<SelectedCropModel> cropModel;
    ImageAdapter.ViewHolder parentHolder;
    SelectedCropModel parentCrop;
    private StringUtils utils;
    List<Crop> mCrops;

    public VarietyAdapter(Context c, ArrayList<SelectedCropModel> cropModel, ImageAdapter.ViewHolder holder, SelectedCropModel crop, List<CropByClass> cropArray, List<Crop> mCrops) {
        this.c = c;
        this.cropModel = cropModel;
        this.parentHolder = holder;
        this.parentCrop = crop;
        utils = AppController.getInstance().getStringUtils();
        this.mCrops = mCrops;
    }

    //INITIALIE VH
    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.variety_item, parent, false);
        MyHolder holder = new MyHolder(v);
        return holder;
    }

    //BIND DATA
    @Override
    public void onBindViewHolder(final MyHolder holder, int position) {
        holder.nameTxt.setText(utils.getBilingualStringForApi(cropModel.get(position).getCrop(), Crop.getSecondNameByCropName(mCrops, cropModel.get(position).getCrop())));
        holder.checkBox.setChecked(cropModel.get(position).getSelected());


        holder.checkBox.setTag(position);
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View tempview = (View) holder.checkBox.getTag(R.integer.btnplusview);
                Integer pos = (Integer) holder.checkBox.getTag();

                if (cropModel.get(pos).getSelected()) {
                    cropModel.get(pos).setSelected(false);
                } else {
                    cropModel.get(pos).setSelected(true);
                }
                if ((null != parentCrop) && (null != parentHolder)) {
                    int selectedProducts = 0;
                    for (SelectedCropModel crop : cropModel) {
                        if (crop.getSelected())
                            selectedProducts++;
                    }

                    if (0 == selectedProducts) {
                        parentCrop.setSelected(false);
                        parentHolder.checkBox.setChecked(false);
                    } else {
                        parentCrop.setSelected(true);
                        parentHolder.checkBox.setChecked(true);
                    }

                }
                parentCrop.setVarieties(cropModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cropModel.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView nameTxt;
        protected CheckBox checkBox;

        public MyHolder(View itemView) {
            super(itemView);
            nameTxt = (TextView) itemView.findViewById(R.id.nameTxt);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkvariety);
            checkBox.setTag(R.integer.btnplusview, itemView);
        }
    }
}
