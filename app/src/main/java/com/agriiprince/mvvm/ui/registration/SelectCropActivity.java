package com.agriiprince.mvvm.ui.registration;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.agriiprince.R;
import com.agriiprince.mvvm.applevel.analytics.FarmerRealtimeDBAnalytics;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class SelectCropActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    boolean fromProfile;
    boolean fromOeProfile = false;

    FirebaseDatabase mFDatabase;
    DatabaseReference mRef;
    DatabaseReference mFarmersRef;
    DatabaseReference mNumRef;
    DatabaseReference mKeyRef;
    UserProfile mUserProfile;
    String mUserId;
    long mTimeStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                fromProfile = getIntent().getExtras().getBoolean("fromProfile");
            }
        }
        if (!fromProfile) {
            setTheme(R.style.AppTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_crop);

        mUserProfile = new UserProfile(this);
        mUserId = mUserProfile.getUserId();
        mFDatabase = FirebaseDatabase.getInstance();
        mRef = mFDatabase.getReference("analytics");
        mFarmersRef = mRef.child("farmers");
        mNumRef = mFarmersRef.child(mUserProfile.getUserMobile());

        final Toolbar toolbar = findViewById(R.id.toolbar_crop);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle(getResources().getString(R.string.choose_crops));

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        Log.e("SELECT CROP ACTIVITY", " ->>>>>>>> onCreate");

        if (!fromProfile) {
            Log.e("SELECT CROP ACTIVITY", " ->>>>>>>> Orange");
            tabLayout.setBackgroundColor(getResources().getColor(R.color.loginBtn1));
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.loginBtn1)));
        }
    }

    /**
     * @Override public boolean onSupportNavigateUp() {
     * Log.e("SELECT CROP ACTIVITY"," ->>>>>>>> onSupportNavigateUp");
     * onBackPressed();
     * return true;
     * }
     * @Override public boolean onKeyDown(int keyCode, KeyEvent event)  {
     * Log.e("SELECT CROP ACTIVITY"," ->>>>>>>> onKeyDown");
     * if (keyCode == KeyEvent.KEYCODE_BACK
     * && event.getRepeatCount() == 0) {
     * Log.e("SELECT CROP ACTIVITY", " ->>>>>>>> onKeyDown Called");
     * onBackPressed();
     * return true;
     * }
     * return super.onKeyDown(keyCode, event);
     * }
     */


    /*@Override
    public void onBackPressed() {
        Log.e("SELECT CROP ACTIVITY", " ->>>>>>>> onBackPressed Called");
        if(currentScreen == 1)
        {
            Log.e("SELECT CROP ACTIVITY", " ->>>>>>>> 1");
            super.onBackPressed();
        }
        else if(currentScreen == 2)
        {
            Log.e("SELECT CROP ACTIVITY", " ->>>>>>>> 2");
            super.onBackPressed();
        }
        else if(currentScreen == 3)
        {
            Log.e("SELECT CROP ACTIVITY", " ->>>>>>>> 3");
            super.onBackPressed();
        }
    }*/

    public interface OnBackClickListener {
        boolean onBackClick();
    }

    private OnBackClickListener onBackClickListener;

    public void setOnBackClickListener(OnBackClickListener onBackClickListener) {
        this.onBackClickListener = onBackClickListener;
    }

    @Override
    public void onBackPressed() {
        //if (onBackClickListener != null && onBackClickListener.onBackClick()) {
        //    return;
        //}
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        onBackPressedListener = null;
        super.onDestroy();
    }

    private void setupViewPager(ViewPager viewPager) {
        final ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(AddInterestedCropFragment.newInstance(fromProfile, fromOeProfile), getResources().getString(R.string.addcropmsg));
        adapter.addFragment(new ShowInterestedCropFragment(), getResources().getString(R.string.showcropmsg));
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                adapter.notifyDataSetChanged(); //this line will force all pages to be loaded fresh when changing between fragments
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            if (fromProfile) {
                Intent intent = new Intent(this, FarmerProfileActivity.class); //msgdisplayActivity is activity which display the selected message.
                startActivityForResult(intent, 1);
            }
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected OnBackPressedListener onBackPressedListener;

    public interface OnBackPressedListener {
        void doBack();
    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTimeStart = System.currentTimeMillis();
    }

    @Override
    protected void onStop() {
        super.onStop();
        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - mTimeStart;
        final int elapsedSeconds = (int)(tDelta / 1000.0);

        Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    FarmerRealtimeDBAnalytics.saveEventFarmerTimeSpent(
                            dataSnapshot, "time_spent_crop_selection", mNumRef, elapsedSeconds);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
}
