package com.agriiprince.mvvm.ui.tradecrop.activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.agriiprince.R;
import com.agriiprince.adapter.ViewPagerAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.databinding.ActivityTradeCropBinding;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.ui.tradecrop.tabs.ManageCropSectionsPagerAdapter;
import com.agriiprince.utils.StringUtils;

public class TradeCropManageActivity extends AppCompatActivity {

    private static final String TAG = com.agriiprince.activities.farmer.trackcrop.TrackCropActivity.class.getName();

    private ViewPagerAdapter viewPagerAdapter;

    StringUtils utils;

    private ActivityTradeCropBinding binding;
    private UserProfile mUserProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trade_crop);
        mUserProfile = new UserProfile(this);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ManageCropSectionsPagerAdapter sectionsPagerAdapter = new ManageCropSectionsPagerAdapter(this, getSupportFragmentManager());
        binding.viewpager.setAdapter(sectionsPagerAdapter);
        binding.tabs.setupWithViewPager(binding.viewpager);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();

                Context context = view.getContext();
                Intent intent = new Intent(context, TradeCropAddActivity.class);
                context.startActivity(intent);
            }
        });

        utils = AppController.getInstance().getStringUtils();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

