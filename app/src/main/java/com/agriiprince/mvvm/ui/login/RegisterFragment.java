package com.agriiprince.mvvm.ui.login;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.agriiprince.BuildConfig;
import com.agriiprince.R;
import com.agriiprince.databinding.FragmentRegisterBinding;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.model.User;
import com.agriiprince.mvvm.retrofit.dto.login.UserExist;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.mvvm.ui.base.BaseFragment;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.mvvm.viewmodel.LoginViewModel;
import com.agriiprince.utils.StringUtils;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.agriiprince.appcontroller.AppController.loggedOut;

public class RegisterFragment extends BaseFragment {

    private static final String TAG = "RegisterFragment";

    public RegisterFragment() {
        // Required empty public constructor
    }

    public static RegisterFragment newInstance() {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    StringUtils mStringUtils;

    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    PrefManager mPrefManager;

    private LoginViewModel mLoginViewModel;

    private FragmentRegisterBinding mBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_register;
    }

    @Override
    protected void performDataBinding(View view) {
        mBinding = DataBindingUtil.bind(view);
    }

    @Override
    protected void initControl() {
        mLoginViewModel = ViewModelProviders.of(getActivity(), mViewModelFactory).get(LoginViewModel.class);
    }

    @Override
    protected void initViewControl() {
        mBinding.setLoginViewModel(mLoginViewModel);
        mBinding.executePendingBindings();

        if (mBinding.password.getText() != null) mBinding.password.getText().clear();

        mBinding.passwordLayout.findViewById(R.id.text_input_password_toggle).performClick();
        mBinding.confirmPasswordLayout.findViewById(R.id.text_input_password_toggle).performClick();

    }

    @Override
    protected void initTextViews() {
        mBinding.register.setText(mStringUtils.getLocalizedString(R.string.register));
        mBinding.haveAccount.setText(mStringUtils.getLocalizedString(R.string.already_have_an_account));
        mBinding.loginBtn.setText(mStringUtils.getLocalizedString(R.string.login));
        mBinding.registerBtn.setText(mStringUtils.getLocalizedString(R.string.register));
    }

    @Override
    protected void initListener() {
        mBinding.loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginViewModel.onClickLoginFragment();
            }
        });

        mBinding.registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });
    }

    private void registerUser() {
        loggedOut = 0;

        String mobile = "";
        String password = "";
        String confirmPassword = "";

        if (mBinding.mobile.getText() != null) mobile = mBinding.mobile.getText().toString();
        if (mBinding.password.getText() != null) password = mBinding.password.getText().toString();
        if (mBinding.confirmPassword.getText() != null) confirmPassword = mBinding.confirmPassword.getText().toString();

        Log.d("USER_REGISTER :"," --> User mob & pass : " + mobile + " " + password);
        mPrefManager = new PrefManager(getContext());
        mPrefManager.setMobileNo(mobile);
        mPrefManager.setUserPassword(password);

        if (BuildConfig.USER_TYPE.equals(User.Type.FARMER)) {
            mPrefManager.setUserType("farmer");
        }
        else {
            mPrefManager.setUserType("oe");
        }

        if (validateCredentials(mobile, password, confirmPassword)){
            checkUserExists();
        }
    }


    API_Manager apiManager=new API_Manager();
    UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);

    private void checkUserExists() {

        Call<UserExist> call = retrofit_interface.userExist(mPrefManager.getMobileNo(),mPrefManager.getUserType());
        call.enqueue(new Callback<UserExist>() {
            @Override
            public void onResponse(Call<UserExist> call, Response<UserExist> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code userExist ");
                    Logs.d("CheckStatus", response.body().getStatus());
                    Logs.d("CheckStatus", response.body().getMessage());
                    Logs.d("CheckStatus", response.body().getCode().toString());

                    if(response.body().getCode() == 200) {
                        if (response.body().getMessage().equals("User Already Exists")) {
                            showDefaultSnackBar(getString(R.string.user_already_exists));
                        } else {
                            mLoginViewModel.verifyMobile();
                        }
                    }
                    //"User Already Exists"

                } catch (Exception e) {
                    e.getMessage();
                    showDefaultSnackBar(getString(R.string.user_already_exists));
                }
            }
            @Override
            public void onFailure(Call<UserExist> call, Throwable t) {
                Log.d("CheckStatus", "user exist "+ t.getMessage());
                //Toast.makeText(getContext(), getBilingualString(getErrorMessageId()), Toast.LENGTH_SHORT).show();
            }
        });

       /* mLoginViewModel.checkUserExists()
                .observe(this, new LiveDataObserver<UserExist>() {
                    @Override
                    protected void onLoading() {
                        showProgressDialog();
                    }

                    @Override
                    protected void onSuccess(UserExist data) {
                        Logs.d(TAG, "onSuccess: " + data.errorCode);
                        hideProgressDialog();
                        if (data.errorCode == 100) {
                            mLoginViewModel.verifyMobile();
                        } else {
                            showDefaultSnackBar(getString(R.string.user_already_exists));
                        }
                    }

                    @Override
                    protected void onFail(Throwable t) {
                        hideProgressDialog();
                        Toast.makeText(getContext(), getBilingualString(getErrorMessageId()), Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    protected void onNetworkFail() {
                        hideProgressDialog();
                    }
                });*/
    }

    private boolean validateCredentials(String mobileNumber, String password, String confirmPassword) {
        if (TextUtils.isEmpty(mobileNumber) || mobileNumber.length() < 10) {
            mBinding.mobile.setError(getString(R.string.enter_valid_number));
            mBinding.mobile.requestFocus();
            return false;

        } else if (TextUtils.isEmpty(password)) {
            mBinding.password.setError(getString(R.string.enter_password));
            mBinding.password.requestFocus();
            return false;

        } else if (password.contains(" ")) {
            mBinding.password.setError(getString(R.string.password_no_space));
            mBinding.password.requestFocus();
            return false;

        } else if (TextUtils.isEmpty(confirmPassword)) {
            mBinding.confirmPassword.setError(getString(R.string.confirm_password));
            mBinding.confirmPassword.requestFocus();
            return false;

        } else if (!password.equals(confirmPassword)) {
            mBinding.confirmPassword.setError(getString(R.string.password_do_not_match));
            mBinding.confirmPassword.requestFocus();
            return false;
        }

        return true;
    }
}
