package com.agriiprince.mvvm.ui.subscription;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.model.FarmerProfileModel;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.profile.UserUpdate;
import com.agriiprince.mvvm.retrofit.dto.razorpay.CheckoutResponse;
import com.agriiprince.mvvm.retrofit.dto.razorpay.ManualSubscriptionResponse;
import com.agriiprince.mvvm.retrofit.dto.razorpay.RazorPayResponse;
import com.agriiprince.mvvm.retrofit.model.login.Profile;
import com.agriiprince.mvvm.retrofit.service.RazorPay;
import com.agriiprince.mvvm.retrofit.service.SubscriptionNode;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.mvvm.ui.registration.FarmerProfileActivity;
import com.agriiprince.mvvm.ui.splash.SplashActivity;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Calendar;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;

import static com.agriiprince.utils.Utils.showToast;

public class SubscriptionPaymentActivity extends AppCompatActivity implements PaymentResultWithDataListener {

    private final String TAG = SubscriptionPaymentActivity.class.getSimpleName();

    private UserProfile mUserProfile;

    private PrefManager mPrefManager;

    private TextView tvPayBody, tvAmtLabel, tvAmount, tvBplAmount, tvValidityLabel, tvValidity;
    private TextView tvAfterDiscountLabel, tvAfterDiscountAmount;

    private TextView tvPamentModeLabel;

    private RadioGroup rgPayModeGroup;

    private AppCompatRadioButton rbOnline, rbOffline;

    private TextView tvOfflineInfo;

    private TextView tvCouponLabel, tvCouponStatus, tvOeNumberLabel;

    private AppCompatButton btnCouponApply, btnOtpApply, btnPayNow;

    private AppCompatEditText etCoupon, etOeNumber, etOtpManual;

    private LinearLayout llBplLayout, llAfterDiscount, llCouponLayout, llOeOtpLayout;

    private ProgressDialog mProgressDialog;

    private String subscriptionId;
    private String subscription_id;
    private int subscriptionDays;
    private int subscriptionAmount;
    private int originalSubscriptionAmount;

    private int collectionCharge;

    private boolean requestedCollection;

    private boolean bplApplicable;
    private int bplAmount;

    private boolean couponApplied = false;

    private int discount = 0;
    private int discountPercent;
    private int discountAmount;

    private boolean isPayingThroughRazorPay = true;
    private boolean isPayingOffline = false;

    private String userId, userType, phoneNo, email;

    private String couponCode, oePhone;
    private StringUtils utils;

    //-------------------------------- RAZOR PAY VARS ----------------------------------------------

    private String razorPayOrderId;

    //----------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_payment);
        utils = AppController.getInstance().getStringUtils();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(utils.getLocalizedString(R.string.subscription_payment));


        //--------------------------------- RAZOR PAY ----------------------------------------------

        /*
         To ensure faster loading of the Checkout form,
          call this method as early as possible in your checkout flow.
         */
        Checkout.preload(getApplicationContext());

        // Payment button created by you in XML layout
        btnPayNow = findViewById(R.id.subscription_pay_now);

        btnPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("CheckStatus", "manual_fee_collection Response 1" + isPayingThroughRazorPay);
                if(isPayingThroughRazorPay)
                {
                    fetchOrderId(originalSubscriptionAmount-discountAmount);
                    //startPayment();
                }
                else //if (isPayingOffline)
                {
                    Log.e("CheckStatus", "manual_fee_collection Response 1");
                    requestServerForManualCollection();
                }
            }
        });

        //------------------------------------------------------------------------------------------


        mUserProfile = new UserProfile(this);
        mPrefManager = new PrefManager(this);

        tvPayBody = findViewById(R.id.subscription_pay_body);
        tvAmtLabel = findViewById(R.id.subscription_pay_cost_label);
        tvAmount = findViewById(R.id.subscription_pay_cost_amount);
        tvBplAmount = findViewById(R.id.subscription_pay_bpl_label);
        tvBplAmount = findViewById(R.id.subscription_pay_bpl_amount);
        tvValidityLabel = findViewById(R.id.subscription_pay_validity_label);
        tvValidity = findViewById(R.id.subscription_pay_validity_days);
        tvAfterDiscountLabel = findViewById(R.id.subscription_pay_after_discount_label);
        tvAfterDiscountAmount = findViewById(R.id.subscription_pay_after_discount);

        tvPamentModeLabel = findViewById(R.id.subscription_pay_mode_label);

        rgPayModeGroup = findViewById(R.id.subscription_pay_mode_group);

        rbOnline = findViewById(R.id.subscription_pay_mode_payu);
        rbOffline = findViewById(R.id.subscription_pay_mode_offline);

        tvOfflineInfo = findViewById(R.id.subscription_pay_offline_info);
        tvOfflineInfo.setText(utils.getLocalizedString(R.string.offline_payment_hint));

        tvCouponLabel = findViewById(R.id.subscription_pay_coupon_label);
        tvCouponLabel.setText(utils.getLocalizedString(R.string.have_a_discount_coupon));
        tvOeNumberLabel = findViewById(R.id.subscription_pay_oe_label);
        tvOeNumberLabel.setText(utils.getLocalizedString(R.string.operative_executive_mobile_number));
        tvCouponStatus = findViewById(R.id.subscription_pay_coupon_status);

        btnCouponApply = findViewById(R.id.subscription_pay_coupon_apply);
        btnCouponApply.setText(utils.getLocalizedString(R.string.apply));
        btnOtpApply = findViewById(R.id.subscription_pay_otp_apply);

        etCoupon = findViewById(R.id.subscription_pay_coupon_code);
        etCoupon.setHint(utils.getLocalizedString(R.string.coupon_code));
        etOeNumber = findViewById(R.id.subscription_pay_oe_mobile);
        etOeNumber.setHint(utils.getLocalizedString(R.string.mobile_number));
        etOtpManual = findViewById(R.id.subscription_pay_manual_otp);

        llCouponLayout = findViewById(R.id.subscription_pay_coupon_layout);
        llOeOtpLayout = findViewById(R.id.subscription_pay_oe_otp_layout);
        llBplLayout = findViewById(R.id.subscription_pay_bpl_amount_layout);
        llAfterDiscount = findViewById(R.id.subscription_pay_after_discount_layout);

        rbOnline.setChecked(true);
        updatePaymentMode();

        rgPayModeGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.subscription_pay_mode_payu:
                        isPayingThroughRazorPay = true;
                        isPayingOffline = false;
                        break;
                    case R.id.subscription_pay_mode_offline:
                        isPayingThroughRazorPay = false;
                        isPayingOffline = true;
                        break;
                }

                updatePaymentMode();
            }
        });

        btnCouponApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPayingThroughRazorPay && !bplApplicable) {
                    verifyCoupon();

                } else {
                    Utils.showToast(SubscriptionPaymentActivity.this, utils.getLocalizedString(R.string.bpl_coupon_applied).toString());
                }
            }
        });

        btnOtpApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activateByOtp();
            }
        });

        btnPayNow.setEnabled(true);
        rgPayModeGroup.setEnabled(true);

        getSubscriptionDetails();

    }


    //================================ OUR METHODS FOR RAZOR PAY ===================================

    API_Manager api_manager=new API_Manager();
    RazorPay retrofit_interface = api_manager.RazorpayClient().create(RazorPay.class);

    public void fetchOrderId(final int amount) {
        Logs.d("RAZORPAY_FINAL", "get_order_id call");

        PrefManager mPrefManager = new PrefManager(this);
        //Call<RazorPayResponse> call=retrofit_interface.get_order_id(mPrefManager.getAccessToken(),amount+"","INR");
        Call<RazorPayResponse> call=retrofit_interface.get_order_id(mPrefManager.getAccessToken(),amount+"","INR",mPrefManager.getUserId());
        call.enqueue(new Callback<RazorPayResponse>() {
            @Override
            public void onResponse(Call<RazorPayResponse> call, retrofit2.Response<RazorPayResponse> response) {
                Logs.d("RAZORPAY_FINAL", "get_order_id Response "+response);
                Logs.d("RAZORPAY_FINAL", "checkout_capture_payment Response "+response.body().getStatus());
                Logs.d("RAZORPAY_FINAL", "checkout_capture_payment Response "+response.body().getMessage());
                if (response.body() != null) {
                    Logs.d("RAZORPAY_FINAL",response.body().getCode().toString());
                    Logs.d("RAZORPAY_FINAL",response.body().getMessage());
                    Logs.d("RAZORPAY_FINAL",response.body().getData().getRazorpayId());
                    razorPayOrderId = response.body().getData().getRazorpayId();
                    subscription_id = response.body().getData().getSubscription_id();
                    startPaymentWIthOurId(amount);
                }
            }
            @Override
            public void onFailure(Call<RazorPayResponse> call, Throwable t) {
                Logs.d("RAZORPAY_FINAL","get_order_id error "+t.getMessage());
            }
        });
    }

    public void startPaymentWIthOurId(int amount) {
        /**
         * Instantiate Checkout
         */
        Checkout checkout = new Checkout();

        /**
         * Set your logo here
         */
        checkout.setImage(R.drawable.logo);

        /**
         * Reference to current activity
         */
        final Activity activity = this;

        /**
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         */
        try {
            JSONObject options = new JSONObject();

            /**
             * Merchant Name
             * eg: ACME Corp || HasGeek etc.
             */
            options.put("name", "Agrios Price Consulting");

            /**
             * Description can be anything
             * eg: Order #123123 - This order number is passed by you for your internal reference. This is not the `razorpay_order_id`.
             *     Invoice Payment
             *     etc.
             */
            options.put("description", "Order #123456");
            //options.put("order_id", "order_9A33XWu170gUtm");
            options.put("order_id", razorPayOrderId);
            options.put("currency", "INR");

            JSONObject preFill = new JSONObject();

            String possibleEmail = "test@razorpay.com";
            Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
            Account[] accounts = AccountManager.get(this).getAccounts();
            for (Account account : accounts) {
                if (emailPattern.matcher(account.name).matches()) {
                    possibleEmail = account.name;
                }
            }

            preFill.put("email", possibleEmail);
            preFill.put("contact", mUserProfile.getUserMobile());


            options.put("prefill", preFill);

            /**
             * Amount is always passed in currency subunits
             * Eg: "500" = INR 5.00
             */
            options.put("amount", (amount*100)+"");

            checkout.open(activity, options);
        } catch(Exception e) {
            Log.e(TAG, "Error in starting Razorpay Checkout", e);
        }
    }

    public void checkout_capture_payment(String orderID, String paymentID, String signature) {
        Logs.d("RAZORPAY_FINAL", "checkout_capture_payment call. IDS :" + orderID + " " + paymentID + " " + signature);
        //Call<CheckoutResponse> call=retrofit_interface.checkout_capture_payment(mUserProfile.getApiToken(), paymentID, orderID, signature);
        Call<CheckoutResponse> call=retrofit_interface.checkout_capture_payment(mUserProfile.getApiToken(), paymentID, orderID, signature,subscription_id,mPrefManager.getUserId());
        call.enqueue(new Callback<CheckoutResponse>() {
            @Override
            public void onResponse(Call<CheckoutResponse> call, retrofit2.Response<CheckoutResponse> response) {
                Logs.d("RAZORPAY_FINAL", "checkout_capture_payment Response "+response);
                Logs.d("RAZORPAY_FINAL", "checkout_capture_payment Response "+response.body().getStatus());
                Logs.d("RAZORPAY_FINAL", "checkout_capture_payment Response "+response.body().getMessage());
                if (response.body() != null) {

                    if(response.body().getStatus().toString().equals("200"))
                    {
                        Log.e("RAZORPAY_FINAL",response.body().getCode().toString());
                        Log.e("RAZORPAY_FINAL",response.body().getMessage());

                        subscriptionDays = 365;
                        subscriptionActivated();
                    }

                }

            }
            @Override
            public void onFailure(Call<CheckoutResponse> call, Throwable t) {
                Logs.d("RAZORPAY_FINAL","checkout_capture_payment error "+t.getMessage());
            }
        });
    }


    //============================== STANDARD METHODS FOR RAZOR PAY ================================

    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "Razorpay Corp");
            options.put("description", "Demoing Charges");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("order_id", razorPayOrderId);
            options.put("currency", "INR");
            options.put("amount", "100");

            JSONObject preFill = new JSONObject();
            preFill.put("email", "test@razorpay.com");
            preFill.put("contact", "9876543210");

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    /**
     * The name of the function has to be
     * onPaymentSuccess
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(String razorpayPaymentID, PaymentData data) {
        String paymentId = data.getPaymentId();
        String signature = data.getSignature();
        String orderId = data.getOrderId();
        try {
            Toast.makeText(this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
            checkout_capture_payment(orderId, paymentId, signature);
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    /**
     * The name of the function has to be
     * onPaymentError
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response, PaymentData data) {
        try {
            Toast.makeText(this, "Payment failed: " + code + " " + response, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }

    //==============================================================================================


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPause() {
        super.onPause();
        dismissProgressDialog();

    }

    private void setupSubscription() {
        String validityDays = subscriptionDays + " " + utils.getLocalizedString(R.string.days, true);
        tvPayBody.setText(utils.getLocalizedString(R.string.subscription_payment_body));
        tvValidity.setText(validityDays);

        tvAmount.setText(formatAmount(originalSubscriptionAmount));

        tvPamentModeLabel.setText(utils.getLocalizedString(R.string.payment_mode));

        String offlinePayment = utils.getLocalizedString(R.string.offline_payment, true) +
                " ( " + getString(R.string.rs_symbol) + " " + formatAmount(collectionCharge) + " " +
                utils.getLocalizedString(R.string.extra, true) + " )";
        rbOffline.setText(offlinePayment);
        rbOnline.setText(utils.getLocalizedString(R.string.net_banking_credit_debit_card));

        if (bplApplicable) {
            tvBplAmount.setText(formatAmount(bplAmount));
            llBplLayout.setVisibility(View.VISIBLE);
        } else {
            llBplLayout.setVisibility(View.GONE);

        }

        updatePaymentMode();
    }


    private void updatePaymentMode() {
        if (isPayingThroughRazorPay) {
            String payNow = utils.getLocalizedString(R.string.pay_now, true) +
                    " ( " + getString(R.string.rs_symbol) + formatAmount(subscriptionAmount) + " )";

            btnPayNow.setText(payNow);

            llCouponLayout.setVisibility(View.VISIBLE);
            llOeOtpLayout.setVisibility(View.GONE);

            tvAfterDiscountAmount.setText(String.valueOf(subscriptionAmount));
            llAfterDiscount.setVisibility(couponApplied ? View.VISIBLE : View.GONE);
            llAfterDiscount.setVisibility(View.VISIBLE);

        } else if (isPayingOffline) {
            removeCouponDiscount();

            String payNow = utils.getLocalizedString(R.string.request_for_collection, true) + " ( " +
                    getString(R.string.rs_symbol) +
                    formatAmount((originalSubscriptionAmount-discountAmount) + collectionCharge) + " )";

            btnPayNow.setText(payNow);

            llCouponLayout.setVisibility(View.GONE);
            llOeOtpLayout.setVisibility(View.VISIBLE);

            llAfterDiscount.setVisibility(View.VISIBLE);
        }

    }


    private void subscriptionActivated() {
        Toast.makeText(this, utils.getLocalizedString(R.string.subscription_activated), Toast.LENGTH_SHORT).show();

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, subscriptionDays);
        mUserProfile.setValidTill(calendar.getTimeInMillis());
        mPrefManager.setValidityTime(calendar.getTimeInMillis());
        //updateProfile();

        Intent intent = new Intent(SubscriptionPaymentActivity.this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    /**
     * @param discountAmount
     */
    private void applyCouponAmountDiscount(int discountAmount) {
        oePhone = etOeNumber.getText().toString();
        couponCode = etCoupon.getText().toString();

        couponApplied = true;

        subscriptionAmount = originalSubscriptionAmount - discountAmount;

        showValidCoupon();

        updatePaymentMode(); // call this method to update amount displayed on pay button
    }

    /**
     * @param percent
     */
    private void applyCouponPercentDiscount(double percent) {
        oePhone = etOeNumber.getText().toString();
        couponCode = etCoupon.getText().toString();

        couponApplied = true;

        subscriptionAmount = (int) (originalSubscriptionAmount - originalSubscriptionAmount * percent / 100);

        showValidCoupon();

        updatePaymentMode(); // call this method to update amount displayed on pay button
    }

    private void removeCouponDiscount() {
        oePhone = "";
        couponCode = "";

        couponApplied = false;
        llCouponLayout.setVisibility(View.GONE);

        subscriptionAmount = originalSubscriptionAmount;
        tvCouponStatus.setText("");
        tvCouponStatus.setVisibility(View.GONE);
        etCoupon.setText("");
        etOeNumber.setText("");
    }


    private void requestServerForManualCollection() {

        Log.e("CheckStatus", "manual_fee_collection Response "+ mUserProfile.getApiToken() + mUserProfile.getUserId() + mUserProfile.getUserMobile()+etOeNumber.getText().toString()+tvAfterDiscountAmount.getText().toString());

        API_Manager api_manager=new API_Manager();
        SubscriptionNode retrofit_interface = api_manager.SubscriptionClient().create(SubscriptionNode.class);
        Call<ManualSubscriptionResponse> call=retrofit_interface.manual_fee_collection(mUserProfile.getApiToken(), mUserProfile.getUserId() +"", mUserProfile.getUserMobile(),etOeNumber.getText().toString(),tvAfterDiscountAmount.getText().toString());
        call.enqueue(new Callback<ManualSubscriptionResponse>() {
            @Override
            public void onResponse(Call<ManualSubscriptionResponse> call, retrofit2.Response<ManualSubscriptionResponse> response) {
                Log.e("CheckStatus", "manual_fee_collection Response "+response);
                try {
                    if(response.body() != null && response.body().getStatus()== 200)
                    Toast.makeText(SubscriptionPaymentActivity.this,response.body().getMessage(), Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    e.printStackTrace();

                    btnPayNow.setEnabled(true);
                    rgPayModeGroup.setEnabled(true);
                }
            }
            @Override
            public void onFailure(Call<ManualSubscriptionResponse> call, Throwable t) {
                Log.e("CheckStatus"," error "+t.getMessage());
                t.printStackTrace();
                btnPayNow.setEnabled(true);
                rgPayModeGroup.setEnabled(true);
            }
        });
    }


    private void activateByOtp() {
        showProgressDialog();

        btnOtpApply.setEnabled(false);

        StringRequest request = new StringRequest(Request.Method.POST, Config.POST_MANUAL_TRANSACTION_VERIFY_OTP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY", response);
                        try {
                            JSONObject responseObject=new JSONObject(response);
                            int error = responseObject.getInt("code");
                            if (error == 200) {
                                subscriptionActivated();

                            } else {
                                Toast.makeText(SubscriptionPaymentActivity.this,
                                        responseObject.getString("message"), Toast.LENGTH_SHORT).show();

                                btnOtpApply.setEnabled(true);

                            }
                            dismissProgressDialog();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dismissProgressDialog();
                            btnOtpApply.setEnabled(true);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        dismissProgressDialog();
                        btnOtpApply.setEnabled(true);

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("user_id", mUserProfile.getUserId());
                params.put("user_type", mUserProfile.getUserType());
                params.put("otp", etOtpManual.getText().toString());

                return params;
            }
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUserProfile.getApiToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(request);
    }


    /**
     * check for valid phone number
     * <p>
     * make api call to server to verify the coupon code whether it is valid or not
     * If response is success. get discount from the response and apply
     */
    private void verifyCoupon() {
        subscriptionAmount = originalSubscriptionAmount;

        couponApplied = false;

        tvCouponStatus.setVisibility(View.GONE);

        updatePaymentMode();

        btnCouponApply.setEnabled(false);

        final String userId = mUserProfile.getUserId();

        showProgressDialog();

        StringRequest verifyCouponRequest = new StringRequest(Request.Method.POST,
                Config.POST_SUBSCRIPTION_VERIFY_COUPON,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Utils.showLog(response);
                        dismissProgressDialog();
                        Log.d("TESTING_VOLLEY", response);
                        btnCouponApply.setEnabled(true);
                        try {
                            //JSONArray responseArray = new JSONArray(response);
                            //JSONObject responseObject = responseArray.getJSONObject(0);
                            //if (responseObject.getInt("error_code") == 100)
                            JSONObject responseObject=new JSONObject(response);
                            int error = responseObject.getInt("code");
                            JSONObject data = responseObject.getJSONObject("data");
                            if (error == 200)
                            {
                                boolean validCoupon = data.getBoolean("valid_coupon");
                                Log.e("SUBS_COUPON ---> ", "1"+validCoupon);

                                if (validCoupon) {

                                    boolean trialActive = data.getBoolean("trial_activated");
                                    Log.e("SUBS_COUPON ---> ", "2"+trialActive);

                                    if (trialActive) {
                                        Log.e("SUBS_COUPON ---> ", "3"+trialActive);
                                        subscriptionActivated();
                                    } else {

                                        JSONArray dataArray = data.getJSONArray("data");
                                        JSONObject mainObject = dataArray.getJSONObject(0);

                                        String amount = mainObject.getString("discount_in_rupees");
                                        Log.e("SUBS_COUPON ---> ", "4"+amount);
                                        if (!TextUtils.isEmpty(amount)) {
                                            discountAmount = Integer.parseInt(amount);
                                            Log.e("SUBS_COUPON ---> ", "5"+discountAmount);
                                            applyCouponAmountDiscount(discountAmount);

                                            Log.d(TAG, "dis amt: " + amount);
                                        } else {
                                            String percent = mainObject.getString("discount_in_percent");
                                            Log.e("SUBS_COUPON ---> ", "6"+percent);

                                            if (!TextUtils.isEmpty(percent)) {
                                                discountPercent = Integer.parseInt(percent);
                                                Log.e("SUBS_COUPON ---> ", "7"+discountPercent);
                                                applyCouponPercentDiscount(discountPercent);

                                            } else {
                                                showInvalidCoupon();
                                            }
                                        }
                                    }

                                } else {
                                    showInvalidCoupon();
                                    showInvalidCouponDialog();
                                }

                            } else if (responseObject.getInt("code") == 101) {
                                showInvalidCoupon();
                                showInvalidCouponDialog();
                            }

                        } catch (JSONException | NumberFormatException e) {
                            e.printStackTrace();
                            Log.d("TESTING_VOLLEY","exception "+e.getMessage());
                            Utils.showToast(SubscriptionPaymentActivity.this, utils.getLocalizedString(R.string.something_went_wrong_try_again).toString());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        dismissProgressDialog();
                        btnCouponApply.setEnabled(true);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("api_token", mUserProfile.getApiToken());

                params.put("user_id", userId);
                params.put("user_type", mUserProfile.getUserType());
                params.put("mode", "check");
                params.put("password", mUserProfile.getUserPassword());
                if (etOeNumber.getText() != null)
                    params.put("oe_mobile", etOeNumber.getText().toString());
                if (etCoupon.getText() != null)
                    params.put("coupon_code", etCoupon.getText().toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUserProfile.getApiToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(verifyCouponRequest);
    }

    private void showInvalidCouponDialog() {
        Log.d(TAG, "showInvalidCouponDialog");
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(utils.getLocalizedString(R.string.coupon_code))
                .setMessage(utils.getLocalizedString(R.string.invalid_coupon_dialog_message))
                .setPositiveButton(utils.getLocalizedString(R.string.btn_ok), null)
                .show();
    }

    private void showInvalidCoupon() {
        tvCouponStatus.setText(utils.getLocalizedString(R.string.invalid_coupon));
        tvCouponStatus.setVisibility(View.VISIBLE);
        tvCouponStatus.setTextColor(getResources().getColor(R.color.rangeRed));
    }

    private void showValidCoupon() {
        tvCouponStatus.setText(utils.getLocalizedString(R.string.coupon_applied));
        tvCouponStatus.setVisibility(View.VISIBLE);
        tvCouponStatus.setTextColor(getResources().getColor(R.color.primary_green));
    }


    private void getSubscriptionDetails() {
        showProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.POST_SUBSCRIPTION_DETAILS,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //GeneralUtils.showLog(response);
                        try {
                            Log.d("TESTING_VOLLEY","POST_SUBSCRIPTION_DETAILS "+response);
                            //JSONArray responseArray = new JSONArray(response);
                            JSONObject jsonObject= new JSONObject(response);
                            //JSONObject responseObject = responseArray.getJSONObject(0);

                            //if (responseObject.getInt("error_code") == 100)
                            if (jsonObject.getInt("code") == 200)
                            {
                                Log.d("TESTING_VOLLEY","POST_SUBSCRIPTION_DETAILS "+jsonObject.getInt("code"));
                                //bplApplicable = responseObject.getString("bpl_status").equals("active");
                                JSONObject dataobj = jsonObject.getJSONObject("data");
                                bplApplicable = dataobj.getString("bpl_status").equals("active");

                                if (bplApplicable) {
                                   // bplAmount = responseObject.getInt("bpl_amount");
                                    bplAmount = jsonObject.getInt("bpl_amount");
                                    subscriptionAmount = bplAmount;

                                    rbOffline.setEnabled(false);
                                    tvOfflineInfo.setVisibility(View.INVISIBLE);
                                    llCouponLayout.setVisibility(View.GONE);

                                }
                                //collectionCharge = responseObject.getInt("manual_collection_charges");
                                collectionCharge = dataobj.getInt("manual_collection_charges");

                                //JSONObject data = responseObject.getJSONArray("data").getJSONObject(0);
                                JSONObject data = dataobj.getJSONArray("data").getJSONObject(0);

                                subscriptionId = data.getString("subscription_id");
                                subscriptionAmount = data.getInt("subscription_price");
                                subscriptionDays = data.getInt("subscription_validity_days");

                                originalSubscriptionAmount = subscriptionAmount;
                                Log.d("TESTING_VOLLEY","POST_SUBSCRIPTION_DETAILS subscriptionId "+subscriptionId);
                                setupSubscription();

                            }
                            dismissProgressDialog();

                        } catch (JSONException e) {
                            Utils.showToast(SubscriptionPaymentActivity.this, utils.getLocalizedString(R.string.something_went_wrong_try_again).toString());
                            Log.d("TESTING_VOLLEY","POST_SUBSCRIPTION_DETAILS exception "+e.getMessage());                            //SubscriptionPaymentActivity.this.finish();

                            e.printStackTrace();
                            dismissProgressDialog();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.d("TESTING_VOLLEY","POST_SUBSCRIPTION_DETAILS error"+error);
                        Utils.showToast(SubscriptionPaymentActivity.this, utils.getLocalizedString(R.string.something_went_wrong_try_again).toString());
                        //SubscriptionPaymentActivity.this.finish();

                        dismissProgressDialog();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());

                params.put("user_id", mUserProfile.getUserId());
                params.put("password", mUserProfile.getUserPassword());
                params.put("mode", "all");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUserProfile.getApiToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(SubscriptionPaymentActivity.this);
            mProgressDialog.setMessage(getString(R.string.please_wait));
        }

        if (!mProgressDialog.isShowing()) {
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
    }

    void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    private String formatAmount(int amount) {
        return formatAmount(String.valueOf(amount));
    }

    private String formatAmount(String amount) {
        return String.format(Locale.ENGLISH, "%.2f", Double.parseDouble(amount));
    }
}
