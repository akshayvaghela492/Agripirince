package com.agriiprince.mvvm.ui.base;

import java.util.List;

public interface RecyclerViewUseCase<T> {

    void addItem(T item);

    void addAllItem(List<T> items);

    void replaceAllItem(List<T> items);

    T getItem(int position);
}
