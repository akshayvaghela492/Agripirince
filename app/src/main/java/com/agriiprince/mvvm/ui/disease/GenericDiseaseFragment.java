package com.agriiprince.mvvm.ui.disease;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.databinding.FragmentGenericDiseaseBinding;
import com.agriiprince.utils.StringUtils;

import org.jetbrains.annotations.NotNull;

public class GenericDiseaseFragment extends Fragment {

    private static final String ARG_BODY = "body";

    private String mBody;

    private StringUtils stringUtils;

    private FragmentGenericDiseaseBinding binding;

    public GenericDiseaseFragment() {
        // Required empty public constructor
    }

    public static GenericDiseaseFragment newInstance(String param2) {
        GenericDiseaseFragment fragment = new GenericDiseaseFragment();
        Bundle args = new Bundle();
        args.putString(ARG_BODY, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mBody = getArguments().getString(ARG_BODY);
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_generic_disease, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = DataBindingUtil.bind(view);
        stringUtils = AppController.getInstance().getStringUtils();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!TextUtils.isEmpty(mBody)) binding.controlBody.setText(mBody);
    }
}
