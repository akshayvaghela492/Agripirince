package com.agriiprince.mvvm.ui.base;

import android.support.annotation.NonNull;
import android.view.View;

public interface BaseFragmentInteraction {

    /**
     * displays the progress dialog
     */
    void showProgressDialog();

    /**
     * hides the progress dialog
     */
    void hideProgressDialog();

    /**
     * displays default snack bar
     * @param message message to be displayed by snack bar
     */
    void showDefaultSnackBar(@NonNull String message);


    /**
     * hides soft input keyboard
     */
    void hideKeyboard();

    /**
     * hides soft input keyboard
     *
     * @param view view which has the focus
     */
    void hideKeyboard(@NonNull View view);
}
