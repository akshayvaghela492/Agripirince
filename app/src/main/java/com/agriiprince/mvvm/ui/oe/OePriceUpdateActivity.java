package com.agriiprince.mvvm.ui.oe;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.ActivityUpdateCommodityPriceBinding;
import com.agriiprince.dataservice.CropDataService;
import com.agriiprince.dataservice.MandiDataService;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.oe.UpdatePrice;
import com.agriiprince.mvvm.retrofit.service.OE;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.mvvm.ui.customwidgets.DatePickerDialog;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.MandiModel;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.mvvm.model.crop.Grade;
import com.agriiprince.mvvm.model.crop.Variety;
import com.agriiprince.utils.DateUtil;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;

public class OePriceUpdateActivity extends AppCompatActivity {

    private static final String TAG = "OePriceUpdateActivity";

    private List<MandiModel> mInterestedMandi;
    private List<Crop> mInterestedCrop;

    private MandiModel mSelectedMandi;
    private Crop mSelectedCrop;

    private String mSelectedMandiName;
    private String mSelectedCropName;
    private String mSelectedVariety;
    private String mSelectedGrade;

    private String mGenPrice;
    private String mMinPrice;
    private String mModalPrice;
    private String mMaxPrice;

    private Calendar calendar;

    private int mSelectedCropIndex;
    private int mSelectedVarietyIndex;

    private StringUtils mStringUtils;

    private PrefManager mPrefs;

    private UserProfile mUserProfile;

    private ActivityUpdateCommodityPriceBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_update_commodity_price);

        mPrefs = new PrefManager(this);
        mStringUtils = AppController.getInstance().getStringUtils();
        mUserProfile = new UserProfile(this);

        calendar = Calendar.getInstance();

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(mStringUtils.getLocalizedString(R.string.update_commodity_prices));


        binding.btnUpdateComPrice.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mGenPrice = binding.generalPrice.getText().toString();
                mMinPrice = binding.minPrice.getText().toString();
                mModalPrice = binding.modalPrice.getText().toString();
                mMaxPrice = binding.maxPrice.getText().toString();

                if (isValid()) {
                    fnUpdateComPrice();
                }
            }
        });

        binding.date.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalender();
            }
        });

        setDateTextView();
        setTexts();

        getMandis();
        getCrops();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTexts() {
        binding.mandiLabel.setText(mStringUtils.getLocalizedString(R.string.select_mandi));
        binding.cropLabel.setText(mStringUtils.getLocalizedString(R.string.select_crop));
        binding.varietyLabel.setText(mStringUtils.getLocalizedString(R.string.select_variety));
        binding.gradeLabel.setText(mStringUtils.getLocalizedString(R.string.select_grade));
        binding.dateLabel.setText(mStringUtils.getLocalizedString(R.string.select_date));

        binding.priceLabel.setText(mStringUtils.getLocalizedString(R.string.enter_crop_prices));
        binding.generalLabel.setText(mStringUtils.getLocalizedString(R.string.general_price));
        binding.minLabel.setText(mStringUtils.getLocalizedString(R.string.min_price));
        binding.modalLabel.setText(mStringUtils.getLocalizedString(R.string.modal_price));
        binding.maxLabel.setText(mStringUtils.getLocalizedString(R.string.max_price));

        binding.generalPrice.setHint(mStringUtils.getLocalizedString(R.string.price_per_kg));
        binding.minPrice.setHint(mStringUtils.getLocalizedString(R.string.price_per_kg));
        binding.modalPrice.setHint(mStringUtils.getLocalizedString(R.string.price_per_kg));
        binding.maxPrice.setHint(mStringUtils.getLocalizedString(R.string.price_per_kg));

        binding.btnUpdateComPrice.setText(mStringUtils.getLocalizedString(R.string.update));
    }

    private boolean isValid() {
        if (mSelectedMandi == null) {
            Toast.makeText(this, mStringUtils.getLocalizedString(R.string.select_mandi), Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(mSelectedCropName)) {
            Toast.makeText(this, mStringUtils.getLocalizedString(R.string.please_select_the_crop_name), Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(mSelectedVariety)) {
            Toast.makeText(this, mStringUtils.getLocalizedString(R.string.select_variety), Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(mSelectedGrade)) {
            Toast.makeText(this, mStringUtils.getLocalizedString(R.string.select_grade), Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(mGenPrice) && TextUtils.isEmpty(mMinPrice) && TextUtils.isEmpty(mModalPrice) && TextUtils.isEmpty(mMaxPrice)) {
            Snackbar.make(findViewById(android.R.id.content),
                    mStringUtils.getLocalizedString(R.string.enter_crop_prices).toString(), Snackbar.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    private void getMandis() {
        MandiDataService dataService = new MandiDataService(this);
        Log.d(TAG, "getInterestedMandis: " + -1);
        dataService.getMandis(new MandiDataService.OnMandiResponse() {
            @Override
            public void onMandiSuccess(List<MandiModel> mandies) {
                mInterestedMandi = mandies;
                setMandiAdapter(mInterestedMandi);
                Log.d(TAG, "getInterestedMandis: " + -2);
                //getInterestedMandis(mandies);
                Log.d(TAG, "getInterestedMandis: " + -3);
            }

            @Override
            public void onMandiFailure() {

            }
        });
    }

    private void getCrops() {
        CropDataService cropDataService = new CropDataService(this);
        cropDataService.getCrops(new CropDataService.OnCropDataRequest() {

            @Override
            public void onCropDataResults(List<Crop> crops) {
                mInterestedCrop = crops;
                setCropAdapter(mInterestedCrop);
                //getInterestedCrops(crops);
            }

            @Override
            public void onCropDataError() {

            }
        });
    }

    private void getInterestedMandis(List<MandiModel> mandiModels) {
        String temp = mPrefs.getInterestedMandiIds();
        Log.d(TAG, "getInterestedMandis: " + temp);

        if (!TextUtils.isEmpty(temp)) {
            Log.d(TAG, "getInterestedMandis: " + 1);
            String[] mandiIds = temp.replace(" ", "").replace("[", "")
                    .replace("]", "").split(",");
            Log.d(TAG, "getInterestedMandis: " + 2);
            mInterestedMandi = MandiModel.getMandisByIds(mandiModels, Arrays.asList(mandiIds));
            Log.d(TAG, "getInterestedMandis: " + 3);
            setMandiAdapter(mInterestedMandi);
            Log.d(TAG, "getInterestedMandis: " + 4);
        }
    }

    private void getInterestedCrops(List<Crop> crops) {
        String temp = mPrefs.getInterestedCropIds();
        Log.d(TAG, "getInterestedCrops: " + temp);

        if (!TextUtils.isEmpty(temp)) {
            String[] cropIds = temp.replace(" ", "").replace("[", "")
                    .replace("]", "").split(",");

            mInterestedCrop = Crop.getCropsByIds(crops, cropIds);
            setCropAdapter(mInterestedCrop);
        }

    }

    private void setMandiAdapter(List<MandiModel> mandiModels) {
        Log.d(TAG, "setMandiAdapter: " + mandiModels.get(0).mandi_name_en);
        List<String> mandiNames = MandiModel.getMandiCityNames(this, mandiModels);
        Collections.sort(mandiNames, String.CASE_INSENSITIVE_ORDER);
        mandiNames.remove(0);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, mandiNames);

        binding.spinnerMandi.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = parent.getAdapter().getItem(position).toString();
                mSelectedMandi = MandiModel.getByMandiCity(OePriceUpdateActivity.this, mInterestedMandi, selected);
                if (mSelectedMandi != null) mSelectedMandiName = mSelectedMandi.mandi_name_en;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        binding.spinnerMandi.setAdapter(adapter);
        binding.spinnerMandi.setSelection(105);

        Log.d(TAG, "setMandiAdapter: " + mandiNames);
    }

    private void setCropAdapter(List<Crop> crops) {
        Log.d(TAG, "setCropAdapter: " + crops.get(0).cropName);
        List<String> cropNames = Crop.getBilingualNames(mStringUtils, crops);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, cropNames);

        binding.spinnerCrop.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = parent.getAdapter().getItem(position).toString();
                for (int i = 0; i < mInterestedCrop.size(); i++) {
                    Crop crop = mInterestedCrop.get(i);
                    if (mStringUtils.getBilingualStringForApi(crop.cropName, crop.cropName_bi).equals(selected)) {
                        mSelectedCropIndex = i;
                        mSelectedCropName = crop.cropName;
                        break;
                    }
                }
                setVarietyAdapter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerCrop.setAdapter(adapter);
        binding.spinnerCrop.setSelection(10);

        Log.d(TAG, "setCropAdapter: " + cropNames);
    }

    private void setVarietyAdapter() {
        List<String> varietyNames = mInterestedCrop.get(mSelectedCropIndex).getBilingualVarietyNames(this);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, varietyNames);

        binding.spinnerVariety.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = parent.getAdapter().getItem(position).toString();
                List<Variety> varieties = mInterestedCrop.get(mSelectedCropIndex).getVarieties();
                for (int i = 0; i < varieties.size(); i++) {
                    Variety variety = varieties.get(i);
                    if (mStringUtils.getBilingualStringForApi(variety.varietyName, variety.varietyName_bi).equals(selected)) {
                        mSelectedVarietyIndex = i;
                        mSelectedVariety = variety.varietyName;
                        break;
                    }
                }
                setGradeAdapter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerVariety.setAdapter(adapter);
        binding.spinnerVariety.setSelection(0);

        Log.d(TAG, "setVarietyAdapter: " + varietyNames);
    }

    private void setGradeAdapter() {
        List<String> gradeNames = mInterestedCrop.get(mSelectedCropIndex)
                .getVariety(mSelectedVarietyIndex).getBilingualGradeNames(this);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, gradeNames);

        binding.spinnerGrade.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = parent.getAdapter().getItem(position).toString();
                List<Grade> grades = mInterestedCrop.get(mSelectedCropIndex).getVariety(mSelectedVarietyIndex).getGrades();
                for (int i = 0; i < grades.size(); i++) {
                    Grade grade = grades.get(i);
                    if (mStringUtils.getBilingualStringForApi(grade.gradeName, grade.gradeName_bi).equals(selected)) {
                        mSelectedVarietyIndex = i;
                        mSelectedGrade = grade.gradeName;
                        break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerGrade.setAdapter(adapter);
        binding.spinnerGrade.setSelection(0);

        Log.d(TAG, "setGradeAdapter: " + gradeNames);
    }

    private void showCalender() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new android.app.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendar.set(year, month, dayOfMonth);
                        setDateTextView();
                    }
                }, calendar);

        DatePicker datePicker = datePickerDialog.getDatePicker();
        datePicker.setMaxDate(Calendar.getInstance().getTimeInMillis());
        datePickerDialog.show();
    }

    private void setDateTextView() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String date = dateFormat.format(calendar.getTime());
        binding.date.setText(date);
    }

    private void fnUpdateComPrice() {
    /*    StringRequest request = new StringRequest(Method.POST, POST_INSERT_OE_PRICE_DATA,
                new Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: " + response);

                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            if (jsonObject.getInt("error_code") == 100) {
                                Toast.makeText(OePriceUpdateActivity.this,
                                        mStringUtils.getLocalizedString(R.string.updated), Toast.LENGTH_SHORT).show();

                                binding.generalPrice.getText().clear();
                                binding.minPrice.getText().clear();
                                binding.modalPrice.getText().clear();
                                binding.maxPrice.getText().clear();
                            } else {
                                Toast.makeText(OePriceUpdateActivity.this,
                                        mStringUtils.getLocalizedString(R.string.upload_failed_try_again), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        GeneralUtils.showVolleyError(OePriceUpdateActivity.this, error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("api_token", mUserProfile.getApiToken());
                params.put("user_id", mUserProfile.getUserId());

                params.put("arrival_date", DateUtil.getServerDate(calendar));

                params.put("commodity_state", mSelectedMandi.mandi_state_en);
                params.put("commodity_district", mSelectedMandi.mandi_district_en);
                params.put("commodity_market", mSelectedMandi.mandi_name_en);

                params.put("commodity_name", mSelectedCropName);
                params.put("commodity_variety", mSelectedVariety);
                params.put("commodity_grade", mSelectedGrade);

                if (!TextUtils.isEmpty(mGenPrice)) params.put("general_price", mGenPrice);
                if (!TextUtils.isEmpty(mMinPrice)) params.put("min_price", mMinPrice);
                if (!TextUtils.isEmpty(mModalPrice)) params.put("modal_price", mModalPrice);
                if (!TextUtils.isEmpty(mMaxPrice)) params.put("max_price", mMaxPrice);

                Log.d(TAG, "getParams: " + params.toString());
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(request);
        */
        API_Manager api_manager=new API_Manager();
        OE retrofit_interface = api_manager.getClient8().create(OE.class);
        Call<UpdatePrice> call = retrofit_interface.UpdatePrice(mUserProfile.getApiToken(), mUserProfile.getUserId(),mSelectedCropName,mSelectedVariety,mSelectedGrade,DateUtil.getServerDate(calendar),mSelectedMandi.mandi_state_en,mSelectedMandi.mandi_district_en,mSelectedMandi.mandi_name_en,mMinPrice,mMaxPrice,mModalPrice,mGenPrice);
        call.enqueue(new Callback<UpdatePrice>() {
            @Override
            public void onResponse(Call<UpdatePrice> call, retrofit2.Response<UpdatePrice> response) {
                Logs.d("CheckStatus_oe", "onResponse: update price Success"+response);
               try {
                   if (response.isSuccessful() && response.body() != null) {
                       if(response.body().getCode() == 200)
                           Logs.d("CheckStatus_oe", "onResponse: update price code "+response.body().getCode());
                           Toast.makeText(OePriceUpdateActivity.this,
                                   mStringUtils.getLocalizedString(R.string.updated), Toast.LENGTH_SHORT).show();

                       binding.generalPrice.getText().clear();
                       binding.minPrice.getText().clear();
                       binding.modalPrice.getText().clear();
                       binding.maxPrice.getText().clear();
                   } else {
                       Toast.makeText(OePriceUpdateActivity.this,
                               mStringUtils.getLocalizedString(R.string.upload_failed_try_again), Toast.LENGTH_SHORT).show();}
               }catch (Exception e){e.printStackTrace();
                   Logs.d("CheckStatus_oe", "Exception "+e.getMessage());}
            }
            @Override
            public void onFailure(Call<UpdatePrice> call, Throwable t) {
                Log.d("CheckStatus_oe", "oe update price fail "+t.getMessage());
                Utils.showToast(OePriceUpdateActivity.this, t.getMessage());

            }
        });
    }
}
