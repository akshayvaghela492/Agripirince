package com.agriiprince.mvvm.ui.registration.infostepsfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.chip.Chip;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.model.FarmerProfileModel;
import com.agriiprince.utils.StringUtils;

public class InfoPriceFragment extends Fragment {

    private static final String TAG = InfoPriceFragment.class.getName();

    private int PRICE_UNIT_INDEX = 0;

    private OnInfoPriceInteractionListener mListener;

    private AppCompatImageView mBtnBack;

    private AppCompatButton mBtnNext;

    private Chip kg, tonne, quintal;

    private String[] priceUnits;

    public InfoPriceFragment() {
        // Required empty public constructor
    }

    public static InfoPriceFragment newInstance() {
        InfoPriceFragment fragment = new InfoPriceFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info_price, container, false);

        StringUtils stringUtils = AppController.getInstance().getStringUtils();
        priceUnits = FarmerProfileModel.priceUnits;

        ((TextView) view.findViewById(R.id.title)).setText(stringUtils.getLocalizedString(R.string.info_price_title));
        ((AppCompatButton) view.findViewById(R.id.info_next)).setText(stringUtils.getLocalizedString(R.string.next));

        mBtnBack = view.findViewById(R.id.info_back);

        mBtnNext = view.findViewById(R.id.info_next);

        kg = view.findViewById(R.id.info_price_chip_kg);
        quintal = view.findViewById(R.id.info_price_chip_quintal);
        tonne = view.findViewById(R.id.info_price_chip_tonne);

        kg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeChipSelection();
                PRICE_UNIT_INDEX = 0;
                kg.setChecked(true);
            }
        });

        quintal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeChipSelection();
                PRICE_UNIT_INDEX = 1;
                quintal.setChecked(true);

            }
        });

        tonne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeChipSelection();
                PRICE_UNIT_INDEX = 2;
                tonne.setChecked(true);

            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, priceUnits[PRICE_UNIT_INDEX]);
                mListener.onInfoPriceSuccess(priceUnits[PRICE_UNIT_INDEX]);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnInfoPriceInteractionListener) {
            mListener = (OnInfoPriceInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnInfoLocationInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void removeChipSelection() {
        kg.setChecked(false);
        quintal.setChecked(false);
        tonne.setChecked(false);
    }

    public interface OnInfoPriceInteractionListener {

        void onInfoPriceSuccess(String unit);

    }
}
