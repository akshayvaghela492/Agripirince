package com.agriiprince.mvvm.ui.subscription;

import android.arch.lifecycle.ViewModel;

import com.agriiprince.mvvm.model.User;
import com.agriiprince.mvvm.data.repository.SubscriptionRepository;
import com.agriiprince.mvvm.data.repository.SubscriptionRepositoryUseCase;
import com.agriiprince.mvvm.retrofit.service.SubscriptionService;
import com.agriiprince.mvvm.di.ViewModelKey;
import com.agriiprince.mvvm.viewmodel.SubscriptionViewModel;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import retrofit2.Retrofit;

@Module
public class SubscriptionModule {

    @Provides
    SubscriptionRepositoryUseCase providesLoginRepositoryUseCase(SubscriptionService subscriptionService, User user) {
        return new SubscriptionRepository(subscriptionService, user);
    }

    @Provides
    SubscriptionService providesSubscriptionService(Retrofit retrofit) {
        return retrofit.create(SubscriptionService.class);
    }

    @Module(
            includes = {
                    SubscriptionModule.class
            }
    )
    public interface BindSubscriptionViewModel {

        @Binds
        @IntoMap
        @Singleton
        @ViewModelKey(SubscriptionViewModel.class)
        ViewModel bindsSubscriptionViewModel(SubscriptionViewModel subscriptionViewModel);
    }
}
