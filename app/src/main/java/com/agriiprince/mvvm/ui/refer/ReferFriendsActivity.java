package com.agriiprince.mvvm.ui.refer;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;

import com.agriiprince.R;
import com.agriiprince.databinding.ActivityReferFriendsBinding;
import com.agriiprince.mvvm.model.Contact;
import com.agriiprince.mvvm.ui.base.BaseActivity;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.mvvm.viewmodel.ReferFriendsViewModel;
import com.agriiprince.mvvm.viewmodel.ReferFriendsViewModelUseCase;

import java.util.List;

import javax.inject.Inject;

public class ReferFriendsActivity extends BaseActivity {

    private static final String TAG = "ReferFriendsActivity";

    public static Intent newIntent(Context context) {
        return new Intent(context, ReferFriendsActivity.class);
    }

    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    @Inject
    ReferFriendsAdapter mAdapter;

    private ReferFriendsViewModelUseCase mReferViewModel;

    private ActivityReferFriendsBinding mBinding;

    @Override
    protected void setViewAndPerformDataBinding() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_refer_friends);
    }

    @Override
    protected void initControl() {
        mReferViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ReferFriendsViewModel.class);
    }

    @Override
    protected void initViewControl() {
        setupToolbar(mBinding.toolbar, true, getBilingualString(R.string.refer_friends));

        mBinding.recycler.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recycler.setAdapter(mAdapter);

        showProgressDialog();
    }

    @Override
    protected void initTextViews() {
        setBilingualText(mBinding.sendMessage, R.string.send_message);
    }

    @Override
    protected void initListener() {
        mReferViewModel.getContacts().observe(this, contactsObserver);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_refer_friends, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.select_all:
                Logs.d(TAG, "onOptionsItemSelected: select all");
                mReferViewModel.selectAllContacts();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private Observer<List<Contact>> contactsObserver = new Observer<List<Contact>>() {
        @Override
        public void onChanged(@Nullable List<Contact> contacts) {
            if (contacts != null) {
                mAdapter.replaceAllItem(contacts);
                hideProgressDialog();
            }
        }
    };
}
