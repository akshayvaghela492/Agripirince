package com.agriiprince.mvvm.ui.registration.adapters;

/**
 * Created by dtrah on 11/13/2018.
 */

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.agriiprince.db.DatabaseManager;
import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.model.SelectedCropModel;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.mvvm.retrofit.model.croplist.CropByClass;
import com.agriiprince.utils.StringUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ImageAdapter extends BaseAdapter {

    private Context context;
    public static ArrayList<SelectedCropModel> modelArrayList;
    public List<CropByClass> cropArray;
    public static ArrayList<SelectedCropModel> modelList;
    ArrayList<String> list = new ArrayList<>();
    ArrayList<String> idlist = new ArrayList<>();
    ArrayList<String> cropNamelist = new ArrayList<>();
    List<Crop> mCrops = new ArrayList<>();
    private StringUtils utils;
    DatabaseManager mDatabase;

    public ImageAdapter(Context context, ArrayList<SelectedCropModel> modelArrayList, List<CropByClass> cropArray, List<Crop> mCrops) {
        final String TAG = ImageAdapter.class.getSimpleName();
        this.context = context;
        ImageAdapter.modelArrayList = modelArrayList;
        this.cropArray = cropArray;
        this.mCrops = mCrops;
        utils = AppController.getInstance().getStringUtils();
        mDatabase = new DatabaseManager(context);
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getCount() {
        return modelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return modelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.showimage, null, true);

            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox1);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageView1);
            holder.cropName = convertView.findViewById(R.id.cropName);
            convertView.setTag(holder);
        } else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            String[] parts = modelArrayList.get(position).getCrop().split("\\(");
            String part0 = parts[0].toLowerCase();
            String image = part0 + Config.IMAGE_NAME_END;
            String fullUrl = Config.CROP_IMAGE_BASE_URL + image + ".png";
            fullUrl = fullUrl.replace(" ", "+");
            Log.d("crop_image_url ",fullUrl);
            //final boolean validUrl = exists(fullUrl);
            //final boolean validUrl = URLUtil.isValidUrl(fullUrl);
            //if(validUrl) {
                Glide.with(context)
                        .setDefaultRequestOptions(new RequestOptions()
                              .diskCacheStrategy(DiskCacheStrategy.ALL))
                        .load(fullUrl).into(holder.imageView);


        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.checkBox.setChecked(modelArrayList.get(position).getSelected());
        holder.cropName.setText(utils.getBilingualStringForApi(modelArrayList.get(position).getCrop(),
                Crop.getSecondNameByCropName(mCrops, modelArrayList.get(position).getCrop())));
        holder.checkBox.setTag(R.integer.btnplusview, convertView);
        holder.checkBox.setTag(position);
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("CHECK_BOX", " ->>>>>>>> 1");
                View tempview = (View) holder.checkBox.getTag(R.integer.btnplusview);
                Log.e("CHECK_BOX", " ->>>>>>>> 2");
                //TextView tv = (TextView) tempview.findViewById(R.id.animal);
                Integer pos = (Integer) holder.checkBox.getTag();
                Log.e("CHECK_BOX", " ->>>>>>>> 3");

                if (modelArrayList.get(pos).getSelected()) {
                    Log.e("CHECK_BOX", " ->>>>>>>> 4");
                    modelArrayList.get(pos).setSelected(false);
                    Log.e("CHECK_BOX", " ->>>>>>>> 5");
                } else {
                    Log.e("CHECK_BOX", " ->>>>>>>> 6");
                    modelArrayList.get(pos).setSelected(true);
                    Log.e("CHECK_BOX", " ->>>>>>>> 7");
                    showVarieties(modelArrayList.get(pos), holder);
                    Log.e("CHECK_BOX", " ->>>>>>>> 8");
                    AppController.currentScreen = 3;
                    Log.e("CHECK_BOX", " ->>>>>>>> 9");
                    Log.e("setCurrentScreen", " ->>>>>>>> 3");
                }
            }
        });

        return convertView;
    }

    public void showVarieties(final SelectedCropModel cropName, ViewHolder holder) {
        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.choose_variety);
        RecyclerView variety_list = dialog.findViewById(R.id.variety_list);
        Button saveVarieties = dialog.findViewById(R.id.saveVarieties);
        Button cancelVarieties = dialog.findViewById(R.id.cancelVarieties);

        final ViewHolder tempHolder = holder;

        saveVarieties.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (cropName.getVarieties() != null) {
                    if (cropName.getVarieties().size() > 0) {
                        for (int i = 0; i < cropName.getVarieties().size(); i++) {
                            //save crop data in local db
                            if (cropName.getVarieties().get(i).getSelected() == true) {
                                mDatabase.addToCropTable(cropName.getVarieties().get(i).getCropId(), cropName.getVarieties().get(i).getCrop(), cropName.getVarieties().get(i).getCropName());
                            }
                        }
                    } else {
                        Log.d("FIRST ELSE", "------>>>>>>>");
                        cropName.setSelected(false);
                        tempHolder.checkBox.setChecked(false);
                    }
                } else {
                    Log.d("SECOND ELSE", "------>>>>>>>");
                    cropName.setSelected(false);
                    tempHolder.checkBox.setChecked(false);
                }

                cropName.setVarieties(null);
                AppController.currentScreen = 2;
                if (cropName.getVarieties() != null)
                    Log.d("setCurrentScreen", " ->>>>>>>> back to 2" + cropName.getVarieties().size());
                dialog.dismiss();
            }
        });

        cancelVarieties.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AppController.currentScreen = 2;
                Log.e("setCurrentScreen", " ->>>>>>>> back to 2");
                cropName.setSelected(false);
                tempHolder.checkBox.setChecked(false);
                dialog.dismiss();
            }
        });
        variety_list.setLayoutManager(new LinearLayoutManager(context));
        list = new ArrayList<>();
        idlist = new ArrayList<>();
        cropNamelist = new ArrayList<>();
        for (int i = 0; i < cropArray.size(); i++) {
        /*    Object cropObj = null;
            try { cropArray.get(i);
                String crop = cropArray.get(i).("crop_name");
                if (crop.equalsIgnoreCase(cropName.getCrop())) {
                    list.add(cropObj.get("crop_variety").toString());
                    idlist.add(cropObj.get("crop_id").toString());
                    cropNamelist.add(cropObj.get("crop_name").toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            */
        try {
            String crop = cropArray.get(i).getCrop_name();
            if (crop.equalsIgnoreCase(cropName.getCrop())) {
                list.add(cropArray.get(i).getCrop_variety());
                idlist.add(cropArray.get(i).getCrop_id());
                cropNamelist.add(cropArray.get(i).getCrop_name());
            }
        }catch (Exception e){e.printStackTrace();}
        }
        //ADAPTER
        modelList = getModel(false);
        VarietyAdapter adapter = new VarietyAdapter(context, modelList, holder, cropName, cropArray, mCrops);
        variety_list.setAdapter(adapter);
        dialog.show();
    }

    public class ViewHolder {

        protected CheckBox checkBox;
        private ImageView imageView;
        private TextView cropName;
    }

    private ArrayList<SelectedCropModel> getModel(boolean isSelect) {
        ArrayList<SelectedCropModel> templist = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            SelectedCropModel model = new SelectedCropModel();
            model.setSelected(isSelect);
            model.setCrop(list.get(i));
            model.setCropId(idlist.get(i));
            model.setCropName(cropNamelist.get(i));
            templist.add(model);
        }
        return templist;
    }
}