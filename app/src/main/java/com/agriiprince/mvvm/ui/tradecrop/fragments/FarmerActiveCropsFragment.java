package com.agriiprince.mvvm.ui.tradecrop.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.databinding.FragmentFarmerTradeActiveCropsBinding;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.applevel.threadmanagement.AppExecutors;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.FarmerCrop;
import com.agriiprince.mvvm.retrofit.model.tradecrop.CropDetails;
import com.agriiprince.mvvm.room.database.AppDatabase;
import com.agriiprince.mvvm.room.entity.trade_crop.CropListDetailsEntity;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.service.CropInterface;
import com.agriiprince.mvvm.ui.tradecrop.adapters.CropDetailsAdapter;
import com.agriiprince.mvvm.ui.tradecrop.tabs.PageViewModel;
import com.agriiprince.mvvm.util.Logs;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A placeholder fragment containing a simple view.
 */
public class FarmerActiveCropsFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private FragmentFarmerTradeActiveCropsBinding binding;

    private PageViewModel pageViewModel;
    private RecyclerView mRecyclerView;

    private UserProfile mUserProfile;

    // Member variable for the Database
    private AppDatabase mDb;
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */

    public static FarmerActiveCropsFragment newInstance(int index) {
        FarmerActiveCropsFragment fragment = new FarmerActiveCropsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        mUserProfile = new UserProfile(this.getContext());
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        //View root = inflater.inflate(R.layout.fragment_farmer_trade_active_crops, container, false);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_farmer_trade_active_crops, container, false);

        getfarmercrop();

        //mRecyclerView = root.findViewById(R.id.item_list);
        mRecyclerView=binding.itemlist.itemList;
        assert mRecyclerView != null;
        return binding.getRoot();
    }

    private void saveToRoomFromAPI(@NonNull ArrayList<CropDetails> cropDetailsArrayList) {
        for(CropDetails cropDetails: cropDetailsArrayList) {
            CropListDetailsEntity cropListDetailsEntity = new CropListDetailsEntity(cropDetails.getId().toString(),
                    null, null, null, cropDetails.getSowingCrop(),
                    cropDetails.getSowingCropVariety(), cropDetails.getSowingCropGrade(), null,
                    null, null, null, null, null,
                    null, null, null, null, null, null,
                    null, null, null, null, null,
                    null, null);

            insertToRoom(cropListDetailsEntity);
        }
    }

    private void insertToRoom(final CropListDetailsEntity cropDetailsToInsert) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.cropListDetailsDao().insertCropDetail(cropDetailsToInsert);
                Log.e("ROOMTEST ===> ", "insert success");
            }
        });
    }

    private void loadFromRoomToUI(@NonNull RecyclerView recyclerView) {

        setupRecyclerView((RecyclerView) recyclerView);
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new CropDetailsAdapter(mCropList, this.getContext()));
        //recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(this, DummyContent.ITEMS, false));
    }

    private API_Manager apiManager=new API_Manager();
    private CropInterface retrofit_interface = apiManager.getClient2().create(CropInterface.class);
    private ArrayList<CropDetails> mCropList = new ArrayList<>();
    //private static List<CropDetails> mCropListStatic = new ArrayList<>();

    public void getfarmercrop() {

        Call<FarmerCrop> call = retrofit_interface.getFarmercrop(
                mUserProfile.getUserId(), mUserProfile.getApiToken());
        call.enqueue(new Callback<FarmerCrop>() {
            @Override
            public void onResponse(Call<FarmerCrop> call, Response<FarmerCrop> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code getfarmercrop ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Log.d("CheckStatus", response.body().getMessage());
                    mCropList = (ArrayList) response.body().getData();

                    //saveToRoomFromAPI(mCropList);


                    setupRecyclerView((RecyclerView) mRecyclerView);


                    //mCropListStatic = mCropList;

                    //for(int i=0;i<mCropList.size();i++)
                    //{
                    //    Log.d("crop",mCropList.get(i).getSowingCrop());
                    //}
                } catch (Exception e)
                {e.getMessage();}
            }

            @Override
            public void onFailure(Call<FarmerCrop> call, Throwable t) {
                Log.d("CheckStatus", "farmer crop  "+ t.getMessage());
            }
        });
    }

    public void getCrop(int position)
    {

    }
}