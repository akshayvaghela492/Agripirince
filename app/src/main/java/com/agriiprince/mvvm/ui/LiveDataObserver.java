package com.agriiprince.mvvm.ui;

import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import com.agriiprince.mvvm.retrofit.ResponseWrapper;

public abstract class LiveDataObserver<T> implements Observer<ResponseWrapper<T>> {

    /**
     * response status code
     */
    private int statusCode;


    /**
     * String res id to display a message to user according to the error time
     */
    @StringRes
    private int errorMessageId;

    @Override
    public void onChanged(@Nullable ResponseWrapper<T> responseWrapper) {
        if (responseWrapper != null) {
            statusCode = responseWrapper.getStatusCode();
            errorMessageId = responseWrapper.getErrorMessageId();

            switch (responseWrapper.getStatus()) {
                case NO_CONNECTIVITY:
                    onNetworkFail();
                    break;
                case LOADING:
                    onLoading();
                    break;
                case SUCCESS:
                    onSuccess(responseWrapper.getData());
                    break;
                case ERROR:
                    onFail(responseWrapper.getError());
                    break;
            }
        }
    }

    /**
     * returns the status code of the response
     * @return int
     */
    protected int getStatusCode() {
        return statusCode;
    }


    protected int getErrorMessageId() { return errorMessageId; }

    /**
     * this method is called on when api call initiated
     */
    protected abstract void onLoading();

    /**
     * on success
     * @param data {@link T}
     */
    protected abstract void onSuccess(T data);

    /**
     * Called on Error.
     * Note this method is not called when there is no network instead onNetworkFail is called
     * @param t throwable error
     */
    protected abstract void onFail(Throwable t);

    /**
     * this method called when there is no network connection
     */
    protected abstract void onNetworkFail();

    public void observe() {
        new Observer<T>() {
            @Override
            public void onChanged(@Nullable T t) {
                LiveDataObserver.this.observe();
                LiveDataObserver.this.onSuccess(t);
            }
        };
    }
}
