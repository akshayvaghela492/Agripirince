package com.agriiprince.mvvm.ui.tradecrop.tabs;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.agriiprince.R;
import com.agriiprince.mvvm.ui.tradecrop.fragments.FarmerViewDetailsGrowingFragment;
import com.agriiprince.mvvm.ui.tradecrop.fragments.FarmerViewDetailsHarvestingFragment;
import com.agriiprince.mvvm.ui.tradecrop.fragments.FarmerViewDetailsSowingFragment;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class ViewCropSectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.trade_crop_view_tab_text_1,
            R.string.trade_crop_view_tab_text_2, R.string.trade_crop_view_tab_text_3};
    private final Context mContext;

    public ViewCropSectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        if(position==0)
            return FarmerViewDetailsSowingFragment.newInstance(position + 1);
        else if(position==1)
            return FarmerViewDetailsGrowingFragment.newInstance(position + 1);
        else
            return FarmerViewDetailsHarvestingFragment.newInstance(position + 1);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 3;
    }
}