package com.agriiprince.mvvm.ui.tradecrop.adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.agriiprince.R;
import com.agriiprince.databinding.ItemManageCropCardBinding;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.DeleteCrop;
import com.agriiprince.mvvm.retrofit.model.tradecrop.CropDetails;
import com.agriiprince.mvvm.retrofit.model.tradecrop.Media;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.service.CropInterface;
import com.agriiprince.mvvm.ui.tradecrop.activities.TradeCropViewDetailsActivity;
import com.agriiprince.mvvm.util.Logs;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CropDetailsAdapter extends RecyclerView.Adapter<CropDetailsAdapter.MyViewHolder> {

    private List<CropDetails> cropDetailsList;
    private Context mContext;
    private List<String> mURLs = new ArrayList<>();
    private UserProfile mUserProfile;

    private ItemManageCropCardBinding binding;

    public class MyViewHolder extends RecyclerView.ViewHolder {
       public TextView crop, variety, grade, grade_quantity, quantity, harvest_date;
        public ToggleButton mToggle;
        public RecyclerView mRVImages;
        public Button mMenuButton;

        private ItemManageCropCardBinding binding;
        public MyViewHolder(ItemManageCropCardBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
            /*     crop = (TextView) view.findViewById(R.id.card_crop);
            variety = (TextView) view.findViewById(R.id.card_variety);
            grade = (TextView) view.findViewById(R.id.card_grade);
            grade_quantity = (TextView) view.findViewById(R.id.card_grade_quantity);
            quantity = (TextView) view.findViewById(R.id.card_quantity);
            harvest_date = (TextView) view.findViewById(R.id.card_harvest_date);
            mToggle = view.findViewById(R.id.toggle);
            mRVImages = view.findViewById(R.id.card_images_list);
            mMenuButton = view.findViewById(R.id.menu_button);
        */
        }
    }


    public CropDetailsAdapter( List<CropDetails> cropDetailsList, Context context) {
        this.cropDetailsList = cropDetailsList;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_manage_crop_card, parent, false);
        //return new MyViewHolder(itemView);

         binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_manage_crop_card, parent, false);
        MyViewHolder viewHolder=new MyViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CropDetails crop = cropDetailsList.get(position);
    /*    holder.crop.setText(crop.getSowingCrop());
        holder.variety.setText(crop.getSowingCropVariety());
        holder.grade.setText(crop.getSowingCropGrade());
        holder.grade_quantity.setText((crop.getHarvestingCropGrade1()+" "+
                crop.getHarvestingCropGrade2()+" "+crop.getHarvestingCropGrade3()).toString());
        holder.quantity.setText("80Kg");
        holder.harvest_date.setText(crop.getHarvestingCropStartDate());
*/
        holder.binding.cardCrop.setText(crop.getSowingCrop());
        holder.binding.cardVariety.setText(crop.getSowingCropVariety());
        holder.binding.cardGrade.setText(crop.getSowingCropGrade());
        holder.binding.cardGradeQuantity.setText((crop.getHarvestingCropGrade1()+" "+
                crop.getHarvestingCropGrade2()+" "+crop.getHarvestingCropGrade3()).toString());
        holder.binding.cardQuantity.setText("80Kg");
        holder.binding.cardHarvestDate.setText(crop.getHarvestingCropStartDate());

        // final Context context = holder.crop.getContext();
        //final Context context = holder.binding.cardCrop.getContext();

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context, TradeCropViewDetailsActivity.class);
                context.startActivity(intent);
            }
        });

        //final PopupMenu popup = new PopupMenu(context, holder.binding.menuButton);
        final PopupMenu popup = new PopupMenu(binding.cardCrop.getContext(), holder.binding.menuButton);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.tradecrop_card_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener(position));

        holder.binding.menuButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(context, "Click event works.", Toast.LENGTH_SHORT).show();
                Toast.makeText(binding.cardCrop.getContext(), "Click event works.", Toast.LENGTH_SHORT).show();
                popup.show();
            }
        });

        /*ImageView[] arr = new ImageView[4];
        arr[0] = new ImageView(holder.mRVImages.getContext());
        arr[0].setBackgroundResource(R.drawable.ap_logo_final);

        arr[1] = new ImageView(holder.mRVImages.getContext());
        arr[1].setBackgroundResource(R.drawable.ap_logo_final);

        arr[2] = new ImageView(holder.mRVImages.getContext());
        arr[2].setBackgroundResource(R.drawable.ap_logo_final);

        arr[3] = new ImageView(holder.mRVImages.getContext());
        arr[3].setBackgroundResource(R.drawable.ap_logo_final);


        URL url = null;
        try {
            url = new URL("https://farmerimage.s3-ap-south-1.amazonaws.com/farmerimage/apple.jpg");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Bitmap bmp = null;
        try {
            bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        //arr[3].setImageBitmap(bmp);

        Glide.with(holder.mRVImages.getContext()).load("https://farmerimage.s3-ap-south-1.amazonaws.com/farmerimage/apple.jpg").into(arr[3]);
*/

        getmedia(holder);

    //holder.mRVImages.setAdapter(new ImageAdapter(arr));
        // holder.mRVImages.setAdapter(new ImageAdapter(mURLs));

        holder.binding.cardImagesList.setAdapter(new ImageAdapter(mURLs));

        final MyViewHolder finalHolder = holder;

        holder.binding.toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               // if(isChecked == true) finalHolder.mRVImages.setVisibility(View.VISIBLE);
                if(isChecked == true) binding.cardImagesList.setVisibility(View.VISIBLE);
                //else if(isChecked == false) finalHolder.mRVImages.setVisibility(View.GONE);
                else if(isChecked == false) binding.cardImagesList.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cropDetailsList.size();
    }

    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;
        public MyMenuItemClickListener(int positon) {
            this.position=positon;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {

                case R.id.action_delete:
                    cropDetailsList.remove(position);
                    notifyItemRemoved(position);
                    DeleteCrop(position);
                    return true;
                case R.id.action_edit:

                    return true;
                default:
            }
            return false;
        }
    }

    private API_Manager apiManager=new API_Manager();
    private CropInterface retrofit_interface = apiManager.getClient2().create(CropInterface.class);
    private List<Media> mMediaList;

    public void getmedia(final MyViewHolder holder) {
        mUserProfile = new UserProfile(mContext);
        Call<com.agriiprince.mvvm.retrofit.dto.tradecrop.Media> call = retrofit_interface.getMedialist("17","100",mUserProfile.getApiToken());
        call.enqueue(new Callback<com.agriiprince.mvvm.retrofit.dto.tradecrop.Media>() {
            @Override
            public void onResponse(Call<com.agriiprince.mvvm.retrofit.dto.tradecrop.Media> call, Response<com.agriiprince.mvvm.retrofit.dto.tradecrop.Media> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code getMediaList ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Log.d("CheckStatus", response.body().getMessage());
                    mMediaList=response.body().getData();

                    mURLs.clear();
                    for (int i=0;i<=mMediaList.size();i++) {
                        String url = response.body().getData().get(i).getUrl();
                        //if(mURLs.size()<5)
                        mURLs.add(url);
                        Logs.d("CheckStatus ASD ->", mURLs.size() + "-> " + url);
                    }

                   // holder.mRVImages.setAdapter(new ImageAdapter(mURLs));
                    holder.binding.cardImagesList.setAdapter(new ImageAdapter(mURLs));
                    //Glide.with(holder.mRVImages.getContext()).load(url).into();
                    //}
                }
                catch (Exception e){
                    e.getMessage();
                }

            }

            @Override
            public void onFailure(Call<com.agriiprince.mvvm.retrofit.dto.tradecrop.Media> call, Throwable t) {
                Log.d("CheckStatus", "Media list  "+ t.getMessage());
            }
        });
    }

    public void DeleteCrop(int position) {
        Call<DeleteCrop> call = retrofit_interface.deleteCrop(position+"",mUserProfile.getApiToken());
        call.enqueue(new Callback<DeleteCrop>() {
            @Override
            public void onResponse(Call<DeleteCrop> call, Response<DeleteCrop> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code DeleteCrop ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Log.d("CheckStatus", response.body().getMessage());
                }
                catch (Exception e){e.getMessage();}
            }

            @Override
            public void onFailure(Call<DeleteCrop> call, Throwable t) {
                Log.d("CheckStatus", "Delete crop  "+ t.getMessage());
            }
        });
    }
}