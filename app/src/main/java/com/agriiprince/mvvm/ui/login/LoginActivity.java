package com.agriiprince.mvvm.ui.login;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.lifecycle.ViewModelStore;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.agriiprince.BuildConfig;
import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.dataservice.FcmTokenUpdateDataService;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.MandiModel;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.login.Login;
import com.agriiprince.mvvm.retrofit.dto.login.TokenGenerate;
import com.agriiprince.mvvm.retrofit.dto.login.UserRegistration;
import com.agriiprince.mvvm.retrofit.dto.oe.OeLoginResponse;
import com.agriiprince.mvvm.retrofit.dto.profile.UserUpdate;
import com.agriiprince.mvvm.retrofit.model.login.Profile;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.mvvm.ui.homescreen.FarmerHomeScreenActivity;
import com.agriiprince.mvvm.ui.registration.FarmerInfoActivity;
import com.agriiprince.mvvm.ui.oe.OE_MainActivity;
import com.agriiprince.activities.oe.OeInfoActivity;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.ActivityLoginBinding;
import com.agriiprince.dataservice.SubscriptionDataService;
import com.agriiprince.mvvm.model.User;
import com.agriiprince.mvvm.ui.LiveDataObserver;
import com.agriiprince.mvvm.ui.base.BaseActivity;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.mvvm.viewmodel.LoginViewModel;
import com.agriiprince.mvvm.viewmodel.LoginViewModelUseCase;
import com.agriiprince.mvvm.applevel.service.FcmService;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.Utils;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.ThemeUIManager;
import com.facebook.accountkit.ui.UIManager;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;


import static com.agriiprince.mvvm.model.User.Type.OPERATIVE_EXECUTIVE;
import static com.agriiprince.mvvm.viewmodel.LoginViewModel.LOGIN_FRAGMENT;
import static com.agriiprince.mvvm.viewmodel.LoginViewModel.SIGN_UP_FRAGMENT;

public class LoginActivity extends BaseActivity {//

    private static final String TAG = "LoginActivity";

    private static final int FACEBOOK_REQUEST_CODE = 2018;

    private String mUserTypeName;
    private String mMobile;
    private String mPassword;

    private final int REQUEST_CODE_CROP_SELECTION = 999;

    public static final String MOBILE_NUMBER = "mobile_number";
    public static final String PASSWORD = "mPassword";

    private ProgressDialog mProgressDialog;

    private UserProfile mUserProfile;

    private List<Crop> mCrops;
    private List<MandiModel> mMandies;
    private List<MandiModel> mFullMandiList;


    private AtomicBoolean isCropsLoaded;
    private AtomicBoolean isMandiesLoaded;

    private Location mLocation;

    private String mName = "NewUser";

    private String mStreet;
    private String mCity = "Delhi";
    private String mState;
    private String mPinCode = "100001";

    private List<String> mSelectedCrops;
    private List<String> mSelectedMandies;

    private String mAreaUnit;
    private String mFarmSize;

    private String mPriceUnit;

    private String mNearestMandi;

    private String mIncome = "1";

    private String mUserType;// = "farmer"; // farmer profile activity, so only farmer :)
                                         // how do you know only farmer profile??   ;P

    private String mLanguage = "en";

    private String mUserId;
    private StringUtils utils;

    public static Intent newIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    /**
     * used during otp verification if user is new or logging in
     */
    private boolean isNewUser;

    @Inject
    StringUtils mStringUtils;

    @Inject
    PrefManager mPrefManager;

    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    private LoginViewModelUseCase mLoginViewModel;

    private ActivityLoginBinding mBinding;

    @Override
    protected void setViewAndPerformDataBinding() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        if(getIntent().getStringExtra("WHO_CALLED")!=null) {
            String parentActivityRef = getIntent().getStringExtra("WHO_CALLED");
            if (parentActivityRef.equals("Logout")) {
                Log.d("LOGOUT_TO_HOME ->>> ","    inside if ");
                //mLoginViewModel.generateToken().removeObservers(this);
                //ViewModelStore viewModelStore = new ViewModelStore();
                //viewModelStore.clear();
                // clean token
            }
        }
    }

    @Override
    protected void initControl() {
        mLoginViewModel = ViewModelProviders.of(this, mViewModelFactory).get(LoginViewModel.class);
    }

    @Override
    protected void initViewControl() {
        replaceWithLoginFragment();
    }

    @Override
    protected void initTextViews() {

    }

    @Override
    protected void initListener() {
        mLoginViewModel.onSwitchFragment()
                .observe(this, new Observer<Integer>() {
                    @Override
                    public void onChanged(@Nullable Integer integer) {
                        if (integer == null) return;

                        if (integer == LOGIN_FRAGMENT) replaceWithLoginFragment();
                        else if (integer == SIGN_UP_FRAGMENT) replaceWithSignUpFragment();
                    }
                });

        mLoginViewModel.onChangeUserType()
                .observe(this, new Observer<String>() {
                    @Override
                    public void onChanged(@Nullable String s) {
                        if (! TextUtils.isEmpty(s)) mUserTypeName = s;
                    }
                });

        mLoginViewModel.onChangeMobile()
                .observe(this, new Observer<String>() {
                    @Override
                    public void onChanged(@Nullable String s) {
                        if (! TextUtils.isEmpty(s)) mMobile = s;
                    }
                });

        mLoginViewModel.onChangePassword()
                .observe(this, new Observer<String>() {
                    @Override
                    public void onChanged(@Nullable String s) {
                        if (! TextUtils.isEmpty(s)) mPassword = s;
                    }
                });

        mLoginViewModel.onVerifyMobile()
                .observe(this, new Observer<String>() {
                    @Override
                    public void onChanged(@Nullable String string) {
                        if (string == null) return;

                        verifyNumberByFacebookAK(string);
                    }
                });

        if (BuildConfig.USER_TYPE.equals(User.Type.FARMER)) {

            mLoginViewModel.onLoginUser()
                    .observe(this, new LiveDataObserver<Login>() {
                        @Override
                        protected void onLoading() {
                            showProgressDialog();
                        }

                        @Override
                        protected void onSuccess(Login data) {
                            hideProgressDialog();
                            generateAccessToken();
                        }

                        @Override
                        protected void onFail(Throwable t) {
                            hideProgressDialog();

                        }

                        @Override
                        protected void onNetworkFail() {
                            hideProgressDialog();

                        }
                    });
        }
        else
        {
            mLoginViewModel.onLoginUserOe()
                    .observe(this, new LiveDataObserver<OeLoginResponse>() {
                        @Override
                        protected void onLoading() {
                            showProgressDialog();
                        }

                        @Override
                        protected void onSuccess(OeLoginResponse data) {
                            hideProgressDialog();
                            generateAccessToken();
                        }

                        @Override
                        protected void onFail(Throwable t) {
                            hideProgressDialog();

                        }

                        @Override
                        protected void onNetworkFail() {
                            hideProgressDialog();

                        }
                    });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("LOGIN BY OTP-1 -> ", "");
        if (requestCode == FACEBOOK_REQUEST_CODE) {
            final AccountKitLoginResult loginResult = AccountKit.loginResultWithIntent(data);

            Logs.d("LOGIN BY OTP0 -> ", "");
            if (loginResult == null || loginResult.wasCancelled()) {
                showDefaultSnackBar(mStringUtils.getLocalizedString(R.string.number_verification_cancelled).toString());

            } else if (loginResult.getError() != null) {
                showDefaultSnackBar(mStringUtils.getLocalizedString(R.string.mobile_verification_failed).toString());

            } else {
                final AccessToken accessToken = loginResult.getAccessToken();
                Logs.d("LOGIN BY OTP1 -> ", "");
                if (accessToken != null) {
                    Logs.d(TAG, "accessToken" + accessToken);

                    if (isNewUser) {
                        Logs.d("LOGIN BY OTP2a -> ", "");

                        if (BuildConfig.USER_TYPE.equals(User.Type.FARMER)) {
                            mUserType = "farmer";
                        } else if (BuildConfig.USER_TYPE.equals(User.Type.OPERATIVE_EXECUTIVE)) {
                            mUserType = "oe";
                        }

                        Log.d("USER_TYPE", " ----> " + mUserType);

                        userRegistration();
                        startHomeActivity();
                        //Set Default Farmer Values Here via Farmer Profile API
                        //startProfilingActivity();
                    } else {
                        Logs.d("LOGIN BY OTP2b -> ", "");
                        mLoginViewModel.loginByOtp();
                    }

                } else {
                    showDefaultSnackBar(mStringUtils.getLocalizedString(R.string.mobile_verification_failed).toString());
                }
            }
        }
    }


    private void userRegistration() {

        Logs.d("CheckStatus", "onResponse: Values " + mUserType.toLowerCase() + " " + mName + " " + mPassword + " " + mCity + " " +
                mPinCode + " " + mMobile + " " + mLanguage);
        Call<UserRegistration> call = retrofit_interface.registration(mUserType.toLowerCase(), mName, mPassword, mCity,
                mPinCode, mMobile, mLanguage, " ");
        call.enqueue(new Callback<UserRegistration>() {
            @Override
            public void onResponse(Call<UserRegistration> call, retrofit2.Response<UserRegistration> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code registration ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus", String.valueOf(response.body().getCode()));
                    Logs.d("CheckStatus", response.body().getMessage());
                    Logs.d("CheckStatus", response.body().getData().getUserId());

                    int error_code = response.body().getCode();
                    Logs.d("CheckStatus", "error_code - " + error_code);


                    if (error_code == 200) {

                        Logs.d("CheckStatus", "Inside");

                        mUserId = response.body().getData().getUserId();

                        Logs.d("CheckStatus", "Inside2");

                        //mPrefManager.setCreatedOn(Calendar.getInstance().getTimeInMillis());
                        //mUserProfile.setCreatedOn(Calendar.getInstance().getTimeInMillis());

                        Logs.d("CheckStatus", "Inside3");

                        onSignUpSuccess();

                        Logs.d("CheckStatus", "Inside4");

                        generateToken();

                        Logs.d("CheckStatus", "Inside5");

                    } else {
                        Toast.makeText(LoginActivity.this, utils.getLocalizedString(R.string.something_went_wrong_try_again), Toast
                                .LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.getMessage();
                }
            }

            @Override
            public void onFailure(Call<UserRegistration> call, Throwable t) {
                Log.d("CheckStatus", "UserRegistration " + t.getMessage());
            }
        });
    }

    private void onSignUpSuccess() {

        mPrefManager = new PrefManager(this);

        mPrefManager.setUserName(mName);
        mPrefManager.setUserId(mUserId);
        mPrefManager.setUserPassword(mPassword);
        mPrefManager.setUserType(mUserType);
        mPrefManager.setCity(mCity);
        mPrefManager.setPinCode(mPinCode);
        mPrefManager.setMobileNo(mMobile);
        mPrefManager.setPrimaryLocale(mLanguage);
        mPrefManager.setNewUser(true);
    }


    API_Manager apiManager=new API_Manager();
    UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);

    private void generateToken() {

        Logs.d("CheckStatus", "onResponseToken: Values " + mPrefManager.getUserId() + " " +  mPrefManager.getUserType().toLowerCase() + " " + mPrefManager.getUserPassword());
        Call<TokenGenerate> call = retrofit_interface.generateToken(mPrefManager.getUserId(), mPrefManager.getUserType().toLowerCase(), mPrefManager.getUserPassword());
        call.enqueue(new Callback<TokenGenerate>() {
            @Override
            public void onResponse(Call<TokenGenerate> call, retrofit2.Response<TokenGenerate> response) {
                try {

                    Logs.d("CheckStatus", "onResponse: code token ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus", response.body().getMessage());
                    Logs.d("CheckStatus", response.body().getData().getToken());

                    int errorCode = response.body().getCode();

                    Log.d("NEW FARMERINFO -> ", errorCode + "");
                    if (errorCode == 200) {
                        Logs.d("CheckStatus", "token---1");
                        mPrefManager.setAccessToken(response.body().getData().getToken());
                        Logs.d("CheckStatus", "token---2");

                        uploadFcmToken();
                        Logs.d("CheckStatus", "token---3");

                        if (BuildConfig.USER_TYPE.equals(OPERATIVE_EXECUTIVE)) {
                            updateProfileOe();
                        }
                        else {
                            updateProfile();
                        }

                        Logs.d("CheckStatus", "token---4");

                    } else if (errorCode == 101) {
                        Logs.d("CheckStatus", "token---5");

                        Toast.makeText(LoginActivity.this, utils.getLocalizedString(R.string.login_to_continue), Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(LoginActivity.this, LoginActivity.class));
                        LoginActivity.this.finish();
                    }

                } catch (Exception e) {
                    e.getMessage();
                    e.printStackTrace();
                    Logs.d("CheckStatus", "token---6");

                    Toast.makeText(LoginActivity.this, utils.getLocalizedString(R.string.login_to_continue), Toast.LENGTH_SHORT).show();
                    Logs.d("CheckStatus", "token---7");

                    startActivity(new Intent(LoginActivity.this, LoginActivity.class));
                    LoginActivity.this.finish();
                }
            }

            @Override
            public void onFailure(Call<TokenGenerate> call, Throwable t) {
                Log.d("CheckStatus", "token " + t.getMessage());
            }
        });
    }

    private void uploadFcmToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        String token = instanceIdResult.getToken();

                        new FcmTokenUpdateDataService(mPrefManager.getUserId(), mPrefManager.getUserType(), mPrefManager.getAccessToken(), token);
                    }
                });
    }

    private void updateProfile() {
        // mProgressDialog.show();

        Profile params= new Profile();

        Log.d("farmer_profile","login response   1");
        params.setFarmerId(mPrefManager.getUserId());
        params.setFarmerPassword(mPrefManager.getUserPassword());
        params.setFarmerContact(mPrefManager.getMobileNo());
        params.setFarmerName(mPrefManager.getUserName());
        params.setFarmerPincode(mPrefManager.getPinCode());
        params.setFarmerAddress( mPrefManager.getCity());

        Log.d("farmer_profile","login response   2");
        //params.setSecondaryLanguage(mLanguage);
        //params.setAverageAnnualIncome(mIncome);
        //params.setLandInAcres(mFarmSize);
        //params.setPriceMeasuringUnit(mPriceUnit);
        //params.setAreaMeasuringUnit(mAreaUnit);

        /*for (MandiModel model : mMandies) {
            if (model.mandi_id.equals(mNearestMandi)) {
                Log.e("nearest_mandi_name", "--------->" + model.mandi_id);
                params.setNearestMandiName(model.mandi_id);
                params.setNearestMandiLat(model.mandi_lat);
                params.setNearestMandiLog(model.mandi_lng);
            }
        }*/
        //params.setInterestedCropList(mSelectedCrops);
        //params.setInterestedMandiList(mSelectedMandies);
        Log.d("farmer_profile","login response   3");

        API_Manager apiManager=new API_Manager();
        UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);
        Call<UserUpdate> call=retrofit_interface.updateprofile(mPrefManager.getAccessToken(),params);
        call.enqueue(new Callback<UserUpdate>() {
            @Override
            public void onResponse(Call<UserUpdate> call, retrofit2.Response<UserUpdate> response) {
                try {
                    Log.d("farmer_profile","login response"+response.body());
                    if (response.body().getCode() == 200) {

                        startActivity(new Intent(LoginActivity.this, FarmerHomeScreenActivity.class));
                        LoginActivity.this.finish();

                    } else {
                        Toast.makeText(LoginActivity.this, utils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<UserUpdate> call, Throwable t) {
                Log.d("farmer_profile","login fail"+ t.getMessage());
                //mProgressDialog.dismiss();
                Utils.showToast(LoginActivity.this, t.getLocalizedMessage());
            }
        });
    }


    private void updateProfileOe() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_OE_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Logs.d("CheckStatus_oe", "onResponse: update profile Success"+response);
                        //refreshScreen();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Logs.d("CheckStatus_oe", "update profile fail "+error.getMessage());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mPrefManager.getAccessToken());
                //Mandatory
                params.put("user_id", mPrefManager.getUserId());
                params.put("password", mPrefManager.getUserPassword());
                params.put("oe_contact", mPrefManager.getMobileNo());
                params.put("oe_name", mPrefManager.getUserName());
                params.put("pincode", mPrefManager.getPinCode());
                params.put("city", mPrefManager.getCity());

                return params;
            }
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mPrefManager.getAccessToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }


    private void replaceWithLoginFragment() {
        isNewUser = false; // set to false since user is in login screen
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(android.R.id.content, LoginFragment.newInstance());
        fragmentTransaction.commit();
    }

    private void replaceWithSignUpFragment() {
        isNewUser = true; // set to true since user is in sign up screen
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(android.R.id.content, RegisterFragment.newInstance());
        fragmentTransaction.commit();
    }

    private void verifyNumberByFacebookAK(@NonNull String mobileNumber) {
        final Intent intent = new Intent(this, AccountKitActivity.class);

        final AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder
                = new AccountKitConfiguration.AccountKitConfigurationBuilder(
                LoginType.PHONE, AccountKitActivity.ResponseType.TOKEN);

        PhoneNumber phoneNumber = new PhoneNumber("+91", mobileNumber, null);
        configurationBuilder.setInitialPhoneNumber(phoneNumber);

        UIManager themeManager = new ThemeUIManager(R.style.FacebookAccountKitLoginTheme);
        configurationBuilder.setUIManager(themeManager);

        final AccountKitConfiguration configuration = configurationBuilder.build();
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, configuration);

        startActivityForResult(intent, FACEBOOK_REQUEST_CODE);
    }


    private void generateAccessToken() {
        mLoginViewModel.generateToken()
                .observe(this, new LiveDataObserver<TokenGenerate>() {
                    @Override
                    protected void onLoading() {
                        showProgressDialog();
                    }

                    @Override
                    protected void onSuccess(TokenGenerate data) {
                        hideProgressDialog();
                        updateFcmToken();
                        startHomeActivity();
                    }

                    @Override
                    protected void onFail(Throwable t) {
                        hideProgressDialog();

                    }

                    @Override
                    protected void onNetworkFail() {
                        hideProgressDialog();

                    }
                });
    }

    private void updateFcmToken() {
        FcmService.resetInstanceId();
    }

    /**
     * start profiling activity for respective user types when new user registers
     * TODO add other types when they are done. currently only farmer
     * <p>
     * TODO change this stupid registration process. do not wait until user complete his profile. make two registration web services one for just mobile and user type, after profie complete update the rest. THis is stupid
     */
    private void startProfilingActivity() {
        if (BuildConfig.USER_TYPE.equals(User.Type.FARMER)) {
            Intent intent = new Intent(this, FarmerInfoActivity.class);
            intent.putExtra(FarmerInfoActivity.MOBILE_NUMBER, mMobile);
            intent.putExtra(FarmerInfoActivity.PASSWORD, mPassword);
            startActivity(intent);
            finish();
        } else if (BuildConfig.USER_TYPE.equals(User.Type.OPERATIVE_EXECUTIVE)) {
            Intent intent = new Intent(this, OeInfoActivity.class);
            intent.putExtra(OeInfoActivity.MOBILE_NUMBER, mMobile);
            intent.putExtra(OeInfoActivity.PASSWORD, mPassword);
            startActivity(intent);
            finish();
        }
    }

    /**
     * on login success start respective home activities
     */
    private void startHomeActivity() {

        if (BuildConfig.USER_TYPE.equals(User.Type.FARMER)) {
            SubscriptionDataService dataService = new SubscriptionDataService(this);
            dataService.saveSubscriptionStatus();
            startActivity(new Intent(getApplicationContext(), FarmerHomeScreenActivity.class));

        } else if (BuildConfig.USER_TYPE.equals(User.Type.OPERATIVE_EXECUTIVE)) {
            startActivity(new Intent(getApplicationContext(), OE_MainActivity.class));
        }

        /*switch (UserUtils.getUserTypeFromUserName(mUserTypeName)) {
            case COMMISSION_AGENT:
                //go to respective activities
                break;

            case FARMER:
                SubscriptionDataService dataService = new SubscriptionDataService(this);
                dataService.saveSubscriptionStatus();
                startActivity(new Intent(getApplicationContext(), FarmerHomeScreenActivity.class));
                break;

            case TRUCKER_SHIPPER:
                //go to respective activities
                break;

            case FORWARDING_AGENT:
                //go to respective activities
                break;

            case OPERATIVE_EXECUTIVE:
                startActivity(new Intent(getApplicationContext(), OE_MainActivity.class));
                break;

            case TRADER:
                //go to respective activities
                break;

            case PESTICIDE_VENDOR:
                //go to respective activities
                break;
        }*/
        finish();
    }
}
