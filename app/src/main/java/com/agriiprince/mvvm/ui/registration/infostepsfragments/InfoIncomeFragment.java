package com.agriiprince.mvvm.ui.registration.infostepsfragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.databinding.FragmentInfoIncomeBinding;
import com.agriiprince.utils.StringUtils;

public class InfoIncomeFragment extends Fragment {

    private OnInfoIncomeInteractionListener mListener;

    private int INCOME_INDEX = -1;

    private FragmentInfoIncomeBinding binding;

    private StringUtils stringUtils;

    public InfoIncomeFragment() {
        // Required empty public constructor
    }

    public static InfoIncomeFragment newInstance() {
        InfoIncomeFragment fragment = new InfoIncomeFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_info_income, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = DataBindingUtil.bind(view);

        stringUtils = AppController.getInstance().getStringUtils();

        binding.title.setText(stringUtils.getLocalizedString(R.string.what_is_your_yearly_income));
        binding.infoNext.setText(stringUtils.getLocalizedString(R.string.next));

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        binding.infoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        binding.infoNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (INCOME_INDEX != -1) {
                    mListener.onInfoIncomeSuccess(INCOME_INDEX);
                    Log.e("Income_Update", " ---> " + INCOME_INDEX);
                } else {
                    Toast.makeText(getContext(), stringUtils.getLocalizedString(R.string.please_select_your_income), Toast.LENGTH_SHORT).show();
                    Log.e("Income_Update", " ---> " + INCOME_INDEX);
                }
            }
        });

        binding.lpa1Less.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                INCOME_INDEX = 1;
            }
        });

        binding.lpa12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                INCOME_INDEX = 2;
            }
        });

        binding.lpa24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                INCOME_INDEX = 4;
            }
        });

        binding.lpa46.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                INCOME_INDEX = 6;
            }
        });

        binding.lpa610.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                INCOME_INDEX = 10;
            }
        });

        binding.lpa10More.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                INCOME_INDEX = 11;
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnInfoIncomeInteractionListener) {
            mListener = (OnInfoIncomeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnInfoLocationInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnInfoIncomeInteractionListener {
        void onInfoIncomeSuccess(int value);
    }
}
