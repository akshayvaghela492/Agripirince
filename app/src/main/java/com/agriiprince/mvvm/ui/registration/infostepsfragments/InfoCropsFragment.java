package com.agriiprince.mvvm.ui.registration.infostepsfragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.mvvm.ui.registration.AddInterestedCropFragment;
import com.agriiprince.mvvm.ui.registration.ShowInterestedCropFragment;

import java.util.ArrayList;
import java.util.List;

public class InfoCropsFragment extends Fragment {

    private static final String TAG = InfoCropsFragment.class.getSimpleName();

    private TabLayout tabLayout;
    private ViewPager viewPager;
    boolean fromProfile;
    boolean fromOeProfile;

    public InfoCropsFragment() {
        // Required empty public constructor
    }


    public static InfoCropsFragment newInstance(boolean fromprofile, boolean fromOeProfile) {
        InfoCropsFragment fragment = new InfoCropsFragment();
        Bundle args = new Bundle();
        args.putBoolean("fromProfile", fromprofile);
        args.putBoolean("fromOeProfile", fromOeProfile);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fromProfile = getArguments().getBoolean("fromProfile");
            fromOeProfile = getArguments().getBoolean("fromOeProfile");
        }
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "inflate view");
        return inflater.inflate(R.layout.fragment_info_crops, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "view created");
        super.onViewCreated(view, savedInstanceState);


        Log.d(TAG, "view created super called");

//        final Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setTitle("Choose crops");

        final Toolbar toolbar = (Toolbar) getView().findViewById(R.id.toolbar_crop);
        toolbar.setTitle(getResources().getString(R.string.choose_crops));

        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    private void setupViewPager(ViewPager viewPager) {

        Log.d(TAG, "view pager setup");
        final ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFragment(AddInterestedCropFragment.newInstance(fromProfile, fromOeProfile), getResources().getString(R.string.addcropmsg));
        adapter.addFragment(new ShowInterestedCropFragment(), getResources().getString(R.string.showcropmsg));
        viewPager.setAdapter(adapter);

/*
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                adapter.notifyDataSetChanged(); //this line will force all pages to be loaded fresh when changing between fragments
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            Toast.makeText(getActivity(), "Go", Toast.LENGTH_SHORT).show();
//            if(fromProfile) {
//                Intent intent = new Intent(this, FarmerProfileActivity.class); //msgdisplayActivity is activity which display the selected message.
//                startActivityForResult(intent, 1);
//            }
//            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
