package com.agriiprince.mvvm.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.agriiprince.utils.StringUtils;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.HasSupportFragmentInjector;

public abstract class BaseFragment extends Fragment implements HasSupportFragmentInjector {

    private BaseFragmentInteraction mBaseFragmentInteraction;

    @Inject
    StringUtils mStringUtils;

    @Inject
    DispatchingAndroidInjector<Fragment> mFragmentDispatchingAndroidInjector;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        performDataBinding(view);
        initControl();
        initViewControl();
        initTextViews();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initListener();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseFragmentInteraction)
            mBaseFragmentInteraction = (BaseFragmentInteraction) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mBaseFragmentInteraction = null;
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return mFragmentDispatchingAndroidInjector;
    }

    /**
     * return layout resource id for inflating the view
     * @return layout id
     */
    protected abstract @LayoutRes int getLayoutId();

    /**
     * bind the view
     */
    protected abstract void performDataBinding(View view);

    /**
     * Init control.
     */
    protected abstract void initControl();

    /**
     * Init view control.
     */
    protected abstract void initViewControl();

    /**
     * set texts for all the views
     */
    protected abstract void initTextViews();

    /**
     * Init listener.
     */
    protected abstract void initListener();

    /**
     * finishes the fragment if fragment is added to back stack
     */
    protected void removeFragment() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    /**
     * hides soft input keyboard
     */
    protected void hideKeyboard() {
        if (mBaseFragmentInteraction != null) mBaseFragmentInteraction.hideKeyboard();
    }

    /**
     * hides soft input keyboard
     *
     * @param view view which has the focus
     */
    protected void hideKeyboard(@NonNull View view) {
        if (mBaseFragmentInteraction != null) mBaseFragmentInteraction.hideKeyboard(view);
    }

    protected void showProgressDialog() {
        if (mBaseFragmentInteraction != null) mBaseFragmentInteraction.showProgressDialog();
    }

    protected void hideProgressDialog() {
        if (mBaseFragmentInteraction != null) mBaseFragmentInteraction.hideProgressDialog();
    }

    protected void showDefaultSnackBar(@NonNull String message) {
        if (mBaseFragmentInteraction != null) mBaseFragmentInteraction.showDefaultSnackBar(message);
    }

























    /**
     * returns bilingual string for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected String getBilingualString(@StringRes int id){
        return mStringUtils.getLocalizedString(id).toString();
    }

    /**
     * set bilingual string to the text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualText(TextView textView, @StringRes int id){
        this.setBilingualText(textView, id, false);
    }

    /**
     * set bilingual string to the text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualText(TextView textView, @StringRes int id, boolean isSingleLine){
        textView.setText(mStringUtils.getLocalizedString(id, isSingleLine));
    }

    /**
     * set bilingual string to the text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualText(AppCompatTextView textView, @StringRes int id){
        this.setBilingualText(textView, id, false);
    }

    /**
     * set bilingual string to the text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualText(AppCompatTextView textView, @StringRes int id, boolean isSingleLine){
        textView.setText(mStringUtils.getLocalizedString(id, isSingleLine));
    }

    /**
     * set bilingual string to the edit text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualText(EditText editText, @StringRes int id){
        this.setBilingualText(editText, id, false);
    }

    /**
     * set bilingual string to the edit text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualText(EditText editText, @StringRes int id, boolean isSingleLine){
        editText.setText(mStringUtils.getLocalizedString(id, isSingleLine));
    }

    /**
     * set bilingual string to the edit text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualText(AppCompatEditText editText, @StringRes int id){
        this.setBilingualText(editText, id, false);
    }

    /**
     * set bilingual string to the edit text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualText(AppCompatEditText editText, @StringRes int id, boolean isSingleLine){
        editText.setText(mStringUtils.getLocalizedString(id, isSingleLine));
    }

    /**
     * set bilingual hint to the edit text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualHint(EditText editText, @StringRes int id){
        this.setBilingualHint(editText, id, false);
    }

    /**
     * set bilingual hint to the edit text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualHint(EditText editText, @StringRes int id, boolean isSingleLine){
        editText.setHint(mStringUtils.getLocalizedString(id, isSingleLine));
    }

    /**
     * set bilingual hint to the edit text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualHint(AppCompatEditText editText, @StringRes int id){
        this.setBilingualHint(editText, id, false);
    }

    /**
     * set bilingual hint to the edit text view for the string resource id.
     * If two languages are chosen then secondary language will be below primary
     * @param id string res id
     * @return bilingual string
     */
    protected void setBilingualHint(AppCompatEditText editText, @StringRes int id, boolean isSingleLine){
        editText.setHint(mStringUtils.getLocalizedString(id, isSingleLine));
    }
}
