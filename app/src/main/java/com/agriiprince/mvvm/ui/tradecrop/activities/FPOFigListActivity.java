package com.agriiprince.mvvm.ui.tradecrop.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.agriiprince.R;
import com.agriiprince.databinding.ActivityFpoFiglistBinding;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.UserResponse;
import com.agriiprince.mvvm.retrofit.model.tradecrop.UserModel;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.model.FigContact;
import com.agriiprince.mvvm.ui.tradecrop.adapters.FigContactAdapter;
import com.agriiprince.mvvm.util.Logs;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FPOFigListActivity extends AppCompatActivity {

    private ActivityFpoFiglistBinding binding;
    private UserProfile mUserProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_fpo_figlist);
        mUserProfile = new UserProfile(this);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ArrayList<FigContact> figContacts = new ArrayList<>();

        FigContact fc1 = new FigContact("Sharma", "8567543650", 12);
        FigContact fc2 = new FigContact("Singh", "8567543654", 15);
        FigContact fc3 = new FigContact("Sharma", "8567543650", 12);
        FigContact fc4 = new FigContact("Singh", "8567543654", 15);
        FigContact fc5 = new FigContact("Sharma", "8567543650", 12);
        FigContact fc6 = new FigContact("Singh", "8567543654", 15);
        FigContact fc7 = new FigContact("Sharma", "8567543650", 12);
        FigContact fc8 = new FigContact("Singh", "8567543654", 15);
        FigContact fc9 = new FigContact("Sharma", "8567543650", 12);
        FigContact fc10 = new FigContact("Singh", "8567543654", 15);
        FigContact fc11 = new FigContact("Sharma", "8567543650", 12);
        FigContact fc12 = new FigContact("Singh", "8567543654", 15);

        figContacts.add(fc1);
        figContacts.add(fc2);
        figContacts.add(fc3);
        figContacts.add(fc4);
        figContacts.add(fc5);
        figContacts.add(fc6);
        figContacts.add(fc7);
        figContacts.add(fc8);
        figContacts.add(fc9);
        figContacts.add(fc10);
        figContacts.add(fc11);
        figContacts.add(fc12);

        FigContactAdapter figContactAdapter = new FigContactAdapter(getBaseContext(), figContacts);

        binding.litFpoFigcontact.setAdapter(figContactAdapter);

        getfiglist();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    API_Manager apiManager=new API_Manager();
    UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);

    public void getfiglist() {
        Call<UserResponse> call = retrofit_interface.getFigList(mUserProfile.getApiToken(),9);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code getfiglist ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus", response.body().getMessage());


                    ArrayList<FigContact> figContacts = new ArrayList<>();

                    for (int i = 0; i < response.body().getData().size(); i++){

                        UserModel userModel = response.body().getData().get(i);
                        FigContact figContact = new FigContact(userModel.getName(), (String.valueOf(userModel.getMobile())),0);
                        figContacts.add(figContact);
                    }

                    FigContactAdapter figContactAdapter = new FigContactAdapter(getBaseContext(), figContacts);
                    binding.litFpoFigcontact.setAdapter(figContactAdapter);
                } catch (Exception e) {
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Log.d("CheckStatus", "fig list "+ t.getMessage());
            }
        });
    }
}
