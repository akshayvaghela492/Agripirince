package com.agriiprince.mvvm.ui.tradecrop.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.agriiprince.R;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.ui.tradecrop.activities.TradeCropViewDetailsActivity;
import com.agriiprince.mvvm.ui.tradecrop.lists.DummyContent;
import com.agriiprince.mvvm.ui.tradecrop.tabs.PageViewModel;

import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class FarmerReadyToHarvestFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private PageViewModel pageViewModel;

    private UserProfile mUserProfile;
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */

    public static FarmerReadyToHarvestFragment newInstance(int index) {
        FarmerReadyToHarvestFragment fragment = new FarmerReadyToHarvestFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        mUserProfile = new UserProfile(this.getContext());
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_farmer_ready_to_harvest, container, false);

        RecyclerView recyclerView = root.findViewById(R.id.item_list);
       // RecyclerView recyclerView=binding.itemlist.itemList;
        assert recyclerView != null;

        setupRecyclerView((RecyclerView) recyclerView);

        return root;
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(this, DummyContent.ITEMS, false));
    }

    public static class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final FarmerReadyToHarvestFragment mParentActivity;
        private final List<DummyContent.DummyItem> mValues;
        private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DummyContent.DummyItem item = (DummyContent.DummyItem) view.getTag();
                    Context context = view.getContext();
                    Intent intent = new Intent(context, TradeCropViewDetailsActivity.class);

                    context.startActivity(intent);

            }
        };

        SimpleItemRecyclerViewAdapter(FarmerReadyToHarvestFragment parent,
                                      List<DummyContent.DummyItem> items,
                                      boolean twoPane) {
            mValues = items;
            mParentActivity = parent;
        }

        Context mContext;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //View view = LayoutInflater.from(parent.getContext())
                    //.inflate(R.layout.item_list_content, parent, false);

            mContext = parent.getContext();

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_manage_crop_card, parent, false);


            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            //holder.mIdView.setText(mValues.get(position).id);
            //holder.mContentView.setText(mValues.get(position).content);

            /*String[] arr = new String[4];
            arr[0] = "Abc";
            arr[1] = "Bbc";
            arr[2] = "Gbc";
            arr[3] = "Nbc";*/

            ImageView[] arr = new ImageView[4];
            arr[0] = new ImageView(holder.mRVImages.getContext());
            arr[0].setBackgroundResource(R.drawable.ap_logo_final);

            arr[1] = new ImageView(holder.mRVImages.getContext());
            arr[1].setBackgroundResource(R.drawable.ap_logo_final);

            arr[2] = new ImageView(holder.mRVImages.getContext());
            arr[2].setBackgroundResource(R.drawable.ap_logo_final);

            arr[3] = new ImageView(holder.mRVImages.getContext());
            arr[3].setBackgroundResource(R.drawable.ap_logo_final);

            //holder.mRVImages.setAdapter(new ImageAdapter(arr), new ArrayList<String>);

            holder.mToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked == true) holder.mRVImages.setVisibility(View.VISIBLE);
                    else if(isChecked == false) holder.mRVImages.setVisibility(View.GONE);
                }
            });


            final PopupMenu popup = new PopupMenu(mContext, holder.mMenuButton);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.tradecrop_card_menu, popup.getMenu());
            popup.setOnMenuItemClickListener(new MyMenuItemClickListener(position));

            holder.mMenuButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Toast.makeText(mContext, "Click event works.", Toast.LENGTH_SHORT).show();
                    popup.show();
                }
            });

            //holder.itemView.setTag(mValues.get(position));
            holder.itemView.setOnClickListener(mOnClickListener);
        }

        class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

            private int position;
            public MyMenuItemClickListener(int positon) {
                this.position=positon;
            }

            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    /*case R.id.Not_interasted_catugury:
                        String RemoveCategory=mDataSet.get(position).getCategory();
                        // mDataSet.remove(position);
                        //notifyItemRemoved(position);
                        // notifyItemRangeChanged(position,mDataSet.size());

                        mySharedPreferences.saveStringPrefs(Constants.REMOVE_CTAGURY,RemoveCategory, MainActivity.context);
                        Toast.makeText(MainActivity.context, "Add to favourite", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.No_interasted:
                        mDataSet.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position,mDataSet.size());
                        Toast.makeText(MainActivity.context, "Done for now", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.delete:
                        mySharedPreferences.deletePrefs(Constants.REMOVE_CTAGURY,MainActivity.context);
                    */default:
                }
                return false;
            }
        }


        @Override
        public int getItemCount() {
            return mValues.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            //final TextView mIdView;
            //final TextView mContentView;
            final ToggleButton mToggle;
            final RecyclerView mRVImages;
            final Button mMenuButton;

            ViewHolder(View view) {
                super(view);
                mToggle = view.findViewById(R.id.toggle);
                mRVImages = view.findViewById(R.id.card_images_list);
                mMenuButton = view.findViewById(R.id.menu_button);

                //mIdView = (TextView) view.findViewById(R.id.id_text);
                //mContentView = (TextView) view.findViewById(R.id.content);
            }
        }
    }
}