package com.agriiprince.mvvm.ui.tradecrop.activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.agriiprince.R;
import com.agriiprince.adapter.ViewPagerAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.databinding.ActivityFigFarmerListBinding;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.model.FarmerContact;
import com.agriiprince.mvvm.ui.tradecrop.adapters.FarmerContactAdapter;
import com.agriiprince.mvvm.ui.tradecrop.tabs.ManageCropSectionsPagerAdapter;
import com.agriiprince.utils.StringUtils;

import java.util.ArrayList;

public class FIGFarmerListActivity extends AppCompatActivity {

    private static final String TAG = com.agriiprince.activities.farmer.trackcrop.TrackCropActivity.class.getName();

    private ViewPagerAdapter viewPagerAdapter;

    StringUtils utils;

    private ActivityFigFarmerListBinding binding;
    private UserProfile mUserProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_fig_farmer_list);
        mUserProfile = new UserProfile(this);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ArrayList<FarmerContact> farmerContacts = new ArrayList<>();

        FarmerContact fc1 = new FarmerContact("Suresh", "8349028340");
        FarmerContact fc2 = new FarmerContact("Rahul", "8349028340");
        FarmerContact fc3 = new FarmerContact("Suresh", "8349028340");
        FarmerContact fc4 = new FarmerContact("Rahul", "8349028340");
        FarmerContact fc5 = new FarmerContact("Suresh", "8349028340");
        FarmerContact fc6 = new FarmerContact("Rahul", "8349028340");
        FarmerContact fc7= new FarmerContact("Suresh", "8349028340");
        FarmerContact fc8 = new FarmerContact("Rahul", "8349028340");
        FarmerContact fc9 = new FarmerContact("Suresh", "8349028340");
        FarmerContact fc10 = new FarmerContact("Rahul", "8349028340");
        FarmerContact fc11 = new FarmerContact("Suresh", "8349028340");
        FarmerContact fc12 = new FarmerContact("Rahul", "8349028340");


        farmerContacts.add(fc1);
        farmerContacts.add(fc2);
        farmerContacts.add(fc3);
        farmerContacts.add(fc4);
        farmerContacts.add(fc5);
        farmerContacts.add(fc6);
        farmerContacts.add(fc7);
        farmerContacts.add(fc8);
        farmerContacts.add(fc9);
        farmerContacts.add(fc10);
        farmerContacts.add(fc11);
        farmerContacts.add(fc12);

        FarmerContactAdapter farmerContactAdapter = new FarmerContactAdapter(getBaseContext(), farmerContacts);

        binding.listviewFarmercontact.setAdapter(farmerContactAdapter);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ManageCropSectionsPagerAdapter sectionsPagerAdapter =
                new ManageCropSectionsPagerAdapter(this, getSupportFragmentManager());

        utils = AppController.getInstance().getStringUtils();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addNewFarmer(View view) {
        Context context = view.getContext();
        Intent intent = new Intent(context, FIGAddFarmerActivity.class);
        context.startActivity(intent);
    }
}

