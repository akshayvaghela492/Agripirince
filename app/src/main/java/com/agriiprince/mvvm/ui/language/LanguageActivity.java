package com.agriiprince.mvvm.ui.language;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import com.agriiprince.R;
import com.agriiprince.databinding.ActivityLanguageSelectionBinding;
import com.agriiprince.mvvm.model.Language;
import com.agriiprince.mvvm.ui.base.BaseActivity;
import com.agriiprince.mvvm.ui.login.LoginActivity;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.mvvm.viewmodel.LanguageViewModel;
import com.agriiprince.mvvm.viewmodel.LanguageViewModelUseCase;
import com.agriiprince.utils.StringUtils;

import java.util.List;

import javax.inject.Inject;

public class LanguageActivity extends BaseActivity {

    private static final String TAG = "LanguageActivity";

    public static Intent newIntent(Context context) {
        return new Intent(context, LanguageActivity.class);
    }

    @Inject
    StringUtils mStringUtils;

    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    @Inject
    LanguageAdapter mAdapter;

    private LanguageViewModelUseCase mLanguageViewModel;

    private ActivityLanguageSelectionBinding mBinding;

    @Override
    protected void setViewAndPerformDataBinding() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_language_selection);
    }

    @Override
    protected void initControl() {
        mLanguageViewModel = ViewModelProviders.of(this, mViewModelFactory).get(LanguageViewModel.class);
        Logs.d(TAG, "initControl: " + mViewModelFactory + " " + mLanguageViewModel);
    }

    @Override
    protected void initViewControl() {
        mBinding.recycler.setLayoutManager(new GridLayoutManager(this, 2));
        mBinding.recycler.setAdapter(mAdapter);

    }

    @Override
    protected void initTextViews() {

        mBinding.header.setText(mStringUtils.getLocalizedString(R.string.choose_your_preferred_language));
        mBinding.next.setText(mStringUtils.getLocalizedString(R.string.text_continue));
    }

    @Override
    protected void initListener() {
        mLanguageViewModel.getLanguages()
                .observe(this, new Observer<List<Language>>() {
                    @Override
                    public void onChanged(@Nullable List<Language> languages) {
                        if (languages != null) {
                            mAdapter.addAllItem(languages);
                        }
                    }
                });

        mBinding.next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateSelectedLanguage();
                startLoginActivity();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdapter.addListener(onLanguageItemListener); // add listener again on resume
    }

    @Override
    protected void onPause() {
        super.onPause();
        mAdapter.removeListener(); // remove the adapter listener to avoid leaks
    }

    /**
     * listener for recycler view click events
     */
    private LanguageAdapter.OnLanguageItemListener onLanguageItemListener = new LanguageAdapter.OnLanguageItemListener() {
        @Override
        public void onClickItem(int position) {
            int oldPosition = mAdapter.getSelectedLanguagePosition();
            Language oldLanguage = mAdapter.getItem(oldPosition);
            oldLanguage.setSelected(false);
            mAdapter.notifyItemChanged(oldPosition, oldLanguage);

            Language language = mAdapter.getItem(position);
            language.setSelected(true);
            mAdapter.notifyItemChanged(position, language);
        }
    };

    /**
     * update the selected language by the user
     */
    private void updateSelectedLanguage() {
        Language language = mAdapter.getSelectedLanguage();
        mLanguageViewModel.updatePrimaryLocale(language.getLanguageCode());
    }

    /**
     * starts new login activity and finish this activity
     */
    private void startLoginActivity() {
        startActivity(LoginActivity.newIntent(this));
        finish();
    }
}
