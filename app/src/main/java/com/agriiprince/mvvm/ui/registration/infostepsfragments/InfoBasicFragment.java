package com.agriiprince.mvvm.ui.registration.infostepsfragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.agriiprince.R;
import com.agriiprince.adapter.CustomStringArrayAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.databinding.FragmentInfoBasicBinding;
import com.agriiprince.dataservice.DataResponse;
import com.agriiprince.dataservice.PlaceDataService;
import com.agriiprince.model.PlaceModel;
import com.agriiprince.utils.StringUtils;
import com.android.volley.VolleyError;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class InfoBasicFragment extends Fragment {

    private final String TAG = InfoBasicFragment.class.getSimpleName();

    private static final String ARG_STREET = "street";
    private static final String ARG_CITY = "city";
    private static final String ARG_STATE = "state";
    private static final String ARG_PIN_CODE = "pin";

    private OnInfoBasicInteractionListener mListener;

    private String mName;
    private String mStreet;
    private String mCity;
    private String mState;
    private String mPinCode;

    private List<PlaceModel> mPlaces;
    private List<String> mStates;
    private List<String> mCities;

    private FragmentInfoBasicBinding binding;

    public InfoBasicFragment() {
        // Required empty public constructor
    }

    public static InfoBasicFragment newInstance(String street, String city, String state, String pinCode) {
        InfoBasicFragment fragment = new InfoBasicFragment();
        Bundle args = new Bundle();
        args.putString(ARG_STREET, street);
        args.putString(ARG_CITY, city);
        args.putString(ARG_STATE, state);
        args.putString(ARG_PIN_CODE, pinCode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mStreet = getArguments().getString(ARG_STREET);
            mCity = getArguments().getString(ARG_CITY);
            mState = getArguments().getString(ARG_STATE);
            mPinCode = getArguments().getString(ARG_PIN_CODE);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_info_basic, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = DataBindingUtil.bind(view);

        StringUtils stringUtils = AppController.getInstance().getStringUtils();
        mStates = new ArrayList<>();
        mCities = new ArrayList<>();

        binding.title.setText(stringUtils.getLocalizedString(R.string.fill_your_personal_information));
        binding.infoNext.setText(stringUtils.getLocalizedString(R.string.next));

        binding.tilName.setHint(stringUtils.getLocalizedString(R.string.name));
        binding.tilStreet.setHint(stringUtils.getLocalizedString(R.string.street));
        binding.tilState.setHint(stringUtils.getLocalizedString(R.string.select_state));
        binding.tilCity.setHint(stringUtils.getLocalizedString(R.string.select_city));
        binding.tilPin.setHint(stringUtils.getLocalizedString(R.string.pincode));

        binding.infoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        binding.infoNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.infoBasicName.getText() != null)
                    mName = binding.infoBasicName.getText().toString();
                if (binding.infoBasicStreet.getText() != null)
                    mStreet = binding.infoBasicStreet.getText().toString();
                if (binding.infoBasicPin.getText() != null)
                    mPinCode = binding.infoBasicPin.getText().toString();

                if (validateValues(mName, mStreet, mCity, mState, mPinCode)) {
                    mListener.onInfoBasicSuccess(mName, mStreet, mCity, mState, mPinCode);
                }
            }
        });

        binding.infoBasicPin.setOnFocusChangeListener(pinFocusChangeListener);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (!TextUtils.isEmpty(mStreet)) binding.infoBasicStreet.setText(mStreet);
        if (!TextUtils.isEmpty(mPinCode)) binding.infoBasicPin.setText(mPinCode);
        if (!TextUtils.isEmpty(mState)) binding.acState.setText(mState);
        if (!TextUtils.isEmpty(mCity)) binding.acCity.setText(mCity);

        getCities();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnInfoBasicInteractionListener) {
            mListener = (OnInfoBasicInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnInfoLocationInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private View.OnFocusChangeListener pinFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus && binding.infoBasicPin.getText() != null) {
                String pin = binding.infoBasicPin.getText().toString();
                if (pin.length() == 6) {
                    getLocationFromPinCode(pin);
                }
            }
        }
    };

    private void getLocationFromPinCode(String pinCode) {
        try {
            Geocoder geocoder = new Geocoder(getContext());
            List<Address> addresses = geocoder.getFromLocationName(pinCode, 5);
            for (Address address : addresses) {
                if (!TextUtils.isEmpty(address.getAdminArea())) {
                    mState = address.getAdminArea();
                    Log.d(TAG, "mState:  " + mState);
                    binding.acState.setText(mState);

                    binding.acCity.getText().clear();
                    setCityAdapter(mState);
                }
                if (!TextUtils.isEmpty(address.getLocality())) {
                    mCity = address.getLocality();
                    Log.d(TAG, "mCity:  " + mCity);
                    binding.acCity.setText(mCity);
                }
                if (!TextUtils.isEmpty(address.getAddressLine(0))) {
                    mStreet = address.getAddressLine(0);
                    Log.d(TAG, "mStreet:  " + mStreet);
                    binding.infoBasicStreet.setText(mStreet);
                }
                if (mState != null && mCity != null)
                    break;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setStateAdapter() {
        if (getContext() == null) return;
        HashSet<String> hashSet = new HashSet<>();
        for (PlaceModel model : mPlaces) {
            hashSet.add(model.state);
        }

        mStates.clear();
        mStates.addAll(hashSet);

        Collections.sort(mStates);

        Log.d(TAG, "states: " + mStates.toString());

        CustomStringArrayAdapter adapter =
                new CustomStringArrayAdapter(getContext(), android.R.layout.simple_list_item_1, mStates);

        binding.acState.setAdapter(adapter);
        binding.acState.setValidator(stateValidator);
        binding.acState.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String state = parent.getAdapter().getItem(position).toString();
                Log.d(TAG, "state selected: " + state);
                if (!state.equals(mState)) {
                    mState = state;
                    binding.acCity.getText().clear();
                    setCityAdapter(mState);
                }
            }
        });

        binding.acState.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                binding.acState.performValidation();
            }
        });
    }

    private void setCityAdapter(String state) {
        if (getContext() == null) return;

        mCities.clear();
        for (PlaceModel model : mPlaces) {
            if (model.state.equalsIgnoreCase(state))
                mCities.add(model.city);
        }

        Log.d(TAG, "cities: " + mCities.toString());

        Collections.sort(mCities);

        CustomStringArrayAdapter adapter =
                new CustomStringArrayAdapter(getContext(), android.R.layout.simple_list_item_1, mCities);

        binding.acCity.setAdapter(adapter);
        binding.acCity.setValidator(cityValidator);
        binding.acCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mCity = parent.getAdapter().getItem(position).toString();
            }
        });

        binding.acCity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                binding.acCity.performValidation();
            }
        });
    }

    private void getCities() {
        new PlaceDataService(getContext(), new DataResponse<List<PlaceModel>>() {
            @Override
            public void onSuccessResponse(List<PlaceModel> response) {
                mPlaces = response;
                setStateAdapter();
                if (!TextUtils.isEmpty(mState))
                    setCityAdapter(mState);
            }

            @Override
            public void onParseError() {

            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
    }

    private boolean validateValues(String name, String street, String city, String state, String pinCode) {
        if (TextUtils.isEmpty(name)) {
            binding.infoBasicName.setError(getString(R.string.enter_your_name));
            binding.infoBasicName.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(pinCode)) {
            binding.infoBasicPin.setError(getString(R.string.enter_pin_code));
            binding.infoBasicPin.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(state)) {
            binding.acState.setError(getString(R.string.select_state));
            binding.acState.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(city)) {
            binding.acCity.setError(getString(R.string.select_district_town));
            binding.acCity.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(street)) {
            binding.infoBasicStreet.setError(getString(R.string.enter_address));
            binding.infoBasicStreet.requestFocus();
            return false;
        }

        return true;
    }

    private AutoCompleteTextView.Validator stateValidator = new AutoCompleteTextView.Validator() {
        @Override
        public boolean isValid(CharSequence text) {
            return mStates.contains(text.toString());
        }

        @Override
        public CharSequence fixText(CharSequence invalidText) {
            Log.d(TAG, "stateValidator fixText:  " + invalidText);
            ArrayAdapter<String> stateAdapter = (ArrayAdapter<String>) binding.acState.getAdapter();

            stateAdapter.getFilter().filter(invalidText);
            if (stateAdapter.getCount() > 0) {
                String state = stateAdapter.getItem(0);
                Log.d(TAG, "state selected: " + state);
                mState = state;
                binding.acCity.getText().clear();
                setCityAdapter(mState);
                return state;
            } else {
                return invalidText;
            }
        }
    };

    private AutoCompleteTextView.Validator cityValidator = new AutoCompleteTextView.Validator() {
        @Override
        public boolean isValid(CharSequence text) {
            return mCities.contains(text.toString());
        }

        @Override
        public CharSequence fixText(CharSequence invalidText) {
            Log.d(TAG, "cityValidator fixText:  " + invalidText);
            ArrayAdapter<String> cityAdapter = (ArrayAdapter<String>) binding.acCity.getAdapter();

            cityAdapter.getFilter().filter(invalidText);
            if (cityAdapter.getCount() > 0) {
                mCity = cityAdapter.getItem(0);
                Log.d(TAG, "city selected: " + mCity);
                return cityAdapter.getItem(0);
            } else {
                return invalidText;
            }
        }
    };

    public interface OnInfoBasicInteractionListener {
        void onInfoBasicSuccess(String name, String street, String city, String state, String pinCode);
    }
}
