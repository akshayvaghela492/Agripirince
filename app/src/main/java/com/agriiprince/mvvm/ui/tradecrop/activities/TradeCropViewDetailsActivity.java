package com.agriiprince.mvvm.ui.tradecrop.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;

import com.agriiprince.R;
import com.agriiprince.adapter.ViewPagerAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.databinding.ActivityFarmerTradeCropdetailBinding;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.ui.tradecrop.tabs.ViewCropSectionsPagerAdapter;
import com.agriiprince.utils.StringUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;
import java.util.List;

public class TradeCropViewDetailsActivity extends AppCompatActivity {

    private static final String TAG = com.agriiprince.activities.farmer.trackcrop.TrackCropActivity.class.getName();

    private ViewPagerAdapter viewPagerAdapter;
    private UserProfile mUserProfile;

    StringUtils utils;

    private ActivityFarmerTradeCropdetailBinding binding;
    List<String> imageUrls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_farmer_trade_cropdetail);
        mUserProfile = new UserProfile(this);

        imageUrls = new ArrayList<>();

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ViewCropSectionsPagerAdapter sectionsPagerAdapter = new ViewCropSectionsPagerAdapter(this, getSupportFragmentManager());
        binding.viewpager.setAdapter(sectionsPagerAdapter);
        binding.tabs.setupWithViewPager(binding.viewpager);
        binding.carouselView.setImageListener(imageListener);

        utils = AppController.getInstance().getStringUtils();
    }

    private ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            try {
                Glide.with(TradeCropViewDetailsActivity.this)
                        .setDefaultRequestOptions(new RequestOptions()
                                .diskCacheStrategy(DiskCacheStrategy.ALL))
                        .load(imageUrls.get(position))
                        .into(imageView);
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

