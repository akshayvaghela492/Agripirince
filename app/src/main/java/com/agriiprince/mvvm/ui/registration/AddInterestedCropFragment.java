package com.agriiprince.mvvm.ui.registration;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.mvvm.ui.oe.OeProfileActivity;
import com.agriiprince.adapter.CropClassAdapter;
import com.agriiprince.mvvm.retrofit.model.croplist.CropByClass;
import com.agriiprince.mvvm.retrofit.model.login.Profile;
import com.agriiprince.mvvm.ui.registration.adapters.ImageAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.dataservice.CropDataService;
import com.agriiprince.db.DatabaseManager;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.SelectedCropModel;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.mvvm.model.crop.CropClass;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.croplist.Croplist_byClass;
import com.agriiprince.mvvm.retrofit.dto.profile.UserUpdate;
import com.agriiprince.mvvm.retrofit.service.CropList;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.Utils;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.agriiprince.utils.Utils.showToast;

public class AddInterestedCropFragment extends Fragment implements View.OnClickListener, SelectCropActivity.OnBackPressedListener {

    private final String TAG = AddInterestedCropFragment.class.getSimpleName();

    private static final String ARG_FROM_PROFILE = "from_profile";
    private static final String ARG_FROM_OE_PROFILE = "from_oe_profile";

    List<String> ImageList = new ArrayList<>();
    RelativeLayout rel_fruits, rel_vegetable, rel_herbs, rel_spices, rel_nuts, rel_pulses, rel_grains, rel_others;
    private StringUtils utils;
    private ProgressDialog mProgressDialog;
    private UserProfile mUserProfile;
    GridView gridView;
    List<CropByClass> cropArray;
    FrameLayout select_crop_class;
    ImageView close_selectcrop;
    private LinearLayout lin_cropClass;
    private ImageAdapter imageAdapter;
    Button savebtn, cancelbtn;
    private boolean clicked = false;
    private List<Crop> mCrops;
    private RecyclerView rvCropClass;
    DatabaseManager mDatabase;
    private CropClassAdapter cropClassAdapter;
    private Toolbar toolbar;

    private AddInterestedCropFragment.OnInfoCropsInteractionListener mListener;
    private boolean fromProfile = false;
    private boolean fromOeProfile = false;
    private ArrayList<SelectedCropModel> modelArrayList = new ArrayList<>();

    public AddInterestedCropFragment() {
        // Required empty public constructor
    }

    public static AddInterestedCropFragment newInstance(boolean fromProfile, boolean fromOeProfile) {
        AddInterestedCropFragment fragment = new AddInterestedCropFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_FROM_PROFILE, fromProfile);
        args.putBoolean(ARG_FROM_OE_PROFILE, fromOeProfile);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fromProfile = getArguments().getBoolean(ARG_FROM_PROFILE);
            fromOeProfile = getArguments().getBoolean(ARG_FROM_OE_PROFILE);
        }

        if (fromProfile) {
            ((SelectCropActivity) getActivity()).setOnBackClickListener(new SelectCropActivity.OnBackClickListener() {
                @Override
                public boolean onBackClick() {
                    //if (true) {                    return false;                }

                    // some codes
                    if (AppController.currentScreen == 1) {
                        //Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> 1");
                        return false;
                    } else if (AppController.currentScreen == 2) {
                        //Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> 2");
                        lin_cropClass.setVisibility(View.VISIBLE);
                        AppController.currentScreen = 1;
                        toolbar.setTitle("        " + utils.getLocalizedString(R.string.crop_category_caps));
                        toolbar.setNavigationIcon(null);
                        //Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> back to 1");
                    } else if (AppController.currentScreen == 3) {
                        //Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> 3");
                    }

                    return true;
                }
            });
        } else if (fromOeProfile) {
            ((OeProfileActivity) getActivity()).setOnBackClickListener(new OeProfileActivity.OnBackClickListener() {
                @Override
                public boolean onBackClick() {
                    //if (true) {                    return false;                }

                    // some codes
                    if (AppController.currentScreen == 1) {
                        //Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> 1");
                        return false;
                    } else if (AppController.currentScreen == 2) {
                        //Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> 2");
                        lin_cropClass.setVisibility(View.VISIBLE);
                        AppController.currentScreen = 1;
                        toolbar.setTitle("        " + utils.getLocalizedString(R.string.crop_category_caps));
                        toolbar.setNavigationIcon(null);
                        //Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> back to 1");
                    } else if (AppController.currentScreen == 3) {
                        //Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> 3");
                    }

                    return true;
                }
            });
        } else {
            ((FarmerInfoActivity) getActivity()).setOnBackClickListener(new FarmerInfoActivity.OnBackClickListener() {
                @Override
                public boolean onBackClick() {
                    //if (true) {                    return false;                }

                    // some codes
                    if (AppController.currentScreen == 1) {
                        //Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> 1");
                        return false;
                    } else if (AppController.currentScreen == 2) {
                        //Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> 2");
                        lin_cropClass.setVisibility(View.VISIBLE);
                        AppController.currentScreen = 1;
                        toolbar.setTitle("        " + utils.getLocalizedString(R.string.crop_category_caps));
                        toolbar.setNavigationIcon(null);
                        //Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> back to 1");
                    } else if (AppController.currentScreen == 3) {
                        //Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> 3");
                    }

                    return true;
                }
            });
        }


    }

    public void callBack()
    {
        ((SelectCropActivity) getActivity()).onBackPressed();
    }


    @Override
    public void doBack() {
        //BackPressed in activity will call this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "view inflate");
        View view = inflater.inflate(R.layout.fragment_add_interested_crop, container, false);
        lin_cropClass = (LinearLayout) view.findViewById(R.id.lin_cropClass);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        if (fromProfile) {
            ((SelectCropActivity) getActivity()).setOnBackPressedListener(this);
        }

        getCrops();
        gridView = (GridView) view.findViewById(R.id.gridview);
        mProgressDialog = new ProgressDialog(getContext());
        mDatabase = new DatabaseManager(getContext());
        utils = AppController.getInstance().getStringUtils();
        mUserProfile = new UserProfile(getContext());

        toolbar = view.findViewById(R.id.toolbar_crop);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_with_tail_back_black_24dp);
        toolbar.setNavigationIcon(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //handle any click event
                if (AppController.currentScreen == 2) {
                    //Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> 2");
                    lin_cropClass.setVisibility(View.VISIBLE);
                    AppController.currentScreen = 1;
                    toolbar.setTitle("        " + utils.getLocalizedString(R.string.crop_category_caps));
                    toolbar.setNavigationIcon(null);
                    //Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> back to 1");
                }
            }
        });

        //toolbar.setTitle(getResources().getString(R.string.choose_crops));
        toolbar.setTitle("        " + utils.getLocalizedString(R.string.crop_category_caps));

        select_crop_class = (FrameLayout) view.findViewById(R.id.select_crop_class);
        close_selectcrop = (ImageView) view.findViewById(R.id.close_selectcrop);

        lin_cropClass = (LinearLayout) view.findViewById(R.id.lin_cropClass);

        select_crop_class.setOnClickListener(this);
        close_selectcrop.setOnClickListener(this);

        savebtn = (Button) view.findViewById(R.id.savebtn);
        cancelbtn = (Button) view.findViewById(R.id.cancelbtn);
        savebtn.setOnClickListener(this);
        cancelbtn.setOnClickListener(this);

        if (!fromProfile && !fromOeProfile) {
            savebtn.setBackgroundColor(getResources().getColor(R.color.loginBtn1));
            cancelbtn.setBackgroundColor(getResources().getColor(R.color.loginBtn1));
        }

        if (fromProfile)   cancelbtn.setVisibility(View.VISIBLE);

        rvCropClass = view.findViewById(R.id.rv_crop_Class);

        cropClassAdapter = new CropClassAdapter(getContext(), CropClass.getCropClassList(getContext()), cropClassListener);
        rvCropClass.setLayoutManager(new GridLayoutManager(getContext(), 3));
        rvCropClass.setAdapter(cropClassAdapter);
        AppController.currentScreen = 1;
        Log.e("setCurrentScreen", " ->>>>>>>> 1");
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Log.e("ADD CROPS VISIBLE", "------->>>>>>>>>>>>>>>");
            if(lin_cropClass != null)  lin_cropClass.setVisibility(View.VISIBLE);
        } else {
            // Do your Work
        }
    }

    private void getCrops() {

        CropDataService cropDataService = new CropDataService(getContext());
        cropDataService.getCrops(new CropDataService.OnCropDataRequest() {
            @Override
            public void onCropDataResults(List<Crop> crops) {
                AddInterestedCropFragment.this.mCrops = crops;
            }

            @Override
            public void onCropDataError() {
                AddInterestedCropFragment.this.mCrops = null;

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if (context instanceof AddInterestedCropFragment.OnInfoCropsInteractionListener) {
                Log.d("INTRESTED_CROP","onAttach 1");
                mListener = (AddInterestedCropFragment.OnInfoCropsInteractionListener) context;
                Log.d("INTRESTED_CROP","onAttach 2");
            } else {
                Log.d("INTRESTED_CROP","onAttach 3");
                throw new RuntimeException(context.toString()
                        + " must implement OnInfoLocationInteractionListener");

            }
        } catch (Exception e) {
            Log.d("INTRESTED_CROP","onAttach 4");
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("INTRESTED_CROP","onAttach 5");
        mListener = null;

    }

    /*@Override
    public boolean onBackClick() {
        if(AppController.currentScreen == 1) Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> 1");
        else if(AppController.currentScreen == 2) Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> 2");
        else if(AppController.currentScreen == 3) Log.e("INTEREED CROP FRAGMENT", " ->>>>>>>> 3");
        return true;
    }*/


    private CropClassAdapter.CropClassListener cropClassListener = new CropClassAdapter.CropClassListener() {
        @Override
        public void onClickItem(int position) {
            CropClass cropClass = cropClassAdapter.getItem(position);
            Log.d("CROP_IMAGES_TEST","------->>> 1" + cropClass.getCrop_class());
            getDataForCropClass(cropClass.getCrop_class());
        }
    };


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.select_crop_class:
                if (ImageAdapter.modelArrayList != null) {
                    if (!clicked) {
                        clicked = true;
                        lin_cropClass.setVisibility(View.GONE);
                        AppController.currentScreen = 2;
                        //Log.e("setCurrentScreen", " ->>>>>>>> 2");
                        toolbar.setTitle(utils.getLocalizedString(R.string.select_crops_caps));
                    } else {
                        clicked = false;
                        lin_cropClass.setVisibility(View.VISIBLE);
                        AppController.currentScreen = 1;
                        //Log.e("setCurrentScreen", " ->>>>>>>> 1");
                        toolbar.setTitle("        " + utils.getLocalizedString(R.string.crop_category_caps));
                    }
                }
                toolbar.setTitle(utils.getLocalizedString(R.string.select_crops_caps));
                break;

            case R.id.close_selectcrop:
                lin_cropClass.setVisibility(View.GONE);
                break;

            case R.id.cancelbtn:
                callBack();
                break;

            case R.id.savebtn:
                final List<String> mSelectedCrops = new ArrayList<>();
                Cursor c = mDatabase.getAllData();
                if (c.getCount() > 0) {
                    if (c.getCount() > 0) {
                        if (c.moveToFirst()) {
                            do {
//                                if (contains(mSelectedCrops, c.getString(c.getColumnIndex("crop_name")))) {
//                                    System.out.println("Exist(s)" + c.getString(c.getColumnIndex("crop_name")));
//                                } else {
                                mSelectedCrops.add(c.getString(c.getColumnIndex("crop_id")));
                                //}
                            } while (c.moveToNext());
                        }
                        c.close();
                        mDatabase.close();
                    }
                }
                if (!fromProfile || fromOeProfile) {
                    int MIN_SELECTION = 1;

                    if (mSelectedCrops.size() >= MIN_SELECTION) {
                        mListener.onInfoCropSuccess(new ArrayList<>(mSelectedCrops));

                        //returnResult(mSelectedCrops); // return the selected crop ids to registration activity @FarmerInfoActivity
                    } else {
                        Toast.makeText(getContext(), "select minimum " + MIN_SELECTION + " crops", Toast.LENGTH_SHORT).show();
                    }
                } else {

                    if (ImageAdapter.modelArrayList != null) {

                        final ProgressDialog progressDialog = new ProgressDialog(getContext());
                        progressDialog.setCancelable(false);
                        progressDialog.show();

                   /*     StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_FARMER_PROFILE,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        System.out.println("update_farmer_profile_response: " + response);
                                        progressDialog.dismiss();
                                        try {
                                            JSONArray responseArray = new JSONArray(response);
                                            JSONObject responseObject = responseArray.getJSONObject(0);
                                            int errorCode = responseObject.getInt("error_code");
                                            if (errorCode == 100) {

                                                PrefManager prefs = new PrefManager(getContext());
                                                prefs.setInterestedCropIds(TextUtils.join(",", mSelectedCrops));

                                                Toast.makeText(getActivity(), utils.getLocalizedString(R.string.successfully_updated), Toast.LENGTH_LONG).show();
                                                if(getActivity() != null) getActivity().finish();
                                                startActivity(new Intent(getActivity(), FarmerProfileActivity.class));
                                            } else {
                                                Toast.makeText(getActivity(), utils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        if (error instanceof TimeoutError) {
                                            showToast(getActivity(), utils.getLocalizedString(R.string.slow_internet_kindly_check_internet_to_proceed_seamlessly).toString());
                                        } else if (error instanceof NoConnectionError) {
                                            showToast(getActivity(), utils.getLocalizedString(R.string.no_internet_connection_try_later).toString());
                                        }
                                    }
                                }
                        ) {
                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<>();

                                params.put("api_token", mUserProfile.getApiToken());

                                //Mandatory
                                params.put("user_id", utils.getPref().getUserId());
                                params.put("password", utils.getPref().getUserPassword());

                                //Optional
                                JSONArray jsonArray = new JSONArray();
                                for (String item : mSelectedCrops) {
                                    jsonArray.put(item);
                                }
                                params.put("interested_crop_list", "" + jsonArray);

                                System.out.println("update_farmer_profile_request: " + params.toString());
                                return params;
                            }
                        };
                        AppController.getInstance().addToRequestQueue(stringRequest);
                        */
                        Profile params = new Profile();

                        //params.setFarmerId("UKYVLUT5");
                        //params.setFarmerContact("1478523691");

                        params.setFarmerId(utils.getPref().getUserId());
                        params.setFarmerContact(utils.getPref().getMobileNo());
                        params.setInterestedCropList(mSelectedCrops);
                        System.out.println("update_farmer_profile_request: " + params.toString());

                        API_Manager apiManager=new API_Manager();
                        UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);
                        Call<UserUpdate> call=retrofit_interface.updateprofile(mUserProfile.getApiToken(),params);
                        call.enqueue(new Callback<UserUpdate>() {
                            @Override
                            public void onResponse(Call<UserUpdate> call, Response<UserUpdate> response) {
                             try {
                                 Log.d("farmer_profile","crop response"+response.body());
                                 Log.d("farmer_profile","crop response code "+response.body().getStatus());
                                 if (response.body().getCode() == 200)
                                 {
                                     Log.d("farmer_profile","crop response code inside if"+response.body().getStatus());
                                     PrefManager prefs = new PrefManager(getContext());
                                     prefs.setInterestedCropIds(TextUtils.join(",", mSelectedCrops));

                                     Toast.makeText(getActivity(), utils.getLocalizedString(R.string.successfully_updated), Toast.LENGTH_LONG).show();
                                     Log.d("farmer_profile","crop updated code ");
                                     if(getActivity() != null) getActivity().finish();
                                     startActivity(new Intent(getActivity(), FarmerProfileActivity.class));
                                 } else {
                                     Toast.makeText(getActivity(), utils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                                 }
                             }catch (Exception e){e.printStackTrace();}
                            }
                            @Override
                            public void onFailure(Call<UserUpdate> call, Throwable t) {
                                if (t instanceof TimeoutError) {
                                    showToast(getActivity(), utils.getLocalizedString(R.string.slow_internet_kindly_check_internet_to_proceed_seamlessly).toString());
                                } else if (t instanceof NoConnectionError) {
                                    showToast(getActivity(), utils.getLocalizedString(R.string.no_internet_connection_try_later).toString());
                                    Log.d("farmer_profile","crop fail "+t.getMessage());
                                }
                            }
                        });
                    } else {
                        startActivity(new Intent(getActivity(), FarmerProfileActivity.class));
                    }
                }
                break;
        }
    }

    private void getDataForCropClass(final String cropClass) {
        mProgressDialog.show();
     /*   StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.CROP_BY_CLASS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        ImageList.clear();
                        lin_cropClass.setVisibility(View.GONE);
                        try {
                            JSONArray responseArray = new JSONArray(response);
                            JSONObject responseObject = responseArray.getJSONObject(0);
                            int errorCode = responseObject.getInt("error_code");
                            if (errorCode == 100) {
                                cropArray = responseObject.getJSONArray("data");
                                for (int i = 0; i < cropArray.length(); i++) {
                                    JSONObject cropObj = (JSONObject) cropArray.get(i);
                                    //Crop crop = new Crop(cropObj.getString("crop_name"));
                                    String crop = (String) cropObj.get("crop_name");
                                    if (contains(ImageList, crop)) {
                                    } else {
                                        ImageList.add(crop);
                                    }
                                }
                                // Instance of ImageAdapter Class
                                modelArrayList = getModel(false);
                                imageAdapter = new ImageAdapter(getContext(), modelArrayList, cropArray, mCrops);
                                gridView.setAdapter(imageAdapter);
                                AppController.currentScreen = 2;
                                //Log.e("setCurrentScreen", " ->>>>>>>> 2");
                                toolbar.setNavigationIcon(R.drawable.ic_arrow_with_tail_back_black_24dp);
                                toolbar.setTitle(utils.getLocalizedString(R.string.select_crops_caps));
                            } else {
                                Toast.makeText(getActivity(), utils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();
                        GeneralUtils.showVolleyError(getActivity(), error);
                    }
                }) {

            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();
                //Mandatory
                params.put("crop_class", cropClass);
                Log.d(TAG, params.toString());
                return params;
            }
        };
       AppController.getInstance().addToRequestQueue(stringRequest);}
*/
        API_Manager api_manager=new API_Manager();
        CropList retrofit_interface = api_manager.getClient5().create(CropList.class);
        Call<Croplist_byClass> call=retrofit_interface.getCropbyclass(mUserProfile.getApiToken(),cropClass);
        call.enqueue(new Callback<Croplist_byClass>() {
            @Override
            public void onResponse(Call<Croplist_byClass> call, retrofit2.Response<Croplist_byClass> response) {
                Log.d("CheckStatus","onResponse: croplist by class "+response);
                mProgressDialog.dismiss();
                ImageList.clear();
                lin_cropClass.setVisibility(View.GONE);
                try {
                    if (response.body().getCode() == 200) {
                        Log.d("CheckStatus","onResponse: croplist by class "+response.body().getCode());
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            cropArray = response.body().getData();

                            String crop=response.body().getData().get(i).getCrop_name();
                            if (contains(ImageList, crop)) {
                            } else {
                                ImageList.add(crop);
                            }
                        }
                        // Instance of ImageAdapter Class
                        modelArrayList = getModel(false);
                        imageAdapter = new ImageAdapter(getContext(), modelArrayList, cropArray, mCrops);//
                        gridView.setAdapter(imageAdapter);
                        AppController.currentScreen = 2;
                        //Log.e("setCurrentScreen", " ->>>>>>>> 2");
                        toolbar.setNavigationIcon(R.drawable.ic_arrow_with_tail_back_black_24dp);
                        toolbar.setTitle(utils.getLocalizedString(R.string.select_crops_caps));
                    } else {
                        Toast.makeText(getActivity(), utils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<Croplist_byClass> call, Throwable t) {
                mProgressDialog.dismiss();
                Utils.showToast(getActivity(),t.getMessage());
                Log.d("CheckStatus","error "+t.getMessage());
                //GeneralUtils.showVolleyError(getActivity(), error);
            }
        });
    }
    private ArrayList<SelectedCropModel> getModel(boolean isSelect) {
        ArrayList<SelectedCropModel> list = new ArrayList<>();
        System.out.println("Exist(s) 1");

        //get value from localdb and mark selected true

        for (int i = 0; i < ImageList.size(); i++) {
            System.out.println("Exist(s) 2");
            SelectedCropModel model = new SelectedCropModel();
            System.out.println("Exist(s) 3");
            model.setSelected(isSelect);
            System.out.println("Exist(s) 4");
            Cursor c = mDatabase.getAllData();
            System.out.println("Exist(s) 5");
            try {
                System.out.println("Exist(s) 6");
                if (c.getCount() > 0) {
                    System.out.println("Exist(s) 8");
                    if (c.moveToFirst()) {
                        System.out.println("Exist(s) 9");
                        do {
                            System.out.println("Exist(s) 10");
                            if (c.getString(c.getColumnIndex("crop_name")).equalsIgnoreCase(ImageList.get(i))) {
                                System.out.println("Exist(s) 11");
                                System.out.println("Exist(s)" + c.getString(c.getColumnIndex("crop_name")));
                                model.setSelected(true);
                            }
                        } while (c.moveToNext());
                    }
                    c.close();
                }
            } catch (Exception e) {
                System.out.println("Exist(s) 12");
                e.printStackTrace();
            } finally {
                if (c != null) {
                    System.out.println("Exist(s) 13");
                    try {
                        System.out.println("Exist(s) 14");
                        c.close();
                    } catch (Exception ignore) {
                        System.out.println("Exist(s) 15");
                    }
                }
                mDatabase.close();
                System.out.println("Exist(s) 16");
            }
            System.out.println("Exist(s) 17");

            model.setCrop(ImageList.get(i));
            System.out.println("Exist(s) 18");

            list.add(model);
            System.out.println("Exist(s) 19");

        }
        System.out.println("Exist(s) 20");
        return list;
    }

    boolean contains(List<String> list, String name) {
        for (String item : list) {
            if (item.equals(name)) {
                return true;
            }
        }
        return false;
    }

    public interface OnInfoCropsInteractionListener {

        void onInfoCropSuccess(List<String> crops);
    }
}
