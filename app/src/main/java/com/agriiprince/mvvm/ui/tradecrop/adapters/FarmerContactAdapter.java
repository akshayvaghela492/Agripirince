package com.agriiprince.mvvm.ui.tradecrop.adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.databinding.ItemFigFarmerListBinding;
import com.agriiprince.mvvm.model.FarmerContact;
import com.agriiprince.mvvm.ui.tradecrop.activities.FIGViewFarmerActivity;
import com.agriiprince.mvvm.ui.tradecrop.lists.PopUpMenuFarmerList;

import java.util.ArrayList;

public class FarmerContactAdapter extends ArrayAdapter<FarmerContact> {

    private ItemFigFarmerListBinding binding;

    private Context mContext;

    public FarmerContactAdapter(Context context, ArrayList<FarmerContact> users) {
        super(context, 0, users);
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        FarmerContact user = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_fig_farmer_list, parent, false);
        }
        binding= DataBindingUtil.bind(convertView);
        convertView.setTag(binding);

        // Lookup view for data population
        //TextView tvName = (TextView) convertView.findViewById(R.id.farmer_name);
        //TextView tvNum = (TextView) convertView.findViewById(R.id.farmer_number);
        // Populate the data into the template view using the data object
        //tvName.setText(user.getName());
        //tvNum.setText(user.getContact());

        binding.farmerName.setText(user.getName());
        binding.farmerNumber.setText(user.getContact());

        final String userName = user.getName();
        final String userNumber = user.getContact();

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, FIGViewFarmerActivity.class);
                intent.putExtra("name", userName);
                intent.putExtra("number", userNumber);
                context.startActivity(intent);
            }
        });

        //final Button mMenuButton;
        //mMenuButton = convertView.findViewById(R.id.menu_button_farmerlist);

        final PopupMenu popup = new PopupMenu(parent.getContext(), binding.menuButtonFarmerlist);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.tradecrop_card_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopUpMenuFarmerList(position));

        binding.menuButtonFarmerlist.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Click event works.", Toast.LENGTH_SHORT).show();
                popup.show();
            }
        });

        // Return the completed view to render on screen
        return binding.getRoot();
    }
}