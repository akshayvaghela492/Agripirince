package com.agriiprince.mvvm.ui.registration.infostepsfragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.chip.Chip;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.mvvm.retrofit.model.login.Profile;
import com.agriiprince.mvvm.ui.registration.FarmerProfileActivity;
import com.agriiprince.adapter.CustomStringArrayAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.dataservice.MandiDataService;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.MandiModel;
import com.agriiprince.model.NearestMandiModel;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.profile.UserUpdate;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.utils.StringUtils;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static com.agriiprince.utils.Utils.showToast;

public class InfoMandiFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private static final String ARG_NEAREST_MANDI = "nearest_mandi";
    private static final String ARG_NEAREST_MANDIS_ID = "nearest_mandi_id";
    private static final String ARG_ALLMANDILIST = "mandislist";
    private static final String ARG_LOCATION = "userlocation";

    private OnInfoMandiInteractionListener mListener;
    //private GoogleMap.OnMarkerClickListener mClickListener;

    private SupportMapFragment mMapFragment;
    private GoogleMap mGoogleMap;
    private double mUserLat;
    private double mUserLng;

    private List<MandiModel> mMandiList;

    private List<NearestMandiModel> mMandiListbyDistance;

    private List<String> listOfNearestMandiNames;
    private String[] mlistOfNearestMandiNamesArray;

    private AppCompatAutoCompleteTextView mAcNearestMandiName;
    private String mNearestMandiId;

    private AppCompatImageView mBtnBack;

    private AppCompatButton mBtnNext;

    private Chip mChip;

    private double mCurLat, mCurLng, mNewLat, mNewLng;
    private String mCurTitle = "";
    private String mNewTitle = "";

    private ScrollView mScrollView;

    private boolean fromProfile;

    private ProgressDialog dialog;

    StringUtils stringUtils;
    private UserProfile mUserProfile;

    private boolean mandiSetupFinished = false;

    String violet = "#BA55D3";
    String indigo = "#4B0082";
    String blue = "#87cefa";
    String green = "#00FF00";
    String yellow = "#FFFF00";
    String orange = "#F47C01";
    String red = "#FF0000";
    String lightred = "#F08080";
    //String grey = "#808080";   //looks better
    String colors[] = {violet, indigo, blue, green, yellow, orange, red};

    public static InfoMandiFragment newInstance(String nearestMandiId, double lat, double lng, boolean fromprofile) {
        InfoMandiFragment fragment = new InfoMandiFragment();
        Bundle args = new Bundle();
        args.putString(ARG_NEAREST_MANDIS_ID, nearestMandiId);
        //args.putSerializable(ARG_ALLMANDILIST, mandiList);
        args.putDouble("userLat", lat);
        args.putDouble("userLng", lng);
        args.putBoolean("fromProfile", fromprofile);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fromProfile = getArguments().getBoolean("fromProfile");
            if (fromProfile) {
                mNearestMandiId = getArguments().getString(ARG_NEAREST_MANDIS_ID);
            }
        }

        mMandiListbyDistance = new ArrayList<>();
        listOfNearestMandiNames = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info_mandi, container, false);

        //mMandiList = (List<MandiModel>) getArguments().getSerializable(ARG_ALLMANDILIST);
        mUserLat = getArguments().getDouble("userLat");
        mUserLng = getArguments().getDouble("userLng");

        if (mUserLat == 0) mUserLat = 28.7041;   //Delhi
        if (mUserLng == 0) mUserLng = 77.1025;   //Delhi

        listOfNearestMandiNames = new ArrayList<>();

        stringUtils = AppController.getInstance().getStringUtils();

        mUserProfile = new UserProfile(getContext());

        ((TextView) view.findViewById(R.id.title)).setText(stringUtils.getLocalizedString(R.string.select_nearest_mandi_to_you));

        if (fromProfile) {
            ((AppCompatButton) view.findViewById(R.id.info_next)).setText(stringUtils.getLocalizedString(R.string.submit));
            ((AppCompatButton) view.findViewById(R.id.info_next)).setBackgroundColor(getResources().getColor(R.color.colorPrimaryFarmer));

        } else
            ((AppCompatButton) view.findViewById(R.id.info_next)).setText(stringUtils.getLocalizedString(R.string.next));

        mBtnBack = view.findViewById(R.id.info_back);

        mBtnNext = view.findViewById(R.id.info_next);

        mAcNearestMandiName = view.findViewById(R.id.info_mandi_choose_nearest);
        //if(fromProfile)
        //{
        //   mAcNearestMandiName.setText(mNearestMandi);
        //   Log.e("INSIDE FROM PROFILE =>", "setText nearest mandi");
        //}

        mAcNearestMandiName.setHint(stringUtils.getLocalizedString(R.string.choose_nearest_mandi));

        mChip = view.findViewById(R.id.info_mandi_chip_nearest);

        mScrollView = view.findViewById(R.id.info_mandi_scroll_view);
        dialog = new ProgressDialog(getActivity());

        getMandies();
        //printMandies();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_choose_mandi);
        mMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        LatLng LatLngUser = new LatLng(mUserLat, mUserLng);

        //mClickListener = (GoogleMap.OnMarkerClickListener) this;
        googleMap.setOnMarkerClickListener(this);

        mGoogleMap = googleMap;

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLngUser, 5));

        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);

        //USER LOCATION (don't remove)
        //mGoogleMap.addMarker(new MarkerOptions().position(LatLngUser).title("MY LOCATION").icon(getMarkerIcon(blue)));

        setupMandi();
    }

    public double FindDistance(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
    }

    public BitmapDescriptor getMarkerIcon(String color) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAcNearestMandiName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (hasFocus) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //scrollViewToTop(v);    // causing crash

                        }
                    }, 500);
                }
            }
        });

        mAcNearestMandiName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (v.hasFocus()) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //scrollViewToTop(v);    // causing crash

                        }
                    }, 500);
                }
            }
        });

        mAcNearestMandiName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = (String) parent.getItemAtPosition(position);
                mChip.setText(name);
                mNearestMandiId = getNearearestIdbyNameAndKM(name);
                if (fromProfile) {
                    mChip.setChipBackgroundColorResource(R.color.colorPrimaryFarmer);
                    Log.d("INSIDE FROM PROFILE =>", "setChipBackgroundColorResource");
                }
                mChip.setVisibility(View.VISIBLE);

                matchNamesDistanceAndKM(name);

                mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mCurLat, mCurLng)).title(mCurTitle).icon(getMarkerIcon(lightred)));
                mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mNewLat, mNewLng)).title(mNewTitle).icon(getMarkerIcon(green)));

                CameraUpdate cameraUpdate = CameraUpdateFactory
                        .newLatLngZoom(new LatLng(mNewLat, mNewLng), 8);
                mGoogleMap.animateCamera(cameraUpdate);

                mCurLat = mNewLat;
                mCurLng = mNewLng;
                mCurTitle = mNewTitle;
            }
        });

        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fromProfile) {
                    updateProfile();
                    Log.d("INSIDE FROM PROFILE =>", "updateProfile()");
                } else {
                    if (TextUtils.isEmpty(mChip.getText().toString())) {
                        Toast.makeText(getContext(), stringUtils.getLocalizedString(R.string.select_nearest_mandi), Toast.LENGTH_SHORT).show();
                    } else {
                        mListener.onInfoMandiSuccess(mNearestMandiId);
                    }
                }
            }
        });
    }

    private void updateProfile() {
     /*   StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_FARMER_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("update_farmer_profile_response: " + response);
                        try {
                            JSONArray responseArray = new JSONArray(response);
                            JSONObject responseObject = responseArray.getJSONObject(0);
                            int errorCode = responseObject.getInt("error_code");
                            if (errorCode == 100) {
                                Toast.makeText(getActivity(), stringUtils.getLocalizedString(R.string.successfully_updated), Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getActivity(), FarmerProfileActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            } else {
                                Toast.makeText(getActivity(), stringUtils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (getActivity() == null) return;
                        if (error instanceof TimeoutError) {
                            showToast(getActivity(), stringUtils.getLocalizedString(R.string.slow_internet_kindly_check_internet_to_proceed_seamlessly).toString());
                        } else if (error instanceof NoConnectionError) {
                            showToast(getActivity(), stringUtils.getLocalizedString(R.string.no_internet_connection_try_later).toString());
                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("api_token", mUserProfile.getApiToken());

                //Mandatory
                params.put("user_id", stringUtils.getPref().getUserId());
                params.put("password", stringUtils.getPref().getUserPassword());

                if (mChip != null && !TextUtils.isEmpty(mChip.getText().toString())) {
                    //String[] extract = mChip.getText().toString().split("\\[");
                    //params.put("nearest_mandi_name", extract[0].trim());
                    params.put("nearest_mandi_name", mNearestMandiId);
                }

                System.out.println("update_farmer_profile_request: " + params.toString());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
*/
        Profile params = new Profile();
        //params.setFarmerId("UKYVLUT5");
        //params.setFarmerContact("1478523691");

        params.setFarmerId(stringUtils.getPref().getUserId());
        params.setFarmerContact(stringUtils.getPref().getMobileNo());

        if (mChip != null && !TextUtils.isEmpty(mChip.getText().toString())) {
            //String[] extract = mChip.getText().toString().split("\\[");
            //params.put("nearest_mandi_name", extract[0].trim());
            params.setNearestMandiName(mNearestMandiId);
        }
        System.out.println("update_farmer_profile_request: " + params.toString());

        API_Manager apiManager=new API_Manager();
        UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);
        Call<UserUpdate> call=retrofit_interface.updateprofile(mUserProfile.getApiToken(),params);
        call.enqueue(new Callback<UserUpdate>() {
            @Override
            public void onResponse(Call<UserUpdate> call, retrofit2.Response<UserUpdate> response) {
                try {
                    Log.d("farmer_profile","mandi frag response"+response.body());
                    if (response.body().getCode() == 200) {
                        Toast.makeText(getActivity(), stringUtils.getLocalizedString(R.string.successfully_updated), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getActivity(), FarmerProfileActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getActivity(), stringUtils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<UserUpdate> call, Throwable t) {
                Log.d("farmer_profile","mandi frag fail"+t.getMessage());
                if (getActivity() == null) return;
                if (t instanceof TimeoutError) {
                    showToast(getActivity(), stringUtils.getLocalizedString(R.string.slow_internet_kindly_check_internet_to_proceed_seamlessly).toString());
                } else if (t instanceof NoConnectionError) {
                    showToast(getActivity(), stringUtils.getLocalizedString(R.string.no_internet_connection_try_later).toString());
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if (context instanceof OnInfoMandiInteractionListener) {
                mListener = (OnInfoMandiInteractionListener) context;
            } else {
                throw new RuntimeException(context.toString()
                        + " must implement OnInfoLocationInteractionListener");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        //mAcNearestMandiName.setOnFocusChangeListener(null);
        //mAcNearestMandiName.setOnClickListener(null);
        //mAcNearestMandiName.setOnItemClickListener(null);
        //mAcNearestMandiName = null;
        //mGoogleMap.setOnMarkerClickListener(null);
    }

    private void getMandies() {
        MandiDataService mandiDataService = new MandiDataService(getContext());
        mandiDataService.getMandis(new MandiDataService.OnMandiResponse() {
            @Override
            public void onMandiSuccess(List<MandiModel> mandies) {
                mMandiList = mandies;
                setupMandi();
            }

            @Override
            public void onMandiFailure() {
                Toast.makeText(getActivity(), "Can't  load Mandies", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupMandi() {
        if (!mandiSetupFinished && mGoogleMap != null && mMandiList != null) {
            mandiSetupFinished = true;
            //Find Mandi distances from User
            for (MandiModel model : mMandiList) {
                double distance = FindDistance(Double.parseDouble(model.mandi_lat), Double.parseDouble(model.mandi_lng), mUserLat, mUserLng);
                NearestMandiModel mandiToAdd = new NearestMandiModel(model.mandi_id, model.mandi_lat, model.mandi_lng, model.mandi_name_en, distance);
                mMandiListbyDistance.add(mandiToAdd);
            }

            Collections.sort(mMandiListbyDistance);

            for (NearestMandiModel model : mMandiListbyDistance) {
                mGoogleMap.addMarker(
                        new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(model.mandi_lat), Double.parseDouble(model.mandi_lng)))
                                .title(model.mandi_name_en + " [" + (int) (model.mandi_distance_from_user * 111) + " km]").icon(getMarkerIcon(lightred))
                );
                listOfNearestMandiNames.add(model.mandi_name_en + " [" + (int) (model.mandi_distance_from_user * 111) + " km]");
            }

            mlistOfNearestMandiNamesArray = (String[]) listOfNearestMandiNames.toArray(new String[0]);

            CustomStringArrayAdapter nearestMandiAdapter = new CustomStringArrayAdapter(getContext(),
                    android.R.layout.simple_dropdown_item_1line, listOfNearestMandiNames);
            mAcNearestMandiName.setAdapter(nearestMandiAdapter);

            CameraUpdate cameraUpdate = null;

            if (fromProfile) {
                matchNamesForId();
                matchNamesKMTitle();

                mChip.setText(mNewTitle);
                mNearestMandiId = getNearearestIdbyNameAndKM(mNewTitle);
                mAcNearestMandiName.setText(mNewTitle);
                mChip.setChipBackgroundColorResource(R.color.colorPrimaryFarmer);
                Log.d("INSIDE FROM PROFILE =>", "setChipBackgroundColorResource");
            } else {
                mChip.setText(listOfNearestMandiNames.get(0));
                mNearestMandiId = getNearearestIdbyNameAndKM(listOfNearestMandiNames.get(0));
                mAcNearestMandiName.setText(listOfNearestMandiNames.get(0));
            }
            mChip.setVisibility(View.VISIBLE);

            //nearest mandi
            mCurLat = Double.parseDouble(mMandiListbyDistance.get(0).mandi_lat);
            mCurLng = Double.parseDouble(mMandiListbyDistance.get(0).mandi_lng);
            mCurTitle = mMandiListbyDistance.get(0).mandi_name_en + " [" + (int) (mMandiListbyDistance.get(0).mandi_distance_from_user * 111) + " km]";

            if (fromProfile) {
                mCurLat = mNewLat;
                mCurLng = mNewLng;
                mCurTitle = mNewTitle;
            }

            mGoogleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(mCurLat, mCurLng)).title(mCurTitle).icon(getMarkerIcon(green)));
            cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(mCurLat, mCurLng), 5);
            mGoogleMap.animateCamera(cameraUpdate);
            cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(mCurLat, mCurLng), 8);
            mGoogleMap.animateCamera(cameraUpdate);
        }
    }

    private void printMandies() {
        for (MandiModel model : mMandiList)
            Log.d("MANDIES: ", model.mandi_id + " " + model.mandi_name_en + " " + model.mandi_lat + " " + model.mandi_lng);
    }

    // not working
    public void scrollViewToTop(View view) {
        int[] xy = new int[2];
        view.getLocationOnScreen(xy);
        int initialY = xy[1];

        int[] xyBack = new int[2];
        mBtnBack.getLocationOnScreen(xyBack);
        int finalY = xyBack[1] + mBtnBack.getBottom();

        int scrollY = initialY - finalY - dpToPx(12);

        mScrollView.smoothScrollTo(0, scrollY);
    }

    public int dpToPx(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    public void matchNamesKMTitle() {
        for (String tempStr : mlistOfNearestMandiNamesArray) {
            String[] extractname = tempStr.split(" \\[");
            if (extractname[0].equals(mNewTitle)) mNewTitle = tempStr;
        }
    }

    public void matchNamesForId() {
        for (NearestMandiModel model : mMandiListbyDistance) {
            if ((model.mandi_id).equals(mNearestMandiId)) {
                mNewLat = Double.parseDouble(model.mandi_lat);
                mNewLng = Double.parseDouble(model.mandi_lng);
                mNewTitle = model.mandi_name_en + " [" + (int) (model.mandi_distance_from_user * 111) + " km]";
            }
        }
    }

    public void matchNamesDistanceAndKM(String tempStr) {
        for (NearestMandiModel model : mMandiListbyDistance) {
            if ((model.mandi_name_en + " [" + (int) (model.mandi_distance_from_user * 111) + " km]").equals(tempStr)) {
                mNewLat = Double.parseDouble(model.mandi_lat);
                mNewLng = Double.parseDouble(model.mandi_lng);
                mNewTitle = model.mandi_name_en + " [" + (int) (model.mandi_distance_from_user * 111) + " km]";
            }
        }
    }

    public String getNearearestIdbyNameAndKM(String tempStr) {
        String Id = "";
        for (NearestMandiModel model : mMandiListbyDistance) {
            if ((model.mandi_name_en + " [" + (int) (model.mandi_distance_from_user * 111) + " km]").equals(tempStr)) {
                Id = model.mandi_id;
            }
        }
        return Id;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        String tempTitle = marker.getTitle();
        if (tempTitle.equals("MY LOCATION")) {
            return true;
        }

        matchNamesDistanceAndKM(tempTitle);

        if (TextUtils.isEmpty(mChip.getText().toString())) {
            Log.d("INSIDE", "-> TextUtils.isEmpty(mChip.getText().toString())");
        } else if ((mNewLat == mCurLat) && (mNewLng == mCurLng)) {
            Log.d("INSIDE", "-> (mNewLat == mCurLat) && (mNewLng == mCurLng)");
            mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mNewLat, mNewLng)).title(mNewTitle).icon(getMarkerIcon(lightred)));
            mAcNearestMandiName.getText().clear();
            mChip.setText("");
            mNearestMandiId = "";
            return true;
        }

        Log.d("OUTSIDE", "-> (mNewLat == mCurLat) && (mNewLng == mCurLng)");
        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mCurLat, mCurLng)).title(mCurTitle).icon(getMarkerIcon(lightred)));
        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mNewLat, mNewLng)).title(mNewTitle).icon(getMarkerIcon(green)));

        matchNamesKMTitle();

        mAcNearestMandiName.setText(mNewTitle);
        mChip.setText(mNewTitle);
        mNearestMandiId = getNearearestIdbyNameAndKM(mNewTitle);

        CameraUpdate cameraUpdate = CameraUpdateFactory
                .newLatLngZoom(new LatLng(mNewLat, mNewLng), 8);
        mGoogleMap.animateCamera(cameraUpdate);

        mCurLat = mNewLat;
        mCurLng = mNewLng;
        mCurTitle = mNewTitle;

        return true;
    }

    public interface OnInfoMandiInteractionListener {
        void onInfoMandiSuccess(String nearestMandi);
    }
}
