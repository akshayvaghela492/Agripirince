package com.agriiprince.mvvm.ui.tradecrop.adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.databinding.ItemFigFarmerListBinding;
import com.agriiprince.databinding.ItemFpoFiglistBinding;
import com.agriiprince.mvvm.model.FigContact;
import com.agriiprince.mvvm.ui.tradecrop.activities.FPOFigDetailsFarmerListsActivity;

import java.util.ArrayList;

public class FigContactAdapter extends ArrayAdapter<FigContact> {
    private ItemFpoFiglistBinding binding;

    private Context mContext;

    public FigContactAdapter(Context context, ArrayList<FigContact> users) {
        super(context, 0, users);
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        FigContact user = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_fpo_figlist, parent, false);
        }
        binding= DataBindingUtil.bind(convertView);
        convertView.setTag(binding);

        // Lookup view for data population
        //TextView tvName = (TextView) convertView.findViewById(R.id.fig_name_and_num_of_farmers);
        //TextView tvNum = (TextView) convertView.findViewById(R.id.fig_number);
        // Populate the data into the template view using the data object
        //tvName.setText(user.getName() + " (" + user.getNumber_of_farmers() + ")");
        //tvNum.setText(user.getContact());

        binding.figNameAndNumOfFarmers.setText(user.getName() + " (" + user.getNumber_of_farmers() + ")");
        binding.figNumber.setText(user.getContact());

        final String userName = user.getName();
        final String userNumber = user.getContact();

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, FPOFigDetailsFarmerListsActivity.class);
                intent.putExtra("name", userName);
                intent.putExtra("number", userNumber);
                context.startActivity(intent);
            }
        });

        // Return the completed view to render on screen
        return binding.getRoot();
    }
}