package com.agriiprince.mvvm.ui.registration.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agriiprince.R;

import java.util.ArrayList;
import java.util.List;

public class InterestedMandisAdapter extends RecyclerView.Adapter<InterestedMandisAdapter.ViewHolder> {

    private static final String TAG = InterestedMandisAdapter.class.getSimpleName();

    private List<String> mInterestedMandiList = new ArrayList<>();

    public InterestedMandisAdapter() {

    }

    public void setData(List<String> list) {
        mInterestedMandiList.clear();
        mInterestedMandiList.addAll(list);
        notifyDataSetChanged();
    }

    public void addData(String mandi) {
        mInterestedMandiList.add(mandi);
        notifyDataSetChanged();
    }

    public List<String> getMandiList() {
        return mInterestedMandiList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_interested_mandis, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String item = mInterestedMandiList.get(position);
        Log.d(TAG, "onBindViewHolder: " + item);
        holder.textViewMandiName.setText(item);
    }

    @Override
    public int getItemCount() {
        return mInterestedMandiList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewMandiName;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewMandiName = itemView.findViewById(R.id.tv_mandi_name);

        }
    }
}
