package com.agriiprince.mvvm.ui.registration;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.agriiprince.db.DatabaseManager;
import com.agriiprince.R;
import com.agriiprince.mvvm.ui.registration.adapters.InterestedListAdapter;
import com.agriiprince.dataservice.CropDataService;
import com.agriiprince.model.SelectedCropModel;
import com.agriiprince.mvvm.model.crop.Crop;

import java.util.ArrayList;
import java.util.List;

public class ShowInterestedCropFragment extends Fragment {
    GridView gridView;
    ArrayList<String> mSelectedCrops = new ArrayList<>();
    ArrayList<SelectedCropModel> mSelectedCropVariety = new ArrayList<>();
    ArrayList<SelectedCropModel> modelArrayList = new ArrayList<>();
    InterestedListAdapter mAdapter;
    List<Crop> mCrops;
    DatabaseManager mDatabase;

    public ShowInterestedCropFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_show_interested_crop, container, false);

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDatabase = new DatabaseManager(getContext());
        gridView = (GridView) view.findViewById(R.id.grid_view);
        getCrops();
    }

    private ArrayList<SelectedCropModel> getModel(boolean isSelect) {
        ArrayList<SelectedCropModel> list = new ArrayList<>();
        for (int i = 0; i < mSelectedCrops.size(); i++) {
            SelectedCropModel model = new SelectedCropModel();
            model.setSelected(true);
            model.setCrop(mSelectedCrops.get(i));
            list.add(model);
        }
        return list;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Log.e("SHOW CROPS VISIBLE", "------->>>>>>>>>>>>>>>");
            mSelectedCrops.clear();
            getCrops();
        }
    }

    private void getCrops() {
        CropDataService cropDataService = new CropDataService(getContext());
        cropDataService.getCrops(new CropDataService.OnCropDataRequest() {
            @Override
            public void onCropDataResults(List<Crop> crops) {
                mCrops = crops;
                try {
                    Cursor c = mDatabase.getAllData();
                    try {
                        if (c.getCount() > 0) {
                            if (c.moveToFirst()) {
                                do {
                                    if (contains(mSelectedCrops, c.getString(c.getColumnIndex("crop_name")))) {
                                        System.out.println("Exist(s)" + c.getString(c.getColumnIndex("crop_name")));
                                    } else {
                                        mSelectedCrops.add(c.getString(c.getColumnIndex("crop_name")));
                                    }
                                } while (c.moveToNext());
                            }
                            c.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        if (c != null) {
                            try {
                                c.close();
                            } catch (Exception ignore) {
                                ;
                            }
                        }
                        mDatabase.close();
                    }
                    modelArrayList = getModel(false);
                    if (modelArrayList.size() > 0) {
                        mAdapter = new InterestedListAdapter(getContext(), modelArrayList, mCrops);
                        gridView.setAdapter(mAdapter);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCropDataError() {

            }
        });
    }

    boolean contains(List<String> list, String name) {
        for (String item : list) {
            if (item.equals(name)) {
                Log.d("INTRESTED_CROP","contain list");
                return true;
            }
        }
        Log.d("INTRESTED_CROP","does not contain list");
        return false;
    }
}
