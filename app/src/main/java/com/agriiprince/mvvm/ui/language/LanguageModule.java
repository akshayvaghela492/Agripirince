package com.agriiprince.mvvm.ui.language;

import android.app.Application;
import android.arch.lifecycle.ViewModel;

import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.data.repository.LanguageRepository;
import com.agriiprince.mvvm.data.repository.LanguageRepositoryUseCase;
import com.agriiprince.mvvm.di.ViewModelKey;
import com.agriiprince.mvvm.viewmodel.LanguageViewModel;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public abstract class LanguageModule {

    @Binds
    @IntoMap
    @Singleton
    @ViewModelKey(LanguageViewModel.class)
    public abstract ViewModel bindsLangaugeViewModel(LanguageViewModel languageViewModel);

    @Provides
    @Singleton
    public static LanguageRepositoryUseCase providesLanguageRepositoryUseCase(Application application, PrefManager prefManager) {
        return new LanguageRepository(application, prefManager);
    }
}
