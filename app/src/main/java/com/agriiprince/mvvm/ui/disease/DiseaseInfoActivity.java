package com.agriiprince.mvvm.ui.disease;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.db.DatabaseManager;
import com.agriiprince.R;
import com.agriiprince.activities.TutorialActivity;
import com.agriiprince.adapter.CropDiseaseInfoAdapter;
import com.agriiprince.adapter.ViewPagerAdapter;
import com.agriiprince.adapter.farmer.DiseaseListAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.ActivityCropDiseasesDiseaseinfoBinding;
import com.agriiprince.dataservice.DataResponse;
import com.agriiprince.dataservice.DiseaseInfoDataService;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.CropDiseaseInfoModel;
import com.agriiprince.models.DiseaseInfoModel;
import com.agriiprince.models.DiseaseListModel;
import com.agriiprince.utils.StringUtils;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.synnapps.carouselview.ImageListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DiseaseInfoActivity extends AppCompatActivity {

    private final String TAG = DiseaseInfoActivity.class.getSimpleName();

    public static final String KEY_DISEASE_ID = "disease_id";
    public static final String KEY_DISEASE_IDS = "disease_ids";
    public static final String KEY_DISEASE_POS = "disease_poss";

    List<String> imageUrls;
    private CropDiseaseInfoAdapter adapter;
    ArrayList<CropDiseaseInfoModel> entries = new ArrayList<>();

    private String disease_id;
    ArrayList<String> DiseaseIds = new ArrayList<String>();
    private int disease_pos;
    static ImageView imgview_pl;

    DiseaseListAdapter dladapter;

    private UserProfile userProfile;
    private PrefManager mPrefManger;

    private ViewPagerAdapter viewPagerAdapter;

    private ActivityCropDiseasesDiseaseinfoBinding binding;

    private StringUtils stringUtils;
    private TextToSpeech textToSpeech;
    ArrayList<String> disease_playlist = new ArrayList<String>();

    private int player_visibility_flag = 0;
    private int playlist_visibility_flag = 1;
    private int camview_visibility_flag = 0;
    private int position_playing = 0;
    private int position_image = 0;
    private int paused_flag = 0;

    File destination;
    Uri selectedImage;
    public static String selectedPath1 = "NONE";
    private static final int PICK_Camera_IMAGE = 2;
    private static final int SELECT_FILE1 = 1;
    public static Bitmap bmpScale;
    public static String imagePath;
    ImageView imageview_gallery;
    ConstraintLayout leftrightLayout;
    ArrayList<Bitmap> bitmapArray = new ArrayList<Bitmap>();
    TextView imgview_numbering;

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            //imageview_gallery.setImageBitmap(imageBitmap);
            BitmapDrawable ob = new BitmapDrawable(getResources(), imageBitmap);
            imageview_gallery.setBackground(ob);
            imageview_gallery.setVisibility(View.VISIBLE);
            bitmapArray.add(imageBitmap);
            if(bitmapArray.size()>1)            leftrightLayout.setVisibility(View.VISIBLE);
            position_image = bitmapArray.size();
            String numbering = (position_image) + "/" + bitmapArray.size();
            imgview_numbering.setText(numbering);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_crop_diseases_diseaseinfo);

        mPrefManger = new PrefManager(this);
        userProfile = new UserProfile(this);
        stringUtils = AppController.getInstance().getStringUtils();
        imageUrls = new ArrayList<>();

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        final ConstraintLayout audioLayout = (ConstraintLayout) this.findViewById(R.id.audio_player_tts);
        final LinearLayout camLayout = (LinearLayout) this.findViewById(R.id.ll_camera_gallery);
        leftrightLayout = (ConstraintLayout) this.findViewById(R.id.ll_constraint_left_right);


        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);

        final TextView tv_1 =  findViewById(R.id.tv_info);
        final TextView tv_2 =  findViewById(R.id.tv_symp);
        final TextView tv_3 =  findViewById(R.id.tv_mgmt);
        final TextView tv_4 =  findViewById(R.id.tv_comments);

        imageview_gallery = findViewById(R.id.imageview_gallery);
        imgview_numbering =  findViewById(R.id.iv_numbering);

        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {

                    int ttsLang;

                    if(mPrefManger.getLocale().equals("hi"))
                        ttsLang = textToSpeech.setLanguage(new Locale("hi","IN"));
                    else
                        ttsLang = textToSpeech.setLanguage(Locale.US);

                    if (ttsLang == TextToSpeech.LANG_MISSING_DATA
                            || ttsLang == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "The Language is not supported!");
                    } else {
                        Log.i("TTS", "Language Supported.");
                    }
                    Log.i("TTS", "Initialization success.");
                } else {
                    Toast.makeText(getApplicationContext(), "TTS Initialization failed!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ImageView imgview_help =  findViewById(R.id.iv_help);

        imgview_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DiseaseInfoActivity.this, TutorialActivity.class);
                intent.putExtra("code", "DISED");
                startActivity(intent);
            }
        });

        ImageView iv_go_left =  findViewById(R.id.iv_go_left);

        iv_go_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position_image>0) position_image--;
                else position_image = bitmapArray.size()-1;

                Bitmap b1 = bitmapArray.get(position_image);
                BitmapDrawable ob = new BitmapDrawable(getResources(), b1);
                imageview_gallery.setBackground(ob);

                String numbering = (position_image+1) + "/" + bitmapArray.size();
                imgview_numbering.setText(numbering);
            }
        });

        ImageView iv_go_right =  findViewById(R.id.iv_go_right);

        iv_go_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position_image<bitmapArray.size()-1) position_image++;
                else position_image = 0;

                Bitmap b1 = bitmapArray.get(position_image);
                BitmapDrawable ob = new BitmapDrawable(getResources(), b1);
                imageview_gallery.setBackground(ob);

                String numbering = (position_image+1) + "/" + bitmapArray.size();
                imgview_numbering.setText(numbering);
            }
        });

        ImageView imgview_camera =  findViewById(R.id.iv_photo);

        imgview_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                audioLayout.setVisibility(View.INVISIBLE);
                player_visibility_flag = 0;
                if (textToSpeech != null) {
                    textToSpeech.stop();
                }

                if(camview_visibility_flag==0)
                {
                    camLayout.setVisibility(View.VISIBLE);
                    camview_visibility_flag=1;
                }
                else
                {
                    camLayout.setVisibility(View.INVISIBLE);
                    camview_visibility_flag=0;
                }
            }
        });

        ImageView iv_close = findViewById(R.id.iv_close_cam);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camLayout.setVisibility(View.INVISIBLE);
                camview_visibility_flag=0;
            }
        });

        TextView tv_cam = findViewById(R.id.btn_cam);

        tv_cam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
                String name = dateFormat.format(new Date());
                destination = new File(Environment
                        .getExternalStorageDirectory(), name + ".jpg");

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(destination));
                startActivityForResult(intent, PICK_Camera_IMAGE);*/
                dispatchTakePictureIntent();
            }
        });

        // ......................gallery_function..........//
        /*mgallery = (Button) findViewById(R.id.button2);
        mgallery.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                openGallery(SELECT_FILE1);
            }
        });*/

        ImageView imgview_audio =  findViewById(R.id.iv_audio);

        imgview_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                camLayout.setVisibility(View.INVISIBLE);
                camview_visibility_flag=0;

                if(player_visibility_flag == 0)
                {

                    tv_1.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryFarmer)));

                    tv_2.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                    tv_3.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                    tv_4.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));

                    audioLayout.setVisibility(View.VISIBLE);
                    player_visibility_flag = 1;

                    position_playing = 0;
                    String to_speak = disease_playlist.get(position_playing);
                    int speechStatus = textToSpeech.speak(to_speak, TextToSpeech.QUEUE_FLUSH, null);

                    if (speechStatus == TextToSpeech.ERROR) {
                        Log.e("TTS", "Error in converting Text to Speech!");
                    }
                }
                else
                {
                    audioLayout.setVisibility(View.INVISIBLE);
                    player_visibility_flag = 0;
                    if (textToSpeech != null) {
                        textToSpeech.stop();
                    }
                }
            }
        });


        ImageView imgview_close =  findViewById(R.id.iv_close);
        imgview_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    audioLayout.setVisibility(View.INVISIBLE);
                    player_visibility_flag = 0;
                if (textToSpeech != null) {
                    textToSpeech.stop();
                }
            }
        });

        ImageView imgview_next =  findViewById(R.id.iv_next);

        imgview_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_1.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                tv_2.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                tv_3.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                tv_4.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));

                if(position_playing<disease_playlist.size()-1) position_playing++;
                else position_playing = 0;

                if(position_playing==0)
                    tv_1.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryFarmer)));
                else if(position_playing==1)
                    tv_2.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryFarmer)));
                else if(position_playing==2)
                    tv_3.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryFarmer)));
                else if(position_playing==3)
                    tv_4.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryFarmer)));

                    String to_speak = disease_playlist.get(position_playing);
                    int speechStatus = textToSpeech.speak(to_speak, TextToSpeech.QUEUE_FLUSH, null);

                    if (speechStatus == TextToSpeech.ERROR) {
                        Log.e("TTS", "Error in converting Text to Speech!");
                    }
            }
        });

        ImageView imgview_prev =  findViewById(R.id.iv_previous);

        imgview_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_1.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                tv_2.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                tv_3.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                tv_4.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));

                if(position_playing!=0) position_playing--;
                else position_playing = disease_playlist.size()-1;

                if(position_playing==0)
                    tv_1.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryFarmer)));
                else if(position_playing==1)
                    tv_2.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryFarmer)));
                else if(position_playing==2)
                    tv_3.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryFarmer)));
                else if(position_playing==3)
                    tv_4.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryFarmer)));

                String to_speak = disease_playlist.get(position_playing);
                int speechStatus = textToSpeech.speak(to_speak, TextToSpeech.QUEUE_FLUSH, null);

                if (speechStatus == TextToSpeech.ERROR) {
                    Log.e("TTS", "Error in converting Text to Speech!");
                }
            }
        });

        tv_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_1.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryFarmer)));

                tv_2.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                tv_3.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                tv_4.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));

                position_playing = 0;
                String to_speak = disease_playlist.get(position_playing);
                int speechStatus = textToSpeech.speak(to_speak, TextToSpeech.QUEUE_FLUSH, null);

                if (speechStatus == TextToSpeech.ERROR) {
                    Log.e("TTS", "Error in converting Text to Speech!");
                }
            }
        });

        tv_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_2.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryFarmer)));

                tv_1.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                tv_3.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                tv_4.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));

                position_playing = 1;
                String to_speak = disease_playlist.get(position_playing);
                int speechStatus = textToSpeech.speak(to_speak, TextToSpeech.QUEUE_FLUSH, null);

                if (speechStatus == TextToSpeech.ERROR) {
                    Log.e("TTS", "Error in converting Text to Speech!");
                }
            }
        });

        tv_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_3.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryFarmer)));

                tv_1.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                tv_2.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                tv_4.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));

                position_playing = 2;
                String to_speak = disease_playlist.get(position_playing);
                int speechStatus = textToSpeech.speak(to_speak, TextToSpeech.QUEUE_FLUSH, null);

                if (speechStatus == TextToSpeech.ERROR) {
                    Log.e("TTS", "Error in converting Text to Speech!");
                }
            }
        });

        tv_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_4.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryFarmer)));

                tv_1.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                tv_2.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                tv_3.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));

                position_playing = 3;
                String to_speak = disease_playlist.get(position_playing);
                int speechStatus = textToSpeech.speak(to_speak, TextToSpeech.QUEUE_FLUSH, null);

                if (speechStatus == TextToSpeech.ERROR) {
                    Log.e("TTS", "Error in converting Text to Speech!");
                }
            }
        });

        final ImageView imgview_pause =  findViewById(R.id.iv_pause);

        imgview_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (paused_flag == 0){
                    if (textToSpeech != null)  textToSpeech.stop();

                    imgview_pause.setImageResource(R.drawable.ic_play_circle_outline_black_24dp);
                    paused_flag = 1;
                }
                else
                {
                    String to_speak = disease_playlist.get(position_playing);
                    int speechStatus = textToSpeech.speak(to_speak, TextToSpeech.QUEUE_FLUSH, null);

                    if (speechStatus == TextToSpeech.ERROR) {
                        Log.e("TTS", "Error in converting Text to Speech!");
                    }

                    imgview_pause.setImageResource(R.drawable.ic_pause_circle_outline_black_24dp);
                    paused_flag = 0;
                }

            }
        });

        imgview_pl =  findViewById(R.id.iv_playlist);
        final LinearLayout plist = (LinearLayout) this.findViewById(R.id.layout_playlist);

        imgview_pl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(playlist_visibility_flag == 0)
                {
                    plist.setVisibility(View.VISIBLE);
                    playlist_visibility_flag = 1;
                    //imgview_pl.setColorFilter(ContextCompat.getColor(DiseaseInfoActivity.this, R.color.colorPrimaryFarmer), android.graphics.PorterDuff.Mode.SRC_IN);
                    ImageViewCompat.setImageTintList(imgview_pl, ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryFarmer)));
                }
                else
                {
                    plist.setVisibility(View.GONE);
                    playlist_visibility_flag = 0;
                    ImageViewCompat.setImageTintList(imgview_pl, ColorStateList.valueOf(getResources().getColor(R.color.primaryTextColor)));
                }

            }
        });


/*
        binding.collapsingToolbar.setTitle(stringUtils.getLocalizedString(R.string.crop_disease, true));
        binding.collapsingToolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
*/

        binding.carouselView.setImageListener(imageListener);

        adapter = new CropDiseaseInfoAdapter(entries, DiseaseInfoActivity.this);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        binding.viewPager.setAdapter(viewPagerAdapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);

        Intent oldIntent = getIntent();
        if (oldIntent.hasExtra(KEY_DISEASE_ID)) {
            disease_id = oldIntent.getStringExtra(KEY_DISEASE_ID);

            disease_pos = Integer.parseInt(oldIntent.getStringExtra(KEY_DISEASE_POS));

            DiseaseIds = oldIntent.getStringArrayListExtra(KEY_DISEASE_IDS);

            DatabaseManager databaseManager = AppController.getInstance().getDataBase();
            DiseaseInfoDataService diseaseInfoDataService = new DiseaseInfoDataService(new PrefManager(this), databaseManager);
            diseaseInfoDataService.getCropDiseaseInfo(disease_id, dataResponse);

        } else {
            finish();
        }
    }

    public void openGallery(int SELECT_FILE1) {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(intent, "Select file to upload "),
                SELECT_FILE1);

    }

    /*protected void onActivityResult(int requestCode, int resultCode,
                                    Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        Uri selectedImageUri = null;
        String filePath = null;
        switch (requestCode) {
            case SELECT_FILE1:
                if (resultCode == Activity.RESULT_OK) {
                    selectedImage = imageReturnedIntent.getData();

                    if (requestCode == SELECT_FILE1) {
                        selectedPath1 = getPath(selectedImage);
                        // mimagepath.setText(selectedPath1);
                        // Toast.makeText(Camera.this, "" + selectedPath1 + "",
                        // 500).show();

                        if (selectedPath1 != null) {

                            BitmapFactory.Options options = new BitmapFactory.Options();

                            options.inJustDecodeBounds = true;
                            // image path `String` where your image is located
                            BitmapFactory.decodeFile(selectedPath1, options);


                            // Log.d("setpath ", "setpath " + selectedPath1);
                            ;

                        }
                    }

                    break;
                    case PICK_Camera_IMAGE:
                        if (resultCode == RESULT_OK) {

                            try {
                                in = new FileInputStream(destination);
                            } catch (FileNotFoundException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inSampleSize = 4;
                            imagePath = destination.getAbsolutePath();

                            // Toast.makeText(Camera.this, "" + imagePath +
                            // "",Toast.LENGTH_LONG).show();

                            break;

                        }

                }
        }
    }*/

    public void goRight(View v)
    {
        //Toast.makeText(this, "Clicked on Button Right", Toast.LENGTH_LONG).show();
        String tempDiseaseId;
        int new_disease_pos;
        if(disease_pos<DiseaseIds.size()-1)        new_disease_pos = disease_pos+1;
        else   new_disease_pos = 0;
        tempDiseaseId = DiseaseIds.get(new_disease_pos);

        Intent intent = new Intent(DiseaseInfoActivity.this, DiseaseInfoActivity.class);
        intent.putExtra(DiseaseInfoActivity.KEY_DISEASE_ID, tempDiseaseId);
        intent.putExtra(DiseaseInfoActivity.KEY_DISEASE_POS, Integer.toString(new_disease_pos));
        intent.putStringArrayListExtra(DiseaseInfoActivity.KEY_DISEASE_IDS, DiseaseIds);
        startActivity(intent);
        finish();
    }

    public void goLeft(View v)
    {
        //Toast.makeText(this, "Clicked on Button Left", Toast.LENGTH_LONG).show();
        String tempDiseaseId;
        int new_disease_pos;
        if(disease_pos>0)        new_disease_pos = disease_pos-1;
        else                     new_disease_pos = DiseaseIds.size()-1;
        tempDiseaseId = DiseaseIds.get(new_disease_pos);

        Intent intent = new Intent(DiseaseInfoActivity.this, DiseaseInfoActivity.class);
        intent.putExtra(DiseaseInfoActivity.KEY_DISEASE_ID, tempDiseaseId);
        intent.putExtra(DiseaseInfoActivity.KEY_DISEASE_POS, Integer.toString(new_disease_pos));
        intent.putStringArrayListExtra(DiseaseInfoActivity.KEY_DISEASE_IDS, DiseaseIds);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            try {
                Glide.with(DiseaseInfoActivity.this)
                        .setDefaultRequestOptions(new RequestOptions()
                                .diskCacheStrategy(DiskCacheStrategy.ALL))
                        .load(imageUrls.get(position))
                        .into(imageView);
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    };

    private DataResponse<DiseaseInfoModel> dataResponse = new DataResponse<DiseaseInfoModel>() {
        @Override
        public void onSuccessResponse(DiseaseInfoModel response) {
            addFragments(response);
        }

        @Override
        public void onParseError() {

        }

        @Override
        public void onErrorResponse(VolleyError error) {

        }
    };

    private boolean isNotEmpty(String s) {
        return !TextUtils.isEmpty(s);
    }

    private void addFragment(String title, String body) {
        viewPagerAdapter.addFragment(GenericDiseaseFragment.newInstance(body), title);
    }


    private void setupImages(String imagesNames) {
        if ( ! TextUtils.isEmpty(imagesNames)) {
            imageUrls = DiseaseListModel.getImageUrls(imagesNames);
            binding.carouselView.setPageCount(imageUrls.size());
        }

        if (imageUrls.size() <= 0) {
            binding.collapsingToolbar.setTitle(stringUtils.getLocalizedString(R.string.crop_disease, true));
            binding.collapsingToolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
            binding.appBarLayout.setExpanded(false, true);
            binding.carouselView.setVisibility(View.GONE);
        }
    }

    private void addFragments(DiseaseInfoModel model) {
        if (model == null) return;

        setupImages(model.getImages());

        viewPagerAdapter.addFragment(DiseaseInformationFragment.newInstance(model),
                stringUtils.getLocalizedString(R.string.disease_information).toString());
        disease_playlist.add(stringUtils.getLocalizedString(R.string.disease_information).toString() + "." + stringUtils.getLocalizedString(R.string.crop_name).toString() + "." + model.getCropName()
                + "." + stringUtils.getLocalizedString(R.string.disease).toString() + "." + model.getDiseaseName()  + "." + stringUtils.getLocalizedString(R.string.scientific_name).toString() + "." + model.getScientificName()
                + "." + stringUtils.getLocalizedString(R.string.category).toString() + "." + model.getCategory()  + "." + stringUtils.getLocalizedString(R.string.cause).toString() + "." + model.getCause());

        if (isNotEmpty(model.getSymptom())) {
            Log.d(TAG, "addFragments: " + model.getSymptomBi());
            addFragment(stringUtils.getLocalizedString(DiseaseInfoModel.getSymptomTitle()).toString(),
                    stringUtils.getBilingualStringForApi(model.getSymptom(), model.getSymptomBi()));
            disease_playlist.add(stringUtils.getLocalizedString(DiseaseInfoModel.getSymptomTitle()).toString() + "." + stringUtils.getBilingualStringForApi(model.getSymptom(), model.getSymptomBi()));
        }

        if (isNotEmpty(model.getManagement()))
            addFragment(stringUtils.getLocalizedString(DiseaseInfoModel.getManagementTitle()).toString(),
                    stringUtils.getBilingualStringForApi(model.getManagement(), model.getManagementBi()));
        disease_playlist.add(stringUtils.getLocalizedString(DiseaseInfoModel.getManagementTitle()).toString() + "." + stringUtils.getBilingualStringForApi(model.getManagement(), model.getManagementBi()));

        addFragment(stringUtils.getLocalizedString(R.string.pesticides).toString(),
                "Coming Soon");

        addFragment(stringUtils.getLocalizedString(R.string.help).toString(),
                "Coming Soon");

        if (isNotEmpty(model.getComments()))
            addFragment(stringUtils.getLocalizedString(DiseaseInfoModel.getCommentsTitle()).toString(),
                    stringUtils.getBilingualStringForApi(model.getComments(), model.getCommentsBi()));
        disease_playlist.add(stringUtils.getLocalizedString(DiseaseInfoModel.getCommentsTitle()).toString() + "." + stringUtils.getBilingualStringForApi(model.getComments(), model.getCommentsBi()));

        viewPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
    }

    public void start_camera(View view) {
    }

    public void show_gallery(View view) {
    }
}
