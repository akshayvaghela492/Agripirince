package com.agriiprince.mvvm.ui.registration.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.utils.StringUtils;

import java.util.List;

/**
 * Created by dtrah on 11/24/2018..
 */

class VarietListAdapter extends RecyclerView.Adapter<VarietListAdapter.MyViewHolder> {

    private List<String> List;
    private StringUtils utils;
    private Context context;
    private List<Crop> mCrops;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.variety_name);
        }
    }


    public VarietListAdapter(List<String> moviesList, Context context, List<Crop> mCrops) {
        this.List = moviesList;
        this.context = context;
        this.mCrops = mCrops;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.variety_list_row, parent, false);
        utils = AppController.getInstance().getStringUtils();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String variety = List.get(position);
        holder.title.setText(utils.getBilingualStringForApi(variety, Crop.getSecondNameByCropName(mCrops, variety)));
    }

    @Override
    public int getItemCount() {
        return List.size();
    }
}
