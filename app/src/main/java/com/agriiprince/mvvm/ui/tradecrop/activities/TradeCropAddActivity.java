package com.agriiprince.mvvm.ui.tradecrop.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.databinding.ActivityTradeCropAddBinding;
import com.agriiprince.dataservice.CropDataService;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.SaveCrop;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.SaveMedia;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.service.CropInterface;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.mvvm.model.crop.Grade;
import com.agriiprince.mvvm.model.crop.Variety;
import com.agriiprince.mvvm.ui.tradecrop.adapters.ImageBitmapAdapter;
import com.agriiprince.mvvm.util.Logs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.agriiprince.mvvm.util.DisplayUtils.dpToPx;

public class TradeCropAddActivity extends AppCompatActivity {

    private ActivityTradeCropAddBinding binding;
    private UserProfile mUserProfile;

    private List<Crop> mCrops = new ArrayList<>();
    private List<Variety> mVariety = new ArrayList<>();
    private List<Grade> mGrade = new ArrayList<>();

    private List<String> mCropList = new ArrayList<>();
    private List<String> mVarietyList = new ArrayList<>();
    private List<String> mGradeList = new ArrayList<>();
    private List<String> mAreaList = new ArrayList<>();
    private List<String> mSeedTypeList = new ArrayList<>();
    private List<String> mHarvestQuantityList = new ArrayList<>();
    private List<String> mPeerTimeList = new ArrayList<>();

    private Spinner mCropSpinner;
    private Spinner mVarietySpinner;
    private Spinner mGradeSpinner;
    private Spinner mAreaSpinner;
    private TextView mSowingDateTv;
    private EditText mSowingArea;

    private List<EditText> mPesticideList = new ArrayList<>();
    private List<EditText> mFertilizerList = new ArrayList<>();

    private Spinner mExpectedGradeSpinner1;
    private Spinner mExpectedGradeSpinner2;
    private Spinner mExpectedGradeSpinner3;
    private TextView mHarvestingDateTv;
    private Spinner mNumTimesSpinner;
    private Spinner mHarvesstingQuantitySpinner;
    private Spinner mPerTimeSpinner;
    private Spinner mPerTimeSpinner2;
    private RadioGroup mRadioGroupHarvestFrequency;
    private EditText mPerNumberET;

    private RecyclerView mUploadImagesRV;
    private List<Bitmap> mBitmaps = new ArrayList<>();
    private List<File> mImageFiles = new ArrayList<>();
    private TextView mPlaceholderTv;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trade_crop_add);
        mUserProfile = new UserProfile(this);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //final Spinner cropSpinner = findViewById(R.id.spinner_crop_select);
        //final Spinner varietySpinner = findViewById(R.id.spinner_variety_select);
        //final Spinner gradeSpinner = findViewById(R.id.spinner_grade_select);
        //final Spinner areaSpinner = findViewById(R.id.spinner_area);

        CropDataService cropDataService = new CropDataService(this);
        cropDataService.getCrops(new CropDataService.OnCropDataRequest() {
            @Override
            public void onCropDataResults(List<Crop> crops) {
                TradeCropAddActivity.this.mCrops = crops;
                for (Crop crop: mCrops) {
                    mCropList.add(crop.cropName);
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(TradeCropAddActivity.this, android.R.layout.simple_spinner_item, mCropList);
                //cropSpinner.setAdapter(adapter);
                binding.expandableSowinginfo.spinnerCropSelect.setAdapter(adapter);
            }

            @Override
            public void onCropDataError() {
                TradeCropAddActivity.this.mCrops = null;
            }
        });

        binding.expandableSowinginfo.spinnerCropSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String selectedCrop =  binding.expandableSowinginfo.spinnerCropSelect.getSelectedItem().toString();
                for (int i = 0; i < mCropList.size(); i++) {
                    if(selectedCrop.equals(mCropList.get(i))) {
                        Crop crop = mCrops.get(i);
                        mVariety = crop.getVarieties();

                        mVarietyList.clear();
                        for (Variety variety: mVariety) {
                            mVarietyList.add(variety.varietyName);
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(TradeCropAddActivity.this, android.R.layout.simple_spinner_item, mVarietyList);
                        //varietySpinner.setAdapter(adapter);
                        binding.expandableSowinginfo.spinnerVarietySelect.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        binding.expandableSowinginfo.spinnerVarietySelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String selectedVariety = binding.expandableSowinginfo.spinnerVarietySelect.getSelectedItem().toString();
                for (int i = 0; i < mVarietyList.size(); i++) {
                    if(selectedVariety.equals(mVarietyList.get(i))) {
                        Variety variety = mVariety.get(i);
                        mGrade = variety.getGrades();

                        mGradeList.clear();
                        //mGradeList = variety.getGradeNames();
                        //if(mGradeList.size()==0) { mGradeList.add("NA"); mGradeList.add("NA"); }
                        for (Grade grade: mGrade) {
                            mGradeList.add(grade.gradeName);
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(TradeCropAddActivity.this, android.R.layout.simple_spinner_item, mGradeList);
                        //gradeSpinner.setAdapter(adapter);
                        binding.expandableSowinginfo.spinnerGradeSelect.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        mAreaList.add("Acre");
        mAreaList.add("Hectare");
        mAreaList.add("Bigha");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(TradeCropAddActivity.this, android.R.layout.simple_spinner_item, mAreaList);
        //areaSpinner.setAdapter(adapter);
        binding.expandableSowinginfo.spinnerArea.setAdapter(adapter);

        //final TextView sowingDateTv = findViewById(R.id.sowing_date_tv);
        final Calendar myCalendar = Calendar.getInstance();
        final String myFormat = "dd - MMM - yyyy"; //In which you need put here
        final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                //sowingDateTv.setText(sdf.format(myCalendar.getTime()));
                binding.expandableSowinginfo.sowingDateTv.setText(sdf.format(myCalendar.getTime()));
            }

        };

        binding.expandableSowinginfo.sowingDateTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(TradeCropAddActivity.this, date1, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        //final EditText sowingArea = findViewById(R.id.area_size_sowing);


        //final TextView harvestingDateTv = findViewById(R.id.harvesting_start_date);
        final DatePickerDialog.OnDateSetListener date2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                //harvestingDateTv.setText(sdf.format(myCalendar.getTime()));
                binding.expandableHarvestinginfo.harvestingStartDate.setText(sdf.format(myCalendar.getTime()));
            }

        };

        binding.expandableHarvestinginfo.harvestingStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(TradeCropAddActivity.this, date2, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final Spinner seedTypeSpinner = findViewById(R.id.spinner_seed_type);

        mSeedTypeList.add("Fertilizer");
        mSeedTypeList.add("Pesticide");

        adapter = new ArrayAdapter<String>(TradeCropAddActivity.this, android.R.layout.simple_spinner_item, mSeedTypeList);
        //seedTypeSpinner.setAdapter(adapter);
        binding.expandableGrowinginfo.spinnerSeedType.setAdapter(adapter);


        //final Spinner expectedGradeSpinner1 = findViewById(R.id.expected_grade_spinner1);
        //final Spinner expectedGradeSpinner2 = findViewById(R.id.expected_grade_spinner2);
        //final Spinner expectedGradeSpinner3 = findViewById(R.id.expected_grade_spinner3);

        ArrayList<String> numbers = new ArrayList<String>();
        for (int i = 1; i <= 100; i++) {
            numbers.add(Integer.toString(i)+"%");
        }

        adapter = new ArrayAdapter<String>(TradeCropAddActivity.this, android.R.layout.simple_spinner_item, numbers);
        /*expectedGradeSpinner1.setAdapter(adapter);
        expectedGradeSpinner2.setAdapter(adapter);
        expectedGradeSpinner3.setAdapter(adapter);
        */
        binding.expandableHarvestinginfo.expectedGradeSpinner1.setAdapter(adapter);
        binding.expandableHarvestinginfo.expectedGradeSpinner2.setAdapter(adapter);
        binding.expandableHarvestinginfo.expectedGradeSpinner3.setAdapter(adapter);

        //final Spinner numTimesSpinner = findViewById(R.id.spinner_no_of_times);

        ArrayList<String> numb = new ArrayList<String>();
        for (int i = 1; i <= 10; i++) {
            numb.add(Integer.toString(i));
        }

        adapter = new ArrayAdapter<String>(TradeCropAddActivity.this, android.R.layout.simple_spinner_item, numb);
        //numTimesSpinner.setAdapter(adapter);
        binding.expandableHarvestinginfo.spinnerNoOfTimes.setAdapter(adapter);


        //final Spinner harvesstingQuantitySpinner = findViewById(R.id.spinner_harvesting_quantity);

        mHarvestQuantityList.add("Kg");
        mHarvestQuantityList.add("Quintal");
        mHarvestQuantityList.add("Tonne");

        adapter = new ArrayAdapter<String>(TradeCropAddActivity.this, android.R.layout.simple_spinner_item, mHarvestQuantityList);
        //harvesstingQuantitySpinner.setAdapter(adapter);
        binding.expandableHarvestinginfo.spinnerHarvestingQuantity.setAdapter(adapter);


        //final Spinner perTimeSpinner = findViewById(R.id.spinner_per_days);
        //final Spinner perTimeSpinner2 = findViewById(R.id.spinner_per_days2);

        mPeerTimeList.add("Days");
        mPeerTimeList.add("Weeks");
        mPeerTimeList.add("Months");
        mPeerTimeList.add("Years");

        adapter = new ArrayAdapter<String>(TradeCropAddActivity.this, android.R.layout.simple_spinner_item, mPeerTimeList);
        //perTimeSpinner.setAdapter(adapter);
        binding.expandableHarvestinginfo.spinnerPerDays.setAdapter(adapter);

        adapter = new ArrayAdapter<String>(TradeCropAddActivity.this, android.R.layout.simple_spinner_item, mPeerTimeList);
        //perTimeSpinner2.setAdapter(adapter);
        binding.expandableHarvestinginfo.spinnerPerDays2.setAdapter(adapter);

     /*   RadioGroup radioGroupHarvestFrequency =  findViewById(R.id.radioGroupHarvestFrequency);
         mRadioGroupHarvestFrequency = radioGroupHarvestFrequency;
        EditText perNumberET =  findViewById(R.id.edittext_per_num);
        mPerNumberET = perNumberET;
        RecyclerView uploadImagesRV = findViewById(R.id.upload_images_list);
        mUploadImagesRV = uploadImagesRV;
        TextView placeholderTv = findViewById(R.id.placeholder_textview);
        mPlaceholderTv = placeholderTv;
    */
        mRadioGroupHarvestFrequency=binding.expandableHarvestinginfo.radioGroupHarvestFrequency;
        mPerNumberET=binding.expandableHarvestinginfo.edittextPerNum;
        mUploadImagesRV=binding.expandableImgvid.uploadImagesList;
        mPlaceholderTv=binding.expandableImgvid.placeholderTextview;


        /*ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 1900; i <= thisYear; i++) {
            years.add(Integer.toString(i));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, years);
        Spinner cropSpinner = findViewById(R.id.spinner_crop_select);
        cropSpinner.setAdapter(adapter);*/

        binding.expandableSwitch1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.expandableSowinginfo.getRoot().getVisibility() == View.GONE)
                    binding.expandableSowinginfo.getRoot().setVisibility(View.VISIBLE);
                else
                    binding.expandableSowinginfo.getRoot().setVisibility(View.GONE);
            }
        });

        binding.expandableSwitch2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.expandableGrowinginfo.getRoot().getVisibility() == View.GONE) {
                    binding.expandableGrowinginfo.getRoot().setVisibility(View.VISIBLE);
                    binding.addDynamicLayoutsHere2.setVisibility(View.VISIBLE);
                } else {
                    binding.expandableGrowinginfo.getRoot().setVisibility(View.GONE);
                    binding.addDynamicLayoutsHere2.setVisibility(View.GONE);

                }
            }
        });

        binding.expandableSwitch3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.expandableHarvestinginfo.getRoot().getVisibility() == View.GONE)
                    binding.expandableHarvestinginfo.getRoot().setVisibility(View.VISIBLE);
                else
                    binding.expandableHarvestinginfo.getRoot().setVisibility(View.GONE);
            }
        });

        binding.expandableSwitch4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.expandableImgvid.getRoot().getVisibility() == View.GONE)
                    binding.expandableImgvid.getRoot().setVisibility(View.VISIBLE);
                else
                    binding.expandableImgvid.getRoot().setVisibility(View.GONE);
            }
        });




        mCropSpinner =  binding.expandableSowinginfo.spinnerCropSelect;
        mVarietySpinner = binding.expandableSowinginfo.spinnerVarietySelect;
        mGradeSpinner = binding.expandableSowinginfo.spinnerGradeSelect;
        mAreaSpinner = binding.expandableSowinginfo.spinnerArea;
        mSowingDateTv = binding.expandableSowinginfo.sowingDateTv;
        mSowingArea = binding.expandableSowinginfo.areaSizeSowing;

        mExpectedGradeSpinner1 = binding.expandableHarvestinginfo.expectedGradeSpinner1;
        mExpectedGradeSpinner2 = binding.expandableHarvestinginfo.expectedGradeSpinner2;
        mExpectedGradeSpinner3 = binding.expandableHarvestinginfo.expectedGradeSpinner3;

        mHarvestingDateTv = binding.expandableHarvestinginfo.harvestingStartDate;
        mNumTimesSpinner = binding.expandableHarvestinginfo.spinnerNoOfTimes;
        mHarvesstingQuantitySpinner = binding.expandableHarvestinginfo.spinnerHarvestingQuantity;
        mPerTimeSpinner = binding.expandableHarvestinginfo.spinnerPerDays;
        mPerTimeSpinner2 = binding.expandableHarvestinginfo.spinnerPerDays2;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addPesticideOrFertilizer(View view) {
        //Spinner seedTypeSpinner = findViewById(R.id.spinner_seed_type);
        String selectedSeedType = binding.expandableGrowinginfo.spinnerSeedType.getSelectedItem().toString();
        if(selectedSeedType.equals("Pesticide")){
            addLayout("pesticide");
        }
        else {
            addLayout("fertilizer");
        }

        /*
        FrameLayout container = new FrameLayout(this);
        container = (FrameLayout) findViewById(R.id.flContainer);
        View inflatedLayout = new LinearLayout(this);
        inflatedLayout = getLayoutInflater().inflate(R.layout.fragment_farmer_trade_growing_subinfo_fertilizer, , false);
        container.addView(inflatedLayout);
        View linearLayout = (LinearLayout)findViewById(R.id.add_dynamic_layouts_here2);
        ((LinearLayout) linearLayout).addView(container);
        */


        /*

        View linearLayoutToAdd = getLayoutInflater().inflate(R.id.dynamic_layout_fertilizer, null, false);
        linearLayoutToAdd.setId(6);
        linearLayoutToAdd.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        ((LinearLayout) linearLayout).addView(linearLayoutToAdd);*/

    }

    @SuppressLint("ResourceType")
    public void addLayout(String text) {
        //final View linearLayout = (LinearLayout)findViewById(R.id.add_dynamic_layouts_here2);
        final TextView valueTV = new TextView(this);
        valueTV.setText(text.toUpperCase());
        valueTV.setId(5);
        valueTV.setLayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        //((LinearLayout) linearLayout).addView(valueTV);

        binding.addDynamicLayoutsHere2.addView(valueTV);

        final LinearLayout valueLL = new LinearLayout(this);
        valueLL.setId(6);
        valueLL.setLayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        valueLL.setOrientation(LinearLayout.HORIZONTAL);
        //((LinearLayout) linearLayout).addView(valueLL);
        binding.addDynamicLayoutsHere2.addView(valueLL);

        final EditText valueET = new EditText(this);
        valueET.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
        valueET.setHint("enter " + text + " name");
        valueET.setId(7);
        valueET.setLayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        ((LinearLayout) valueLL).addView(valueET);


        final ImageView valueIV = new ImageView(this);
        valueIV.setBackground(this.getResources().getDrawable(R.drawable.cb_payu_close_grey));
        valueIV.setId(8);
        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        lp1.setMargins(dpToPx(this,4), dpToPx(this,12), 0, 0);
        valueIV.setLayoutParams(lp1);
        valueIV.getLayoutParams().height = dpToPx(this,20);
        valueIV.getLayoutParams().width = dpToPx(this,20);
        ((LinearLayout) valueLL).addView(valueIV);

        if(text.equals("pesticide")){
            mPesticideList.add(valueET);
        }
        else {
            mFertilizerList.add(valueET);
        }

        valueIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(valueTV.getText().equals("PESTICIDE")){
                    mPesticideList.remove(valueET);
                }
                else {
                    mFertilizerList.remove(valueET);
                }
               // ((LinearLayout) linearLayout).removeView(valueTV);
                //((LinearLayout) linearLayout).removeView(valueLL);

                binding.addDynamicLayoutsHere2.removeView(valueTV);
                binding.addDynamicLayoutsHere2.removeView(valueLL);
            }
        });
    }

    public static final int PICK_IMAGE = 1;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == PICK_IMAGE) {
            //TODO: action
            if (resultCode == Activity.RESULT_OK) {
                if (data == null) {
                    //Display an error
                    return;
                }
                try {

                    Uri selectedImageURI = data.getData();
                    File imageFile = new File(getRealPathFromURI(selectedImageURI));
                    mImageFiles.add(imageFile);

                    InputStream inputStream = this.getContentResolver().openInputStream(data.getData());
                    //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...
                    Bitmap bitmap1 = BitmapFactory.decodeStream(inputStream);
                    //image.setImageBitmap(bitmap);

                    mBitmaps.add(bitmap1);

                    mUploadImagesRV.setAdapter(new ImageBitmapAdapter(mBitmaps));
                    mUploadImagesRV.setVisibility(View.VISIBLE);

                    mPlaceholderTv.setVisibility(View.INVISIBLE);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void openGallery(View view) {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

        startActivityForResult(chooserIntent, PICK_IMAGE);
    }



    private API_Manager apiManager=new API_Manager();
    private CropInterface retrofit_interface = apiManager.getClient2().create(CropInterface.class);

    public void SaveCropFarmer() {

        StringBuilder pesticideList = new StringBuilder();
        for(TextView tv: mPesticideList) {
            pesticideList.append(tv.getText().toString());
            pesticideList.append(",");
        }
        String pesticideListString = pesticideList.toString();

        StringBuilder fertilizerList = new StringBuilder();
        for(TextView tv: mFertilizerList) {
            fertilizerList.append(tv.getText().toString());
            fertilizerList.append(",");
        }
        String fertilizerListString = fertilizerList.toString();

        String frequency = ((RadioButton)findViewById(mRadioGroupHarvestFrequency.getCheckedRadioButtonId())).getText().toString();

        Call<SaveCrop> call = retrofit_interface.SaveCropFarmer(
                "3",mUserProfile.getApiToken(),
                mCropSpinner.getSelectedItem().toString(), mVarietySpinner.getSelectedItem().toString(), mGradeSpinner.getSelectedItem().toString(),
                mSowingDateTv.getText().toString(),  mSowingArea.getText().toString(),
                mAreaSpinner.getSelectedItem().toString(),"NOT USED",
                pesticideListString, fertilizerListString,
                mExpectedGradeSpinner1.getSelectedItem().toString(),mExpectedGradeSpinner2.getSelectedItem().toString(),
                mExpectedGradeSpinner3.getSelectedItem().toString(),mHarvestingDateTv.getText().toString(),
                frequency,"Apple",
                "Apple","Apple");

        TextView mHarvestingDateTv;

        //mNumTimesSpinner
        //mHarvesstingQuantitySpinner
        //mPerTimeSpinner
        //mPerTimeSpinner2
        //mPerNumberET

        call.enqueue(new Callback<SaveCrop>() {
            @Override
            public void onResponse(Call<SaveCrop> call, Response<SaveCrop> response) {
                try {
                    Log.d("TRADE CROP", "onResponse: code Crop farmer ");
                    Log.d("TRADE CROP", String.valueOf(response.body().getStatus()));
                    Log.d("TRADE CROP", response.body().getMessage().toString());
                    Log.d("TRADE CROP", response.body().getData().getCreatedByType());
                    Log.d("TRADE CROP", response.body().getData().getSowingCrop());
                }
                catch (Exception e){e.getMessage();}
            }

            @Override
            public void onFailure(Call<SaveCrop> call, Throwable t) {
                Log.d("TRADE CROP", "Save Crop Farmer  "+ t.getMessage());
            }
        });
    }

    public void saveImage(File file) {
        Log.d("FILE NAME -> ", file.getName());
        String token = mUserProfile.getApiToken();
        Log.d("MY TOKEN -> ", token);

        final RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file",file.getName(),requestFile);

        Call<SaveMedia> call = retrofit_interface.SaveImage("17", mUserProfile.getApiToken(),body);

        call.enqueue(new Callback<SaveMedia>() {
            @Override
            public void onResponse(Call<SaveMedia> call, Response<SaveMedia> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code save image ");
                    int status = response.body().getStatus();
                    Logs.d("CheckStatus", String.valueOf(status));
                    Logs.d("CheckStatus", response.body().getMessage());
                    String name=response.body().getMedia().getUrl();
                    String type=response.body().getMedia().getType();
                    Logs.d("CheckStatus", "image name "+ name);
                    Logs.d("CheckStatus", "type "+type);

                }
                catch (Exception e){e.getMessage();}
            }

            @Override
            public void onFailure(Call<SaveMedia> call, Throwable t) {
                Log.d("CheckStatus", "save image  "+ t.getMessage());
            }
        });
    }

    public void saveFarmerCrop(View view) {
        SaveCropFarmer();
        for(int i =0; i < mImageFiles.size(); i++) {
            File file = mImageFiles.get(i);
            saveImage(file);
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }
}
