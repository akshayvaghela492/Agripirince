package com.agriiprince.mvvm.ui.registration.adapters;

/**
 * Created by dtrah on 11/13/2018.
 */

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.db.DatabaseManager;
import com.agriiprince.R;
import com.agriiprince.mvvm.retrofit.model.login.Profile;
import com.agriiprince.mvvm.ui.registration.SelectCropActivity;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.SelectedCropModel;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.profile.UserUpdate;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.utils.StringUtils;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static com.agriiprince.utils.Utils.showToast;

public class InterestedListAdapter extends BaseAdapter {

    private Context context;
    public static ArrayList<SelectedCropModel> modelArrayList;
    List<Crop> mCrops;
    private StringUtils utils;
    private UserProfile mUserProfile;
    public VarietListAdapter mAdapter;
    DatabaseManager mDatabase;

    //ArrayList<String> mSelectedCropVar = new ArrayList<>();
    public InterestedListAdapter(Context context, ArrayList<SelectedCropModel> modelArrayList, List<Crop> mCrops) {

        this.context = context;
        this.modelArrayList = modelArrayList;
        this.mCrops = mCrops;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return modelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return modelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        utils = AppController.getInstance().getStringUtils();
        mDatabase = new DatabaseManager(context);
        mUserProfile = new UserProfile(context);
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.crop_listitem, null, true);

            holder.crop_variety = (RecyclerView) convertView.findViewById(R.id.recycler_View);
            holder.imageView = (ImageView) convertView.findViewById(R.id.crop_img);
            holder.crop_name = (TextView) convertView.findViewById(R.id.crop_name);
            holder.close_selectcrop = (ImageView) convertView.findViewById(R.id.close_selectcrop);
            //holder.down_btn = (ImageView)convertView.findViewById(R.id.down_btn);

            convertView.setTag(holder);

        } else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder) convertView.getTag();
        }

        holder.close_selectcrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //remove all data where cropname is this and save in local db n call API
                mDatabase.deleteTable("cropvariety", " crop_name = '" + modelArrayList.get(position).getCrop() + "'");
                mAdapter.notifyItemRemoved(position);
                modelArrayList.remove(position);
                notifyDataSetChanged();

                final List<String> mSelectedCrops = new ArrayList<>();
                for (int i = 0; i < modelArrayList.size(); i++) {
                    if (modelArrayList.get(i).getSelected()) {
                        //get data from db where crop_name == getCrop() n call API
                        Cursor c = mDatabase.getDataByCropName(modelArrayList.get(i).getCrop());
                        if (c.getCount() > 0) {
                            if (c.moveToFirst()) {
                                do {
                                    mSelectedCrops.add(c.getString(c.getColumnIndex("crop_id")));
                                } while (c.moveToNext());
                            }
                            c.close();
                            mDatabase.close();
                        }
                    }
                }
                //
    /*            StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_FARMER_PROFILE,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                System.out.println("update_farmer_profile_response: " + response);
                                try {
                                    JSONArray responseArray = new JSONArray(response);
                                    JSONObject responseObject = responseArray.getJSONObject(0);
                                    int errorCode = responseObject.getInt("error_code");
                                    if (errorCode == 100) {
                                        Toast.makeText(context, utils.getLocalizedString(R.string.successfully_updated), Toast.LENGTH_LONG).show();
                                       /* Intent intent = new Intent(getActivity(), FarmerProfileActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);*/
                              /*      } else {
                                        Toast.makeText(context, utils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if (error instanceof TimeoutError) {
                                    showToast((SelectCropActivity) context, utils.getLocalizedString(R.string.slow_internet_kindly_check_internet_to_proceed_seamlessly).toString());
                                } else if (error instanceof NoConnectionError) {
                                    showToast((SelectCropActivity) context, utils.getLocalizedString(R.string.no_internet_connection_try_later).toString());
                                }
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();

                        params.put("api_token", mUserProfile.getApiToken());

                        //Mandatory
                        params.put("user_id", utils.getPref().getUserId());
                        params.put("password", utils.getPref().getUserPassword());

                        //Optional
                        JSONArray jsonArray = new JSONArray();
                        for (String item : mSelectedCrops) {
                            jsonArray.put(item);
                        }
                        params.put("interested_crop_list", "" + jsonArray);

                        System.out.println("update_farmer_profile_request: " + params.toString());
                        return params;
                    }
                };
                AppController.getInstance().addToRequestQueue(stringRequest);
                */

                Profile params = new Profile();
                //params.setFarmerId("UKYVLUT5");
                //params.setFarmerContact("1478523691");

                params.setFarmerId(utils.getPref().getUserId());
                params.setFarmerContact(utils.getPref().getMobileNo());

                params.setInterestedCropList(mSelectedCrops);

                System.out.println("update_farmer_profile_request: " + params.toString());

                API_Manager apiManager=new API_Manager();
                UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);
                Call<UserUpdate> call=retrofit_interface.updateprofile(mUserProfile.getApiToken(),params);
                call.enqueue(new Callback<UserUpdate>() {
                    @Override
                    public void onResponse(Call<UserUpdate> call, retrofit2.Response<UserUpdate> response) {
                        try {
                            Log.d("farmer_profile","intrest adap response"+response.body());
                            if (response.body().getCode() == 200) {
                                Toast.makeText(context, utils.getLocalizedString(R.string.successfully_updated), Toast.LENGTH_LONG).show();
                                       /* Intent intent = new Intent(getActivity(), FarmerProfileActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);*/
                                   } else {
                                        Toast.makeText(context, utils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                    }
                    @Override
                    public void onFailure(Call<UserUpdate> call, Throwable t) {
                        Log.d("farmer_profile","intrest adap fail"+ t.getMessage());
                        if (t instanceof TimeoutError) {
                            showToast((SelectCropActivity) context, utils.getLocalizedString(R.string.slow_internet_kindly_check_internet_to_proceed_seamlessly).toString());
                        } else if (t instanceof NoConnectionError) {
                            showToast((SelectCropActivity) context, utils.getLocalizedString(R.string.no_internet_connection_try_later).toString());
                        }
                    }
                });
            }

        });


        holder.crop_name.setText(utils.getBilingualStringForApi(modelArrayList.get(position).getCrop(), Crop.getSecondNameByCropName(mCrops, modelArrayList.get(position).getCrop())));
        //mSelectedCropVar.clear();
        ArrayList<String> mSelectedCropVar = new ArrayList<>();
        Cursor c = mDatabase.getDataByCropName(modelArrayList.get(position).getCrop());
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    if (contains(mSelectedCropVar, c.getString(c.getColumnIndex("crop_variety")))) {
                    } else {
                        mSelectedCropVar.add(c.getString(c.getColumnIndex("crop_variety")));
                    }

                } while (c.moveToNext());
            }
            c.close();
            mDatabase.close();
        }

        mAdapter = new VarietListAdapter(mSelectedCropVar, context, mCrops);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        holder.crop_variety.setLayoutManager(mLayoutManager);
        holder.crop_variety.setItemAnimator(new DefaultItemAnimator());
        holder.crop_variety.setAdapter(mAdapter);

        try {
            String[] parts = modelArrayList.get(position).getCrop().split("\\(");
            String image = parts[0].toLowerCase() + Config.IMAGE_NAME_END;
            Glide.with(context)
                    .setDefaultRequestOptions(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.ALL))
                    .load(Config.CROP_IMAGE_BASE_URL + image)
                    .into(holder.imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    boolean contains(List<String> list, String name) {
        for (String item : list) {
            if (item.equals(name)) {
                return true;
            }
        }
        return false;
    }

    public class ViewHolder {
        private ImageView imageView, close_selectcrop;
        private TextView crop_name;
        private RecyclerView crop_variety;

    }
}