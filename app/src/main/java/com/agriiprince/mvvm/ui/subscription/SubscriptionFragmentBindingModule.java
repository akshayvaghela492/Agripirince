package com.agriiprince.mvvm.ui.subscription;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SubscriptionFragmentBindingModule {

    @ContributesAndroidInjector
    public abstract BplCardFragment contributesBplCardFragmentInjector();
}
