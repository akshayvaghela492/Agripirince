package com.agriiprince.mvvm.ui.disease;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.agriiprince.activities.TutorialActivity;
import com.agriiprince.db.DatabaseManager;
import com.agriiprince.R;
import com.agriiprince.adapter.farmer.DiseaseListAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.ActivityCropDiseasesDiseaselistBinding;
import com.agriiprince.dataservice.DataResponse;
import com.agriiprince.dataservice.DiseaseListDataService;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.appcontroller.DiseaseDiagnosisJob;
import com.agriiprince.models.DiseaseListModel;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.Utils;
import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

import static com.agriiprince.utils.Utils.showLog;

public class DiseaseListActivity extends AppCompatActivity {
    private final String TAG = DiseaseListActivity.class.getSimpleName();

    DiseaseListAdapter adapter;

    private StringUtils utils;

    UserProfile userProfile;

    private String cropName;

    private DiseaseListDataService diseaseListDataService;

    ActivityCropDiseasesDiseaselistBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_crop_diseases_diseaselist);
        utils = AppController.getInstance().getStringUtils();
        userProfile = new UserProfile(this);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent oldIntent = getIntent();
        cropName = oldIntent.getStringExtra("cropName");
        showLog("cropName:" + cropName);

        adapter = new DiseaseListAdapter(utils, diseaseListListener);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        binding.rvDiseaseName.setLayoutManager(linearLayoutManager);
        binding.rvDiseaseName.setAdapter(adapter);

        //getCropDiseaseNames(cropName);

        ImageView imgview_help =  findViewById(R.id.iv_help);

        imgview_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DiseaseListActivity.this, TutorialActivity.class);
                intent.putExtra("code", "DISED");
                startActivity(intent);
            }
        });

        DatabaseManager databaseManager = AppController.getInstance().getDataBase();
        diseaseListDataService = new DiseaseListDataService(new PrefManager(this), databaseManager);
        Log.d("TESTING_VOLLEY", "fetchDiseaseInfo-2");
        diseaseListDataService.getCropDiseaseList(cropName, userProfile.getApiToken(), dataResponse);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_disease_crop_list, menu);

        MenuItem searchItem = menu.findItem( R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(onQueryTextListener);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private SearchView.OnQueryTextListener onQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            adapter.getFilter().filter(s);
            return false;
        }
    };

    private DiseaseListAdapter.OnDiseaseListListener diseaseListListener = new DiseaseListAdapter.OnDiseaseListListener() {
        @Override
        public void onClickItem(int position) {
            //-------

            int size = adapter.getItemCount();
            ArrayList<String> DiseaseIds = new ArrayList<String>();
            for (int i = 0; i < size; i++)
            {
                DiseaseListModel model = adapter.getItem(i);
                DiseaseIds.add(model.getDisease_id());
            }
            
            //-------
            
            DiseaseListModel model = adapter.getItem(position);
            Intent intent = new Intent(DiseaseListActivity.this, DiseaseInfoActivity.class);
            intent.putExtra(DiseaseInfoActivity.KEY_DISEASE_ID, model.getDisease_id());
            intent.putExtra(DiseaseInfoActivity.KEY_DISEASE_POS, Integer.toString(position));
            intent.putStringArrayListExtra(DiseaseInfoActivity.KEY_DISEASE_IDS, DiseaseIds);

            startActivity(intent);
        }
    };

    private void hideProgressBar() {
        binding.diseaseNameProgress.setVisibility(View.GONE);
    }

    private void showProgressBar() {
        hideEmptyText();
        binding.diseaseNameProgress.setVisibility(View.VISIBLE);
    }

    private void showEmptyText() {
        binding.diseaseNameEmpty.setText(utils.getLocalizedString(R.string.disease_name_not_found));
        binding.diseaseNameEmpty.setVisibility(View.VISIBLE);
    }

    private void hideEmptyText() {
        binding.diseaseNameEmpty.setVisibility(View.GONE);
    }

    private void showDialogToRequestDowload() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(utils.getLocalizedString(R.string.you_are_offline));

        String message = utils.getPrimaryString(R.string.disease_offline_request_part_one)
                + " " + cropName + " " + utils.getPrimaryString(R.string.disease_offline_request_part_two);

        if (utils.showSecondaryText()) {
            message += "\n" + utils.getSecondaryString(R.string.disease_offline_request_part_one)
                    + " " + cropName + " " + utils.getSecondaryString(R.string.disease_offline_request_part_two);
        }

        dialog.setMessage(message);

        dialog.setPositiveButton(utils.getLocalizedString(R.string.okay), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DiseaseDiagnosisJob.scheduleJob();

                diseaseListDataService.requestToDowload(cropName);

                Toast.makeText(DiseaseListActivity.this,
                        utils.getLocalizedString(R.string.disease_offline_request_success),
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        dialog.setNegativeButton(utils.getLocalizedString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        dialog.show();
    }

    private DataResponse<List<DiseaseListModel>> dataResponse = new DataResponse<List<DiseaseListModel>>() {
        @Override
        public void onSuccessResponse(List<DiseaseListModel> response) {
            hideProgressBar();
            hideEmptyText();
            adapter.addAllItem(response);
        }

        @Override
        public void onParseError() {
            hideProgressBar();
            showEmptyText();
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgressBar();
            if (error instanceof NoConnectionError) {
                showDialogToRequestDowload();
            } else {
                showEmptyText();
                Utils.showVolleyError(DiseaseListActivity.this, error);
            }
        }
    };

}
