package com.agriiprince.mvvm.ui.customwidgets;

import android.content.Context;

import java.util.Calendar;

/**
 * Created by Manu Sajjan on 19-12-2018.
 */
public class DatePickerDialog extends android.app.DatePickerDialog {

    public DatePickerDialog(Context context, android.app.DatePickerDialog.OnDateSetListener listener, Calendar calendar) {
        this(context, listener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    public DatePickerDialog(Context context, android.app.DatePickerDialog.OnDateSetListener listener, int year, int month, int dayOfMonth) {
        super(context, listener, year, month, dayOfMonth);
    }

}
