package com.agriiprince.mvvm.ui.tradecrop.activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.agriiprince.R;
import com.agriiprince.databinding.ActivityFigViewfarmerBinding;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.ui.tradecrop.tabs.ManageCropSectionsPagerAdapter;

public class FIGViewFarmerActivity extends AppCompatActivity {

    private ActivityFigViewfarmerBinding binding;
    private UserProfile mUserProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_fig_viewfarmer);
        mUserProfile = new UserProfile(this);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ManageCropSectionsPagerAdapter sectionsPagerAdapter = new ManageCropSectionsPagerAdapter(this, getSupportFragmentManager());
        binding.viewpager.setAdapter(sectionsPagerAdapter);
        binding.tabs.setupWithViewPager(binding.viewpager);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();

                Context context = view.getContext();
                Intent intent = new Intent(context, TradeCropAddActivity.class);
                context.startActivity(intent);
            }
        });

        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String number = intent.getStringExtra("number");

        binding.phoneNum.setText(number);

        setTitle(name);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
