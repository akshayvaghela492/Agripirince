package com.agriiprince.mvvm.ui.weather;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.mvvm.model.WeatherModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class WeatherForecastAdapter extends RecyclerView.Adapter<WeatherForecastAdapter.MyViewHolder> {

    private ArrayList<WeatherModel> weatherForecastList;
    Context context;

    public WeatherForecastAdapter(ArrayList<WeatherModel> weatherForecastList, Context context) {
        this.weatherForecastList = weatherForecastList;
        this.context = context;
    }

    @NonNull
    @Override
    public WeatherForecastAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_weather_forecast, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherForecastAdapter.MyViewHolder holder, int position) {

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        DateFormat outputFormat = new SimpleDateFormat("EEE, dd MMM, yyyy", Locale.ENGLISH);
        String inputDateStr = weatherForecastList.get(position).getDate();
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
            String outputDateStr = outputFormat.format(date);
            holder.date.setText(outputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.city.setText(weatherForecastList.get(position).getCity());

        double maxTemp = (5.0 / 9.0) * (weatherForecastList.get(position).getTempMax() - 32);
        double minTemp = (5.0 / 9.0) * (weatherForecastList.get(position).getTempMin() - 32);

        String maxTemperature = String.format("%.1f", maxTemp);
        String minTemperature = String.format("%.1f", minTemp);

        holder.minTemp.setText(minTemperature + " \u2103" + " " + context.getResources().getString(R.string.min_temp));
        holder.maxTemp.setText(maxTemperature + " \u2103" + " " + context.getResources().getString(R.string.max_temp));
        holder.nightCondition.setText(weatherForecastList.get(position).getNightCondition());
        holder.dayCondition.setText(weatherForecastList.get(position).getDayCondition());
        holder.dayIcon.setImageResource(weatherForecastList.get(position).getDayIcon());
        holder.nightIcon.setImageResource(weatherForecastList.get(position).getNightIcon());

    }

    @Override
    public int getItemCount() {
        return weatherForecastList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView city;
        TextView date;
        TextView maxTemp;
        TextView minTemp;
        TextView dayCondition;
        TextView nightCondition;
        ImageView dayIcon;
        ImageView nightIcon;

        public MyViewHolder(View itemView) {
            super(itemView);

            city = itemView.findViewById(R.id.city_name);
            date = itemView.findViewById(R.id.date);
            maxTemp = itemView.findViewById(R.id.max_temp);
            minTemp = itemView.findViewById(R.id.min_temp);
            dayCondition = itemView.findViewById(R.id.day_condition);
            nightCondition = itemView.findViewById(R.id.night_condition);
            dayIcon = itemView.findViewById(R.id.day_weather_icon);
            nightIcon = itemView.findViewById(R.id.night_weather_icon);

        }

    }

}
