package com.agriiprince.mvvm.ui.splash;

import android.arch.lifecycle.ViewModel;

import com.agriiprince.mvvm.data.repository.VersionUpdateRepository;
import com.agriiprince.mvvm.data.repository.VersionUpdateRepositoryUseCase;
import com.agriiprince.mvvm.retrofit.service.VersionUpdateService;
import com.agriiprince.mvvm.di.ViewModelKey;
import com.agriiprince.mvvm.viewmodel.VersionUpdateViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import retrofit2.Retrofit;

@Module
public class VersionUpdateModule {

    @Provides
    VersionUpdateRepositoryUseCase providesVersionUpdateUseCase(VersionUpdateService versionUpdateService) {
        return new VersionUpdateRepository(versionUpdateService);
    }

    @Provides
    VersionUpdateService providesVersionUpdateService(Retrofit retrofit) {
        return retrofit.create(VersionUpdateService.class);
    }


    @Module(
            includes = VersionUpdateModule.class
    )
    public interface BindVersionUpdateViewModelModule {

        @Binds
        @IntoMap
        @ViewModelKey(VersionUpdateViewModel.class)
        ViewModel bindsVersionUpdateViewModel(VersionUpdateViewModel versionUpdateViewModel);
    }
}
