package com.agriiprince.mvvm.ui.tradecrop.adapters;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.agriiprince.R;
import com.agriiprince.mvvm.util.Logs;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class ImageBitmapAdapter extends RecyclerView.Adapter<ImageBitmapAdapter.MyViewHolder> {
    //private ImageView[] mDataset;
    private List<Bitmap> mBitmaps = new ArrayList<>();

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView imageView;
        public MyViewHolder(ImageView v) {
            super(v);
            imageView = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ImageBitmapAdapter(List<Bitmap> myBitmaps) {
        mBitmaps = myBitmaps;
        //mDataset = new ImageView[mURLs.size()];
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ImageBitmapAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
        // create a new view
        ImageView v = (ImageView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_image_content, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        //ImageView[] arr = new ImageView[mURLs.size()];

        //for(int i = 0; i < mURLs.size(); i++) {
        //    arr[i] = new ImageView(holder.imageView.getContext());
        //    arr[i].setBackgroundResource(R.drawable.ap_logo_final);
        //}

        //holder.imageView.setBackground(mDataset[position].getBackground());

        Logs.d("CheckStatus ASD in ->", mBitmaps.size()+" "+ mBitmaps.get(position));

        Glide.with(holder.imageView.getContext()).load(mBitmaps.get(position)).into(holder.imageView);
        //Glide.with(holder.imageView.getContext()).load("https://farmerimage.s3-ap-south-1.amazonaws.com/farmerimage/apple.jpg").into(holder.imageView);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mBitmaps.size();
    }
}
