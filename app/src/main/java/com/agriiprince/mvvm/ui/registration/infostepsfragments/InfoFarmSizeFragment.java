package com.agriiprince.mvvm.ui.registration.infostepsfragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.chip.Chip;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRadioButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.databinding.FragmentInfoFarmSizeBinding;
import com.agriiprince.model.FarmerProfileModel;
import com.agriiprince.utils.StringUtils;

public class InfoFarmSizeFragment extends Fragment {

    private static final String TAG = InfoFarmSizeFragment.class.getName();

    private int AREA_UNIT_INDEX = 0;

    private OnInfoFarmSizeInteractionListener mListener;

    private AppCompatImageView mBtnBack;

    private AppCompatButton mBtnNext;

    private Chip acre, hectare, bigha;

    private RadioGroup mRadioGroup;

    private AppCompatRadioButton mRbCustom;

    private TextInputEditText mEtCustomSize;

    private StringUtils stringUtils;

    private String[] areaUnits;

    private String[] areas;

    private FragmentInfoFarmSizeBinding binding;

    public InfoFarmSizeFragment() {
        // Required empty public constructor
    }


    public static InfoFarmSizeFragment newInstance() {
        InfoFarmSizeFragment fragment = new InfoFarmSizeFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info_farm_size, container, false);
        stringUtils = AppController.getInstance().getStringUtils();
        areaUnits = FarmerProfileModel.areaUnits;
        areas = FarmerProfileModel.areas;

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = DataBindingUtil.bind(view);

        ((TextView) view.findViewById(R.id.title)).setText(stringUtils.getLocalizedString(R.string.what_is_the_size_of_your_farm));
        ((TextView) view.findViewById(R.id.units_prefer)).setText(stringUtils.getLocalizedString(R.string.units_you_prefer));
        ((AppCompatButton) view.findViewById(R.id.info_next)).setText(stringUtils.getLocalizedString(R.string.next));

        mBtnBack = view.findViewById(R.id.info_back);

        mBtnNext = view.findViewById(R.id.info_next);

        mRadioGroup = view.findViewById(R.id.info_farm_size_radio_group);

        mRbCustom = view.findViewById(R.id.info_farm_size_rb_custom);

        mEtCustomSize = view.findViewById(R.id.info_farm_size_et_custom);
        mEtCustomSize.setHint(stringUtils.getLocalizedString(R.string.enter_size));

        acre = view.findViewById(R.id.info_farm_chip_acre);
        hectare = view.findViewById(R.id.info_farm_chip_hectare);
        bigha = view.findViewById(R.id.info_farm_chip_bigha);

        acre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetSelection();
                AREA_UNIT_INDEX = 0;
                acre.setChecked(true);
                onAreaUnitChange();
            }
        });
        hectare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetSelection();
                AREA_UNIT_INDEX = 1;
                hectare.setChecked(true);
                onAreaUnitChange();
            }
        });
        bigha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetSelection();
                AREA_UNIT_INDEX = 2;
                bigha.setChecked(true);
                onAreaUnitChange();
            }
        });

        onAreaUnitChange();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, areaUnits[AREA_UNIT_INDEX]);
                int farmsize = 0;

                // if farm size is not selected ask user to select and check first to avoid null pointer
                if (mRadioGroup.getCheckedRadioButtonId() == -1 && !mRbCustom.isChecked()) {
                    Toast.makeText(getContext(), stringUtils.getLocalizedString(R.string.please_select_your_farm_size), Toast.LENGTH_SHORT).show();

                } else if (mRbCustom.isChecked()) {
                    if (mEtCustomSize.getText() != null && !TextUtils.isEmpty(mEtCustomSize.getText().toString())) {

                        if(!(mEtCustomSize.getText().equals("")))
                        farmsize = Integer.parseInt(mEtCustomSize.getText().toString());
                        Log.d("Size...", " -------2> " + farmsize);

                        mListener.onInfoFarmSizeSuccess(areaUnits[AREA_UNIT_INDEX], farmsize);

                    } else {
                        Toast.makeText(getContext(), stringUtils.getLocalizedString(R.string.enter_farm_size), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    AppCompatRadioButton radioButton = mRadioGroup.findViewById(mRadioGroup.getCheckedRadioButtonId());
                    String[] temp = radioButton.getText().toString().split(" ");
                    String size = temp[temp.length - 2];

                    Log.d("Size...", " -------> " + size);

                    if(size.equals("1")) farmsize = 1;
                    else if(size.equals("1-2")) farmsize = 2;
                    else if(size.equals("2-3")) farmsize = 3;
                    else if(size.equals("3-5")) farmsize = 5;
                    else if(size.equals("5-10")) farmsize = 10;
                    else if(size.equals("10")) farmsize = 11;

                    Log.d("Size...", " -------2> " + farmsize);

                    mListener.onInfoFarmSizeSuccess(areaUnits[AREA_UNIT_INDEX], farmsize);
                }
            }
        });

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (mRbCustom.isChecked() && group.getCheckedRadioButtonId() != -1) {
                    mRbCustom.setChecked(false);
                }
            }
        });

        mRbCustom.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mRadioGroup.clearCheck();
                    mEtCustomSize.setEnabled(true);
                    mRbCustom.setChecked(true);
                } else {
                    mEtCustomSize.setEnabled(false);
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnInfoFarmSizeInteractionListener) {
            mListener = (OnInfoFarmSizeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnInfoLocationInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void onAreaUnitChange() {
        String unit = "";
        if (AREA_UNIT_INDEX == 0) unit = stringUtils.getLocalizedString(R.string.acre, true).toString().toLowerCase();
        else if (AREA_UNIT_INDEX == 1) unit = stringUtils.getLocalizedString(R.string.hectare, true).toString().toLowerCase();
        else unit = stringUtils.getLocalizedString(R.string.bigha, true).toString().toLowerCase();

        binding.infoFarmSizeLess1.setText(areas[0] + " " + unit);
        binding.infoFarmSize12.setText(areas[1] + " " + unit);
        binding.infoFarmSize23.setText(areas[2] + " " + unit);
        binding.infoFarmSize35.setText(areas[3] + " " + unit);
        binding.infoFarmSize510.setText(areas[4] + " " + unit);
        binding.infoFarmSizeAbove10.setText(areas[5] + " " + unit);
    }


    private void resetSelection() {
        acre.setChecked(false);
        hectare.setChecked(false);
        bigha.setChecked(false);
    }


    public interface OnInfoFarmSizeInteractionListener {

        void onInfoFarmSizeSuccess(String unit, int size);
    }
}
