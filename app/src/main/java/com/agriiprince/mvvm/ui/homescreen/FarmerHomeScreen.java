package com.agriiprince.mvvm.ui.homescreen;

import android.app.ProgressDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.agriiprince.R;
import com.agriiprince.fragments.CaListFragment;
import com.agriiprince.fragments.CropClassFragment;
import com.agriiprince.fragments.CropNameFragment;
import com.agriiprince.fragments.InfoTabFragment;
import com.agriiprince.fragments.FarmerCaFragment;
import com.agriiprince.fragments.NotificationFragment;
import com.agriiprince.fragments.rss.RssFeedFragment;
import com.agriiprince.fragments.rss.RssSubscribeFragment;

public abstract class FarmerHomeScreen extends AppCompatActivity implements
        FarmerCaFragment.OnFarmerCaInteractionListener, HomeFragment.OnHomeFragmentInteractionListener,
        CropClassFragment.OnCropClassInteractionListener, CropNameFragment.OnCropTypeInteractionListener,
        CaListFragment.OnFragmentInteractionListener, RssSubscribeFragment.OnRssSubscribeInteractionListener,
        RssFeedFragment.OnRssFeedInteractionListener, InfoTabFragment.OnInfoTabInteractionListener,
        NotificationFragment.OnNotificationInteractionListener {


    protected final String TAG = FarmerHomeScreenActivity.class.getName();

    protected static final int PERMISSION_REQUEST_CODE = 9;

    protected boolean isCaSearchMandiVisible;

    private ProgressDialog mProgressDialog;

    @Override
    public void showProgress() {
        showProgressDialog();
    }

    @Override
    public void dismissProgress() {
        dismissProgressDialog();
    }

    @Override
    public void onSuccess() {
        // do nothing
    }

    @Override
    public void requestPermission(String permission) {
        ActivityCompat.requestPermissions(this, new String[]{permission}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onSelectCropClass(String cropClass) {
        FarmerCaFragment farmerCaFragment = (FarmerCaFragment) getSupportFragmentManager().findFragmentByTag(FarmerCaFragment.class.getSimpleName());

        if (farmerCaFragment != null)
            farmerCaFragment.setCropClass(cropClass);

    }

    @Override
    public void onSelectCropType(String cropType) {
        FarmerCaFragment farmerCaFragment = (FarmerCaFragment) getSupportFragmentManager().findFragmentByTag(FarmerCaFragment.class.getSimpleName());

        if (farmerCaFragment != null)
            farmerCaFragment.setCropType(cropType);
    }

    @Override
    public void onSearchMandiOpen() {
        isCaSearchMandiVisible = true;
    }

    @Override
    public void onSearchMandiClose() {
        isCaSearchMandiVisible = false;
    }

    protected void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.please_wait));
        }

        if (!mProgressDialog.isShowing()) {
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
    }

    protected void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

}
