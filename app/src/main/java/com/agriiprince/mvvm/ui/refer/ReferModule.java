package com.agriiprince.mvvm.ui.refer;

import android.app.Application;
import android.arch.lifecycle.ViewModel;

import com.agriiprince.mvvm.data.repository.ReferFriendsRepository;
import com.agriiprince.mvvm.data.repository.ReferFriendsRepositoryUseCase;
import com.agriiprince.mvvm.di.ViewModelKey;
import com.agriiprince.mvvm.viewmodel.ReferFriendsViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public class ReferModule {

    @Provides
    ReferFriendsRepositoryUseCase providesReferFriendsRepository(Application application) {
        return new ReferFriendsRepository(application);
    }


    @Provides
    ReferFriendsAdapter providesReferFriendsAdapter() {
        return new ReferFriendsAdapter();
    }

    @Module(
            includes = {
                    ReferModule.class
            }
    )
    public interface BindReferViewModel {

        @Binds
        @IntoMap
        @ViewModelKey(ReferFriendsViewModel.class)
        ViewModel bindsReferFriendsViewModel(ReferFriendsViewModel referFriendsViewModel);

    }
}
