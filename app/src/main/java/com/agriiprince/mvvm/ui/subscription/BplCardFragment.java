package com.agriiprince.mvvm.ui.subscription;


import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.databinding.FragmentBplCardBinding;
import com.agriiprince.mvvm.retrofit.dto.subscription.BplPhotoUpload;
import com.agriiprince.mvvm.ui.LiveDataObserver;
import com.agriiprince.mvvm.ui.base.BaseFragment;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.mvvm.util.ResUtils;
import com.agriiprince.mvvm.util.GeneralUtils;
import com.agriiprince.mvvm.viewmodel.SubscriptionViewModel;
import com.agriiprince.mvvm.viewmodel.SubscriptionViewModelUseCase;

import javax.inject.Inject;

import static android.app.Activity.RESULT_OK;

public class BplCardFragment extends BaseFragment {

    private static final String TAG = "BplCardFragment";

    private final int REQUEST_CODE_CAMERA = 1122;

    private final int REQUEST_CODE_GALLERY = 1133;

    public BplCardFragment() {
        // Required empty public constructor
    }

    public static BplCardFragment newInstance() {
        BplCardFragment fragment = new BplCardFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    private SubscriptionViewModelUseCase mViewModel;

    private Bitmap mBplImage;

    private FragmentBplCardBinding mBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_bpl_card;
    }

    @Override
    protected void performDataBinding(View view) {
        mBinding = DataBindingUtil.bind(view);
    }

    @Override
    protected void initControl() {
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(SubscriptionViewModel.class);
    }

    @Override
    protected void initViewControl() {
        Drawable drawable = ResUtils.tintDrawable(getResources().getDrawable(R.drawable.ic_add_a_photo_black_24dp),
                getResources().getColor(R.color.colorPrimaryFarmer));
        mBinding.startCamera.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        drawable = ResUtils.tintDrawable(getResources().getDrawable(R.drawable.ic_image_black_24dp),
                getResources().getColor(R.color.colorPrimaryFarmer));
        mBinding.openGallery.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

    }

    @Override
    protected void initTextViews() {
        setBilingualText(mBinding.mainLabel, R.string.bpl_card, true);
        setBilingualText(mBinding.addImageHint, R.string.add_your_bpl_card_photo);
        setBilingualText(mBinding.or, R.string.text_or);
        setBilingualText(mBinding.startCamera, R.string.start_camera);
        setBilingualText(mBinding.openGallery, R.string.open_gallery);
        setBilingualText(mBinding.submit, R.string.submit);
    }

    @Override
    protected void initListener() {
        mBinding.root.setOnClickListener(onClickOutside);
        mBinding.submit.setOnClickListener(onClickSubmit);

        mBinding.startCamera.setOnClickListener(onClickCamera);
        mBinding.openGallery.setOnClickListener(onClickGallery);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CAMERA && resultCode == RESULT_OK) {
            Logs.d(TAG, "onActivityResult: camera");

            Uri imageUri = data.getData();
            if (getContext() != null) {
                String path = GeneralUtils.getImagePathForCameraUri(getContext(), imageUri);
                onPickImage(path);
            }

        } else if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK) {
            Logs.d(TAG, "onActivityResult: gallery");

            Uri imageUri = data.getData();
            if (getContext() != null) {
                Bitmap bitmap = GeneralUtils.getBitmapForMediaUri(getContext(), imageUri);
                onPickImage(bitmap);
            }
        }
    }

    /**
     * touch listener on outside main layout. which removes the fragment from the stack
     */
    private View.OnClickListener onClickOutside = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            removeFragment();
        }
    };

    /**
     * listener for submit button
     */
    private View.OnClickListener onClickSubmit = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mBplImage != null) {
                mViewModel.uploadBplPhoto(mBplImage)
                        .observe(BplCardFragment.this, bplUploadObserver);
            } else {

            }
        }
    };

    /**
     *
     */
    private View.OnClickListener onClickCamera = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CODE_CAMERA);
        }
    };

    /**
     *
     */
    private View.OnClickListener onClickGallery = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, REQUEST_CODE_GALLERY);
        }
    };


    private void onPickImage(String path) {
        onPickImage(BitmapFactory.decodeFile(path));
    }

    private void onPickImage(Bitmap bitmap) {
        mBplImage = bitmap;
        mBinding.bplImage.setImageBitmap(mBplImage);
    }

    private LiveDataObserver<BplPhotoUpload> bplUploadObserver = new LiveDataObserver<BplPhotoUpload>() {
                @Override
                protected void onLoading() {
                    showProgressDialog();
                    Logs.d(TAG, "onLoading: bplUploadObserver");
                }

        @Override
                protected void onSuccess(BplPhotoUpload data) {
                    hideProgressDialog();
                    Logs.d(TAG, "onSuccess: bplUploadObserver");

                    if (data != null) {
                        //if (data.getErrorCode() == 100) {
                        if (data.getCode() == 200) {
                            showDefaultSnackBar(getBilingualString(R.string.bpl_photo_uploaded));
                            removeFragment();
                        } else {
                            showDefaultSnackBar(data.getMessage());
                        }
                    }

                }

                @Override
                protected void onFail(Throwable t) {
                    hideProgressDialog();
                    Toast.makeText(getContext(), getBilingualString(getErrorMessageId()), Toast.LENGTH_SHORT).show();
                }

                @Override
                protected void onNetworkFail() {
                    hideProgressDialog();
                    showDefaultSnackBar(getBilingualString(R.string.no_internet_connection_try_later));
                }
            };
}
