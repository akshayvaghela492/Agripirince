package com.agriiprince.mvvm.ui.refer;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.databinding.ItemReferFriendBinding;
import com.agriiprince.mvvm.model.Contact;
import com.agriiprince.mvvm.ui.base.BaseRecyclerViewAdapter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

public class ReferFriendsAdapter extends BaseRecyclerViewAdapter<Contact, ReferFriendsAdapter.ReferViewHolder> {


    @NonNull
    @Override
    public ReferViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_refer_friend, viewGroup, false);
        return new ReferViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReferViewHolder holder, int i) {
        Contact contact = getItem(i);
        holder.bind(contact);
    }

    class ReferViewHolder extends RecyclerView.ViewHolder {

        private ItemReferFriendBinding mBinding;

        public ReferViewHolder(@NonNull View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }

        private void bind(Contact contact) {
            mBinding.setContact(contact);
            mBinding.executePendingBindings();

            Glide.with(mBinding.image)
                    .load(contact.getPhotoUri())
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                            .placeholder(R.drawable.bg_refer_friends_image))
                    .into(mBinding.image);
        }
    }
}
