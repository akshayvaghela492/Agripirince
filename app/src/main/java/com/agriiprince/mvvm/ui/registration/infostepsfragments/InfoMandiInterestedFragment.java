package com.agriiprince.mvvm.ui.registration.infostepsfragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.chip.Chip;
import android.support.design.chip.ChipGroup;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.mvvm.retrofit.model.login.Profile;
import com.agriiprince.mvvm.ui.registration.FarmerProfileActivity;
import com.agriiprince.adapter.CustomStringArrayAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.dataservice.MandiDataService;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.MandiModel;
import com.agriiprince.model.NearestMandiModel;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.profile.UserUpdate;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.utils.StringUtils;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static com.agriiprince.utils.Utils.showToast;

public class InfoMandiInterestedFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private static final String TAG = InfoMandiInterestedFragment.class.getSimpleName();

    private static final String ARG_INTERESTED_MANDIS_ID = "mandis";
    private static final String ARG_SELECTED_MANDIS = "selected_mandis";
    private static final String ARG_NEAREST_MANDIS_ID = "nearest_mandi_id";
    private static final String ARG_ALLMANDILIST = "mandislist";
    private static final String ARG_LOCATION = "userlocation";

    private static final String ARG_lAT = "userLat";
    public static final String ARG_LNG = "userLng";
    public static final String ARG_FROM_OE_PROFILE = "fromOeProfile";

    private final int MAX_SELECTION = 14;

    private final int MIN_SELECTION = 3;

    private OnInfoMandiInteractionListener mListener;

    private boolean mandiSetupFinished = false;

    private SupportMapFragment mMapFragment;
    private GoogleMap mGoogleMap;
    private double mUserLat;
    private double mUserLng;

    private List<MandiModel> mMandiList;

    private List<NearestMandiModel> mMandiListbyDistance;

    private List<String> listOfNearestMandiNames;
    //private List<String> mListOfNearestMandiNamesCopy;
    private String[] mlistOfNearestMandiNamesArray;
    private int listOfNearestMandiNamesSize;

    //private List<String> mSelectedMandies;
    private List<String> mSelectedMandiesIds;

    private String mNearestMandiId;

    private AppCompatAutoCompleteTextView mAcMandiName;
    private AppCompatAutoCompleteTextView mAcNearestMandiName;

    private AppCompatImageView mBtnBack;

    private AppCompatButton mBtnNext;

    private ChipGroup mChipGroup;

    private double mCurLat, mCurLng, mNewLat, mNewLng;
    private String mCurTitle = "";
    private String mNewTitle = "";

    private ScrollView mScrollView;

    private boolean fromOeProfile;
    private boolean fromProfile;

    private ProgressDialog dialog;

    List<String> mItems;

    private List<String> mandiListfromPrev;
    StringUtils stringUtils;
    private UserProfile mUserProfile;

    String violet = "#BA55D3";
    String indigo = "#4B0082";
    String blue = "#87cefa";
    String green = "#00FF00";
    String yellow = "#FFFF00";
    String orange = "#F47C01";
    String red = "#FF0000";
    String lightred = "#F08080";
    //String grey = "#808080";
    String colors[] = {violet, indigo, blue, green, yellow, orange, red};

    //private AtomicBoolean isMandiesLoaded;

    public static InfoMandiInterestedFragment newInstance(String nearestMandiId, ArrayList<String> mandis,
                                                          double lat, double lng, boolean fromprofile, boolean fromOeProfile) {
        InfoMandiInterestedFragment fragment = new InfoMandiInterestedFragment();
        Bundle args = new Bundle();
        args.putString(ARG_NEAREST_MANDIS_ID, nearestMandiId);
        args.putStringArrayList(ARG_INTERESTED_MANDIS_ID, mandis);
        args.putDouble(ARG_lAT, lat);
        args.putDouble(ARG_LNG, lng);
        args.putBoolean("fromProfile", fromprofile);
        args.putBoolean(ARG_FROM_OE_PROFILE, fromOeProfile);

        fragment.setArguments(args);
        return fragment;
    }

    public static InfoMandiInterestedFragment newInstance(String nearestMandiId, ArrayList<String> mandies,
                                                          double lat, double lng, boolean fromprofile) {
        InfoMandiInterestedFragment fragment = new InfoMandiInterestedFragment();
        Bundle args = new Bundle();
        args.putString(ARG_NEAREST_MANDIS_ID, nearestMandiId);
        args.putStringArrayList(ARG_INTERESTED_MANDIS_ID, mandies);
        //args.putSerializable(ARG_ALLMANDILIST, mandiList);
        args.putDouble(ARG_lAT, lat);
        args.putDouble(ARG_LNG, lng);
        args.putBoolean("fromProfile", fromprofile);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fromOeProfile = getArguments().getBoolean(ARG_FROM_OE_PROFILE);
            fromProfile = getArguments().getBoolean("fromProfile");

            if (fromProfile || fromOeProfile)
                mandiListfromPrev = getArguments().getStringArrayList(ARG_INTERESTED_MANDIS_ID);

            mNearestMandiId = getArguments().getString(ARG_NEAREST_MANDIS_ID);

            mUserLat = getArguments().getDouble(ARG_lAT);
            mUserLng = getArguments().getDouble(ARG_LNG);

            Log.d(TAG, "onCreate: " + mUserLat + " " + mUserLng);
        }

        mMandiListbyDistance = new ArrayList<>();
        listOfNearestMandiNames = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info_mandi_interested, container, false);

        //mMandiList = (List<MandiModel>) getArguments().getSerializable(ARG_ALLMANDILIST);

        if (mUserLat == 0) mUserLat = 28.7041;   //Delhi
        if (mUserLng == 0) mUserLng = 77.1025;   //Delhi

        listOfNearestMandiNames = new ArrayList<>();

        stringUtils = AppController.getInstance().getStringUtils();

        mUserProfile = new UserProfile(getContext());

        ((TextView) view.findViewById(R.id.title_1)).setText(stringUtils.getLocalizedString(R.string.select_the_mandies_you_re_interested_in));
        if (fromProfile || fromOeProfile) {
            ((AppCompatButton) view.findViewById(R.id.info_next)).setText(stringUtils.getLocalizedString(R.string.submit));
            ((AppCompatButton) view.findViewById(R.id.info_next)).setBackgroundColor(getResources().getColor(R.color.colorPrimaryFarmer));
        } else
            ((AppCompatButton) view.findViewById(R.id.info_next)).setText(stringUtils.getLocalizedString(R.string.next));

        mBtnBack = view.findViewById(R.id.info_back);

        mBtnNext = view.findViewById(R.id.info_next);

        mAcMandiName = view.findViewById(R.id.info_mandi_choose);
        mAcMandiName.setHint(stringUtils.getLocalizedString(R.string.choose_mandi));

        mChipGroup = view.findViewById(R.id.info_mandi_chips);

        mScrollView = view.findViewById(R.id.info_mandi_scroll_view);
        dialog = new ProgressDialog(getActivity());

        getMandies();
        return view;
    }

    private void setAdapter(List<MandiModel> mandies) {

        List<String> mandiNames = MandiModel.getMandiCityNames(getContext(), mandies);
        CustomStringArrayAdapter mandiAdapter = new CustomStringArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, mandiNames);
        mAcMandiName.setAdapter(mandiAdapter);
    }

    private void getMandies() {
        dialog.show();
        MandiDataService mandiDataService = new MandiDataService(getContext());
        mandiDataService.getMandis(new MandiDataService.OnMandiResponse() {
            @Override
            public void onMandiSuccess(List<MandiModel> mandies) {
                dialog.dismiss();
                InfoMandiInterestedFragment.this.mMandiList = mandies;
                setAdapter(mandies);
                setupMandis();
            }

            @Override
            public void onMandiFailure() {
                Toast.makeText(getActivity(), "Can't load Mandies", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    private void setupMandis() {
        if (!mandiSetupFinished && mGoogleMap != null && mMandiList != null) {
            mandiSetupFinished = true;
            for (MandiModel model : mMandiList) {
                double distance = FindDistance(Double.parseDouble(model.mandi_lat), Double.parseDouble(model.mandi_lng), mUserLat, mUserLng);
                NearestMandiModel mandiToAdd = new NearestMandiModel(model.mandi_id, model.mandi_lat, model.mandi_lng, model.mandi_name_en, distance);
                mMandiListbyDistance.add(mandiToAdd);
            }

            Collections.sort(mMandiListbyDistance);

            for (NearestMandiModel model : mMandiListbyDistance) {
                mGoogleMap.addMarker(
                        new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(model.mandi_lat), Double.parseDouble(model.mandi_lng)))
                                .title(model.mandi_name_en + " [" + (int) (model.mandi_distance_from_user * 111) + " km]").icon(getMarkerIcon(lightred))
                );
                listOfNearestMandiNames.add(model.mandi_name_en + " [" + (int) (model.mandi_distance_from_user * 111) + " km]");
            }

            mlistOfNearestMandiNamesArray = (String[]) listOfNearestMandiNames.toArray(new String[0]);
            listOfNearestMandiNamesSize = mlistOfNearestMandiNamesArray.length;

            CustomStringArrayAdapter nearestMandiAdapter = new CustomStringArrayAdapter(getContext(),
                    android.R.layout.simple_dropdown_item_1line, listOfNearestMandiNames);
            mAcMandiName.setAdapter(nearestMandiAdapter);

            if (fromProfile || fromOeProfile) {
                String[] listForChipGroupTitle = new String[mItems.size()];
                Double[] listForChipGroupLat = new Double[mItems.size()];
                Double[] listForChipGroupLng = new Double[mItems.size()];
                for (int i = 0; i < mItems.size(); i++) {
                    //Chip tempChip = (Chip) mChipGroup.getChildAt(i);
                    //String tempStr =
                    matchNamesForId(mItems.get(i));
                    matchNamesKMTitle();

                    listForChipGroupTitle[i] = mNewTitle;
                    listForChipGroupLat[i] = mNewLat;
                    listForChipGroupLng[i] = mNewLng;
                }

                mChipGroup.removeAllViews();
                mSelectedMandiesIds.clear();

                for (int i = 0; i < listForChipGroupTitle.length; i++) {
                    if (TextUtils.isEmpty(listForChipGroupTitle[i].trim())) continue;
                    Chip chip = new Chip(getContext());
                    chip.setCheckable(false);
                    chip.setChipMinHeight(dpToPx(22));
                    chip.setCloseIconStartPadding(0);
                    chip.setCloseIconEndPadding(-12);
                    chip.setTextStartPadding(0);
                    chip.setTextEndPadding(0);
                    //chip.setWidth(dpToPx(250));
                    //chip.setMaxWidth(dpToPx(100));
                    //chip.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                    chip.setText(listForChipGroupTitle[i]);
                    chip.setTextColor(getResources().getColor(R.color.white));
                    if (fromProfile || fromOeProfile)
                        chip.setChipBackgroundColorResource(R.color.colorPrimaryFarmer);
                    else
                        chip.setChipBackgroundColorResource(R.color.loginBtn1);
                    chip.setCloseIconEnabled(true);
                    chip.setCloseIconResource(R.drawable.ic_cancel_black_24dp);

                    chip.setOnCloseIconClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Chip item = (Chip) v;
                            mSelectedMandiesIds.remove(getIdbyNameAndKM(item.getText().toString()));
                            mChipGroup.removeView(v);
                            String temp = item.getText().toString();
                            matchNamesDistance(temp);
                            mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mNewLat, mNewLng)).title(mNewTitle).icon(getMarkerIcon(lightred)));
                        }
                    });
                    mChipGroup.addView(chip);
                    mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(listForChipGroupLat[i], listForChipGroupLng[i]))
                            .title(listForChipGroupTitle[i]).icon(getMarkerIcon(green)));
                    mSelectedMandiesIds.add(getIdbyNameAndKM(listForChipGroupTitle[i]));
                }
            }

            //nearest mandi
            mCurLat = Double.parseDouble(mMandiListbyDistance.get(0).mandi_lat);
            mCurLng = Double.parseDouble(mMandiListbyDistance.get(0).mandi_lng);
            mCurTitle = mMandiListbyDistance.get(0).mandi_name_en + " [" + (int) (mMandiListbyDistance.get(0).mandi_distance_from_user * 111) + " km]";

            //mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mCurLat, mCurLng)).title(mCurTitle).icon(getMarkerIcon(green)));

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(mCurLat, mCurLng), 5);
            mGoogleMap.animateCamera(cameraUpdate);

            cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(mCurLat, mCurLng), 8);
            mGoogleMap.animateCamera(cameraUpdate);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_choose_mandi);
        mMapFragment.getMapAsync(this);

        mSelectedMandiesIds = new ArrayList<>();
        if (fromProfile || fromOeProfile) {
            mItems = Arrays.asList(mandiListfromPrev.toString().replace("[", "").replace("]", "").split("\\s*,\\s*"));
            //for (String item : items) {
            //    if (TextUtils.isEmpty(item.trim())) continue;
            //    addCropChip(item);
            //}
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        //LatLng LatLngNewDelhi = new LatLng(28.7041, 77.1025);
        LatLng LatLngUser = new LatLng(mUserLat, mUserLng);

        googleMap.setOnMarkerClickListener(this);

        mGoogleMap = googleMap;

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLngUser, 5));

        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);

        //USER LOCATION (don't remove)
        //mGoogleMap.addMarker(new MarkerOptions().position(LatLngUser).title("MY LOCATION").icon(getMarkerIcon(blue)));

        //Find Mandi distances from User
        setupMandis();
    }

    public double FindDistance(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
    }

    public BitmapDescriptor getMarkerIcon(String color) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAcMandiName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (hasFocus) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scrollViewToTop(v);

                        }
                    }, 500);
                }
            }
        });

        mAcMandiName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (v.hasFocus()) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scrollViewToTop(v);

                        }
                    }, 500);
                }
            }
        });

        mAcMandiName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = (String) parent.getItemAtPosition(position);
                if (mSelectedMandiesIds.size() < MAX_SELECTION) {

                    //String tempTitle = name;
                    matchNamesDistance(name);

                    //String tempId = getIdbyNameAndKM(name);

                    addCropChip(name);

                    mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mNewLat, mNewLng)).title(mNewTitle).icon(getMarkerIcon(green)));

                    CameraUpdate cameraUpdate = CameraUpdateFactory
                            .newLatLngZoom(new LatLng(mNewLat, mNewLng), 8);
                    mGoogleMap.animateCamera(cameraUpdate);

                    mCurLat = mNewLat;
                    mCurLng = mNewLng;
                    mCurTitle = mNewTitle;

                } else {
                    Toast.makeText(getContext(), stringUtils.getLocalizedString(R.string.cannot_select_more_than, true).toString()
                            + " " + MAX_SELECTION + " " + stringUtils.getLocalizedString(R.string.mandi, true), Toast.LENGTH_SHORT).show();
                }
                mAcMandiName.getText().clear();
            }
        });

        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fromProfile) {
                    if (mSelectedMandiesIds.size() < MIN_SELECTION) {
                        Toast.makeText(getContext(), "select minimum " + MIN_SELECTION + " mandies", Toast.LENGTH_SHORT).show();
                    } else {
                        updateProfile();
                    }
                } else if (fromOeProfile) {
                    List<String> extractedList = new ArrayList<>();
                    for (String tempStr : mSelectedMandiesIds) {
                        extractedList.add(tempStr.trim());
                    }
                    mListener.onInfoMandiInterestedSuccess(mNearestMandiId, new ArrayList<>(extractedList));

                    if (getActivity() != null)
                        getActivity().getSupportFragmentManager().popBackStack();
                } else {
                    if (mSelectedMandiesIds.size() < MIN_SELECTION) {
                        Toast.makeText(getContext(), "select minimum " + MIN_SELECTION + " mandies", Toast.LENGTH_SHORT).show();

                        //} else if (TextUtils.isEmpty(mChip.getText().toString())) {
                        //    Toast.makeText(getContext(), stringUtils.getLocalizedString(R.string.select_nearest_mandi), Toast.LENGTH_SHORT).show();

                    } else {
                        List<String> extractedList = new ArrayList<>();
                        for (String tempStr : mSelectedMandiesIds) {
                            extractedList.add(tempStr.trim());
                        }
                        mListener.onInfoMandiInterestedSuccess(mNearestMandiId, new ArrayList<>(extractedList));
                    }
                }
            }
        });
    }

    private void updateProfile() {
      /*  StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_FARMER_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("update_farmer_profile_response: " + response);
                        try {
                            JSONArray responseArray = new JSONArray(response);
                            JSONObject responseObject = responseArray.getJSONObject(0);
                            int errorCode = responseObject.getInt("error_code");
                            if (errorCode == 100) {
                                Toast.makeText(getActivity(), stringUtils.getLocalizedString(R.string.successfully_updated), Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getActivity(), FarmerProfileActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            } else {
                                Toast.makeText(getActivity(), stringUtils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showToast(getActivity(), stringUtils.getLocalizedString(R.string.slow_internet_kindly_check_internet_to_proceed_seamlessly).toString());
                        } else if (error instanceof NoConnectionError) {
                            showToast(getActivity(), stringUtils.getLocalizedString(R.string.no_internet_connection_try_later).toString());
                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("api_token", mUserProfile.getApiToken());

                //Mandatory
                params.put("user_id", stringUtils.getPref().getUserId());
                params.put("password", stringUtils.getPref().getUserPassword());

                //Optional
                JSONArray jsonArray = new JSONArray();

                List<String> extractedList = new ArrayList();
                for (String tempStr : mSelectedMandiesIds) {
                    extractedList.add(tempStr.trim());
                }
                //Set<String> hs = new HashSet<>();
                //hs.addAll(extractedList);
                //extractedList.clear();
                //extractedList.addAll(hs);
                for (String item : extractedList) {
                    Log.d("Mandi:   ", item);
                    //MandiModel mandiModel = MandiModel.getByMandiCity(getContext(), mMandiList, item);
                    //if (mandiModel != null)
                    //   jsonArray.put(mandiModel.mandi_name_en);
                    if (item != null) jsonArray.put(item);
                }

                params.put("interested_mandi_list", "" + jsonArray);

                //if (mChip != null && !TextUtils.isEmpty(mChip.getText().toString())) {
                //    params.put("nearest_mandi_name", mChip.getText().toString());
                //}

                System.out.println("update_farmer_profile_request: " + params.toString());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
*/

        Log.e("INTERESTED_MANDI", " ---> 1");
        Profile params=new Profile();
        Log.e("INTERESTED_MANDI", " ---> 2");
        params.setFarmerId(stringUtils.getPref().getUserId());
        Log.e("INTERESTED_MANDI", " ---> 3");
        params.setFarmerContact(stringUtils.getPref().getMobileNo());
        Log.e("INTERESTED_MANDI", " ---> 4");

        List<String> extractedList = new ArrayList();
        Log.e("INTERESTED_MANDI", " ---> 5");

        Log.e("INTERESTED_MANDI", " ---> " + "size - " + mSelectedMandiesIds.size());
        for (String tempStr : mSelectedMandiesIds) {
            Log.e("INTERESTED_MANDI", " ---> 6");
            extractedList.add(tempStr.trim());
        }

        JSONArray jsonArray = new JSONArray();
        Log.e("INTERESTED_MANDI", " ---> " + "size2 - " + extractedList.size());
        for (String item : extractedList) {
            Log.e("INTERESTED_MANDI", " ---> 7");
            Log.d("Mandi:   ", item);
            if (item != null) jsonArray.put(item);
        }
        Log.e("INTERESTED_MANDI", " ---> 8");
        params.setInterestedMandiList(extractedList);
        Log.e("INTERESTED_MANDI", " ---> 9");
        //if (mChip != null && !TextUtils.isEmpty(mChip.getText().toString())) {
        //    params.put("nearest_mandi_name", mChip.getText().toString());
        //}
        Log.e("INTERESTED_MANDI", " ---> 10");
        System.out.println("update_farmer_profile_request: " + params.toString());

        API_Manager apiManager=new API_Manager();
        UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);
        Log.e("INTERESTED_MANDI", " ---> 11");
        Call<UserUpdate> call=retrofit_interface.updateprofile(mUserProfile.getApiToken(),params);
        call.enqueue(new Callback<UserUpdate>() {
            @Override
            public void onResponse(Call<UserUpdate> call, retrofit2.Response<UserUpdate> response) {
                try {
                    Log.d("farmer_profile","mandi intrest response"+response.body());
                    if (response.body().getCode() == 200) {
                        Toast.makeText(getActivity(), stringUtils.getLocalizedString(R.string.successfully_updated), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getActivity(), FarmerProfileActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getActivity(), stringUtils.getLocalizedString(R.string.something_went_wrong_try_again), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<UserUpdate> call, Throwable t) {
                Log.d("farmer_profile","mandi intrest fail"+t.getMessage());
                if (t instanceof TimeoutError) {
                    showToast(getActivity(), stringUtils.getLocalizedString(R.string.slow_internet_kindly_check_internet_to_proceed_seamlessly).toString());
                } else if (t instanceof NoConnectionError) {
                    showToast(getActivity(), stringUtils.getLocalizedString(R.string.no_internet_connection_try_later).toString());
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if (context instanceof OnInfoMandiInteractionListener) {
                mListener = (OnInfoMandiInteractionListener) context;
            } else {
                throw new RuntimeException(context.toString()
                        + " must implement OnInfoLocationInteractionListener");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void addCropChip(String value) {
        String valueId = getIdbyNameAndKM(value);
        if (!mSelectedMandiesIds.contains(valueId)) {
            mSelectedMandiesIds.add(valueId);
            Log.e("INTERESTED_MANDI", " ---> " + mSelectedMandiesIds);

            Chip chip = new Chip(getContext());

            chip.setCheckable(false);
            chip.setChipMinHeight(dpToPx(22));
            chip.setCloseIconStartPadding(0);
            chip.setCloseIconEndPadding(-12);
            chip.setTextStartPadding(0);
            chip.setTextEndPadding(0);
            chip.setText(value);
            chip.setTextColor(getResources().getColor(R.color.white));
            if (fromProfile || fromOeProfile)
                chip.setChipBackgroundColorResource(R.color.colorPrimaryFarmer);
            else
                chip.setChipBackgroundColorResource(R.color.loginBtn1);
            chip.setCloseIconEnabled(true);
            chip.setCloseIconResource(R.drawable.ic_cancel_black_24dp);

            chip.setOnCloseIconClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Chip item = (Chip) v;
                    mChipGroup.removeView(v);
                    mSelectedMandiesIds.remove(getIdbyNameAndKM(item.getText().toString()));
                    String temp = item.getText().toString();
                    matchNamesDistance(temp);
                    mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mNewLat, mNewLng)).title(mNewTitle).icon(getMarkerIcon(lightred)));
                }
            });

            mChipGroup.addView(chip);
        }
    }

    /*private void getMandies() {
        new MandiDataService(getActivity(), new MandiDataService.OnMandiResponse() {
            @Override
            public void onMandiSuccess(List<MandiModel> mandies) {
                mMandiList = mandies;
            }

            @Override
            public void onMandiFailure() {

            }
        });
    }*/

    // not working
    public void scrollViewToTop(View view) {
        int[] xy = new int[2];
        view.getLocationOnScreen(xy);
        int initialY = xy[1];

        int[] xyBack = new int[2];
        mBtnBack.getLocationOnScreen(xyBack);
        int finalY = xyBack[1] + mBtnBack.getBottom();

        int scrollY = initialY - finalY - dpToPx(12);

        mScrollView.smoothScrollTo(0, scrollY);
    }

    public int dpToPx(int dp) {
        if (getContext() == null) return 0;
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    public void matchNamesKMTitle() {
        for (String tempStr : mlistOfNearestMandiNamesArray) {
            String[] extractname = tempStr.split(" \\[");
            if (extractname[0].equals(mNewTitle)) mNewTitle = tempStr;
        }
        return;
    }

    public void matchNamesForId(String idToMatch) {
        for (NearestMandiModel model : mMandiListbyDistance) {
            if ((model.mandi_id).equals(idToMatch)) {
                mNewLat = Double.parseDouble(model.mandi_lat);
                mNewLng = Double.parseDouble(model.mandi_lng);
                mNewTitle = model.mandi_name_en + " [" + (int) (model.mandi_distance_from_user * 111) + " km]";
            }
        }
        return;
    }

    public void matchNamesDistance(String tempStr) {
        for (NearestMandiModel model : mMandiListbyDistance) {
            if ((model.mandi_name_en + " [" + (int) (model.mandi_distance_from_user * 111) + " km]").equals(tempStr)) {
                mNewLat = Double.parseDouble(model.mandi_lat);
                mNewLng = Double.parseDouble(model.mandi_lng);
                mNewTitle = model.mandi_name_en + " [" + (int) (model.mandi_distance_from_user * 111) + " km]";
            }
        }
        return;
    }

    public String getIdbyNameAndKM(String tempStr) {
        String Id = "";
        for (NearestMandiModel model : mMandiListbyDistance) {
            if ((model.mandi_name_en + " [" + (int) (model.mandi_distance_from_user * 111) + " km]").equals(tempStr)) {
                Id = model.mandi_id;
            }
        }
        return Id;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        String tempTitle = marker.getTitle();
        if (tempTitle.equals("MY LOCATION")) {
            return true;
        }


        for (int i = 0; i < mChipGroup.getChildCount(); i++) {
            Chip tempChip = (Chip) mChipGroup.getChildAt(i);
            String tempStr = tempChip.getText().toString();
            //String[] extractname = tempStr.split(" \\[");

            if (tempTitle.equals(tempStr)) {
                matchNamesDistance(tempTitle);
                mChipGroup.removeView(tempChip);
                mSelectedMandiesIds.remove(getIdbyNameAndKM(tempTitle));
                mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mNewLat, mNewLng)).title(mNewTitle).icon(getMarkerIcon(lightred)));

                return true;
            }
        }


        if (mSelectedMandiesIds.size() >= MAX_SELECTION) {
            Toast.makeText(getContext(), stringUtils.getLocalizedString(R.string.cannot_select_more_than, true).toString()
                    + " " + MAX_SELECTION + " " + stringUtils.getLocalizedString(R.string.mandi, true), Toast.LENGTH_SHORT).show();
            return true;
        }


        matchNamesDistance(tempTitle);
        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mNewLat, mNewLng)).title(mNewTitle).icon(getMarkerIcon(green)));
        matchNamesKMTitle();

        String tempMandiId = getIdbyNameAndKM(mNewTitle);
        mSelectedMandiesIds.add(tempMandiId);
        Log.e("INTERESTED_MANDI", " ---> " + mSelectedMandiesIds);

        Chip chip = new Chip(getContext());

        chip.setCheckable(false);
        chip.setChipMinHeight(dpToPx(22));
        chip.setCloseIconStartPadding(0);
        chip.setCloseIconEndPadding(-12);
        chip.setTextStartPadding(0);
        chip.setTextEndPadding(0);
        chip.setText(mNewTitle);
        chip.setTextColor(getResources().getColor(R.color.white));
        if (fromProfile || fromOeProfile)
            chip.setChipBackgroundColorResource(R.color.colorPrimaryFarmer);
        else
            chip.setChipBackgroundColorResource(R.color.loginBtn1);
        chip.setCloseIconEnabled(true);
        chip.setCloseIconResource(R.drawable.ic_cancel_black_24dp);

        chip.setOnCloseIconClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Chip item = (Chip) v;
                //mSelectedMandies.remove(item.getText().toString());
                mSelectedMandiesIds.remove(getIdbyNameAndKM(item.getText().toString()));
                mChipGroup.removeView(v);
                String temp = item.getText().toString();
                matchNamesDistance(temp);
                mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mNewLat, mNewLng)).title(mNewTitle).icon(getMarkerIcon(lightred)));
            }
        });

        mChipGroup.addView(chip);
        //------------------

        CameraUpdate cameraUpdate = CameraUpdateFactory
                .newLatLngZoom(new LatLng(mNewLat, mNewLng), 8);
        mGoogleMap.animateCamera(cameraUpdate);

        mCurLat = mNewLat;
        mCurLng = mNewLng;
        mCurTitle = mNewTitle;

        return true;
    }


    public interface OnInfoMandiInteractionListener {
        void onInfoMandiInterestedSuccess(String nearestMandi, List<String> selectedMandies);
    }
}
