package com.agriiprince.mvvm.ui.splash;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.agriiprince.BuildConfig;
import com.agriiprince.R;
import com.agriiprince.mvvm.retrofit.dto.apkversions.ApkVers;
import com.agriiprince.mvvm.ui.homescreen.FarmerHomeScreenActivity;
import com.agriiprince.mvvm.ui.oe.OE_MainActivity;
import com.agriiprince.databinding.ActivitySplashBinding;
import com.agriiprince.dataservice.SubscriptionDataService;
import com.agriiprince.mvvm.retrofit.dto.login.TokenGenerate;
import com.agriiprince.mvvm.model.User;
import com.agriiprince.mvvm.ui.LiveDataObserver;
import com.agriiprince.mvvm.ui.base.BaseActivity;
import com.agriiprince.mvvm.ui.language.LanguageActivity;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.mvvm.util.PermissionHelper;
import com.agriiprince.mvvm.viewmodel.LoginViewModel;
import com.agriiprince.mvvm.viewmodel.LoginViewModelUseCase;
import com.agriiprince.mvvm.viewmodel.VersionUpdateViewModel;
import com.agriiprince.mvvm.viewmodel.VersionUpdateViewModelUseCase;
import com.gun0912.tedpermission.PermissionListener;

import java.util.List;

import javax.inject.Inject;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.Manifest.permission.GET_ACCOUNTS;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.SEND_SMS;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.agriiprince.mvvm.model.User.Type.COMMISSION_AGENT;
import static com.agriiprince.mvvm.model.User.Type.FARMER;
import static com.agriiprince.mvvm.model.User.Type.FORWARDING_AGENT;
import static com.agriiprince.mvvm.model.User.Type.OPERATIVE_EXECUTIVE;
import static com.agriiprince.mvvm.model.User.Type.PESTICIDE_VENDOR;
import static com.agriiprince.mvvm.model.User.Type.TRADER;
import static com.agriiprince.mvvm.model.User.Type.TRUCKER_SHIPPER;

public class SplashActivity extends BaseActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();

    @Inject
    User mUser;

    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    private Animation mFadeIn;
    private Animation mFadeOut;

    private VersionUpdateViewModelUseCase mUpdateViewModel;

    private LoginViewModelUseCase mLoginViewModel;

    private ActivitySplashBinding mBinding;

    @Override
    protected void setViewAndPerformDataBinding() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
    }

    @Override
    protected void initControl() {
        mFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        mFadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);

        mUpdateViewModel = ViewModelProviders.of(this, mViewModelFactory).get(VersionUpdateViewModel.class);
        mLoginViewModel = ViewModelProviders.of(this, mViewModelFactory).get(LoginViewModel.class);
        Logs.d(TAG, "initViewControl: " + mViewModelFactory + " " + mUpdateViewModel + " " + mLoginViewModel);
    }

    @Override
    protected void initViewControl() {

    }

    @Override
    protected void initTextViews() {
        setBilingualText(mBinding.tagLine, R.string.splash_text);
    }

    @Override
    protected void initListener() {
        PermissionHelper.requestPermissions(this, permissionlistener,
                getBilingualString(R.string.permissions),
                getBilingualString(R.string.you_need_to_allow_following_permissions_for_this_app_to_work_properly),
                getBilingualString(R.string.permission_denied),
                getBilingualString(R.string.permission_denied_message),
                getBilingualString(R.string.permissions),
                new String[] {INTERNET, ACCESS_NETWORK_STATE, WRITE_EXTERNAL_STORAGE, READ_CONTACTS,
                        READ_PHONE_STATE, SEND_SMS, RECEIVE_SMS, GET_ACCOUNTS, ACCESS_COARSE_LOCATION,
                        ACCESS_FINE_LOCATION});

    }

    @Override
    protected void onResume() {
        super.onResume();
        startAnimation(); // start the animation on resume
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopAnimation(); // stop the animation on pause to avoid the crash and leaks
    }


    /**
     * add listeners to the animation to start other after finishing one.
     * Then starts the animation on logo image
     */
    private void startAnimation() {
        mFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { }

            @Override
            public void onAnimationEnd(Animation animation) {
                mBinding.logo.startAnimation(mFadeOut); // start fade out animation
            }

            @Override
            public void onAnimationRepeat(Animation animation) { }
        });

        mFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { }

            @Override
            public void onAnimationEnd(Animation animation) {
                mBinding.logo.startAnimation(mFadeIn); // start fade in animation
            }

            @Override
            public void onAnimationRepeat(Animation animation) { }
        });

        mBinding.logo.startAnimation(mFadeIn); // start with fade in animation
    }

    /**
     * stop the animation of logo and remove the listeners
     */
    private void stopAnimation() {
        mBinding.logo.clearAnimation();
        mFadeIn.setAnimationListener(null);
        mFadeOut.setAnimationListener(null);
    }
    
    private PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            checkForUpdate(); // check for updates
        }

        @Override
        public void onPermissionDenied(List<String> deniedPermissions) {
            Toast.makeText(SplashActivity.this, "Permission Denied\n" + deniedPermissions.toString(),
                    Toast.LENGTH_SHORT).show();
            SplashActivity.this.finish();
        }
    };

    /**
     * checks for any updates by calling the server.
     * if any update available then
     */
    private void checkForUpdate() {
        mUpdateViewModel.checkForUpdate(BuildConfig.VERSION_NAME, BuildConfig.USER_TYPE)
                .observe(this, new LiveDataObserver<ApkVers>() {
                    @Override
                    protected void onNetworkFail() {
                        Logs.d(TAG, "onNetworkFail: ");
                        checkLoggedUser();
                    }

                    @Override
                    protected void onLoading() {
                        Logs.d(TAG, "onLoading: ");
                    }

                    @Override
                    protected void onSuccess(ApkVers data) {
                        Logs.d("ApkVers", "APK onSuccess: " + data);
                        //if (getStatusCode() == 100)
                        if (getStatusCode() == 200)
                        {
                            //onUpdateAvailable(data.getUrl().get(0));
                            onUpdateAvailable(data.getData().getLink());
                        } else {
                            checkLoggedUser();
                        }
                    }

                    @Override
                    protected void onFail(Throwable t) {
                        Logs.d(TAG, "onFail: " + t);
                    }
                });
    }

    /**
     * ask for user permission to download the update and then install
     * @param url update url
     */
    private void onUpdateAvailable(String url) {
        mUpdateViewModel.onDownloadUpdateProgress()
                .observe(this, new Observer<String>() {
                    @Override
                    public void onChanged(@Nullable String s) {

                    }
                });

        mUpdateViewModel.onDownloadUpdate(url)
                .observe(this, new Observer<String>() {
                    @Override
                    public void onChanged(@Nullable String s) {

                    }
                });
    }

    /**
     * checks if user is logged in
     * If user is logged in then access token is updated
     * else then user is taken to language selection screen
     */
    private void checkLoggedUser() {
        if (mUser.isLogged()) {
            updateAccessToken();
        } else {
            startLanguageSelectionActivity();
        }
    }

    /**
     * start language selection activity and finish the splash activity
     */
    private void startLanguageSelectionActivity() {
        startActivity(LanguageActivity.newIntent(this));
        finish();
    }

    /**
     * method to update the access token for logged user
     */
    private void updateAccessToken() {
        mLoginViewModel.generateToken()
                .observe(this, new LiveDataObserver<TokenGenerate>() {
                    @Override
                    protected void onLoading() {

                    }

                    @Override
                    protected void onSuccess(TokenGenerate data) {
                        // TODO remove
                        //if (BuildConfig.DEBUG) {
                        //    startActivity(SubscriptionActivity.newIntent(SplashActivity.this));
                        //    finish();
                        //} else
                            startHomeActivity();
                    }

                    @Override
                    protected void onFail(Throwable t) {
                        startHomeActivity();
                    }

                    @Override
                    protected void onNetworkFail() {
                        startHomeActivity();
                    }
                });
    }

    /**
     * on login success start respective home activities
     */
    private void startHomeActivity() {
        switch (mUser.getUserType()) {
            case COMMISSION_AGENT:
                //go to respective activities
                break;

            case FARMER:
                SubscriptionDataService dataService = new SubscriptionDataService(this);
                dataService.saveSubscriptionStatus();
                startActivity(new Intent(this, FarmerHomeScreenActivity.class));
                break;

            case TRUCKER_SHIPPER:
                //go to respective activities
                break;

            case FORWARDING_AGENT:
                //go to respective activities
                break;

            case OPERATIVE_EXECUTIVE:
                startActivity(new Intent(this, OE_MainActivity.class));
                break;

            case TRADER:
                //go to respective activities
                break;

            case PESTICIDE_VENDOR:
                //go to respective activities
                break;
        }
        finish();
    }
}
