package com.agriiprince.mvvm.ui.disease;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import com.agriiprince.activities.TutorialActivity;
import com.agriiprince.db.DatabaseManager;
import com.agriiprince.R;
import com.agriiprince.adapter.CustomStringArrayAdapter;
import com.agriiprince.mvvm.applevel.analytics.FarmerRealtimeDBAnalytics;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.ActivityCropDiseasesCroplistBinding;
import com.agriiprince.dataservice.DataResponse;
import com.agriiprince.dataservice.DiseaseCropNamesDataService;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.models.DiseaseCropListModel;
import com.agriiprince.mvvm.model.DiseaseIdNameModel;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.Utils;
import com.android.volley.VolleyError;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class DiseaseCropNamesActivity extends AppCompatActivity {

    private final String TAG = DiseaseCropNamesActivity.class.getSimpleName();

    private ProgressDialog mProgressDialog;

    private UserProfile mUserProfile;
    FirebaseDatabase mFDatabase;
    DatabaseReference mRef;
    DatabaseReference mFarmersRef;
    DatabaseReference mNumRef;
    DatabaseReference mKeyRef;
    long mTimeStart;
    String mUserId;

    private StringUtils stringUtils;

    private List<String> cropList;

    private List<DiseaseCropListModel> mList;

    private PrefManager prefManager;

    private CustomStringArrayAdapter adapter;

    private ActivityCropDiseasesCroplistBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_crop_diseases_croplist);

        stringUtils = AppController.getInstance().getStringUtils();
        mUserProfile = new UserProfile(this);
        mUserId = mUserProfile.getUserId();
        cropList = new ArrayList<>();
        mList = new ArrayList<>();

        mFDatabase = FirebaseDatabase.getInstance();
        mRef = mFDatabase.getReference("analytics");
        mFarmersRef = mRef.child("farmers");
        mNumRef = mFarmersRef.child(mUserProfile.getUserMobile());

        prefManager = new PrefManager(this);

        adapter = new CustomStringArrayAdapter(DiseaseCropNamesActivity.this, android.R.layout.simple_list_item_1, cropList);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        binding.listViewCropNames.setAdapter(adapter);

        binding.listViewCropNames.setTextFilterEnabled(true);
        binding.listViewCropNames.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String entry = (String) parent.getAdapter().getItem(position);
//                showLog(entry);

                for (DiseaseCropListModel model : mList) {
                    if (stringUtils.getBilingualStringForApi(model.getCrop_name(), model.getCrop_name_bi()).contains(entry)) {
                        entry = model.getCrop_name();
                        Log.d(TAG, entry);
                        break;
                    }
                }

                Intent intent = new Intent(DiseaseCropNamesActivity.this, DiseaseListActivity.class);
                intent.putExtra("cropName", entry);
                startActivity(intent);
            }
        });

        ImageView imgview_help =  findViewById(R.id.iv_help);

        imgview_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DiseaseCropNamesActivity.this, TutorialActivity.class);
                intent.putExtra("code", "DISED");
                startActivity(intent);
            }
        });


        showProgressDialog();
        DatabaseManager databaseManager = AppController.getInstance().getDataBase();
        DiseaseCropNamesDataService diseaseCropNamesDataService = new DiseaseCropNamesDataService(new PrefManager(this), databaseManager);
        diseaseCropNamesDataService.getCropNames(mUserProfile.getApiToken(), mUserProfile.getUserId(),
                mUserProfile.getUserType(), diseaseCropResponse);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_disease_crop_list, menu);

        MenuItem searchItem = menu.findItem( R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(onQueryTextListener);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private SearchView.OnQueryTextListener onQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            adapter.getFilter().filter(s);
            return false;
        }
    };

    private void setCropListAdapter(List<DiseaseCropListModel> list) {
        for (DiseaseCropListModel model : list) {
            if (TextUtils.isEmpty(model.getCrop_name().trim()))
                continue;

            Log.d("TESTING_CROPLIST", model.toString());
            cropList.add(stringUtils.getBilingualStringForApi(model.getCrop_name(), model.getCrop_name_bi()));
            //Log.d(TAG, model.getCrop_name());
        }

        cropList.remove(cropList.size()-1);

        adapter.addAllString(cropList);
        adapter.notifyDataSetChanged();
    }

    private DataResponse<List<DiseaseCropListModel>> diseaseCropResponse = new DataResponse<List<DiseaseCropListModel>>() {
        @Override
        public void onSuccessResponse(List<DiseaseCropListModel> response) {
            dismissProgressDialog();

            mList = response;
            // Remove duplicates
            //LinkedHashSet<DiseaseCropListModel> hashSet = new LinkedHashSet<>(mList);
            //ArrayList<DiseaseCropListModel> listWithoutDuplicates = new ArrayList<>(hashSet);

            List<DiseaseIdNameModel> diseaseIdNameModelList = new ArrayList<>();

            /*for (DiseaseCropListModel d : mList) {
                DiseaseIdNameModel dm = new DiseaseIdNameModel();
                dm.setDisease_id(d.getDiseaseId());
                dm.setDisease_name(d.getCrop_name());
                diseaseIdNameModelList.add();
            }*/

            List<DiseaseCropListModel> allEvents = mList;
            List<DiseaseCropListModel> noRepeat = new ArrayList<DiseaseCropListModel>();

            for (DiseaseCropListModel event : allEvents) {
                boolean isFound = false;
                // check if the event name exists in noRepeat
                for (DiseaseCropListModel e : noRepeat) {
                    if (e.getCrop_name().equals(event.getCrop_name()) || (e.equals(event))) {
                        isFound = true;
                        break;
                    }
                }
                if (!isFound) noRepeat.add(event);
            }

            setCropListAdapter(noRepeat);
        }

        @Override
        public void onParseError() {
            dismissProgressDialog();
            Toast.makeText(DiseaseCropNamesActivity.this,
                    stringUtils.getLocalizedString(R.string.something_went_wrong_try_again),
                    Toast.LENGTH_LONG).show();
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            dismissProgressDialog();
            Utils.showVolleyError(DiseaseCropNamesActivity.this, error);

        }
    };


    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.please_wait));
        }

        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }
    @Override
    protected void onResume() {
        super.onResume();
        mTimeStart = System.currentTimeMillis();
    }

    @Override
    protected void onStop() {
        super.onStop();
        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - mTimeStart;
        final int elapsedSeconds = (int)(tDelta / 1000.0);

        Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    FarmerRealtimeDBAnalytics.saveEventFarmerTimeSpent(
                            dataSnapshot, "time_spent_crop_disease", mNumRef, elapsedSeconds);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

}


