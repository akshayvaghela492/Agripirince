package com.agriiprince.mvvm.ui.customwidgets;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Manu Sajjan on 13-12-2018.
 */
public class WrapViewPager extends ViewPager {

    private int MAX_VIEW_HEIGHT;

    public WrapViewPager(@NonNull Context context) {
        super(context);
    }

    public WrapViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        View child = getChildAt(0);
        if (child != null) {
            child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            int h = child.getMeasuredHeight();
            if (h > MAX_VIEW_HEIGHT) {
                MAX_VIEW_HEIGHT = h;
            }
        }
        if (MAX_VIEW_HEIGHT != 0) {
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(MAX_VIEW_HEIGHT, MeasureSpec.EXACTLY);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}