package com.agriiprince.mvvm.retrofit.model.oe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OEupdateprice implements Serializable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("reported_by_user_type")
    @Expose
    private String reportedByUserType;
    @SerializedName("reported_by_user")
    @Expose
    private String reportedByUser;
    @SerializedName("list_arrival_date")
    @Expose
    private String listArrivalDate;
    @SerializedName("commodity_state")
    @Expose
    private String commodityState;
    @SerializedName("commodity_district")
    @Expose
    private String commodityDistrict;
    @SerializedName("commodity_grade")
    @Expose
    private String commodityGrade;
    @SerializedName("commodity_market")
    @Expose
    private String commodityMarket;
    @SerializedName("commodity_name")
    @Expose
    private String commodityName;
    @SerializedName("commodity_variety")
    @Expose
    private String commodityVariety;
    @SerializedName("min_price")
    @Expose
    private String minPrice;
    @SerializedName("max_price")
    @Expose
    private String maxPrice;
    @SerializedName("modal_price")
    @Expose
    private String modalPrice;
    @SerializedName("general_price")
    @Expose
    private String generalPrice;
    @SerializedName("reported_from")
    @Expose
    private String reportedFrom;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("updated_on")
    @Expose
    private String updatedOn;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReportedByUserType() {
        return reportedByUserType;
    }

    public void setReportedByUserType(String reportedByUserType) {
        this.reportedByUserType = reportedByUserType;
    }

    public String getReportedByUser() {
        return reportedByUser;
    }

    public void setReportedByUser(String reportedByUser) {
        this.reportedByUser = reportedByUser;
    }

    public String getListArrivalDate() {
        return listArrivalDate;
    }

    public void setListArrivalDate(String listArrivalDate) {
        this.listArrivalDate = listArrivalDate;
    }

    public String getCommodityState() {
        return commodityState;
    }

    public void setCommodityState(String commodityState) {
        this.commodityState = commodityState;
    }

    public String getCommodityDistrict() {
        return commodityDistrict;
    }

    public void setCommodityDistrict(String commodityDistrict) {
        this.commodityDistrict = commodityDistrict;
    }

    public String getCommodityGrade() {
        return commodityGrade;
    }

    public void setCommodityGrade(String commodityGrade) {
        this.commodityGrade = commodityGrade;
    }

    public String getCommodityMarket() {
        return commodityMarket;
    }

    public void setCommodityMarket(String commodityMarket) {
        this.commodityMarket = commodityMarket;
    }

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    public String getCommodityVariety() {
        return commodityVariety;
    }

    public void setCommodityVariety(String commodityVariety) {
        this.commodityVariety = commodityVariety;
    }

    public String getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }

    public String getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getModalPrice() {
        return modalPrice;
    }

    public void setModalPrice(String modalPrice) {
        this.modalPrice = modalPrice;
    }

    public String getGeneralPrice() {
        return generalPrice;
    }

    public void setGeneralPrice(String generalPrice) {
        this.generalPrice = generalPrice;
    }

    public String getReportedFrom() {
        return reportedFrom;
    }

    public void setReportedFrom(String reportedFrom) {
        this.reportedFrom = reportedFrom;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

}