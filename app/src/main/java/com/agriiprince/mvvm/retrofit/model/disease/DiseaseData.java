package com.agriiprince.mvvm.retrofit.model.disease;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DiseaseData implements Serializable {
    @SerializedName("locale")
    @Expose
    private String locale;
    @SerializedName("data")
    @Expose
    private List<DiseaseDetailModel> data;

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public List<DiseaseDetailModel> getData() {
        return data;
    }

    public void setData(List<DiseaseDetailModel> data) {
        this.data = data;
    }
}
