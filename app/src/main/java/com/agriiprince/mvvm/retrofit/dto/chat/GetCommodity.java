package com.agriiprince.mvvm.retrofit.dto.chat;

import com.agriiprince.mvvm.retrofit.model.chat.Commodity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetCommodity implements Serializable {
    @SerializedName("status")
@Expose
private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("data")
    @Expose
    private List<Commodity> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<Commodity> getData() {
        return data;
    }

    public void setData(List<Commodity> data) {
        this.data = data;
    }


}
