package com.agriiprince.mvvm.retrofit.model.disease;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DiseaseDetailModel implements Serializable
{
    @SerializedName("id")
    @Expose
    private Integer Id;
    @SerializedName("crop_name")
    @Expose
    private String cropName;
    @SerializedName("crop_diseases")
    @Expose
    private String cropDiseases;
    @SerializedName("crop_causes")
    @Expose
    private String cropCauses;
    @SerializedName("crop_symptom")
    @Expose
    private String cropSymptom;
    @SerializedName("crop_diseases_scientific_name")
    @Expose
    private String cropDiseasesScientificName;
    @SerializedName("crop_diseases_category")
    @Expose
    private String cropDiseasesCategory;
    @SerializedName("crop_diseases_management")
    @Expose
    private String cropDiseasesManagement;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("crop_images")
    @Expose
    private String cropImages;

    public String getCropImages() {
        return cropImages;
    }

    public void setCropImages(String cropImages) {
        this.cropImages = cropImages;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        this.Id = id;
    }

    public String getCropName() {
        return cropName;
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public String getCropDiseases() {
        return cropDiseases;
    }

    public void setCropDiseases(String cropDiseases) {
        this.cropDiseases = cropDiseases;
    }

    public String getCropCauses() {
        return cropCauses;
    }

    public void setCropCauses(String cropCauses) {
        this.cropCauses = cropCauses;
    }

    public String getCropSymptom() {
        return cropSymptom;
    }

    public void setCropSymptom(String cropSymptom) {
        this.cropSymptom = cropSymptom;
    }

    public String getCropDiseasesScientificName() {
        return cropDiseasesScientificName;
    }

    public void setCropDiseasesScientificName(String cropDiseasesScientificName) {
        this.cropDiseasesScientificName = cropDiseasesScientificName;
    }

    public String getCropDiseasesCategory() {
        return cropDiseasesCategory;
    }

    public void setCropDiseasesCategory(String cropDiseasesCategory) {
        this.cropDiseasesCategory = cropDiseasesCategory;
    }



    public String getCropDiseasesManagement() {
        return cropDiseasesManagement;
    }

    public void setCropDiseasesManagement(String cropDiseasesManagement) {
        this.cropDiseasesManagement = cropDiseasesManagement;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
