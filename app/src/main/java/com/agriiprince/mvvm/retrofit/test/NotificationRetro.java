package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.notification.Notification;
import com.agriiprince.mvvm.retrofit.dto.notification.NotificationStatusUpdate;
import com.agriiprince.mvvm.retrofit.dto.notification.NotificationsResp;
import com.agriiprince.mvvm.util.Logs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationRetro {

API_Manager api_manager=new API_Manager();
    com.agriiprince.mvvm.retrofit.service.Notification retrofit_interface = api_manager.getClient7().create(com.agriiprince.mvvm.retrofit.service.Notification.class);

    public void getmsg() {
        Call<NotificationsResp> call = retrofit_interface.getmsg("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","5b41fff2d8","all","farmer");
        call.enqueue(new Callback<NotificationsResp>() {
            @Override
            public void onResponse(Call<NotificationsResp> call, Response<NotificationsResp> response) {
                Logs.d("CheckStatus", "onResponse: notification msg");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus", "onResponse: code "+response.body().getCode());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getData().getWeather().getLastUnread().getMessage());
                    Logs.d("CheckStatus",response.body().getData().getPersonal().getNotifications().get(1).getMessage());
                }
            }
            @Override
            public void onFailure(Call<NotificationsResp> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }
    public void updateStatus() {
        Call<NotificationStatusUpdate> call = retrofit_interface.updatestatus("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","5b41fff2d8", "farmer","single","1005");
        call.enqueue(new Callback<NotificationStatusUpdate>() {
            @Override
            public void onResponse(Call<NotificationStatusUpdate> call, Response<NotificationStatusUpdate> response) {
                Logs.d("CheckStatus", "onResponse: update status notification ");
                if (response.isSuccessful() && response.body() != null) {
                    try {
                        Logs.d("CheckStatus",response.body().getStatus());
                        Logs.d("CheckStatus",response.body().getMessage());
                        Logs.d("CheckStatus",response.body().getCode().toString());
                        Logs.d("CheckStatus",response.body().getData().getMessage());
                    }catch (Exception e){e.getMessage();}
                }
            }
            @Override
            public void onFailure(Call<NotificationStatusUpdate> call, Throwable t) {
                Log.d("CheckStatus", "update status notification "+t.getMessage());
            }
        });
    }
    public void userNotification() {
        Call<Notification> call = retrofit_interface.getUserNotification("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","5b41fff2d8");
        call.enqueue(new Callback<Notification>() {
            @Override
            public void onResponse(Call<Notification> call, Response<Notification> response) {
                Logs.d("CheckStatus", "onResponse: user notification");
                if (response.isSuccessful() && response.body() != null) {
                    try {
                        Logs.d("CheckStatus",response.body().getStatus());
                        Logs.d("CheckStatus",response.body().getMessage());
                        Logs.d("CheckStatus",response.body().getCode().toString());
                        Logs.d("CheckStatus",response.body().getData().get(1).getMessage());
                    }catch (Exception e){e.getMessage();}
                }
            }
            @Override
            public void onFailure(Call<Notification> call, Throwable t) {
                Log.d("CheckStatus", "user notification error "+t.getMessage());
            }
        });
    }
}
