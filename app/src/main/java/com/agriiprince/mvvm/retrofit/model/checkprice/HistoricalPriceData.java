package com.agriiprince.mvvm.retrofit.model.checkprice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class HistoricalPriceData implements Serializable {

    @SerializedName("arrival")
    @Expose
    private String arrival;
    @SerializedName("district")
    @Expose
    private String district;
    @SerializedName("market")
    @Expose
    private String market;
    @SerializedName("commodity")
    @Expose
    private String commodity;
    @SerializedName("variety")
    @Expose
    private String variety;
    @SerializedName("grade")
    @Expose
    private String grade;
    @SerializedName("state")
    @Expose
    private Object state;
    @SerializedName("minP")
    @Expose
    private Integer minP;
    @SerializedName("maxP")
    @Expose
    private Integer maxP;
    @SerializedName("modP")
    @Expose
    private Integer modP;
    @SerializedName("tonnage")
    @Expose
    private Double tonnage;


    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public Integer getMinP() {
        return minP;
    }

    public void setMinP(Integer minP) {
        this.minP = minP;
    }

    public Integer getMaxP() {
        return maxP;
    }

    public void setMaxP(Integer maxP) {
        this.maxP = maxP;
    }

    public Integer getModP() {
        return modP;
    }

    public void setModP(Integer modP) {
        this.modP = modP;
    }

    public Double getTonnage() {
        return tonnage;
    }

    public void setTonnage(Double tonnage) {
        this.tonnage = tonnage;
    }

}