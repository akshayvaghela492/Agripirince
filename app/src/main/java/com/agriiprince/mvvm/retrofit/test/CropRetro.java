package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.FarmerCrop;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.DeleteCrop;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.DeleteCropMedia;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.SaveCrop;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.SaveMedia;
import com.agriiprince.mvvm.retrofit.model.tradecrop.CropDetails;
import com.agriiprince.mvvm.retrofit.model.tradecrop.Media;
import com.agriiprince.mvvm.retrofit.service.CropInterface;
import com.agriiprince.mvvm.util.Logs;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CropRetro {
    API_Manager apiManager=new API_Manager();
    CropInterface retrofit_interface = apiManager.getClient2().create(CropInterface.class);

    public void getfarmercrop() {
        Call<FarmerCrop> call = retrofit_interface.getFarmercrop(
               "3","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY");
        call.enqueue(new Callback<FarmerCrop>() {
            @Override
            public void onResponse(Call<FarmerCrop> call, Response<FarmerCrop> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code getfarmercrop ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Log.d("CheckStatus", response.body().getMessage());
                    List<CropDetails> cropDetails=response.body().getData();
                    for(int i=0;i<cropDetails.size();i++)
                    {
                        Log.d("crop",cropDetails.get(i).getSowingCrop());
                    }
                } catch (Exception e)
                {e.getMessage();}
            }

            @Override
            public void onFailure(Call<FarmerCrop> call, Throwable t) {
                Log.d("CheckStatus", "farmer crop  "+ t.getMessage());
            }
        });
    }
    public void getmedia() {
        Call<com.agriiprince.mvvm.retrofit.dto.tradecrop.Media> call = retrofit_interface.getMedialist("17","5","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY");
        call.enqueue(new Callback<com.agriiprince.mvvm.retrofit.dto.tradecrop.Media>() {
            @Override
            public void onResponse(Call<com.agriiprince.mvvm.retrofit.dto.tradecrop.Media> call, Response<com.agriiprince.mvvm.retrofit.dto.tradecrop.Media> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code getMediaList ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Log.d("CheckStatus", response.body().getMessage());
                    List<Media> mediaList=response.body().getData();

                    for (int i=0;i<=mediaList.size();i++) {
                        String url = response.body().getMediaBaseUrl() + response.body().getData().get(i).getUrl();
                        Logs.d("CheckStatus", url);
                    }
                }
                catch (Exception e){
                    e.getMessage();
                }

            }

            @Override
            public void onFailure(Call<com.agriiprince.mvvm.retrofit.dto.tradecrop.Media> call, Throwable t) {
                Log.d("CheckStatus", "Media list  "+ t.getMessage());
            }
        });
    }
    public void Deletemedia() {
        Call<DeleteCropMedia> call = retrofit_interface.deleteCropMedia("2","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY");
        call.enqueue(new Callback<DeleteCropMedia>() {
            @Override
            public void onResponse(Call<DeleteCropMedia> call, Response<DeleteCropMedia> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code DeleteCropMedia ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Log.d("CheckStatus", response.body().getMessage());
                }
              catch (Exception e){e.getMessage();}
            }

            @Override
            public void onFailure(Call<DeleteCropMedia> call, Throwable t) {
                Log.d("CheckStatus", "Delete Media  "+ t.getMessage());
            }
        });
    }
    public void saveImage() {
        File file=new File("/storage/emulated/0/Download/image.jpg");
        final RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file",file.getName(),requestFile);

        Call<SaveMedia> call = retrofit_interface.SaveImage("17","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY",body);
        call.enqueue(new Callback<SaveMedia>() {
            @Override
            public void onResponse(Call<SaveMedia> call, Response<SaveMedia> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code save image ");
                    int status = response.body().getStatus();
                    Logs.d("CheckStatus", String.valueOf(status));
                    Logs.d("CheckStatus", response.body().getMessage());
                    String name=response.body().getMedia().getUrl();
                    String type=response.body().getMedia().getType();
                    Logs.d("CheckStatus", "image name "+ name);
                    Logs.d("CheckStatus", "type "+type);

                }catch (Exception e){e.getMessage();}

            }

            @Override
            public void onFailure(Call<SaveMedia> call, Throwable t) {
                Log.d("CheckStatus", "save image  "+ t.getMessage());
            }
        });
    }
    public void saveVideo() {
        File file=new File("/storage/emulated/0/Download/image.jpg");
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file",file.getName(),requestFile);

        Call<SaveMedia> call = retrofit_interface.SaveVideo("17","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY",body);
        call.enqueue(new Callback<SaveMedia>() {
            @Override
            public void onResponse(Call<SaveMedia> call, Response<SaveMedia> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code save video ");
                    if (response.body() != null) {
                        int status = response.body().getStatus();
                        Logs.d("CheckStatus", String.valueOf(status));
                        Logs.d("CheckStatus", response.body().getMessage());
                        String name=response.body().getMedia().getUrl();
                        String type=response.body().getMedia().getType();
                        Logs.d("CheckStatus", "image name "+ name);
                        Logs.d("CheckStatus", "type "+type);
                    }
                }
                catch (Exception e){e.getMessage();}
            }

            @Override
            public void onFailure(Call<SaveMedia> call, Throwable t) {
                Log.d("CheckStatus", "save video  "+ t.getMessage());
            }
        });
    }

    public void SaveCropFpo() {
        Call<SaveCrop> call = retrofit_interface.SaveCropFpo("3","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","Apple","Apple","Apple","2019-06-11T19:12:40.799Z","Apple","Apple","Apple","Apple","Apple","Apple","Apple","Apple","2019-06-11T19:12:40.799Z","Apple","Apple","Apple","Apple");
        call.enqueue(new Callback<SaveCrop>() {
            @Override
            public void onResponse(Call<SaveCrop> call, Response<SaveCrop> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code cropFpo ");
                    Log.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Log.d("CheckStatus", response.body().getMessage().toString());
                }
              catch (Exception e){e.getMessage();}

            }

            @Override
            public void onFailure(Call<SaveCrop> call, Throwable t) {
                Log.d("CheckStatus", "Save Crop Fpo  "+ t.getMessage());
            }
        });
    }
    public void SaveCropFig() {
        Call<SaveCrop> call = retrofit_interface.SaveCropFig("3","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","Apple","Apple","Apple","2019-06-11T19:12:40.799Z","Apple","Apple","Apple","Apple","Apple","Apple","Apple","Apple","2019-06-11T19:12:40.799Z","Apple","Apple","Apple","Apple");
        call.enqueue(new Callback<SaveCrop>() {
            @Override
            public void onResponse(Call<SaveCrop> call, Response<SaveCrop> response) {
                try {
                    Log.d("RetroCode",String.valueOf(response.code()));
                    Logs.d("CheckStatus", "onResponse: code Crop fig ");
                    Log.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Log.d("CheckStatus", response.body().getMessage().toString());
                }
              catch (Exception e){e.getMessage();}
            }

            @Override
            public void onFailure(Call<SaveCrop> call, Throwable t) {
                Log.d("CheckStatus", "Save Crop Fig  "+ t.getMessage());
            }
        });
    }
    public void SaveCropFarmer() {
        Call<SaveCrop> call = retrofit_interface.SaveCropFarmer("3","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","Apple","Apple","Apple","2019-06-11T19:12:40.799Z","Apple","Apple","Apple","Apple","Apple","Apple","Apple","Apple","2019-06-11T19:12:40.799Z","Apple","Apple","Apple","Apple");
        call.enqueue(new Callback<SaveCrop>() {
            @Override
            public void onResponse(Call<SaveCrop> call, Response<SaveCrop> response) {
         try {
             Logs.d("CheckStatus", "onResponse: code Crop farmer ");
             Log.d("CheckStatus", String.valueOf(response.body().getStatus()));
             Log.d("CheckStatus", response.body().getMessage().toString());
             Log.d("CheckStatus", response.body().getData().getCreatedByType());
             Log.d("CheckStatus", response.body().getData().getSowingCrop());
         }catch (Exception e){e.getMessage();}

            }

            @Override
            public void onFailure(Call<SaveCrop> call, Throwable t) {
                Log.d("CheckStatus", "Save Crop Farmer  "+ t.getMessage());
            }
        });
    }
    public void DeleteCrop() {
        Call<DeleteCrop> call = retrofit_interface.deleteCrop("52","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY");
        call.enqueue(new Callback<DeleteCrop>() {
            @Override
            public void onResponse(Call<DeleteCrop> call, Response<DeleteCrop> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code DeleteCrop ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Log.d("CheckStatus", response.body().getMessage());
                }
                catch (Exception e){e.getMessage();}
            }

            @Override
            public void onFailure(Call<DeleteCrop> call, Throwable t) {
                Log.d("CheckStatus", "Delete crop  "+ t.getMessage());
            }
        });
    }
}
