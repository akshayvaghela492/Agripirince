package com.agriiprince.mvvm.retrofit.model.mandi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MandiLocationDetail implements Serializable {

    @SerializedName("list_modal_price")
    @Expose
    private List<Integer> list_modal_price;

    @SerializedName("mandi_name_en")
    @Expose
    private String mandi_name_en;
    @SerializedName("mandi_lat")
    @Expose
    private double mandi_lat;
    @SerializedName("mandi_lng")
    @Expose
    private double mandi_lng;

    public List<Integer> getList_modal_price() {
        return list_modal_price;
    }

    public void setList_modal_price(List<Integer> list_modal_price) {
        this.list_modal_price = list_modal_price;
    }

    public String getMandi_name_en() {
        return mandi_name_en;
    }

    public void setMandi_name_en(String mandi_name_en) {
        this.mandi_name_en = mandi_name_en;
    }

    public double getMandi_lat() {
        return mandi_lat;
    }

    public void setMandi_lat(double mandi_lat) {
        this.mandi_lat = mandi_lat;
    }

    public double getMandi_lng() {
        return mandi_lng;
    }

    public void setMandi_lng(double mandi_lng) {
        this.mandi_lng = mandi_lng;
    }
}
