package com.agriiprince.mvvm.retrofit.dto.subscription;

import com.agriiprince.mvvm.retrofit.model.subscription.Fonepaisa_Subscription;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

// for migrated API created on 12/8/2019
public class Fonepaise_Subscription implements Serializable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Fonepaisa_Subscription data = null;
    @SerializedName("code")
    @Expose
    private Integer code;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Fonepaisa_Subscription getData() {
        return data;
    }

    public void setData(Fonepaisa_Subscription data) {
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
