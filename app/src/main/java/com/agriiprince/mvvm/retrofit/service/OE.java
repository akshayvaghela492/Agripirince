package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.login.LoginUser;
import com.agriiprince.mvvm.retrofit.dto.oe.Consignment;
import com.agriiprince.mvvm.retrofit.dto.oe.OeLoginResponse;
import com.agriiprince.mvvm.retrofit.dto.oe.OeUpdateProfile;
import com.agriiprince.mvvm.retrofit.dto.oe.UpdatePrice;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface OE {
    @FormUrlEncoded
    @POST("login")
    Call<OeLoginResponse> OeLogin(@Field("mobileno") String mobileno,
                                  @Field("password") String password,
                                  @Field("mode") String mode,
                                  @Field("fcm_token") String fcm_token,
                                  @Field("login_type") String login_type
                                  );




    @FormUrlEncoded
    @POST("oe/report_price")
    Call<UpdatePrice> UpdatePrice(@Header("authentication") String token,
                                  @Field("reported_by_user") String reported_by_user,
                                  @Field("commodity_name") String commodity_name,
                                  @Field("commodity_variety") String commodity_variety,
                                  @Field("commodity_grade") String commodity_grade,
                                  @Field("list_arrival_date") String list_arrival_date,
                                  @Field("commodity_state") String commodity_state,
                                  @Field("commodity_district") String commodity_district,
                                  @Field("commodity_market") String commodity_market,
                                  @Field("min_price") String min_price,
                                  @Field("max_price") String max_price,
                                  @Field("modal_price") String modal_price,
                                  @Field("general_price") String general_price
                                          );


    @FormUrlEncoded
    @POST(" ")
    Call<OeUpdateProfile> UpdateProfile(@Header("authentication") String token,
                                        @Field("interested_crop_list") List<String> interested_crop_list,
                                        @Field("interested_mandi_list") List<String> interested_mandi_list,
                                        @Field("my_crop_list") List<String> my_crop_list,
                                        @Field("user_id") String user_id,
                                        @Field("password") String password,
                                        @Field("secondary_language") String secondary_language
    );

    @FormUrlEncoded
    @POST(" ")
    Call<LoginUser> LoginDetails(@Header("authentication") String token,
                                 @Field("user_id") String user_id,
                                 @Field("user_type") String user_type
    );
    @FormUrlEncoded
    @POST(" ")
    Call<Consignment> Consignment(@Header("authentication") String token,
                                   @Field("user_id") String user_id,
                                   @Field("user_type") String user_type
    );

}
