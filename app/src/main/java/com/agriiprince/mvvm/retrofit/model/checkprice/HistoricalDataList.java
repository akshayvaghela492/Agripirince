package com.agriiprince.mvvm.retrofit.model.checkprice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class HistoricalDataList implements Serializable {

    @SerializedName("result")
    @Expose
    private List<HistoricalPriceData> result = null;


    public List<HistoricalPriceData> getResult() {
        return result;
    }

    public void setResult(List<HistoricalPriceData> result) {
        this.result = result;
    }
}