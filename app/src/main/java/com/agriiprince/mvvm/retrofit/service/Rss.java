package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.rss.RssFeed;
import com.agriiprince.mvvm.retrofit.dto.rss.RssTopic;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Rss {

    @FormUrlEncoded
    @POST(" ")
    Call<RssTopic> RSS_TOPIC(@Header("authentication") String token,
                                @Field("user_id") String user_id,
                                @Field("user_type") String user_type,
                                @Field("rss_feed_id") String rss_feed_id,
                                @Field("mode") String mode
    );
    @GET(" ")
    Call<RssFeed> RSS_Feed(@Header("authentication") String token);
}
