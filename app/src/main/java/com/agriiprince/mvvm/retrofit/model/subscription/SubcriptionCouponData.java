package com.agriiprince.mvvm.retrofit.model.subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.io.Serializable;

public class SubcriptionCouponData implements Serializable {

    @SerializedName("valid_coupon")
    @Expose
    private Boolean validCoupon;
    @SerializedName("trial_activated")
    @Expose
    private Boolean trialActivated;
    @SerializedName("data")
    @Expose
    private SubscriptionDetailList data;


    public Boolean getValidCoupon() {
        return validCoupon;
    }

    public void setValidCoupon(Boolean validCoupon) {
        this.validCoupon = validCoupon;
    }

    public Boolean getTrialActivated() {
        return trialActivated;
    }

    public void setTrialActivated(Boolean trialActivated) {
        this.trialActivated = trialActivated;
    }

    public SubscriptionDetailList getData() {
        return data;
    }

    public void setData(SubscriptionDetailList data) {
        this.data = data;
    }

}