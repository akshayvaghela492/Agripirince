package com.agriiprince.mvvm.retrofit.test;

import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.checkprice.GovReportPrices;
import com.agriiprince.mvvm.retrofit.dto.checkprice.HistoricalPrices;
import com.agriiprince.mvvm.retrofit.dto.checkprice.OeReportedPrices;
import com.agriiprince.mvvm.retrofit.model.checkprice.GovRequestBody;
import com.agriiprince.mvvm.retrofit.service.CheckPrice;
import com.agriiprince.mvvm.util.Logs;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CheckPriceRetro {

    API_Manager api_manager = new API_Manager();
    CheckPrice retrofit_interface=api_manager.getClient10().create(CheckPrice.class);

  /*  public void GovReportedPriceData() {
        List<String> cropGrade=new ArrayList<>();
        cropGrade.add("FAQ");
        List<String> cropVariety=new ArrayList<>();
        cropVariety.add("Other");
        List<String> cropName=new ArrayList<>();
        cropName.add("Ajwan");
        cropName.add("Apple");

        GovRequestBody requestBody = new GovRequestBody();
        requestBody.setLimit(20);
        requestBody.setOffset(0);
        requestBody.setDate("2018-10-12T00:00:00.301Z");
        requestBody.setUserId("UKYVLUT5");
        requestBody.setCropGrade(cropGrade);
        requestBody.setCropVariety(cropVariety);
        requestBody.setCropName(cropName);

        Call<GovReportPrices> call=retrofit_interface.GovReportedPriceData("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxNCwibmFtZSI6Im1lZ1xuIiwiZW1haWwiOiJtZWdAZ21haWwuY29tIiwiemlwQ29kZSI6MTIzLCJtb2JpbGUiOjMyMTMyMTMyMTMsInN0YXR1cyI6MSwiY3JlYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidXBkYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidHlwZSI6ImZwbyJ9LCJpYXQiOjE1NjYwMjMwNDgsImV4cCI6MTU2ODYxNTA0OH0.-yaQVXHScLrl-poZsV3alGKC4YLiPUhvoIEhD-h0g6Y","application/json",requestBody);

        call.enqueue(new Callback<GovReportPrices>() {

            @Override
            public void onResponse(Call<GovReportPrices> call, Response<GovReportPrices> response) {
                Logs.d("CheckStatus_gov", "onResponse: Gov"+response);
                if (response.body() != null) {
                    Logs.d("CheckStatus_gov", response.body().getStatus().toString());
                    Logs.d("CheckStatus_gov", response.body().getMessage());
                    if (response.isSuccessful() && response.body() != null) {
                        Logs.d("CheckStatus_gov",response.body().getData().getResult().get(1).getVariety());
                }

                }
            }
            @Override
            public void onFailure(Call<GovReportPrices> call, Throwable t) {
                Logs.d("CheckStatus_gov","Gov error message  "+t.getMessage());
                Logs.d("CheckStatus_gov","Gov error cause "+t.getCause());


            }
        });
    }

    public void HistoricalPriceData() {

        Call<HistoricalPrices> call=retrofit_interface.HistoricalPriceData("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxNCwibmFtZSI6Im1lZ1xuIiwiZW1haWwiOiJtZWdAZ21haWwuY29tIiwiemlwQ29kZSI6MTIzLCJtb2JpbGUiOjMyMTMyMTMyMTMsInN0YXR1cyI6MSwiY3JlYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidXBkYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidHlwZSI6ImZwbyJ9LCJpYXQiOjE1NjYwMjMwNDgsImV4cCI6MTU2ODYxNTA0OH0.-yaQVXHScLrl-poZsV3alGKC4YLiPUhvoIEhD-h0g6Y","","2018-09-29T15:11:04.583Z","60","");
        call.enqueue(new Callback<HistoricalPrices>() {
            @Override
            public void onResponse(Call<HistoricalPrices> call, Response<HistoricalPrices> response) {
                Logs.d("CheckStatus", "onResponse Historical"+response);
                if (response.body() != null) {
                    Logs.d("CheckStatus", response.body().getStatus().toString());
                    Logs.d("CheckStatus", response.body().getMessage());
                    if (response.isSuccessful() && response.body() != null) {
                        Logs.d("CheckStatus",response.body().getData().getResult().get(1).getVariety());
                    }
                }

            }
            @Override
            public void onFailure(Call<HistoricalPrices> call, Throwable t) {
                Logs.d("CheckStatus","Historical error "+t.getMessage());
            }
        });
    }
    public void OeReportedPriceData() {

        Call<OeReportedPrices> call=retrofit_interface.OeReportedPriceData("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTk1MzU4NCwiZXhwIjoxNTY0NTQ1NTg0fQ.vu6qstszSI4kp_l-r1ogOnYM3-Nj0wFDKVewf1P-7ZI","","","","","");
        call.enqueue(new Callback<OeReportedPrices>() {
            @Override
            public void onResponse(Call<OeReportedPrices> call, Response<OeReportedPrices> response) {
                Logs.d("CheckStatus", "onResponse: cold storage");
                Logs.d("CheckStatus", response.body().getStatus());
                Logs.d("CheckStatus", response.body().getCode().toString());
                Logs.d("CheckStatus", response.body().getMessage());
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getData().get(1).getVariety());
                }
            }
            @Override
            public void onFailure(Call<OeReportedPrices> call, Throwable t) {
                Logs.d("CheckStatus","cold storage error "+t.getMessage());
            }
        });
    }
    */
}
