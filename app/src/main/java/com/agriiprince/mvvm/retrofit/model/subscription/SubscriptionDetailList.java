package com.agriiprince.mvvm.retrofit.model.subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubscriptionDetailList implements Serializable
{

    public static final int NONE = 0;

    public static final int BASIC = 1;

    public static final int PREMIUM = 2;

    private boolean isBasic;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("subscription_id")
    @Expose
    private String subscriptionId;
    @SerializedName("subscription_type")
    @Expose
    private String subscriptionType;
    @SerializedName("subscription_price")
    @Expose
    private String subscriptionPrice;
    @SerializedName("subscription_validity_days")
    @Expose
    private String subscriptionValidityDays;
    @SerializedName("subscription_status")
    @Expose
    private String subscriptionStatus;
    @SerializedName("subscription_name")
    @Expose
    private String subscriptionName;
    @SerializedName("subscription_name_hindi")
    @Expose
    private String subscriptionNameHindi;
    @SerializedName("inaugral_offer")
    @Expose
    private String inaugralOffer;
    @SerializedName("inaugral_offer_price")
    @Expose
    private String inaugralOfferPrice;
    @SerializedName("applicable_for_waver")
    @Expose
    private String applicableForWaver;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("updated_on")
    @Expose
    private String updatedOn;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public String getSubscriptionPrice() {
        return subscriptionPrice;
    }

    public void setSubscriptionPrice(String subscriptionPrice) {
        this.subscriptionPrice = subscriptionPrice;
    }

    public String getSubscriptionValidityDays() {
        return subscriptionValidityDays;
    }

    public void setSubscriptionValidityDays(String subscriptionValidityDays) {
        this.subscriptionValidityDays = subscriptionValidityDays;
    }

    public String getSubscriptionStatus() {
        return subscriptionStatus;
    }

    public void setSubscriptionStatus(String subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    public String getSubscriptionName() {
        return subscriptionName;
    }

    public void setSubscriptionName(String subscriptionName) {
        this.subscriptionName = subscriptionName;
    }

    public String getSubscriptionNameHindi() {
        return subscriptionNameHindi;
    }

    public void setSubscriptionNameHindi(String subscriptionNameHindi) {
        this.subscriptionNameHindi = subscriptionNameHindi;
    }

    public String getInaugralOffer() {
        return inaugralOffer;
    }

    public void setInaugralOffer(String inaugralOffer) {
        this.inaugralOffer = inaugralOffer;
    }

    public String getInaugralOfferPrice() {
        return inaugralOfferPrice;
    }

    public void setInaugralOfferPrice(String inaugralOfferPrice) {
        this.inaugralOfferPrice = inaugralOfferPrice;
    }

    public String getApplicableForWaver() {
        return applicableForWaver;
    }

    public void setApplicableForWaver(String applicableForWaver) {
        this.applicableForWaver = applicableForWaver;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public boolean isBasic() {
        return isBasic;
    }

    public void setBasic(boolean basic) {
        isBasic = basic;
    }
}