package com.agriiprince.mvvm.retrofit.dto.trackcrop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TrackGetCrop implements Serializable {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<com.agriiprince.mvvm.retrofit.model.trackcrop.TrackGetCrop> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<com.agriiprince.mvvm.retrofit.model.trackcrop.TrackGetCrop> getData() {
        return data;
    }

    public void setData(List<com.agriiprince.mvvm.retrofit.model.trackcrop.TrackGetCrop> data) {
        this.data = data;
    }
}
