
package com.agriiprince.mvvm.retrofit.dto.tradecrop;

import java.io.Serializable;
import java.util.List;

import com.agriiprince.mvvm.retrofit.model.tradecrop.CropDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FarmerCrop implements Serializable
{

        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<CropDetails> data;

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<CropDetails> getData() {
            return data;
        }

        public void setData(List<CropDetails> data) {
            this.data = data;
        }

}

