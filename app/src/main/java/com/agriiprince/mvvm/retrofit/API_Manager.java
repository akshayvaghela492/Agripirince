package com.agriiprince.mvvm.retrofit;

import com.agriiprince.mvvm.applevel.constants.Config;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class API_Manager {
    private Retrofit retrofit = null;
    //private String BaseUrl="http://13.232.163.213:8000/api/v1/users/";

    public Retrofit getClient() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.NODE_APIS_USERS)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    //private Retrofit retrofit = null;
    //private String BaseUrl2="http://13.232.163.213:8000/api/v1/crop/";
    public Retrofit getClient2() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.NODE_APIS_CROP)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
   // private Retrofit retrofit = null;
    //private String Base_url="http://13.232.163.213:8000/api/v1/apk_version/";
    public Retrofit getClient3() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.NODE_APK_VERSION)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    //private Retrofit retrofit = null;
    //private String baseurl = "http://13.232.163.213:8000/api/v1/cold_storage/";


    public Retrofit getClient4() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.NODE_COLD_STORAGE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    //private Retrofit retrofit = null;
    //private String baseurl="http://13.232.163.213:8000/api/v1/crop_list/";

    public Retrofit getClient5() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.NODE_CROP_LIST)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    //private Retrofit retrofit = null;
    //private String baseurl="http://13.232.163.213:8000/api/v1/crop_diseases/";

    public Retrofit getClient6() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.NODE_DISEASE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    //private Retrofit retrofit = null;
    //private String baseurl="http://13.232.163.213:8000/api/v1/notifications/";

    public Retrofit getClient7() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.NODE_NOTIFICATION)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    //private Retrofit retrofit = null;
    //private String baseurl="http://13.232.163.213:8000/api/v1/users/";

    public Retrofit getClient8() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.NODE_APIS_USERS)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    //private Retrofit retrofit = null;
    // private String baseurl="http://13.232.163.213:8000/api/v1/weather/forecast/";

    public  Retrofit getClient9() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.NODE_WEATHER)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    private OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(2, TimeUnit.MINUTES)
            .readTimeout(2, TimeUnit.MINUTES)
            .writeTimeout(2, TimeUnit.MINUTES)
            .build();
    public Retrofit getClient10() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.NODE_CHECK_PRICE)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    public Retrofit getClient11() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.NODE_CA)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    public  Retrofit phpClient() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.NEW_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    public  Retrofit RazorpayClient() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.NODE_RAZOR_PAY)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    public  Retrofit SubscriptionClient() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.NODE_SUBSCRIPTION)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    public  Retrofit TutorialClient() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.NODE_TUTORIAL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
