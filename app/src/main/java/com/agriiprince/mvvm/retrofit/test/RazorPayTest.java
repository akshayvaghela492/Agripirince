package com.agriiprince.mvvm.retrofit.test;

import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.razorpay.CheckoutResponse;
import com.agriiprince.mvvm.retrofit.dto.razorpay.ManualSubscriptionResponse;
import com.agriiprince.mvvm.retrofit.dto.razorpay.RazorPayResponse;
import com.agriiprince.mvvm.retrofit.dto.tutorial.TutorialResponse;
import com.agriiprince.mvvm.retrofit.service.RazorPay;
import com.agriiprince.mvvm.util.Logs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RazorPayTest {
    API_Manager api_manager=new API_Manager();
    RazorPay retrofit_interface = api_manager.RazorpayClient().create(RazorPay.class);


    public void get_order_id() {
        Logs.d("RazorPayTest", "get_order_id call");
        Call<RazorPayResponse> call=retrofit_interface.get_order_id("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaTEyMyIsInVzZXJfaWQiOiJGMTVBS0xLUSIsInBhc3N3b3JkIjoiMTIzMTIzIiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjYyMTI3NzIsImV4cCI6MTU2ODgwNDc3Mn0.Nhx5uc_FTtaifgF3BcE3-lPYxJnNQ5mzHVgdvBFQoSk","","100","INR");
        call.enqueue(new Callback<RazorPayResponse>() {
            @Override
            public void onResponse(Call<RazorPayResponse> call, Response<RazorPayResponse> response) {
                Logs.d("RazorPayTest", "get_order_id Response "+response);
                if (response.body() != null) {
                    Logs.d("RazorPayTest",response.body().getCode().toString());
                    Logs.d("RazorPayTest",response.body().getMessage());
                    Logs.d("RazorPayTest",response.body().getData().getRazorpayId());
                }
            }
            @Override
            public void onFailure(Call<RazorPayResponse> call, Throwable t) {
                Logs.d("RazorPayTest","get_order_id error "+t.getMessage());
            }
        });
    }
    public void checkout_capture_payment() {
        Logs.d("RazorPayTest", "checkout_capture_payment call");
        Call<CheckoutResponse> call=retrofit_interface.checkout_capture_payment("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaTEyMyIsInVzZXJfaWQiOiJGMTVBS0xLUSIsInBhc3N3b3JkIjoiMTIzMTIzIiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjYyMTI3NzIsImV4cCI6MTU2ODgwNDc3Mn0.Nhx5uc_FTtaifgF3BcE3-lPYxJnNQ5mzHVgdvBFQoSk","sldjfd","lsjdfd","lsdkjf","","");
        call.enqueue(new Callback<CheckoutResponse>() {
            @Override
            public void onResponse(Call<CheckoutResponse> call, Response<CheckoutResponse> response) {
                Logs.d("RazorPayTest", "checkout_capture_payment Response "+response);
                if (response.body() != null) {
                    Logs.d("RazorPayTest",response.body().getCode().toString());
                    Logs.d("RazorPayTest",response.body().getMessage());
                }

            }
            @Override
            public void onFailure(Call<CheckoutResponse> call, Throwable t) {
                Logs.d("RazorPayTest","checkout_capture_payment error "+t.getMessage());
            }
        });
    }
    /*public void manual_fee_collection() {
        Logs.d("CheckStatus", " call");
        Call<ManualSubscriptionResponse> call=retrofit_interface.manual_fee_collection("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxNCwibmFtZSI6Im1lZ1xuIiwiZW1haWwiOiJtZWdAZ21haWwuY29tIiwiemlwQ29kZSI6MTIzLCJtb2JpbGUiOjMyMTMyMTMyMTMsInN0YXR1cyI6MSwiY3JlYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidXBkYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidHlwZSI6ImZwbyJ9LCJpYXQiOjE1NjYwMjMwNDgsImV4cCI6MTU2ODYxNTA0OH0.-yaQVXHScLrl-poZsV3alGKC4YLiPUhvoIEhD-h0g6Y","12","9988998899","9988998899","100");
        call.enqueue(new Callback<ManualSubscriptionResponse>() {
            @Override
            public void onResponse(Call<ManualSubscriptionResponse> call, Response<ManualSubscriptionResponse> response) {
                Logs.d("RazorPayTest", "manual_fee_collection Response "+response);
                if (response.body() != null) {
                    Logs.d("RazorPayTest",response.body().getCode().toString());
                    Logs.d("RazorPayTest",response.body().getMessage());
                    Logs.d("RazorPayTest",response.body().getData().getData().getSubscriptionId());
                }

            }
            @Override
            public void onFailure(Call<ManualSubscriptionResponse> call, Throwable t) {
                Logs.d("CheckStatus"," error "+t.getMessage());
            }
        });
    }*/
}
