package com.agriiprince.mvvm.retrofit;

import android.support.annotation.StringRes;

import com.agriiprince.R;
import com.agriiprince.mvvm.di.network.NoConnectivityException;
import com.agriiprince.mvvm.util.Logs;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import java.net.SocketTimeoutException;

public class ResponseWrapper<T> {

    private static final String TAG = "ResponseWrapper";
    
    /**
     * initial status will be loading
     */
    private Status status = Status.LOADING;

    /**
     * error type
     */
    private Throwable throwable;

    /**
     * String res id to display a message to user according to the error time
     */
    private @StringRes int errorMessageId;

    /**
     * response status code from the server
     */
    @Expose
    @SerializedName("status_code")
    private int statusCode;

    /**
     * response message from the server
     */
    @Expose
    @SerializedName("message")
    private String message;

    /**
     * response data object for parsing json
     */
    @Expose
    @SerializedName("data")
    private T data;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int errorCode) {
        this.statusCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @StringRes
    public int getErrorMessageId() {
        return errorMessageId;
    }

    public T getData() {
        return data;
    }

    /**
     * sets the data object serialized by the the retrofit and status to {@link Status} and errorMessageId to success
     * @param data serialized object type
     */
    public void setData(T data) {
        this.data = data;
        this.status = Status.SUCCESS;
        this.errorMessageId = R.string.success;
    }

    public Throwable getError() {
        return throwable;
    }

    /**
     * set the retrofit onFailure error, status will be set according to the error type.
     * errorMessageId according to error
     * @param throwable throwable error from retrofit callback
     */
    public void setError(Throwable throwable) {
        Logs.d(TAG, "setError: " + throwable.getMessage());
        this.throwable = throwable;

        if (throwable instanceof NoConnectivityException) {
            this.status = Status.NO_CONNECTIVITY;
            this.errorMessageId = R.string.no_internet_connection; // no network available message
        } else if (throwable instanceof SocketTimeoutException) {
            this.status = Status.ERROR;
            this.errorMessageId = R.string.connection_time_out; // set the message id to connection time out
        } else if (throwable instanceof IOException) {
            this.status = Status.ERROR;
            this.errorMessageId = R.string.something_went_wrong_try_again;
        } else {
            this.status = Status.ERROR;
            this.errorMessageId = R.string.something_went_wrong_try_again;
        }
    }

    public enum Status {
        LOADING,
        SUCCESS,
        ERROR,
        NO_CONNECTIVITY
    }

}
