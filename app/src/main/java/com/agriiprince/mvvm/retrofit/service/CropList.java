package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.croplist.Croplist_byname;
import com.agriiprince.mvvm.retrofit.dto.croplist.Croplist_byClass;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

// this class is for migrated crop list APIs

public interface CropList {
    @FormUrlEncoded
    @POST("list_by_name/get_crop_list")
    Call<Croplist_byname> getCropbyname(@Header("authentication") String token,

                                        @Field("mode") String mode,
                                        @Field("lang") String lang);
    @FormUrlEncoded
    @POST("list_by_class/get_crop_list_by_class")
    Call<Croplist_byClass>  getCropbyclass(@Header("authentication") String token,
                                           @Field("crop_class") String cropclass);
}
