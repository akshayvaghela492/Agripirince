
package com.agriiprince.mvvm.retrofit.model.notification;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UnreadMsgDetail implements Serializable
{

    @SerializedName("weather")
    @Expose
    private WeatherNotification weather;
    @SerializedName("commodity")
    @Expose
    private Commodity commodity;
    @SerializedName("pincode")
    @Expose
    private Pincode pincode;
    @SerializedName("city")
    @Expose
    private City city;
    @SerializedName("personal")
    @Expose
    private Personal personal;

    public WeatherNotification getWeather() {
        return weather;
    }

    public void setWeather(WeatherNotification weather) {
        this.weather = weather;
    }

    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    public Pincode getPincode() {
        return pincode;
    }

    public void setPincode(Pincode pincode) {
        this.pincode = pincode;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Personal getPersonal() {
        return personal;
    }

    public void setPersonal(Personal personal) {
        this.personal = personal;
    }

}
