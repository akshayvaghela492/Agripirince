package com.agriiprince.mvvm.retrofit.model.subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubscriptionCouponList implements Serializable
{
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("discount_id")
    @Expose
    private String discountId;
    @SerializedName("discount_name")
    @Expose
    private String discountName;
    @SerializedName("subscription_id")
    @Expose
    private String subscriptionId;
    @SerializedName("valid_in_days")
    @Expose
    private String validInDays;
    @SerializedName("discount_in_percent")
    @Expose
    private String discountInPercent;
    @SerializedName("discount_in_rupees")
    @Expose
    private String discountInRupees;
    @SerializedName("coupon_code")
    @Expose
    private String couponCode;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("updated_on")
    @Expose
    private String updatedOn;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDiscountId() {
        return discountId;
    }

    public void setDiscountId(String discountId) {
        this.discountId = discountId;
    }

    public String getDiscountName() {
        return discountName;
    }

    public void setDiscountName(String discountName) {
        this.discountName = discountName;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getValidInDays() {
        return validInDays;
    }

    public void setValidInDays(String validInDays) {
        this.validInDays = validInDays;
    }

    public String getDiscountInPercent() {
        return discountInPercent;
    }

    public void setDiscountInPercent(String discountInPercent) {
        this.discountInPercent = discountInPercent;
    }

    public String getDiscountInRupees() {
        return discountInRupees;
    }

    public void setDiscountInRupees(String discountInRupees) {
        this.discountInRupees = discountInRupees;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

}