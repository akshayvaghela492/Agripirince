package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.login.FcmTokenUpdate;
import com.agriiprince.mvvm.retrofit.dto.login.Logout;
import com.agriiprince.mvvm.retrofit.dto.profile.UserUpdate;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.FigLogin;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.FpoResponse;
import com.agriiprince.mvvm.retrofit.dto.login.Login;
import com.agriiprince.mvvm.retrofit.dto.login.TokenGenerate;
import com.agriiprince.mvvm.retrofit.dto.login.UserRegistration;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.UpdateDeleteUser;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.UserCreate;
import com.agriiprince.mvvm.retrofit.dto.login.UserExist;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.UserResponse;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.mvvm.util.Logs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRetro {
    API_Manager apiManager=new API_Manager();
    UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);

    public void generateToken() {
        Call<TokenGenerate> call = retrofit_interface.generateToken("XEB14KYR","farmer","123123");
        call.enqueue(new Callback<TokenGenerate>() {
            @Override
            public void onResponse(Call<TokenGenerate> call, Response<TokenGenerate> response) {
                try {

                    Logs.d("CheckStatus", "onResponse: code token " );
                    Logs.d("CheckStatus",String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getData().getToken());
                }catch (Exception e){e.getMessage();}
            }

            @Override
            public void onFailure(Call<TokenGenerate> call, Throwable t) {
                Log.d("CheckStatus", "token "+ t.getMessage());
            }
        });
    }
    public void Registration() {
        Call<UserRegistration> call = retrofit_interface.registration("farmer","mg","321654","delhi","110022","741852963","english","abc123abc123abc123");
        call.enqueue(new Callback<UserRegistration>() {
            @Override
            public void onResponse(Call<UserRegistration> call, Response<UserRegistration> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code registration ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus", response.body().getMessage());
                    Logs.d("CheckStatus", response.body().getData().getUserId());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<UserRegistration> call, Throwable t) {
                Log.d("CheckStatus", "UserRegistration "+ t.getMessage());
            }
        });
    }
    public void FigLogin() {
        Call<FigLogin> call = retrofit_interface.FigLogin("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTk1MzU4NCwiZXhwIjoxNTY0NTQ1NTg0fQ.vu6qstszSI4kp_l-r1ogOnYM3-Nj0wFDKVewf1P-7ZI","663366","663366-IDrcy");
        call.enqueue(new Callback<FigLogin>() {
            @Override
            public void onResponse(Call<FigLogin> call, Response<FigLogin> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code registration ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus", response.body().getMessage());
                    Logs.d("CheckStatus", response.body().getData().getName());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<FigLogin> call, Throwable t) {
                Log.d("CheckStatus", "UserRegistration "+ t.getMessage());
            }
        });
    }
    public void Login() {
        Call<Login> call = retrofit_interface.Login("8319073618","123123","farmer"," "," ");
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code Login ");
                    Logs.d("CheckStatus", response.body().getCode().toString());
                    Logs.d("CheckStatus", response.body().getMessage());
                    Logs.d("CheckStatus", response.body().getStatus().toString());
                    Logs.d("CheckStatus", response.body().getData().getFarmerName());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Log.d("CheckStatus", "Login "+ t.getMessage());
            }
        });
    }
    public void FcmTokenUpdate() {
        Call<FcmTokenUpdate> call = retrofit_interface.FcmTokenUpdate("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTk1MzU4NCwiZXhwIjoxNTY0NTQ1NTg0fQ.vu6qstszSI4kp_l-r1ogOnYM3-Nj0wFDKVewf1P-7ZI","farmer","JUSPA85W","abc123###asd1q2dsfgsac@@!!_");
        call.enqueue(new Callback<FcmTokenUpdate>() {
            @Override
            public void onResponse(Call<FcmTokenUpdate> call, Response<FcmTokenUpdate> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code FCM token update ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()) );
                    Logs.d("CheckStatus", response.body().getMessage());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<FcmTokenUpdate> call, Throwable t) {
                Log.d("CheckStatus", "FCM token update error "+ t.getMessage());
            }
        });
    }
    public void userExist() {
        Call<UserExist> call = retrofit_interface.userExist("8763476609","farmer");
        call.enqueue(new Callback<UserExist>() {
            @Override
            public void onResponse(Call<UserExist> call, Response<UserExist> response) {
                try {

                    Logs.d("CheckStatus", "onResponse: code userExist ");
                    Logs.d("CheckStatus", response.body().getStatus());
                    Logs.d("CheckStatus", response.body().getMessage());
                    Logs.d("CheckStatus", response.body().getCode().toString());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<UserExist> call, Throwable t) {
                Log.d("CheckStatus", "user exist "+ t.getMessage());
            }
        });
    }
    public void getfiglist() {
        Call<UserResponse> call = retrofit_interface.getFigList("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY",9);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code getfiglist ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus", response.body().getMessage());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Log.d("CheckStatus", "fig list "+ t.getMessage());
            }
        });
    }
    public void getFarmerListFPO() {
        Call<UserResponse> call = retrofit_interface.getFarmerListFPO("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","3");
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code FarmerListFPO ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus", response.body().getMessage());

                } catch (Exception e) {
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Log.d("CheckStatus", "farmer list by fpo "+ t.getMessage());
            }
        });
    }
    public void getFarmerListFIG() {
        Call<UserResponse> call = retrofit_interface.getFarmerListFIG("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","3");
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code FarmerListFIG ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus", response.body().getMessage());

                }
                catch(Exception e){
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Log.d("CheckStatus", "farmer list by fig "+t.getMessage());
            }
        });
    }
    public void CreateFpo() {
        Call<FpoResponse> call = retrofit_interface.CreateFpo("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","A1","a1@gmail.com","10222","114477");
        call.enqueue(new Callback<FpoResponse>() {
            @Override
            public void onResponse(Call<FpoResponse> call, Response<FpoResponse> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code create fpo ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus", response.body().getMessage());
                    Logs.d("CheckStatus", String.valueOf(response.body().getData().getId()));

                } catch (Exception e) {
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<FpoResponse> call, Throwable t) {
                Log.d("CheckStatus", "create fpo fail " +t.getMessage());
            }
        });
    }
    public void CreateFIG_AGG() {
        Call<UserCreate> call = retrofit_interface.CreateFIG_AGG("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","3","a2","a2@gmail.com","11822","225588");
        call.enqueue(new Callback<UserCreate>() {
            @Override
            public void onResponse(Call<UserCreate> call, Response<UserCreate> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code CreateFIG_Agg ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus", response.body().getMessage());
                }

                catch(Exception e){
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<UserCreate> call, Throwable t) {
                Log.d("CheckStatus", "create fig agg " +t.getMessage());
            }
        });
    }
    public void CreateFarmer_Fpo() {
        Call<UserCreate> call = retrofit_interface.CreateFarmer_Fpo("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","9","a3","3201","3","hectare","336699");
        call.enqueue(new Callback<UserCreate>() {
            @Override
            public void onResponse(Call<UserCreate> call, Response<UserCreate> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code createFarmerFPO ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus", response.body().getMessage());
                    Logs.d("CheckStatus", response.body().getData().getFarmer_id());

                }
                catch(Exception e){
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<UserCreate> call, Throwable t) {
                Log.d("CheckStatus", "create farmer fpo " +t.getMessage());
            }
        });
    }
    public void CreateFarmer_FIG() {
        Call<UserCreate> call = retrofit_interface.CreateFarmer_FIG("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","9","a4","11002","4","hectare","998877");
        call.enqueue(new Callback<UserCreate>() {
            @Override
            public void onResponse(Call<UserCreate> call, Response<UserCreate> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code CreateFarmer_FIG ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus", response.body().getMessage());
                    Logs.d("CheckStatus", response.body().getData().getFarmer_id());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<UserCreate> call, Throwable t) {
                Log.d("CheckStatus", "create farmer fig " +t.getMessage());
            }
        });
    }
    public void updateFarmer() {
        Call<UpdateDeleteUser> call = retrofit_interface.updateUser("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxNCwibmFtZSI6Im1lZ1xuIiwiZW1haWwiOiJtZWdAZ21haWwuY29tIiwiemlwQ29kZSI6MTIzLCJtb2JpbGUiOjMyMTMyMTMyMTMsInN0YXR1cyI6MSwiY3JlYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidXBkYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidHlwZSI6ImZwbyJ9LCJpYXQiOjE1NjMwMDQ0MDgsImV4cCI6MTU2NTU5NjQwOH0.Gv4P-fQM_RawGjPRY01CvSyc8WBmJmCVfvtpt_IKJDQ","farmer","xyz","UFUQ6856","114477");
        call.enqueue(new Callback<UpdateDeleteUser>() {
            @Override
            public void onResponse(Call<UpdateDeleteUser> call, Response<UpdateDeleteUser> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code update farmer ");
                    String status=String.valueOf(response.body().getStatus());
                    Logs.d("CheckStatus", status);
                    Logs.d("CheckStatus", response.body().getMessage());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<UpdateDeleteUser> call, Throwable t) {
                Log.d("CheckStatus", "update Farmer " +t.getMessage());
            }
        });
    }
    public void deleteFarmer() {
        Call<UpdateDeleteUser> call = retrofit_interface.deleteUser("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxNCwibmFtZSI6Im1lZ1xuIiwiZW1haWwiOiJtZWdAZ21haWwuY29tIiwiemlwQ29kZSI6MTIzLCJtb2JpbGUiOjMyMTMyMTMyMTMsInN0YXR1cyI6MSwiY3JlYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidXBkYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidHlwZSI6ImZwbyJ9LCJpYXQiOjE1NjMwMDQ0MDgsImV4cCI6MTU2NTU5NjQwOH0.Gv4P-fQM_RawGjPRY01CvSyc8WBmJmCVfvtpt_IKJDQ","farmer","oEinacD0");
        call.enqueue(new Callback<UpdateDeleteUser>() {
            @Override
            public void onResponse(Call<UpdateDeleteUser> call, Response<UpdateDeleteUser> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code delete farmer ");
                    String status=String.valueOf(response.body().getStatus());
                    Logs.d("CheckStatus", status);
                    Logs.d("CheckStatus", response.body().getMessage());

                } catch (Exception e) {
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<UpdateDeleteUser> call, Throwable t) {
                Log.d("CheckStatus", "delete Farmer " +t.getMessage());
            }
        });
    }
    public void updateFig() {
        Call<UpdateDeleteUser> call = retrofit_interface.updateUser("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxNCwibmFtZSI6Im1lZ1xuIiwiZW1haWwiOiJtZWdAZ21haWwuY29tIiwiemlwQ29kZSI6MTIzLCJtb2JpbGUiOjMyMTMyMTMyMTMsInN0YXR1cyI6MSwiY3JlYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidXBkYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidHlwZSI6ImZwbyJ9LCJpYXQiOjE1NjMwMDQ0MDgsImV4cCI6MTU2NTU5NjQwOH0.Gv4P-fQM_RawGjPRY01CvSyc8WBmJmCVfvtpt_IKJDQ","fig","xyz","13","112233445566");
        call.enqueue(new Callback<UpdateDeleteUser>() {
            @Override
            public void onResponse(Call<UpdateDeleteUser> call, Response<UpdateDeleteUser> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code update Fig ");
                    String status=String.valueOf(response.body().getStatus());
                    Logs.d("CheckStatus", status);
                    Logs.d("CheckStatus", response.body().getMessage());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<UpdateDeleteUser> call, Throwable t) {
                Log.d("CheckStatus", "update Fig " +t.getMessage());
            }
        });
    }
    public void deleteFig() {
        Call<UpdateDeleteUser> call = retrofit_interface.deleteUser("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxNCwibmFtZSI6Im1lZ1xuIiwiZW1haWwiOiJtZWdAZ21haWwuY29tIiwiemlwQ29kZSI6MTIzLCJtb2JpbGUiOjMyMTMyMTMyMTMsInN0YXR1cyI6MSwiY3JlYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidXBkYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidHlwZSI6ImZwbyJ9LCJpYXQiOjE1NjMwMDQ0MDgsImV4cCI6MTU2NTU5NjQwOH0.Gv4P-fQM_RawGjPRY01CvSyc8WBmJmCVfvtpt_IKJDQ","fig","14");
        call.enqueue(new Callback<UpdateDeleteUser>() {
            @Override
            public void onResponse(Call<UpdateDeleteUser> call, Response<UpdateDeleteUser> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code delete Fig ");
                    String status=String.valueOf(response.body().getStatus());
                    Logs.d("CheckStatus", status);
                    Logs.d("CheckStatus", response.body().getMessage());

                } catch (Exception e) {
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<UpdateDeleteUser> call, Throwable t) {
                Log.d("CheckStatus", "delete Fig " +t.getMessage());
            }
        });
    }


    public void logout() {
        Call<Logout> call = retrofit_interface.logout("8319073618","AOBXOK69","farmer");
        call.enqueue(new Callback<Logout>() {
            @Override
            public void onResponse(Call<Logout> call, Response<Logout> response) {
                Logs.d("CheckStatus", "onResponse: logout Success");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus().toString());
                    Logs.d("CheckStatus",response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<Logout> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }
  /*  public void updateprofile() {
        Logs.d("CheckStatus", "update profile call");
        Call<UserUpdate> call=retrofit_interface.updateprofile("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","Arvind","5487021169");
        call.enqueue(new Callback<UserUpdate>() {
            @Override
            public void onResponse(Call<UserUpdate> call, Response<UserUpdate> response) {
                Logs.d("CheckStatus", "onResponse: Success");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getCode().toString());
                    Logs.d("CheckStatus",response.body().getMessage());

                }
            }
            @Override
            public void onFailure(Call<UserUpdate> call, Throwable t) {
                Log.d("CheckStatus","fail entry");
            }
        });
    }
    */
}
