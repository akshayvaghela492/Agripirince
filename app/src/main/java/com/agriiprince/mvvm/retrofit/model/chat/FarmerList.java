package com.agriiprince.mvvm.retrofit.model.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FarmerList implements Serializable {
    @SerializedName("farmer_location")
    @Expose
    private String farmer_location;
    @SerializedName("farmer_id")
    @Expose
    private String farmer_id;
    @SerializedName("farmer_name")
    @Expose
    private String farmer_name;
    @SerializedName("subscribed_pesticide_vendor")
    @Expose
    private String subscribed_pesticide_vendor;
    @SerializedName("invited_by")
    @Expose
    private String invited_by;
    @SerializedName("farmer_contact")
    @Expose
    private String farmer_contact;

    public String getFarmer_location() {
        return farmer_location;
    }

    public void setFarmer_location(String farmer_location) {
        this.farmer_location = farmer_location;
    }

    public String getFarmer_id() {
        return farmer_id;
    }

    public void setFarmer_id(String farmer_id) {
        this.farmer_id = farmer_id;
    }

    public String getFarmer_name() {
        return farmer_name;
    }

    public void setFarmer_name(String farmer_name) {
        this.farmer_name = farmer_name;
    }

    public String getSubscribed_pesticide_vendor() {
        return subscribed_pesticide_vendor;
    }

    public void setSubscribed_pesticide_vendor(String subscribed_pesticide_vendor) {
        this.subscribed_pesticide_vendor = subscribed_pesticide_vendor;
    }

    public String getInvited_by() {
        return invited_by;
    }

    public void setInvited_by(String invited_by) {
        this.invited_by = invited_by;
    }

    public String getFarmer_contact() {
        return farmer_contact;
    }

    public void setFarmer_contact(String farmer_contact) {
        this.farmer_contact = farmer_contact;
    }
}
