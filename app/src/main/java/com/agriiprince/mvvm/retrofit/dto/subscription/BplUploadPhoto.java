package com.agriiprince.mvvm.retrofit.dto.subscription;

import com.google.gson.annotations.SerializedName;

public class BplUploadPhoto {

    @SerializedName("error_code")
    private int errorCode;

    @SerializedName("message")
    private String message;

    public int getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }
}
