package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.subscription.BplPhotoUpload;
import com.agriiprince.mvvm.retrofit.dto.subscription.CouponVerification;
import com.agriiprince.mvvm.retrofit.dto.subscription.FonepaisaStatus;
import com.agriiprince.mvvm.retrofit.dto.subscription.Fonepaise_Subscription;
import com.agriiprince.mvvm.retrofit.dto.subscription.ManualCollection;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionDetails;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionInfo;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionInitiate;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Subscription {
    @FormUrlEncoded
    @POST()
    Call<Fonepaise_Subscription> FonepaisaPayment(@Header("authentication") String token,
                                                  @Field("user_id") String userId,
                                                  @Field("user_type") String userType,
                                                  @Field("mobile_number") String mobile,
                                                  @Field("subscription_id") String subscriptionId,
                                                  @Field("coupon_code") String couponCode,
                                                  @Field("amount") String amount);


    @FormUrlEncoded
    @POST()
    Call<FonepaisaStatus> FonepaisaFailiure(@Header("authentication") String token,
                                            @Field("user_id") String userId,
                                            @Field("user_type") String userType,
                                            @Field("invoice") String invoice,
                                            @Field("payment_reference") int payment_reference,
                                            @Field("status") String status,
                                            @Field("payment_typeCredit") String payment_typeCredit,
                                            @Field("amount") int amount);


    @FormUrlEncoded
    @POST()
    Call<FonepaisaStatus> FonepaisaSuccess(@Header("authentication") String token,
                                           @Field("user_id") String userId,
                                           @Field("user_type") String userType,
                                           @Field("invoice") String invoice,
                                           @Field("payment_reference") int payment_reference,
                                           @Field("status") String status,
                                           @Field("payment_typeCredit") String payment_typeCredit,
                                           @Field("amount") int amount);


    @FormUrlEncoded
    @POST()
    Call<ManualCollection> ManualCollection(@Header("authentication") String token,
                                            @Field("user_id") String userId,
                                            @Field("user_type") String userType,
                                            @Field("collection_charges") String collection_charges,
                                            @Field("subscription_id") String subscription_id
                                           );
    @FormUrlEncoded
    @POST()
    Call<ManualCollection> verifyOTP(@Header("authentication") String token,
                                            @Field("user_id") String userId,
                                            @Field("user_type") String userType,
                                            @Field("otp") String otp
    );

    @FormUrlEncoded
    @POST()
    Call<SubscriptionInfo> SubscriptionInfo(@Header("authentication") String token,
                                            @Field("user_id") String userId,
                                            @Field("user_type") String userType,
                                            @Field("password") String password
    );
    @FormUrlEncoded
    @POST()
    Call<SubscriptionDetails> SubscriptionDetail(@Header("authentication") String token,
                                                 @Field("user_id") String userId,
                                                 @Field("password") String password,
                                                 @Field("mode") String mode
    );

    @FormUrlEncoded
    @POST()
    Call<CouponVerification> VerifyCoupon(@Header("authentication") String token,
                                          @Field("user_id") String userId,
                                          @Field("user_type") String user_type,
                                          @Field("password") String password,
                                          @Field("oe_mobile") String oe_mobile,
                                          @Field("coupon_code") String coupon_code,
                                          @Field("mode") String mode
    );
    @FormUrlEncoded
    @POST()
    Call<SubscriptionInitiate> SubscriptionInitiate(@Header("authentication") String token,
                                                      @Field("user_id") String userId,
                                                      @Field("user_type") String user_type,
                                                      @Field("merchant_key") String merchant_key,
                                                      @Field("mobile_number") String mobile_number,
                                                      @Field("product_info") String product_info,
                                                      @Field("user_name") String user_name,
                                                      @Field("email") String email,
                                                      @Field("subscription_id") String subscription_id
    );

    @FormUrlEncoded
    @POST()
    Call<SubscriptionInitiate> SubscriptionSuccess(@Header("authentication") String token,
                                                   @Field("payuMoneyId") String payuMoneyId,
                                                   @Field("mode") String mode,
                                                   @Field("status") String status,
                                                   @Field("amount") String amount,
                                                   @Field("txnid") String txnid
    );
    @FormUrlEncoded
    @POST()
    Call<SubscriptionInitiate> SubscriptionFailiure(@Header("authentication") String token,
                                                    @Field("payuMoneyId") String payuMoneyId,
                                                    @Field("mode") String mode,
                                                    @Field("status") String status,
                                                    @Field("amount") String amount,
                                                    @Field("txnid") String txnid
    );

    @FormUrlEncoded
    @POST("upload_bpl_image.php")
    Call<BplPhotoUpload> uploadBplImage(
            @Field("api_token") String apiToken, @Field("password") String password,
            @Field("user_id") String userId, @Field("image_file") String imageFile);


}
