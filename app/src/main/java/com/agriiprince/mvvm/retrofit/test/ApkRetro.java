package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.apkversions.ApkVers;
import com.agriiprince.mvvm.retrofit.service.ApkVersion;
import com.agriiprince.mvvm.util.Logs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApkRetro {

    API_Manager api_manager=new API_Manager();
    ApkVersion retrofit_interface = api_manager.getClient3().create(ApkVersion.class);

    public void CheckApkVersion() {
        Call<ApkVers> call = retrofit_interface.ApkVersionCheck("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","1.0", "oe");
       call.enqueue(new Callback<ApkVers>() {
           @Override
           public void onResponse(Call<ApkVers> call, Response<ApkVers> response) {
               Logs.d("CheckStatus", "onResponse: Success");
               Logs.d("CheckStatus",response.body().getCode().toString());
               Logs.d("CheckStatus",response.body().getMessage());
               Logs.d("CheckStatus",response.body().getData().getLink());

           }
           @Override
           public void onFailure(Call<ApkVers> call, Throwable t) {
               Log.d("CheckStatus", "fail entry");
           }
       });
    }
}
