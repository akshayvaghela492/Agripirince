package com.agriiprince.mvvm.retrofit.model.razorpay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ManualSubscriptionData implements Serializable
{

    @SerializedName("data")
    @Expose
    private ManualSubscriptionDetails data;


    public ManualSubscriptionDetails getData() {
        return data;
    }

    public void setData(ManualSubscriptionDetails data) {
        this.data = data;
    }

}