package com.agriiprince.mvvm.retrofit.model.ca;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CaListModel implements Serializable {
    @SerializedName("ca_id")
    @Expose
    private Integer caId;
    @SerializedName("ca_name")
    @Expose
    private String caName;
    @SerializedName("ca_address")
    @Expose
    private String caAddress;
    @SerializedName("ca_contact")
    @Expose
    private String caContact;
    @SerializedName("commodity")
    @Expose
    private String commodity;
    @SerializedName("pincode")
    @Expose
    private Integer pincode;
    @SerializedName("ca_lat")
    @Expose
    private String caLat;
    @SerializedName("ca_lng")
    @Expose
    private String caLng;
    @SerializedName("ca_mandi")
    @Expose
    private String caMandi;
    @SerializedName("ca_city")
    @Expose
    private String caCity;
    @SerializedName("fair_price_rating")
    @Expose
    private Integer fairPriceRating;
    @SerializedName("quick_payment_rating")
    @Expose
    private Integer quickPaymentRating;
    @SerializedName("polite_mannerisms_rating")
    @Expose
    private Integer politeMannerismsRating;
    @SerializedName("complete_payment_rating")
    @Expose
    private Integer completePaymentRating;
    @SerializedName("will_visit_again_rating")
    @Expose
    private Integer willVisitAgainRating;
    @SerializedName("ca_name_bi")
    @Expose
    private String caNameBi;
    @SerializedName("ca_address_bi")
    @Expose
    private String caAddressBi;
    @SerializedName("commodity_bi")
    @Expose
    private String commodityBi;
    @SerializedName("ca_city_bi")
    @Expose
    private String caCityBi;
    @SerializedName("ca_mandi_bi")
    @Expose
    private String caMandiBi;
    @SerializedName("ca_business_name")
    @Expose
    private String ca_business_name;
    @SerializedName("ca_business_name_bi")
    @Expose
    private String ca_business_name_bi;

    public Integer getCaId() {
        return caId;
    }

    public void setCaId(Integer caId) {
        this.caId = caId;
    }

    public String getCaName() {
        return caName;
    }

    public void setCaName(String caName) {
        this.caName = caName;
    }

    public String getCaAddress() {
        return caAddress;
    }

    public void setCaAddress(String caAddress) {
        this.caAddress = caAddress;
    }

    public String getCaContact() {
        return caContact;
    }

    public void setCaContact(String caContact) {
        this.caContact = caContact;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public Integer getPincode() {
        return pincode;
    }

    public void setPincode(Integer pincode) {
        this.pincode = pincode;
    }

    public String getCaLat() {
        return caLat;
    }

    public void setCaLat(String caLat) {
        this.caLat = caLat;
    }

    public String getCaLng() {
        return caLng;
    }

    public void setCaLng(String caLng) {
        this.caLng = caLng;
    }

    public String getCaMandi() {
        return caMandi;
    }

    public void setCaMandi(String caMandi) {
        this.caMandi = caMandi;
    }

    public String getCaCity() {
        return caCity;
    }

    public void setCaCity(String caCity) {
        this.caCity = caCity;
    }

    public Integer getFairPriceRating() {
        return fairPriceRating;
    }

    public void setFairPriceRating(Integer fairPriceRating) {
        this.fairPriceRating = fairPriceRating;
    }

    public Integer getQuickPaymentRating() {
        return quickPaymentRating;
    }

    public void setQuickPaymentRating(Integer quickPaymentRating) {
        this.quickPaymentRating = quickPaymentRating;
    }

    public Integer getPoliteMannerismsRating() {
        return politeMannerismsRating;
    }

    public void setPoliteMannerismsRating(Integer politeMannerismsRating) {
        this.politeMannerismsRating = politeMannerismsRating;
    }

    public Integer getCompletePaymentRating() {
        return completePaymentRating;
    }

    public void setCompletePaymentRating(Integer completePaymentRating) {
        this.completePaymentRating = completePaymentRating;
    }

    public Integer getWillVisitAgainRating() {
        return willVisitAgainRating;
    }

    public void setWillVisitAgainRating(Integer willVisitAgainRating) {
        this.willVisitAgainRating = willVisitAgainRating;
    }

    public String getCaNameBi() {
        return caNameBi;
    }

    public void setCaNameBi(String caNameBi) {
        this.caNameBi = caNameBi;
    }

    public String getCaAddressBi() {
        return caAddressBi;
    }

    public void setCaAddressBi(String caAddressBi) {
        this.caAddressBi = caAddressBi;
    }

    public String getCommodityBi() {
        return commodityBi;
    }

    public void setCommodityBi(String commodityBi) {
        this.commodityBi = commodityBi;
    }

    public String getCaCityBi() {
        return caCityBi;
    }

    public void setCaCityBi(String caCityBi) {
        this.caCityBi = caCityBi;
    }

    public String getCaMandiBi() {
        return caMandiBi;
    }

    public void setCaMandiBi(String caMandiBi) {
        this.caMandiBi = caMandiBi;
    }

    public String getCa_business_name() {
        return ca_business_name;
    }

    public void setCa_business_name(String ca_business_name) {
        this.ca_business_name = ca_business_name;
    }

    public String getCa_business_name_bi() {
        return ca_business_name_bi;
    }

    public void setCa_business_name_bi(String ca_business_name_bi) {
        this.ca_business_name_bi = ca_business_name_bi;
    }
}