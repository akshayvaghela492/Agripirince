package com.agriiprince.mvvm.retrofit.model.ca;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CaDataList implements Serializable {
    @SerializedName("ca_rating")
    @Expose
    private List<CaListModel> caRating = null;


    public List<CaListModel> getCaRating() {
        return caRating;
    }

    public void setCaRating(List<CaListModel> caRating) {
        this.caRating = caRating;
    }
}
