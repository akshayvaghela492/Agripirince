package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.mvvm.retrofit.dto.trackcrop.TrackCrop;
import com.agriiprince.mvvm.retrofit.dto.trackcrop.TrackSaveCrop;
import com.agriiprince.mvvm.retrofit.dto.trackcrop.TrackSaveOrder;
import com.agriiprince.mvvm.retrofit.model.trackcrop.TrackGetCrop;
import com.agriiprince.mvvm.retrofit.service.TrackCropService;
import com.agriiprince.mvvm.util.Logs;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackCropRetro {
    TrackApiManager apiManager=new TrackApiManager();
    TrackCropService retrofit_interface = apiManager.getClient().create(TrackCropService.class);

    public void saveCrop() {
        Call<TrackSaveCrop> call = retrofit_interface.saveCrop(1,"Abc","Abc","Abc","Abc","Abc","Abc","Abc","Abc","Abc","Abc","Abc","Abc","Abc","Abc");
        call.enqueue(new Callback<TrackSaveCrop>() {
            @Override
            public void onResponse(Call<TrackSaveCrop> call, Response<TrackSaveCrop> response) {
                Logs.d("CheckStatus", "onResponse: code Track Save crop " );
                Logs.d("CheckStatus",String.valueOf(response.body().getStatus()));
                Logs.d("CheckStatus",response.body().getMessage());
                Logs.d("CheckStatus",response.body().getData().getTcropId().toString());
            }

            @Override
            public void onFailure(Call<TrackSaveCrop> call, Throwable t) {
                Log.d("CheckStatus", "Track save crop error "+ t.getMessage());
            }
        });
    }
    public void saveOrder() {
        Call<TrackSaveOrder> call = retrofit_interface.saveOrder(1,1,1,"1","abcd","5","aa","1","1","farmer");
        call.enqueue(new Callback<TrackSaveOrder>() {
            @Override
            public void onResponse(Call<TrackSaveOrder> call, Response<TrackSaveOrder> response) {
                Logs.d("CheckStatus", "onResponse: code Track Save order " );
                Logs.d("CheckStatus",String.valueOf(response.body().getStatus()));
                Logs.d("CheckStatus",response.body().getMessage());
            }

            @Override
            public void onFailure(Call<TrackSaveOrder> call, Throwable t) {
                Log.d("CheckStatus", "Track save order error "+ t.getMessage());
            }
        });
    }
    public void getCrop() {
        Call<com.agriiprince.mvvm.retrofit.dto.trackcrop.TrackGetCrop> call = retrofit_interface.getCrop(1);
        call.enqueue(new Callback<com.agriiprince.mvvm.retrofit.dto.trackcrop.TrackGetCrop>() {
            @Override
            public void onResponse(Call<com.agriiprince.mvvm.retrofit.dto.trackcrop.TrackGetCrop> call, Response<com.agriiprince.mvvm.retrofit.dto.trackcrop.TrackGetCrop> response) {
                Logs.d("CheckStatus", "onResponse: code Track get crop " );
                Logs.d("CheckStatus",String.valueOf(response.body().getStatus()));
                Logs.d("CheckStatus",response.body().getMessage());
                List<TrackGetCrop> cropModels=response.body().getData();
                for (int i=0;i<=cropModels.size();i++) {
                    Logs.d("CheckStatus",response.body().getData().get(3).getCrop());
                }
            }

            @Override
            public void onFailure(Call<com.agriiprince.mvvm.retrofit.dto.trackcrop.TrackGetCrop> call, Throwable t) {
                Log.d("CheckStatus", "Track get crop error "+ t.getMessage());
            }
        });
    }

    public void getActivitylist() {
        Call<TrackCrop> call = retrofit_interface.getActivitylist(1);
        call.enqueue(new Callback<TrackCrop>() {
            @Override
            public void onResponse(Call<TrackCrop> call, Response<TrackCrop> response) {
                Logs.d("CheckStatus", "onResponse: code Track crop get activity list" );
                Logs.d("CheckStatus",String.valueOf(response.body().getStatus()));
                Logs.d("CheckStatus",response.body().getMessage());
            }

            @Override
            public void onFailure(Call<TrackCrop> call, Throwable t) {
                Log.d("CheckStatus", "Track crop get activity list error "+ t.getMessage());
            }
        });
    }
    public void getActivityDetails() {
        Call<TrackCrop> call = retrofit_interface.getActivityDetails(1);
        call.enqueue(new Callback<TrackCrop>() {
            @Override
            public void onResponse(Call<TrackCrop> call, Response<TrackCrop> response) {
                Logs.d("CheckStatus", "onResponse: code Track crop get Activity Details" );
                Logs.d("CheckStatus",String.valueOf(response.body().getStatus()));
                Logs.d("CheckStatus",response.body().getMessage());
                
            }

            @Override
            public void onFailure(Call<TrackCrop> call, Throwable t) {
                Log.d("CheckStatus", "Track crop get Activity Details error "+ t.getMessage());
            }
        });
    }
    public void getSprinklers() {
        Call<TrackCrop> call = retrofit_interface.getSprinklers(1);
        call.enqueue(new Callback<TrackCrop>() {
            @Override
            public void onResponse(Call<TrackCrop> call, Response<TrackCrop> response) {
                Logs.d("CheckStatus", "onResponse: code Track crop get Sprinklers" );
                Logs.d("CheckStatus",String.valueOf(response.body().getStatus()));
                Logs.d("CheckStatus",response.body().getMessage());
            }

            @Override
            public void onFailure(Call<TrackCrop> call, Throwable t) {
                Log.d("CheckStatus", "Track crop get Sprinklers error "+ t.getMessage());
            }
        });
    }
    public void getPesticide() {
        Call<TrackCrop> call = retrofit_interface.getPesticides(1);
        call.enqueue(new Callback<TrackCrop>() {
            @Override
            public void onResponse(Call<TrackCrop> call, Response<TrackCrop> response) {
                Logs.d("CheckStatus", "onResponse: code Track crop get Pesticides" );
                Logs.d("CheckStatus",String.valueOf(response.body().getStatus()));
                Logs.d("CheckStatus",response.body().getMessage());
            }

            @Override
            public void onFailure(Call<TrackCrop> call, Throwable t) {
                Log.d("CheckStatus", "Track crop get Pesticides error "+ t.getMessage());
            }
        });
    }
    public void getFerilizers() {
        Call<TrackCrop> call = retrofit_interface.getFertilizers(1);
        call.enqueue(new Callback<TrackCrop>() {
            @Override
            public void onResponse(Call<TrackCrop> call, Response<TrackCrop> response) {
                Logs.d("CheckStatus", "onResponse: code Track crop get Fertilizers" );
                Logs.d("CheckStatus",String.valueOf(response.body().getStatus()));
                Logs.d("CheckStatus",response.body().getMessage());
            }

            @Override
            public void onFailure(Call<TrackCrop> call, Throwable t) {
                Log.d("CheckStatus", "Track crop get Fertilizers error "+ t.getMessage());
            }
        });
    }
    public void getSuitability() {
        Call<TrackCrop> call = retrofit_interface.getSuitability(1);
        call.enqueue(new Callback<TrackCrop>() {
            @Override
            public void onResponse(Call<TrackCrop> call, Response<TrackCrop> response) {
                Logs.d("CheckStatus", "onResponse: code Track crop get Suitability" );
                Logs.d("CheckStatus",String.valueOf(response.body().getStatus()));
                Logs.d("CheckStatus",response.body().getMessage());
            }

            @Override
            public void onFailure(Call<TrackCrop> call, Throwable t) {
                Log.d("CheckStatus", "Track crop get Suitability error "+ t.getMessage());
            }
        });
    }
}
