package com.agriiprince.mvvm.retrofit.model.tutorial;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TutorialSections implements Serializable
{@SerializedName("section_code")
@Expose
private String sectionCode;
    @SerializedName("section_title")
    @Expose
    private String sectionTitle;
    @SerializedName("section_content")
    @Expose
    private String sectionContent;
    @SerializedName("sub_sections")
    @Expose
    private List<SubSection> subSections = null;


    public String getSectionCode() {
        return sectionCode;
    }

    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }

    public String getSectionTitle() {
        return sectionTitle;
    }

    public void setSectionTitle(String sectionTitle) {
        this.sectionTitle = sectionTitle;
    }

    public String getSectionContent() {
        return sectionContent;
    }

    public void setSectionContent(String sectionContent) {
        this.sectionContent = sectionContent;
    }

    public List<SubSection> getSubSections() {
        return subSections;
    }

    public void setSubSections(List<SubSection> subSections) {
        this.subSections = subSections;
    }
}
