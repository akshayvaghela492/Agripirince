package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.tutorial.TutorialResponse;
import com.agriiprince.mvvm.retrofit.model.tutorial.LanguageRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Tutorial {
    @FormUrlEncoded
    @POST("tutorial")
    Call<TutorialResponse> tutorial(@Header("authentication") String token,
                                    @Field("tutorialLanguage") String language);
}
