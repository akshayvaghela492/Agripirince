package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.retrofit.dto.subscription.BplPhotoUpload;
import com.agriiprince.mvvm.retrofit.dto.subscription.CouponVerification;
import com.agriiprince.mvvm.retrofit.dto.subscription.FonepaisaStatus;
import com.agriiprince.mvvm.retrofit.dto.subscription.Fonepaise_Subscription;
import com.agriiprince.mvvm.retrofit.dto.subscription.ManualCollection;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionDetails;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionInfo;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionInitiate;
import com.agriiprince.mvvm.retrofit.service.Subscription;
import com.agriiprince.mvvm.util.Logs;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SubscriptionRetro {
    private Retrofit retrofit = null;

    public Retrofit getClient() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    Subscription retrofit_interface = getClient().create(Subscription.class);

    public void getSubscription() {
        Call<Fonepaise_Subscription> call = retrofit_interface.FonepaisaPayment("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","","","","","","");
        call.enqueue(new Callback<Fonepaise_Subscription>() {
            @Override
            public void onResponse(Call<Fonepaise_Subscription> call, Response<Fonepaise_Subscription> response) {
                Logs.d("CheckStatus", "onResponse: Success");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                    Logs.d("CheckStatus",response.body().getData().getTransaction_id());
                }
            }

            @Override
            public void onFailure(Call<Fonepaise_Subscription> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }
    public void SubscriptionSuccess() {
        Call<FonepaisaStatus> call = retrofit_interface.FonepaisaSuccess("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","","","",1,"","",11);
        call.enqueue(new Callback<FonepaisaStatus>() {
            @Override
            public void onResponse(Call<FonepaisaStatus> call, Response<FonepaisaStatus> response) {
                Logs.d("CheckStatus", "onResponse: Success");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                }
            }

            @Override
            public void onFailure(Call<FonepaisaStatus> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }
    public void SubscriptionFail() {
        Call<FonepaisaStatus> call = retrofit_interface.FonepaisaFailiure("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","","","",1,"","",11);
        call.enqueue(new Callback<FonepaisaStatus>() {
            @Override
            public void onResponse(Call<FonepaisaStatus> call, Response<FonepaisaStatus> response) {
                Logs.d("CheckStatus", "onResponse: Success");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                }
            }

            @Override
            public void onFailure(Call<FonepaisaStatus> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }

    public void manualcollection() {
        Call<ManualCollection> call = retrofit_interface.ManualCollection("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","","","","");
        call.enqueue(new Callback<ManualCollection>() {
            @Override
            public void onResponse(Call<ManualCollection> call, Response<ManualCollection> response) {
                Logs.d("CheckStatus", "onResponse: Success");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                }
            }

            @Override
            public void onFailure(Call<ManualCollection> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }
    public void verifyotp() {
        Call<ManualCollection> call = retrofit_interface.verifyOTP("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","","","");
        call.enqueue(new Callback<ManualCollection>() {
            @Override
            public void onResponse(Call<ManualCollection> call, Response<ManualCollection> response) {
                Logs.d("CheckStatus", "onResponse: Success");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                }
            }

            @Override
            public void onFailure(Call<ManualCollection> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }
    public void SubscriptionInfo() {
        Call<SubscriptionInfo> call = retrofit_interface.SubscriptionInfo("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","","","");
        call.enqueue(new Callback<SubscriptionInfo>() {
            @Override
            public void onResponse(Call<SubscriptionInfo> call, Response<SubscriptionInfo> response) {
                Logs.d("CheckStatus", "onResponse: Success");
                if (response.isSuccessful() && response.body() != null) {
                    //Logs.d("CheckStatus",response.body().getStatus());
                    //Logs.d("CheckStatus",response.body().getMessage());
                    //Logs.d("CheckStatus",response.body().getData().getSubscription_expiry_date());
                    //Logs.d("CheckStatus",response.body().getCode().toString());
                }
            }

            @Override
            public void onFailure(Call<SubscriptionInfo> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }

    public void SubscriptionDetails() {
        Call<SubscriptionDetails> call = retrofit_interface.SubscriptionDetail("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","","","");
        call.enqueue(new Callback<SubscriptionDetails>() {
            @Override
            public void onResponse(Call<SubscriptionDetails> call, Response<SubscriptionDetails> response) {
                Logs.d("CheckStatus", "onResponse: Success");
                if (response.isSuccessful() && response.body() != null) {
                    //Logs.d("CheckStatus",response.body().getStatus());
                    //Logs.d("CheckStatus",response.body().getMessage());
                    //Logs.d("CheckStatus",response.body().getData().getSubscription_id().toString());
                    //Logs.d("CheckStatus",response.body().getCode().toString());
                }
            }

            @Override
            public void onFailure(Call<SubscriptionDetails> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }

    public void VerifyCoupon() {
        Call<CouponVerification> call = retrofit_interface.VerifyCoupon("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","","","","","","");
        call.enqueue(new Callback<CouponVerification>() {
            @Override
            public void onResponse(Call<CouponVerification> call, Response<CouponVerification> response) {
                Logs.d("CheckStatus", "onResponse: Success");
                if (response.isSuccessful() && response.body() != null) {
                    //Logs.d("CheckStatus",response.body().getStatus());
                    //Logs.d("CheckStatus",response.body().getMessage());
                    //Logs.d("CheckStatus",response.body().getData().get(1).getDiscount_in_percent().toString());
                    //Logs.d("CheckStatus",response.body().getCode().toString());
                }
            }

            @Override
            public void onFailure(Call<CouponVerification> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }

    public void SubscriptionInitiate() {
        Call<SubscriptionInitiate> call = retrofit_interface.SubscriptionInitiate("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","","","","","","","","");
        call.enqueue(new Callback<SubscriptionInitiate>() {
            @Override
            public void onResponse(Call<SubscriptionInitiate> call, Response<SubscriptionInitiate> response) {
                Logs.d("CheckStatus", "onResponse: Success");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getData().getTransactionId());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                }
            }

            @Override
            public void onFailure(Call<SubscriptionInitiate> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }
    public void Subscription_Success() {
        Call<SubscriptionInitiate> call = retrofit_interface.SubscriptionSuccess("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","","","","","");
        call.enqueue(new Callback<SubscriptionInitiate>() {
            @Override
            public void onResponse(Call<SubscriptionInitiate> call, Response<SubscriptionInitiate> response) {
                Logs.d("CheckStatus", "onResponse: Success");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                }
            }

            @Override
            public void onFailure(Call<SubscriptionInitiate> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }
    public void Subscription_Failiure() {
        Call<SubscriptionInitiate> call = retrofit_interface.SubscriptionFailiure("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","","","","","");
        call.enqueue(new Callback<SubscriptionInitiate>() {
            @Override
            public void onResponse(Call<SubscriptionInitiate> call, Response<SubscriptionInitiate> response) {
                Logs.d("CheckStatus", "onResponse: Success");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                }
            }

            @Override
            public void onFailure(Call<SubscriptionInitiate> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }


    public void saveBplImage() {
        File file = new File("/storage/emulated/0/Download/image.jpg");
        final RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);

  /*      Call<BplPhotoUpload> call = retrofit_interface.SaveBplImage("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","","",body);
        call.enqueue(new Callback<BplPhotoUpload>() {
            @Override
            public void onResponse(Call<BplPhotoUpload> call, Response<BplPhotoUpload> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code save image ");
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());


                }catch (Exception e){e.getMessage();}

            }

            @Override
            public void onFailure(Call<BplPhotoUpload> call, Throwable t) {
                Log.d("CheckStatus", "save image  "+ t.getMessage());
            }
        });
    */
    }
}
