
package com.agriiprince.mvvm.retrofit.model.notification;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Personal implements Serializable
{

    @SerializedName("notifications")
    @Expose
    private List<Notification_> notifications = null;
    @SerializedName("total_unread_notifications")
    @Expose
    private Integer totalUnreadNotifications;


    public List<Notification_> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification_> notifications) {
        this.notifications = notifications;
    }

    public Integer getTotalUnreadNotifications() {
        return totalUnreadNotifications;
    }

    public void setTotalUnreadNotifications(Integer totalUnreadNotifications) {
        this.totalUnreadNotifications = totalUnreadNotifications;
    }

}
