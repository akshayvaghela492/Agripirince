package com.agriiprince.mvvm.retrofit.dto.trackcrop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TrackSaveCrop implements Serializable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private com.agriiprince.mvvm.retrofit.model.trackcrop.TrackSaveCrop data;


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public com.agriiprince.mvvm.retrofit.model.trackcrop.TrackSaveCrop getData() {
        return data;
    }

    public void setData(com.agriiprince.mvvm.retrofit.model.trackcrop.TrackSaveCrop data) {
        this.data = data;
    }

}
