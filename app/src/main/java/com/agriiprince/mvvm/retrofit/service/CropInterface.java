package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.tradecrop.FarmerCrop;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.DeleteCrop;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.DeleteCropMedia;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.Media;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.SaveCrop;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.SaveMedia;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface CropInterface {

    @GET("list/user/{id}")
    Call<FarmerCrop> getFarmercrop(
            @Path("id") String user_id,@Header("authentication") String token);

    @GET("{id}/Media/list/0/{limit}")
    Call<Media> getMedialist(@Path("id") String media_id, @Path("limit") String limit, @Header("authentication") String token);

    @GET("delete/Media/{id}")
    Call<DeleteCropMedia> deleteCropMedia(@Path("id") String media_id,@Header("authentication") String token);


    @Multipart
    @POST("{id}/save/Media/image")
    Call<SaveMedia> SaveImage(@Path("id") String id,
                              @Header("authentication") String token,
                              @Part MultipartBody.Part file);


    @Multipart
    @POST("{id}/save/Media/video")
    Call<SaveMedia> SaveVideo(@Path("id") String id,
                              @Header("authentication") String token,
                              @Part MultipartBody.Part file);


    @FormUrlEncoded
    @POST("fig/{id}/create")
    Call<SaveCrop> SaveCropFig(@Path("id") String id,

                               @Header("authentication") String token,
                               @Field("sowingCrop") String sowingCrop,
                               @Field("sowingCropVariety") String sowingCropVariety,
                               @Field("sowingCropGrade") String sowingCropGrade,
                               @Field("sowingCropDate") String sowingCropDate,
                               @Field("sowingCropArea") String sowingCropArea,
                               @Field("sowingCropAreaUnit") String sowingCropAreaUnit,
                               @Field("growingCropSeedType") String growingCropSeedType,
                               @Field("growingCropSeedPesticide") String growingCropSeedPesticide,
                               @Field("growingCropSeedFertilizer") String growingCropSeedFertilizer,
                               @Field("harvestingCropGrade1") String harvestingCropGrade1,
                               @Field("harvestingCropGrade2") String harvestingCropGrade2,
                               @Field("harvestingCropGrade3") String harvestingCropGrade3,
                               @Field("harvestingCropStartDate") String harvestingCropStartDate,
                               @Field("harvestingCropFrequency") String harvestingCropFrequency,
                               @Field("harvestingCropFrequencyTime") String harvestingCropFrequencyTime,
                               @Field("harvestingCropFrequencyTimeLine") String harvestingCropFrequencyTimeLine,
                               @Field("harvestingCropTotalPeriod") String harvestingCropTotalPeriod
                                    );

    @FormUrlEncoded
    @POST("fpo/{id}/create")
    Call<SaveCrop> SaveCropFpo(@Path("id") String id,

                               @Header("authentication") String token,

                               @Field("sowingCrop") String sowingCrop,
                               @Field("sowingCropVariety") String sowingCropVariety,
                               @Field("sowingCropGrade") String sowingCropGrade,
                               @Field("sowingCropDate") String sowingCropDate,
                               @Field("sowingCropArea") String sowingCropArea,
                               @Field("sowingCropAreaUnit") String sowingCropAreaUnit,
                               @Field("growingCropSeedType") String growingCropSeedType,
                               @Field("growingCropSeedPesticide") String growingCropSeedPesticide,
                               @Field("growingCropSeedFertilizer") String growingCropSeedFertilizer,
                               @Field("harvestingCropGrade1") String harvestingCropGrade1,
                               @Field("harvestingCropGrade2") String harvestingCropGrade2,
                               @Field("harvestingCropGrade3") String harvestingCropGrade3,
                               @Field("harvestingCropStartDate") String harvestingCropStartDate,
                               @Field("harvestingCropFrequency") String harvestingCropFrequency,
                               @Field("harvestingCropFrequencyTime") String harvestingCropFrequencyTime,
                               @Field("harvestingCropFrequencyTimeLine") String harvestingCropFrequencyTimeLine,
                               @Field("harvestingCropTotalPeriod") String harvestingCropTotalPeriod
                                 );
    @FormUrlEncoded
    @POST("farmer/{id}/create")
    Call<SaveCrop> SaveCropFarmer(@Path("id") String id,
                                  @Header("authentication") String token,
                                  @Field("sowingCrop") String sowingCrop,
                                  @Field("sowingCropVariety") String sowingCropVariety,
                                  @Field("sowingCropGrade") String sowingCropGrade,
                                  @Field("sowingCropDate") String sowingCropDate,
                                  @Field("sowingCropArea") String sowingCropArea,
                                  @Field("sowingCropAreaUnit") String sowingCropAreaUnit,
                                  @Field("growingCropSeedType") String growingCropSeedType,
                                  @Field("growingCropSeedPesticide") String growingCropSeedPesticide,
                                  @Field("growingCropSeedFertilizer") String growingCropSeedFertilizer,
                                  @Field("harvestingCropGrade1") String harvestingCropGrade1,
                                  @Field("harvestingCropGrade2") String harvestingCropGrade2,
                                  @Field("harvestingCropGrade3") String harvestingCropGrade3,
                                  @Field("harvestingCropStartDate") String harvestingCropStartDate,
                                  @Field("harvestingCropFrequency") String harvestingCropFrequency,
                                  @Field("harvestingCropFrequencyTime") String harvestingCropFrequencyTime,
                                  @Field("harvestingCropFrequencyTimeLine") String harvestingCropFrequencyTimeLine,
                                  @Field("harvestingCropTotalPeriod") String harvestingCropTotalPeriod
    );

    @GET("delete/crop/{id}")
    Call<DeleteCrop> deleteCrop(@Path("id") String crop_id,
                                @Header("authentication") String token);
}
