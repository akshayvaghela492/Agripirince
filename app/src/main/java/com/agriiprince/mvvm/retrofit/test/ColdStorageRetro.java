package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.coldstorage.ColdStorageResponse;
import com.agriiprince.mvvm.retrofit.model.coldstorage.ColdStorageList;
import com.agriiprince.mvvm.retrofit.service.ColdStorages;
import com.agriiprince.mvvm.util.Logs;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ColdStorageRetro {
    API_Manager api_manager=new API_Manager();
    ColdStorages retrofit_interface = api_manager.getClient4().create(ColdStorages.class);

    public void getcoldstorage() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id","VGMG888");
        params.put("user_type","farmer");
        params.put("primary_language","en");
        params.put("secondary_language","kn");
        params.put("latitude","22.69863");
        params.put("longitude","75.88883");
        params.put("max_distance","250000");

        Call<ColdStorageResponse> call=retrofit_interface.getcoldstorage("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTk1MzU4NCwiZXhwIjoxNTY0NTQ1NTg0fQ.vu6qstszSI4kp_l-r1ogOnYM3-Nj0wFDKVewf1P-7ZI",params);
        call.enqueue(new Callback<ColdStorageResponse>() {
            @Override
            public void onResponse(Call<ColdStorageResponse> call, Response<ColdStorageResponse> response) {
                Logs.d("CheckStatus", "onResponse: cold storage");
                Logs.d("CheckStatus",response.toString());
                Logs.d("CheckStatus", response.body().getStatus());
                Logs.d("CheckStatus", response.body().getCode().toString());
                Logs.d("CheckStatus", response.body().getMessage());
                if (response.isSuccessful() && response.body() != null) {
                    List<ColdStorageList> data=response.body().getData();
                    Logs.d("CheckStatus",data.get(1).getColdStorageName());
                }
            }
            @Override
            public void onFailure(Call<ColdStorageResponse> call, Throwable t) {
                Log.d("CheckStatus","cold storage error "+t.getMessage());
            }
        });
    }
}
