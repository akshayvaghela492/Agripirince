package com.agriiprince.mvvm.retrofit.model.subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SubscriptionDataInfo implements Serializable {

    @SerializedName("data")
    @Expose
    private List<SubscriptionInformation> data;

    @SerializedName( "bpl_status")
    @Expose
    private String bpl_status;

    public String getBpl_status() {
        return bpl_status;
    }

    public void setBpl_status(String bpl_status) {
        this.bpl_status = bpl_status;
    }

    public List<SubscriptionInformation> getData() {
        return data;
    }

    public void setData(List<SubscriptionInformation> data) {
        this.data = data;
    }
}
