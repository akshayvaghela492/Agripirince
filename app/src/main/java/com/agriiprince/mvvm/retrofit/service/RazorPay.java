package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.razorpay.CheckoutResponse;
import com.agriiprince.mvvm.retrofit.dto.razorpay.ManualSubscriptionResponse;
import com.agriiprince.mvvm.retrofit.dto.razorpay.RazorPayResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface RazorPay {
    @FormUrlEncoded
    @POST("generate_order_id")
    Call<RazorPayResponse> get_order_id(@Header("authentication") String token,
                                          @Field("amount") String amount,
                                          @Field("currency") String currency,

                                          @Field("farmer_id") String farmer_id);


    @FormUrlEncoded
    @POST("verify_checkout")
    Call<CheckoutResponse> checkout_capture_payment(@Header("authentication") String token,
                                                    @Field("razorpay_payment_id") String razorpay_payment_id,
                                                    @Field("razorpay_order_id") String razorpay_order_id,
                                                    @Field("razorpay_signature") String razorpay_signature,


                                                    @Field("subscription_id") String subscription_id,
                                                    @Field("farmer_id") String farmer_id);


}
