package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.weather.WeatherResponse;
import com.agriiprince.mvvm.retrofit.service.WeatherForecast;
import com.agriiprince.mvvm.util.Logs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherRetro {

    API_Manager api_manager=new API_Manager();
   WeatherForecast retrofit_interface = api_manager.getClient9().create(WeatherForecast.class);

    public void WeatherForecast() {
        Call<WeatherResponse> call = retrofit_interface.WeatherResponse("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJBT0JYT0s2OSIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTU1MDMyOSwiZXhwIjoxNTY0MTQyMzI5fQ.eIYhX8sqI6WIby1MXIkdub8kjRR64u2pA9tHyrRKtHI","2018-10-23", "delhi");
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                Logs.d("CheckStatus", "onResponse: Success");
              try {
                  int code = response.code();
                  Logs.d("CheckStatus", "onResponse: code " + code);
                  Logs.d("CheckStatus",response.body().getData().getDailyForecasts().get(1).getDate());
                  Logs.d("CheckStatus",response.body().getData().getDailyForecasts().get(1).getDay().getShortPhrase());
                  Logs.d("CheckStatus",response.body().getData().getDailyForecasts().get(1).getTemperature().getMaximum().getUnit());
              }
                   catch (Exception e){e.getMessage();}
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                Log.d("CheckStatus", "fail entry"+t.getMessage());
            }
        });

    }
}
