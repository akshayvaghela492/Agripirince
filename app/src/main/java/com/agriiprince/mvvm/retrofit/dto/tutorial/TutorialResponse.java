package com.agriiprince.mvvm.retrofit.dto.tutorial;

import com.agriiprince.mvvm.retrofit.model.apk_version.Apk_links;
import com.agriiprince.mvvm.retrofit.model.tutorial.TutorialModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TutorialResponse implements Serializable {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private TutorialModel data;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private Integer code;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public TutorialModel getData() {
        return data;
    }

    public void setData(TutorialModel data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
