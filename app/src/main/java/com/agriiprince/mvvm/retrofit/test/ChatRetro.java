package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.chat.FetchMessage;
import com.agriiprince.mvvm.retrofit.dto.chat.GetCommodity;
import com.agriiprince.mvvm.retrofit.dto.chat.GetFarmerList;
import com.agriiprince.mvvm.retrofit.dto.chat.MessageStartUp;
import com.agriiprince.mvvm.retrofit.dto.chat.MessageStatus;
import com.agriiprince.mvvm.retrofit.dto.chat.PincodeCommodity;
import com.agriiprince.mvvm.retrofit.dto.chat.SendMessageResponse;
import com.agriiprince.mvvm.retrofit.service.Chat;
import com.agriiprince.mvvm.util.Logs;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChatRetro {

    private Retrofit retrofit = null;

    public Retrofit getClient() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    Chat retrofit_interface=getClient().create(Chat.class);

    public void fetchmsg() {
        Logs.d("CheckStatus", "coldstorage call");
        Call<FetchMessage> call=retrofit_interface.fetch_chat_message("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTk1MzU4NCwiZXhwIjoxNTY0NTQ1NTg0fQ.vu6qstszSI4kp_l-r1ogOnYM3-Nj0wFDKVewf1P-7ZI","","","","","","");
        call.enqueue(new Callback<FetchMessage>() {
            @Override
            public void onResponse(Call<FetchMessage> call, Response<FetchMessage> response) {
                Logs.d("CheckStatus", "onResponse: fetchmsg ");
                Logs.d("CheckStatus", response.body().getStatus());
                Logs.d("CheckStatus", response.body().getCode().toString());
                Logs.d("CheckStatus", response.body().getMessage());
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getData().get(1).getId());
                }
            }
            @Override
            public void onFailure(Call<FetchMessage> call, Throwable t) {
                Log.d("CheckStatus"," error "+t.getMessage());
            }
        });
    }
    public void get_msg_startup() {
        Logs.d("CheckStatus", "coldstorage call");
        Call<MessageStartUp> call=retrofit_interface.get_chat_message("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTk1MzU4NCwiZXhwIjoxNTY0NTQ1NTg0fQ.vu6qstszSI4kp_l-r1ogOnYM3-Nj0wFDKVewf1P-7ZI","","","");
        call.enqueue(new Callback<MessageStartUp>() {
            @Override
            public void onResponse(Call<MessageStartUp> call, Response<MessageStartUp> response) {
                Logs.d("CheckStatus", "onResponse: fetchmsg ");
                Logs.d("CheckStatus", response.body().getStatus());
                Logs.d("CheckStatus", response.body().getCode().toString());
                Logs.d("CheckStatus", response.body().getMessage());
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getData().get(1).toString());
                }
            }
            @Override
            public void onFailure(Call<MessageStartUp> call, Throwable t) {
                Log.d("CheckStatus"," error "+t.getMessage());
            }
        });
    }
API_Manager api_manager = new API_Manager();
    Chat retrofits=api_manager.getClient7().create(Chat.class);
    public void insert_chat_message() {
        HashMap<String,String> param= new HashMap<>();
        param.put("sender_id","5b32470a87");
        param.put("sender_type","farmer");
        param.put("receiver_id","5b32470a87");
        param.put("receiver_type","farmer");
        param.put("mode1","farmer_farmer");
        param.put("mode2","pincode");
        param.put("message","test");
        Call<MessageStatus> call=retrofits.insert_chat_message("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxNCwibmFtZSI6Im1lZ1xuIiwiZW1haWwiOiJtZWdAZ21haWwuY29tIiwiemlwQ29kZSI6MTIzLCJtb2JpbGUiOjMyMTMyMTMyMTMsInN0YXR1cyI6MSwiY3JlYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidXBkYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidHlwZSI6ImZwbyJ9LCJpYXQiOjE1NjYwMjMwNDgsImV4cCI6MTU2ODYxNTA0OH0.-yaQVXHScLrl-poZsV3alGKC4YLiPUhvoIEhD-h0g6Y",param);
        call.enqueue(new Callback<MessageStatus>() {
            @Override
            public void onResponse(Call<MessageStatus> call, Response<MessageStatus> response) {
                Logs.d("CheckStatus", "onResponse: ");
                Logs.d("CheckStatus", response.body().getStatus().toString());
                Logs.d("CheckStatus", response.body().getCode().toString());
                Logs.d("CheckStatus", response.body().getMessage());
            }
            @Override
            public void onFailure(Call<MessageStatus> call, Throwable t) {
                Log.d("CheckStatus"," error "+t.getMessage());
            }
        });
    }
    public void send_message() {
        Call<SendMessageResponse> call=retrofits.send_message("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxNCwibmFtZSI6Im1lZ1xuIiwiZW1haWwiOiJtZWdAZ21haWwuY29tIiwiemlwQ29kZSI6MTIzLCJtb2JpbGUiOjMyMTMyMTMyMTMsInN0YXR1cyI6MSwiY3JlYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidXBkYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidHlwZSI6ImZwbyJ9LCJpYXQiOjE1NjYwMjMwNDgsImV4cCI6MTU2ODYxNTA0OH0.-yaQVXHScLrl-poZsV3alGKC4YLiPUhvoIEhD-h0g6Y","test","5b32470a7","farmer","5b32470a7","farmer");
        call.enqueue(new Callback<SendMessageResponse>() {
            @Override
            public void onResponse(Call<SendMessageResponse> call, Response<SendMessageResponse> response) {
                Logs.d("CheckStatus", "onResponse: "+response);
                Logs.d("CheckStatus", response.body().getStatus());
                Logs.d("CheckStatus", response.body().getCode().toString());
                Logs.d("CheckStatus", response.body().getMessage());
            }
            @Override
            public void onFailure(Call<SendMessageResponse> call, Throwable t) {
                Log.d("CheckStatus"," error "+t.getMessage());
            }
        });
    }
    public void update_status() {
        Call<MessageStatus> call=retrofit_interface.update_status("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTk1MzU4NCwiZXhwIjoxNTY0NTQ1NTg0fQ.vu6qstszSI4kp_l-r1ogOnYM3-Nj0wFDKVewf1P-7ZI","","","","","");
        call.enqueue(new Callback<MessageStatus>() {
            @Override
            public void onResponse(Call<MessageStatus> call, Response<MessageStatus> response) {
                Logs.d("CheckStatus", "onResponse: ");
                Logs.d("CheckStatus", response.body().getStatus().toString());
                Logs.d("CheckStatus", response.body().getCode().toString());
                Logs.d("CheckStatus", response.body().getMessage());
            }
            @Override
            public void onFailure(Call<MessageStatus> call, Throwable t) {
                Log.d("CheckStatus"," error "+t.getMessage());
            }
        });
    }

    public void get_commodity() {
        Call<GetCommodity> call=retrofit_interface.get_commodity("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTk1MzU4NCwiZXhwIjoxNTY0NTQ1NTg0fQ.vu6qstszSI4kp_l-r1ogOnYM3-Nj0wFDKVewf1P-7ZI","","");
        call.enqueue(new Callback<GetCommodity>() {
            @Override
            public void onResponse(Call<GetCommodity> call, Response<GetCommodity> response) {
                Logs.d("CheckStatus", "onResponse: ");
                Logs.d("CheckStatus", response.body().getStatus());
                Logs.d("CheckStatus", response.body().getCode().toString());
                Logs.d("CheckStatus", response.body().getMessage());
                Logs.d("CheckStatus", response.body().getData().get(1).getCommodity_id());
            }
            @Override
            public void onFailure(Call<GetCommodity> call, Throwable t) {
                Log.d("CheckStatus"," error "+t.getMessage());
            }
        });
    }
    public void get_commodity_pincode() {
        Call<PincodeCommodity> call=retrofit_interface.get_commodity_pincode("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTk1MzU4NCwiZXhwIjoxNTY0NTQ1NTg0fQ.vu6qstszSI4kp_l-r1ogOnYM3-Nj0wFDKVewf1P-7ZI","","","");
        call.enqueue(new Callback<PincodeCommodity>() {
            @Override
            public void onResponse(Call<PincodeCommodity> call, Response<PincodeCommodity> response) {
                Logs.d("CheckStatus", "onResponse: ");
                Logs.d("CheckStatus", response.body().getStatus());
                Logs.d("CheckStatus", response.body().getCode().toString());
                Logs.d("CheckStatus", response.body().getMessage());
                Logs.d("CheckStatus", response.body().getData().get(1).getCommodity());
            }
            @Override
            public void onFailure(Call<PincodeCommodity> call, Throwable t) {
                Log.d("CheckStatus"," error "+t.getMessage());
            }
        });
    }
    public void get_farmer_list() {
        Call<GetFarmerList> call=retrofit_interface.get_farmer_list("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTk1MzU4NCwiZXhwIjoxNTY0NTQ1NTg0fQ.vu6qstszSI4kp_l-r1ogOnYM3-Nj0wFDKVewf1P-7ZI","","");
        call.enqueue(new Callback<GetFarmerList>() {
            @Override
            public void onResponse(Call<GetFarmerList> call, Response<GetFarmerList> response) {
                Logs.d("CheckStatus", "onResponse: ");
                Logs.d("CheckStatus", response.body().getStatus().toString());
                Logs.d("CheckStatus", response.body().getCode().toString());
                Logs.d("CheckStatus", response.body().getMessage());
                Logs.d("CheckStatus", response.body().getData().get(1).getFarmer_id());
            }
            @Override
            public void onFailure(Call<GetFarmerList> call, Throwable t) {
                Log.d("CheckStatus"," error "+t.getMessage());
            }
        });
    }
}
