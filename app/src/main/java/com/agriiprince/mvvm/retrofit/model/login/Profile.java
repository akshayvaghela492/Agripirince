package com.agriiprince.mvvm.retrofit.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Profile implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("farmer_id")
    @Expose
    private String farmerId;
    @SerializedName("fcm_token")
    @Expose
    private String fcmToken;
    @SerializedName("farmer_name")
    @Expose
    private String farmerName;
    @SerializedName("farmer_contact")
    @Expose
    private String farmerContact;
    @SerializedName("farmer_otp")
    @Expose
    private String farmerOtp;
    @SerializedName("farmer_password")
    @Expose
    private String farmerPassword;
    @SerializedName("farmer_address")
    @Expose
    private String farmerAddress;
    @SerializedName("farmer_location")
    @Expose
    private String farmerLocation;
    @SerializedName("farmer_pincode")
    @Expose
    private String farmerPincode;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("preferred_language")
    @Expose
    private String preferredLanguage;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("bank_account_number")
    @Expose
    private String bankAccountNumber;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("updated_on")
    @Expose
    private String updatedOn;
    @SerializedName("subscription_services")
    @Expose
    private String subscriptionServices;
    @SerializedName("subscribed_pesticide_vendor")
    @Expose
    private String subscribedPesticideVendor;
    @SerializedName("login_status")
    @Expose
    private String loginStatus;
    @SerializedName("invited_by")
    @Expose
    private String invitedBy;
    @SerializedName("asked_for_invite")
    @Expose
    private String askedForInvite;
    @SerializedName("referred_number")
    @Expose
    private String referredNumber;
    @SerializedName("my_crop_list")
    @Expose
    private String myCropList;
    @SerializedName("interested_crop_list")
    @Expose
    private List<String> interestedCropList;
    @SerializedName("secondary_language")
    @Expose
    private String secondaryLanguage;
    @SerializedName("nearest_mandi_name")
    @Expose
    private String nearestMandiName;
    @SerializedName("nearest_mandi_lat")
    @Expose
    private String nearestMandiLat;
    @SerializedName("nearest_mandi_log")
    @Expose
    private String nearestMandiLog;
    @SerializedName("allow_gps")
    @Expose
    private String allowGps;
    @SerializedName("geo_tag_location")
    @Expose
    private String geoTagLocation;
    @SerializedName("has_net_banking")
    @Expose
    private String hasNetBanking;
    @SerializedName("has_paytm")
    @Expose
    private String hasPaytm;
    @SerializedName("has_bhim")
    @Expose
    private String hasBhim;
    @SerializedName("screen_saver_enabled")
    @Expose
    private String screenSaverEnabled;
    @SerializedName("interested_mandi_list")
    @Expose
    private List<String> interestedMandiList;
    @SerializedName("average_annual_income")
    @Expose
    private Integer averageAnnualIncome;
    @SerializedName("land_in_acres")
    @Expose
    private Integer landInAcres;
    @SerializedName("area_measuring_unit")
    @Expose
    private String areaMeasuringUnit;
    @SerializedName("price_measuring_unit")
    @Expose
    private String priceMeasuringUnit;
    @SerializedName("interested_rss_topics")
    @Expose
    private String interestedRssTopics;
    @SerializedName("bpl_image")
    @Expose
    private String bplImage;
    @SerializedName("bpl_status")
    @Expose
    private String bplStatus;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFarmerId() {
        return farmerId;
    }

    public void setFarmerId(String farmerId) {
        this.farmerId = farmerId;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getFarmerName() {
        return farmerName;
    }

    public void setFarmerName(String farmerName) {
        this.farmerName = farmerName;
    }

    public String getFarmerContact() {
        return farmerContact;
    }

    public void setFarmerContact(String farmerContact) {
        this.farmerContact = farmerContact;
    }

    public String getFarmerOtp() {
        return farmerOtp;
    }

    public void setFarmerOtp(String farmerOtp) {
        this.farmerOtp = farmerOtp;
    }

    public String getFarmerPassword() {
        return farmerPassword;
    }

    public void setFarmerPassword(String farmerPassword) {
        this.farmerPassword = farmerPassword;
    }

    public String getFarmerAddress() {
        return farmerAddress;
    }

    public void setFarmerAddress(String farmerAddress) {
        this.farmerAddress = farmerAddress;
    }

    public String getFarmerLocation() {
        return farmerLocation;
    }

    public void setFarmerLocation(String farmerLocation) {
        this.farmerLocation = farmerLocation;
    }

    public String getFarmerPincode() {
        return farmerPincode;
    }

    public void setFarmerPincode(String farmerPincode) {
        this.farmerPincode = farmerPincode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPreferredLanguage() {
        return preferredLanguage;
    }

    public void setPreferredLanguage(String preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getSubscriptionServices() {
        return subscriptionServices;
    }

    public void setSubscriptionServices(String subscriptionServices) {
        this.subscriptionServices = subscriptionServices;
    }

    public String getSubscribedPesticideVendor() {
        return subscribedPesticideVendor;
    }

    public void setSubscribedPesticideVendor(String subscribedPesticideVendor) {
        this.subscribedPesticideVendor = subscribedPesticideVendor;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getInvitedBy() {
        return invitedBy;
    }

    public void setInvitedBy(String invitedBy) {
        this.invitedBy = invitedBy;
    }

    public String getAskedForInvite() {
        return askedForInvite;
    }

    public void setAskedForInvite(String askedForInvite) {
        this.askedForInvite = askedForInvite;
    }

    public String getReferredNumber() {
        return referredNumber;
    }

    public void setReferredNumber(String referredNumber) {
        this.referredNumber = referredNumber;
    }

    public String getMyCropList() {
        return myCropList;
    }

    public void setMyCropList(String myCropList) {
        this.myCropList = myCropList;
    }

    public List<String> getInterestedCropList() {
        return interestedCropList;
    }

    public void setInterestedCropList(List<String> interestedCropList) {
        this.interestedCropList = interestedCropList;
    }

    public String getSecondaryLanguage() {
        return secondaryLanguage;
    }

    public void setSecondaryLanguage(String secondaryLanguage) {
        this.secondaryLanguage = secondaryLanguage;
    }

    public String getNearestMandiName() {
        return nearestMandiName;
    }

    public void setNearestMandiName(String nearestMandiName) {
        this.nearestMandiName = nearestMandiName;
    }

    public String getNearestMandiLat() {
        return nearestMandiLat;
    }

    public void setNearestMandiLat(String nearestMandiLat) {
        this.nearestMandiLat = nearestMandiLat;
    }

    public String getNearestMandiLog() {
        return nearestMandiLog;
    }

    public void setNearestMandiLog(String nearestMandiLog) {
        this.nearestMandiLog = nearestMandiLog;
    }

    public String getAllowGps() {
        return allowGps;
    }

    public void setAllowGps(String allowGps) {
        this.allowGps = allowGps;
    }

    public String getGeoTagLocation() {
        return geoTagLocation;
    }

    public void setGeoTagLocation(String geoTagLocation) {
        this.geoTagLocation = geoTagLocation;
    }

    public String getHasNetBanking() {
        return hasNetBanking;
    }

    public void setHasNetBanking(String hasNetBanking) {
        this.hasNetBanking = hasNetBanking;
    }

    public String getHasPaytm() {
        return hasPaytm;
    }

    public void setHasPaytm(String hasPaytm) {
        this.hasPaytm = hasPaytm;
    }

    public String getHasBhim() {
        return hasBhim;
    }

    public void setHasBhim(String hasBhim) {
        this.hasBhim = hasBhim;
    }

    public String getScreenSaverEnabled() {
        return screenSaverEnabled;
    }

    public void setScreenSaverEnabled(String screenSaverEnabled) {
        this.screenSaverEnabled = screenSaverEnabled;
    }

    public List<String> getInterestedMandiList() {
        return interestedMandiList;
    }

    public void setInterestedMandiList(List<String> interestedMandiList) {
        this.interestedMandiList = interestedMandiList;
    }

    public Integer getAverageAnnualIncome() {
        return averageAnnualIncome;
    }

    public void setAverageAnnualIncome(Integer averageAnnualIncome) {
        this.averageAnnualIncome = averageAnnualIncome;
    }

    public Integer getLandInAcres() {
        return landInAcres;
    }

    public void setLandInAcres(Integer landInAcres) {
        this.landInAcres = landInAcres;
    }

    public String getAreaMeasuringUnit() {
        return areaMeasuringUnit;
    }

    public void setAreaMeasuringUnit(String areaMeasuringUnit) {
        this.areaMeasuringUnit = areaMeasuringUnit;
    }

    public String getPriceMeasuringUnit() {
        return priceMeasuringUnit;
    }

    public void setPriceMeasuringUnit(String priceMeasuringUnit) {
        this.priceMeasuringUnit = priceMeasuringUnit;
    }

    public String getInterestedRssTopics() {
        return interestedRssTopics;
    }

    public void setInterestedRssTopics(String interestedRssTopics) {
        this.interestedRssTopics = interestedRssTopics;
    }

    public String getBplImage() {
        return bplImage;
    }

    public void setBplImage(String bplImage) {
        this.bplImage = bplImage;
    }

    public String getBplStatus() {
        return bplStatus;
    }

    public void setBplStatus(String bplStatus) {
        this.bplStatus = bplStatus;
    }
}