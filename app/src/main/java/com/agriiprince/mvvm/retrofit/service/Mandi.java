package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.mandi.MandiList;
import com.agriiprince.mvvm.retrofit.dto.mandi.MandiLocation;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Mandi {

    @FormUrlEncoded
    @POST()
    Call<MandiLocation> MANDI_LOCATION(@Header("authentication") String token,
                                            @Field("commodity_name") String commodity_name,
                                            @Field("commodity_variety") String commodity_variety

    );

    @FormUrlEncoded
    @POST()
    Call<MandiList> MANDI_LIST(@Header("authentication") String token,
                               @Field("user_id") String user_id,
                               @Field("password") String password,
                               @Field("mode") String mode,
                               @Field("mandi_state_en") String mandi_state_en,
                               @Field("type") String type

    );
}
