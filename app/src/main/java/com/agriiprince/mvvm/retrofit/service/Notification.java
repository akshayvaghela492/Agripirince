package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.notification.NotificationStatusUpdate;
import com.agriiprince.mvvm.retrofit.dto.notification.NotificationsResp;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Notification {

    @FormUrlEncoded
    @POST ("get_all_unread_notifications")
    Call<NotificationsResp> getmsg(@Header("authentication") String token,
                                         @Field("user_id") String user_id,
                                         @Field("mode") String mode,
                                         @Field("user_type") String user_type);

    @FormUrlEncoded
    @POST ("update_notification_status")
    Call<NotificationStatusUpdate> updatestatus(@Header("authentication") String token,

                                                @Field("user_id") String user_id,
                                                @Field("user_type") String user_type,
                                                @Field("mode") String mode,
                                                @Field("message_id") String message_id);

    @FormUrlEncoded
    @POST ("get_notifications")
    Call<com.agriiprince.mvvm.retrofit.dto.notification.Notification> getUserNotification(@Header("authentication") String token,
                                                                                          @Field("user_id") String user_id);
}
