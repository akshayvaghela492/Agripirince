package com.agriiprince.mvvm.retrofit.model.mandi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MandiListDetail implements Serializable {
    @SerializedName("mandi_state")
    @Expose
    private String mandi_state ;

    @SerializedName("status")
    @Expose
    private String status ;

    public String getMandi_state() {
        return mandi_state;
    }

    public void setMandi_state(String mandi_state) {
        this.mandi_state = mandi_state;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
