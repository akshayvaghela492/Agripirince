package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.mvvm.retrofit.dto.rss.RssFeed;
import com.agriiprince.mvvm.retrofit.dto.rss.RssTopic;
import com.agriiprince.mvvm.retrofit.service.Rss;
import com.agriiprince.mvvm.util.Logs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RssRetro {
    private Retrofit retrofit = null;
    private String baseurl=" ";

    public Retrofit getClient() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseurl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    Rss retrofit_interface=getClient().create(Rss.class);

    public void UpdateRssTopic() {
        Call<RssTopic> call = retrofit_interface.RSS_TOPIC("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","","","","");
        call.enqueue(new Callback<RssTopic>() {
            @Override
            public void onResponse(Call<RssTopic> call, Response<RssTopic> response) {
                Logs.d("CheckStatus", "onResponse: update price Success");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                    Logs.d("CheckStatus",response.body().getData().getTopic());
                }
            }
            @Override
            public void onFailure(Call<RssTopic> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }
    public void getRssFeed() {
        Call<RssFeed> call = retrofit_interface.RSS_Feed("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww");
        call.enqueue(new Callback<RssFeed>() {
            @Override
            public void onResponse(Call<RssFeed> call, Response<RssFeed> response) {
                Logs.d("CheckStatus", "onResponse: update price Success");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                    Logs.d("CheckStatus",response.body().getData().getRss_feed_description());
                }
            }
            @Override
            public void onFailure(Call<RssFeed> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }
}
