package com.agriiprince.mvvm.retrofit.model.subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Fonepaisa_Subscription implements Serializable {

    @SerializedName("transaction_id")
    @Expose
    private String transaction_id;
    @SerializedName("amount")
    @Expose
    private String amount;

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
