package com.agriiprince.mvvm.retrofit.model.checkprice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GovRequestBody  implements Serializable {

    @SerializedName("limit")
    @Expose
    private Integer limit;
    @SerializedName("offset")
    @Expose
    private Integer offset;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("cropGrade")
    @Expose
    private List<String> cropGrade = null;
    @SerializedName("cropVariety")
    @Expose
    private List<String> cropVariety = null;
    @SerializedName("cropName")
    @Expose
    private List<String> cropName = null;


    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<String> getCropGrade() {
        return cropGrade;
    }

    public void setCropGrade(List<String> cropGrade) {
        this.cropGrade = cropGrade;
    }

    public List<String> getCropVariety() {
        return cropVariety;
    }

    public void setCropVariety(List<String> cropVariety) {
        this.cropVariety = cropVariety;
    }

    public List<String> getCropName() {
        return cropName;
    }

    public void setCropName(List<String> cropName) {
        this.cropName = cropName;
    }
}

