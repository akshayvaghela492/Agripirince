package com.agriiprince.mvvm.retrofit.dto.subscription;

import com.agriiprince.mvvm.retrofit.model.subscription.SubcriptionCouponData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
// for migrated API created on 12/8/2019
public class CouponVerification implements Serializable {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private SubcriptionCouponData data;
    @SerializedName("status")
    @Expose
    private String status;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SubcriptionCouponData getData() {
        return data;
    }

    public void setData(SubcriptionCouponData data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}