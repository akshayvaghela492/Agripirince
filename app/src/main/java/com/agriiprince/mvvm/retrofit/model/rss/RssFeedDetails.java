package com.agriiprince.mvvm.retrofit.model.rss;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RssFeedDetails implements Serializable {

    @SerializedName("rss_feed_link")
    @Expose
    private String rss_feed_link;
    @SerializedName("rss_feed_description")
    @Expose
    private String rss_feed_description;
    @SerializedName("rss_feed_ratings")
    @Expose
    private String rss_feed_ratings;
    @SerializedName("govt_private_ngo")
    @Expose
    private String govt_private_ngo;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("categories")
    @Expose
    private String categories;
    @SerializedName("id")
    @Expose
    private Integer rss_id;

    public String getRss_feed_link() {
        return rss_feed_link;
    }

    public void setRss_feed_link(String rss_feed_link) {
        this.rss_feed_link = rss_feed_link;
    }

    public String getRss_feed_description() {
        return rss_feed_description;
    }

    public void setRss_feed_description(String rss_feed_description) {
        this.rss_feed_description = rss_feed_description;
    }

    public String getRss_feed_ratings() {
        return rss_feed_ratings;
    }

    public void setRss_feed_ratings(String rss_feed_ratings) {
        this.rss_feed_ratings = rss_feed_ratings;
    }

    public String getGovt_private_ngo() {
        return govt_private_ngo;
    }

    public void setGovt_private_ngo(String govt_private_ngo) {
        this.govt_private_ngo = govt_private_ngo;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public Integer getRss_id() {
        return rss_id;
    }

    public void setRss_id(Integer rss_id) {
        this.rss_id = rss_id;
    }
}
