package com.agriiprince.mvvm.retrofit.model.trackcrop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TrackGetCrop implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("crop")
    @Expose
    private String crop;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("userType")
    @Expose
    private String userType;
    @SerializedName("variety")
    @Expose
    private String variety;
    @SerializedName("grade")
    @Expose
    private String grade;
    @SerializedName("locationState")
    @Expose
    private String locationState;
    @SerializedName("locationDistrict")
    @Expose
    private String locationDistrict;
    @SerializedName("locationTaluka")
    @Expose
    private String locationTaluka;
    @SerializedName("locationVillage")
    @Expose
    private String locationVillage;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCrop() {
        return crop;
    }

    public void setCrop(String crop) {
        this.crop = crop;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getLocationState() {
        return locationState;
    }

    public void setLocationState(String locationState) {
        this.locationState = locationState;
    }

    public String getLocationDistrict() {
        return locationDistrict;
    }

    public void setLocationDistrict(String locationDistrict) {
        this.locationDistrict = locationDistrict;
    }

    public String getLocationTaluka() {
        return locationTaluka;
    }

    public void setLocationTaluka(String locationTaluka) {
        this.locationTaluka = locationTaluka;
    }

    public String getLocationVillage() {
        return locationVillage;
    }

    public void setLocationVillage(String locationVillage) {
        this.locationVillage = locationVillage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
