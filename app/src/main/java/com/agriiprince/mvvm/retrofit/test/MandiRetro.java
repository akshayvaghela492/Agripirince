package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.mvvm.retrofit.dto.mandi.MandiList;
import com.agriiprince.mvvm.retrofit.dto.mandi.MandiLocation;
import com.agriiprince.mvvm.retrofit.service.Mandi;
import com.agriiprince.mvvm.util.Logs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MandiRetro {
    private Retrofit retrofit = null;
    private String baseurl=" ";

    public Retrofit getClient() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseurl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    Mandi retrofit_interface = getClient().create(Mandi.class);

    public void MandiLocation() {

        Call<MandiLocation> call=retrofit_interface.MANDI_LOCATION("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","","");
        call.enqueue(new Callback<MandiLocation>() {
            @Override
            public void onResponse(Call<MandiLocation> call, Response<MandiLocation> response) {
                Logs.d("CheckStatus", "onResponse: ");
                try {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                    Logs.d("CheckStatus",response.body().getData().getMandi_name_en());
                }
                catch (Exception e){e.getMessage();}
            }
            @Override
            public void onFailure(Call<MandiLocation> call, Throwable t) {
                Log.d("CheckStatus","error"+t.getMessage());
            }
        });
    }

    public void MandiList() {

        Call<MandiList> call=retrofit_interface.MANDI_LIST("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","","","","","");
        call.enqueue(new Callback<MandiList>() {
            @Override
            public void onResponse(Call<MandiList> call, Response<MandiList> response) {
                Logs.d("CheckStatus", "onResponse: ");
                try {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                    Logs.d("CheckStatus",response.body().getData().get(1).getMandi_state());
                }
                catch (Exception e){e.getMessage();}
            }
            @Override
            public void onFailure(Call<MandiList> call, Throwable t) {
                Log.d("CheckStatus","error"+t.getMessage());
            }
        });
    }
}
