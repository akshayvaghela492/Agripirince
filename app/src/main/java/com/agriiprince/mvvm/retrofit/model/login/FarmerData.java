package com.agriiprince.mvvm.retrofit.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FarmerData implements Serializable {

    @SerializedName("data")
    @Expose
    private List<ProfileDataDetails> data = null;


    public List<ProfileDataDetails> getData() {
        return data;
    }

    public void setData(List<ProfileDataDetails> data) {
        this.data = data;
    }
}