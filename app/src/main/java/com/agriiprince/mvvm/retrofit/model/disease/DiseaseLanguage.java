package com.agriiprince.mvvm.retrofit.model.disease;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DiseaseLanguage implements Serializable
{
    @SerializedName("primary")
    @Expose
    private DiseaseData primary;
    @SerializedName("secondary")
    @Expose
    private DiseaseData secondary;

    public DiseaseData getPrimary() {
        return primary;
    }

    public void setPrimary(DiseaseData primary) {
        this.primary = primary;
    }

    public DiseaseData getSecondary() {
        return secondary;
    }
    public void setSecondary(DiseaseData secondary) {
        this.secondary = secondary;
    }
}
