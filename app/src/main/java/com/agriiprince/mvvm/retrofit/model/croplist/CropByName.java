package com.agriiprince.mvvm.retrofit.model.croplist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CropByName implements Serializable {

    @SerializedName("crop_name")
    @Expose
    private String crop_name;
    @SerializedName("crop_variety")
    @Expose
    private String crop_variety;

    public String getCrop_name() {
        return crop_name;
    }

    public void setCrop_name(String crop_name) {
        this.crop_name = crop_name;
    }

    public String getCrop_variety() {
        return crop_variety;
    }

    public void setCrop_variety(String crop_variety) {
        this.crop_variety = crop_variety;
    }

}
