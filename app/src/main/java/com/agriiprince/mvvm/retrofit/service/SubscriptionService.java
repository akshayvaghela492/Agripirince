package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.subscription.BplPhotoUpload;
import com.agriiprince.mvvm.retrofit.dto.subscription.BplUploadPhoto;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionList;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionUser;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionCoupon;
import com.agriiprince.mvvm.retrofit.ResponseWrapper;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface SubscriptionService {

    /**
     * active subscription details of user
     * @param accessToken api token
     * @param userId user id
     * @param userType type of user
     * @param password user password
     * @return
     */
    @FormUrlEncoded
    @POST("subscription/subscription_information.php")
    Call<List<SubscriptionUser>> getUserSubscription(
            @Field("api_token") String accessToken, @Field("user_id") String userId,
            @Field("user_type") String userType, @Field("password") String password);

    /**
     *
     */
    @FormUrlEncoded
    @POST("nodejs/subscription/subscription_details.php")
    Call<List<ResponseWrapper<SubscriptionList>>> getSubscription(
            @Field("api_token") String accessToken, @Field("user_id") String userId,
            @Field("password") String password, @Field("mode") String mode);



    @FormUrlEncoded
    @POST("subscription/subscription_verify_coupon.php")
    Call<List<SubscriptionCoupon>> applyCouponCode(
            @Field("api_token") String accessToken, @Field("user_id") String userId,
            @Field("user_type") String userType, @Field("password") String password,
            @Field("mode") String mode, @Field("coupon_code") String couponCode,
            @Field("oe_mobile") String oeMobileNumber);


    @FormUrlEncoded
    @POST("upload_bpl_image.php")
    Call<BplPhotoUpload> uploadBplImage(
            @Field("api_token") String apiToken, @Field("password") String password,
            @Field("user_id") String userId, @Field("image_file") String imageFile);

}
