package com.agriiprince.mvvm.retrofit.model.tradecrop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Media implements Serializable
    {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("cropId")
        @Expose
        private Integer cropId;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;
        @SerializedName("updatedAt")
        @Expose
        private String updatedAt;

        public Integer getId() {
        return id;
    }

        public void setId(Integer id) {
        this.id = id;
    }

        public Integer getCropId() {
        return cropId;
    }

        public void setCropId(Integer cropId) {
        this.cropId = cropId;
    }

        public String getUrl() {
        return url;
    }

        public void setUrl(String url) {
        this.url = url;
    }

        public Integer getStatus() {
        return status;
    }

        public void setStatus(Integer status) {
        this.status = status;
    }

        public String getType() {
        return type;
    }

        public void setType(String type) {
        this.type = type;
    }

        public String getCreatedAt() {
        return createdAt;
    }

        public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

        public String getUpdatedAt() {
        return updatedAt;
    }

        public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    }