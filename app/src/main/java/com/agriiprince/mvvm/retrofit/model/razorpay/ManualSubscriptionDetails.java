package com.agriiprince.mvvm.retrofit.model.razorpay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ManualSubscriptionDetails implements Serializable
{

    @SerializedName("subscription_id")
    @Expose
    private String subscriptionId;
    @SerializedName("subscription_type")
    @Expose
    private String subscriptionType;
    @SerializedName("subscription_validity_days")
    @Expose
    private Integer subscriptionValidityDays;
    @SerializedName("subscription_status")
    @Expose
    private String subscriptionStatus;
    @SerializedName("subscription_name")
    @Expose
    private String subscriptionName;
    @SerializedName("subscription_name_hindi")
    @Expose
    private String subscriptionNameHindi;
    @SerializedName("inaugral_offer")
    @Expose
    private Integer inaugralOffer;
    @SerializedName("inaugral_offer_price")
    @Expose
    private Integer inaugralOfferPrice;
    @SerializedName("applicable_for_waver")
    @Expose
    private Integer applicableForWaver;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("updated_on")
    @Expose
    private String updatedOn;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("farmer_id")
    @Expose
    private String farmerId;
    @SerializedName("farmer_mobile")
    @Expose
    private String farmerMobile;
    @SerializedName("oe_mobile")
    @Expose
    private String oeMobile;
    @SerializedName("subscription_price")
    @Expose
    private String subscriptionPrice;


    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public Integer getSubscriptionValidityDays() {
        return subscriptionValidityDays;
    }

    public void setSubscriptionValidityDays(Integer subscriptionValidityDays) {
        this.subscriptionValidityDays = subscriptionValidityDays;
    }

    public String getSubscriptionStatus() {
        return subscriptionStatus;
    }

    public void setSubscriptionStatus(String subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    public String getSubscriptionName() {
        return subscriptionName;
    }

    public void setSubscriptionName(String subscriptionName) {
        this.subscriptionName = subscriptionName;
    }

    public String getSubscriptionNameHindi() {
        return subscriptionNameHindi;
    }

    public void setSubscriptionNameHindi(String subscriptionNameHindi) {
        this.subscriptionNameHindi = subscriptionNameHindi;
    }

    public Integer getInaugralOffer() {
        return inaugralOffer;
    }

    public void setInaugralOffer(Integer inaugralOffer) {
        this.inaugralOffer = inaugralOffer;
    }

    public Integer getInaugralOfferPrice() {
        return inaugralOfferPrice;
    }

    public void setInaugralOfferPrice(Integer inaugralOfferPrice) {
        this.inaugralOfferPrice = inaugralOfferPrice;
    }

    public Integer getApplicableForWaver() {
        return applicableForWaver;
    }

    public void setApplicableForWaver(Integer applicableForWaver) {
        this.applicableForWaver = applicableForWaver;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFarmerId() {
        return farmerId;
    }

    public void setFarmerId(String farmerId) {
        this.farmerId = farmerId;
    }

    public String getFarmerMobile() {
        return farmerMobile;
    }

    public void setFarmerMobile(String farmerMobile) {
        this.farmerMobile = farmerMobile;
    }

    public String getOeMobile() {
        return oeMobile;
    }

    public void setOeMobile(String oeMobile) {
        this.oeMobile = oeMobile;
    }

    public String getSubscriptionPrice() {
        return subscriptionPrice;
    }

    public void setSubscriptionPrice(String subscriptionPrice) {
        this.subscriptionPrice = subscriptionPrice;
    }

}