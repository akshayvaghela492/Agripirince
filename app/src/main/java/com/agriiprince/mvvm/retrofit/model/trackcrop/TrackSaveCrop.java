package com.agriiprince.mvvm.retrofit.model.trackcrop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TrackSaveCrop implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("tcropId")
    @Expose
    private Integer tcropId;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTcropId() {
        return tcropId;
    }

    public void setTcropId(Integer tcropId) {
        this.tcropId = tcropId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
