package com.agriiprince.mvvm.retrofit.model.disease;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Diseases implements Serializable {
    @SerializedName("crop_name")
    @Expose
    private String crop_name;
    @SerializedName("crop_name_bi")
    @Expose
    private String crop_name_bi;
    @SerializedName("crop_diseases_id")
    @Expose
    private String crop_diseases_id;

    public String getCrop_name() {
        return crop_name;
    }

    public void setCrop_name(String crop_name) {
        this.crop_name = crop_name;
    }

    public String getCrop_name_bi() {
        return crop_name_bi;
    }

    public void setCrop_name_bi(String crop_name_bi) {
        this.crop_name_bi = crop_name_bi;
    }

    public String getCrop_diseases_id() {
        return crop_diseases_id;
    }

    public void setCrop_diseases_id(String crop_diseases_id) {
        this.crop_diseases_id = crop_diseases_id;
    }
}
