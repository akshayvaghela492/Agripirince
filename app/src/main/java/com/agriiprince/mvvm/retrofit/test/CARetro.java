package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.ca.CADetail;
import com.agriiprince.mvvm.retrofit.dto.ca.CAlist;
import com.agriiprince.mvvm.retrofit.service.CA;
import com.agriiprince.mvvm.util.Logs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CARetro {
    API_Manager api_manager = new API_Manager();
    CA retrofit_interface=api_manager.getClient11().create(CA.class);

    public void CAdetails() {
        Call<CADetail> call = retrofit_interface.CA_DETAIL("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww");
        call.enqueue(new Callback<CADetail>() {
            @Override
            public void onResponse(Call<CADetail> call, Response<CADetail> response) {
                Logs.d("CheckStatus", "onResponse: "+response);
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus().toString());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                    Logs.d("CheckStatus",response.body().getData().get(1).getCa_name());
                }
            }
            @Override
            public void onFailure(Call<CADetail> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }

   /* public void CAList() {
        Call<CAlist> call = retrofit_interface.CA_LIST("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxNCwibmFtZSI6Im1lZ1xuIiwiZW1haWwiOiJtZWdAZ21haWwuY29tIiwiemlwQ29kZSI6MTIzLCJtb2JpbGUiOjMyMTMyMTMyMTMsInN0YXR1cyI6MSwiY3JlYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidXBkYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidHlwZSI6ImZwbyJ9LCJpYXQiOjE1NjYwMjMwNDgsImV4cCI6MTU2ODYxNTA0OH0.-yaQVXHScLrl-poZsV3alGKC4YLiPUhvoIEhD-h0g6Y","3793","ca","Kurnool","en","hi");
        call.enqueue(new Callback<CAlist>() {
            @Override
            public void onResponse(Call<CAlist> call, Response<CAlist> response) {
                Logs.d("CheckStatus", "onResponse: CA "+response);
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus().toString());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                    //Logs.d("CheckStatus",response.body().getData().getCaRating().get(1).getCaName());
                }
            }
            @Override
            public void onFailure(Call<CAlist> call, Throwable t) {
                Log.d("CheckStatus", "fail CA"+t.getMessage());
            }
        });
    }
    */
}
