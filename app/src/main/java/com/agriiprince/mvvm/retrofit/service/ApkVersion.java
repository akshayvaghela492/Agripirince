package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.apkversions.ApkVers;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApkVersion {

    @FormUrlEncoded
    @POST("check")
    Call<ApkVers> ApkVersionCheck(@Header("authentication") String token,
                                  @Field("apk_version") String apk_version, @Field("category") String userType);
}
