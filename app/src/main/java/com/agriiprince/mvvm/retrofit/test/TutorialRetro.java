package com.agriiprince.mvvm.retrofit.test;

import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.tutorial.TutorialResponse;
import com.agriiprince.mvvm.retrofit.model.tutorial.LanguageRequest;
import com.agriiprince.mvvm.retrofit.model.tutorial.SubSection;
import com.agriiprince.mvvm.retrofit.model.tutorial.TutorialModel;
import com.agriiprince.mvvm.retrofit.model.tutorial.TutorialSections;
import com.agriiprince.mvvm.retrofit.service.Tutorial;
import com.agriiprince.mvvm.util.Logs;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TutorialRetro {

    API_Manager api_manager=new API_Manager();
    Tutorial retrofit_interface = api_manager.TutorialClient().create(Tutorial.class);

    public void tutorial() {

        Call<TutorialResponse> call=retrofit_interface.tutorial("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaTEyMyIsInVzZXJfaWQiOiJGMTVBS0xLUSIsInBhc3N3b3JkIjoiMTIzMTIzIiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjYyMTI3NzIsImV4cCI6MTU2ODgwNDc3Mn0.Nhx5uc_FTtaifgF3BcE3-lPYxJnNQ5mzHVgdvBFQoSk","hi");
        call.enqueue(new Callback<TutorialResponse>() {
            @Override
            public void onResponse(Call<TutorialResponse> call, Response<TutorialResponse> response) {
                Logs.d("CHECKSTATUS", "tutorial onResponse "+response);
              try {
                  if (response.body() != null && response.body().getStatus() == 200)
                      Logs.d("CHECKSTATUS", "tutorial  status " + response.body().getStatus());
                      Logs.d("CHECKSTATUS", "tutorial message " + response.body().getMessage());

                  List<TutorialSections> tutorialSections = response.body().getData().getTutorial();

                  Logs.d("CHECKSTATUS", "tutorial section_code " + tutorialSections.get(7).getSectionCode());
                  Logs.d("CHECKSTATUS", "tutorial section_title " + tutorialSections.get(7).getSectionTitle());
                  Logs.d("CHECKSTATUS", "tutorial section_content " + tutorialSections.get(7).getSectionContent());

                  List<SubSection> subSection = tutorialSections.get(7).getSubSections();

                  Logs.d("CHECKSTATUS", "tutorial  sub_section_code " + subSection.get(1).getSubSectionCode());
                  Logs.d("CHECKSTATUS", "tutorial  sub_section_title " + subSection.get(1).getSubSectionTitle());
                  Logs.d("CHECKSTATUS", "tutorial  sub_section_content " + subSection.get(1).getSubSectionContent());

              }
              catch
              (Exception e)
              {e.printStackTrace();}

            }
            @Override
            public void onFailure(Call<TutorialResponse> call, Throwable t) {
                Logs.d("CHECKSTATUS","tutorial error "+t.getMessage());
            }
        });
    }
}
