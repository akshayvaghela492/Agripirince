package com.agriiprince.mvvm.retrofit.model.coldstorage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ColdStorageList implements Serializable
{
    @SerializedName("cold_storages_id")
    @Expose
    private String coldStoragesId;
    @SerializedName("cold_storage_name")
    @Expose
    private String coldStorageName;
    @SerializedName("cold_storage_address")
    @Expose
    private String coldStorageAddress;
    @SerializedName("cold_storage_lat")
    @Expose
    private String coldStorageLat;
    @SerializedName("cold_storage_long")
    @Expose
    private String coldStorageLong;
    @SerializedName("cold_storage_contact")
    @Expose
    private String coldStorageContact;
    @SerializedName("cold_storage_name_bi")
    @Expose
    private String coldStorageNameBi;
    @SerializedName("cold_storage_address_bi")
    @Expose
    private String coldStorageAddressBi;
    @SerializedName("fair_price_rating")
    @Expose
    private Integer fairPriceRating;
    @SerializedName("quick_payment_rating")
    @Expose
    private Integer quickPaymentRating;
    @SerializedName("polite_mannerisms_rating")
    @Expose
    private Integer politeMannerismsRating;
    @SerializedName("complete_payment_rating")
    @Expose
    private Integer completePaymentRating;
    @SerializedName("will_visit_again_rating")
    @Expose
    private Integer willVisitAgainRating;
    @SerializedName("distance")
    @Expose
    private Double distance;


    public String getColdStoragesId() {
        return coldStoragesId;
    }

    public void setColdStoragesId(String coldStoragesId) {
        this.coldStoragesId = coldStoragesId;
    }

    public String getColdStorageName() {
        return coldStorageName;
    }

    public void setColdStorageName(String coldStorageName) {
        this.coldStorageName = coldStorageName;
    }

    public String getColdStorageAddress() {
        return coldStorageAddress;
    }

    public void setColdStorageAddress(String coldStorageAddress) {
        this.coldStorageAddress = coldStorageAddress;
    }

    public String getColdStorageLat() {
        return coldStorageLat;
    }

    public void setColdStorageLat(String coldStorageLat) {
        this.coldStorageLat = coldStorageLat;
    }

    public String getColdStorageLong() {
        return coldStorageLong;
    }

    public void setColdStorageLong(String coldStorageLong) {
        this.coldStorageLong = coldStorageLong;
    }

    public String getColdStorageContact() {
        return coldStorageContact;
    }

    public void setColdStorageContact(String coldStorageContact) {
        this.coldStorageContact = coldStorageContact;
    }

    public String getColdStorageNameBi() {
        return coldStorageNameBi;
    }

    public void setColdStorageNameBi(String coldStorageNameBi) {
        this.coldStorageNameBi = coldStorageNameBi;
    }

    public String getColdStorageAddressBi() {
        return coldStorageAddressBi;
    }

    public void setColdStorageAddressBi(String coldStorageAddressBi) {
        this.coldStorageAddressBi = coldStorageAddressBi;
    }

    public Integer getFairPriceRating() {
        return fairPriceRating;
    }

    public void setFairPriceRating(Integer fairPriceRating) {
        this.fairPriceRating = fairPriceRating;
    }

    public Integer getQuickPaymentRating() {
        return quickPaymentRating;
    }

    public void setQuickPaymentRating(Integer quickPaymentRating) {
        this.quickPaymentRating = quickPaymentRating;
    }

    public Integer getPoliteMannerismsRating() {
        return politeMannerismsRating;
    }

    public void setPoliteMannerismsRating(Integer politeMannerismsRating) {
        this.politeMannerismsRating = politeMannerismsRating;
    }

    public Integer getCompletePaymentRating() {
        return completePaymentRating;
    }

    public void setCompletePaymentRating(Integer completePaymentRating) {
        this.completePaymentRating = completePaymentRating;
    }

    public Integer getWillVisitAgainRating() {
        return willVisitAgainRating;
    }

    public void setWillVisitAgainRating(Integer willVisitAgainRating) {
        this.willVisitAgainRating = willVisitAgainRating;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }



}