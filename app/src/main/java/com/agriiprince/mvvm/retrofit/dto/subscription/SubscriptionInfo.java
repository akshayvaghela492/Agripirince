package com.agriiprince.mvvm.retrofit.dto.subscription;

import com.agriiprince.mvvm.retrofit.model.subscription.SubscriptionDataInfo;
import com.agriiprince.mvvm.retrofit.model.subscription.SubscriptionInformation;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
// for migrated API created on 12/8/2019
public class SubscriptionInfo implements Serializable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private SubscriptionDataInfo data;
    @SerializedName("code")
    @Expose
    private Integer code;


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SubscriptionDataInfo getData() {
        return data;
    }

    public void setData(SubscriptionDataInfo data) {
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}