package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.applevel.annotation.UserType;
import com.agriiprince.mvvm.retrofit.dto.login.VersionUpdate;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface VersionUpdateService {

    /**
     * check for version updates.
     *
     * @param apk_version the apk version
     * @param userType the user type
     * @return the response
     */
    @FormUrlEncoded
    @POST("apk_version_check.php")
    Call<List<VersionUpdate>> versionCheck(@Field("apk_version") String apk_version, @UserType @Field("category") String userType);

}
