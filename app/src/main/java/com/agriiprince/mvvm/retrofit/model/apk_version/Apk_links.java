package com.agriiprince.mvvm.retrofit.model.apk_version;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Apk_links implements Serializable {
    @SerializedName("link")
    @Expose
    private String link;


    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
