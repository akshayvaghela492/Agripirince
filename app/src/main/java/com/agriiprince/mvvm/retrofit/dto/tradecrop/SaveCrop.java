package com.agriiprince.mvvm.retrofit.dto.tradecrop;

import com.agriiprince.mvvm.retrofit.model.tradecrop.CropDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SaveCrop implements Serializable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private Object message;
    @SerializedName("data")
    @Expose
    private CropDetails data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public CropDetails getData() {
        return data;
    }

    public void setData(CropDetails data) {
        this.data = data;
    }

}