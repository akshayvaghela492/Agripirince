package com.agriiprince.mvvm.retrofit.model.tradecrop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Farmerdetail {
    @SerializedName("user_id")
    @Expose
    private int user_id;
    @SerializedName("farmer_name")
    @Expose
    private String farmer_name;
    @SerializedName("mobile")
    @Expose
    private Integer mobile;
    @SerializedName("farm_size")
    @Expose
    private String farm_size;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getFarmer_name() {
        return farmer_name;
    }

    public void setFarmer_name(String farmer_name) {
        this.farmer_name = farmer_name;
    }

    public Integer getMobile() {
        return mobile;
    }

    public void setMobile(Integer mobile) {
        this.mobile = mobile;
    }

    public String getFarm_size() {
        return farm_size;
    }

    public void setFarm_size(String farm_size) {
        this.farm_size = farm_size;
    }
}
