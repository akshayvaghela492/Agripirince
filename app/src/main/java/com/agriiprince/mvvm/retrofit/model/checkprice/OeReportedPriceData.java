package com.agriiprince.mvvm.retrofit.model.checkprice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OeReportedPriceData implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("district")
    @Expose
    private String district;
    @SerializedName("market")
    @Expose
    private String market;
    @SerializedName("commodity_variety")
    @Expose
    private String variety;
    @SerializedName("commodity_grade")
    @Expose
    private String grade;
    @SerializedName("mandi_lat")
    @Expose
    private double mandi_lat;
    @SerializedName("mandi_lng")
    @Expose
    private double mandi_lng;
    @SerializedName("min_price")
    @Expose
    private int minP;
    @SerializedName("max_price")
    @Expose
    private int maxP;
    @SerializedName("modal_price")
    @Expose
    private int modP;
    @SerializedName("tonnage")
    @Expose
    private double tonnage;
    @SerializedName("isInterested")
    @Expose
    private boolean isInterested;
    @SerializedName("distance")
    @Expose
    private double distance;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public double getMandi_lat() {
        return mandi_lat;
    }

    public void setMandi_lat(double mandi_lat) {
        this.mandi_lat = mandi_lat;
    }

    public double getMandi_lng() {
        return mandi_lng;
    }

    public void setMandi_lng(double mandi_lng) {
        this.mandi_lng = mandi_lng;
    }

    public int getMinP() {
        return minP;
    }

    public void setMinP(int minP) {
        this.minP = minP;
    }

    public int getMaxP() {
        return maxP;
    }

    public void setMaxP(int maxP) {
        this.maxP = maxP;
    }

    public int getModP() {
        return modP;
    }

    public void setModP(int modP) {
        this.modP = modP;
    }

    public double getTonnage() {
        return tonnage;
    }

    public void setTonnage(double tonnage) {
        this.tonnage = tonnage;
    }

    public boolean isInterested() {
        return isInterested;
    }

    public void setInterested(boolean interested) {
        isInterested = interested;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
