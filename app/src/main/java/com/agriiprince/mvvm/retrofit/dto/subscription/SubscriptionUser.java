package com.agriiprince.mvvm.retrofit.dto.subscription;

import com.agriiprince.utils.DateUtil;

import java.util.List;

/*

[{"error_code":100,"message":"success.",
        "bpl_status":"not applicable",
        "data":[
        {"id":"90","user_id":"GHVK9449","user_type":"farmer","transaction_id":"",
        "subscription_id":"5b34e48759256","subscription_type":"trial",
        "subscription_start_date":"2019-03-15 14:57:13",
        "subscription_expiry_date":"2019-04-14 00:00:00",
        "created_on":"2019-03-15 00:00:00",
        "amount_paid_in_rs":"0",
        "subscription_status":"A",
        "coupon_applied_status":"Y",
        "coupon_code_applied":"TRIAL30",
        "transaction_from":""}]}]

*/

public class SubscriptionUser {

    public int error_code;

    public String message;

    public String bpl_status;

    public List<SubscriptionData> data;

    public class SubscriptionData {

        public String subscription_start_date;

        public String subscription_expiry_date;


        public long getStartTime() {
            return DateUtil.getTimeForServerDate(subscription_start_date);
        }

        public long getExpiryTime() {
            return DateUtil.getTimeForServerDate(subscription_expiry_date);
        }
    }
}
