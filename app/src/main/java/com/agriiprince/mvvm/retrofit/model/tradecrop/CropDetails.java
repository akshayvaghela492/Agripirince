package com.agriiprince.mvvm.retrofit.model.tradecrop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CropDetails implements Serializable
{
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("createdBy")
    @Expose
    private String createdBy;
    @SerializedName("createdByType")
    @Expose
    private String createdByType;
    @SerializedName("sowingCrop")
    @Expose
    private String sowingCrop;
    @SerializedName("sowingCropVariety")
    @Expose
    private String sowingCropVariety;
    @SerializedName("sowingCropGrade")
    @Expose
    private String sowingCropGrade;
    @SerializedName("sowingCropDate")
    @Expose
    private String sowingCropDate;
    @SerializedName("sowingCropArea")
    @Expose
    private String sowingCropArea;
    @SerializedName("sowingCropAreaUnit")
    @Expose
    private String sowingCropAreaUnit;
    @SerializedName("growingCropSeedType")
    @Expose
    private String growingCropSeedType;
    @SerializedName("growingCropSeedPesticide")
    @Expose
    private String growingCropSeedPesticide;
    @SerializedName("growingCropSeedFertilizer")
    @Expose
    private String growingCropSeedFertilizer;
    @SerializedName("harvestingCropGrade1")
    @Expose
    private String harvestingCropGrade1;
    @SerializedName("harvestingCropGrade2")
    @Expose
    private String harvestingCropGrade2;
    @SerializedName("harvestingCropGrade3")
    @Expose
    private String harvestingCropGrade3;
    @SerializedName("harvestingCropStartDate")
    @Expose
    private String harvestingCropStartDate;
    @SerializedName("harvestingCropFrequency")
    @Expose
    private String harvestingCropFrequency;
    @SerializedName("harvestingCropFrequencyTime")
    @Expose
    private String harvestingCropFrequencyTime;
    @SerializedName("harvestingCropFrequencyTimeLine")
    @Expose
    private String harvestingCropFrequencyTimeLine;
    @SerializedName("harvestingCropTotalPeriod")
    @Expose
    private String harvestingCropTotalPeriod;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedByType() {
        return createdByType;
    }

    public void setCreatedByType(String createdByType) {
        this.createdByType = createdByType;
    }

    public String getSowingCrop() {
        return sowingCrop;
    }

    public void setSowingCrop(String sowingCrop) {
        this.sowingCrop = sowingCrop;
    }

    public String getSowingCropVariety() {
        return sowingCropVariety;
    }

    public void setSowingCropVariety(String sowingCropVariety) {
        this.sowingCropVariety = sowingCropVariety;
    }

    public String getSowingCropGrade() {
        return sowingCropGrade;
    }

    public void setSowingCropGrade(String sowingCropGrade) {
        this.sowingCropGrade = sowingCropGrade;
    }

    public String getSowingCropDate() {
        return sowingCropDate;
    }

    public void setSowingCropDate(String sowingCropDate) {
        this.sowingCropDate = sowingCropDate;
    }

    public String getSowingCropArea() {
        return sowingCropArea;
    }

    public void setSowingCropArea(String sowingCropArea) {
        this.sowingCropArea = sowingCropArea;
    }

    public String getSowingCropAreaUnit() {
        return sowingCropAreaUnit;
    }

    public void setSowingCropAreaUnit(String sowingCropAreaUnit) {
        this.sowingCropAreaUnit = sowingCropAreaUnit;
    }

    public String getGrowingCropSeedType() {
        return growingCropSeedType;
    }

    public void setGrowingCropSeedType(String growingCropSeedType) {
        this.growingCropSeedType = growingCropSeedType;
    }

    public String getGrowingCropSeedPesticide() {
        return growingCropSeedPesticide;
    }

    public void setGrowingCropSeedPesticide(String growingCropSeedPesticide) {
        this.growingCropSeedPesticide = growingCropSeedPesticide;
    }

    public String getGrowingCropSeedFertilizer() {
        return growingCropSeedFertilizer;
    }

    public void setGrowingCropSeedFertilizer(String growingCropSeedFertilizer) {
        this.growingCropSeedFertilizer = growingCropSeedFertilizer;
    }

    public String getHarvestingCropGrade1() {
        return harvestingCropGrade1;
    }

    public void setHarvestingCropGrade1(String harvestingCropGrade1) {
        this.harvestingCropGrade1 = harvestingCropGrade1;
    }

    public String getHarvestingCropGrade2() {
        return harvestingCropGrade2;
    }

    public void setHarvestingCropGrade2(String harvestingCropGrade2) {
        this.harvestingCropGrade2 = harvestingCropGrade2;
    }

    public String getHarvestingCropGrade3() {
        return harvestingCropGrade3;
    }

    public void setHarvestingCropGrade3(String harvestingCropGrade3) {
        this.harvestingCropGrade3 = harvestingCropGrade3;
    }

    public String getHarvestingCropStartDate() {
        return harvestingCropStartDate;
    }

    public void setHarvestingCropStartDate(String harvestingCropStartDate) {
        this.harvestingCropStartDate = harvestingCropStartDate;
    }

    public String getHarvestingCropFrequency() {
        return harvestingCropFrequency;
    }

    public void setHarvestingCropFrequency(String harvestingCropFrequency) {
        this.harvestingCropFrequency = harvestingCropFrequency;
    }

    public String getHarvestingCropFrequencyTime() {
        return harvestingCropFrequencyTime;
    }

    public void setHarvestingCropFrequencyTime(String harvestingCropFrequencyTime) {
        this.harvestingCropFrequencyTime = harvestingCropFrequencyTime;
    }

    public String getHarvestingCropFrequencyTimeLine() {
        return harvestingCropFrequencyTimeLine;
    }

    public void setHarvestingCropFrequencyTimeLine(String harvestingCropFrequencyTimeLine) {
        this.harvestingCropFrequencyTimeLine = harvestingCropFrequencyTimeLine;
    }

    public String getHarvestingCropTotalPeriod() {
        return harvestingCropTotalPeriod;
    }

    public void setHarvestingCropTotalPeriod(String harvestingCropTotalPeriod) {
        this.harvestingCropTotalPeriod = harvestingCropTotalPeriod;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
