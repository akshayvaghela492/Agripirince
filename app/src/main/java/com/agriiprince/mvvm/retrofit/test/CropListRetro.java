package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.croplist.Croplist_byname;
import com.agriiprince.mvvm.retrofit.dto.croplist.Croplist_byClass;
import com.agriiprince.mvvm.retrofit.service.CropList;
import com.agriiprince.mvvm.util.Logs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CropListRetro {

    API_Manager api_manager=new API_Manager();
    CropList retrofit_interface = api_manager.getClient5().create(CropList.class);

    public void getcropbyname() {

        Call<Croplist_byname> call=retrofit_interface.getCropbyname("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","crop","kn");
        call.enqueue(new Callback<Croplist_byname>() {
            @Override
            public void onResponse(Call<Croplist_byname> call, Response<Croplist_byname> response) {
                Logs.d("CheckStatus", "onResponse: croplist by name");
                try {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                    Logs.d("CheckStatus",response.body().getData().get(1).getCrop_name());
                }
                catch (Exception e){e.getMessage();}
            }
            @Override
            public void onFailure(Call<Croplist_byname> call, Throwable t) {
                Log.d("CheckStatus","croplist by name error"+t.getMessage());
            }
        });
    }
    public void getcropbyclass() {

        Call<Croplist_byClass> call=retrofit_interface.getCropbyclass("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","pulses");
        call.enqueue(new Callback<Croplist_byClass>() {
            @Override
            public void onResponse(Call<Croplist_byClass> call, Response<Croplist_byClass> response) {
                Log.d("CheckStatus","onResponse: croplist by class ");
                try {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                    Logs.d("CheckStatus",response.body().getData().get(1).getCrop_name());
                }
                catch (Exception e){e.getMessage();}
            }
            @Override
            public void onFailure(Call<Croplist_byClass> call, Throwable t) {
                Log.d("CheckStatus","croplist by class error"+t.getMessage());
            }
        });
    }

}
