package com.agriiprince.mvvm.retrofit.model.tutorial;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LanguageRequest  implements Serializable
{

    @SerializedName("tutorialLanguage")
    @Expose
    private String tutorialLanguage;


    public String getTutorialLanguage() {
        return tutorialLanguage;
    }

    public void setTutorialLanguage(String tutorialLanguage) {
        this.tutorialLanguage = tutorialLanguage;
    }

}
