package com.agriiprince.mvvm.retrofit.model.checkprice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GovDataList implements Serializable {
    @SerializedName("data")
    @Expose
    private List<GovReportedPriceData> result = null;


    public List<GovReportedPriceData> getResult() {
        return result;
    }

    public void setResult(List<GovReportedPriceData> result) {
        this.result = result;
    }


}
