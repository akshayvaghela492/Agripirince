package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.login.LoginUser;
import com.agriiprince.mvvm.retrofit.dto.oe.Consignment;
import com.agriiprince.mvvm.retrofit.dto.oe.OeLoginResponse;
import com.agriiprince.mvvm.retrofit.dto.oe.OeUpdateProfile;
import com.agriiprince.mvvm.retrofit.dto.oe.UpdatePrice;
import com.agriiprince.mvvm.retrofit.service.OE;
import com.agriiprince.mvvm.util.Logs;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OERetro {

    API_Manager api_manager=new API_Manager();
    OE retrofit_interface = api_manager.getClient8().create(OE.class);


    public void OeLogin() {
        Call<OeLoginResponse> call = retrofit_interface.OeLogin("1478523690","123123","oe"," " , " " );
        call.enqueue(new Callback<OeLoginResponse>() {
            @Override
            public void onResponse(Call<OeLoginResponse> call, Response<OeLoginResponse> response) {
                Logs.d("CheckStatus", "success oe login "+response);
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus().toString());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getData().getOeName());
                }
            }
            @Override
            public void onFailure(Call<OeLoginResponse> call, Throwable t) {
                Log.d("CheckStatus", "fail oe login "+ t.getMessage());
            }
        });
    }



    public void UpdatePrice() {
        Call<UpdatePrice> call = retrofit_interface.UpdatePrice("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","5b3cc42c0c","Apple","ALL","all","2019-5-28","variety","all","mndi","0","0","0","100");
        call.enqueue(new Callback<UpdatePrice>() {
            @Override
            public void onResponse(Call<UpdatePrice> call, Response<UpdatePrice> response) {
                Logs.d("CheckStatus", "onResponse: update price Success");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                    Logs.d("CheckStatus",response.body().getData().getGeneralPrice());
                }
            }
            @Override
            public void onFailure(Call<UpdatePrice> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }

    public void UpdateProfile() {
        ArrayList<String> list = new ArrayList<>();
        list.add("20");
        list.add("30");

        Call<OeUpdateProfile> call = retrofit_interface.UpdateProfile("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww",list,list,list,"","","");
        call.enqueue(new Callback<OeUpdateProfile>() {
            @Override
            public void onResponse(Call<OeUpdateProfile> call, Response<OeUpdateProfile> response) {
                Logs.d("CheckStatus", "onResponse: update profile Success");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                }
            }
            @Override
            public void onFailure(Call<OeUpdateProfile> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }
    public void UserDetails() {
        Call<LoginUser> call = retrofit_interface.LoginDetails("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","5b3cc42c0c","Apple");
        call.enqueue(new Callback<LoginUser>() {
            @Override
            public void onResponse(Call<LoginUser> call, Response<LoginUser> response) {
                Logs.d("CheckStatus", "onResponse: login details Success");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus().toString());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getData().farmer_name);
                    Logs.d("CheckStatus",response.body().getCode().toString());
                }
            }
            @Override
            public void onFailure(Call<LoginUser> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }

    public void consignment() {
        Call<Consignment> call = retrofit_interface.Consignment("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww","","");
        call.enqueue(new Callback<Consignment>() {
            @Override
            public void onResponse(Call<Consignment> call, Response<Consignment> response) {
                Logs.d("CheckStatus", "onResponse: ");
                if (response.isSuccessful() && response.body() != null) {
                    Logs.d("CheckStatus",response.body().getStatus().toString());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getData().get(1).getFarmer_name()

                    );
                    Logs.d("CheckStatus",response.body().getCode().toString());
                }
            }
            @Override
            public void onFailure(Call<Consignment> call, Throwable t) {
                Log.d("CheckStatus", "fail entry");
            }
        });
    }
}
