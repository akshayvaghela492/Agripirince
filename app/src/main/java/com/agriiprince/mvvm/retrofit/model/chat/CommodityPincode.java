package com.agriiprince.mvvm.retrofit.model.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CommodityPincode implements Serializable {

    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("farmer_location")
    @Expose
    private String farmer_location;
    @SerializedName("commodity")
    @Expose
    private String commodity;
    @SerializedName("commodity_name")
    @Expose
    private String commodity_name;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getFarmer_location() {
        return farmer_location;
    }

    public void setFarmer_location(String farmer_location) {
        this.farmer_location = farmer_location;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getCommodity_name() {
        return commodity_name;
    }

    public void setCommodity_name(String commodity_name) {
        this.commodity_name = commodity_name;
    }
}
