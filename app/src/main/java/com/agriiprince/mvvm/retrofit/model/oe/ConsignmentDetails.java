package com.agriiprince.mvvm.retrofit.model.oe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ConsignmentDetails implements Serializable {
    @SerializedName("consignment_id")
    @Expose
    private String consignment_id;
    @SerializedName("consignment_no")
    @Expose
    private String consignment_no;
    @SerializedName("commodity_id")
    @Expose
    private String commodity_id;
    @SerializedName("farmer_id")
    @Expose
    private String farmer_id;
    @SerializedName("ca_id")
    @Expose
    private String ca_id;
    @SerializedName("trucker_id")
    @Expose
    private String trucker_id;
    @SerializedName("created_on")
    @Expose
    private String created_on;
    @SerializedName("updated_on")
    @Expose
    private String updated_on;
    @SerializedName("status_of_payment")
    @Expose
    private String status_of_payment;
    @SerializedName("starting_location")
    @Expose
    private String starting_location;
    @SerializedName("status_of_consignment")
    @Expose
    private String status_of_consignment;
    @SerializedName("consignment_amount")
    @Expose
    private String consignment_amount;
    @SerializedName("picture_of_commodity")
    @Expose
    private String picture_of_commodity;
    @SerializedName("video_of_commodity")
    @Expose
    private String video_of_commodity;
    @SerializedName("msp")
    @Expose
    private Integer msp;
    @SerializedName("flag_error")
    @Expose
    private Integer flag_error;
    @SerializedName("details_of_consignment")
    @Expose
    private String details_of_consignment;
    @SerializedName("max_days_wait")
    @Expose
    private Integer max_days_wait;
    @SerializedName("commodity_name")
    @Expose
    private String commodity_name;
    @SerializedName("farmer_name")
    @Expose
    private String farmer_name;
    @SerializedName("ca_name")
    @Expose
    private String ca_name;
    @SerializedName("trucker_name")
    @Expose
    private String trucker_name;

    public String getConsignment_id() {
        return consignment_id;
    }

    public void setConsignment_id(String consignment_id) {
        this.consignment_id = consignment_id;
    }

    public String getConsignment_no() {
        return consignment_no;
    }

    public void setConsignment_no(String consignment_no) {
        this.consignment_no = consignment_no;
    }

    public String getCommodity_id() {
        return commodity_id;
    }

    public void setCommodity_id(String commodity_id) {
        this.commodity_id = commodity_id;
    }

    public String getFarmer_id() {
        return farmer_id;
    }

    public void setFarmer_id(String farmer_id) {
        this.farmer_id = farmer_id;
    }

    public String getCa_id() {
        return ca_id;
    }

    public void setCa_id(String ca_id) {
        this.ca_id = ca_id;
    }

    public String getTrucker_id() {
        return trucker_id;
    }

    public void setTrucker_id(String trucker_id) {
        this.trucker_id = trucker_id;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }

    public String getStatus_of_payment() {
        return status_of_payment;
    }

    public void setStatus_of_payment(String status_of_payment) {
        this.status_of_payment = status_of_payment;
    }

    public String getStarting_location() {
        return starting_location;
    }

    public void setStarting_location(String starting_location) {
        this.starting_location = starting_location;
    }

    public String getStatus_of_consignment() {
        return status_of_consignment;
    }

    public void setStatus_of_consignment(String status_of_consignment) {
        this.status_of_consignment = status_of_consignment;
    }

    public String getConsignment_amount() {
        return consignment_amount;
    }

    public void setConsignment_amount(String consignment_amount) {
        this.consignment_amount = consignment_amount;
    }

    public String getPicture_of_commodity() {
        return picture_of_commodity;
    }

    public void setPicture_of_commodity(String picture_of_commodity) {
        this.picture_of_commodity = picture_of_commodity;
    }

    public String getVideo_of_commodity() {
        return video_of_commodity;
    }

    public void setVideo_of_commodity(String video_of_commodity) {
        this.video_of_commodity = video_of_commodity;
    }

    public Integer getMsp() {
        return msp;
    }

    public void setMsp(Integer msp) {
        this.msp = msp;
    }

    public Integer getFlag_error() {
        return flag_error;
    }

    public void setFlag_error(Integer flag_error) {
        this.flag_error = flag_error;
    }

    public String getDetails_of_consignment() {
        return details_of_consignment;
    }

    public void setDetails_of_consignment(String details_of_consignment) {
        this.details_of_consignment = details_of_consignment;
    }

    public Integer getMax_days_wait() {
        return max_days_wait;
    }

    public void setMax_days_wait(Integer max_days_wait) {
        this.max_days_wait = max_days_wait;
    }

    public String getCommodity_name() {
        return commodity_name;
    }

    public void setCommodity_name(String commodity_name) {
        this.commodity_name = commodity_name;
    }

    public String getFarmer_name() {
        return farmer_name;
    }

    public void setFarmer_name(String farmer_name) {
        this.farmer_name = farmer_name;
    }

    public String getCa_name() {
        return ca_name;
    }

    public void setCa_name(String ca_name) {
        this.ca_name = ca_name;
    }

    public String getTrucker_name() {
        return trucker_name;
    }

    public void setTrucker_name(String trucker_name) {
        this.trucker_name = trucker_name;
    }
}
