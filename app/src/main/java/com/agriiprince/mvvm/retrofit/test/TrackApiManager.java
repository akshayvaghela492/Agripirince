package com.agriiprince.mvvm.retrofit.test;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TrackApiManager {
    private Retrofit retrofit = null;
    private String BaseUrl="http://13.232.163.213:8000/api/v1/crop/track/";

    public Retrofit getClient() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BaseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
