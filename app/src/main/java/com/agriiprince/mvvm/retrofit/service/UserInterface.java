package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.login.FcmTokenUpdate;
import com.agriiprince.mvvm.retrofit.dto.login.Logout;
import com.agriiprince.mvvm.retrofit.dto.profile.UserUpdate;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.FigLogin;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.FpoResponse;
import com.agriiprince.mvvm.retrofit.dto.login.Login;
import com.agriiprince.mvvm.retrofit.dto.login.TokenGenerate;
import com.agriiprince.mvvm.retrofit.dto.login.UserRegistration;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.UpdateDeleteUser;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.UserCreate;
import com.agriiprince.mvvm.retrofit.dto.login.UserExist;
import com.agriiprince.mvvm.retrofit.dto.tradecrop.UserResponse;
import com.agriiprince.mvvm.retrofit.model.login.Profile;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserInterface {
    @FormUrlEncoded
    @POST("generate_token")
    Call<TokenGenerate> generateToken(@Field("user_id") String user_id,
                                      @Field("user_type") String user_type,
                                      @Field("password") String password);

    @FormUrlEncoded
    @POST("registration")
    Call<UserRegistration> registration(@Field("user_type") String user_type,
                                        @Field("name") String name,
                                        @Field("password") String password,
                                        @Field("city") String city,
                                        @Field("pincode") String pincode,
                                        @Field("mobile_number") String mobile_number,
                                        @Field("language") String language,
                                        @Field("fcm_token") String fcm_token);

    @FormUrlEncoded
    @POST("fig/login")
    Call<FigLogin> FigLogin(@Header("authentication") String token,
                            @Field("mobile") String mobile,
                            @Field("invite_code") String invite_code);
    @FormUrlEncoded
    @POST("login")
    Call<Login> Login(@Field("mobileno") String mobileno,
                      @Field("password") String password,
                      @Field("mode") String mode,
                      @Field("fcm_token") String fcm_token,
                      @Field("login_type") String login_type);

    @FormUrlEncoded
    @POST("update/fcm_token")
    Call<FcmTokenUpdate> FcmTokenUpdate(@Header("authentication") String token,
                                        @Field("user_type") String user_type,
                                        @Field("user_id") String user_id,
                                        @Field("token") String fcm_token);


    @FormUrlEncoded
    @POST("user_exists")
    Call<UserExist> userExist(@Field("mobileno") String mobileno,
                              @Field("user_type") String user_type);


    @FormUrlEncoded
    @POST("register/fpo")
    Call<FpoResponse> CreateFpo(@Header("authentication") String token,
                                @Field("name") String name,
                                @Field("email") String email,
                                @Field("zip") String zip,
                                @Field("mobile") String mobile);

    @FormUrlEncoded
    @POST("fpo/{id}/register/fig")
    Call<UserCreate> CreateFIG_AGG(@Header("authentication") String token,
                                    @Path("id") String Id,

                                   @Field("name") String name,
                                   @Field("email") String email,
                                   @Field("zip") String zip,
                                   @Field("mobile") String mobile);
    @FormUrlEncoded
    @POST("fpo/{id}/register/farmer")
    Call<UserCreate> CreateFarmer_Fpo(@Header("authentication") String token,
                                      @Path("id") String Id,

                                      @Field("farmer_name") String farmer_name,
                                      @Field("zip") String zip,
                                      @Field("farm_size") String farm_size,
                                      @Field("farmer_size_unit") String farmer_size_unit,
                                      @Field("mobile") String mobile);
    @FormUrlEncoded
    @POST("fig/{id}/register/farmer")
    Call<UserCreate> CreateFarmer_FIG(@Header("authentication") String token,
                                      @Path("id") String Id,

                                      @Field("farmer_name") String farmer_name,
                                      @Field("zip") String zip,
                                      @Field("farm_size") String farm_size,
                                      @Field("farmer_size_unit") String farmer_size_unit,
                                      @Field("mobile") String mobile);

    @GET("fpo/{id}/fig/list")
    Call<UserResponse> getFigList(@Header("authentication") String token,
                                  @Path("id") int id);

    @GET("fpo/{id}/farmer/list")
    Call<UserResponse> getFarmerListFPO(@Header("authentication") String token,
                                        @Path("id") String Id);

    @GET("fig/{id}/farmer/list")
    Call<UserResponse> getFarmerListFIG(@Header("authentication") String token,
                                        @Path("id") String Id);


    //For update/delete farmer and fig only use token of fpo.
    @FormUrlEncoded
    @PUT("update/{user_type}")
    Call<UpdateDeleteUser> updateUser( @Header("authentication") String token,
                                       @Path("user_type") String user_type,
                                       @Field("name") String name,
                                       @Field("user_id") String user_id,
                                       @Field("number") String number);
    @FormUrlEncoded
    @POST("delete/{user_type}")
    Call<UpdateDeleteUser> deleteUser( @Header("authentication") String token,
                                       @Path("user_type") String user_type,
                                       @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("logout")
    Call<Logout> logout(
                       @Field("mobileno") String mobileno,
                       @Field("user_id") String user_id, @Field("mode") String mode);


    @POST("farmer/profile/update")
    Call<UserUpdate> updateprofile(@Header("authentication") String token,
                                   @Body Profile param);
}
