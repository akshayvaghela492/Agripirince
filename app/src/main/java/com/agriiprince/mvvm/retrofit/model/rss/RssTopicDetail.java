package com.agriiprince.mvvm.retrofit.model.rss;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RssTopicDetail implements Serializable {
    @SerializedName("topic")
    @Expose
    private String topic;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("interested")
    @Expose
    private Boolean interested;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Boolean getInterested() {
        return interested;
    }

    public void setInterested(Boolean interested) {
        this.interested = interested;
    }
}
