package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.disease.DiseaseDetailResponse;
import com.agriiprince.mvvm.retrofit.dto.disease.DistinctDisease;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Disease  {

    @POST("names/get_crop_names")
    Call<DistinctDisease> getdistinctdisease(@Header("authentication") String token,
                                             @Body HashMap<String,String> user);

    @FormUrlEncoded
    @POST("diseases/get_details")
    Call<DiseaseDetailResponse> getDiseasedetail(@Header("authentication") String token,
                                                 @Field("crop_name") String crop_name,
                                                 @Field("primary_language") String primary_language,
                                                 @Field("secondary_language") String secondary_language
    );
}
