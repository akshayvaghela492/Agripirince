package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.chat.FetchMessage;
import com.agriiprince.mvvm.retrofit.dto.chat.GetCommodity;
import com.agriiprince.mvvm.retrofit.dto.chat.GetFarmerList;
import com.agriiprince.mvvm.retrofit.dto.chat.MessageStartUp;
import com.agriiprince.mvvm.retrofit.dto.chat.MessageStatus;
import com.agriiprince.mvvm.retrofit.dto.chat.PincodeCommodity;
import com.agriiprince.mvvm.retrofit.dto.chat.SendMessageResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Chat {
    @FormUrlEncoded
    @POST("insert_chat_message")
    Call<MessageStatus> insert_chat_message (@Header("authentication") String token,
                                             @FieldMap HashMap<String,String> params
    );

    @FormUrlEncoded
    @POST("send_message")
    Call<SendMessageResponse> send_message (@Header("authentication") String token,
                                            @Field("message") String message,
                                            @Field("sender_id") String sender_id,
                                            @Field("sender_type") String sender_type,
                                            @Field("receiver_id") String receiver_id,
                                            @Field("receiver_type") String receiver_type
    );


    @FormUrlEncoded
    @POST(" ")
    Call<FetchMessage> fetch_chat_message (@Header("authentication") String token,
                                           @Field("sender_id") String sender_id,
                                           @Field("sender_type") String sender_type,
                                           @Field("receiver_id") String receiver_id,
                                           @Field("receiver_type") String receiver_type,
                                           @Field("mode1") String mode1,
                                           @Field("mode2") String mode2
                                            );

    @FormUrlEncoded
    @POST(" ")
    Call<MessageStartUp> get_chat_message (@Header("authentication") String token,
                                           @Field("user_id") String user_id,
                                           @Field("user_type") String user_type,
                                           @Field("mode") String mode
                            );

    @FormUrlEncoded
    @POST(" ")
    Call<MessageStatus> update_status (@Header("authentication") String token,
                                             @Field("user_id") String user_id,
                                             @Field("user_type") String user_type,
                                             @Field("message_id") String message_id,
                                             @Field("mode") String mode,
                                             @Field("date_time") String date_time
                                        );
    @FormUrlEncoded
    @POST(" ")
    Call<GetCommodity> get_commodity (@Header("authentication") String token,
                                         @Field("user_id") String user_id,
                                         @Field("user_type") String user_type

    );

    @FormUrlEncoded
    @POST(" ")
    Call<PincodeCommodity> get_commodity_pincode (@Header("authentication") String token,
                                                  @Field("user_id") String user_id,
                                                  @Field("user_type") String user_type,
                                                  @Field("mode") String mode

    );
    @FormUrlEncoded
    @POST(" ")
    Call<GetFarmerList> get_farmer_list (@Header("authentication") String token,
                                               @Field("user_id") String user_id,
                                               @Field("user_type") String user_type

    );

}
