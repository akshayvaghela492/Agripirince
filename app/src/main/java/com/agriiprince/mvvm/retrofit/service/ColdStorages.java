package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.coldstorage.ColdStorageResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ColdStorages {

    @FormUrlEncoded
    @POST("get_cold_storage_list")
    Call<ColdStorageResponse> getcoldstorage(@Header("authentication") String token,
                                             @FieldMap HashMap<String, String> params);
}
