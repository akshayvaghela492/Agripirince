package com.agriiprince.mvvm.retrofit.dto.login;

import com.google.gson.annotations.SerializedName;

public class CheckUserExist {

    @SerializedName("error_code")
    public int errorCode;
    @SerializedName("message")
    public String message;
}
