package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.razorpay.CheckoutResponse;
import com.agriiprince.mvvm.retrofit.dto.razorpay.ManualSubscriptionResponse;
import com.agriiprince.mvvm.retrofit.dto.razorpay.RazorPayResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface SubscriptionNode {

    @FormUrlEncoded
    @POST("manual_fee_collection_request_for_subscription")
    Call<ManualSubscriptionResponse> manual_fee_collection(@Header("authentication") String token,
                                                           @Field("farmer_userId") String farmer_userId,
                                                           @Field("farmer_mobile") String farmer_mobile,
                                                           @Field("oe_mobile") String oe_mobile,
                                                           @Field("subscription_fee") String subscription_fee);

}
