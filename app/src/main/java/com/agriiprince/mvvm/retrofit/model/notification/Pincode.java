
package com.agriiprince.mvvm.retrofit.model.notification;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pincode implements Serializable
{

    @SerializedName("last_unread")
    @Expose
    private LastUnread lastUnread;
    @SerializedName("total_unread_notifications")
    @Expose
    private Integer totalUnreadNotifications;


    public LastUnread getLastUnread() {
        return lastUnread;
    }

    public void setLastUnread(LastUnread lastUnread) {
        this.lastUnread = lastUnread;
    }

    public Integer getTotalUnreadNotifications() {
        return totalUnreadNotifications;
    }

    public void setTotalUnreadNotifications(Integer totalUnreadNotifications) {
        this.totalUnreadNotifications = totalUnreadNotifications;
    }

}
