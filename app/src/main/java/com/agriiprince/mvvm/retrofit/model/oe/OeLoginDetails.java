package com.agriiprince.mvvm.retrofit.model.oe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OeLoginDetails implements Serializable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("oe_id")
    @Expose
    private String oeId;
    @SerializedName("fcm_token")
    @Expose
    private String fcmToken;
    @SerializedName("oe_name")
    @Expose
    private String oeName;
    @SerializedName("oe_business_name")
    @Expose
    private String oeBusinessName;
    @SerializedName("oe_contact")
    @Expose
    private String oeContact;
    @SerializedName("oe_dob")
    @Expose
    private Object oeDob;
    @SerializedName("oe_license")
    @Expose
    private String oeLicense;
    @SerializedName("oe_pan")
    @Expose
    private String oePan;
    @SerializedName("oe_otp")
    @Expose
    private String oeOtp;
    @SerializedName("oe_password")
    @Expose
    private String oePassword;
    @SerializedName("oe_address")
    @Expose
    private String oeAddress;
    @SerializedName("oe_location")
    @Expose
    private String oeLocation;
    @SerializedName("pincode")
    @Expose
    private Integer pincode;
    @SerializedName("mandi_address")
    @Expose
    private String mandiAddress;
    @SerializedName("my_mandi_list")
    @Expose
    private String myMandiList;
    @SerializedName("my_crop_list")
    @Expose
    private String myCropList;
    @SerializedName("interested_crop_list")
    @Expose
    private String interestedCropList;
    @SerializedName("interested_mandi_list")
    @Expose
    private String interestedMandiList;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("preferred_language")
    @Expose
    private String preferredLanguage;
    @SerializedName("secondary_language")
    @Expose
    private String secondaryLanguage;
    @SerializedName("terms_and_conditions")
    @Expose
    private Integer termsAndConditions;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("updated_on")
    @Expose
    private String updatedOn;
    @SerializedName("ap_certified")
    @Expose
    private Integer apCertified;
    @SerializedName("login_status")
    @Expose
    private String loginStatus;
    @SerializedName("asked_for_invite")
    @Expose
    private String askedForInvite;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOeId() {
        return oeId;
    }

    public void setOeId(String oeId) {
        this.oeId = oeId;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getOeName() {
        return oeName;
    }

    public void setOeName(String oeName) {
        this.oeName = oeName;
    }

    public String getOeBusinessName() {
        return oeBusinessName;
    }

    public void setOeBusinessName(String oeBusinessName) {
        this.oeBusinessName = oeBusinessName;
    }

    public String getOeContact() {
        return oeContact;
    }

    public void setOeContact(String oeContact) {
        this.oeContact = oeContact;
    }

    public Object getOeDob() {
        return oeDob;
    }

    public void setOeDob(Object oeDob) {
        this.oeDob = oeDob;
    }

    public String getOeLicense() {
        return oeLicense;
    }

    public void setOeLicense(String oeLicense) {
        this.oeLicense = oeLicense;
    }

    public String getOePan() {
        return oePan;
    }

    public void setOePan(String oePan) {
        this.oePan = oePan;
    }

    public String getOeOtp() {
        return oeOtp;
    }

    public void setOeOtp(String oeOtp) {
        this.oeOtp = oeOtp;
    }

    public String getOePassword() {
        return oePassword;
    }

    public void setOePassword(String oePassword) {
        this.oePassword = oePassword;
    }

    public String getOeAddress() {
        return oeAddress;
    }

    public void setOeAddress(String oeAddress) {
        this.oeAddress = oeAddress;
    }

    public String getOeLocation() {
        return oeLocation;
    }

    public void setOeLocation(String oeLocation) {
        this.oeLocation = oeLocation;
    }

    public Integer getPincode() {
        return pincode;
    }

    public void setPincode(Integer pincode) {
        this.pincode = pincode;
    }

    public String getMandiAddress() {
        return mandiAddress;
    }

    public void setMandiAddress(String mandiAddress) {
        this.mandiAddress = mandiAddress;
    }

    public String getMyMandiList() {
        return myMandiList;
    }

    public void setMyMandiList(String myMandiList) {
        this.myMandiList = myMandiList;
    }

    public String getMyCropList() {
        return myCropList;
    }

    public void setMyCropList(String myCropList) {
        this.myCropList = myCropList;
    }

    public String getInterestedCropList() {
        return interestedCropList;
    }

    public void setInterestedCropList(String interestedCropList) {
        this.interestedCropList = interestedCropList;
    }

    public String getInterestedMandiList() {
        return interestedMandiList;
    }

    public void setInterestedMandiList(String interestedMandiList) {
        this.interestedMandiList = interestedMandiList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPreferredLanguage() {
        return preferredLanguage;
    }

    public void setPreferredLanguage(String preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }

    public String getSecondaryLanguage() {
        return secondaryLanguage;
    }

    public void setSecondaryLanguage(String secondaryLanguage) {
        this.secondaryLanguage = secondaryLanguage;
    }

    public Integer getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(Integer termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Integer getApCertified() {
        return apCertified;
    }

    public void setApCertified(Integer apCertified) {
        this.apCertified = apCertified;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getAskedForInvite() {
        return askedForInvite;
    }

    public void setAskedForInvite(String askedForInvite) {
        this.askedForInvite = askedForInvite;
    }

}