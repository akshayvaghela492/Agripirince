package com.agriiprince.mvvm.retrofit.dto.subscription;

import com.google.gson.annotations.SerializedName;

import java.util.List;
// for migrated API created on 12/8/2019
public class SubscriptionCoupon {

    @SerializedName("error_code")
    private int errorCode;

    @SerializedName("message")
    private String message;

    @SerializedName("valid_coupon")
    private boolean validCoupon;

    @SerializedName("trial_activated")
    private boolean trialActivated;

    @SerializedName("data")
    private List<DiscountData> discountData;


    public int getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }

    public boolean isValidCoupon() {
        return validCoupon;
    }

    public boolean isTrialActivated() {
        return trialActivated;
    }

    public List<DiscountData> getDiscountData() {
        return discountData;
    }

    public class DiscountData {

        @SerializedName("discount_id")
        private String discountId;

        @SerializedName("valid_in_days")
        private String validityDays;

        @SerializedName("discount_in_percent")
        private String discountPercent;

        @SerializedName("discount_in_rupees")
        private String discountAmount;

        @SerializedName("coupon_code")
        private String couponCode;


        public String getDiscountId() {
            return discountId;
        }

        public String getValidityDays() {
            return validityDays;
        }

        public String getDiscountPercent() {
            return discountPercent;
        }

        public String getDiscountAmount() {
            return discountAmount;
        }

        public String getCouponCode() {
            return couponCode;
        }
    }

}
