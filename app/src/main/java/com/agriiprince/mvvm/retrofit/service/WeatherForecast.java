package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.weather.WeatherResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface WeatherForecast {

    @FormUrlEncoded
    @POST("get_weather_data")
    Call<WeatherResponse> WeatherResponse(@Header("authentication") String token,
                                          @Field("date") String date,
                                          @Field("city") String city
                                                                        );
}
