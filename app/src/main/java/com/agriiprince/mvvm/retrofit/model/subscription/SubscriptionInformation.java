package com.agriiprince.mvvm.retrofit.model.subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubscriptionInformation implements Serializable {
    @SerializedName("subscription_expiry_date")
    @Expose
    private String subscription_expiry_date;
    @SerializedName( "subscription_start_date")
    @Expose
    private String subscription_start_date;


    public String getSubscription_expiry_date() {
        return subscription_expiry_date;
    }

    public void setSubscription_expiry_date(String subscription_expiry_date) {
        this.subscription_expiry_date = subscription_expiry_date;
    }

    public String getSubscription_start_date() {
        return subscription_start_date;
    }

    public void setSubscription_start_date(String subscription_start_date) {
        this.subscription_start_date = subscription_start_date;
    }
}
