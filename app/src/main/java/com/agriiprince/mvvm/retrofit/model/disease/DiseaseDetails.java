package com.agriiprince.mvvm.retrofit.model.disease;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DiseaseDetails implements Serializable
{
    @SerializedName("crop_disease_id")
    @Expose
    private String cropDiseaseId;
    @SerializedName("crop_name")
    @Expose
    private String cropName;
    @SerializedName("crop_diseases")
    @Expose
    private String cropDiseases;
    @SerializedName("crop_causes")
    @Expose
    private String cropCauses;
    @SerializedName("crop_symptom")
    @Expose
    private String cropSymptom;
    @SerializedName("crop_diseases_scientific_name")
    @Expose
    private String cropDiseasesScientificName;
    @SerializedName("crop_diseases_category")
    @Expose
    private String cropDiseasesCategory;
    @SerializedName("crop_diseases_management")
    @Expose
    private String cropDiseasesManagement;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("crop_images")
    @Expose
    private String cropImages;
    @SerializedName("plant_name")
    @Expose
    private String plantName;
    @SerializedName("disease")
    @Expose
    private String disease;
    @SerializedName("effecting_stage")
    @Expose
    private String effectingStage;
    @SerializedName("organic_cures")
    @Expose
    private String organicCures;
    @SerializedName("icar_recommended_cure_1")
    @Expose
    private String icarRecommendedCure1;
    @SerializedName("icar_recommended_cure_2")
    @Expose
    private String icarRecommendedCure2;
    @SerializedName("pesticide_1")
    @Expose
    private String pesticide1;
    @SerializedName("application_1")
    @Expose
    private String application1;
    @SerializedName("pesticide_2")
    @Expose
    private String pesticide2;
    @SerializedName("application_2")
    @Expose
    private String application2;
    @SerializedName("pesticide_3")
    @Expose
    private String pesticide3;
    @SerializedName("application_3")
    @Expose
    private String application3;
    @SerializedName("pesticide_4")
    @Expose
    private String pesticide4;
    @SerializedName("application_4")
    @Expose
    private String application4;
    @SerializedName("pesticide_5")
    @Expose
    private String pesticide5;
    @SerializedName("application_5")
    @Expose
    private String application5;
    @SerializedName("dose_acre")
    @Expose
    private String doseAcre;
    @SerializedName("iffco")
    @Expose
    private String iffco;
    @SerializedName("iffco_photo_link")
    @Expose
    private String iffcoPhotoLink;
    @SerializedName("syngenta")
    @Expose
    private String syngenta;
    @SerializedName("syngenta_photo_link")
    @Expose
    private String syngentaPhotoLink;
    @SerializedName("dhanuka")
    @Expose
    private String dhanuka;
    @SerializedName("dhanuka_photo_link")
    @Expose
    private String dhanukaPhotoLink;
    @SerializedName("buyer")
    @Expose
    private String buyer;
    @SerializedName("buyer_photo_link")
    @Expose
    private String buyerPhotoLink;
    @SerializedName("excel_crop_care")
    @Expose
    private String excelCropCare;
    @SerializedName("excel_crop_care_photo_link")
    @Expose
    private String excelCropCarePhotoLink;
    @SerializedName("coromondal")
    @Expose
    private String coromondal;
    @SerializedName("coromondal_photo_link")
    @Expose
    private String coromondalPhotoLink;
    @SerializedName("pi")
    @Expose
    private String pi;
    @SerializedName("pi_photo_link")
    @Expose
    private String piPhotoLink;
    @SerializedName("tata_rallis")
    @Expose
    private String tataRallis;
    @SerializedName("tata_rallis_photo_link")
    @Expose
    private String tataRallisPhotoLink;
    @SerializedName("upl")
    @Expose
    private String upl;
    @SerializedName("upl_photo_link")
    @Expose
    private String uplPhotoLink;
    @SerializedName("crystal")
    @Expose
    private String crystal;
    @SerializedName("crystal_photo_link")
    @Expose
    private String crystalPhotoLink;
    @SerializedName("nagarjuna")
    @Expose
    private String nagarjuna;
    @SerializedName("nagarjuna_photo_link")
    @Expose
    private String nagarjunaPhotoLink;
    @SerializedName("ill")
    @Expose
    private String ill;
    @SerializedName("ill_photo_link")
    @Expose
    private String illPhotoLink;

    public String getCropDiseaseId() {
        return cropDiseaseId;
    }

    public void setCropDiseaseId(String cropDiseaseId) {
        this.cropDiseaseId = cropDiseaseId;
    }

    public String getCropName() {
        return cropName;
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public String getCropDiseases() {
        return cropDiseases;
    }

    public void setCropDiseases(String cropDiseases) {
        this.cropDiseases = cropDiseases;
    }

    public String getCropCauses() {
        return cropCauses;
    }

    public void setCropCauses(String cropCauses) {
        this.cropCauses = cropCauses;
    }

    public String getCropSymptom() {
        return cropSymptom;
    }

    public void setCropSymptom(String cropSymptom) {
        this.cropSymptom = cropSymptom;
    }

    public String getCropDiseasesScientificName() {
        return cropDiseasesScientificName;
    }

    public void setCropDiseasesScientificName(String cropDiseasesScientificName) {
        this.cropDiseasesScientificName = cropDiseasesScientificName;
    }

    public String getCropDiseasesCategory() {
        return cropDiseasesCategory;
    }

    public void setCropDiseasesCategory(String cropDiseasesCategory) {
        this.cropDiseasesCategory = cropDiseasesCategory;
    }

    public String getCropDiseasesManagement() {
        return cropDiseasesManagement;
    }

    public void setCropDiseasesManagement(String cropDiseasesManagement) {
        this.cropDiseasesManagement = cropDiseasesManagement;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCropImages() {
        return cropImages;
    }

    public void setCropImages(String cropImages) {
        this.cropImages = cropImages;
    }

    public String getPlantName() {
        return plantName;
    }

    public void setPlantName(String plantName) {
        this.plantName = plantName;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getEffectingStage() {
        return effectingStage;
    }

    public void setEffectingStage(String effectingStage) {
        this.effectingStage = effectingStage;
    }

    public String getOrganicCures() {
        return organicCures;
    }

    public void setOrganicCures(String organicCures) {
        this.organicCures = organicCures;
    }

    public String getIcarRecommendedCure1() {
        return icarRecommendedCure1;
    }

    public void setIcarRecommendedCure1(String icarRecommendedCure1) {
        this.icarRecommendedCure1 = icarRecommendedCure1;
    }

    public String getIcarRecommendedCure2() {
        return icarRecommendedCure2;
    }

    public void setIcarRecommendedCure2(String icarRecommendedCure2) {
        this.icarRecommendedCure2 = icarRecommendedCure2;
    }

    public String getPesticide1() {
        return pesticide1;
    }

    public void setPesticide1(String pesticide1) {
        this.pesticide1 = pesticide1;
    }

    public String getApplication1() {
        return application1;
    }

    public void setApplication1(String application1) {
        this.application1 = application1;
    }

    public String getPesticide2() {
        return pesticide2;
    }

    public void setPesticide2(String pesticide2) {
        this.pesticide2 = pesticide2;
    }

    public String getApplication2() {
        return application2;
    }

    public void setApplication2(String application2) {
        this.application2 = application2;
    }

    public String getPesticide3() {
        return pesticide3;
    }

    public void setPesticide3(String pesticide3) {
        this.pesticide3 = pesticide3;
    }

    public String getApplication3() {
        return application3;
    }

    public void setApplication3(String application3) {
        this.application3 = application3;
    }

    public String getPesticide4() {
        return pesticide4;
    }

    public void setPesticide4(String pesticide4) {
        this.pesticide4 = pesticide4;
    }

    public String getApplication4() {
        return application4;
    }

    public void setApplication4(String application4) {
        this.application4 = application4;
    }

    public String getPesticide5() {
        return pesticide5;
    }

    public void setPesticide5(String pesticide5) {
        this.pesticide5 = pesticide5;
    }

    public String getApplication5() {
        return application5;
    }

    public void setApplication5(String application5) {
        this.application5 = application5;
    }

    public String getDoseAcre() {
        return doseAcre;
    }

    public void setDoseAcre(String doseAcre) {
        this.doseAcre = doseAcre;
    }

    public String getIffco() {
        return iffco;
    }

    public void setIffco(String iffco) {
        this.iffco = iffco;
    }

    public String getIffcoPhotoLink() {
        return iffcoPhotoLink;
    }

    public void setIffcoPhotoLink(String iffcoPhotoLink) {
        this.iffcoPhotoLink = iffcoPhotoLink;
    }

    public String getSyngenta() {
        return syngenta;
    }

    public void setSyngenta(String syngenta) {
        this.syngenta = syngenta;
    }

    public String getSyngentaPhotoLink() {
        return syngentaPhotoLink;
    }

    public void setSyngentaPhotoLink(String syngentaPhotoLink) {
        this.syngentaPhotoLink = syngentaPhotoLink;
    }

    public String getDhanuka() {
        return dhanuka;
    }

    public void setDhanuka(String dhanuka) {
        this.dhanuka = dhanuka;
    }

    public String getDhanukaPhotoLink() {
        return dhanukaPhotoLink;
    }

    public void setDhanukaPhotoLink(String dhanukaPhotoLink) {
        this.dhanukaPhotoLink = dhanukaPhotoLink;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getBuyerPhotoLink() {
        return buyerPhotoLink;
    }

    public void setBuyerPhotoLink(String buyerPhotoLink) {
        this.buyerPhotoLink = buyerPhotoLink;
    }

    public String getExcelCropCare() {
        return excelCropCare;
    }

    public void setExcelCropCare(String excelCropCare) {
        this.excelCropCare = excelCropCare;
    }

    public String getExcelCropCarePhotoLink() {
        return excelCropCarePhotoLink;
    }

    public void setExcelCropCarePhotoLink(String excelCropCarePhotoLink) {
        this.excelCropCarePhotoLink = excelCropCarePhotoLink;
    }

    public String getCoromondal() {
        return coromondal;
    }

    public void setCoromondal(String coromondal) {
        this.coromondal = coromondal;
    }

    public String getCoromondalPhotoLink() {
        return coromondalPhotoLink;
    }

    public void setCoromondalPhotoLink(String coromondalPhotoLink) {
        this.coromondalPhotoLink = coromondalPhotoLink;
    }

    public String getPi() {
        return pi;
    }

    public void setPi(String pi) {
        this.pi = pi;
    }

    public String getPiPhotoLink() {
        return piPhotoLink;
    }

    public void setPiPhotoLink(String piPhotoLink) {
        this.piPhotoLink = piPhotoLink;
    }

    public String getTataRallis() {
        return tataRallis;
    }

    public void setTataRallis(String tataRallis) {
        this.tataRallis = tataRallis;
    }

    public String getTataRallisPhotoLink() {
        return tataRallisPhotoLink;
    }

    public void setTataRallisPhotoLink(String tataRallisPhotoLink) {
        this.tataRallisPhotoLink = tataRallisPhotoLink;
    }

    public String getUpl() {
        return upl;
    }

    public void setUpl(String upl) {
        this.upl = upl;
    }

    public String getUplPhotoLink() {
        return uplPhotoLink;
    }

    public void setUplPhotoLink(String uplPhotoLink) {
        this.uplPhotoLink = uplPhotoLink;
    }

    public String getCrystal() {
        return crystal;
    }

    public void setCrystal(String crystal) {
        this.crystal = crystal;
    }

    public String getCrystalPhotoLink() {
        return crystalPhotoLink;
    }

    public void setCrystalPhotoLink(String crystalPhotoLink) {
        this.crystalPhotoLink = crystalPhotoLink;
    }

    public String getNagarjuna() {
        return nagarjuna;
    }

    public void setNagarjuna(String nagarjuna) {
        this.nagarjuna = nagarjuna;
    }

    public String getNagarjunaPhotoLink() {
        return nagarjunaPhotoLink;
    }

    public void setNagarjunaPhotoLink(String nagarjunaPhotoLink) {
        this.nagarjunaPhotoLink = nagarjunaPhotoLink;
    }

    public String getIll() {
        return ill;
    }

    public void setIll(String ill) {
        this.ill = ill;
    }

    public String getIllPhotoLink() {
        return illPhotoLink;
    }

    public void setIllPhotoLink(String illPhotoLink) {
        this.illPhotoLink = illPhotoLink;
    }
}
