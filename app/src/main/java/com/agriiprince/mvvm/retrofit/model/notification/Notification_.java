
package com.agriiprince.mvvm.retrofit.model.notification;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification_ implements Serializable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("message_title")
    @Expose
    private String messageTitle;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("message_type")
    @Expose
    private String messageType;
    @SerializedName("sender")
    @Expose
    private String sender;
    @SerializedName("sender_type")
    @Expose
    private String senderType;
    @SerializedName("receiver")
    @Expose
    private String receiver;
    @SerializedName("receiver_type")
    @Expose
    private String receiverType;
    @SerializedName("medium")
    @Expose
    private String medium;
    @SerializedName("deliver_status")
    @Expose
    private String deliverStatus;
    @SerializedName("delivered_date_time")
    @Expose
    private Object deliveredDateTime;
    @SerializedName("read_status")
    @Expose
    private String readStatus;
    @SerializedName("read_date_time")
    @Expose
    private Object readDateTime;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("commodity_id")
    @Expose
    private String commodityId;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("chat_type")
    @Expose
    private String chatType;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSenderType() {
        return senderType;
    }

    public void setSenderType(String senderType) {
        this.senderType = senderType;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getReceiverType() {
        return receiverType;
    }

    public void setReceiverType(String receiverType) {
        this.receiverType = receiverType;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getDeliverStatus() {
        return deliverStatus;
    }

    public void setDeliverStatus(String deliverStatus) {
        this.deliverStatus = deliverStatus;
    }

    public Object getDeliveredDateTime() {
        return deliveredDateTime;
    }

    public void setDeliveredDateTime(Object deliveredDateTime) {
        this.deliveredDateTime = deliveredDateTime;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }

    public Object getReadDateTime() {
        return readDateTime;
    }

    public void setReadDateTime(Object readDateTime) {
        this.readDateTime = readDateTime;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(String commodityId) {
        this.commodityId = commodityId;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getChatType() {
        return chatType;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }

}
