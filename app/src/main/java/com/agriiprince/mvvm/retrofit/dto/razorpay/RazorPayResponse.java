package com.agriiprince.mvvm.retrofit.dto.razorpay;

import com.agriiprince.mvvm.retrofit.model.apk_version.Apk_links;
import com.agriiprince.mvvm.retrofit.model.razorpay.GetOrderId;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RazorPayResponse implements Serializable {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private GetOrderId data;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private Integer code;
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public GetOrderId getData() {
        return data;
    }

    public void setData(GetOrderId data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
