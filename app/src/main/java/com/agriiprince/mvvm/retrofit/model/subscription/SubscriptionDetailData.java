package com.agriiprince.mvvm.retrofit.model.subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SubscriptionDetailData implements Serializable {

    @SerializedName("data")
    @Expose
    private List<SubscriptionDetailList> data = null;
    @SerializedName("bpl_status")
    @Expose
    private String bplStatus;
    @SerializedName("bpl_amount")
    @Expose
    private Integer bplAmount;
    @SerializedName("manual_collection_charges")
    @Expose
    private Integer manualCollectionCharges;

    public List<SubscriptionDetailList> getData() {
        return data;
    }

    public void setData(List<SubscriptionDetailList> data) {
        this.data = data;
    }

    public String getBplStatus() {
        return bplStatus;
    }

    public void setBplStatus(String bplStatus) {
        this.bplStatus = bplStatus;
    }

    public Integer getBplAmount() {
        return bplAmount;
    }

    public void setBplAmount(Integer bplAmount) {
        this.bplAmount = bplAmount;
    }

    public Integer getManualCollectionCharges() {
        return manualCollectionCharges;
    }

    public void setManualCollectionCharges(Integer manualCollectionCharges) {
        this.manualCollectionCharges = manualCollectionCharges;
    }

}
