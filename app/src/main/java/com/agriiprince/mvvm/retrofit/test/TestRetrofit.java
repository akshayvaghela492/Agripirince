package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.retrofit.model.tutorial.TutorialModel;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class TestRetrofit
{
    public void testing()

    {
        TrackCropRetro trackCropRetro=new TrackCropRetro();
        //trackCropRetro.saveCrop();
        //trackCropRetro.saveOrder();
        //trackCropRetro.getCrop();
        //trackCropRetro.getActivitylist();
        //trackCropRetro.getActivityDetails();
        //trackCropRetro.getSprinklers();
        //trackCropRetro.getPesticide();
        //trackCropRetro.getFerilizers();
        //trackCropRetro.getSuitability();


     UserRetro i = new UserRetro();
       //i.generateToken();
       //i.Registration();
       //i.FigLogin();
       //i.Login();
       //i.FcmTokenUpdate();
       //i.userExist();
       //i.CreateFpo();
       //i.CreateFIG_AGG();
       //i.CreateFarmer_Fpo();
       //i.CreateFarmer_FIG();
       //i.getFarmerListFIG();
       //i.getFarmerListFPO();
       //i.getfiglist();
       //i.updateFarmer();
       //i.deleteFarmer();
       //i.updateFig();
       //i.deleteFig();
        //i.logout();
        //i.updateprofile();


       CropRetro j = new CropRetro();
        //j.getfarmercrop();
        //j.getmedia();
        //j.Deletemedia();
        //j.saveImage();
        //j.saveVideo();
        //j.SaveCropFpo();
        //j.SaveCropFig();
        //j.SaveCropFarmer();
        //j.DeleteCrop();

     CropListRetro q=new CropListRetro();
        //q.getcropbyname();
        //q.getcropbyclass();

      ColdStorageRetro k=new ColdStorageRetro();
      //k.getcoldstorage();

      NotificationRetro n=new NotificationRetro();
      //n.userNotification();
      //n.updateStatus();
      //n.getmsg();

      WeatherRetro w=new WeatherRetro();
      //w.WeatherForecast();

      ApkRetro a=new ApkRetro();
      //a.CheckApkVersion();

      OERetro p =new OERetro();
      //p.UpdatePrice();
        //p.OeLogin();

        RazorPayTest payTest = new RazorPayTest();
        //payTest.get_order_id();
        //payTest.checkout_capture_payment();
        //payTest.manual_fee_collection();

        CheckPriceRetro priceRetro =new  CheckPriceRetro();
        //priceRetro.GovReportedPriceData();
        //priceRetro.HistoricalPriceData();

        CARetro ca = new CARetro();
        //ca.CAList();

        ChatRetro chat=new ChatRetro();
        //chat.insert_chat_message();
        //chat.send_message();

        TutorialRetro tut = new TutorialRetro();
         //tut.tutorial();

        DiseaseRetro d=new DiseaseRetro();
        //d.getdisease();
        //d.getDiseasedetails();

        //testvolley();
    }

    public void testvolley()
    {Log.d("VolleyStatus", "testing");
        StringRequest request = new StringRequest(Request.Method.GET, "http://13.233.94.164:8000/api/v1/tutorial",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("VolleyStatus", response);
                        try {

                            JSONObject Object = new JSONObject(response);
                            int code=Object.getInt("status");
                            Log.d("VolleyStatus ", code+"");
                            String msg=Object.getString("message");
                            Log.d("VolleyStatus ", msg);
                            String tutorial = Object.getString("data");
                            Gson gson = new Gson();
                            TutorialModel model = gson.fromJson(tutorial,TutorialModel.class);
                            Log.d("VolleyStatus ", model.getTutorial().get(0).getSectionTitle());
                        }
                        catch (Exception e){}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("VolleyStatus", error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("tutorialLanguage", "en");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("authentication", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxNCwibmFtZSI6Im1lZ1xuIiwiZW1haWwiOiJtZWdAZ21haWwuY29tIiwiemlwQ29kZSI6MTIzLCJtb2JpbGUiOjMyMTMyMTMyMTMsInN0YXR1cyI6MSwiY3JlYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidXBkYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidHlwZSI6ImZwbyJ9LCJpYXQiOjE1NjYwMjMwNDgsImV4cCI6MTU2ODYxNTA0OH0.-yaQVXHScLrl-poZsV3alGKC4YLiPUhvoIEhD-h0g6Y");
                header.put("Content-Type","application/json");
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(request);

    }

}

