package com.agriiprince.mvvm.retrofit.model.tutorial;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubSection implements Serializable
{
    @SerializedName("sub_section_code")
    @Expose
    private Integer subSectionCode;
    @SerializedName("sub_section_title")
    @Expose
    private String subSectionTitle;
    @SerializedName("sub_section_content")
    @Expose
    private String subSectionContent;

    public Integer getSubSectionCode() {
        return subSectionCode;
    }

    public void setSubSectionCode(Integer subSectionCode) {
        this.subSectionCode = subSectionCode;
    }

    public String getSubSectionTitle() {
        return subSectionTitle;
    }

    public void setSubSectionTitle(String subSectionTitle) {
        this.subSectionTitle = subSectionTitle;
    }

    public String getSubSectionContent() {
        return subSectionContent;
    }

    public void setSubSectionContent(String subSectionContent) {
        this.subSectionContent = subSectionContent;
    }
}