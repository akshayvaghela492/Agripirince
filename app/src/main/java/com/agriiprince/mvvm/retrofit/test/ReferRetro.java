package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.mvvm.retrofit.model.referral.ReferNumber;
import com.agriiprince.mvvm.retrofit.service.Refer;
import com.agriiprince.mvvm.util.Logs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ReferRetro {
    private Retrofit retrofit = null;
    private String baseurl=" ";

    public Retrofit getClient() {
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseurl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    Refer retrofit_interface = getClient().create(Refer.class);

    public void ReferNumber() {

        Call<ReferNumber> call=retrofit_interface.REFER_NUMBER("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicmFjaHVyaSIsInVzZXJfaWQiOiJYRUIxNEtZUiIsInBhc3N3b3JkIjoiMTIzMTIzIn0sImlhdCI6MTU2MTcyNjk2NiwiZXhwIjoxNTY0MzE4OTY2fQ.kGwARZFq-ndaSaU4rwV8zrWX5mkJxp4OZ4AC5BMubTY","","","");
        call.enqueue(new Callback<ReferNumber>() {
            @Override
            public void onResponse(Call<ReferNumber> call, Response<ReferNumber> response) {
                Logs.d("CheckStatus", "onResponse: ");
                try {
                    Logs.d("CheckStatus",response.body().getStatus());
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getCode().toString());
                }
                catch (Exception e){e.getMessage();}
            }
            @Override
            public void onFailure(Call<ReferNumber> call, Throwable t) {
                Log.d("CheckStatus","error"+t.getMessage());
            }
        });
    }
}
