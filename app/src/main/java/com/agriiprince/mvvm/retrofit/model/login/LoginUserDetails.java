package com.agriiprince.mvvm.retrofit.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginUserDetails implements Serializable {
    @SerializedName("created_on")
    @Expose
    public String created_on;
    @SerializedName("updated_on")
    @Expose
    public String updated_on;
    @SerializedName("farmer_id")
    @Expose
    public String farmer_id;
    @SerializedName("farmer_name")
    @Expose
    public String farmer_name;
    @SerializedName("farmer_contact")
    @Expose
    public String farmer_contact;
    @SerializedName("farmer_otp")
    @Expose
    public Integer farmer_otp;
    @SerializedName("farmer_password")
    @Expose
    public String farmer_password;
    @SerializedName("farmer_address")
    @Expose
    public String farmer_address;
    @SerializedName("farmer_location")
    @Expose
    public String farmer_location;//city
    @SerializedName("farmer_pincode")
    @Expose
    public String farmer_pincode;
    @SerializedName("preferred_language")
    @Expose
    public String preferred_language;
    @SerializedName("secondary_language")
    @Expose
    public String secondary_language;
    @SerializedName("bank_name")
    @Expose
    public String bank_name;
    @SerializedName("bank_account_number")
    @Expose
    public String bank_account_number;
    @SerializedName("has_net_banking")
    @Expose
    public String has_net_banking;
    @SerializedName("has_paytm")
    @Expose
    public String has_paytm;
    @SerializedName("has_bhim")
    @Expose
    public String has_bhim;
    @SerializedName("screen_saver_enabled")
    @Expose
    public String screen_saver_enabled;
    @SerializedName("average_annual_income")
    @Expose
    public Integer average_annual_income;
    @SerializedName("land_in_acres")
    @Expose
    public Integer land_in_acres;
    @SerializedName("price_measuring_unit")
    @Expose
    public String price_measuring_unit;
    @SerializedName("area_measuring_unit")
    @Expose
    public String area_measuring_unit;
    @SerializedName("nearest_mandi_name")
    @Expose
    public String nearest_mandi_name;
    @SerializedName("nearest_mandi_lat")
    @Expose
    public double nearest_mandi_lat;
    @SerializedName("nearest_mandi_log")
    @Expose
    public double nearest_mandi_log;
    @SerializedName("interested_mandi_list")
    @Expose
    public String interested_mandi_list;
    @SerializedName("interested_crop_list")
    @Expose
    public String interested_crop_list;
    @SerializedName("interested_rss_topics")
    @Expose
    public String interested_rss_topics;
    @SerializedName("bpl_image")
    @Expose
    public String bpl_image;
    @SerializedName("bpl_status")
    @Expose
    public String bpl_status;
    @SerializedName("login_status")
    @Expose
    public String login_status;

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }

    public String getFarmer_id() {
        return farmer_id;
    }

    public void setFarmer_id(String farmer_id) {
        this.farmer_id = farmer_id;
    }

    public String getFarmer_name() {
        return farmer_name;
    }

    public void setFarmer_name(String farmer_name) {
        this.farmer_name = farmer_name;
    }

    public String getFarmer_contact() {
        return farmer_contact;
    }

    public void setFarmer_contact(String farmer_contact) {
        this.farmer_contact = farmer_contact;
    }

    public Integer getFarmer_otp() {
        return farmer_otp;
    }

    public void setFarmer_otp(Integer farmer_otp) {
        this.farmer_otp = farmer_otp;
    }

    public String getFarmer_password() {
        return farmer_password;
    }

    public void setFarmer_password(String farmer_password) {
        this.farmer_password = farmer_password;
    }

    public String getFarmer_address() {
        return farmer_address;
    }

    public void setFarmer_address(String farmer_address) {
        this.farmer_address = farmer_address;
    }

    public String getFarmer_location() {
        return farmer_location;
    }

    public void setFarmer_location(String farmer_location) {
        this.farmer_location = farmer_location;
    }

    public String getFarmer_pincode() {
        return farmer_pincode;
    }

    public void setFarmer_pincode(String farmer_pincode) {
        this.farmer_pincode = farmer_pincode;
    }

    public String getPreferred_language() {
        return preferred_language;
    }

    public void setPreferred_language(String preferred_language) {
        this.preferred_language = preferred_language;
    }

    public String getSecondary_language() {
        return secondary_language;
    }

    public void setSecondary_language(String secondary_language) {
        this.secondary_language = secondary_language;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBank_account_number() {
        return bank_account_number;
    }

    public void setBank_account_number(String bank_account_number) {
        this.bank_account_number = bank_account_number;
    }

    public String getHas_net_banking() {
        return has_net_banking;
    }

    public void setHas_net_banking(String has_net_banking) {
        this.has_net_banking = has_net_banking;
    }

    public String getHas_paytm() {
        return has_paytm;
    }

    public void setHas_paytm(String has_paytm) {
        this.has_paytm = has_paytm;
    }

    public String getHas_bhim() {
        return has_bhim;
    }

    public void setHas_bhim(String has_bhim) {
        this.has_bhim = has_bhim;
    }

    public String getScreen_saver_enabled() {
        return screen_saver_enabled;
    }

    public void setScreen_saver_enabled(String screen_saver_enabled) {
        this.screen_saver_enabled = screen_saver_enabled;
    }

    public Integer getAverage_annual_income() {
        return average_annual_income;
    }

    public void setAverage_annual_income(Integer average_annual_income) {
        this.average_annual_income = average_annual_income;
    }

    public Integer getLand_in_acres() {
        return land_in_acres;
    }

    public void setLand_in_acres(Integer land_in_acres) {
        this.land_in_acres = land_in_acres;
    }

    public String getPrice_measuring_unit() {
        return price_measuring_unit;
    }

    public void setPrice_measuring_unit(String price_measuring_unit) {
        this.price_measuring_unit = price_measuring_unit;
    }

    public String getArea_measuring_unit() {
        return area_measuring_unit;
    }

    public void setArea_measuring_unit(String area_measuring_unit) {
        this.area_measuring_unit = area_measuring_unit;
    }

    public String getNearest_mandi_name() {
        return nearest_mandi_name;
    }

    public void setNearest_mandi_name(String nearest_mandi_name) {
        this.nearest_mandi_name = nearest_mandi_name;
    }

    public double getNearest_mandi_lat() {
        return nearest_mandi_lat;
    }

    public void setNearest_mandi_lat(double nearest_mandi_lat) {
        this.nearest_mandi_lat = nearest_mandi_lat;
    }

    public double getNearest_mandi_log() {
        return nearest_mandi_log;
    }

    public void setNearest_mandi_log(double nearest_mandi_log) {
        this.nearest_mandi_log = nearest_mandi_log;
    }

    public String getInterested_mandi_list() {
        return interested_mandi_list;
    }

    public void setInterested_mandi_list(String interested_mandi_list) {
        this.interested_mandi_list = interested_mandi_list;
    }

    public String getInterested_crop_list() {
        return interested_crop_list;
    }

    public void setInterested_crop_list(String interested_crop_list) {
        this.interested_crop_list = interested_crop_list;
    }

    public String getInterested_rss_topics() {
        return interested_rss_topics;
    }

    public void setInterested_rss_topics(String interested_rss_topics) {
        this.interested_rss_topics = interested_rss_topics;
    }

    public String getBpl_image() {
        return bpl_image;
    }

    public void setBpl_image(String bpl_image) {
        this.bpl_image = bpl_image;
    }

    public String getBpl_status() {
        return bpl_status;
    }

    public void setBpl_status(String bpl_status) {
        this.bpl_status = bpl_status;
    }

    public String getLogin_status() {
        return login_status;
    }

    public void setLogin_status(String login_status) {
        this.login_status = login_status;
    }
}
