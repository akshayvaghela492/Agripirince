package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.ca.CADetail;
import com.agriiprince.mvvm.retrofit.dto.ca.CAlist;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface CA {
    @GET(" ")
    Call<CADetail> CA_DETAIL(@Header("authentication") String token);

    @FormUrlEncoded
    @POST("get_list_of_comission_agents_with_bilingual_info")
    Call<CAlist> CA_LIST(@Header("authentication") String token,
                         @FieldMap HashMap<String,String> param

                         );
}
