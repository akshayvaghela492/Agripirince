package com.agriiprince.mvvm.retrofit.test;

import android.util.Log;

import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.disease.DiseaseDetailResponse;
import com.agriiprince.mvvm.retrofit.dto.disease.DistinctDisease;
import com.agriiprince.mvvm.retrofit.service.Disease;
import com.agriiprince.mvvm.util.Logs;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiseaseRetro {

    API_Manager api_manager=new API_Manager();
    Disease retrofit_interface = api_manager.getClient6().create(Disease.class);

    public void getdisease() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put("user_id", "VGMG8828");
        user.put("user_type", "farmer");
        user.put("primary_language", "kn");
        user.put("secondary_language", "hi");

        Call<DistinctDisease> call = retrofit_interface.getdistinctdisease("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxNCwibmFtZSI6Im1lZ1xuIiwiZW1haWwiOiJtZWdAZ21haWwuY29tIiwiemlwQ29kZSI6MTIzLCJtb2JpbGUiOjMyMTMyMTMyMTMsInN0YXR1cyI6MSwiY3JlYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidXBkYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidHlwZSI6ImZwbyJ9LCJpYXQiOjE1NjYwMjMwNDgsImV4cCI6MTU2ODYxNTA0OH0.-yaQVXHScLrl-poZsV3alGKC4YLiPUhvoIEhD-h0g6Y",user);
        call.enqueue(new Callback<DistinctDisease>() {
            @Override
            public void onResponse(Call<DistinctDisease> call, Response<DistinctDisease> response) {
                Log.d("CHECK","onResponse: DistinctDisease  "+response);
                try {
                    Logs.d("CHECK",response.body().getStatus());
                    Logs.d("CHECK",response.body().getMessage());
                    Logs.d("CHECK",response.body().getData().get(1).getCrop_name());
                }
                catch (Exception e){e.getMessage();}
            }

            @Override
            public void onFailure(Call<DistinctDisease> call, Throwable t) {
                Log.d("CHECK", "DistinctDisease "+t.getMessage());
            }
        });
    }

    public void getDiseasedetails() {

        Call<DiseaseDetailResponse> call = retrofit_interface.getDiseasedetail("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxNCwibmFtZSI6Im1lZ1xuIiwiZW1haWwiOiJtZWdAZ21haWwuY29tIiwiemlwQ29kZSI6MTIzLCJtb2JpbGUiOjMyMTMyMTMyMTMsInN0YXR1cyI6MSwiY3JlYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidXBkYXRlZEF0IjoiMjAxOS0wNi0xOFQwNDo0MTowMi4wMDBaIiwidHlwZSI6ImZwbyJ9LCJpYXQiOjE1NjYwMjMwNDgsImV4cCI6MTU2ODYxNTA0OH0.-yaQVXHScLrl-poZsV3alGKC4YLiPUhvoIEhD-h0g6Y","Tea","en","hi");
        call.enqueue(new Callback<DiseaseDetailResponse>() {
            @Override
            public void onResponse(Call<DiseaseDetailResponse> call, Response<DiseaseDetailResponse> response) {
                Log.d("CHECK","onResponse: Disease details"+response);
                Log.d("CHECK","onResponse: Disease details"+response.body().getStatus());
                try {
                    if (response.body() != null && response.body().getStatus().equalsIgnoreCase("success"))
                    Logs.d("CHECK",response.body().getStatus());
                    Logs.d("CHECK",response.body().getData().getPrimary().getData().get(0).getCropName());
                    Logs.d("CHECK",response.body().getData().getPrimary().getData().get(0).getCropDiseases());
                }
                catch (Exception e){e.getMessage();}
            }

            @Override
            public void onFailure(Call<DiseaseDetailResponse> call, Throwable t) {
                Log.d("CHECK", "Disease details"+t.getMessage());
            }
        });
    }

}
