package com.agriiprince.mvvm.retrofit.model.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Commodity implements Serializable {
    @SerializedName( "commodity_id")
    @Expose
    private String commodity_id;
    @SerializedName("commodity_name")
    @Expose
    private String commodity_name;
    @SerializedName( "commodity_state")
    @Expose
    private String commodity_state;
    @SerializedName("commodity_market")
    @Expose
    private String commodity_market;
    @SerializedName("commodity_district")
    @Expose
    private String commodity_district;
    @SerializedName("commodity_variety")
    @Expose
    private String commodity_variety;
    @SerializedName("marketLat")
    @Expose
    private double marketLat;
    @SerializedName("marketLng")
    @Expose
    private double marketLng;
    @SerializedName("list_max_price")
    @Expose
    private Integer list_max_price;
    @SerializedName("list_min_price")
    @Expose
    private Integer list_min_price;
    @SerializedName("list_modal_price")
    @Expose
    private Integer list_modal_price;

    public String getCommodity_id() {
        return commodity_id;
    }

    public void setCommodity_id(String commodity_id) {
        this.commodity_id = commodity_id;
    }

    public String getCommodity_name() {
        return commodity_name;
    }

    public void setCommodity_name(String commodity_name) {
        this.commodity_name = commodity_name;
    }

    public String getCommodity_state() {
        return commodity_state;
    }

    public void setCommodity_state(String commodity_state) {
        this.commodity_state = commodity_state;
    }

    public String getCommodity_market() {
        return commodity_market;
    }

    public void setCommodity_market(String commodity_market) {
        this.commodity_market = commodity_market;
    }

    public String getCommodity_district() {
        return commodity_district;
    }

    public void setCommodity_district(String commodity_district) {
        this.commodity_district = commodity_district;
    }

    public String getCommodity_variety() {
        return commodity_variety;
    }

    public void setCommodity_variety(String commodity_variety) {
        this.commodity_variety = commodity_variety;
    }

    public double getMarketLat() {
        return marketLat;
    }

    public void setMarketLat(double marketLat) {
        this.marketLat = marketLat;
    }

    public double getMarketLng() {
        return marketLng;
    }

    public void setMarketLng(double marketLng) {
        this.marketLng = marketLng;
    }

    public Integer getList_max_price() {
        return list_max_price;
    }

    public void setList_max_price(Integer list_max_price) {
        this.list_max_price = list_max_price;
    }

    public Integer getList_min_price() {
        return list_min_price;
    }

    public void setList_min_price(Integer list_min_price) {
        this.list_min_price = list_min_price;
    }

    public Integer getList_modal_price() {
        return list_modal_price;
    }

    public void setList_modal_price(Integer list_modal_price) {
        this.list_modal_price = list_modal_price;
    }
}
