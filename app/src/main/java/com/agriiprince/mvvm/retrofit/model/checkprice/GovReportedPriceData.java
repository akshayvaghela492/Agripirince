package com.agriiprince.mvvm.retrofit.model.checkprice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GovReportedPriceData implements Serializable {
    @SerializedName("arrival")
    @Expose
    private String arrival;
    @SerializedName("district")
    @Expose
    private String district;
    @SerializedName("market")
    @Expose
    private String market;
    @SerializedName("commodity")
    @Expose
    private String commodity;
    @SerializedName("variety")
    @Expose
    private String variety;
    @SerializedName("grade")
    @Expose
    private String grade;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("minP")
    @Expose
    private Integer minP;
    @SerializedName("maxP")
    @Expose
    private Integer maxP;
    @SerializedName("modP")
    @Expose
    private Integer modP;
    @SerializedName("tonnage")
    @Expose
    private Double tonnage;
    @SerializedName("mandi_lat")
    @Expose
    private String mandiLat;
    @SerializedName("mandi_lng")
    @Expose
    private String mandiLng;

    private double distance;
    private String mandi_id;
    private boolean isInterested;


    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getMinP() {
        return minP;
    }

    public void setMinP(Integer minP) {
        this.minP = minP;
    }

    public Integer getMaxP() {
        return maxP;
    }

    public void setMaxP(Integer maxP) {
        this.maxP = maxP;
    }

    public Integer getModP() {
        return modP;
    }

    public void setModP(Integer modP) {
        this.modP = modP;
    }

    public Double getTonnage() {
        return tonnage;
    }

    public void setTonnage(Double tonnage) {
        this.tonnage = tonnage;
    }

    public String getMandiLat() {
        return mandiLat;
    }

    public void setMandiLat(String mandiLat) {
        this.mandiLat = mandiLat;
    }

    public String getMandiLng() {
        return mandiLng;
    }

    public void setMandiLng(String mandiLng) {
        this.mandiLng = mandiLng;
    }


    public double getDistanceInKm() {
        return distance / 1000;
    }

    public void setId(String mandi_id) {
        this.mandi_id = mandi_id;
    }

    public String getId() {
        return mandi_id;
    }

    public void setInterested(boolean isInterested) {
        this.isInterested=isInterested;
    }

    public boolean isInterested() {
        return isInterested;
    }
}
