package com.agriiprince.mvvm.retrofit.model.tutorial;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TutorialModel implements Serializable {

    @SerializedName("tutorial")
    @Expose
    private List<TutorialSections> tutorial = null;


    public List<TutorialSections> getTutorial() {
        return tutorial;
    }

    public void setTutorial(List<TutorialSections> tutorial) {
        this.tutorial = tutorial;
    }

}
