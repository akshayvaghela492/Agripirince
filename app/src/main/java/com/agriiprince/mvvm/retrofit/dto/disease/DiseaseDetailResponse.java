package com.agriiprince.mvvm.retrofit.dto.disease;

import com.agriiprince.mvvm.retrofit.model.disease.DiseaseLanguage;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DiseaseDetailResponse implements Serializable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private DiseaseLanguage data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DiseaseLanguage getData() {
        return data;
    }

    public void setData(DiseaseLanguage data) {
        this.data = data;
    }
}
