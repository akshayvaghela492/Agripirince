package com.agriiprince.mvvm.retrofit.model.croplist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CropByClass implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("crop_id")
    @Expose
    private String crop_id;
    @SerializedName("crop_name")
    @Expose
    private String crop_name;
    @SerializedName("crop_variety")
    @Expose
    private String crop_variety;
    @SerializedName("crop_category")
    @Expose
    private String crop_category;
    @SerializedName("crop_grade")
    @Expose
    private String crop_grade;
    @SerializedName("crop_reference")
    @Expose
    private String crop_reference;
    @SerializedName("pnwhandbook_name")
    @Expose
    private String pnwhandbook_name;
    @SerializedName("plantvillage_name")
    @Expose
    private String plantvillage_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCrop_id() {
        return crop_id;
    }

    public void setCrop_id(String crop_id) {
        this.crop_id = crop_id;
    }

    public String getCrop_name() {
        return crop_name;
    }

    public void setCrop_name(String crop_name) {
        this.crop_name = crop_name;
    }

    public String getCrop_variety() {
        return crop_variety;
    }

    public void setCrop_variety(String crop_variety) {
        this.crop_variety = crop_variety;
    }

    public String getCrop_category() {
        return crop_category;
    }

    public void setCrop_category(String crop_category) {
        this.crop_category = crop_category;
    }

    public String getCrop_grade() {
        return crop_grade;
    }

    public void setCrop_grade(String crop_grade) {
        this.crop_grade = crop_grade;
    }

    public String getCrop_reference() {
        return crop_reference;
    }

    public void setCrop_reference(String crop_reference) {
        this.crop_reference = crop_reference;
    }

    public String getPnwhandbook_name() {
        return pnwhandbook_name;
    }

    public void setPnwhandbook_name(String pnwhandbook_name) {
        this.pnwhandbook_name = pnwhandbook_name;
    }

    public String getPlantvillage_name() {
        return plantvillage_name;
    }

    public void setPlantvillage_name(String plantvillage_name) {
        this.plantvillage_name = plantvillage_name;
    }
}
