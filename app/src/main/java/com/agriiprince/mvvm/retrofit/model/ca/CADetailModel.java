package com.agriiprince.mvvm.retrofit.model.ca;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CADetailModel implements Serializable {
    @SerializedName("ca_id")
    @Expose
    private Integer ca_id;
    @SerializedName("ca_name")
    @Expose
    private String ca_name;
    @SerializedName("ca_business_name")
    @Expose
    private String ca_business_name;
    @SerializedName("ca_address")
    @Expose
    private String ca_address;
    @SerializedName("ca_mandi")
    @Expose
    private String ca_mandi;
    @SerializedName("ca_contact")
    @Expose
    private String ca_contact;

    public Integer getCa_id() {
        return ca_id;
    }

    public void setCa_id(Integer ca_id) {
        this.ca_id = ca_id;
    }

    public String getCa_name() {
        return ca_name;
    }

    public void setCa_name(String ca_name) {
        this.ca_name = ca_name;
    }

    public String getCa_business_name() {
        return ca_business_name;
    }

    public void setCa_business_name(String ca_business_name) {
        this.ca_business_name = ca_business_name;
    }

    public String getCa_address() {
        return ca_address;
    }

    public void setCa_address(String ca_address) {
        this.ca_address = ca_address;
    }

    public String getCa_mandi() {
        return ca_mandi;
    }

    public void setCa_mandi(String ca_mandi) {
        this.ca_mandi = ca_mandi;
    }

    public String getCa_contact() {
        return ca_contact;
    }

    public void setCa_contact(String ca_contact) {
        this.ca_contact = ca_contact;
    }
}
