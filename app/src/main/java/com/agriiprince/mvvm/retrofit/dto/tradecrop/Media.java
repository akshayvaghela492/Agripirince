package com.agriiprince.mvvm.retrofit.dto.tradecrop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Media implements Serializable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("mediaBaseUrl")
    @Expose
    private String mediaBaseUrl;
    @SerializedName("data")
    @Expose
    private List<com.agriiprince.mvvm.retrofit.model.tradecrop.Media> data = null;

    public String getMediaBaseUrl() {
        return mediaBaseUrl;
    }

    public void setMediaBaseUrl(String mediaBaseUrl) {
        this.mediaBaseUrl = mediaBaseUrl;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<com.agriiprince.mvvm.retrofit.model.tradecrop.Media> getData() {
        return data;
    }

    public void setData(List<com.agriiprince.mvvm.retrofit.model.tradecrop.Media> data) {
        this.data = data;
    }

}