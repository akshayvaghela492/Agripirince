package com.agriiprince.mvvm.retrofit.dto.subscription;

import com.agriiprince.mvvm.model.Subscription;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubscriptionList {

    @SerializedName("bpl_status")
    private String bplStatus;

    @SerializedName("bpl_amount")
    private String bplAmount;

    @SerializedName("data")
    private List<Subscription> subscriptions;

    public String getBplStatus() {
        return bplStatus;
    }

    public String getBplAmount() {
        return bplAmount;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }
}
