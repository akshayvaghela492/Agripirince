package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.trackcrop.TrackCrop;
import com.agriiprince.mvvm.retrofit.dto.trackcrop.TrackGetCrop;
import com.agriiprince.mvvm.retrofit.dto.trackcrop.TrackSaveCrop;
import com.agriiprince.mvvm.retrofit.dto.trackcrop.TrackSaveOrder;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface TrackCropService {
    @FormUrlEncoded
    @POST("fig/{id}/create")
    Call<TrackSaveCrop> saveCrop(@Path("id") int user_id,
                                 @Field("userType") String userType,
                                 @Field("userId") String userId,
                                 @Field("cropId") String cropId,
                                 @Field("variety") String variety,
                                 @Field("grade") String grade,
                                 @Field("locationState") String locationState,
                                 @Field("locationDistrict") String locationDistrict,
                                 @Field("locationTaluka") String locationTaluka,
                                 @Field("locationVillage") String locationVillage,
                                 @Field("status") String status,
                                 @Field("sowingDate") String sowingDate,
                                 @Field("lengthOfCultivation") String lengthOfCultivation,
                                 @Field("sowingMethods") String sowingMethods,
                                 @Field("webRemovalMethods") String webRemovalMethods);
    @FormUrlEncoded
    @POST("{id}/order/product/{productid}/fig/{userid}")
    Call<TrackSaveOrder> saveOrder(@Path("id") int id,
                                   @Path("productid") int productid,
                                   @Path("userid") int userid,
                                   @Field("productId") String productId,
                                   @Field("address") String address,
                                   @Field("quantity") String quantity,
                                   @Field("quantityUnit") String quantityUnit,
                                   @Field("productId") String product_Id,
                                   @Field("userId") String userId,
                                   @Field("userType") String userType);


    @GET("user/{id}/crops")
    Call<TrackGetCrop> getCrop(@Path("id") int user_id);

    @GET("{id}/activity")
    Call<TrackCrop> getActivitylist(@Path("id") int id);

    @GET("activity/{id}")
    Call<TrackCrop> getActivityDetails(@Path("id") int activity_id);

    @GET("{id}/products/sprinkler")
    Call<TrackCrop> getSprinklers(@Path("id") int id);

    @GET("{id}/products/pesticide")
    Call<TrackCrop> getPesticides(@Path("id") int id);

    @GET("{id}/products/fertilizer")
    Call<TrackCrop> getFertilizers(@Path("id") int id);

    @GET("{id}/suitability")
    Call<TrackCrop> getSuitability(@Path("id") int id);



}
