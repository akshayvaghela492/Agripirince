package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.dto.checkprice.GovReportPrices;
import com.agriiprince.mvvm.retrofit.dto.checkprice.HistoricalPrices;
import com.agriiprince.mvvm.retrofit.dto.checkprice.OeReportedPrices;
import com.agriiprince.mvvm.retrofit.model.checkprice.GovRequestBody;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface CheckPrice {
    @FormUrlEncoded
    @POST(" ")
    Call<OeReportedPrices> OeReportedPriceData (@Header("authentication") String token,
                                                @Field("user_id") String user_id,
                                                @Field("commodity_name") String commodity_name,
                                                @Field("commodity_variety") String commodity_variety,
                                                @Field("commodity_grade") String commodity_grade,
                                                @Field("arrival_date") String arrival_date
    );

    @FormUrlEncoded
    @POST("get_historic_price_based_on_mandi_and_crop_list")
    Call<HistoricalPrices> HistoricalPriceData (@Header("authentication") String token,
                                                @Field("mandiList") String mandiList,
                                                @Field("date") String date,
                                                @Field("days") String days,
                                                @Field("cropList") String cropList
    );

    @FormUrlEncoded
    @POST("checkprice/get_gov_reported_price.php")
    Call<GovReportPrices> GovReportedPriceData (@Header("Authentication") String token,
                                                @FieldMap  HashMap<String,String> params

                                                //// FOR NODE API
                                                //@Header("Content-Type") String content_type,
                                                //@Body GovRequestBody requestBody
                                                );

}
