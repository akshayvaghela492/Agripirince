package com.agriiprince.mvvm.retrofit.service;

import com.agriiprince.mvvm.retrofit.model.referral.ReferNumber;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Refer {
    @FormUrlEncoded
    @POST()
    Call<ReferNumber> REFER_NUMBER(@Header("authentication") String token,
                                     @Field("user_id") String user_id,
                                     @Field("user_type") String user_type,
                                     @Field("referred_number") String referred_number

    );
}
