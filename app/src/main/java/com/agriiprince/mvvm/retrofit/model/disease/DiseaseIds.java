package com.agriiprince.mvvm.retrofit.model.disease;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DiseaseIds implements Serializable
{

    @SerializedName("crop_name")
    @Expose
    private String cropName;
    @SerializedName("crop_disease_ids")
    @Expose
    private List<Integer> cropDiseaseIds = null;
    @SerializedName("crop_name_bi")
    @Expose
    private String cropNameBi;

    public String getCropName() {
        return cropName;
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public List<Integer> getCropDiseaseIds() {
        return cropDiseaseIds;
    }

    public void setCropDiseaseIds(List<Integer> cropDiseaseIds) {
        this.cropDiseaseIds = cropDiseaseIds;
    }

    public String getCropNameBi() {
        return cropNameBi;
    }

    public void setCropNameBi(String cropNameBi) {
        this.cropNameBi = cropNameBi;
    }

}
