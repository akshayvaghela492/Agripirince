package com.agriiprince.mvvm.retrofit.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginService {


    /* NO LONGER USED
     * check if the mobile used to register is already exist in the database.
     * @param userType user type for registration
     * @param mobileNumber mobile number used for new registration
     * @return {@link CheckUserExist} DTO class
     *
    @FormUrlEncoded
    @POST("check_user_exist.php")
    Call<List<CheckUserExist>> checkUserExists(@Field("user_type") String userType, @Field("mobileno") String mobileNumber);*/

    //FCM_TOKEN_UPDATE.php
    //token/generate_token.php

    //user_registration.php
    //login.php
    //logout.php

    /* NO LONGER USED
     * login user
     * @param mobile mobile number
     * @param password password
     * @param userType user type
     * @param fcmToken fcm token
     * @param loginType login type otp / password
     * @return {@link Login}


    @FormUrlEncoded
    @POST("login.php")
    Call<ResponseBody> login(
            @Field("mobileno") String mobile, @Field("password") String password, @Field("mode") String userType,
            @Field("fcm_token") String fcmToken, @Field("login_type") String loginType
    );


    /* NO LONGER USED
     * generate new access token for logged user
     * @param userId user id
     * @param userType user type
     * @param password password
     * @return {@link TokenGenerate}
     *
    @FormUrlEncoded
    @POST("token/generate_token.php")
    Call<List<TokenGenerate>> generateAccessToken(
            @Field("user_id") String userId, @Field("user_type") String userType, @Field("password") String password
    );*/


    /*
     * upload the fcm token to server
     * @param userId
     * @param userType
     * @param accessToken
     * @param fcmToken
     *
    @FormUrlEncoded
    @POST("FCM_TOKEN_UPDATE.php")
    Call<ResponseBody> updateFcmToken(
            @Field("user_id") String userId, @Field("user_type") String userType,
            @Field("api_token") String accessToken, @Field("token") String fcmToken
    );*/
}
