package com.agriiprince.mvvm.room.entity.trade_crop;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;

@Entity(tableName = "fig_list_details_table")
public class FIGListDetailsEntity
{
    @NonNull
    @PrimaryKey(autoGenerate = false)
    private String fig_id;
    private String fig_name;
    private String fig_mobile;
    private String fig_pin;
    private String fig_yield_per_day;
    private String fig_yield_per_week;
    private String fig_no_of_farmers;
    private String updater_id;
    private Date updated_at;

    private ArrayList<String> farmer_list_csv;


    public FIGListDetailsEntity(@NonNull String fig_id, String fig_name, String fig_mobile, String fig_pin, String fig_yield_per_day, String fig_yield_per_week,
                                String fig_no_of_farmers, String updater_id, Date updated_at, ArrayList<String> farmer_list_csv) {
        this.fig_id = fig_id;
        this.fig_name = fig_name;
        this.fig_mobile = fig_mobile;
        this.fig_pin = fig_pin;
        this.fig_yield_per_day = fig_yield_per_day;
        this.fig_yield_per_week = fig_yield_per_week;
        this.fig_no_of_farmers = fig_no_of_farmers;
        this.updater_id = updater_id;
        this.updated_at = updated_at;
        this.farmer_list_csv = farmer_list_csv;
    }


    @NonNull
    public String getFig_id() {
        return fig_id;
    }

    public void setFig_id(@NonNull String fig_id) {
        this.fig_id = fig_id;
    }

    public String getFig_name() {
        return fig_name;
    }

    public void setFig_name(String fig_name) {
        this.fig_name = fig_name;
    }

    public String getFig_mobile() {
        return fig_mobile;
    }

    public void setFig_mobile(String fig_mobile) {
        this.fig_mobile = fig_mobile;
    }

    public String getFig_pin() {
        return fig_pin;
    }

    public void setFig_pin(String fig_pin) {
        this.fig_pin = fig_pin;
    }

    public String getFig_yield_per_day() {
        return fig_yield_per_day;
    }

    public void setFig_yield_per_day(String fig_yield_per_day) {
        this.fig_yield_per_day = fig_yield_per_day;
    }

    public String getFig_yield_per_week() {
        return fig_yield_per_week;
    }

    public void setFig_yield_per_week(String fig_yield_per_week) {
        this.fig_yield_per_week = fig_yield_per_week;
    }

    public String getFig_no_of_farmers() {
        return fig_no_of_farmers;
    }

    public void setFig_no_of_farmers(String fig_no_of_farmers) {
        this.fig_no_of_farmers = fig_no_of_farmers;
    }

    public String getUpdater_id() {
        return updater_id;
    }

    public void setUpdater_id(String updater_id) {
        this.updater_id = updater_id;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public ArrayList<String> getFarmer_list_csv() {
        return farmer_list_csv;
    }

    public void setFarmer_list_csv(ArrayList<String> farmer_list_csv) {
        this.farmer_list_csv = farmer_list_csv;
    }
}
