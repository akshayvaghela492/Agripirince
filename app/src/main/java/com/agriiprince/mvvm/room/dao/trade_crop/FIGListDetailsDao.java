package com.agriiprince.mvvm.room.dao.trade_crop;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agriiprince.mvvm.room.entity.trade_crop.FIGListDetailsEntity;

import java.util.List;

@Dao
public interface FIGListDetailsDao {

    @Query("SELECT * FROM fig_list_details_table ORDER BY fig_id")
    List<FIGListDetailsEntity> loadAllFigDetails();

    @Insert
    void insertFigDetail(FIGListDetailsEntity figDetail);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateFigDetail(FIGListDetailsEntity figDetail);

    @Delete
    void deleteFigDetail(FIGListDetailsEntity figDetail);

    @Query("DELETE FROM fig_list_details_table")
    void deleteEntireFigDetailsTable();
}
