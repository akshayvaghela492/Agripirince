package com.agriiprince.mvvm.room.dao.trade_crop;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agriiprince.mvvm.room.entity.trade_crop.FarmerListDetailsEntity;

import java.util.List;

@Dao
public interface FarmerListDetailsDao {

    @Query("SELECT * FROM farmer_list_details_table ORDER BY farmer_id")
    List<FarmerListDetailsEntity> loadAllFarmerDetails();

    @Query("SELECT * FROM farmer_list_details_table WHERE fig_id = :id ORDER BY farmer_id")
    List<FarmerListDetailsEntity> loadFarmerDetailsByFigId(int id);

    @Insert
    void insertFarmerDetail(FarmerListDetailsEntity farmerDetail);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateFarmerDetail(FarmerListDetailsEntity farmerDetail);

    @Delete
    void deleteFarmerDetail(FarmerListDetailsEntity farmerDetail);

    @Query("DELETE FROM farmer_list_details_table")
    void deleteEntireFarmerDetailsTable();
}
