package com.agriiprince.mvvm.room.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.util.Log;

import com.agriiprince.mvvm.room.dao.TradeCropDetailDao;
import com.agriiprince.mvvm.room.dao.trade_crop.CropListDetailsDao;
import com.agriiprince.mvvm.room.dao.trade_crop.FIGListDetailsDao;
import com.agriiprince.mvvm.room.dao.trade_crop.FarmerListDetailsDao;
import com.agriiprince.mvvm.room.entity.TradeCropDetailEntity;
import com.agriiprince.mvvm.room.entity.trade_crop.CropListDetailsEntity;
import com.agriiprince.mvvm.room.entity.trade_crop.FIGListDetailsEntity;
import com.agriiprince.mvvm.room.entity.trade_crop.FarmerListDetailsEntity;
import com.agriiprince.mvvm.room.typeconverter.ArrayListDateConverter;
import com.agriiprince.mvvm.room.typeconverter.ArrayListIntegerConverter;
import com.agriiprince.mvvm.room.typeconverter.ArrayListStringConverter;
import com.agriiprince.mvvm.room.typeconverter.DateConverter;

@Database(entities = {TradeCropDetailEntity.class, CropListDetailsEntity.class, FarmerListDetailsEntity.class, FIGListDetailsEntity.class},
        version = 2, exportSchema = false)
@TypeConverters({DateConverter.class, ArrayListStringConverter.class, ArrayListIntegerConverter.class, ArrayListDateConverter.class})
public abstract class AppDatabase extends RoomDatabase {

    private static final String LOG_TAG = AppDatabase.class.getSimpleName();
    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "agriiprincedb";
    private static AppDatabase sInstance;

    public static AppDatabase getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                if (sInstance == null) {
                    Log.d(LOG_TAG, "Creating new database instance");
                    sInstance = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, AppDatabase.DATABASE_NAME)
                            .build();
                }
            }
        }
        Log.d(LOG_TAG, "Getting the database instance");
        return sInstance;
    }

    public abstract TradeCropDetailDao tradeCropDetailDao();

    public abstract CropListDetailsDao cropListDetailsDao();

    public abstract FarmerListDetailsDao farmerListDetailsDao();

    public abstract FIGListDetailsDao figListDetailsDao();
}
