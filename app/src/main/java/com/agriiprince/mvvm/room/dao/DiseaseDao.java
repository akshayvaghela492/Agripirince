package com.agriiprince.mvvm.room.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agriiprince.mvvm.room.entity.DiseaseEntity;

import java.util.List;

@Dao
public interface DiseaseDao {

    @Query("SELECT * FROM disease ORDER BY crop_id")
    LiveData<List<DiseaseEntity>> loadAllDiseases();

    @Insert
    void insertDisease(DiseaseEntity Disease);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateDisease(DiseaseEntity Disease);

    @Delete
    void deleteDisease(DiseaseEntity Disease);

    @Query("SELECT * FROM disease WHERE crop_id = :id")
    LiveData<DiseaseEntity> loadDiseaseByDiseaseId(int id);

}
