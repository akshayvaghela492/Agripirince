package com.agriiprince.mvvm.room.entity.trade_crop;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;

@Entity(tableName = "farmer_list_details_table")
public class FarmerListDetailsEntity
{
    @NonNull
    @PrimaryKey(autoGenerate = false)
    private String farmer_id;
    private String farmer_name;
    private String farmer_mobile;
    private String farmer_pin;
    private String farmer_area;
    private String farmer_area_unit;
    private String fig_id;
    private String updater_id;
    private Date updated_at;

    private ArrayList<String> crop_list_csv;


    public FarmerListDetailsEntity(@NonNull String farmer_id, String farmer_name, String farmer_mobile, String farmer_pin,
                                   String farmer_area, String farmer_area_unit, String fig_id, String updater_id, Date updated_at, ArrayList<String> crop_list_csv) {
        this.farmer_id = farmer_id;
        this.farmer_name = farmer_name;
        this.farmer_mobile = farmer_mobile;
        this.farmer_pin = farmer_pin;
        this.farmer_area = farmer_area;
        this.farmer_area_unit = farmer_area_unit;
        this.fig_id = fig_id;
        this.updater_id = updater_id;
        this.updated_at = updated_at;
        this.crop_list_csv = crop_list_csv;
    }


    @NonNull
    public String getFarmer_id() {
        return farmer_id;
    }

    public void setFarmer_id(@NonNull String farmer_id) {
        this.farmer_id = farmer_id;
    }

    public String getFarmer_name() {
        return farmer_name;
    }

    public void setFarmer_name(String farmer_name) {
        this.farmer_name = farmer_name;
    }

    public String getFarmer_mobile() {
        return farmer_mobile;
    }

    public void setFarmer_mobile(String farmer_mobile) {
        this.farmer_mobile = farmer_mobile;
    }

    public String getFarmer_pin() {
        return farmer_pin;
    }

    public void setFarmer_pin(String farmer_pin) {
        this.farmer_pin = farmer_pin;
    }

    public String getFarmer_area() {
        return farmer_area;
    }

    public void setFarmer_area(String farmer_area) {
        this.farmer_area = farmer_area;
    }

    public String getFarmer_area_unit() {
        return farmer_area_unit;
    }

    public void setFarmer_area_unit(String farmer_area_unit) {
        this.farmer_area_unit = farmer_area_unit;
    }

    public String getFig_id() {
        return fig_id;
    }

    public void setFig_id(String owner_id) {
        this.fig_id = owner_id;
    }

    public String getUpdater_id() {
        return updater_id;
    }

    public void setUpdater_id(String updater_id) {
        this.updater_id = updater_id;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public ArrayList<String> getCrop_list_csv() {
        return crop_list_csv;
    }

    public void setCrop_list_csv(ArrayList<String> crop_list_csv) {
        this.crop_list_csv = crop_list_csv;
    }
}
