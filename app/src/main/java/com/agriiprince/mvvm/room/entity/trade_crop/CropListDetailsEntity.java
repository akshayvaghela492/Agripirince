package com.agriiprince.mvvm.room.entity.trade_crop;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;

@Entity(tableName = "crop_list_details_table")
public class CropListDetailsEntity
{
    @NonNull
    @PrimaryKey(autoGenerate = false)
    private String crop_id;
    private String farmer_id;
    private String updater_id;
    private Date updated_at;

    private String crop;
    private String crop_variety;
    private String crop_grade;
    private Date sowing_date;
    private Integer area_under_cultivation;
    private String area_unit;

    private String seed_type;
    private String seed_type_name;

    private Integer grade1;
    private Integer grade2;
    private Integer grade3;
    private Date harvesting_date;
    private Integer quantity;
    private String quantity_unit;
    private Integer frequency;
    private Integer number_of_times;
    private Integer per;
    private String per_unit;
    private Integer harvesting_period;
    private String harvesting_period_unit;

    private ArrayList<String> image_urls_csv;
    private ArrayList<String> video_urls_csv;


    public CropListDetailsEntity(@NonNull String crop_id, String farmer_id, String updater_id, Date updated_at, String crop,
                                 String crop_variety, String crop_grade, Date sowing_date, Integer area_under_cultivation,
                                 String area_unit, String seed_type, String seed_type_name, Integer grade1, Integer grade2,
                                 Integer grade3, Date harvesting_date, Integer quantity, String quantity_unit, Integer frequency,
                                 Integer number_of_times, Integer per, String per_unit, Integer harvesting_period,
                                 String harvesting_period_unit, ArrayList<String> image_urls_csv, ArrayList<String> video_urls_csv) {
        this.crop_id = crop_id;
        this.farmer_id = farmer_id;
        this.updater_id = updater_id;
        this.updated_at = updated_at;
        this.crop = crop;
        this.crop_variety = crop_variety;
        this.crop_grade = crop_grade;
        this.sowing_date = sowing_date;
        this.area_under_cultivation = area_under_cultivation;
        this.area_unit = area_unit;
        this.seed_type = seed_type;
        this.seed_type_name = seed_type_name;
        this.grade1 = grade1;
        this.grade2 = grade2;
        this.grade3 = grade3;
        this.harvesting_date = harvesting_date;
        this.quantity = quantity;
        this.quantity_unit = quantity_unit;
        this.frequency = frequency;
        this.number_of_times = number_of_times;
        this.per = per;
        this.per_unit = per_unit;
        this.harvesting_period = harvesting_period;
        this.harvesting_period_unit = harvesting_period_unit;
        this.image_urls_csv = image_urls_csv;
        this.video_urls_csv = video_urls_csv;
    }


    @NonNull
    public String getCrop_id() {
        return crop_id;
    }

    public void setCrop_id(@NonNull String crop_id) {
        this.crop_id = crop_id;
    }

    public String getFarmer_id() {
        return farmer_id;
    }

    public void setFarmer_id(String farmer_id) {
        this.farmer_id = farmer_id;
    }

    public String getUpdater_id() {
        return updater_id;
    }

    public void setUpdater_id(String updater_id) {
        this.updater_id = updater_id;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public String getCrop() {
        return crop;
    }

    public void setCrop(String crop) {
        this.crop = crop;
    }

    public String getCrop_variety() {
        return crop_variety;
    }

    public void setCrop_variety(String crop_variety) {
        this.crop_variety = crop_variety;
    }

    public String getCrop_grade() {
        return crop_grade;
    }

    public void setCrop_grade(String crop_grade) {
        this.crop_grade = crop_grade;
    }

    public Date getSowing_date() {
        return sowing_date;
    }

    public void setSowing_date(Date sowing_date) {
        this.sowing_date = sowing_date;
    }

    public Integer getArea_under_cultivation() {
        return area_under_cultivation;
    }

    public void setArea_under_cultivation(Integer area_under_cultivation) {
        this.area_under_cultivation = area_under_cultivation;
    }

    public String getArea_unit() {
        return area_unit;
    }

    public void setArea_unit(String area_unit) {
        this.area_unit = area_unit;
    }

    public String getSeed_type() {
        return seed_type;
    }

    public void setSeed_type(String seed_type) {
        this.seed_type = seed_type;
    }

    public String getSeed_type_name() {
        return seed_type_name;
    }

    public void setSeed_type_name(String seed_type_name) {
        this.seed_type_name = seed_type_name;
    }

    public Integer getGrade1() {
        return grade1;
    }

    public void setGrade1(Integer grade1) {
        this.grade1 = grade1;
    }

    public Integer getGrade2() {
        return grade2;
    }

    public void setGrade2(Integer grade2) {
        this.grade2 = grade2;
    }

    public Integer getGrade3() {
        return grade3;
    }

    public void setGrade3(Integer grade3) {
        this.grade3 = grade3;
    }

    public Date getHarvesting_date() {
        return harvesting_date;
    }

    public void setHarvesting_date(Date harvesting_date) {
        this.harvesting_date = harvesting_date;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getQuantity_unit() {
        return quantity_unit;
    }

    public void setQuantity_unit(String quantity_unit) {
        this.quantity_unit = quantity_unit;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public Integer getNumber_of_times() {
        return number_of_times;
    }

    public void setNumber_of_times(Integer number_of_times) {
        this.number_of_times = number_of_times;
    }

    public Integer getPer() {
        return per;
    }

    public void setPer(Integer per) {
        this.per = per;
    }

    public String getPer_unit() {
        return per_unit;
    }

    public void setPer_unit(String per_unit) {
        this.per_unit = per_unit;
    }

    public Integer getHarvesting_period() {
        return harvesting_period;
    }

    public void setHarvesting_period(Integer harvesting_period) {
        this.harvesting_period = harvesting_period;
    }

    public String getHarvesting_period_unit() {
        return harvesting_period_unit;
    }

    public void setHarvesting_period_unit(String harvesting_period_unit) {
        this.harvesting_period_unit = harvesting_period_unit;
    }

    public ArrayList<String> getImage_urls_csv() {
        return image_urls_csv;
    }

    public void setImage_urls_csv(ArrayList<String> image_urls_csv) {
        this.image_urls_csv = image_urls_csv;
    }

    public ArrayList<String> getVideo_urls_csv() {
        return video_urls_csv;
    }

    public void setVideo_urls_csv(ArrayList<String> video_urls_csv) {
        this.video_urls_csv = video_urls_csv;
    }
}
