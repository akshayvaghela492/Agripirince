package com.agriiprince.mvvm.room.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "historicprices")
public class InfoTabPricesEntity {

    @PrimaryKey(autoGenerate = false)
    private String pid;

    private String arrival;
    private String market;
    private String commodity;
    private String variety;
    private String grade;

    private String minP;
    private String maxP;
    private String modP;

    public InfoTabPricesEntity(String pid, String arrival, String market, String commodity, String variety, String grade, String minP, String maxP, String modP) {
        this.pid = pid;
        this.arrival = arrival;
        this.market = market;
        this.commodity = commodity;
        this.variety = variety;
        this.grade = grade;
        this.minP = minP;
        this.maxP = maxP;
        this.modP = modP;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getMinP() {
        return minP;
    }

    public void setMinP(String minP) {
        this.minP = minP;
    }

    public String getMaxP() {
        return maxP;
    }

    public void setMaxP(String maxP) {
        this.maxP = maxP;
    }

    public String getModP() {
        return modP;
    }

    public void setModP(String modP) {
        this.modP = modP;
    }
}
