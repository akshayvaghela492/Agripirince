package com.agriiprince.mvvm.room.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agriiprince.mvvm.room.entity.TradeCropDetailEntity;

import java.util.List;

@Dao
public interface TradeCropDetailDao {

    @Query("SELECT * FROM tradecropdetail ORDER BY crop_id")
    List<TradeCropDetailEntity> loadAllCropDetails();

    @Query("SELECT * FROM tradecropdetail ORDER BY crop_id")
    LiveData<List<TradeCropDetailEntity>> loadAllCropDetailsLive();

    @Insert
    void insertCropDetail(TradeCropDetailEntity cropDetail);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateCropDetail(TradeCropDetailEntity cropDetail);

    @Delete
    void deleteCropDetail(TradeCropDetailEntity cropDetail);

    @Query("DELETE FROM tradecropdetail")
    void nukeTable();

    @Query("SELECT * FROM tradecropdetail WHERE crop_id = :id")
    LiveData<TradeCropDetailEntity> loadCropDetailByCropId(int id);

}
