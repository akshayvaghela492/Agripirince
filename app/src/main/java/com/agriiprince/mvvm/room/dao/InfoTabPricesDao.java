package com.agriiprince.mvvm.room.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agriiprince.mvvm.room.entity.InfoTabPricesEntity;

import java.util.List;

@Dao
public interface InfoTabPricesDao {

    @Query("SELECT * FROM historicprices ORDER BY pid")
    LiveData<List<InfoTabPricesEntity>> loadAllInfoTabPrices();

    @Insert
    void insertInfoTabPrices(InfoTabPricesEntity Prices);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateInfoTabPrices(InfoTabPricesEntity Prices);

    @Delete
    void deleteInfoTabPrices(InfoTabPricesEntity Prices);

    @Query("SELECT * FROM historicprices WHERE pid = :id")
    LiveData<InfoTabPricesEntity> loadInfoTabPricesById(int id);

}

