package com.agriiprince.mvvm.room.typeconverter;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ArrayListIntegerConverter {

        @TypeConverter
        public static ArrayList<Integer> fromString(String value) {
            Type listType = new TypeToken<ArrayList<String>>() {}.getType();
            return new Gson().fromJson(value, listType);
        }

        @TypeConverter
        public static String fromArrayList(ArrayList<Integer> list) {
            Gson gson = new Gson();
            String json = gson.toJson(list);
            return json;
        }
}
