package com.agriiprince.mvvm.room.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.ArrayList;

@Entity(tableName = "disease")
public class DiseaseEntity {

    @PrimaryKey(autoGenerate = false)
    private String disease_id;

    private String crop_id;
    private String crop_name;
    private String disease_name;
    private String scientific_name;
    private String category;
    private String cause;
    private String management;
    private String symptoms;
    private String comments;

    private String crop_images;

    private ArrayList<String> image_urls_csv;

    private String language;

    public DiseaseEntity(String disease_id, String crop_id, String crop_name, String disease_name, String scientific_name, String category, String cause, String management, String symptoms, String comments, String crop_images, ArrayList<String> image_urls_csv, String language) {
        this.disease_id = disease_id;
        this.crop_id = crop_id;
        this.crop_name = crop_name;
        this.disease_name = disease_name;
        this.scientific_name = scientific_name;
        this.category = category;
        this.cause = cause;
        this.management = management;
        this.symptoms = symptoms;
        this.comments = comments;
        this.crop_images = crop_images;
        this.image_urls_csv = image_urls_csv;
        this.language = language;
    }

    public String getDisease_id() {
        return disease_id;
    }

    public void setDisease_id(String disease_id) {
        this.disease_id = disease_id;
    }

    public String getCrop_id() {
        return crop_id;
    }

    public void setCrop_id(String crop_id) {
        this.crop_id = crop_id;
    }

    public String getCrop_name() {
        return crop_name;
    }

    public void setCrop_name(String crop_name) {
        this.crop_name = crop_name;
    }

    public String getDisease_name() {
        return disease_name;
    }

    public void setDisease_name(String disease_name) {
        this.disease_name = disease_name;
    }

    public String getScientific_name() {
        return scientific_name;
    }

    public void setScientific_name(String scientific_name) {
        this.scientific_name = scientific_name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getManagement() {
        return management;
    }

    public void setManagement(String management) {
        this.management = management;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCrop_images() {
        return crop_images;
    }

    public void setCrop_images(String crop_images) {
        this.crop_images = crop_images;
    }

    public ArrayList<String> getImage_urls_csv() {
        return image_urls_csv;
    }

    public void setImage_urls_csv(ArrayList<String> image_urls_csv) {
        this.image_urls_csv = image_urls_csv;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
