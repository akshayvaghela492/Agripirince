package com.agriiprince.mvvm.room.dao.trade_crop;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agriiprince.mvvm.room.entity.trade_crop.CropListDetailsEntity;

import java.util.List;

@Dao
public interface CropListDetailsDao {

    @Query("SELECT * FROM crop_list_details_table ORDER BY crop_id")
    List<CropListDetailsEntity> loadAllCropDetails();

    @Query("SELECT * FROM crop_list_details_table WHERE farmer_id = :id ORDER BY crop_id")
    List<CropListDetailsEntity> loadCropDetailsByFarmerId(int id);

    @Insert
    void insertCropDetail(CropListDetailsEntity cropDetail);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateCropDetail(CropListDetailsEntity cropDetail);

    @Delete
    void deleteCropDetail(CropListDetailsEntity cropDetail);

    @Query("DELETE FROM crop_list_details_table")
    void deleteEntireCropDetailsTable();
}
