package com.agriiprince.mvvm.room.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;

@Entity(tableName = "tradecropdetail")
public class TradeCropDetailEntity
{
    @NonNull
    @PrimaryKey(autoGenerate = false)
    private String farmer_id;
    private String updater_id;
    private Date updated_at;

    private ArrayList<String> crop_id;
    private ArrayList<String> crop_grade;
    private ArrayList<Date> sowing_date;
    private ArrayList<Integer> area_under_cultivation;
    private ArrayList<String> area_unit;

    private ArrayList<String> seed_type;
    private ArrayList<String> seed_type_name;

    private ArrayList<Integer> grade1;
    private ArrayList<Integer> grade2;
    private ArrayList<Integer> grade3;
    private ArrayList<Date> harvesting_date;
    private ArrayList<Integer> quantity;
    private ArrayList<String> quantity_unit;
    private ArrayList<Integer> frequency;
    private ArrayList<Integer> number_of_times;
    private ArrayList<Integer> per;
    private ArrayList<String> per_unit;
    private ArrayList<Integer> harvesting_period;
    private ArrayList<String> harvesting_period_unit;

    private ArrayList<String> image_urls_csv;
    private ArrayList<String> video_urls_csv;

    public TradeCropDetailEntity(String farmer_id, String updater_id, Date updated_at, ArrayList<String> crop_id, ArrayList<String> crop_grade, ArrayList<Date> sowing_date, ArrayList<Integer> area_under_cultivation, ArrayList<String> area_unit, ArrayList<String> seed_type, ArrayList<String> seed_type_name, ArrayList<Integer> grade1, ArrayList<Integer> grade2, ArrayList<Integer> grade3, ArrayList<Date> harvesting_date, ArrayList<Integer> quantity, ArrayList<String> quantity_unit, ArrayList<Integer> frequency, ArrayList<Integer> number_of_times, ArrayList<Integer> per, ArrayList<String> per_unit, ArrayList<Integer> harvesting_period, ArrayList<String> harvesting_period_unit, ArrayList<String> image_urls_csv, ArrayList<String> video_urls_csv) {
        this.farmer_id = farmer_id;
        this.updater_id = updater_id;
        this.updated_at = updated_at;
        this.crop_id = crop_id;
        this.crop_grade = crop_grade;
        this.sowing_date = sowing_date;
        this.area_under_cultivation = area_under_cultivation;
        this.area_unit = area_unit;
        this.seed_type = seed_type;
        this.seed_type_name = seed_type_name;
        this.grade1 = grade1;
        this.grade2 = grade2;
        this.grade3 = grade3;
        this.harvesting_date = harvesting_date;
        this.quantity = quantity;
        this.quantity_unit = quantity_unit;
        this.frequency = frequency;
        this.number_of_times = number_of_times;
        this.per = per;
        this.per_unit = per_unit;
        this.harvesting_period = harvesting_period;
        this.harvesting_period_unit = harvesting_period_unit;
        this.image_urls_csv = image_urls_csv;
        this.video_urls_csv = video_urls_csv;
    }

    public String getFarmer_id() {
        return farmer_id;
    }

    public void setFarmer_id(String farmer_id) {
        this.farmer_id = farmer_id;
    }

    public String getUpdater_id() {
        return updater_id;
    }

    public void setUpdater_id(String updater_id) {
        this.updater_id = updater_id;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public ArrayList<String> getCrop_id() {
        return crop_id;
    }

    public void setCrop_id(ArrayList<String> crop_id) {
        this.crop_id = crop_id;
    }

    public ArrayList<String> getCrop_grade() {
        return crop_grade;
    }

    public void setCrop_grade(ArrayList<String> crop_grade) {
        this.crop_grade = crop_grade;
    }

    public ArrayList<Date> getSowing_date() {
        return sowing_date;
    }

    public void setSowing_date(ArrayList<Date> sowing_date) {
        this.sowing_date = sowing_date;
    }

    public ArrayList<Integer> getArea_under_cultivation() {
        return area_under_cultivation;
    }

    public void setArea_under_cultivation(ArrayList<Integer> area_under_cultivation) {
        this.area_under_cultivation = area_under_cultivation;
    }

    public ArrayList<String> getArea_unit() {
        return area_unit;
    }

    public void setArea_unit(ArrayList<String> area_unit) {
        this.area_unit = area_unit;
    }

    public ArrayList<String> getSeed_type() {
        return seed_type;
    }

    public void setSeed_type(ArrayList<String> seed_type) {
        this.seed_type = seed_type;
    }

    public ArrayList<String> getSeed_type_name() {
        return seed_type_name;
    }

    public void setSeed_type_name(ArrayList<String> seed_type_name) {
        this.seed_type_name = seed_type_name;
    }

    public ArrayList<Integer> getGrade1() {
        return grade1;
    }

    public void setGrade1(ArrayList<Integer> grade1) {
        this.grade1 = grade1;
    }

    public ArrayList<Integer> getGrade2() {
        return grade2;
    }

    public void setGrade2(ArrayList<Integer> grade2) {
        this.grade2 = grade2;
    }

    public ArrayList<Integer> getGrade3() {
        return grade3;
    }

    public void setGrade3(ArrayList<Integer> grade3) {
        this.grade3 = grade3;
    }

    public ArrayList<Date> getHarvesting_date() {
        return harvesting_date;
    }

    public void setHarvesting_date(ArrayList<Date> harvesting_date) {
        this.harvesting_date = harvesting_date;
    }

    public ArrayList<Integer> getQuantity() {
        return quantity;
    }

    public void setQuantity(ArrayList<Integer> quantity) {
        this.quantity = quantity;
    }

    public ArrayList<String> getQuantity_unit() {
        return quantity_unit;
    }

    public void setQuantity_unit(ArrayList<String> quantity_unit) {
        this.quantity_unit = quantity_unit;
    }

    public ArrayList<Integer> getFrequency() {
        return frequency;
    }

    public void setFrequency(ArrayList<Integer> frequency) {
        this.frequency = frequency;
    }

    public ArrayList<Integer> getNumber_of_times() {
        return number_of_times;
    }

    public void setNumber_of_times(ArrayList<Integer> number_of_times) {
        this.number_of_times = number_of_times;
    }

    public ArrayList<Integer> getPer() {
        return per;
    }

    public void setPer(ArrayList<Integer> per) {
        this.per = per;
    }

    public ArrayList<String> getPer_unit() {
        return per_unit;
    }

    public void setPer_unit(ArrayList<String> per_unit) {
        this.per_unit = per_unit;
    }

    public ArrayList<Integer> getHarvesting_period() {
        return harvesting_period;
    }

    public void setHarvesting_period(ArrayList<Integer> harvesting_period) {
        this.harvesting_period = harvesting_period;
    }

    public ArrayList<String> getHarvesting_period_unit() {
        return harvesting_period_unit;
    }

    public void setHarvesting_period_unit(ArrayList<String> harvesting_period_unit) {
        this.harvesting_period_unit = harvesting_period_unit;
    }

    public ArrayList<String> getImage_urls_csv() {
        return image_urls_csv;
    }

    public void setImage_urls_csv(ArrayList<String> image_urls_csv) {
        this.image_urls_csv = image_urls_csv;
    }

    public ArrayList<String> getVideo_urls_csv() {
        return video_urls_csv;
    }

    public void setVideo_urls_csv(ArrayList<String> video_urls_csv) {
        this.video_urls_csv = video_urls_csv;
    }
}
