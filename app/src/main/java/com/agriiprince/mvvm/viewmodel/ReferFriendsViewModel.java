package com.agriiprince.mvvm.viewmodel;

import android.arch.lifecycle.LiveData;

import com.agriiprince.mvvm.model.Contact;
import com.agriiprince.mvvm.data.repository.ReferFriendsRepositoryUseCase;
import com.agriiprince.mvvm.ui.base.BaseViewModel;

import java.util.List;

import javax.inject.Inject;

public class ReferFriendsViewModel extends BaseViewModel implements ReferFriendsViewModelUseCase {

    private ReferFriendsRepositoryUseCase mReferFriendsRepository;

    @Inject
    public ReferFriendsViewModel(ReferFriendsRepositoryUseCase referFriendsRepository) {
        this.mReferFriendsRepository = referFriendsRepository;
    }

    @Override
    public LiveData<List<Contact>> getContacts() {
        return mReferFriendsRepository.getContacts();
    }


    @Override
    public void selectAllContacts() {
        mReferFriendsRepository.selectAllContacts();
    }

    @Override
    public void unSelectAllContacts() {
        mReferFriendsRepository.unSelectAllContacts();
    }
}
