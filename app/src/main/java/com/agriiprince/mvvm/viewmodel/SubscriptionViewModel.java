package com.agriiprince.mvvm.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.util.Log;

import com.agriiprince.mvvm.retrofit.dto.subscription.BplPhotoUpload;
import com.agriiprince.mvvm.retrofit.dto.subscription.BplUploadPhoto;
import com.agriiprince.mvvm.retrofit.dto.subscription.CouponVerification;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionDetails;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionInfo;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionUser;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionCoupon;
import com.agriiprince.mvvm.model.Subscription;
import com.agriiprince.mvvm.data.repository.SubscriptionRepositoryUseCase;
import com.agriiprince.mvvm.retrofit.ResponseWrapper;
import com.agriiprince.mvvm.retrofit.model.subscription.SubscriptionDetailList;
import com.agriiprince.mvvm.retrofit.model.subscription.SubscriptionInformation;
import com.agriiprince.mvvm.ui.base.BaseViewModel;
import com.agriiprince.mvvm.ui.subscription.SubscriptionViewState;
import com.agriiprince.utils.DateUtil;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

public class SubscriptionViewModel extends BaseViewModel implements SubscriptionViewModelUseCase {

    private static final String TAG = "SubscriptionViewModel";

    private MediatorLiveData<SubscriptionViewState> mMediatorViewState;

    private SubscriptionViewState mViewState;

    private SubscriptionRepositoryUseCase mSubscriptionRepository;

    @Inject
    public SubscriptionViewModel(SubscriptionRepositoryUseCase subscriptionRepositoryUseCase) {
        this.mSubscriptionRepository = subscriptionRepositoryUseCase;
        this.mMediatorViewState = new MediatorLiveData<>();
        this.mViewState = new SubscriptionViewState();
    }

    @Override
    public LiveData<SubscriptionViewState> getViewState() {
        mMediatorViewState.setValue(mViewState);
        return mMediatorViewState;
    }

    @Override
    public LiveData<ResponseWrapper<SubscriptionInfo>> getUserSubscription() {
        LiveData<ResponseWrapper<SubscriptionInfo>> source = mSubscriptionRepository.getUserSubscription();
        mMediatorViewState.addSource(source, subscriptionUserMediatorObserver);
        return source;
    }

    private Observer<ResponseWrapper<SubscriptionInfo>> subscriptionUserMediatorObserver = new Observer<ResponseWrapper<SubscriptionInfo>>() {
        @Override
        public void onChanged(@Nullable ResponseWrapper<SubscriptionInfo> subscriptionUserResponseWrapper) {
            Log.d(TAG, "onChanged: viewState mediator");
            if (subscriptionUserResponseWrapper != null && subscriptionUserResponseWrapper.getData() != null) {
                SubscriptionInfo subscriptionInfo = subscriptionUserResponseWrapper.getData();
                Log.d("TESTING_API", "view model onchange ");
                Log.d("TESTING_API","subscriptionUserResponseWrapper"+subscriptionUserResponseWrapper.getStatusCode());
                Log.d("TESTING_API","subscriptionUserResponseWrapper"+subscriptionUserResponseWrapper.getData());
                int code=subscriptionInfo.getCode();
                Log.d("TESTING_API","subscriptionInfo code "+code);
                Log.d("TESTING_API","subscriptionInfo"+subscriptionInfo.getData().getData().get(0).getSubscription_start_date());

                //if (subscriptionUser.error_code == 100) {SubscriptionUser.SubscriptionData data = subscriptionUser.data.get(0);

                    if (subscriptionInfo.getCode() == 200)
                {
                    Log.d("TESTING_API", "viewmodel code=200 ");
                    SubscriptionInformation data = subscriptionInfo.getData().getData().get(0);

                    Calendar calendar = Calendar.getInstance();
                    long currentTime = calendar.getTimeInMillis();

                    calendar.setTimeInMillis(DateUtil.getTimeForServerDate(data.getSubscription_start_date()));
                    long activationTime = calendar.getTimeInMillis();
                    Log.d("TESTING_API", "getSubscription_start_date"+activationTime);

                    Log.d("SUBS_API_TEST ->>",data.getSubscription_expiry_date());
                    calendar.setTimeInMillis(DateUtil.getTimeForServerDate(data.getSubscription_expiry_date()));
                    long expiryTime = calendar.getTimeInMillis();
                    Log.d("TESTING_API", "getSubscription_expiry_date"+expiryTime);
                    mViewState.setActivationDate(DateUtil.ConvertMilliSecondsToFormattedDate(
                            DateUtil.Fomat.DD_MMM_YYYY, activationTime));

                    mViewState.setExpiryDate(DateUtil.ConvertMilliSecondsToFormattedDate(
                            DateUtil.Fomat.DD_MMM_YYYY, expiryTime));

                    int validityDays = (int) ((expiryTime - currentTime) / DateUtils.DAY_IN_MILLIS);
                    validityDays = validityDays + 1; // converting to int will reduce the day by one
                    mViewState.setValidityDays(validityDays);

                    int maxValidityDays = (int) ((expiryTime - activationTime) / DateUtils.DAY_IN_MILLIS);
                    maxValidityDays = maxValidityDays + 1; // converting to int will reduce the day by one
                    mViewState.setMaxValidityDays(maxValidityDays);

                    mViewState.setSubscriptionType(Subscription.BASIC);

                } else {
                    mViewState.setDefault();
                }

                mMediatorViewState.setValue(mViewState);
            }
        }
    };

    @Override
    public LiveData<ResponseWrapper<List<SubscriptionDetailList>>> getSubscriptions() {
        return mSubscriptionRepository.getSubscriptions();
    }

    @Override
    public void activateSubscription(int subscriptionType) {

    }

    @Override
    public LiveData<ResponseWrapper<CouponVerification>> applyCoupon(@NonNull String couponCode) {
        return mSubscriptionRepository.applyCoupon(couponCode);
    }

    @Override
    public LiveData<ResponseWrapper<BplPhotoUpload>> uploadBplPhoto(@NonNull Bitmap bitmap) {
        return mSubscriptionRepository.uploadBplPhoto(bitmap);
    }
}
