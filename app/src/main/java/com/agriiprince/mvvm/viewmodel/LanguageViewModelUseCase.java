package com.agriiprince.mvvm.viewmodel;

import android.arch.lifecycle.LiveData;

import com.agriiprince.mvvm.model.Language;

import java.util.List;

public interface LanguageViewModelUseCase {


    /**
     * returns the list of language models
     * @return {@link Language}
     */
    LiveData<List<Language>> getLanguages();


    /**
     * save the primary locale in shared preferences and update the server
     * @param primaryLocale locale code
     */
    void updatePrimaryLocale(String primaryLocale);


    /**
     * save the secondary locale and update the server
     * @param secondaryLocale locale code
     */
    void updateSecondaryLocale(String secondaryLocale);
}
