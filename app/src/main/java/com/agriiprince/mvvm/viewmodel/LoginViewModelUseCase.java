package com.agriiprince.mvvm.viewmodel;

import android.arch.lifecycle.LiveData;

import com.agriiprince.mvvm.retrofit.dto.login.CheckUserExist;
import com.agriiprince.mvvm.retrofit.dto.login.Login;
import com.agriiprince.mvvm.retrofit.dto.login.TokenGenerate;
import com.agriiprince.mvvm.retrofit.dto.login.UserExist;
import com.agriiprince.mvvm.retrofit.ResponseWrapper;
import com.agriiprince.mvvm.retrofit.dto.oe.OeLoginResponse;

public interface LoginViewModelUseCase {

    /**
     * calling this method will set the onSwitch login fragment to 0
     */
    void onClickLoginFragment();

    /**
     * calling this method will set the onSwitch sing up fragment to 1
     */
    void onClickSingUpFragment();

    /**
     * A single event is notified to observer to switch between login and sign up fragment
     * @return integer 0 for login and 1 for sing up
     */
    LiveData<Integer> onSwitchFragment();


    /**
     * updates the user type for login
     */
    void onChangeLoginUserType(String userType);

    /**
     * returns the user type selected on change
     * @return string user type
     */
    LiveData<String> onChangeUserType();

    /**
     * mobile number entered by the user is returns
     * @return mobile number
     */
    LiveData<String> onChangeMobile();

    /**
     * password entered by the user
     * @return password
     */
    LiveData<String> onChangePassword();

    /**
     * returns check user exists dto for new user
     * @return {@link CheckUserExist} mutable live of check user exist wrapped in {@link ResponseWrapper}
     */
    LiveData<ResponseWrapper<UserExist>> checkUserExists();

    /**
     * call this method to verify the mobile number
     */
    void verifyMobile();

    /**
     * this method will return mobile number to be verified by OTP
     * @return mobile number
     */
    LiveData<String> onVerifyMobile();

    /**
     * makes the network call to login the user
     * @return login response
     */
    LiveData<ResponseWrapper<Login>> onLoginUser();

    LiveData<ResponseWrapper<OeLoginResponse>> onLoginUserOe();


    /**
     * performs login by otp
     */
    void loginByOtp();

    /**
     * try to login the user by password method
     */
    void loginByPassword();

    /**
     * new access token is generated
     * @return {@link TokenGenerate}
     */
    LiveData<ResponseWrapper<TokenGenerate>> generateToken();

}
