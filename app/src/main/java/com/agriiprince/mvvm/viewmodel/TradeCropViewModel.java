package com.agriiprince.mvvm.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.util.Log;

import com.agriiprince.mvvm.room.database.AppDatabase;
import com.agriiprince.mvvm.room.entity.TradeCropDetailEntity;

import java.util.List;

public class TradeCropViewModel extends AndroidViewModel {

        // Constant for logging
        private static final String TAG = TradeCropDetailEntity.class.getSimpleName();

        private LiveData<List<TradeCropDetailEntity>> tradeCrops;

        public TradeCropViewModel(Application application) {
            super(application);
            AppDatabase database = AppDatabase.getInstance(this.getApplication());
            Log.d(TAG, "Actively retrieving the tasks from the DataBase");
            tradeCrops = database.tradeCropDetailDao().loadAllCropDetailsLive();
        }

        public LiveData<List<TradeCropDetailEntity>> getCropDetails() {
            return tradeCrops;
        }

}


