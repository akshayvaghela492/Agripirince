package com.agriiprince.mvvm.viewmodel;

import android.arch.lifecycle.LiveData;

import com.agriiprince.mvvm.applevel.annotation.UserType;
import com.agriiprince.mvvm.retrofit.dto.apkversions.ApkVers;
import com.agriiprince.mvvm.retrofit.dto.login.VersionUpdate;
import com.agriiprince.mvvm.data.repository.VersionUpdateRepositoryUseCase;
import com.agriiprince.mvvm.retrofit.ResponseWrapper;
import com.agriiprince.mvvm.ui.base.BaseViewModel;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

public class VersionUpdateViewModel extends BaseViewModel implements VersionUpdateViewModelUseCase {

    private VersionUpdateRepositoryUseCase mVersionUpdateUseCase;

    @Inject
    public VersionUpdateViewModel(VersionUpdateRepositoryUseCase versionUpdateUseCase) {
        this.mVersionUpdateUseCase = versionUpdateUseCase;
    }

    @Override
    public LiveData<ResponseWrapper<ApkVers>> checkForUpdate(String apkVersion, @UserType String userType) {
        return mVersionUpdateUseCase.checkForUpdate(apkVersion, userType);
    }

    @Override
    public LiveData<String> onDownloadUpdateProgress() {
        return mVersionUpdateUseCase.onDownloadUpdateProgress();
    }

    @Override
    public LiveData<String> onDownloadUpdate(@NotNull String url) {
        return mVersionUpdateUseCase.onDownloadUpdate(url);
    }
}
