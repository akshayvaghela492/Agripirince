package com.agriiprince.mvvm.viewmodel;

import android.arch.lifecycle.LiveData;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.agriiprince.mvvm.retrofit.dto.subscription.BplPhotoUpload;
import com.agriiprince.mvvm.retrofit.dto.subscription.BplUploadPhoto;
import com.agriiprince.mvvm.retrofit.dto.subscription.CouponVerification;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionDetails;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionInfo;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionUser;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionCoupon;
import com.agriiprince.mvvm.model.Subscription;
import com.agriiprince.mvvm.retrofit.ResponseWrapper;
import com.agriiprince.mvvm.retrofit.model.subscription.SubscriptionDetailList;
import com.agriiprince.mvvm.ui.subscription.SubscriptionViewState;

import java.util.List;

public interface SubscriptionViewModelUseCase {

    LiveData<SubscriptionViewState> getViewState();

    LiveData<ResponseWrapper<SubscriptionInfo>> getUserSubscription();

    LiveData<ResponseWrapper<List<SubscriptionDetailList>>> getSubscriptions();

    void activateSubscription(int subscriptionType);

    LiveData<ResponseWrapper<CouponVerification>> applyCoupon(@NonNull String couponCode);

    LiveData<ResponseWrapper<BplPhotoUpload>> uploadBplPhoto(@NonNull Bitmap bitmap);

}
