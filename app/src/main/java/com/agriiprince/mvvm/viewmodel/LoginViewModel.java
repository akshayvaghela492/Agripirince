package com.agriiprince.mvvm.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.databinding.Bindable;
import android.util.Log;

import com.agriiprince.BR;
import com.agriiprince.BuildConfig;
import com.agriiprince.activities.checkprice.SingleEvenLiveData;
import com.agriiprince.mvvm.retrofit.dto.login.Login;
import com.agriiprince.mvvm.retrofit.dto.login.TokenGenerate;
import com.agriiprince.mvvm.retrofit.dto.login.UserExist;
import com.agriiprince.mvvm.data.repository.LoginRepositoryUseCase;
import com.agriiprince.mvvm.retrofit.ResponseWrapper;
import com.agriiprince.mvvm.retrofit.dto.oe.OeLoginResponse;
import com.agriiprince.mvvm.ui.base.BaseViewModel;

import javax.inject.Inject;

public class LoginViewModel extends BaseViewModel implements LoginViewModelUseCase {

    private static final String TAG = "LoginViewModel";

    public static final int LOGIN_FRAGMENT = 0;
    public static final int SIGN_UP_FRAGMENT = 1;

    private static final String LOGIN_TYPE_PASSWORD = "with_password";
    private static final String LOGIN_TYPE_OTP = "with_otp";

    private boolean isLogin;

    /**
     * mobile number entered by the user
     */
    private String mobile;

    /**
     * password entered by the user, not used in case of login with otp
     */
    private String password;

    /**
     * user type selected by user for login
     */
    private String userType;

    private MutableLiveData<Integer> mSwitchFragmentMode;

    private MutableLiveData<String> mVerifyMobile;

    private MutableLiveData<String> mUserType;
    private MutableLiveData<String> mMobile;
    private MutableLiveData<String> mPassword;

    private LoginRepositoryUseCase mLoginRepository;

    @Inject
    public LoginViewModel(LoginRepositoryUseCase loginRepository) {
        Log.d(TAG, "LoginViewModel: " + loginRepository);
        this.mLoginRepository = loginRepository;
        this.mSwitchFragmentMode = new SingleEvenLiveData<>();
        this.mVerifyMobile = new SingleEvenLiveData<>();
        this.mUserType = new MutableLiveData<>();
        this.mMobile = new MutableLiveData<>();
        this.mPassword = new MutableLiveData<>();
    }

    @Bindable
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
        mMobile.setValue(mobile);
        notifyPropertyChanged(BR.mobile);
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        mPassword.setValue(password);
        notifyPropertyChanged(BR.password);
    }

    @Override
    public void onClickLoginFragment() {
        mSwitchFragmentMode.setValue(LOGIN_FRAGMENT);
    }

    @Override
    public void onClickSingUpFragment() {
        mSwitchFragmentMode.setValue(SIGN_UP_FRAGMENT);
    }

    @Override
    public LiveData<Integer> onSwitchFragment() {
        return mSwitchFragmentMode;
    }

    @Override
    public void onChangeLoginUserType(String userType) {
        this.userType = userType;
        mUserType.setValue(userType);
    }

    @Override
    public LiveData<String> onChangeUserType() {
        return mUserType;
    }

    @Override
    public LiveData<String> onChangeMobile() {
        return mMobile;
    }

    @Override
    public LiveData<String> onChangePassword() {
        return mPassword;
    }

    @Override
    public LiveData<ResponseWrapper<UserExist>> checkUserExists() {
        return mLoginRepository.checkUserExists(BuildConfig.USER_TYPE, mobile);
    }

    @Override
    public void verifyMobile() {
        mVerifyMobile.setValue(mobile);
    }

    @Override
    public LiveData<String> onVerifyMobile() {
        return mVerifyMobile;
    }

    @Override
    public LiveData<ResponseWrapper<Login>> onLoginUser() {
        return mLoginRepository.onLogin();
    }

    @Override
    public LiveData<ResponseWrapper<OeLoginResponse>> onLoginUserOe() {
        return mLoginRepository.onLoginOe();
    }

    @Override
    public void loginByOtp() {
        mLoginRepository.login(userType, mobile, " ", LOGIN_TYPE_OTP);
    }

    @Override
    public void loginByPassword() {
        mLoginRepository.login(userType, mobile, password, LOGIN_TYPE_PASSWORD);
    }

    @Override
    public LiveData<ResponseWrapper<TokenGenerate>> generateToken() {
        return mLoginRepository.generateAccessToken();
    }
}
