package com.agriiprince.mvvm.viewmodel;

import android.arch.lifecycle.LiveData;

import com.agriiprince.mvvm.model.Contact;

import java.util.List;

public interface ReferFriendsViewModelUseCase {

    LiveData<List<Contact>> getContacts();

    void selectAllContacts();

    void unSelectAllContacts();
}
