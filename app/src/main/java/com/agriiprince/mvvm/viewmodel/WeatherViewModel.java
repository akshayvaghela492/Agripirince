package com.agriiprince.mvvm.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.agriiprince.mvvm.model.WeatherModel;

import java.util.List;

public class WeatherViewModel extends ViewModel {

    MutableLiveData<List<WeatherModel>> weather;

   public LiveData<List<WeatherModel>> Weather(){
       return weather;
   }

}
