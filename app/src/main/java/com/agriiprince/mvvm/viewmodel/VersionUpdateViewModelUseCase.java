package com.agriiprince.mvvm.viewmodel;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.agriiprince.mvvm.applevel.annotation.UserType;
import com.agriiprince.mvvm.retrofit.dto.apkversions.ApkVers;
import com.agriiprince.mvvm.retrofit.dto.login.VersionUpdate;
import com.agriiprince.mvvm.retrofit.ResponseWrapper;

public interface VersionUpdateViewModelUseCase {

    /**
     * check for any update available to download
     *
     * @param apkVersion current version name
     * @param userType application user type for update, use build config. login user type may be different from the application user type
     * @return version update
     */
    LiveData<ResponseWrapper<ApkVers>> checkForUpdate(String apkVersion, @UserType String userType);

    /**
     * download update progress is returned
     * @return string percentage of total download
     */
    LiveData<String> onDownloadUpdateProgress();

    /**
     * this method will start the resource download in background and on complete file path is returned
     * @param url resource url
     * @return downloaded file path
     */
    LiveData<String> onDownloadUpdate(@NonNull String url);

}
