package com.agriiprince.mvvm.viewmodel;

import android.arch.lifecycle.LiveData;

import com.agriiprince.mvvm.model.Language;
import com.agriiprince.mvvm.data.repository.LanguageRepositoryUseCase;
import com.agriiprince.mvvm.ui.base.BaseViewModel;

import java.util.List;

import javax.inject.Inject;

public class LanguageViewModel extends BaseViewModel implements LanguageViewModelUseCase {

    private LanguageRepositoryUseCase mLanguageRepository;

    @Inject
    public LanguageViewModel(LanguageRepositoryUseCase languageRepositoryUseCase) {
        this.mLanguageRepository = languageRepositoryUseCase;
    }

    @Override
    public LiveData<List<Language>> getLanguages() {
        return mLanguageRepository.getLanguages();
    }

    @Override
    public void updatePrimaryLocale(String primaryLocale) {
        mLanguageRepository.updatePrimaryLocale(primaryLocale);
    }

    @Override
    public void updateSecondaryLocale(String secondaryLocale) {
        mLanguageRepository.updateSecondaryLocale(secondaryLocale);
    }
}
