package com.agriiprince.mvvm.util;

import android.content.Context;

import com.agriiprince.R;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

public class PermissionHelper {

    /**
     * no public constructor, forcing to use static methods
     */
    private PermissionHelper() {

    }


    public static void requestPermissions(Context context, PermissionListener permissionlistener,
                                         String rationaleTitle, String rationaleMessage, String deniedTitle,
                                         String deniedMessage, String settingButtonText, String[] permissions) {
        TedPermission.with(context)
                .setPermissionListener(permissionlistener)
                .setRationaleTitle(rationaleTitle)
                .setRationaleMessage(rationaleMessage)
                .setDeniedTitle(deniedTitle)
                .setDeniedMessage(deniedMessage)
                .setGotoSettingButtonText(settingButtonText)
                .setPermissions(permissions)
                .check();
    }

}
