package com.agriiprince.mvvm.util;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.text.TextUtils;

import com.agriiprince.mvvm.applevel.annotation.LanguageCode;
import com.agriiprince.mvvm.data.prefs.PreferenceManager;
import com.agriiprince.mvvm.model.Language;

import java.util.Locale;

public class BilingualHelper {

    private static int DEFAULT_PARAGRAPH_SPACING = 1;

    /**
     * primary language context to get primary resources
     */
    private Context mPrimaryContext;

    /**
     * secondary language context to get secondary resources
     */
    private Context mSecondaryContext;

    private @LanguageCode String mPrimaryLocale;

    private @LanguageCode String mSecondaryLocale;

    private boolean isBilingual = false;

    private PreferenceManager mPreferenceManager;

    public BilingualHelper(Context context, PreferenceManager preferenceManager) {
        this.mPrimaryContext = context;
        this.mPreferenceManager = preferenceManager;

        mPrimaryLocale = mPreferenceManager.getPrimaryLocale();
        mSecondaryLocale = mPreferenceManager.getSecondaryLocale();

        setSecondaryContext();
    }

    /**
     * this method will update the preferences and bilingual context to deliver the right primary strings
     * @param primaryLocale primary language code
     */
    public void setPrimaryLocale(@LanguageCode String primaryLocale) {
        this.mPrimaryLocale = primaryLocale;
        mPreferenceManager.setPrimaryLocale(primaryLocale);
        setPrimaryContext();
    }

    /**
     * this method will update the preferences and bilingual context to deliver the right secondary strings
     * @param secondaryLocale secondary language code
     */
    public void setSecondaryLocale(@LanguageCode String secondaryLocale) {
        this.mSecondaryLocale = secondaryLocale;
        mPreferenceManager.setSecondaryLocale(secondaryLocale);
        setSecondaryContext();
    }

    /**
     * update the primary context on locale change
     */
    private void setPrimaryContext() {
        Locale locale = new Locale(mPrimaryLocale);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        mPrimaryContext.getResources().updateConfiguration(config,
                mPrimaryContext.getResources().getDisplayMetrics());

        setSecondaryContext();
    }

    /**
     * check for bilingual and update the secondary context if bilingual
     */
    private void setSecondaryContext() {
        setBilingual();
        if (isBilingual) {
            Configuration config = new Configuration(mPrimaryContext.getResources().getConfiguration());
            config.setLocale(new Locale(mSecondaryLocale));
            mSecondaryContext = mPrimaryContext.createConfigurationContext(config);
        } else {
            mSecondaryContext = null;
        }
    }

    /**
     * checks if secondary locale is valid and not equal to primary locale and set the secondary locale value
     */
    private void setBilingual() {
        isBilingual = !TextUtils.isEmpty(mSecondaryLocale) && !mPrimaryLocale.equalsIgnoreCase(mSecondaryLocale);
    }


    /**
     * returns bilingual string for the string id. returns only primary string if secondary locale is not selected
     * @param stringId string resource id
     * @return {@link String}
     */
    public String getString(@StringRes int stringId) {
        return getString(stringId, false);
    }

    /**
     * returns bilingual string for the string id. returns only primary string if secondary locale is not selected.
     * Format the bilingual string to single line
     * @param stringId string resource id
     * @return {@link String}
     */
    public String getString(@StringRes int stringId, boolean singleLine) {
        return getString(stringId, DEFAULT_PARAGRAPH_SPACING, singleLine);
    }

    /**
     * returns bilingual string for the string id. returns only primary string if secondary locale is not selected.
     * Format the bilingual string to have given spaces between two language strings. default minimum spacing will be one
     * @param stringId string resource id
     * @return {@link String}
     */
    public String getString(@StringRes int stringId, int paragraphSpace) {
        return getString(stringId, paragraphSpace, false);
    }

    /**
     * returns bilingual string for the string id. returns only primary string if secondary locale is not selected.
     * Format the bilingual string to single line
     * @param stringId string resource id
     * @return formatted bilingual string
     */
    private String getString(@StringRes int stringId, int paragraphSpace, boolean singleLine) {
        if (isBilingual) {
            return getString(mPrimaryContext.getString(stringId),
                    mSecondaryContext.getString(stringId), paragraphSpace, singleLine);
        } else {
            return mPrimaryContext.getString(stringId);
        }
    }

    /**
     * returns bilingual string for the two strings. returns only primary string if secondary locale is not selected.
     * @param primaryString primary string
     * @param secondaryString secondary string
     * @return formatted bilingual string
     */
    public String getString(@NonNull String primaryString, @Nullable String secondaryString) {
        return getString(primaryString, secondaryString, false);
    }

    /**
     * returns bilingual string for the two strings. returns only primary string if secondary locale is not selected.
     * Format the bilingual string to single line if single line is true
     * @param primaryString primary string
     * @param secondaryString secondary string
     * @return formatted bilingual string
     */
    public String getString(@NonNull String primaryString, @Nullable String secondaryString, boolean singleLine) {
        return getString(primaryString, secondaryString, DEFAULT_PARAGRAPH_SPACING, singleLine);
    }

    /**
     * returns bilingual string for the two strings. returns only primary string if secondary locale is not selected.
     * Format the bilingual string to have given spaces between two language strings. default minimum spacing will be one
     * @param primaryString primary string
     * @param secondaryString secondary string
     * @return formatted bilingual string
     */
    public String getString(@NonNull String primaryString, @Nullable String secondaryString, int paragraphSpace) {
        return getString(primaryString, secondaryString, paragraphSpace, false);
    }

    /**
     * returns bilingual string for the two strings. returns only primary string if secondary locale is not selected.
     * Format the bilingual string to single line if true or else paragraph spacing
     * @param primaryString primary string
     * @param secondaryString secondary string
     * @return formatted bilingual string
     */
    private String getString(@NonNull String primaryString, @Nullable String secondaryString,
                                     int paragraphSpace, boolean singleLine) {

        if (!TextUtils.isEmpty(secondaryString)) return primaryString;

        if (singleLine) {
            if (mPrimaryLocale.equalsIgnoreCase(Language.Code.ENGLISH)) {
                return primaryString + "/" + secondaryString;
            } else {
                return secondaryString + "/" + primaryString;
            }
        } else {
            StringBuilder text = new StringBuilder();
            if (mPrimaryLocale.equalsIgnoreCase(Language.Code.ENGLISH)) text.append(primaryString);
            else text.append(secondaryString);

            do {
                text.append("\n");
                paragraphSpace--;
            } while (paragraphSpace > 0);

            if (mPrimaryLocale.equalsIgnoreCase(Language.Code.ENGLISH)) text.append(secondaryString);
            else text.append(primaryString);

            return text.toString();
        }
    }
}
