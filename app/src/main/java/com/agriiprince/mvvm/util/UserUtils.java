package com.agriiprince.mvvm.util;

import com.agriiprince.R;
import com.agriiprince.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import static com.agriiprince.mvvm.model.User.Type.COMMISSION_AGENT;
import static com.agriiprince.mvvm.model.User.Type.FARMER;
import static com.agriiprince.mvvm.model.User.Type.FORWARDING_AGENT;
import static com.agriiprince.mvvm.model.User.Type.OPERATIVE_EXECUTIVE;
import static com.agriiprince.mvvm.model.User.Type.PESTICIDE_VENDOR;
import static com.agriiprince.mvvm.model.User.Type.TRADER;
import static com.agriiprince.mvvm.model.User.Type.TRUCKER_SHIPPER;

public class UserUtils {

    public static List<String> getUserTypeNames(StringUtils stringUtils) {
        List<String> userTypes = new ArrayList<>();
        userTypes.add(stringUtils.getLocalizedString(R.string.commission_agents_directory).toString());
        userTypes.add(stringUtils.getLocalizedString(R.string.trader).toString());
        userTypes.add(stringUtils.getLocalizedString(R.string.farmer).toString());
        userTypes.add(stringUtils.getLocalizedString(R.string.trucker_shipper).toString());
        userTypes.add(stringUtils.getLocalizedString(R.string.operative_executive).toString());
        userTypes.add(stringUtils.getLocalizedString(R.string.forwarding_agent).toString());
        userTypes.add(stringUtils.getLocalizedString(R.string.pesticide_vendor).toString());

        return userTypes;
    }

    public static String getUserType(int index) {
        switch (index) {
            case 0:
                return COMMISSION_AGENT;
            case 1:
                return TRADER;
            case 2:
                return FARMER;
            case 3:
                return TRUCKER_SHIPPER;
            case 4:
                return OPERATIVE_EXECUTIVE;
            case 5:
                return FORWARDING_AGENT;
            case 6:
                return PESTICIDE_VENDOR;
            default:
                return FARMER;
        }
    }

    public static String getLoginUserType(int index) {
        switch (index) {
            case 0:
                return "Commission Agent";
            case 1:
                return "Trader";
            case 2:
                return "Farmer";
            case 3:
                return "Trucker / Shipper";
            case 4:
                return "Operations Executive";
            case 5:
                return "Forwarding Agent";
            case 6:
                return "Pesticides Vendor";
            default:
                return "Farmer";
        }
    }

    public static String getUserTypeFromUserName(String userName) {
        switch (userName) {
            case "Commission Agent":
                return COMMISSION_AGENT;
            case "Trader":
                return TRADER;
            case "Farmer":
                return FARMER;
            case "Trucker / Shipper":
                return TRUCKER_SHIPPER;
            case "Operations Executive":
                return OPERATIVE_EXECUTIVE;
            case "Forwarding Agent":
                return FORWARDING_AGENT;
            case "Pesticides Vendor":
                return PESTICIDE_VENDOR;
            default:
                return FARMER;
        }
    }

}
