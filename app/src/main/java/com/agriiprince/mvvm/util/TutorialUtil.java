package com.agriiprince.mvvm.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.retrofit.model.tutorial.SubSection;
import com.agriiprince.mvvm.retrofit.model.tutorial.TutorialSections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TutorialUtil {

    private TutorialUtil() {}

    public static TutorialSections getSectionBySectionCode(String code, String lang, List<TutorialSections> TutList) {

        for(TutorialSections section: TutList)
        {
            if(section.getSectionCode().equals(code))
            {
                return section;
            }
        }
        return null;

        /*Log.e("TUT_DATA ----> ", "1");

        if (lang.equals("hi"))
        {
            Log.e("TUT_DATA ----> ", "2");

            ArrayList<TutorialSections> HinTutList = (ArrayList) AppController.getInstance().gTutorialSectionsHi;
            Log.e("TUT_DATA ----> ", "3" + HinTutList.size());

            for(TutorialSections section: HinTutList)
            {
                Log.e("TUT_DATA ----> ", "4" + section.getSectionCode());
                if(section.getSectionCode().equals(code))
                {
                    Log.e("TUT_DATA ----> ", "5");
                    return section;
                }
            }
        }
        else
        {
            List<TutorialSections> EngTutList = AppController.getInstance().gTutorialSectionsEn;
            for(TutorialSections section: EngTutList)
            {
                if(section.getSectionCode().equals(code))
                {
                    return section;
                }
            }
        }
        return null;*/
    }

    public static String getStringFromSection(TutorialSections section) {

        String stringToReturn = "";

        stringToReturn = stringToReturn + section.getSectionTitle() + section.getSectionContent();

        for(SubSection subSection: section.getSubSections())
        {
            stringToReturn = stringToReturn + subSection.getSubSectionTitle() + subSection.getSubSectionContent();
        }

        return stringToReturn;
    }

    public static String getStringBySectionCode(String code, String lang, List<TutorialSections> TutList) {

        Log.e("TUT_DATA ----> ", "" + code + lang);
        Collections.reverse(TutList);

        TutorialSections tutorialSection = getSectionBySectionCode(code, lang, TutList);
        Log.e("TUT_DATA ----> ", "" + tutorialSection.toString());

        String result  = getStringFromSection(tutorialSection);
        Log.e("TUT_DATA ----> ", "" + result);

        return result;
    }
}
