package com.agriiprince.mvvm.util;

import android.support.annotation.NonNull;
import android.util.Log;

import com.agriiprince.BuildConfig;

public class Logs {

    private Logs() {}

    public static void d(@NonNull String TAG, @NonNull String message) {
        if (BuildConfig.DEBUG) Log.d(TAG, message+"");
    }

    public static void i(@NonNull String TAG, @NonNull String message) {
        if (BuildConfig.DEBUG) Log.i(TAG, message);
    }

    public static void w(@NonNull String TAG, @NonNull String message) {
        Log.w(TAG, message);
    }
}
