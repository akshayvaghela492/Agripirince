package com.agriiprince.mvvm.di.module;

import com.agriiprince.mvvm.di.scope.ActivityScope;
import com.agriiprince.mvvm.ui.refer.ReferFriendsActivity;
import com.agriiprince.mvvm.ui.refer.ReferModule;
import com.agriiprince.mvvm.ui.subscription.SubscriptionActivity;
import com.agriiprince.mvvm.ui.subscription.SubscriptionFragmentBindingModule;
import com.agriiprince.mvvm.ui.subscription.SubscriptionModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FarmerActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector(
            modules = {
                    SubscriptionModule.class,
                    SubscriptionModule.BindSubscriptionViewModel.class,
                    SubscriptionFragmentBindingModule.class
            }
    )
    public abstract SubscriptionActivity contributesSubscriptionActivityInjection();

    @ActivityScope
    @ContributesAndroidInjector(
            modules = {
                    ReferModule.class,
                    ReferModule.BindReferViewModel.class
            }
    )
    public abstract ReferFriendsActivity contributesReferFriendsActivityInjection();
}
