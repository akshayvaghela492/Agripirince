package com.agriiprince.mvvm.di.module;

import android.app.Application;

import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.utils.StringUtils;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Singleton
    @Provides
    public PrefManager providesPreferenceManager(Application application) {
        return new PrefManager(application);
    }

    @Singleton
    @Provides
    public StringUtils providesStringUtils(Application application) {
        return new StringUtils(application);
    }
}
