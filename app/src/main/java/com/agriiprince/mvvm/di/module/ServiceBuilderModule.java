package com.agriiprince.mvvm.di.module;


import com.agriiprince.mvvm.applevel.service.FcmService;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ServiceBuilderModule {

    @ContributesAndroidInjector
    abstract FcmService contributesFcmService();
}
