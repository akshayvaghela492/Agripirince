package com.agriiprince.mvvm.di.component;

import android.app.Application;

import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.di.module.AppActivityBuilderModule;
import com.agriiprince.mvvm.di.module.AppModule;
import com.agriiprince.mvvm.di.module.FarmerActivityBuilderModule;
import com.agriiprince.mvvm.di.module.NetworkModule;
import com.agriiprince.mvvm.di.module.ServiceBuilderModule;
import com.agriiprince.mvvm.di.module.ViewModelFactoryModule;
import com.agriiprince.mvvm.ui.language.LanguageModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;


/**
 * Dagger Application component for injecting the modules
 */
@Singleton
@Component(
        modules = {
                AndroidInjectionModule.class,
                AndroidSupportInjectionModule.class,
                AppModule.class,
                NetworkModule.class,
                LanguageModule.class,
                AppActivityBuilderModule.class,
                FarmerActivityBuilderModule.class,
                ServiceBuilderModule.class,
                ViewModelFactoryModule.class
        }
)
public interface AppComponent {

    /**
     * inject the application after building the component
     *
     * @param appController application injector
     */
    void inject(AppController appController);

    /**
     * The interface Builder.
     */
    @Component.Builder
    interface Builder {

        /**
         * build the application
         *
         * @param application {@link Application}
         * @return Builder
         */
        @BindsInstance
        Builder application(Application application);

        /**
         * returns the build component
         * @return {@link AppComponent}
         */
        AppComponent build();
    }
}
