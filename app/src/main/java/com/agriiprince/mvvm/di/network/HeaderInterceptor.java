package com.agriiprince.mvvm.di.network;

import android.os.Build;

import com.agriiprince.BuildConfig;
import com.agriiprince.mvvm.util.Logs;
import com.google.gson.Gson;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HeaderInterceptor implements Interceptor {

    private static final String TAG = "HeaderInterceptor";

    public HeaderInterceptor() {

    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();

        Field[] fields = Build.VERSION_CODES.class.getFields();
        String osName = fields[Build.VERSION.SDK_INT ].getName();

        builder.addHeader("Content-Type", "application/x-www-form-urlencoded");
        builder.addHeader("osVersion", osName);
        builder.addHeader("appVersion", BuildConfig.VERSION_NAME);
        builder.addHeader("platform", "android");
        builder.addHeader("language-type", Locale.getDefault().getLanguage());
        Logs.d(TAG, "Header:" + new Gson().toJson(builder));
        return chain.proceed(builder.build());
    }
}
