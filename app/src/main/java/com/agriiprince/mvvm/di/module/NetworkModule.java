package com.agriiprince.mvvm.di.module;

import android.app.Application;

import com.agriiprince.BuildConfig;
import com.agriiprince.mvvm.di.network.ConnectivityInterceptor;
import com.agriiprince.mvvm.di.network.HeaderInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.lang.reflect.Modifier;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    private static final int CACHE_SIZE = 2 * 1024 * 1024; // 2 MB cache

    private static final String CACHE_FILE_NAME = "httpCache";

    @Singleton
    @Provides
    Retrofit providesRetrofit(Gson gson, OkHttpClient client) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BuildConfig.BASE_URL)
                .client(client)
                .build();
    }

    @Singleton
    @Provides
    Gson providesGson() {
        return new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .setLenient()
                .create();
    }

    @Singleton
    @Provides
    OkHttpClient providesOkHttpClient(
            HttpLoggingInterceptor loggingInterceptor, ConnectivityInterceptor connectivityInterceptor,
            HeaderInterceptor headerInterceptor, Cache cache) {

        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();

        // add logging only in debug mode
        if (BuildConfig.DEBUG) {
            okHttpClient.addInterceptor(loggingInterceptor);
        }

        okHttpClient.interceptors().add(connectivityInterceptor);
        okHttpClient.interceptors().add(headerInterceptor);

        okHttpClient.connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .cache(cache);

        return okHttpClient.build();
    }

    @Singleton
    @Provides
    HttpLoggingInterceptor providesHttpLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    @Singleton
    @Provides
    ConnectivityInterceptor providesConnectivityInterceptor(Application application) {
        return new ConnectivityInterceptor(application);
    }

    @Singleton
    @Provides
    HeaderInterceptor providesHeaderInterceptor() {
        return new HeaderInterceptor();
    }

    @Singleton
    @Provides
    Cache providesCache(File cacheFile) {
        return new Cache(cacheFile, CACHE_SIZE);
    }

    @Singleton
    @Provides
    File providesCacheFile(Application application) {
        return new File(application.getCacheDir(), CACHE_FILE_NAME);
    }

}
