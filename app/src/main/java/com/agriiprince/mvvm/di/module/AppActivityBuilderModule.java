package com.agriiprince.mvvm.di.module;

import com.agriiprince.mvvm.di.scope.ActivityScope;
import com.agriiprince.mvvm.ui.language.LanguageActivity;
import com.agriiprince.mvvm.ui.login.LoginActivity;
import com.agriiprince.mvvm.ui.login.LoginFragmentBindingModule;
import com.agriiprince.mvvm.ui.login.LoginModule;
import com.agriiprince.mvvm.ui.splash.SplashActivity;
import com.agriiprince.mvvm.ui.splash.VersionUpdateModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AppActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector(
            modules = {
                    VersionUpdateModule.class,
                    VersionUpdateModule.BindVersionUpdateViewModelModule.class
            }
    )
    public abstract SplashActivity contributesSplashActivityInjector();

    @ActivityScope
    @ContributesAndroidInjector
    public abstract LanguageActivity contributesLanguageSelectionActivityInjection();


    @ContributesAndroidInjector(
            modules = {
                    LoginModule.class,
                    LoginModule.BindLoginViewModel.class,
                    LoginFragmentBindingModule.class
            }
    )
    public abstract LoginActivity contributesLoginActivityInjection();
}
