package com.agriiprince.mvvm.di.module;

import android.arch.lifecycle.ViewModelProvider;

import com.agriiprince.mvvm.ui.refer.ReferModule;
import com.agriiprince.mvvm.ui.subscription.SubscriptionModule;
import com.agriiprince.mvvm.ui.language.LanguageModule;
import com.agriiprince.mvvm.ui.login.LoginModule;
import com.agriiprince.mvvm.ui.splash.VersionUpdateModule;
import com.agriiprince.mvvm.viewmodel.ViewModelFactory;

import dagger.Binds;
import dagger.Module;

@Module(
        includes = {
                LanguageModule.class,
                LoginModule.BindLoginViewModel.class,
                ReferModule.BindReferViewModel.class,
                SubscriptionModule.BindSubscriptionViewModel.class,
                VersionUpdateModule.BindVersionUpdateViewModelModule.class
        }
)
public abstract class ViewModelFactoryModule {

    @Binds
    public abstract ViewModelProvider.Factory bindsViewModelFactory(ViewModelFactory viewModelFactory);

}
