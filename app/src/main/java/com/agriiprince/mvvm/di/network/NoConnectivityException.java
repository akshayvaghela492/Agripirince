package com.agriiprince.mvvm.di.network;

import java.io.IOException;

public class NoConnectivityException extends IOException {

    public NoConnectivityException() {
        super("No Connectivity");
    }

    public NoConnectivityException(String message) {
        super(message);
    }
}
