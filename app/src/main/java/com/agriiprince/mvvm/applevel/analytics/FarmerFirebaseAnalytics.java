package com.agriiprince.mvvm.applevel.analytics;

/**
 * Created by Suryakant Bharti on 24-01-2019.
 */

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * {@link FarmerFirebaseAnalytics} helps log analytics events
 */

public class FarmerFirebaseAnalytics {

    //ANALYTICS UNIQUE USER IDS [NEEDS PAID FIREBASE...]
    public static void logUserId(Context context, String Id) {
        FirebaseAnalytics.getInstance(context).setUserId(Id);
    }

    //ANALYTICS CUSTOM EVENTS [NEEDS GCP/BIGQUERY PAID/CREDITS...]
    public static void logEventFarmerClicked(Context context, String number, String userid, String name, String city, String  pin, String event) {
        Bundle params = new Bundle();
        params.putString("number", number);
        params.putString("userid", userid);
        params.putString("name", name);
        params.putString("city", city);
        params.putString("pin", pin);
        FirebaseAnalytics.getInstance(context).logEvent(event, params);
    }
}
