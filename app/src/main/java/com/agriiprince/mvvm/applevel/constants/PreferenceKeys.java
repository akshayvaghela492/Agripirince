package com.agriiprince.mvvm.applevel.constants;

public class PreferenceKeys {

    public static final String KEY_PRIMARY_LOCALE = "primary_locale";
    public static final String KEY_SECONDARY_LOCALE = "secondary_locale";

    public static final String KEY_HARVEST_AREA_UNIT = "harvest_area_unit";
    public static final String KEY_HARVEST_AREA = "harvest_area";
    public static final String KEY_HARVEST_YIELD_UNIT = "harvest_yield_unit";
    public static final String KEY_HARVEST_YIELD = "harvest_yield";
    public static final String KEY_HARVEST_COST = "harvest_cost";

    public static final String KEY_TRANSPORT_WEIGHT_UNIT = "transport_weight_unit";
    public static final String KEY_TRANSPORT_WEIGHT = "transport_weight";
    public static final String KEY_TRANSPORT_DISTANCE_UNIT = "transport_distance_unit";
    public static final String KEY_TRANSPORT_DISTANCE = "transport_distance";
    public static final String KEY_TRANSPORT_COST = "transport_cost";

    public static final String KEY_WASTE_PERCENT_INDEX = "waste_percent_index";
    public static final String KEY_WASTE_DISTANCE_UNIT = "waste_distance_unit";
    public static final String KEY_WASTE_DISTANCE = "waste_distance";

    public static final String KEY_OTHER_FIXED = "other_fixed";
    public static final String KEY_OTHER_COST = "other_cost";
    public static final String KEY_OTHER_DISTANCE_UNIT = "other_distance_unit";
    public static final String KEY_OTHER_DISTANCE = "other_distance";
    public static final String KEY_CHECKPRICING_SHOW_DIALOG = "show_dialog";
}
