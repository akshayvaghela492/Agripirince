package com.agriiprince.mvvm.applevel.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.agriiprince.R;
import com.agriiprince.activities.ChatOneToOneActivity;
import com.agriiprince.activities.ChatRoomActivity;
import com.agriiprince.mvvm.ui.weather.WeatherActivityFinal;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.data.repository.LoginRepositoryUseCase;
import com.agriiprince.mvvm.util.Logs;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

import static com.agriiprince.utils.Utils.showLog;

public class FcmService extends FirebaseMessagingService {

    private static final String TAG = "FcmService";

    PrefManager prefManager;

    @Inject
    LoginRepositoryUseCase mLoginRepository;

    public static void deleteInstanceId(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static void resetInstanceId() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                    FirebaseInstanceId.getInstance().getInstanceId();
                    Logs.d(TAG, "InstanceId removed and regenerated.");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public void onCreate() {
        AndroidInjection.inject(this);
        super.onCreate();
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

        prefManager = new PrefManager(getApplicationContext());

        Log.d("Token", "" + s);

        mLoginRepository.updateFcmToken(s);

/*
        if (prefManager.getFCMToken() == null || ! prefManager.getFCMToken().equals(s)) {
            prefManager.setFCMToken(s);

            String id = GeneralUtils.fnGetID(getApplicationContext());

            if (id != null)
                new FcmTokenUpdateDataService(id, prefManager.onChangeUserType(), s);
        }
*/
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        prefManager = new PrefManager(this);

        //todo rishijay - mode1, mode2 is coming show heading as per
        showLog("json data" + String.valueOf(remoteMessage.getData()));

    /*if (title.equalsIgnoreCase("chat")){
      processNotification(remoteMessage.getData().get("message"));
      showLog("onMessageReceived : " + remoteMessage.getData().get("message"));
    }else if (title.equalsIgnoreCase("weather")){
      pushNotification(remoteMessage.getData().get("message"));
      showLog("onMessageReceived : " + remoteMessage.getData().get("message"));
    }else {*/

        //todo if user logged out then no notification
        //do not use below line
        //processNotification(remoteMessage.getData().get("message"));
        //showLog("onMessageReceived : " + remoteMessage.getData().get("message"));
        //}

        //for weather
        //todo remoteMessage.getData().get("weather");
        //on click of it go to WeatherNotificationActivity where we will show details in detail and
        // remedies for it. and also add this WeatherNotificationActivity to onOptionMenu for
        // WeatherActivityFinal.

        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(remoteMessage.getData()));
            parseJson(jsonObject);
            //convertStandardJSONString(userJsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String convertStandardJSONString(String data_json) {
        data_json = data_json.replaceAll("\\\\r\\\\n", "");
        data_json = data_json.replace("\"{", "{");
        data_json = data_json.replace("}\",", "},");
        data_json = data_json.replace("}\"", "}");
        return data_json;
    }

    private void parseJson(JSONObject jsonObject) {
        try {
            String type = jsonObject.getString("type"); //type : chat, notification, weather
            String mode1 = jsonObject.getString("mode1");
            String message = jsonObject.getString("message");
            String message_id = "-1";
            if (mode1.equalsIgnoreCase("farmer_farmer"))
                message_id = jsonObject.getString("message_id");

            String sender_id = jsonObject.getString("sender_id");
            String receiver_id = jsonObject.getString("receiver_id");
            String sender_type = jsonObject.getString("sender_type");
            String receiver_type = jsonObject.getString("receiver_type");
            showLog(jsonObject.toString());

            createNotificationChannel();
            if (type.equalsIgnoreCase("chat")) {
                if (mode1.equalsIgnoreCase("farmer_all")) {
                    String mode2 = jsonObject.getString("mode2");
                    String commodity_id = jsonObject.getString("commodity_id");
                    String pincode = jsonObject.getString("pincode");
                    String city = jsonObject.getString("city");
                    String name = jsonObject.getString("name");
                    showLog("mode2 " + mode2);
                    showLog("commodity_id " + commodity_id);
                    showLog("pincode " + pincode);
                    showLog("city " + city);
                    pushChatRoomUpdate(mode2, commodity_id, pincode, city, name, message, sender_id);
                }

                if (mode1.equalsIgnoreCase("farmer_farmer")
                        || mode1.equalsIgnoreCase("pv_farmer") || mode1.equalsIgnoreCase("farmer_pv")) {
                    String name = jsonObject.getString("name"); //sender name
                    showLog("sender name " + name);
                    pushOneToOneNotification(message_id, sender_id, jsonObject.getString("name"), message);
                }
            } else {
                // Todo Rishijay : need to change it later
                processNotification(message);
            }


            //todo rishijay show notification on basis of above and check which activity and whether it
            // is visible or not (check below code)
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static final String CHANNEL_ID = "chat";

    public static final String CHANNED_GROUP_CHAT = "group_chat";

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Chat notifications";
            String description = "Notify when chat message arrives";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void pushChatRoomUpdate(String mode2, String commodity_id, String pincode, String city, String name, String message, String sender_id) {
        String chatIdentifier = AppController.getInstance().getChatIdentifier();
        if (commodity_id.equalsIgnoreCase(chatIdentifier) || pincode.equalsIgnoreCase(chatIdentifier) || city.equalsIgnoreCase(chatIdentifier)) {

            Intent pushChatRoomUpdate = new Intent(Config.NOTIFICATION_TAG3);
            showLog("pushChatRoomUpdate : " + "sending broadcast");
            pushChatRoomUpdate.putExtra("message", message);
            pushChatRoomUpdate.putExtra("mode2", mode2);
            pushChatRoomUpdate.putExtra("commodity_id", commodity_id);
            pushChatRoomUpdate.putExtra("pincode", pincode);
            pushChatRoomUpdate.putExtra("city", city);
            pushChatRoomUpdate.putExtra("name", name);
            pushChatRoomUpdate.putExtra("sender_id", sender_id);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushChatRoomUpdate);
        } else {
            String groupName = " ";

            Intent intent = new Intent(this, ChatRoomActivity.class);
            intent.putExtra(ChatRoomActivity.KEY_MODE, mode2);
            if (mode2.equalsIgnoreCase("pincode")) {
                intent.putExtra(ChatRoomActivity.KEY_PINCODE, pincode);
                groupName = pincode;
            } else if (mode2.equalsIgnoreCase("commodity_id")) {
                intent.putExtra(ChatRoomActivity.KEY_COMMODITY, commodity_id);
                groupName = commodity_id;
            } else if (mode2.equalsIgnoreCase("city")) {
                intent.putExtra(ChatRoomActivity.KEY_CITY, city);
                groupName = city;
            }

            Log.e("receiver_name", name);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            final NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNED_GROUP_CHAT);
            builder.setStyle(new NotificationCompat.BigTextStyle(builder)
                    .bigText(message)
                    .setBigContentTitle(name + " sent a message in " + groupName + " group"))
                    .setContentTitle(name + " sent a message in " + groupName + " group")
                    .setContentText(message)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ap_logo_final)
                    .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.ap_logo_final));
            builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            builder.setVibrate(new long[]{0, 100, 200, 300});
            builder.setAutoCancel(true);

            final NotificationManager nm = (NotificationManager) getSystemService(
                    Context.NOTIFICATION_SERVICE);
            nm.notify(0, builder.build());
        }
    }

    private void pushWeatherNotification(String message) {
        Intent intent = new Intent(this, WeatherActivityFinal.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        final Notification.Builder builder = new Notification.Builder(this);
        builder.setStyle(new Notification.BigTextStyle(builder)
                .bigText(message)
                .setBigContentTitle("WeatherForecast NotificationsResp"))
                .setContentTitle("WeatherForecast NotificationsResp")
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ap_logo_final)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.ap_logo_final));

        final NotificationManager nm = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);
        nm.notify(0, builder.build());
    }

    private void pushOneToOneNotification(String message_id, String receiver_id, String name, String message) {
        Intent pushOneToOneNotification = new Intent(Config.NOTIFICATION_TAG2);
        //Adding notification data to the intent
        pushOneToOneNotification.putExtra("message_id", message_id);
        pushOneToOneNotification.putExtra("message", message);

        if (receiver_id.equalsIgnoreCase(AppController.getInstance().getChatIdentifier()) &&
                AppController.getInstance().isChatOneToOneActivityVisible()) {

            showLog("pushOneToOneNotification : " + "sending broadcast");
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushOneToOneNotification);
        } else {
            Intent intent = new Intent(this, ChatOneToOneActivity.class);
            intent.putExtra("call_from", prefManager.getUserType());
            intent.putExtra("message_id", message_id);
            intent.putExtra("receiver_id", receiver_id);
            intent.putExtra("receiver_name", name);
            Log.e("receiver_name", name);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            final NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
            builder.setStyle(new NotificationCompat.BigTextStyle(builder)
                    .bigText(message)
                    .setBigContentTitle(name + " sent you message"))
                    .setContentTitle(name + " sent you message")
                    .setContentText(message)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ap_logo_final)
                    .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.ap_logo_final));
            builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            builder.setVibrate(new long[]{0, 100, 200, 300});
            builder.setAutoCancel(true);

            final NotificationManager nm = (NotificationManager) getSystemService(
                    Context.NOTIFICATION_SERVICE);
            nm.notify(0, builder.build());
        }
    }


    private void processNotification(String message) {

        //todo do not process message if logged out
        //Creating a broadcast intent
        Intent pushNotification = new Intent(Config.NOTIFICATION_TAG);
        //Adding notification data to the intent
        pushNotification.putExtra("message", message);
        //pushNotification.putExtra("name", title);
        //pushNotification.putExtra("id", id);

        //if (!AppController.getInstance().isCommunicateToAPActivityVisible()) {
        showLog("process notification : " + AppController.getInstance()
                .isCommunicateToAPActivityVisible());
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ap_logo_final);
        Intent intent = new Intent(this, WeatherActivityFinal.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        builder.setContentIntent(pendingIntent);
        builder.setLargeIcon(
                BitmapFactory.decodeResource(this.getResources(), R.drawable.ap_logo_final));
        builder.setContentTitle("AP WeatherForecast Update");
        builder.setContentText(message);
        //builder.setSmallIcon(android.R.drawable.ic_menu_help);
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        builder.setVibrate(new long[]{0, 100, 200, 300});
        builder.setAutoCancel(true);
        NotificationManager notificationManager = (NotificationManager) getSystemService(
                NOTIFICATION_SERVICE);
        notificationManager.notify(Config.NOTIFICATION_ID, builder.build());
    /*} else {
      showLog("process notification : " + "sending broadcast");
      LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
    }*/

    /*Intent i = new Intent(this,MainActivity.class);
    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

    PendingIntent pendingIntent = PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_UPDATE_CURRENT);

    NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
        .setAutoCancel(true)
        .setContentTitle("FCM Test")
        .setContentText(message)
        .setSmallIcon(android.R.drawable.ic_menu_help)
        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        .setVibrate(new long[]{0, 100, 200, 300})
        .setContentIntent(pendingIntent);

    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

    manager.notify(0,builder.build());*/
    }


}
