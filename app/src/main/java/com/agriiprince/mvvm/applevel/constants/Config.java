package com.agriiprince.mvvm.applevel.constants;

import com.agriiprince.BuildConfig;

public class Config {

    // KEYS & OTHER CONSTANTS:
    // ===================================

    // RazorPay related keys required in Frontend [Do not delete these, to be used in manifest]
    public static final String RAZORPAY_MECHANT_ID = "D9kaYzWPKm4hIJ";
    public static final String RAZORPAY_KEY_ID_TEST = "rzp_test_QqTvdYRl4nj2Kx";  // Change these in Manifest
    public static final String RAZORPAY_KEY_ID_LIVE = "rzp_live_eqHNAMXiB3LzWP";  // Change these in Manifest


    // Other Keys
    public static final String YOUTUBE_KEY = "AIzaSyAH2wEx0PBo-s8VqojaMkA0_NzyrxRQ0tQ";
    public static final String YOUTUBE_VIDE_CODE = "_oEA18Y8gM0";

    // Constants
    public static final int NOTIFICATION_ID = 235;
    public static final String NOTIFICATION_TAG = "NotificationsResp";
    public static final String NOTIFICATION_TAG2 = "ChatOneToOneNotification";
    public static final String NOTIFICATION_TAG3 = "ChatRoomUpdate";
    public static final String IMAGE_NAME_END = "_1";


    // NON-API WEB LINKS:
    // ===================================

    // Terms and Conditions
    public static final String TERMS_AND_CONDITION = "https://agriiprince.com/t&c.php";

    // Apk Download Urls
    public static final String APP_LINK_FARMER = "https://apk.agriiprince.com/agriiprince.apk";

    // Google Map Urls
    public static final  String GOOGLE_MAP = "https://maps.googleapis.com/maps/api/geocode/json?address=";
    public static final  String DISTANCE_URL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=";

    // RSS Module Related Urls
    public static final String INDIA_TIMES = "https://economictimes.INDIA_TIMES.com/rssfeeds/1202099874.cms";
    public static final String THE_HINDU = "https://www.THE_HINDU.com/sci-tech/agriculture/?service=rss";
    public static final String DOWN_TO_EARTH = "https://www.DOWN_TO_EARTH.org.in/rss/agriculture";
    public static final String OPEN_THE_MAGAZINE = "http://www.OPEN_THE_MAGAZINE.com/taxonomy/term/25949/feed";
    public static final String OECD = "http://www.OECD.org/agriculture/index.xml";
    public static final String RSS_EN = "https://services.india.gov.in/feed/rss?cat_id=12&ln=en";
    public static final String RSS_HI = "https://services.india.gov.in/feed/rss?cat_id=12&ln=hi";
    public static final String GOI_Directory = "http://GOI_Directory.nic.in/rss/minstry_rss.php?categ_id=1";
    public static final String GOI_DIRECTORY_AYUSH = "http://GOI_Directory.nic.in/rss/minstry_rss.php?categ_id=723";
    public static final String GOI_DIRECTORY_CHEMICAL = "http://GOI_Directory.nic.in/rss/minstry_rss.php?categ_id=3";
    public static final String NATURE = "https://www.NATURE.com/subjects/agriculture.rss";


    // NODE API CONSTANTS:
    // ===================================

    // DEV
    public static final String AWS_NODE_BASE_URL = "http://13.233.94.164:8000";

    // PROD
    //public static final String AWS_NODE_BASE_URL = "http://13.232.163.213:8000";

    public static final String NODE_APIS_CROP = AWS_NODE_BASE_URL + "/api/v1/crop/";
    public static final String NODE_APIS_USERS = AWS_NODE_BASE_URL + "/api/v1/users/";
    public static final String NODE_APK_VERSION=AWS_NODE_BASE_URL+"/api/v1/apk_version/";
    public static final String NODE_COLD_STORAGE =AWS_NODE_BASE_URL+"/api/v1/cold_storage/";
    public static final String NODE_CROP_LIST =AWS_NODE_BASE_URL+"/api/v1/crop_list/";
    public static final String NODE_DISEASE=AWS_NODE_BASE_URL+"/api/v1/crop_diseases/";
    public static final String NODE_NOTIFICATION=AWS_NODE_BASE_URL+"/api/v1/notifications/";
    public static final String NODE_WEATHER=AWS_NODE_BASE_URL+"/api/v1/weather/forecast/";

    public static final String NODE_RAZOR_PAY=AWS_NODE_BASE_URL+"/api/v1/razorpay/";

    public static final String NODE_SUBSCRIPTION=AWS_NODE_BASE_URL+"/api/v1/subscription/";
    public static final String NODE_CHECK_PRICE=AWS_NODE_BASE_URL+"/api/v1/price/";
    public static final String NODE_CA=AWS_NODE_BASE_URL+"/api/v1/ca_directory/";
    public static final String NODE_TUTORIAL=AWS_NODE_BASE_URL+"/api/v1/";


    // PHP API CONSTANTS:
    // ===================================

    // Base API Urls
    public static final String BASE_URL = "http://www.newapi1.agriiprince.com/testing/";

    // DEV
    public static final String NEW_BASE_URL = "https://devapis.agriiprince.com/phpapis/";

    // PROD
    //public static final String NEW_BASE_URL = "https://prodapis.agriiprince.com/phpapis/";

    // Images Urls (These are not APIs, these are base urls for downloading images)
    //public static final String DISEASE_IMAGE_BASE_URL = BASE_URL + "all_images/crop_diseases_images/";
    //public static final String CROP_IMAGE_BASE_URL = BASE_URL + "all_images/crop_name_images/";

    // Checkprice related apis
    public static final String GET_OE_REPORTED_PRICE = NEW_BASE_URL + "checkprice/get_oe_reported_price_us1.php";
    public static final String GET_GOV_REPORTED_PRICE = NEW_BASE_URL + "checkprice/get_gov_reported_price.php";

    // Subscription related apis
    public static final String POST_SUBSCRIPTION_INFORMATION = NEW_BASE_URL + "subscription/subscription_information.php";
    public static final String POST_SUBSCRIPTION_DETAILS = NEW_BASE_URL + "subscription/subscription_details.php";
    public static final String POST_SUBSCRIPTION_VERIFY_COUPON = NEW_BASE_URL + "subscription/subscription_verify_coupon.php";
    public static final String POST_MANUAL_TRANSACTION_VERIFY_OTP = NEW_BASE_URL + "subscription/manual_transaction_verify_otp.php";

    // Chat related apis
    public static final String GET_CHAT_MESSAGE_AT_START_UP = NEW_BASE_URL + "get_chat_message_at_start_up.php";
    public static final String GET_FARMER_ONE_TO_ONE_CHAT_MESSAGE_AT_START_UP = NEW_BASE_URL + "fetch_chat_message.php";
    public static final String BRING_PINCODE_AND_CITIES_DATA = NEW_BASE_URL + "get_pincode_cities_commodity.php";
    public static final String UPDATE_READ_STATUS_OF_CHAT = NEW_BASE_URL + "update_chat_status.php";
    public static final String GET_FARMERS_LIST = NEW_BASE_URL + "get_farmer_list.php";

    // profile/nav_drawer related apis
    public static final String GET_PROFILE = NEW_BASE_URL + "get_details_of_login_user.php";
    public static final String POST_UPDATE_REFERREL_NUMBER = NEW_BASE_URL + "update_referred_number.php";
    public static final String UPDATE_OE_PROFILE = NEW_BASE_URL + "oe_profile_update.php";

    // RSS related apis
    public static final String POST_RSS_UPDATE = NEW_BASE_URL + "update_rss_topics.php";
    public static final String POST_RSS_LIST = NEW_BASE_URL + "get_rss_feed.php";

    // InfoTab related apis
    public static final String GET_UNREAD_NOTIFICATIONS = BASE_URL + "get_all_unread_notification.php"; // WILL BE REPLACED BY api/v1/notifications/get_all_unread_notifications


    // URLS/APIS OTHER THAN MAIN BASE URL:
    // ===================================
    //public static final String CROP_CLASS_IMAGE_BASE_URL = "http://api.agriiprince.com/testing/all_images/crop_class/";



    // S3: URLs
    public static final String CROP_CLASS_IMAGE_BASE_URL = "https://farmerimage.s3.ap-south-1.amazonaws.com/crop_class/";
    public static final String CROP_IMAGE_BASE_URL = "https://farmerimage.s3.ap-south-1.amazonaws.com/crop_name_images/";
    public static final String DISEASE_IMAGE_BASE_URL = "https://cropdiseaseimages.s3.ap-south-1.amazonaws.com/diseased/";


}