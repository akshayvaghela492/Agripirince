package com.agriiprince.mvvm.applevel.analytics;

import com.agriiprince.utils.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

/**
 * Created by Suryakant Bharti on 03-02-2019.
 */
public class FarmerRealtimeDBAnalytics {

    public static void saveEventFarmerTimeSpent(DataSnapshot dataSnapshot, String timeEventName, DatabaseReference numRef, final int elapsedSeconds) {
        String snapshotStr = dataSnapshot.getValue().toString();
        if(snapshotStr.contains(timeEventName)==true) {
            String separator = timeEventName + "=";
            String splitArray[] = snapshotStr.split(separator);
            int valueToPut = Utils.extractInt(splitArray[1]);
            numRef.child(timeEventName).setValue(valueToPut + elapsedSeconds);
        }
        else
            numRef.child(timeEventName).setValue(elapsedSeconds);
    }

    public static void saveEventFarmerClicks(DataSnapshot dataSnapshot, String clickEventName, DatabaseReference numRef) {
        String snapshotStr = dataSnapshot.getValue().toString();
        if(snapshotStr.contains(clickEventName)==true) {
            String separator = clickEventName + "=";
            String splitArray[] = snapshotStr.split(separator);
            int valueToPut = Utils.extractInt(splitArray[1]);
            numRef.child(clickEventName).setValue(valueToPut + 1);
        }
        else
            numRef.child(clickEventName).setValue(1);
    }
}
