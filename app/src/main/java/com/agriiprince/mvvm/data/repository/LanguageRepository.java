package com.agriiprince.mvvm.data.repository;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.model.Language;
import com.agriiprince.mvvm.util.AssetsUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class LanguageRepository implements LanguageRepositoryUseCase{

    private static final String TAG = "LanguageRepository";

    private Context mContext;

    private PrefManager mPrefManager;

    private MutableLiveData<List<Language>> mLanguages;

    public LanguageRepository(Application context, PrefManager prefManager) {
        this.mContext = context;
        this.mPrefManager = prefManager;
        this.mLanguages = new MutableLiveData<>();

        loadLanguages();
    }

    private void loadLanguages() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String json = AssetsUtil.loadJSON(mContext.getApplicationContext(), AssetsUtil.ASSETS_LANGUAGE_JSON);
                List<Language> languages = new Gson().fromJson(json, new TypeToken<List<Language>>() {}.getType());

                String locale = mPrefManager.getLocale();
                for (Language language : languages) {
                    if (language.getLanguageCode().equalsIgnoreCase(locale)) {
                        language.setSelected(true);
                        break;
                    }
                }

                mLanguages.postValue(languages);
            }
        }).start();
    }


    @Override
    public MutableLiveData<List<Language>> getLanguages() {
        return mLanguages;
    }

    @Override
    public void updatePrimaryLocale(String primaryLocale) {
        mPrefManager.setPrimaryLocale(primaryLocale);
    }

    @Override
    public void updateSecondaryLocale(String secondaryLocale) {
        mPrefManager.setSecondaryLocale(secondaryLocale);
    }
}
