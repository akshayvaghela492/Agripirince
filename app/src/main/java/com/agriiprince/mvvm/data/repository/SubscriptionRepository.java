package com.agriiprince.mvvm.data.repository;

import android.arch.lifecycle.MutableLiveData;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.subscription.BplPhotoUpload;
import com.agriiprince.mvvm.retrofit.dto.subscription.CouponVerification;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionDetails;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionInfo;
import com.agriiprince.mvvm.model.User;
import com.agriiprince.mvvm.retrofit.model.subscription.SubscriptionDetailData;
import com.agriiprince.mvvm.retrofit.model.subscription.SubscriptionDetailList;
import com.agriiprince.mvvm.retrofit.service.Subscription;
import com.agriiprince.mvvm.retrofit.service.SubscriptionService;
import com.agriiprince.mvvm.retrofit.ResponseWrapper;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubscriptionRepository implements SubscriptionRepositoryUseCase {

    private static final String TAG = "SubscriptionRepository";

    private final int ON_BITMAP_CONVERT_TO_STRING = 0;

    private MutableLiveData<ResponseWrapper<BplPhotoUpload>> uploadResponse;

    private List<SubscriptionDetailList> subscriptions;

    private SubscriptionService mService;
    private User mUser;


    public SubscriptionRepository(SubscriptionService mService, User user) {
        this.mService = mService;
        this.mUser = user;
        this.uploadResponse = new MutableLiveData<>();
        subscriptions = new ArrayList<>();
    }

    @Override
    public MutableLiveData<ResponseWrapper<SubscriptionInfo>> getUserSubscription() {
        final MutableLiveData<ResponseWrapper<SubscriptionInfo>> result = new MutableLiveData<>();

        final ResponseWrapper<SubscriptionInfo> wrapper = new ResponseWrapper<>();
        result.setValue(wrapper);

        final StringRequest request = new StringRequest(Request.Method.POST, Config.POST_SUBSCRIPTION_INFORMATION,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_API", "onResponse: " + response);
                            //JSONArray jsonArray = new JSONArray(response);
                            //JSONObject jsonObject = jsonArray.getJSONObject(0);
                            //int error_code = jsonObject.getInt("error_code");
                            //if (error_code == 100)
                            //JSONObject jsonObject = new JSONObject();
                        //jsonObject = response;
                        JSONObject Object = null;
                        try {
                            Object = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (Object.getInt("code") == 200){
                                Log.d("TESTING_API", "code 200: ");
                                    //JSONObject data = jsonObject.getJSONArray("data").getJSONObject(0);
                                    //String subscribedOn = data.getString("subscription_expiry_date");
                                    //mUserProfile.setValidTill(DateUtil.getTimeForServerDate(subscribedOn));
                                    //JSONObject object=Object.getJSONObject("data");
                                   /* SubscriptionInfo subscriptionInfo = new Gson().fromJson(String.
                                            valueOf(Object),
                                            new TypeToken<SubscriptionInfo>() {
                                            }.getType());
                                            */
                                   Gson g = new Gson();
                                   SubscriptionInfo subscriptionInfo = g.fromJson(response,SubscriptionInfo.class);
                                Log.d("TESTING_API", "Gson() ");
                                Log.d("TESTING_API",subscriptionInfo.toString());
                                Log.d("TESTING_API",subscriptionInfo.getCode().toString());
                                Log.d("TESTING_API",subscriptionInfo.getMessage());
                                Log.d("TESTING_API",subscriptionInfo.getData().getData().get(0).getSubscription_start_date());

                                    wrapper.setData(subscriptionInfo);
                                }
                                else {
                                    wrapper.setError(new JsonParseException("parse error"));
                                }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        result.setValue(wrapper);
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("TESTING_API", "volley error : ");
                        wrapper.setError(error);
                        result.setValue(wrapper);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUser.getAccessToken());
                params.put("user_id", mUser.getUserId());
                params.put("user_type", mUser.getUserType());
                params.put("password", mUser.getPassword());
                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUser.getAccessToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(request);

        return result;

    }

    @Override
    public MutableLiveData<ResponseWrapper<List<SubscriptionDetailList>>> getSubscriptions() {
        final MutableLiveData<ResponseWrapper<List<SubscriptionDetailList>>> result = new MutableLiveData<>();

        final ResponseWrapper<List<SubscriptionDetailList>> responseWrapper = new ResponseWrapper<>();
        result.setValue(responseWrapper);

 /*       mService.getSubscription(mUser.getAccessToken(), mUser.getUserId(), mUser.getPassword(), "all")
                .enqueue(new Callback<List<ResponseWrapper<SubscriptionList>>>() {
                    @Override
                    public void onResponse(Call<List<ResponseWrapper<SubscriptionList>>> call, Response<List<ResponseWrapper<SubscriptionList>>> response) {
                        Log.d(TAG, "onResponse: getSubscriptions" + response.body());
                        if (response.body() != null && response.body().size() > 0
                                && response.body().get(0).getStatusCode() == 200) {

                            SubscriptionList subscriptionList = response.body().get(0).getData();

                            subscriptions = subscriptionList.getSubscriptions();

                            // check for premium and basic types
                            for (Subscription subscription : subscriptions) {
                                if (subscription.getSubscriptionName().equalsIgnoreCase("basic")) {
                                    subscription.setBasic(true);
                                } else if (subscription.getSubscriptionName().equalsIgnoreCase("premium")) {
                                    subscription.setBasic(false);
                                }
                            }

                            responseWrapper.setData(subscriptions);

                        } else {
                            responseWrapper.setError(new JsonParseException("parse error"));
                        }

                        result.setValue(responseWrapper);
                    }

                    @Override
                    public void onFailure(Call<List<ResponseWrapper<SubscriptionList>>> call, Throwable t) {
                        responseWrapper.setError(t);
                        result.setValue(responseWrapper);
                    }
                });
        return result;
        */
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.POST_SUBSCRIPTION_DETAILS,

                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_API","subs detail "+response);
                        try {
                            Gson g = new Gson();
                            SubscriptionDetails subscriptionDetails = g.fromJson(response, SubscriptionDetails.class);
                            if (subscriptionDetails.getCode() == 200) { //SubscriptionList subscriptionList = response.body().get(0).getData();
                                //subscriptions = subscriptionList.getSubscriptions();
                                SubscriptionDetailData detailList = subscriptionDetails.getData();
                                subscriptions = detailList.getData();

                                // check for premium and basic types
                                for (SubscriptionDetailList subscription : subscriptions) {
                                    if (subscription.getSubscriptionName().equalsIgnoreCase("basic")) {
                                        subscription.setBasic(true);
                                    } else if (subscription.getSubscriptionName().equalsIgnoreCase("premium")) {
                                        subscription.setBasic(false);
                                    }
                                }
                                Log.d("TESTING_API","subs detail "+subscriptionDetails.toString());
                                responseWrapper.setData(subscriptions);

                            } else {
                                responseWrapper.setError(new JsonParseException("parse error"));
                            }

                            result.setValue(responseWrapper);
                        }catch (Exception e){e.printStackTrace();}
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("TESTING_API","volley error subs detail ");
                        responseWrapper.setError(error);
                        result.setValue(responseWrapper);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUser.getAccessToken());

                params.put("user_id", mUser.getUserId());
                params.put("password", mUser.getPassword());
                params.put("mode", "all");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUser.getAccessToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
        return result;
    }

    @Override
    public void activateSubscription(int type) {

    }

    @Override
    public MutableLiveData<ResponseWrapper<CouponVerification>> applyCoupon(@NonNull final String coupon) {
        final MutableLiveData<ResponseWrapper<CouponVerification>> result = new MutableLiveData<>();

        final ResponseWrapper<CouponVerification> responseWrapper = new ResponseWrapper<>();
        result.setValue(responseWrapper);

  /*      mService.applyCouponCode(mUser.getAccessToken(), mUser.getUserId(), mUser.getUserType(), mUser.getPassword(),
                "check", coupon, " ")
                .enqueue(new Callback<List<SubscriptionCoupon>>() {
                    @Override
                    public void onResponse(Call<List<SubscriptionCoupon>> call, Response<List<SubscriptionCoupon>> response) {
                        Logs.d(TAG, "onResponse: " + response.body());
                        if (response.body() != null) {
                            responseWrapper.setData(response.body().get(0));
                        } else {
                            responseWrapper.setError(new JsonParseException("parse error"));
                        }

                        result.setValue(responseWrapper);
                    }

                    @Override
                    public void onFailure(Call<List<SubscriptionCoupon>> call, Throwable t) {
                        responseWrapper.setError(t);
                        result.setValue(responseWrapper);
                    }
                });
                */

        //Log.d("TESTING_API","coupon "+  mUser.getAccessToken() + mUser.getUserType() + );

        StringRequest verifyCouponRequest = new StringRequest(Request.Method.POST,
                Config.POST_SUBSCRIPTION_VERIFY_COUPON,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_API","coupon "+response);
                        Gson g = new Gson();
                        CouponVerification couponVerification = g.fromJson(response,CouponVerification.class);
                        Log.d("TESTING_API",couponVerification.getCode().toString());
                        if (couponVerification.getCode()==200)
                        {Log.d("TESTING_API","coupon code "+couponVerification.getCode());
                            //responseWrapper.setData(response.body().get(0));
                            responseWrapper.setData(couponVerification);
                        } else {
                            Log.d("TESTING_API","coupon else ");
                            responseWrapper.setError(new JsonParseException("parse error"));
                        }

                        result.setValue(responseWrapper);

                }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("TESTING_API","coupon volley error "+error);
                        responseWrapper.setError(error);
                        result.setValue(responseWrapper);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("api_token", mUser.getAccessToken());

                params.put("user_id",mUser.getUserId());
                params.put("user_type", mUser.getUserType());
                params.put("mode", "check");
                params.put("password", mUser.getPassword());
                params.put("coupon_code",coupon);
             //   if (etOeNumber.getText() != null)
              //      params.put("oe_mobile", etOeNumber.getText().toString());
              //  if (etCoupon.getText() != null)
               //     params.put("coupon_code", etCoupon.getText().toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUser.getAccessToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(verifyCouponRequest);
        return result;
    }


    @Override
    public MutableLiveData<ResponseWrapper<BplPhotoUpload>> uploadBplPhoto(@NonNull Bitmap bitmap) {
        new Thread(new RunnableBitmapToBase64(bitmap)).start();

        ResponseWrapper<BplPhotoUpload> responseWrapper = new ResponseWrapper<>();
        uploadResponse.setValue(responseWrapper);

        return uploadResponse;
    }

    private void onConvertUploadBplPhoto(String base64) {
        final ResponseWrapper<BplPhotoUpload> bplUploadPhoto = new ResponseWrapper<>();
        API_Manager apiManager=new API_Manager();
        Subscription retrofit_interface = apiManager.phpClient().create(Subscription.class);
        //mService.uploadBplImage(mUser.getAccessToken(), mUser.getPassword(), mUser.getUserId(), base64)
        //Log.d("TESTING_API","bpl --->"+response);

        retrofit_interface.uploadBplImage(mUser.getAccessToken(), mUser.getPassword(), mUser.getUserId(), base64)
                .enqueue(new Callback<BplPhotoUpload>() {
                    @Override
                    public void onResponse(Call<BplPhotoUpload> call, Response<BplPhotoUpload> response) {
                        Log.d("TESTING_API","bpl "+response);
                        if (response.body() != null) {
                            Log.d("TESTING_API","bpl code "+response.body().getCode());
                            bplUploadPhoto.setData(response.body());
                        } else {
                            bplUploadPhoto.setError(new JsonParseException("parse exception"));
                        }
                        uploadResponse.postValue(bplUploadPhoto);
                    }

                    @Override
                    public void onFailure(Call<BplPhotoUpload> call, Throwable t) {
                        Log.d("TESTING_API","bpl fail error "+t.getMessage());
                        bplUploadPhoto.setError(t);
                        uploadResponse.postValue(bplUploadPhoto);
                    }
                });
    }

    private class RunnableBitmapToBase64 implements Runnable {

        private Bitmap imageBitmap;

        private RunnableBitmapToBase64(@NonNull Bitmap bitmap) {
            this.imageBitmap = bitmap;
        }

        @Override
        public void run() {
            Bitmap bitmap = Bitmap.createScaledBitmap(imageBitmap, 600, 800, true);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 40, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();

            String image = Base64.encodeToString(byteArray, Base64.DEFAULT);

            onConvertUploadBplPhoto(image);
        }
    }


}
