package com.agriiprince.mvvm.data.repository;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.agriiprince.mvvm.applevel.annotation.UserType;
import com.agriiprince.mvvm.retrofit.dto.apkversions.ApkVers;
import com.agriiprince.mvvm.retrofit.dto.login.VersionUpdate;
import com.agriiprince.mvvm.retrofit.ResponseWrapper;

public interface VersionUpdateRepositoryUseCase {

    /**
     * check for application update and returns the response
     * @param apkVersion app version name
     * @param userType user type
     * @return version update response
     */
    MutableLiveData<ResponseWrapper<ApkVers>> checkForUpdate(String apkVersion, @UserType String userType);

    /**
     * this method will update the mutable live with download progress
     * @return string percentage of total size
     */
    MutableLiveData<String> onDownloadUpdateProgress();

    /**
     * Download of resource starts in the background thread and updates are dispatched though onDownloadUpdateProgress method.
     * Once the download is complete then the file path is returned
     * @param url resource url
     * @return String file location path
     */
    MutableLiveData<String> onDownloadUpdate(@NonNull String url);

}
