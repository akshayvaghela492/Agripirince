package com.agriiprince.mvvm.data.repository;

import android.arch.lifecycle.MutableLiveData;

import com.agriiprince.mvvm.model.Language;

import java.util.List;

public interface LanguageRepositoryUseCase {


    /**
     * returns the language models loaded from the json file
     * @return List of languages
     */
    MutableLiveData<List<Language>> getLanguages();


    /**
     * save the primary locale in shared preferences and update the server
     * @param primaryLocale locale code
     */
    void updatePrimaryLocale(String primaryLocale);


    /**
     * save the secondary locale and update the server
     * @param secondaryLocale locale code
     */
    void updateSecondaryLocale(String secondaryLocale);
}
