package com.agriiprince.mvvm.data.repository;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import com.agriiprince.mvvm.model.Contact;

import java.util.ArrayList;
import java.util.List;

public class ReferFriendsRepository implements ReferFriendsRepositoryUseCase {

    private static final String TAG = "ReferFriendsRepository";

    private MutableLiveData<List<Contact>> mContactsLiveData;

    private List<Contact> mContacts;

    private Context mContext;

    public ReferFriendsRepository(Context context) {
        mContext = context;
        this.mContactsLiveData = new MutableLiveData<>();
        this.mContacts = new ArrayList<>();
    }

    @Override
    public MutableLiveData<List<Contact>> getContacts() {
        new Thread(contactsRunnable).start();
        return mContactsLiveData;
    }


    @Override
    public void selectAllContacts() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (Contact contact : mContacts) {
                    if (! contact.isSelected()) contact.setSelected(true);
                }
            }
        }).start();
    }

    @Override
    public void unSelectAllContacts() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (Contact contact : mContacts) {
                    if (contact.isSelected()) contact.setSelected(false);
                }
            }
        }).start();
    }

    private Runnable contactsRunnable = new Runnable() {
        @Override
        public void run() {
            Uri uri = ContactsContract.Contacts.CONTENT_URI; // Contact URI
            Cursor contactsCursor = mContext.getContentResolver().query(uri, null, null,
                    null, ContactsContract.Contacts.DISPLAY_NAME + " ASC "); // Return all contacts ids

            if (contactsCursor != null && contactsCursor.moveToFirst()) {
                do {
                    String contactId = String.valueOf(contactsCursor.getLong(contactsCursor
                            .getColumnIndex("_ID"))); // Get contact ID

                    String displayName = contactsCursor
                            .getString(contactsCursor
                                    .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                    String photoUri = contactsCursor.getString(contactsCursor.getColumnIndex(ContactsContract.Contacts.PHOTO_URI));

                    int totalNumber = Integer.parseInt(contactsCursor
                            .getString(contactsCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));

                    if (totalNumber > 0) {
                        Uri dataUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI; // URI to get data of contacts

                        Cursor phoneCursor = mContext.getContentResolver().query(dataUri, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId,
                                null, null);// Retrun data cusror represntative to
                        if (phoneCursor != null && phoneCursor.moveToFirst()) {

                            do {
                                String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                                phoneNumber = phoneNumber
                                        .replace("-", "")
                                        .replace(" ", "").trim();

                                if (phoneNumber.length() >= 10) {
                                    Contact contact = new Contact(contactId, displayName, phoneNumber, photoUri);
                                    mContacts.add(contact);
                                }
                            } while (phoneCursor.moveToNext());

                            phoneCursor.close();
                        }
                    }
                } while (contactsCursor.moveToNext());

                contactsCursor.close();
            }

            mContactsLiveData.postValue(mContacts);
        }
    };
}
