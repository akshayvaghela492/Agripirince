package com.agriiprince.mvvm.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.agriiprince.mvvm.applevel.annotation.LanguageCode;
import com.agriiprince.mvvm.model.Language;

import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_PRIMARY_LOCALE;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_SECONDARY_LOCALE;

public class PreferenceManager {

    private final String PREFERENCE_NAME = "ap_preferences";

    private final int PRIVATE_MODE = 0;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @SuppressWarnings("CommitPrefEdits")
    public PreferenceManager(Context context) {
        this.sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, PRIVATE_MODE);
        this.editor = sharedPreferences.edit();
    }

    public void setPrimaryLocale(@LanguageCode @NonNull String locale) {
        editor.putString(KEY_PRIMARY_LOCALE, locale);
        editor.apply();
    }

    public @LanguageCode String getPrimaryLocale() {
        return sharedPreferences.getString(KEY_PRIMARY_LOCALE, Language.Code.ENGLISH);
    }

    public void setSecondaryLocale(@LanguageCode String locale) {
        editor.putString(KEY_SECONDARY_LOCALE, locale);
        editor.apply();
    }

    public @LanguageCode String getSecondaryLocale() {
        return sharedPreferences.getString(KEY_PRIMARY_LOCALE, null);
    }

}
