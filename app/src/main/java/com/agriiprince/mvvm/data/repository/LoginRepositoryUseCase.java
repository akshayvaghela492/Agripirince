package com.agriiprince.mvvm.data.repository;

import android.arch.lifecycle.MutableLiveData;

import com.agriiprince.mvvm.applevel.annotation.UserType;
import com.agriiprince.mvvm.retrofit.dto.login.CheckUserExist;
import com.agriiprince.mvvm.retrofit.dto.login.Login;
import com.agriiprince.mvvm.retrofit.dto.login.TokenGenerate;
import com.agriiprince.mvvm.retrofit.dto.login.UserExist;
import com.agriiprince.mvvm.retrofit.ResponseWrapper;
import com.agriiprince.mvvm.retrofit.dto.oe.OeLoginResponse;

public interface LoginRepositoryUseCase {

    /**
     * returns check user exists dto for new user
     * @param userType user type
     * @param mobileNumber mobile number for registration
     * @return {@link CheckUserExist} mutable live of check user exist wrapped in {@link ResponseWrapper}
     */
    MutableLiveData<ResponseWrapper<UserExist>> checkUserExists(@UserType String userType, String mobileNumber);

    /**
     * login the user. observe the method onLogin for results
     * @param userType user type
     * @param mobile mobile number
     * @param password user password
     * @param loginMode otp or password
     */
    void login(String userType, String mobile, String password, String loginMode);

    /**
     * observe this data for login results
     * @return {@link Login} login user DTO
     */
    MutableLiveData<ResponseWrapper<Login>> onLogin();

    MutableLiveData<ResponseWrapper<OeLoginResponse>> onLoginOe();


    /**
     * new access token is generated
     * @return {@link TokenGenerate}
     */
    MutableLiveData<ResponseWrapper<TokenGenerate>> generateAccessToken();

    /**
     * upload the fcm token to server
     * @param fcmToken
     */
    void updateFcmToken(String fcmToken);

}
