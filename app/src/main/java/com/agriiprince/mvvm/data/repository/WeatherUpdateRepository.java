package com.agriiprince.mvvm.data.repository;

import android.arch.lifecycle.MutableLiveData;

import com.agriiprince.mvvm.model.WeatherModel;

import java.util.ArrayList;
import java.util.List;

public class WeatherUpdateRepository {
private ArrayList<WeatherModel> weatherModels=new ArrayList<>();

public MutableLiveData<List<WeatherModel>> weather(){
    MutableLiveData<List<WeatherModel>> data=new MutableLiveData<>();
    data.setValue(weatherModels);
    return data;
}
}
