package com.agriiprince.mvvm.data.repository;

import android.arch.lifecycle.MutableLiveData;

import com.agriiprince.mvvm.retrofit.model.weather.WeatherDaily;
import com.agriiprince.mvvm.retrofit.ResponseWrapper;

public interface WeatherUpdateRepositoryUsecase {


    MutableLiveData<ResponseWrapper<WeatherDaily>> w(String date, String city);


}
