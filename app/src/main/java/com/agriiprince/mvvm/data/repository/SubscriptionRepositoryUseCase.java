package com.agriiprince.mvvm.data.repository;

import android.arch.lifecycle.MutableLiveData;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.agriiprince.mvvm.retrofit.dto.subscription.BplPhotoUpload;
import com.agriiprince.mvvm.retrofit.dto.subscription.BplUploadPhoto;
import com.agriiprince.mvvm.retrofit.dto.subscription.CouponVerification;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionInfo;
import com.agriiprince.mvvm.retrofit.dto.subscription.SubscriptionCoupon;
import com.agriiprince.mvvm.retrofit.ResponseWrapper;
import com.agriiprince.mvvm.retrofit.model.subscription.SubscriptionDetailList;

import java.util.List;

public interface SubscriptionRepositoryUseCase {

    MutableLiveData<ResponseWrapper<SubscriptionInfo>> getUserSubscription();

    MutableLiveData<ResponseWrapper<List<SubscriptionDetailList>>> getSubscriptions();

    void activateSubscription(int type);

    MutableLiveData<ResponseWrapper<CouponVerification>> applyCoupon(@NonNull String coupon);

    MutableLiveData<ResponseWrapper<BplPhotoUpload>> uploadBplPhoto(@NonNull Bitmap bitmap);

}
