package com.agriiprince.mvvm.data.prefs;

import android.content.Context;

import com.facebook.accountkit.AccountKit;
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;

public class UserProfile {

    private Context mContext;
    private PrefManager prefManager;


    public UserProfile(Context context) {
        this.mContext = context;
        this.prefManager = new PrefManager(context);
    }

    public void setPassword(String pass) {
        prefManager.setUserPassword(pass);
    }

    public void setContact(String mob) {
        prefManager.setMobileNo(mob);
    }

    public boolean isLogedin() {
        return getApiToken() != null;
    }

    public String getUserName() {
        return prefManager.getUserName();
    }

    public void setUserName(String name) {
        prefManager.setUserName(name);
    }

    public void setApiToken(String token) {
        prefManager.setAccessToken(token);
    }

    public String getApiToken() {
        return prefManager.getAccessToken();
    }

    public String getUserPassword() {
        return prefManager.getUserPassword();
    }

    public String getUserFcm() {
        return prefManager.getFCMToken();
    }

    public String getUserCity() {
        return prefManager.getCity();
    }

    public String getUserPincode() {
        return prefManager.getPinCode();
    }

    public String getUserType() {
        return prefManager.getUserType();
    }

    public String getUserMobile() {
        return prefManager.getMobileNo();
    }

    public void setNewUser(boolean isNewUser) {
        prefManager.setNewUser(isNewUser);
    }

    public boolean isNewUser() {
        return prefManager.isNewUser();
    }

    public String getUserId() {
        return prefManager.getUserId();
    }

    public long getInfoTabLastLoadTime() {
        return prefManager.getInfoTabLastLoadTime();
    }

    public void setInfoTabLastLoadTime(long time) {
        prefManager.setInfoTabLastLoadTime(time);
    }

    public void setValidTill(long validTill) {
        prefManager.setValidityTime(validTill);
    }

    public long getValidTill() {
        return prefManager.getValidityTime();
    }

    public void setCreatedOn(long time) {
        prefManager.setCreatedOn(time);
    }

    public long getCreatedOn() {
        return prefManager.getCreatedOn();
    }

    public void setInterestedMandiIds(String ids) {
        prefManager.setInterestedMandiIds(ids);
    }

    public String getInterestedMandiIds() {
        return prefManager.getInterestedMandiIds();
    }
    public void setInterestedCropIds(String ids) {
        prefManager.setInterestedCropIds(ids);
    }

    public String getInterestedCropIds() {
        return prefManager.getInterestedCropIds();
    }

    public void logOut() {
        AccountKit.logOut(); // logout fb account kit
        PayUmoneyFlowManager.logoutUser(mContext); // logout pay user login

        prefManager.logOut();
    }
}
