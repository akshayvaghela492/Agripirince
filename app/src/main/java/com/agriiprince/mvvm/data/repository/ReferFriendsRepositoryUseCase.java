package com.agriiprince.mvvm.data.repository;

import android.arch.lifecycle.MutableLiveData;

import com.agriiprince.mvvm.model.Contact;

import java.util.List;

public interface ReferFriendsRepositoryUseCase {

    /**
     * list of contacts
     * @return
     */
    MutableLiveData<List<Contact>> getContacts();


    void selectAllContacts();

    void unSelectAllContacts();

}
