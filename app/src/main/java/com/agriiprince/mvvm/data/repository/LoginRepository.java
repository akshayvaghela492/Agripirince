package com.agriiprince.mvvm.data.repository;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;
import android.widget.Toast;

import com.agriiprince.BuildConfig;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.retrofit.dto.login.FcmTokenUpdate;
import com.agriiprince.mvvm.retrofit.dto.login.Login;
import com.agriiprince.mvvm.retrofit.dto.login.LoginUser;
import com.agriiprince.mvvm.retrofit.dto.login.TokenGenerate;
import com.agriiprince.mvvm.retrofit.dto.login.UserExist;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.oe.OeLoginResponse;
import com.agriiprince.mvvm.retrofit.service.LoginService;
import com.agriiprince.mvvm.retrofit.service.OE;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.mvvm.model.User;
import com.agriiprince.mvvm.retrofit.ResponseWrapper;
import com.agriiprince.mvvm.util.Logs;
import com.google.gson.JsonParseException;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.agriiprince.appcontroller.AppController.loggedOut;

public class LoginRepository implements LoginRepositoryUseCase {//

    private static final String TAG = "LoginRepository";

    private MutableLiveData<ResponseWrapper<Login>> mUserLogin;
    private MutableLiveData<ResponseWrapper<OeLoginResponse>> mUserLoginOe;

    private LoginService mLoginService;

    private User mUser;

    public LoginRepository(LoginService loginService, User user) {
        this.mLoginService = loginService;
        this.mUser = user;
        this.mUserLogin = new MutableLiveData<ResponseWrapper<Login>>();
        this.mUserLoginOe = new MutableLiveData<ResponseWrapper<OeLoginResponse>>();
    }

    API_Manager apiManager=new API_Manager();

    @Override
    public MutableLiveData<ResponseWrapper<UserExist>> checkUserExists(String userType, String mobileNumber) {
        final MutableLiveData<ResponseWrapper<UserExist>> result = new MutableLiveData<>();

        final ResponseWrapper<UserExist> responseWrapper = new ResponseWrapper<>();
        result.setValue(responseWrapper);

        UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);
        Call<UserExist> call = retrofit_interface.userExist(mobileNumber, "farmer");
        call.enqueue(new Callback<UserExist>() {
            @Override
            public void onResponse(Call<UserExist> call, Response<UserExist> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code userExist ");
                    Logs.d("CheckStatus", response.body().getStatus());
                    Logs.d("CheckStatus", response.body().getMessage());
                    Logs.d("CheckStatus", response.body().getCode().toString());

                    UserExist userExist = response.body();
                    responseWrapper.setData(userExist);
                    //"User Already Exists"

                } catch (Exception e) {
                    e.getMessage();
                    responseWrapper.setError(new JsonParseException("response parsing error"));
                }
                result.setValue(responseWrapper);
            }

            @Override
            public void onFailure(Call<UserExist> call, Throwable t) {
                Log.d("CheckStatus", "user exist " + t.getMessage());
                responseWrapper.setError(t);
                result.setValue(responseWrapper);
            }
        });
        return result;

    }

    @Override
    public void login(final String userType, String mobile, String password, final String loginMode) {

        String loginType = " ";
        if(loginMode.equals("with_otp")) {
            loginType = "with_otp";
            password = " ";
        }

        if (BuildConfig.USER_TYPE.equals(User.Type.FARMER)) {

            final ResponseWrapper<Login> responseWrapper = new ResponseWrapper<>();

            UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);
            Call<Login> call = retrofit_interface.Login(mobile,password,userType.toLowerCase(), " ", loginType);
            call.enqueue(new Callback<Login>() {
                @Override
                public void onResponse(Call<Login> call, Response<Login> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        try {

                            Logs.d("CheckStatus NEWLOG", "onResponse: code Login ");
                            Logs.d("CheckStatus NEWLOG", response.body().getCode().toString());
                            Logs.d("CheckStatus NEWLOG", response.body().getMessage());
                            Logs.d("CheckStatus NEWLOG", response.body().getStatus().toString());
                            Logs.d("CheckStatus NEWLOG", response.body().getData().getFarmerName());

                            Login loginUser = new Login();
                            loginUser.setCode(response.body().getCode());

                            //int errorCode = responseObject.getInt("error_code");
                            //loginUser.errorCode = errorCode;

                            Log.d("NEW LOGIN ->", loginUser.getCode()+"  "+userType);
                            if (loginUser.getCode() == 200) {

                                //JSONObject jsonObject = new JSONObject(response.body().getData());

                                //JSONArray responseArray = new JSONArray(response.body().getData());
                                //JSONObject responseObject = responseArray.getJSONObject(0);
                                //mUser.saveUserLoginData(userType, responseObject);

                                //JSONArray dataArray = responseObject.getJSONArray("data");
                                //mUser.saveUserLoginData(userType, dataArray.getJSONObject(0));

                                mUser.saveUserLoginDataFromModel(userType, response.body().getData());

                                responseWrapper.setData(loginUser);

                            } else {
                                responseWrapper.setError(new JsonParseException("parse error 1"));
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        responseWrapper.setError(new JsonParseException("parse error 2"));
                    }

                    mUserLogin.setValue(responseWrapper);
                }

                @Override
                public void onFailure(Call<Login> call, Throwable t) {
                    responseWrapper.setError(t);
                    mUserLogin.setValue(responseWrapper);
                }
            });

            mUserLogin.setValue(responseWrapper);
        }


        else if (BuildConfig.USER_TYPE.equals(User.Type.OPERATIVE_EXECUTIVE)) {
            final ResponseWrapper<OeLoginResponse> responseWrapper = new ResponseWrapper<>();

            OE retrofit_interface = apiManager.getClient8().create(OE.class);
            Call<OeLoginResponse> call = retrofit_interface.OeLogin(mobile,password,"oe"," ", loginType);
            call.enqueue(new Callback<OeLoginResponse>() {
                @Override
                public void onResponse(Call<OeLoginResponse> call, Response<OeLoginResponse> response) {
                    Logs.d("CheckStatus", "success oe login "+response);
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                        Logs.d("CheckStatus",response.body().getStatus().toString());
                        Logs.d("CheckStatus",response.body().getCode().toString());
                        Logs.d("CheckStatus",response.body().getMessage());
                        Logs.d("CheckStatus",response.body().getData().getOeName());

                        OeLoginResponse loginUser = new OeLoginResponse();
                        Logs.d("CheckStatus", " ----> 1");

                        loginUser.setCode(response.body().getCode());
                            Logs.d("CheckStatus", " ----> 2");

                        Log.d("NEW LOGIN ->", loginUser.getCode()+"  "+userType);
                        if (loginUser.getCode() == 200) {
                            Logs.d("CheckStatus", " ----> 3");

                            mUser.saveUserLoginDataFromModelOe(userType, response.body().getData());
                            Logs.d("CheckStatus", " ----> 4");

                            responseWrapper.setData(loginUser);
                            Logs.d("CheckStatus", " ----> 5");

                        } else {
                            Logs.d("CheckStatus", " ----> 6");
                            responseWrapper.setError(new JsonParseException("parse error 1"));
                            Logs.d("CheckStatus", " ----> 7");
                        }
                        } catch (Exception e) {
                            Logs.d("CheckStatus", " ----> 8");
                            e.printStackTrace();
                        }
                    } else {
                        Logs.d("CheckStatus", " ----> 9");
                        responseWrapper.setError(new JsonParseException("parse error 2"));
                        Logs.d("CheckStatus", " ----> 10");
                    }
                    Logs.d("CheckStatus", " ----> 11");
                    mUserLoginOe.setValue(responseWrapper);
                    Logs.d("CheckStatus", " ----> 12");
                }
                @Override
                public void onFailure(Call<OeLoginResponse> call, Throwable t) {
                    Log.d("CheckStatus", "fail oe login "+ t.getMessage());
                    responseWrapper.setError(t);
                    mUserLoginOe.setValue(responseWrapper);
                    Logs.d("CheckStatus", " ----> 13");
                }
            });
            Logs.d("CheckStatus", " ----> 14");
            mUserLoginOe.setValue(responseWrapper);
            Logs.d("CheckStatus", " ----> 15");
        }


    }

    @Override
    public MutableLiveData<ResponseWrapper<Login>> onLogin() {
        return mUserLogin;
    }

    @Override
    public MutableLiveData<ResponseWrapper<OeLoginResponse>> onLoginOe() {
        return mUserLoginOe;
    }


    @Override
    public MutableLiveData<ResponseWrapper<TokenGenerate>> generateAccessToken() {

        final MutableLiveData<ResponseWrapper<TokenGenerate>> result = new MutableLiveData<>();

        Log.d("LOGGING_IN ---> ", " do something man");
        if(loggedOut == 0) {

            final ResponseWrapper<TokenGenerate> responseWrapper = new ResponseWrapper<>();

            UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);
            Call<TokenGenerate> call = retrofit_interface.generateToken(mUser.getUserId(), mUser.getUserType().toLowerCase(), mUser.getPassword());
            call.enqueue(new Callback<TokenGenerate>() {
                @Override
                public void onResponse(Call<TokenGenerate> call, Response<TokenGenerate> response) {
                    if (response.isSuccessful() && response.body() != null) {

                        Logs.d("CheckStatus NEWLOG", "onResponse: code token ");
                        Logs.d("CheckStatus NEWLOG", String.valueOf(response.body().getStatus()));
                        Logs.d("CheckStatus NEWLOG", response.body().getMessage());
                        Logs.d("CheckStatus NEWLOG", response.body().getData().getToken());

                        TokenGenerate authToken = response.body();
                        mUser.setAccessToken(authToken.getData().getToken());

                        responseWrapper.setData(authToken);
                    } else {
                        responseWrapper.setError(new JsonParseException("parse error"));
                    }

                    result.setValue(responseWrapper);
                }

                @Override
                public void onFailure(Call<TokenGenerate> call, Throwable t) {
                    responseWrapper.setError(t);
                    result.setValue(responseWrapper);
                }
            });

            result.setValue(responseWrapper);
        }
        else
        {
            final ResponseWrapper<TokenGenerate> responseWrapper = new ResponseWrapper<>();
            responseWrapper.setError(new Exception("Logout"));
            result.setValue(responseWrapper);
        }
            return result;
    }

    @Override
    public void updateFcmToken(String fcmToken) {
        UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);
        Call<FcmTokenUpdate> call = retrofit_interface.FcmTokenUpdate(mUser.getAccessToken(),mUser.getUserType(),mUser.getUserId(),fcmToken);
        call.enqueue(new Callback<FcmTokenUpdate>() {
            @Override
            public void onResponse(Call<FcmTokenUpdate> call, Response<FcmTokenUpdate> response) {
                Logs.d(TAG, "onResponse: updateFcmToken");
                if (response.isSuccessful() && response.body() != null) {
                    try {
                        Logs.d(TAG, "onResponse: " + response.body().getMessage());
                    }catch (Exception e){e.printStackTrace();}
                }
            }
            @Override
            public void onFailure(Call<FcmTokenUpdate> call, Throwable t) {
                Logs.d(TAG, "onFailure: updateFcmToken " + t.getMessage());
            }
        });
    }
   /*     mLoginService.updateFcmToken(mUser.getUserId(), mUser.getUserType(), mUser.getAccessToken(), fcmToken)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Logs.d(TAG, "onResponse: updateFcmToken");
                        if (response.isSuccessful() && response.body() != null) {
                            try {
                                Logs.d(TAG, "onResponse: " + response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Logs.d(TAG, "onFailure: updateFcmToken " + t.getMessage());
                    }
                });
                }
                */
}
