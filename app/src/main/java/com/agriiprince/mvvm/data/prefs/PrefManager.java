package com.agriiprince.mvvm.data.prefs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.util.Logs;

import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_CHECKPRICING_SHOW_DIALOG;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_HARVEST_AREA;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_HARVEST_AREA_UNIT;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_HARVEST_COST;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_HARVEST_YIELD;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_HARVEST_YIELD_UNIT;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_OTHER_COST;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_OTHER_DISTANCE;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_OTHER_DISTANCE_UNIT;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_OTHER_FIXED;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_TRANSPORT_COST;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_TRANSPORT_DISTANCE;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_TRANSPORT_DISTANCE_UNIT;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_TRANSPORT_WEIGHT;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_TRANSPORT_WEIGHT_UNIT;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_WASTE_DISTANCE;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_WASTE_DISTANCE_UNIT;
import static com.agriiprince.mvvm.applevel.constants.PreferenceKeys.KEY_WASTE_PERCENT_INDEX;


/**
 * Created by V1k on 30-Oct-17.
 */

public class PrefManager {

    private static final String TAG = PrefManager.class.getSimpleName();

    //Shared preference name
    private static final String PREF_NAME = "miscos_coach_inspector";
    // Shared pref mode
    private static int PRIVATE_MODE = 0;
    // Shared Preferences
    SharedPreferences pref;
    // Editor for Shared preferences
    SharedPreferences.Editor editor;
    // Context
    private Context _context;
    //Key
    private String KEY_NEW_USER = "key_new_user";
    private String KEY_FILLED_DEATAILS = "key_filled_details";

    private String KEY_USER_ID = "user_id";

    private String KEY_USER_TYPE = "key_user_type";
    private String KEY_FCM_TOKEN = "key_fcm_token";
    private String KEY_LOCALE = "key_locale";
    private String KEY_SECONDARY_LOCALE = "key_secondary_locale";
    private String KEY_USER_NAME = "key_user_name";
    private String KEY_CITY = "key_city";
    private String KEY_INVITE_CODE = "key_invite_code";
    private String KEY_PIN_CODE = "key_pin_code";
    private String KEY_ASK_PWD = "key_ask_pwd";
    private String KEY_MOBILE_NO = "key_mobile_no";
    private String KEY_USER_PASSWORD = "key_user_password";
    private String KEY_RSS_TOPICS = "key_rss_topics";
    private String KEY_FARMER_PROFILE = "key_farmer_profile";
    private String KEY_VALIDITY_TIME = "key_validity_time";
    private String KEY_CREATED_ON = "key_created_on";
    private String KEY_INFO_TAB_LAST_LOAD_TIME = "info_tab_last_load_time";
    private String KEY_INTERESTED_CROP_ID = "interested_crop_id";
    private String KEY_INTERESTED_MANDI_ID = "interested_mandi_id";

    private static final String KEY_ACCESS_TOKEN = "access_token";

    @SuppressLint("CommitPrefEdits")
    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setUserName(String userName) {
        editor.putString(KEY_USER_NAME, userName);
        editor.apply();
    }

    public String getUserName() {
        return pref.getString(KEY_USER_NAME, "");
    }

    public void setInterestedCropIds(String ids) {
        editor.putString(KEY_INTERESTED_CROP_ID, ids);
        editor.commit();
    }

    public String getInterestedCropIds() {
        return pref.getString(KEY_INTERESTED_CROP_ID, "");
    }

    public void setInterestedMandiIds(String ids) {
        editor.putString(KEY_INTERESTED_MANDI_ID, ids);
        editor.commit();
    }

    public String getInterestedMandiIds() {
        return pref.getString(KEY_INTERESTED_MANDI_ID, "");
    }

    public void setAccessToken(String token) {
        editor.putString(KEY_ACCESS_TOKEN, token);
        editor.apply();

        Logs.d("sharedPref", token);
    }

    public String getAccessToken() {
        return pref.getString(KEY_ACCESS_TOKEN, "");

    }

    public String getUserId() {
        return pref.getString(KEY_USER_ID, null);
    }

    public void setUserId(String userId) {
        editor.putString(KEY_USER_ID, userId);
        editor.apply();
    }


    public String getUserType() {
        return pref.getString(KEY_USER_TYPE, null);
    }

    public void setUserType(String s) {
        editor.putString(KEY_USER_TYPE, s);
        editor.commit();
    }

    public String getFCMToken() {
        return pref.getString(KEY_FCM_TOKEN, null);
    }

    public void setFCMToken(String s) {
        editor.putString(KEY_FCM_TOKEN, s);
        editor.commit();
    }

    public String getLocale() {
        return pref.getString(KEY_LOCALE, "en");
    } //edited 4/7/18 //edit 14/7/18

    public void setPrimaryLocale(String loc) {
//        Log.d("language:    ", loc != null ? loc : " null ");
        editor.putString(KEY_LOCALE, loc);
        editor.commit();

        AppController.getInstance().onPrimaryLocaleChange(loc);
    }

    public String getSecondaryLocale() {
        return pref.getString(KEY_SECONDARY_LOCALE, null);
    }

    public void setSecondaryLocale(String loc) {
//        Log.d("secondary language:    ", loc != null ? loc : " null ");
        editor.putString(KEY_SECONDARY_LOCALE, loc);
        editor.commit();

        AppController.getInstance().onSecondaryLocaleChange(loc);
    }

    public String getCity() {
        return pref.getString(KEY_CITY, null);
    }

    public void setCity(String city) {
        editor.putString(KEY_CITY, city);
        editor.commit();
    }

    public String getInviteCode() {
        return pref.getString(KEY_INVITE_CODE, null);
    }

    public void setInviteCode(String inviteCode) {
        editor.putString(KEY_INVITE_CODE, inviteCode);
        editor.commit();
    }

    public String getPinCode() {
        return pref.getString(KEY_PIN_CODE, null);
    }

    public void setPinCode(String inviteCode) {
        editor.putString(KEY_PIN_CODE, inviteCode);
        editor.commit();
    }

    public boolean getAskPWD() {
        return pref.getBoolean(KEY_ASK_PWD, false);
    }

    public void setAskPWD(boolean status) {
        editor.putBoolean(KEY_ASK_PWD, status);
        editor.commit();
    }

    public boolean getAskForSubscription() {
        return pref.getBoolean("key_ask_subscription", false);
    }

    public void setAskForSubscription(boolean b) {
        editor.putBoolean("key_ask_subscription", b);
        editor.commit();
    }

    private String KEY_SEARCH_COUNT = "key_search_count";

    public int getSearchCount() {
        return pref.getInt(KEY_SEARCH_COUNT, 0);
    }

    public void setSearchCount(int countOfSearch) {
        editor.putInt(KEY_SEARCH_COUNT, countOfSearch);
        editor.commit();
    }

    public String getRSStopics() {
        return pref.getString(KEY_RSS_TOPICS, null);
    }

    public void setRSStopics(String s) {
        editor.putString(KEY_RSS_TOPICS, s);
        editor.commit();
    }

    public void setValidityTime(long validityTime) {
        Log.d(TAG, "setValidityTime: " + validityTime);
        editor.putLong(KEY_VALIDITY_TIME, validityTime);
        editor.commit();
    }

    public long getValidityTime() {
        return pref.getLong(KEY_VALIDITY_TIME, 0);
    }

    public void setCreatedOn(long validityTime) {
        editor.putLong(KEY_CREATED_ON, validityTime);
        editor.commit();
    }

    public long getCreatedOn() {
        return pref.getLong(KEY_CREATED_ON, 0);
    }

    public String getMobileNo() {
        return pref.getString(KEY_MOBILE_NO, "");
    }

    public void setMobileNo(String mobileNo) {
        editor.putString(KEY_MOBILE_NO, mobileNo);
        editor.commit();
    }

    public String getUserPassword() {
        return pref.getString(KEY_USER_PASSWORD, "");
    }

    public void setUserPassword(String mobileNo) {
        editor.putString(KEY_USER_PASSWORD, mobileNo);
        editor.commit();
    }

    public String getFarmerProfile() {
        return pref.getString(KEY_FARMER_PROFILE, null);
    }

    public void setFarmerProfile(String farmerProfile) {
        editor.putString(KEY_FARMER_PROFILE, farmerProfile);
        editor.commit();
    }


    public void setNewUser(boolean isNewUser) {
        editor.putBoolean(KEY_NEW_USER, isNewUser);
        editor.commit();
    }

    public void setFilledDetails(boolean hasFilledDetails) {
        editor.putBoolean(KEY_FILLED_DEATAILS, hasFilledDetails);
        editor.commit();
    }

    public boolean getFilledDetails() {
            return pref.getBoolean(KEY_FILLED_DEATAILS, false);
    }

    public void setInfoTabLastLoadTime(long time) {
        editor.putLong(KEY_INFO_TAB_LAST_LOAD_TIME, time);
        editor.commit();
    }

    public long getInfoTabLastLoadTime() {
        return pref.getLong(KEY_INFO_TAB_LAST_LOAD_TIME, -1);
    }

    public boolean isNewUser() {
        return pref.getBoolean(KEY_NEW_USER, false);
    }


    public void logOut() {
        editor.clear();
        editor.commit();
    }


    public void setHarvestArea(String area) {
        editor.putString(KEY_HARVEST_AREA, area);
        editor.apply();
    }

    public String getHarvestArea() {
        return pref.getString(KEY_HARVEST_AREA, "0");
    }

    public void setHarvestAreaUnit(int position) {
        editor.putInt(KEY_HARVEST_AREA_UNIT, position);
        editor.apply();
    }

    public int getHarvestAreaUnit() {
        return pref.getInt(KEY_HARVEST_AREA_UNIT, 0);
    }

    public void setHarvestYieldUnit(int position) {
        editor.putInt(KEY_HARVEST_YIELD_UNIT, position);
        editor.apply();
    }

    public int getHarvestYieldUnit() {
        return pref.getInt(KEY_HARVEST_YIELD_UNIT, 0);
    }

    public void setHarvestYield(String yield) {
        editor.putString(KEY_HARVEST_YIELD, yield);
        editor.apply();
    }

    public String getHarvestYield() {
        return pref.getString(KEY_HARVEST_YIELD, "0");
    }

    public void setHarvestCost(String cost) {
        editor.putString(KEY_HARVEST_COST, cost);
        editor.apply();
    }

    public void setShowDialogFlag(int flag) {
        editor.putInt(KEY_CHECKPRICING_SHOW_DIALOG, flag);
        editor.apply();
    }

    public int getShowDialogFlag()
    {
        return pref.getInt(KEY_CHECKPRICING_SHOW_DIALOG, 1);
    }

    public String getHarvestCost() {
        return pref.getString(KEY_HARVEST_COST, "0");
    }

    public void setTransportWeightUnit(int position) {
        editor.putInt(KEY_TRANSPORT_WEIGHT_UNIT, position);
        editor.apply();
    }

    public int getTransportWeightUnit() {
        return pref.getInt(KEY_TRANSPORT_WEIGHT_UNIT, 0);
    }

    public void setTransportWeight(String weight) {
        editor.putString(KEY_TRANSPORT_WEIGHT, weight);
        editor.apply();
    }

    public String getTransportWeight() {
        return pref.getString(KEY_TRANSPORT_WEIGHT, "0");
    }

    public void setTransportDistanceUnit(int position) {
        editor.putInt(KEY_TRANSPORT_DISTANCE_UNIT, position);
        editor.apply();
    }

    public int getTransportDistanceUnit() {
        return pref.getInt(KEY_TRANSPORT_DISTANCE_UNIT, 0);
    }

    public void setTransportDistance(String distance) {
        editor.putString(KEY_TRANSPORT_DISTANCE, distance);
        editor.apply();
    }

    public String getTransportDistance() {
        return pref.getString(KEY_TRANSPORT_DISTANCE, "0");
    }

    public void setTransportCost(String cost) {
        editor.putString(KEY_TRANSPORT_COST, cost);
        editor.apply();
    }

    public String getTransportCost() {
        return pref.getString(KEY_TRANSPORT_COST, "0");
    }

    public void setWastePercentIndex(int position) {
        editor.putInt(KEY_WASTE_PERCENT_INDEX, position);
        editor.apply();
    }

    public int getWastePercentIndex() {
        return pref.getInt(KEY_WASTE_PERCENT_INDEX, 0);
    }

    public void setWasteDistanceUnit(int position) {
        editor.putInt(KEY_WASTE_DISTANCE_UNIT, position);
        editor.apply();
    }

    public int getWasteDistanceUnit() {
        return pref.getInt(KEY_WASTE_DISTANCE_UNIT, 0);
    }

    public void setWasteDistance(String distance) {
        editor.putString(KEY_WASTE_DISTANCE, distance);
        editor.apply();
    }

    public String getWasteDistance() {
        return pref.getString(KEY_WASTE_DISTANCE, "0");
    }

    public void setOtherFixed(boolean value) {
        editor.putBoolean(KEY_OTHER_FIXED, value);
        editor.apply();
    }

    public boolean getOtherFixed() {
        return pref.getBoolean(KEY_OTHER_FIXED, false);
    }

    public void setOtherCost(String cost) {
        editor.putString(KEY_OTHER_COST, cost);
        editor.apply();
    }

    public String getOtherCost() {
        return pref.getString(KEY_OTHER_COST, "0");
    }

    public void setOtherDistanceUnit(int position) {
        editor.putInt(KEY_OTHER_DISTANCE_UNIT, position);
        editor.apply();
    }

    public int getOtherDistanceUnit() {
        return pref.getInt(KEY_OTHER_DISTANCE_UNIT, 0);
    }

    public void setOtherDistance(String distance) {
        editor.putString(KEY_OTHER_DISTANCE, distance);
        editor.apply();
    }


    public String getOtherDistance() {
        return pref.getString(KEY_OTHER_DISTANCE, "0");
    }

}
