package com.agriiprince.mvvm.data.repository;

import android.arch.lifecycle.MutableLiveData;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import com.agriiprince.activities.checkprice.SingleEvenLiveData;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.apkversions.ApkVers;
import com.agriiprince.mvvm.retrofit.dto.login.VersionUpdate;
import com.agriiprince.mvvm.retrofit.ResponseWrapper;
import com.agriiprince.mvvm.retrofit.service.ApkVersion;
import com.agriiprince.mvvm.retrofit.service.VersionUpdateService;
import com.agriiprince.mvvm.util.Logs;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VersionUpdateRepository implements VersionUpdateRepositoryUseCase {

    private static final String TAG = VersionUpdateRepository.class.getSimpleName();

    private final String DOWNLOAD_PATH = Environment.getExternalStorageDirectory() + "/download/";
    private final String DOWNLOAD_FILE_NAME = "ap.apk";

    private MutableLiveData<String> mDownloadUpdateProgress;

    private VersionUpdateService mVersionUpdateService;

    public VersionUpdateRepository(VersionUpdateService versionUpdateService) {
        this.mVersionUpdateService = versionUpdateService;
        this.mDownloadUpdateProgress = new SingleEvenLiveData<>();
    }

    @Override
    public MutableLiveData<ResponseWrapper<ApkVers>> checkForUpdate(String apkVersion, String userType) {
        final MutableLiveData<ResponseWrapper<ApkVers>> result = new MutableLiveData<>();

        final ResponseWrapper<ApkVers> responseWrapper = new ResponseWrapper<>();
        result.setValue(responseWrapper);

/*        mVersionUpdateService.versionCheck(apkVersion, userType)
                .enqueue(new Callback<List<VersionUpdate>>() {
                    @Override
                    public void onResponse(Call<List<VersionUpdate>> call, Response<List<VersionUpdate>> response) {
                        Logs.d(TAG, response.toString());
                        if (response.isSuccessful() && response.body() != null) {
                            VersionUpdate versionUpdate = response.body().get(0);
                            responseWrapper.setStatusCode(versionUpdate.getErrorCode());
                            responseWrapper.setData(versionUpdate);
                        } else {

                        }
                        result.setValue(responseWrapper);
                    }

                    @Override
                    public void onFailure(Call<List<VersionUpdate>> call, Throwable t) {
                        responseWrapper.setError(t);
                        result.setValue(responseWrapper);
                    }
                });
                */
        API_Manager api_manager=new API_Manager();
        ApkVersion retrofit_interface = api_manager.getClient3().create(ApkVersion.class);
            Call<ApkVers> call = retrofit_interface.ApkVersionCheck("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoibWVnIiwidXNlcl9pZCI6IkpVU1BBODVXIiwicGFzc3dvcmQiOiIxMjM0IiwidHlwZSI6ImZhcm1lciJ9LCJpYXQiOjE1NjM4NTQ5NDUsImV4cCI6MTU2NjQ0Njk0NX0.KuTkLZ-GxIrjS4UntEZuNqynEp8EpBU7qvkqedWPKww",apkVersion, userType);
            call.enqueue(new Callback<ApkVers>() {
                @Override
                public void onResponse(Call<ApkVers> call, Response<ApkVers> response) {
                    Logs.d("API_TESTING", "APK ver "+response.toString());
                    if (response.isSuccessful() && response.body() != null) {
                        ApkVers versionUpdate = response.body();
                        responseWrapper.setStatusCode(versionUpdate.getCode());
                        responseWrapper.setData(versionUpdate);
                    } else {

                    }
                    result.setValue(responseWrapper);
                }

                @Override
                public void onFailure(Call<ApkVers> call, Throwable t) {
                    Log.d("API_TESTING", "APK ver fail");
                    responseWrapper.setError(t);
                    result.setValue(responseWrapper);
                }
            });

        return result;
    }

    @Override
    public MutableLiveData<String> onDownloadUpdateProgress() {
        return mDownloadUpdateProgress;
    }

    @Override
    public MutableLiveData<String> onDownloadUpdate(@NonNull final String url) {
        final MutableLiveData<String> filePath = new MutableLiveData<>();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int count;

                    URL updateUrl = new URL(url);
                    URLConnection connection = updateUrl.openConnection();
                    connection.connect();

                    int lengthOfFile = connection.getContentLength();

                    // download the file
                    InputStream input = new BufferedInputStream(updateUrl.openStream(), 8192);

                    File file = new File(DOWNLOAD_PATH);
                    file.mkdirs();
                    File outputFile = new File(file, DOWNLOAD_FILE_NAME);

                    if (outputFile.exists()) {
                        outputFile.delete();
                    }

                    FileOutputStream output = new FileOutputStream(outputFile);

                    byte data[] = new byte[1024];
                    long total = 0;

                    while ((count = input.read(data)) != -1) {
                        total += count;

                        // writing data to file
                        output.write(data, 0, count);

                        String progress = "" + (int) (total * 100) / lengthOfFile;
                        mDownloadUpdateProgress.postValue(progress);
                    }

                    // flushing output
                    output.flush();

                    // closing streams
                    output.close();
                    input.close();

                    filePath.postValue(outputFile.getAbsolutePath());

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        return filePath;
    }
}
