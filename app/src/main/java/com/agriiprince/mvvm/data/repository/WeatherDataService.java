package com.agriiprince.mvvm.data.repository;

import android.app.Activity;
import android.util.Log;

import com.agriiprince.R;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.weather.WeatherResponse;
import com.agriiprince.mvvm.retrofit.model.weather.DailyForecast;
import com.agriiprince.mvvm.retrofit.service.WeatherForecast;
import com.agriiprince.mvvm.ui.homescreen.FarmerHomeScreenActivity;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.model.WeatherModel;
import com.agriiprince.mvvm.util.Logs;

import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherDataService {

    private static final String TAG = WeatherDataService.class.getName();

    private boolean todayWeather = false;

    private Activity mActivity;

    private UserProfile mUserProfile;

    private OnWeatherCallback mCallback;

    public WeatherDataService(Activity activity, OnWeatherCallback callback) {
        this.mActivity = activity;
        mCallback = callback;

        mUserProfile = new UserProfile(activity);

        if (activity instanceof FarmerHomeScreenActivity)
            todayWeather = true;

        getWeatherInfo();

    }

    private void getWeatherInfo() {
        PrefManager prefManager = new PrefManager(mActivity);

        String city = prefManager.getCity();

        if (city != null) getWeatherInfo(prefManager.getCity());
    }


    private void getWeatherInfo(String place) {
        Log.d("WEATHER_DATA", " ---> "+ place);
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

        getWeatherInfo(simpleDateFormat.format(calendar.getTime()), place);
    }


    private void getWeatherInfo(final String date, final String place) {
 /*       StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_WEATHER_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.d(TAG, listener);
                        try {
                            parseResponse(response, place);

                        } catch (JSONException e) {
                            e.printStackTrace();

                            mCallback.onWeatherUpdateFail();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                mCallback.onWeatherUpdateFail();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("api_token", mUserProfile.getApiToken());

                GeneralUtils.showLog(date + "  " + place);

                params.put("date", date);
                params.put("city", place);

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest); */
        API_Manager api_manager=new API_Manager();
        WeatherForecast retrofit_interface = api_manager.getClient9().create(WeatherForecast.class);

        Log.d("WEATHER_DATA", " --->2 "+ place);
        Call<WeatherResponse> call = retrofit_interface.WeatherResponse(mUserProfile.getApiToken(),date,place);
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                Logs.d("CheckStatus", "onResponse: "+response);
                try {
                    Log.d("WEATHER_DATA", " --->3 "+ place);
                    Log.d("WEATHER_DATA", " --->4 "+ response);
                    parseResponse(response, place);

                } catch (JSONException e) {
                    e.printStackTrace();

                    mCallback.onWeatherUpdateFail();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                t.printStackTrace();

                mCallback.onWeatherUpdateFail();
            }
        });
    }


    private void parseResponse(Response<WeatherResponse> response, String place) throws JSONException {

        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);

        String todayDate = outputFormat.format(Calendar.getInstance().getTime());

        List<WeatherModel> weatherModels = new ArrayList<>();

 /*       JSONObject responseObject = new JSONObject(response);
        JSONArray forecastArray = responseObject.getJSONArray("DailyForecasts");
        for (int i = 0; i < forecastArray.length(); i++) {
            WeatherModel weatherModel = new WeatherModel();
            JSONObject forecastObject = forecastArray.getJSONObject(i);
            JSONObject dayObject = forecastObject.getJSONObject("Day");
            JSONObject nightObject = forecastObject.getJSONObject("Night");
            String dateStamp = forecastObject.getString("Date");
            JSONObject temperatureObject = forecastObject.getJSONObject("Temperature");
            double minTemp = (double) temperatureObject.getJSONObject("Minimum").getInt("Value");
            double maxTemp = (double) temperatureObject.getJSONObject("Maximum").getInt("Value");
            String date = dateStamp.split("T")[0];
            try {
                date = outputFormat.format(inputFormat.parse(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
 */
        List<DailyForecast> forecastArray =response.body().getData().getDailyForecasts();
        for (int i = 0; i < forecastArray.size(); i++) {
            WeatherModel weatherModel = new WeatherModel();
            String dateStamp = forecastArray.get(i).getDate();
            String date = dateStamp.split("T")[0];
            try {
                date = outputFormat.format(inputFormat.parse(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            weatherModel.setDate(date);
            weatherModel.setCity(place);
            weatherModel.setTempMax(forecastArray.get(i).getTemperature().getMaximum().getValue());
            weatherModel.setTempMin(forecastArray.get(i).getTemperature().getMinimum().getValue());
            weatherModel.setDayIcon(getIcon(forecastArray.get(i).getDay().getIcon(), 0));
            weatherModel.setDayCondition(forecastArray.get(i).getDay().getShortPhrase());
            weatherModel.setNightIcon(getIcon(forecastArray.get(i).getNight().getIcon(), 1));
            weatherModel.setNightCondition(forecastArray.get(i).getNight().getShortPhrase());

            if (todayWeather && date.equals(todayDate)) {
                mCallback.onDayWeatherUpdate(weatherModel);
                return;
            }

            if (i < 5) weatherModels.add(weatherModel);

            Log.d(TAG, weatherModel.getDate());

        }

        mCallback.onWeatherUpdate(weatherModels);

        if (todayWeather) mCallback.onWeatherUpdateFail();
    }


    private int getIcon(int icon, int timeOfDay) {

        switch (icon) {
            case 1:
                return R.drawable.img1;

            case 24:
                return R.drawable.img22;

            case 2:
                return R.drawable.img2;

            case 3:
                return R.drawable.img3;

            case 4:
                return R.drawable.img4;

            case 5:
                return R.drawable.img5;

            case 6:
                return R.drawable.img6;

            case 7:
                return R.drawable.img7;

            case 8:
                return R.drawable.img8;

            case 11:
                if (timeOfDay == 0) return R.drawable.img11_day;

                else return R.drawable.img11_night;

            case 12:
                return R.drawable.img12;

            case 13:
                return R.drawable.img13;

            case 14:
                return R.drawable.img14;

            case 15:
                return R.drawable.img15;

            case 16:
                return R.drawable.img16;

            case 17:
                return R.drawable.img17;

            case 18:
                return R.drawable.img18;

            case 19:
                return R.drawable.img19;

            case 20:
                return R.drawable.img20;

            case 21:
                return R.drawable.img21;

            case 22:
                return R.drawable.img22;

            case 23:
                return R.drawable.img23;

            case 25:
                return R.drawable.img25;

            case 26:
                return R.drawable.img26;

            case 29:
                return R.drawable.img29;

            case 33:
                return R.drawable.img33;

            case 34:
                return R.drawable.img34;

            case 35:
                return R.drawable.img35;

            case 36:
                return R.drawable.img36;

            case 37:
                return R.drawable.img37;

            case 38:
                return R.drawable.img38;

            case 39:
                return R.drawable.img39;

            case 40:
                return R.drawable.img40;

            case 41:
                return R.drawable.img41;

            case 42:
                return R.drawable.img42;

            case 43:
                return R.drawable.img43;

            case 44:
                return R.drawable.img44;

            default:
                return R.drawable.sunny1;

        }

    }

    public interface OnWeatherCallback {
        void onDayWeatherUpdate(WeatherModel weatherModel);

        void onWeatherUpdate(List<WeatherModel> weatherModelList);

        void onWeatherUpdateFail();
    }

}
