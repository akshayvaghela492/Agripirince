package com.agriiprince.mvvm.model;

import android.text.TextUtils;

import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.retrofit.model.login.LoginUser;
import com.agriiprince.mvvm.retrofit.model.oe.OeLoginDetails;
import com.agriiprince.mvvm.util.UserUtils;
import com.agriiprince.utils.DateUtil;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

public class User {

    private static final String TAG = "User";

    private String userId;

    private String userType;

    private String password;

    private String accessToken;

    private PrefManager mPrefManager;

    @Inject
    public User(PrefManager preferenceManager) {
        this.mPrefManager = preferenceManager;
        updateUserData();
    }

    private void updateUserData() {
        this.userId = mPrefManager.getUserId();
        this.userType = mPrefManager.getUserType();
        this.password = mPrefManager.getUserPassword();
        this.accessToken = mPrefManager.getAccessToken();
    }

    /**
     * check if user is logged in
     * @return true if user id, user type, password, access token is valid or else false
     */
    public boolean isLogged() {
        return !TextUtils.isEmpty(userId) && !TextUtils.isEmpty(userType) &&
                !TextUtils.isEmpty(password) && !TextUtils.isEmpty(accessToken);
    }

    /**
     * returns user id
     * @return String user id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * returns user type
     * @return string user type
     */
    public String getUserType() {
        return userType;
    }

    /**
     * returns user password
     * @return String user password
     */
    public String getPassword() {
        return password;
    }

    /**
     * returns access token for apis
     * @return string access token for api calls
     */
    public String getAccessToken() {
        return accessToken;
    }

    public void setUserId(String userId) {
        this.userId = userId;
        mPrefManager.setUserId(userId);
    }

    public void setUserType(String userType) {
        this.userType = userType;
        mPrefManager.setUserType(userType);
    }

    public void setPassword(String mPassword) {
        this.password = mPassword;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        mPrefManager.setAccessToken(accessToken);
    }

    public interface Type {
        String COMMISSION_AGENT = "ca";
        String FARMER = "farmer";
        String TRUCKER_SHIPPER = "trucker";
        String FORWARDING_AGENT = "fa";
        String OPERATIVE_EXECUTIVE = "oe";
        String TRADER = "trader";
        String PESTICIDE_VENDOR = "pv";
    }

    //newer
    public void saveUserLoginDataFromModel(String userName, LoginUser userObject) throws JSONException {

                mPrefManager.setUserName(userObject.getFarmerName());
                mPrefManager.setMobileNo(userObject.getFarmerContact());
                mPrefManager.setUserId(userObject.getFarmerId());
                mPrefManager.setUserPassword(userObject.getFarmerPassword());
                mPrefManager.setUserType("farmer");
                mPrefManager.setCity(userObject.getFarmerLocation());
                mPrefManager.setPinCode(userObject.getFarmerPincode());
                mPrefManager.setRSStopics(userObject.getInterestedRssTopics());
                mPrefManager.setCreatedOn(DateUtil.getTimeForServerDate(userObject.getCreatedOn()));

                mPrefManager.setInterestedCropIds(userObject.getInterestedCropList());
                mPrefManager.setInterestedMandiIds(userObject.getInterestedMandiList());


        updateUserData(); // update the fields once they are stored in locally
    }


    public void saveUserLoginDataFromModelOe(String userName, OeLoginDetails userObject) throws JSONException {

                mPrefManager.setUserName(userObject.getOeName());
                mPrefManager.setMobileNo(userObject.getOeContact());
                mPrefManager.setUserId(userObject.getOeId());
                mPrefManager.setUserPassword(userObject.getOePassword());
                mPrefManager.setUserType("oe");
                //mPrefManager.setCity(userObject.getOeLocation());
                //mPrefManager.setPinCode(userObject.getPincode().toString());
                //mPrefManager.setCreatedOn(DateUtil.getTimeForServerDate(userObject.getCreatedOn()));


        updateUserData(); // update the fields once they are stored in locally
    }

    //older
    public void saveUserLoginData(String userName, JSONObject userObject) throws JSONException {
        switch (UserUtils.getUserTypeFromUserName(userName)) {

            case Type.COMMISSION_AGENT:
                mPrefManager.setUserName(userObject.getString("ca_name"));
                mPrefManager.setMobileNo(userObject.getString("ca_contact"));
                mPrefManager.setUserId(userObject.getString("ca_id"));
                mPrefManager.setUserPassword(userObject.getString("ca_password"));
                mPrefManager.setUserType("ca");
                mPrefManager.setCity(userObject.getString("ca_mandi"));
                break;

            case Type.FARMER:
                mPrefManager.setUserName(userObject.getString("farmer_name"));
                mPrefManager.setMobileNo(userObject.getString("farmer_contact"));
                mPrefManager.setUserId(userObject.getString("farmer_id"));
                mPrefManager.setUserPassword(userObject.getString("farmer_password"));
                mPrefManager.setUserType("farmer");
                mPrefManager.setCity(userObject.getString("farmer_location"));
                mPrefManager.setPinCode(userObject.getString("farmer_pincode"));
                mPrefManager.setRSStopics(userObject.getString("interested_rss_topics"));
                mPrefManager.setCreatedOn(DateUtil.getTimeForServerDate(userObject.getString("created_on")));

                mPrefManager.setInterestedCropIds(userObject.getString("interested_crop_list"));
                mPrefManager.setInterestedMandiIds(userObject.getString("interested_mandi_list"));
                break;

            case Type.TRUCKER_SHIPPER:
                mPrefManager.setUserId(userObject.getString("trucker_id"));
                mPrefManager.setUserPassword(userObject.getString("trucker_password"));
                mPrefManager.setUserType("trucker");
                mPrefManager.setCity(userObject.getString("trucker_location"));
                break;

            case Type.FORWARDING_AGENT:

                break;

            case Type.OPERATIVE_EXECUTIVE:
                mPrefManager.setUserName(userObject.getString("oe_name"));
                mPrefManager.setMobileNo(userObject.getString("oe_contact"));
                mPrefManager.setUserId(userObject.getString("oe_id"));
                mPrefManager.setUserPassword(userObject.getString("oe_password"));
                mPrefManager.setUserType("oe");
                mPrefManager.setCity(userObject.getString("oe_location"));
                mPrefManager.setPinCode(userObject.getString("pincode"));


                mPrefManager.setInterestedCropIds(userObject.getString("interested_crop_list"));
                mPrefManager.setInterestedMandiIds(userObject.getString("interested_mandi_list"));
                break;

            case Type.TRADER:
                //go to respective activities
                break;

            case Type.PESTICIDE_VENDOR:
                mPrefManager.setUserName(userObject.getString("pesticide_vendor_name"));
                mPrefManager.setMobileNo(userObject.getString("pesticide_vendor_contact"));
                mPrefManager.setUserId(userObject.getString("pesticide_vendor_id"));
                mPrefManager.setInviteCode(userObject.getString("invite_code"));
                mPrefManager.setUserPassword(userObject.getString("pesticide_vendor_password"));
                mPrefManager.setUserType("pv");
                mPrefManager.setCity(userObject.getString("pesticide_vendor_location"));
                break;
        }

        updateUserData(); // update the fields once they are stored in locally
    }
}
