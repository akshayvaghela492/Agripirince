package com.agriiprince.mvvm.model;

import com.google.gson.annotations.SerializedName;

public class Subscription {

    public static final int NONE = 0;

    public static final int BASIC = 1;

    public static final int PREMIUM = 2;

    @SerializedName("subscription_id")
    private String id;

    @SerializedName("subscription_price")
    private String amount;

    @SerializedName("subscription_validity_days")
    private String validityDays;

    @SerializedName("subscription_name")
    private String subscriptionName;

    private String actualAmount;

    private boolean isBasic;

    private String discountAmount;

    public String getId() {
        return id;
    }

    public String getAmount() {
        return amount;
    }

    public String getValidityDays() {
        return validityDays;
    }

    public String getSubscriptionName() {
        return subscriptionName;
    }

    public String getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(String actualAmount) {
        this.actualAmount = actualAmount;
    }

    public boolean isBasic() {
        return isBasic;
    }

    public void setBasic(boolean basic) {
        isBasic = basic;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }
}
