package com.agriiprince.mvvm.model;

import com.agriiprince.mvvm.retrofit.model.weather.DailyForecast;

public class WeatherModel extends DailyForecast {

    String city;
    String date;
    double tempMin;
    double tempMax;
    Integer dayIcon;
    Integer nightIcon;
    String dayCondition;
    String nightCondition;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getTempMin() {
        return tempMin;
    }

    public void setTempMin(double tempMin) {
        this.tempMin = tempMin;
    }

    public double getTempMax() {
        return tempMax;
    }

    public void setTempMax(double tempMax) {
        this.tempMax = tempMax;
    }

    public Integer getDayIcon() {
        return dayIcon;
    }

    public void setDayIcon(Integer dayIcon) {
        this.dayIcon = dayIcon;
    }

    public Integer getNightIcon() {
        return nightIcon;
    }

    public void setNightIcon(Integer nightIcon) {
        this.nightIcon = nightIcon;
    }

    public String getDayCondition() {
        return dayCondition;
    }

    public void setDayCondition(String dayCondition) {
        this.dayCondition = dayCondition;
    }

    public String getNightCondition() {
        return nightCondition;
    }

    public void setNightCondition(String nightCondition) {
        this.nightCondition = nightCondition;
    }
}
