package com.agriiprince.mvvm.model.crop;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;

import java.util.ArrayList;
import java.util.List;

public class CropClass {

    private static final String TAG = CropClass.class.getSimpleName();

    public static final String BEVERAGES = "Beverages";
    public static final String CEREALS = "Cereals";
    public static final String COMMERCIAL = "Commercial";
    public static final String DRUG_AND_NARCOTICS = "Drug And Narcotics";
    public static final String DRY_FRUITS = "Dry Fruits";
    public static final String DYE = "Dye";
    public static final String FIBER = "Fiber";
    public static final String FLOWERS = "Flowers";
    public static final String FOREST_PRODUCTS = "Forest Products";
    public static final String FRUIT = "Fruit";
    public static final String GREEN_MANURING = "Green Manuring";
    public static final String LEGUMES = "Legumes";
    public static final String LIVE_STOCK_POULTRY_FISHERIES = "Live Stock,Poultry, Fisheries";
    public static final String MEDICINAL = "Medicinal";
    public static final String MILK_PRODUCT = "Milk Product";
    public static final String NUTS = "Nuts";
    public static final String OIL_AND_FATS = "Oil & Fats";
    public static final String OIL_SEEDS = "Oil Seeds";
    public static final String ORNAMENTAL_FLOWER = "Ornamental Flower";
    public static final String OTHERS = "Others";
    public static final String PULSES = "Pulses";
    public static final String RICE_MECHINE = "Rice Mechine";
    public static final String SPICES = "Spices";
    public static final String STIMULANTS = "Stimulants";
    public static final String SUGAR_CROPS = "Sugar Crops";
    public static final String TIMBER = "Timber";
    public static final String VEGETABLE = "Vegetable";

    private static String[] cropClassEn = {
            "Vegetable",
            "Fruit",
            "Pulses",
            "Cereals",
            "Nuts",
            "Sugar Crops",
            "Beverages",
            "Commercial",
            "Drug And Narcotics",
            "Dry Fruits",
            "Dye",
            "Fiber",
            "Flowers",
            "Forest Products",
            "Green Manuring",
            "Legumes",
            "Live Stock,Poultry, Fisheries",
            "Medicinal",
            "Milk Product",
            "Oil & Fats",
            "Oil Seeds",
            "Ornamental Flower",
            "Others",
            "Rice Mechine",
            "Spices",
            "Stimulants",
            "Timber"
    };

    private static String[] getCropClassHi = {
            "सबजी",
            "फल",
            "दलहन",
            "अनाज",
            "नट्स",
            "चीनी फसलों",
            "पेय",
            "व्यावसायिक",
            "दवा और नारकोटिक्स",
            "मेवे",
            "रंग",
            "रेशा",
            "पुष्प",
            "वनोपज",
            "ग्रीन मैनेरिंग",
            "सब्जियों",
            "लाइव स्टॉक, कुक्कुट, मत्स्य पालन",
            "औषधीय",
            "दूध उत्पाद",
            "तेल और वसा",
            "तेल बीज",
            "सजावटी फूल",
            "अन्य",
            "चावल मेचिन",
            "मसाले",
            "उत्तेजक",
            "लकड़ी"
    };

    private static String[] getCropClassKn = {
            "ತರಕಾರಿ",
            "ಹಣ್ಣು",
            "ದ್ವಿದಳ ಧಾನ್ಯಗಳು",
            "ಧಾನ್ಯಗಳು",
            "ಬೀಜಗಳು",
            "ಸಕ್ಕರೆ ಬೆಳೆಗಳು",
            "ಪಾನೀಯಗಳು",
            "ವಾಣಿಜ್ಯ",
            "ಡ್ರಗ್ ಮತ್ತು ನಾರ್ಕೋಟಿಕ್ಸ್",
            "ಒಣ ಹಣ್ಣುಗಳು",
            "ಡೈ",
            "ಫೈಬರ್",
            "ಹೂಗಳು",
            "ಅರಣ್ಯ ಉತ್ಪನ್ನಗಳು",
            "ಹಸಿರು ಗೊಬ್ಬರ",
            "ಲೆಗ್ಯೂಮ್ಸ್",
            "ಲೈವ್ ಸ್ಟಾಕ್, ಪೌಲ್ಟ್ರಿ, ಫಿಶರೀಸ್",
            "ಔಷಧೀಯ",
            "ಹಾಲು ಉತ್ಪನ್ನ",
            "ತೈಲ ಮತ್ತು ಕೊಬ್ಬು",
            "ತೈಲ ಬೀಜಗಳು",
            "ಅಲಂಕಾರಿಕ ಹೂವು",
            "ಇತರರು",
            "ರೈಸ್ ಮೆಚೈನ್",
            "ಮಸಾಲೆಗಳು",
            "ಉತ್ತೇಜಕಗಳು",
            "ಮರದ"
    };

    private String crop_class;
    private String crop_class_bi;

    public String getCrop_class() {
        return crop_class;
    }

    public void setCrop_class(String crop_class) {
        this.crop_class = crop_class;
    }

    public String getCrop_class_bi() {
        return crop_class_bi;
    }

    public void setCrop_class_bi(String crop_class_bi) {
        this.crop_class_bi = crop_class_bi;
    }

    public static List<CropClass> getCropClassList(Context context) {
        PrefManager prefManager = new PrefManager(context);
        String primaryLocale = prefManager.getLocale();
        String secondaryLocale = prefManager.getSecondaryLocale();

        String secondaryCode;
        if (primaryLocale.equalsIgnoreCase("en")) {
            if (TextUtils.isEmpty(secondaryLocale)) secondaryCode = "hi";
            else secondaryCode = secondaryLocale;
        } else {
            secondaryCode = primaryLocale;
        }

        List<CropClass> cropClasses = new ArrayList<>();
        for (int i = 0; i < cropClassEn.length; i++) {
            CropClass cropClass = new CropClass();
            cropClass.setCrop_class(cropClassEn[i]);

            if (secondaryCode.equalsIgnoreCase("hi"))
                cropClass.setCrop_class_bi(getCropClassHi[i]);
            else if (secondaryCode.equalsIgnoreCase("kn"))
                cropClass.setCrop_class_bi(getCropClassKn[i]);

            cropClasses.add(cropClass);
        }

        return cropClasses;
    }

    public static String getImageUrl(String cropClass) {
        return Config.CROP_CLASS_IMAGE_BASE_URL + Uri.encode(cropClass + ".png");
    }
}
