package com.agriiprince.mvvm.model.crop;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.utils.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * usage details
 * <p>
 * create new cropName
 * <p>
 * List<cropName> corps = new ArrayList<>();
 * <p>
 * cropName crop1 = new cropName(cropName1);
 * <p>
 * // add multiple varieties like this to a single cropName
 * Variety variety1 = crop1.addVariety(varietyName1);
 * variety1.id = "crop_id";
 * <p>
 * // add multiple grades to single variety
 * Grade grade11 = variety1.addGrade(gradeName11);
 * <p>
 * Grade grade21 = variety1.addGrade(gradeName21);
 * <p>
 * Variety variety2 = crop1.addVariety(varietyName2);
 * variety2.id = "crop_id";
 * <p>
 * Grade grade21 = variety2.addGrade(gradeName21);
 * Grade grade22 = variety2.addGrade(gradeName22);
 * <p>
 * crops.add(cropName);
 * <p>
 * <p>
 * getItem back crops
 * <p>
 * cropName cropName = crops.getItem(index);
 * <p>
 * Variety variety = cropName.getVariety(indexVariety);
 * <p>
 * Grade grade = cropName.getVariety(indexVariety).getGrade(indexGrade);
 * <p>
 * List<String> cropNames = cropName.getNames(crops);
 * <p>
 * List<String> varieties = cropName.getVarietyNames();
 * <p>
 * List<String> grades = variety.getGradeNames();
 */
public class Crop {

    private static final String TAG = "Crop";

    public String crop_id;

    public String cropName;
    public String cropName_bi;

    public String cropClass;
    public String cropClass_bi;

    public String cropImageName;

    private List<Variety> varieties;

    public Crop(String cropName, String cropName_bi) {
        this.cropName = cropName;
        this.cropName_bi = cropName_bi;
        this.varieties = new ArrayList<>();
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Crop)) return false;
        Crop crop = (Crop) obj;
        return this.cropName.equalsIgnoreCase(crop.cropName);
    }

    @NonNull
    @Override
    public String toString() {
        return cropName + " " + cropName_bi;
    }

    public List<String> getVarietyNames() {
        return Variety.getNames(varieties);
    }

    public List<String> getBilingualVarietyNames(Context context) {
        return Variety.getBilingualNames(context, varieties);
    }

    public List<Variety> getVarieties() {
        return varieties;
    }

    public Variety getVariety(String varietyName) {
        for (Variety variety : varieties) {
            if (variety.varietyName.equals(varietyName))
                return variety;
        }
        return null;
    }

    public Variety getVariety(int position) {
        return varieties.get(position);
    }

    public Variety addVarietyAt(int index, String variety, String variety_hi) {
        Variety model = new Variety(variety, variety_hi);
        varieties.add(index, model);
        return model;
    }

    public Variety addVariety(String variety, String variety_hi) {
        Variety model = new Variety(variety, variety_hi);
        varieties.add(model);
        return model;
    }


    public String getImageUrl() {
        if (TextUtils.isEmpty(cropImageName)) return null;
        String[] images = cropImageName.split(",");
        String image = images[0] + Config.IMAGE_NAME_END;
        return Config.CROP_IMAGE_BASE_URL + Uri.encode(image);
    }


    public static List<String> getNames(List<Crop> crops) {
        List<String> cropNames = new ArrayList<>();
        for (Crop crop : crops) {
            cropNames.add(crop.cropName);
        }
        return cropNames;
    }

    public static List<String> getBilingualNames(StringUtils utils, List<Crop> crops) {
        List<String> cropNames = new ArrayList<>();
        for (Crop crop : crops) {
            cropNames.add(utils.getBilingualStringForApi(crop.cropName, crop.cropName_bi));
        }
        return cropNames;
    }

    public static Crop getCropByCropName(@NonNull List<Crop> crops, @NonNull String cropName) {
        for (Crop crop : crops) {
            if (crop.cropName.equalsIgnoreCase(cropName)) return crop;
        }
        return null;
    }

    public static Crop getCropById(List<Crop> crops, String id) {
        if (crops == null || TextUtils.isEmpty(id)) return null;
        for (Crop crop : crops) {
            for (Variety variety : crop.getVarieties()) {
                if (variety.id.equalsIgnoreCase(id))
                    return crop;
            }
        }
        return null;
    }

    public static List<Crop> getCropsByIds(List<Crop> crops, String[] ids) {
        return getCropsByIds(crops, Arrays.asList(ids));
    }

    public static List<Crop> getCropsByIds(List<Crop> crops, List<String> ids) {
        if (crops == null) return null;
        List<Crop> results = new ArrayList<>();
        for (Crop crop : crops) {
            for (Variety variety : crop.getVarieties()) {
                if (ids.contains(variety.id)) {
                    if (!results.contains(crop)) // add  if not exists
                        results.add(crop);
                }
            }
        }
        return results;
    }

    public static List<String> getCropNames(List<Crop> crops) {
        List<String> cropNames = new ArrayList<>();
        for (Crop crop : crops) {
            cropNames.add(crop.cropName);
        }
        return cropNames;
    }

    public static Crop getCropByBilingualCropName(StringUtils utils, List<Crop> crops, String cropName) {
        if (crops == null) return null;
        for (Crop crop : crops) {
            if (cropName.contains(utils.getBilingualStringForApi(crop.cropName, crop.cropName_bi)))
                return crop;
        }
        return null;
    }

    public static String getSecondNameByCropName(List<Crop> crops, String cropName) {
        if (crops == null) return null;
        for (Crop crop : crops) {
            if (cropName.contains(crop.cropName)) return crop.cropName_bi;
        }
        return null;
    }

}
