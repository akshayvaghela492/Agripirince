package com.agriiprince.mvvm.model;

public class FigContact {
    private String name;
    private String contact;
    private int number_of_farmers;

    public FigContact(String name, String contact, int number_of_farmers) {
        this.name = name;
        this.contact = contact;
        this.number_of_farmers = number_of_farmers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public int getNumber_of_farmers() {
        return number_of_farmers;
    }

    public void setNumber_of_farmers(int number_of_farmers) {
        this.number_of_farmers = number_of_farmers;
    }
}
