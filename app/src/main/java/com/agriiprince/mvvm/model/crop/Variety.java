package com.agriiprince.mvvm.model.crop;

import android.content.Context;

import com.agriiprince.appcontroller.AppController;
import com.agriiprince.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class Variety {

    public String varietyName;
    public String varietyName_bi;

    public String id;

    private List<Grade> grades;

    public Variety(String varietyName, String varietyName_bi) {
        this.varietyName = varietyName;
        this.varietyName_bi = varietyName_bi;
        this.grades = new ArrayList<>();
    }

    public Grade addGradeAt(int index, String gradeName, String gradeName_hi) {
        Grade grade = new Grade(gradeName, gradeName_hi);
        grades.add(index, grade);
        return grade;
    }

    public Grade addGrade(String gradeName, String gradeName_hi) {
        Grade grade = new Grade(gradeName, gradeName_hi);
        grades.add(grade);
        return grade;
    }

    public Grade getGrade(int position) {
        return grades.get(position);
    }

    public List<Grade> addGrades(List<Grade> grades) {
        this.grades.addAll(grades);

        return grades;
    }

    public Grade getGrade(String gradeName) {
        for (Grade grade : grades) {
            if (grade.gradeName.equals(gradeName))
                return grade;
        }
        return null;
    }

    public List<Grade> getGrades() {
        return grades;
    }

    public List<String> getGradeNames() {
        return Grade.getNames(grades);
    }

    public List<String> getBilingualGradeNames(Context context) {
        return Grade.getBilingualNames(context, grades);
    }

    public static List<String> getNames(List<Variety> varieties) {
        List<String> varietyNames = new ArrayList<>();
        for (Variety variety : varieties) {
            varietyNames.add(variety.varietyName);
        }

        return varietyNames;
    }

    public static List<String> getBilingualNames(Context context, List<Variety> varieties) {
        StringUtils utils = AppController.getInstance().getStringUtils();
        List<String> varietyNames = new ArrayList<>();
        for (Variety variety : varieties) {
            varietyNames.add(utils.getBilingualStringForApi(variety.varietyName, variety.varietyName_bi));
        }

        return varietyNames;
    }

    public static Variety getVarietyByVarietyName(List<Variety> varieties, String varietyName) {
        for (Variety variety : varieties) {
            if (variety.varietyName.equalsIgnoreCase(varietyName)) return variety;
        }
        return null;
    }

}
