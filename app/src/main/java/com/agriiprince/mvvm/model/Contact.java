package com.agriiprince.mvvm.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.android.databinding.library.baseAdapters.BR;

public class Contact extends BaseObservable {

    private static final String TAG = "Contact";

    private String id;

    private String name;

    private String number;

    private String photoUri;

    private boolean isSelected;

    public Contact(String id, String name, String number, String photoUri) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.photoUri = photoUri;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    @Bindable
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
        notifyPropertyChanged(BR.selected);
    }

    public static void onCheckChangeCheckBox(Contact contact, boolean isChecked) {
        contact.setSelected(isChecked);
    }
}
