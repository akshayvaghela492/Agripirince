package com.agriiprince.mvvm.model;

import com.agriiprince.mvvm.applevel.annotation.LanguageCode;

public class Language {

    private String languageTitle;
    private String languageLetter;
    private String languageCode;
    private boolean isSelected;

    public Language(String languageTitle, String languageLetter, String languageCode) {
        this.languageTitle = languageTitle;
        this.languageLetter = languageLetter;
        this.languageCode = languageCode;
    }

    public String getLanguageTitle() {
        return languageTitle;
    }

    public String getLanguageLetter() {
        return languageLetter;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public interface Code {
        @LanguageCode String ENGLISH = "en";
        @LanguageCode String HINDI = "hi";
        @LanguageCode String KANNADA = "kn";
    }
}
