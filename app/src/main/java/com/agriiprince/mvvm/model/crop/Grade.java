package com.agriiprince.mvvm.model.crop;

import android.content.Context;

import com.agriiprince.appcontroller.AppController;
import com.agriiprince.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class Grade {

    public String id;

    public String gradeName;
    public String gradeName_bi;

    public Grade(String gradeName, String gradeName_bi) {
        this.gradeName = gradeName;
        this.gradeName_bi = gradeName_bi;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static List<String> getNames(List<Grade> grades) {
        List<String> gradeNames = new ArrayList<>();
        for (Grade grade : grades) {
            gradeNames.add(grade.gradeName);
        }

        return gradeNames;
    }

    public static List<String> getBilingualNames(Context context, List<Grade> grades) {
        StringUtils utils = AppController.getInstance().getStringUtils();
        List<String> gradeNames = new ArrayList<>();
        for (Grade grade : grades) {
            gradeNames.add(utils.getBilingualStringForApi(grade.gradeName, grade.gradeName_bi));
        }

        return gradeNames;
    }

    public static Grade getGradeByGradeName(List<Grade> grades, String gradeName) {
        for (Grade grade : grades) {
            if (grade.gradeName.equalsIgnoreCase(gradeName)) return grade;
        }

        return null;
    }
}
