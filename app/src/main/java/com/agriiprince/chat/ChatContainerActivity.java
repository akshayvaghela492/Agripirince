package com.agriiprince.chat;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.agriiprince.R;
import com.agriiprince.activities.ChatOneToOneActivity;
import com.agriiprince.activities.ChatRoomActivity;
import com.agriiprince.activities.TutorialActivity;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.ActivityChatContainerBinding;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.models.ChatListModel;
import com.agriiprince.mvvm.ui.homescreen.FarmerHomeScreenActivity;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.Utils;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.agriiprince.utils.Utils.showLog;

public class ChatContainerActivity extends AppCompatActivity implements AllChatsAdapter.OnItemClick {

    private AllChatsAdapter mAllChatsGroupAdapter;
    private AllChatsAdapter mAllChatsOneOnOneAdapter;

    private PrefManager pref;

    private UserProfile userProfile;

    private AppCompatTextView newChat;

    private BroadcastReceiver mOne2OneBroadCastReceiver;

    private ChatTableOperations mChatDBOperations;

    private ProgressBar progressBar;

    private ActivityChatContainerBinding binding;

    private StringUtils stringUtils;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat_container);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pref = new PrefManager(this);
        stringUtils = AppController.getInstance().getStringUtils();
        userProfile = new UserProfile(this);
        mChatDBOperations = new ChatTableOperations(this);

        RecyclerView rvGroup = findViewById(R.id.rv_group_chat);
        rvGroup.setLayoutManager(new LinearLayoutManager(this));
        mAllChatsGroupAdapter = new AllChatsAdapter();
        mAllChatsGroupAdapter.setOnItemClickListener(this);
        rvGroup.setAdapter(mAllChatsGroupAdapter);

        RecyclerView rvOneOnOne = findViewById(R.id.rv_one_on_one);
        rvOneOnOne.setLayoutManager(new LinearLayoutManager(this));
        mAllChatsOneOnOneAdapter = new AllChatsAdapter();
        mAllChatsOneOnOneAdapter.setOnItemClickListener(this);
        rvOneOnOne.setAdapter(mAllChatsOneOnOneAdapter);

        ViewCompat.setNestedScrollingEnabled(rvGroup, false);
        ViewCompat.setNestedScrollingEnabled(rvOneOnOne, false);

        newChat = findViewById(R.id.newChat);
        newChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChatContainerActivity.this, NewChatActivity.class));
            }
        });


/*
        mOne2OneBroadCastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase(Config.NOTIFICATION_TAG2)) {
                    //processMessage(new Gson().fromJson(data, MessageModel.class));

                    processOne2OneChat(intent);

                } else if (intent.getAction().equalsIgnoreCase(Config.NOTIFICATION_TAG3)) {
                    processGroupChat(intent);
                }
            }
        };
*/
        ImageView imgview_help =  findViewById(R.id.iv_help);

        imgview_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ChatContainerActivity.this, TutorialActivity.class);
                intent.putExtra("code", "CHAT");
                startActivity(intent);
            }
        });

        getAllChats();


/*
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_layout_chat_container, AllChatsFragment.newInstance());
        fm.executePendingTransactions();
        ft.commit();
*/
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //This method will return current timestamp
    public static String getTimeStamp() {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(new Date());
    }

    private void hideIndividualContainer() {
        binding.individualContainer.setVisibility(View.GONE);
    }

    private void showIndividualContainer() {
        binding.individualContainer.setVisibility(View.VISIBLE);
    }

    private void hideGroupContainer() {
        binding.groupContainer.setVisibility(View.GONE);
    }

    private void showGroupContainer() {
        binding.groupContainer.setVisibility(View.VISIBLE);
    }

    private void showProgressBar() {
        hideIndividualContainer();
        hideGroupContainer();
        binding.chatProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        binding.chatProgress.setVisibility(View.GONE);
    }

    private void showEmptyText() {
        binding.chatEmptyText.setText(stringUtils.getLocalizedString(R.string.no_chat_found_start_new));
        binding.chatEmptyText.setVisibility(View.VISIBLE);
    }

    private void hideEmptyText() {
        binding.chatEmptyText.setVisibility(View.GONE);
    }

    private void processOne2OneChat(Intent intent) {
        ChatListModel chatListModel = new ChatListModel();
        chatListModel.setSender(intent.getStringExtra("sender_id"));
        chatListModel.setMessage(intent.getStringExtra("message"));
        chatListModel.setReceiver(intent.getStringExtra("receiver_id"));
        chatListModel.setReceiver_type(intent.getStringExtra("receiver_type"));
        chatListModel.setFinal_name(intent.getStringExtra("name"));
        chatListModel.setCreated_on(getTimeStamp());

        ChatListModel chatListModel1 = mAllChatsOneOnOneAdapter.removeOne2OneItem(chatListModel.getSender());

        if (chatListModel1 != null) {
            chatListModel1.setCreated_on(getTimeStamp());
            chatListModel1.setMessage(intent.getStringExtra("message"));

        } else {
            mAllChatsOneOnOneAdapter.addItem(chatListModel);
        }
    }


    private void processGroupChat(Intent intent) {
        String message = intent.getStringExtra("message");
        String mode_2 = intent.getStringExtra("mode2");
        String commodity_id = intent.getStringExtra("commodity_id");
        String pincode = intent.getStringExtra("pincode");
        String strcity = intent.getStringExtra("city");
        String name = intent.getStringExtra("name");
        String sender_id = intent.getStringExtra("sender_id");

        ChatListModel chatListModel = new ChatListModel();
        chatListModel.setSender(sender_id);
        chatListModel.setMessage(message);
        chatListModel.setFinal_name(name);
        chatListModel.setFinal_name(name);
        chatListModel.setCreated_on(getTimeStamp());

        if (mode_2.equalsIgnoreCase("commodity_id")) {
            chatListModel.setCommodity_id(commodity_id);
        } else if (mode_2.equalsIgnoreCase("pincode")) {
            chatListModel.setPincode(pincode);
        } else if (mode_2.equalsIgnoreCase("city")) {
            chatListModel.setCity(strcity);
        }

        ChatListModel chatListModel1 = mAllChatsOneOnOneAdapter.removeGroupItem(name);

        if (chatListModel1 != null) {
            chatListModel1.setCreated_on(getTimeStamp());
            chatListModel1.setMessage(intent.getStringExtra("message"));

        } else {
            mAllChatsOneOnOneAdapter.addItem(chatListModel);
        }
    }

    private void processMessage(MessageModel messageModel) {
        mChatDBOperations.open();
        int hashcode = messageModel.getHashcode();
        int unreadCount = 1;
        Cursor cursor = mChatDBOperations.getUnreadCountByHashcode(hashcode);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToFirst()) {
                unreadCount = cursor.getInt(cursor.getColumnIndex(ChatTableData.ChatEntry.COLUMN_UNREAD_COUNT)) + 1;
                mChatDBOperations.updateUnreadCount(unreadCount, hashcode);
            }
        } else {
            mChatDBOperations.insert(messageModel.sender, messageModel.sender_type, unreadCount, hashcode);
        }
        mChatDBOperations.close();
        if (!TextUtils.isEmpty(messageModel.sender)) {
/*
            AllChatsResponse item = mAllChatsOneOnOneAdapter.removeOne2OneItem(messageModel.sender);
            if (item == null) {
                item = new AllChatsResponse();
                item.sender_id = messageModel.sender;
                item.sender_type = messageModel.sender_type;
                item.city = messageModel.city;
                item.pincode = messageModel.pincode;
                item.commodity_id = messageModel.commodity_id;
                item.latest_message_timestamp = messageModel.created_on;
                item.latest_message = messageModel.message;
                item.sender_name = messageModel.final_name;
                item.sender_pic = null;//TODO
            }
            item.unread_message_count = unreadCount + "";
            mAllChatsOneOnOneAdapter.addItem(item);
*/
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //     LocalBroadcastManager.getInstance(this).registerReceiver(mOne2OneBroadCastReceiver,
        //           new IntentFilter(Config.NOTIFICATION_TAG2));
    }

    @Override
    public void onPause() {
        super.onPause();
        //       LocalBroadcastManager.getInstance(this).unregisterReceiver(mOne2OneBroadCastReceiver);
    }

    private void getAllChats() {
        showProgressBar();
        StringRequest request = new StringRequest(Request.Method.POST, Config.GET_CHAT_MESSAGE_AT_START_UP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showLog("TESTING_VOLLEY " + response);
                        //Log.d("TESTING_VOLLEY" , response);
                        hideProgressBar();
                        try {
                           /* JSONArray responseArray = new JSONArray(response);
                            JSONObject responseObject = responseArray.getJSONObject(0);
                            int errorCode = responseObject.getInt("error_code");
                            if (errorCode == 100) {
                                String data = responseObject.getString("data");
                                List<ChatListModel> allChatsList = new Gson().fromJson(data, new TypeToken<List<ChatListModel>>() {
                                }.getType());
                                */
                           JSONObject object=new JSONObject(response);
                            int errorCode = object.getInt("code");
                            //Log.d("RESPONSE" , errorCode+"");
                            if (errorCode == 200)
                            {
                                JSONObject data=object.getJSONObject("data");
                                JSONArray dataarray=data.getJSONArray("data");
                                List<ChatListModel> allChatsList = new Gson().fromJson(String.valueOf(dataarray), new TypeToken<List<ChatListModel>>() {
                                }.getType());

                                List<ChatListModel> groupChatsList = new ArrayList<>();
                                List<ChatListModel> oneOnOneChatsList = new ArrayList<>();
                                //mChatDBOperations.open();
                                //mChatDBOperations.deleteAllRows();
                                for (ChatListModel chat : allChatsList) {
                                    //mChatDBOperations.insert(chat.sender_id, chat.sender_type, Integer.parseInt(chat.unread_message_count), chat.getHashcode());
                                    if (chat.getMode2().equalsIgnoreCase("no_mode")) {
                                        oneOnOneChatsList.add(chat);
                                    } else {
                                        groupChatsList.add(chat);
                                    }
                                }
                                //mChatDBOperations.close();

                                hideEmptyText();
                                if (groupChatsList.size() > 0) {
                                    mAllChatsGroupAdapter.setData(groupChatsList);
                                    showGroupContainer();
                                }
                                if (oneOnOneChatsList.size() > 0) {
                                    mAllChatsOneOnOneAdapter.setData(oneOnOneChatsList);
                                    showIndividualContainer();
                                }

                                if (groupChatsList.size() == 0 && oneOnOneChatsList.size() == 0)
                                    showEmptyText();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressBar();
                        Utils.showVolleyError(ChatContainerActivity.this, error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", userProfile.getApiToken());
                params.put("mode", "by_me");
                params.put("user_id", userProfile.getUserId());
                params.put("user_type", pref.getUserType());
                return params;
            }
                        @Override
            public Map<String, String> getHeaders(){
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",userProfile.getApiToken());
                return header;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public void onChatItemClicked(ChatListModel chatItem) {
        String mode2 = chatItem.getMode2();

        if (mode2.equals("no_mode")) {
            Intent intent = new Intent(ChatContainerActivity.this, ChatOneToOneActivity.class);
            intent.putExtra("call_from", pref.getUserType());

            if (chatItem.getSender().equalsIgnoreCase(userProfile.getUserId())) {
                intent.putExtra("receiver_id", chatItem.getReceiver());
            } else {
                intent.putExtra("receiver_id", chatItem.getSender());
            }

            intent.putExtra("receiver_name", chatItem.getFinal_name());
            startActivity(intent);
        } else {
            Intent intent = new Intent(ChatContainerActivity.this, ChatRoomActivity.class);
            intent.putExtra(ChatRoomActivity.KEY_MODE, mode2);

            if (mode2.equalsIgnoreCase("pincode")) {
                intent.putExtra(ChatRoomActivity.KEY_PINCODE, chatItem.getPincode());
            } else if (mode2.equalsIgnoreCase("commodity_id")) {
                intent.putExtra(ChatRoomActivity.KEY_COMMODITY, chatItem.getCommodity_id());
            } else if (mode2.equalsIgnoreCase("city")) {
                intent.putExtra(ChatRoomActivity.KEY_CITY, chatItem.getCity());
            }

            startActivity(intent);
        }

/*
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_layout_chat_container, ChatMessageFragment.newInstance(fnDecideUserID(), pref.onChangeUserType(), chatItem));
        ft.addToBackStack(null);
        fm.executePendingTransactions();
        ft.commit();
*/
    }
}
