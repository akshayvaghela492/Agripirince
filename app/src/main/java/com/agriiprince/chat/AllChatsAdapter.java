package com.agriiprince.chat;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.models.ChatListModel;
import com.android.volley.toolbox.NetworkImageView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AllChatsAdapter extends RecyclerView.Adapter<AllChatsAdapter.ViewHolder> {


    private SimpleDateFormat dateInputFormat;
    private SimpleDateFormat dateOutputFormat;

    private List<ChatListModel> mChatsList = new ArrayList();

    private OnItemClick mOnChatItemClickListener;

    interface OnItemClick {
        void onChatItemClicked(ChatListModel chatItem);
    }

    public AllChatsAdapter() {
        this.dateInputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        this.dateOutputFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
    }

    public void setOnItemClickListener(OnItemClick listener) {
        mOnChatItemClickListener = listener;
    }

    public void setData(List<ChatListModel> chatsList) {
        mChatsList.clear();
        mChatsList.addAll(chatsList);
        notifyDataSetChanged();
    }

    public void addItem(ChatListModel item) {
        mChatsList.add(0, item);
        notifyItemInserted(0);
    }

    public ChatListModel removeOne2OneItem(String senderId) {
        for (int i = 0; i < mChatsList.size(); i++) {
            if (mChatsList.get(i).getSender().equalsIgnoreCase(senderId)) {
                return mChatsList.remove(i);
            }
        }
        return null;
    }


    public ChatListModel removeGroupItem(String groupName) {
        for (int i = 0; i < mChatsList.size(); i++) {
            if (mChatsList.get(i).getCommodity_id().equalsIgnoreCase(groupName)) {
                return mChatsList.remove(i);
            } else if (mChatsList.get(i).getCity().equalsIgnoreCase(groupName)) {
                return mChatsList.remove(i);
            } else if (mChatsList.get(i).getPincode().equalsIgnoreCase(groupName)) {
                return mChatsList.remove(i);
            }
        }
        return null;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_all_chats, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        ChatListModel item = mChatsList.get(position);

/*      viewHolder.nivSenderPic.setDefaultImageResId(R.drawable.ap_logo_final);
        viewHolder.nivSenderPic.setErrorImageResId(R.drawable.ap_logo_final);
        if (item.sender_pic != null) {
            viewHolder.nivSenderPic.setImageUrl(item.sender_pic, AppController.getInstance().getImageLoader());
        }
*/
        //viewHolder.tvUnreadCount.setText(item.unread_message_count);


        viewHolder.tvLatestMessage.setText(item.getMessage());

        try {
            String date = dateOutputFormat.format(dateInputFormat.parse(item.getCreated_on()));
            viewHolder.tvLastMessageTimestamp.setText(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String title = "";

        if (!TextUtils.isEmpty(item.getCommodity_id())) {
            title = item.getCommodity_id();
        } else if (!TextUtils.isEmpty(item.getCity())) {
            title = item.getCity();
        } else if (!TextUtils.isEmpty(item.getPincode())) {
            title = item.getPincode();
        } else if (!TextUtils.isEmpty(item.getFinal_name())) {
            title = item.getFinal_name();
        }

        if (!TextUtils.isEmpty(title)) {
            viewHolder.tvSenderName.setText(title);
            viewHolder.txtInitial.setText(viewHolder.tvSenderName.getText().toString().substring(0, 1).toUpperCase());
        }

    }

    @Override
    public int getItemCount() {
        return mChatsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        NetworkImageView nivSenderPic;
        TextView tvSenderName;
        TextView txtInitial;
        TextView tvUnreadCount;
        TextView tvLatestMessage;
        TextView tvLastMessageTimestamp;
        LinearLayout relativeLayoutItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nivSenderPic = itemView.findViewById(R.id.niv_sender_pic);
            tvSenderName = itemView.findViewById(R.id.tv_sender_name);
            txtInitial = itemView.findViewById(R.id.txtInitial);
            tvUnreadCount = itemView.findViewById(R.id.tv_unread_count);
            tvLatestMessage = itemView.findViewById(R.id.tv_latest_message);
            tvLastMessageTimestamp = itemView.findViewById(R.id.tv_timestamp);
            relativeLayoutItem = itemView.findViewById(R.id.relative_layout_item);
            relativeLayoutItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getAdapterPosition() > -1 && mOnChatItemClickListener != null) {
                        mOnChatItemClickListener.onChatItemClicked(mChatsList.get(getAdapterPosition()));
                    }
                }
            });
        }
    }
}
