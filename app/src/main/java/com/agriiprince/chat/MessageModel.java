package com.agriiprince.chat;

public class MessageModel {
    public String id;
    public String message;
    public String message_type;
    public String sender;
    public String sender_type;
    public String receiver;
    public String receiver_type;
    public String medium;
    public String deliver_status;
    public String delivered_date_time;
    public String read_status;
    public String read_date_time;
    public String created_on;
    public String commodity_id;
    public String pincode;
    public String city;
    public String final_name;

    public int getHashcode() {
        return (sender + sender_type + commodity_id + pincode + city).hashCode();
    }

}
