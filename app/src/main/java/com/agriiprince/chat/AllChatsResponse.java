package com.agriiprince.chat;

import android.text.TextUtils;

public class AllChatsResponse {
    public String sender_id;
    public String sender_type;
    public String sender_name;
    public String sender_pic;
    public String latest_message;
    public String latest_message_timestamp;
    public String unread_message_count;
    public String commodity_id;
    public String pincode;
    public String city;

    public String getMode2() {
        String mode2 = "";
        if (!TextUtils.isEmpty(sender_id)) {
            mode2 = "";
        } else if (!TextUtils.isEmpty(commodity_id)) {
            mode2 = "commodity_id";
        } else if (!TextUtils.isEmpty(pincode)) {
            mode2 = "pincode";
        } else if (!TextUtils.isEmpty(city)) {
            mode2 = "city";
        }
        return mode2;
    }

    public int getHashcode() {
        return (sender_id + sender_type + commodity_id + pincode + city).hashCode();
    }
}
