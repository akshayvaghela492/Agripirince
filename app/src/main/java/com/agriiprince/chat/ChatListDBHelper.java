package com.agriiprince.chat;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.agriiprince.chat.ChatTableData.SQL_CHAT_ENTRIES;
import static com.agriiprince.chat.ChatTableData.SQL_DELETE_CHAT_ENTRIES;

public class ChatListDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "ap.db";

    public ChatListDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CHAT_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_CHAT_ENTRIES);
        onCreate(sqLiteDatabase);
    }
}
