package com.agriiprince.chat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import static com.agriiprince.chat.ChatTableData.ChatEntry.TABLE_NAME;

public class ChatTableOperations {

    private ChatListDBHelper mChatDBHelper;
    private SQLiteDatabase mSQLiteDatabase;

    public ChatTableOperations(Context context) {
        mChatDBHelper = new ChatListDBHelper(context);
    }

    public void open() {
        mSQLiteDatabase = mChatDBHelper.getWritableDatabase();
    }

    public void insert(String senderId, String senderType, int unreadCount, int hashcode) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ChatTableData.ChatEntry.COLUMN_SENDER_ID, senderId);
        contentValues.put(ChatTableData.ChatEntry.COLUMN_SENDER_TYPE, senderType);
        contentValues.put(ChatTableData.ChatEntry.COLUMN_UNREAD_COUNT, unreadCount);
        contentValues.put(ChatTableData.ChatEntry.COLUMN_HASHCODE, hashcode);
        mSQLiteDatabase.insert(TABLE_NAME, null, contentValues);
    }

    public Cursor getUnreadCountByHashcode(int hashcode) {
        return mSQLiteDatabase.query(TABLE_NAME,
                null, null, null, null, null, null);
    }

    public void updateUnreadCount(int unreadCount, int hashcode) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ChatTableData.ChatEntry.COLUMN_UNREAD_COUNT, unreadCount);
        contentValues.put(ChatTableData.ChatEntry.COLUMN_HASHCODE, hashcode);
        mSQLiteDatabase.update(TABLE_NAME, contentValues,
                ChatTableData.ChatEntry.COLUMN_HASHCODE + "=?", new String[]{hashcode + ""});
    }

    public void deleteAllRows() {
        mSQLiteDatabase.delete(TABLE_NAME, null, null);
    }

    public void close() {
        mSQLiteDatabase.close();
    }

}
