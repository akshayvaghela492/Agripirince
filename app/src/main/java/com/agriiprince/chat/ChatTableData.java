package com.agriiprince.chat;

import android.provider.BaseColumns;

final class ChatTableData {

    static final String SQL_CHAT_ENTRIES =
            "CREATE TABLE " + ChatEntry.TABLE_NAME + " (" +
                    ChatEntry._ID + " INTEGER PRIMARY KEY," +
                    ChatEntry.COLUMN_SENDER_ID + " TEXT," +
                    ChatEntry.COLUMN_SENDER_TYPE + " TEXT," +
                    ChatEntry.COLUMN_UNREAD_COUNT + " INTEGER," +
                    ChatEntry.COLUMN_HASHCODE + " INTEGER)";

    static final String SQL_DELETE_CHAT_ENTRIES = "DROP TABLE IF EXISTS " + ChatEntry.TABLE_NAME;

    private ChatTableData() {
    }

    static class ChatEntry implements BaseColumns {
        static final String TABLE_NAME = "chatEntries";
        static final String COLUMN_SENDER_ID = "senderId";
        static final String COLUMN_SENDER_TYPE = "senderType";
        static final String COLUMN_UNREAD_COUNT = "unreadCount";
        static final String COLUMN_HASHCODE = "breedNameHashCode";
    }

}
