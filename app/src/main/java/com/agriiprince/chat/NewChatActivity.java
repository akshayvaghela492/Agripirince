package com.agriiprince.chat;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.activities.ChatRoomActivity;
import com.agriiprince.adapter.PesticidesVendorFarmersListAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.ActivityNewChatBinding;
import com.agriiprince.dataservice.CropDataService;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.FarmersListModel;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.utils.Utils;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class NewChatActivity extends AppCompatActivity {

    private ArrayList<String> names = new ArrayList<>();
    private ArrayList<String> locations = new ArrayList<>();
    private ArrayList<String> subscribed_pesticide_vendor = new ArrayList<>();
    private ArrayList<String> invited_by = new ArrayList<>();
    private ArrayList<String> farmer_id = new ArrayList<>();
    private ArrayList<FarmersListModel> farmers_list = new ArrayList<>();

    PrefManager prefManager;
    private RecyclerView recyclerView;
    EditText etSearchFarmer;
    private PesticidesVendorFarmersListAdapter pesticidesVendorFarmersListAdapter2;
    private ProgressDialog progressDialog;
    private TextView noFramerListFound;
    private TextInputLayout textInputLayout;

    private UserProfile userProfile;

    private List<String> crops;

    private AtomicBoolean isCropLoaded;

    private ActivityNewChatBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_chat);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        isCropLoaded = new AtomicBoolean(false);

        userProfile = new UserProfile(this);
        prefManager = new PrefManager(this);
       // etSearchFarmer = findViewById(R.id.etSearchFarmer);
        noFramerListFound = findViewById(R.id.no_data_found_text_framer_list);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));


        findViewById(R.id.txtNewCommodity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (!isCropLoaded.get()) {

                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                showAlertToEnterCommodity();
                            }
                        });
                    }
                }).start();
            }
        });

        findViewById(R.id.txtCity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGrupChat(null, userProfile.getUserCity(), null);
            }
        });

        findViewById(R.id.txtPincode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGrupChat(null, null, userProfile.getUserPincode());

            }
        });

        getCrops();

        initRecyclerView();

        initNamesandlocations();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showAlertToEnterCommodity() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_get_commodity, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alert = dialogBuilder.create();
        final AutoCompleteTextView autocomplete = dialogView.findViewById(R.id.autoCompleteTextView1);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.select_dialog_item, crops);
        autocomplete.setThreshold(0);
        autocomplete.setAdapter(adapter);
        dialogView.findViewById(R.id.btnProceed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(autocomplete.getText().toString())) {
                    if (crops.contains(autocomplete.getText().toString())) {

                        startGrupChat(autocomplete.getText().toString(), null, null);

                    } else {
                        autocomplete.setError("No chat room for this commodity");
                    }
                } else {
                    autocomplete.setError("Can't be blank");
                }

            }
        });
        alert.show();
    }


    private void getCrops() {
        CropDataService cropDataService = new CropDataService(this);
        cropDataService.getCrops(new CropDataService.OnCropDataRequest() {
            @Override
            public void onCropDataResults(List<Crop> crops) {
                NewChatActivity.this.crops = Crop.getNames(crops);
                isCropLoaded.set(true);
            }

            @Override
            public void onCropDataError() {
                NewChatActivity.this.crops = null;
            }
        });
    }


    private void startGrupChat(String commodity, String city, String pincode) {
        String mode = "";
        if (!TextUtils.isEmpty(commodity)) mode = "commodity_id";

        else if (!TextUtils.isEmpty(city)) mode = "city";

        else if (!TextUtils.isEmpty(pincode)) mode = "pincode";

        Intent intent = new Intent(NewChatActivity.this, ChatRoomActivity.class);
        intent.putExtra(ChatRoomActivity.KEY_MODE, mode);

        if (mode.equalsIgnoreCase("pincode")) {
            intent.putExtra(ChatRoomActivity.KEY_PINCODE, pincode);
        } else if (mode.equalsIgnoreCase("commodity_id")) {
            intent.putExtra(ChatRoomActivity.KEY_COMMODITY, commodity);
        } else if (mode.equalsIgnoreCase("city")) {
            intent.putExtra(ChatRoomActivity.KEY_CITY, city);
        }

        Log.d("New Chat ", mode);

        if (!TextUtils.isEmpty(mode)) {
            startActivity(intent);
            finish();
        }
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.farmerslist);
            /*pesticidesVendorFarmersListAdapter =
                    new PesticidesVendorFarmersListAdapter(names,locations,this,subscribed_pesticide_vendor,invited_by,farmer_id);*/
        pesticidesVendorFarmersListAdapter2 = new PesticidesVendorFarmersListAdapter(this, farmers_list);
        recyclerView.setAdapter(pesticidesVendorFarmersListAdapter2);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initNamesandlocations() {
        binding.txtFarmerList.setVisibility(View.GONE);

        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST,Config.GET_FARMERS_LIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.d("TESTING_VOLLEY", response);
                        try {
                            //JSONArray responseArray = new JSONArray(response);
                            JSONObject object=new JSONObject(response);
                            //JSONObject responseObject = responseArray.getJSONObject(0);
                            //int errorCode = responseObject.getInt("error_code");
                            int Code = object.getInt("code");
                            Log.d("TESTING_VOLLEY",Code+"");
                            //if (errorCode == 100)
                            if (Code == 200)
                            {
                                //txtNoRecord.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                noFramerListFound.setVisibility(View.GONE);
                               // JSONArray dataArray = responseObject.getJSONArray("data");
                                JSONObject dataobj=object.getJSONObject("data");
                                JSONArray dataArray=dataobj.getJSONArray("data");
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject dataObject = dataArray.getJSONObject(i);
                                    locations.add(dataObject.getString("farmer_location"));
                                    farmer_id.add(dataObject.getString("farmer_id"));
                                    //consignmentDetails.setPictureOfCommodity(dataObject.getString("picture_of_commodity"));
                                    //  consignmentDetails.setVideoOfCommodity(dataObject.getString("video_of_commodity"));
                                    //  consignmentDetails.setMsp(dataObject.getInt("msp"));
                                    //  consignmentDetails.setCommodityName(dataObject.getString("commodity_name"));
                                    names.add(dataObject.getString("farmer_name"));
                                    subscribed_pesticide_vendor.add(dataObject.getString("subscribed_pesticide_vendor"));
                                    invited_by.add(dataObject.getString("invited_by"));

                                    FarmersListModel farmersListModel = new FarmersListModel(dataObject.getString("farmer_name"),
                                            dataObject.getString("farmer_location"), dataObject.getString("subscribed_pesticide_vendor"),
                                            dataObject.getString("invited_by"), dataObject.getString("farmer_id"), dataObject.getString("farmer_contact"));
                                    farmers_list.add(farmersListModel);
                                    //pesticidesVendorFarmersListAdapter2.notifyDataSetChanged();

                                    Log.d("names & locations", dataObject.getString("farmer_name") + " & " + dataObject.getString("farmer_location"));
                                }
                                //pesticidesVendorFarmersListAdapter2.notifyDataSetChanged();

                                if (farmers_list.size() > 0)
                                    binding.txtFarmerList.setVisibility(View.VISIBLE);

                                initRecyclerView();
                                //pesticidesVendorFarmersListAdapter2.notifyDataSetChanged();


                            }
                            //else if (errorCode == 107)
                            else if (Code == 107)
                            {
                                //txtNoRecord.setVisibility(View.VISIBLE);
                                //String message = responseObject.getString("message");
                                String message = object.getString("message");
                                if (prefManager.getUserType().equals("pv")) {
                                    recyclerView.setVisibility(View.GONE);
                                    noFramerListFound.setVisibility(View.VISIBLE);
                                    noFramerListFound.setText("No subscribe Framer List");
                                } else if (prefManager.getUserType().equals("farmer")) {
                                    recyclerView.setVisibility(View.GONE);
                                    noFramerListFound.setVisibility(View.VISIBLE);
                                    noFramerListFound.setText(message);
                                    //Toast.makeText(PesticidesVendorFarmersListActivity.this,message,Toast.LENGTH_SHORT).show();
                                }


                            }

                        } catch (JSONException e) {
                            Toast.makeText(NewChatActivity.this, response, Toast.LENGTH_SHORT).show();
                            Log.e("initNamesandlo", "" + e.toString());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utils.showVolleyError(NewChatActivity.this, error);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", userProfile.getApiToken());
                params.put("user_id", userProfile.getUserId());
                params.put("user_type", userProfile.getUserType());
                return params;
            }

             @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",userProfile.getApiToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);

    }
}
