package com.agriiprince.adapter.farmer;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.agriiprince.R;
import com.agriiprince.databinding.ItemFarmerInfoTabMandiPriceBinding;
import com.agriiprince.databinding.ItemFarmerLandingCropInfoBinding;
import com.agriiprince.model.MandiModel;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.models.FarmerInfoTabModel;
import com.agriiprince.models.HistoricPriceModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class FarmerInfoAdapter extends RecyclerView.Adapter<FarmerInfoAdapter.FarmerInfoViewHolder> {

    private static final String TAG = FarmerInfoAdapter.class.getSimpleName();

    private static int TEXT_VIEW_HEIGHT;
    private static int HEADER_VIEW_HEIGHT;

    private OnInfoItemClickListener mListener;
    private Context mContext;
    private List<FarmerInfoTabModel> mList;

    public FarmerInfoAdapter(Context context, OnInfoItemClickListener listener, boolean firstTime) {
        this.mContext = context;
        this.mListener = listener;
        this.mList = new ArrayList<>();

        HEADER_VIEW_HEIGHT = getPixels(24);
        TEXT_VIEW_HEIGHT = getPixels(20); // change to 40 if bilingual
    }


    @NonNull
    @Override
    public FarmerInfoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemFarmerLandingCropInfoBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_farmer_landing_crop_info, parent, false);
        return new FarmerInfoViewHolder(binding, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull FarmerInfoViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else {
            if (payloads.get(0) instanceof FarmerInfoTabModel) {
                FarmerInfoTabModel infoTabModel = (FarmerInfoTabModel) payloads.get(0);
                populateItem(holder, infoTabModel);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull FarmerInfoViewHolder holder, int position) {
        FarmerInfoTabModel farmerInfoTabModel = mList.get(position);
        Crop crop = farmerInfoTabModel.crop;

        Glide.with(holder.binding.itemInfoTabCropImage)
                .setDefaultRequestOptions(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .load(crop.getImageUrl())
                .into(holder.binding.itemInfoTabCropImage);

        holder.binding.itemInfoTabCropName.setText(crop.cropName);

        holder.binding.itemInfoTabVarietySpinner
                .setAdapter(getSpinnerAdapter(crop.getVarietyNames()));
        holder.binding.itemInfoTabGradeSpinner
                .setAdapter(getSpinnerAdapter(crop.getVariety(farmerInfoTabModel.selectedVariety).getGradeNames()));

        holder.binding.itemInfoTabMandiLabel.setHeight(HEADER_VIEW_HEIGHT);
        holder.binding.itemInfoTabLabelMin.setHeight(TEXT_VIEW_HEIGHT);
        holder.binding.itemInfoTabLabelModal.setHeight(TEXT_VIEW_HEIGHT);
        holder.binding.itemInfoTabLabelMax.setHeight(TEXT_VIEW_HEIGHT);

        holder.binding.itemInfoTabVarietySpinner.setOnItemSelectedListener(holder.varietyItemClickListener);
        holder.binding.itemInfoTabGradeSpinner.setOnItemSelectedListener(holder.gradeItemClickListener);
        holder.binding.itemFarmerLandingCropMap.setOnClickListener(holder.onMapClickListener);
        holder.binding.itemFarmerLandingCropProfile.setOnClickListener(holder.onProfileClickListener);
        holder.binding.itemInfoTabNextDate.setOnClickListener(holder.onNextDateClickListener);
        holder.binding.itemInfoTabPrevDate.setOnClickListener(holder.onPrevDateClickListener);
        holder.binding.itemInfoTabPickDate.setOnClickListener(holder.onPickDateClickListener);

        populateItem(holder, farmerInfoTabModel);
    }

    private void populateItem(FarmerInfoViewHolder holder, FarmerInfoTabModel farmerInfoTabModel) {
        holder.binding.itemInfoTabDay.setText(farmerInfoTabModel.getDay());
        holder.binding.itemInfoTabMonth.setText(farmerInfoTabModel.getMonth());

        holder.binding.itemInfoTabVarietySpinner.setSelection(farmerInfoTabModel.selectedVariety);
        holder.binding.itemInfoTabGradeSpinner.setSelection(farmerInfoTabModel.selectedGrade);

        int childCount = holder.binding.itemInfoTabPricesLayout.getChildCount();
        if (childCount > 1) {
            holder.binding.itemInfoTabPricesLayout.removeViews(1, childCount - 1);
        }

        for (MandiModel mandiModel : farmerInfoTabModel.mandis) {
            String minPrice = "N/A";
            String modalPrice = "N/A";
            String maxPrice = "N/A";

            if (farmerInfoTabModel.historicPriceModels != null) {
                for (HistoricPriceModel historicPriceModel : farmerInfoTabModel.historicPriceModels) {
//                    Log.d(TAG, mandiModel.mandi_name_en + " " + historicPriceModel.getMarket());
                    if (historicPriceModel.getMarket().equalsIgnoreCase(mandiModel.mandi_name_en)) {
                        minPrice = historicPriceModel.getMinPrice();
                        modalPrice = historicPriceModel.getModalPrice();
                        maxPrice = historicPriceModel.getMaxPrice();
                        break;
                    }
                }
            }

            ItemFarmerInfoTabMandiPriceBinding priceBinding
                    = DataBindingUtil.inflate(LayoutInflater.from(holder.binding.itemInfoTabPricesLayout.getContext()),
                    R.layout.item_farmer_info_tab_mandi_price, holder.binding.itemInfoTabPricesLayout, false);

            priceBinding.infoTabPriceMandiName.setHeight(HEADER_VIEW_HEIGHT);
            priceBinding.infoTabPriceMin.setHeight(TEXT_VIEW_HEIGHT);
            priceBinding.infoTabPriceModal.setHeight(TEXT_VIEW_HEIGHT);
            priceBinding.infoTabPriceMax.setHeight(TEXT_VIEW_HEIGHT);

            priceBinding.infoTabPriceMandiName.setText(mandiModel.mandi_name_en);
            priceBinding.infoTabPriceMin.setText(minPrice);
            priceBinding.infoTabPriceModal.setText(modalPrice);
            priceBinding.infoTabPriceMax.setText(maxPrice);

            holder.binding.itemInfoTabPricesLayout.addView(priceBinding.getRoot());
        }
    }

    private int getPixels(float dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, mContext.getResources().getDisplayMetrics());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public FarmerInfoTabModel get(int position) {
        return mList.get(position);
    }

    public void add(FarmerInfoTabModel model) {
        int index = getItemCount();
        mList.add(model);
        notifyItemInserted(index);
    }

    public void removeAll() {
        mList.clear();
        notifyDataSetChanged();
    }

    private ArrayAdapter<String> getSpinnerAdapter(List<String> list) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                mContext, android.R.layout.simple_spinner_item, list);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return arrayAdapter;
    }


    public static class FarmerInfoViewHolder extends RecyclerView.ViewHolder {

        private WeakReference<OnInfoItemClickListener> mListener;

        private ItemFarmerLandingCropInfoBinding binding;

        private FarmerInfoViewHolder(ItemFarmerLandingCropInfoBinding binding, OnInfoItemClickListener listener) {
            super(binding.getRoot());
            this.binding = binding;
            this.mListener = new WeakReference<>(listener);

        }

        private AdapterView.OnItemSelectedListener varietyItemClickListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mListener.get().onChangeVariety(getAdapterPosition(), binding.itemInfoTabVarietySpinner.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };

        private AdapterView.OnItemSelectedListener gradeItemClickListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mListener.get().onChangeGrade(getAdapterPosition(), binding.itemInfoTabGradeSpinner.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };

        private View.OnClickListener onMapClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.get().onClickMap(getAdapterPosition());
            }
        };

        private View.OnClickListener onProfileClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.get().onClickProfile(getAdapterPosition());
            }
        };

        private View.OnClickListener onNextDateClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.get().onClickNextDay(getAdapterPosition());
            }
        };

        private View.OnClickListener onPrevDateClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.get().onClickPrevDay(getAdapterPosition());
            }
        };

        private View.OnClickListener onPickDateClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.get().onClickCalender(getAdapterPosition());
            }
        };

    }

    public interface OnInfoItemClickListener {

        void onChangeVariety(int recyclerPosition, int varietyPosition);

        void onChangeGrade(int recyclerPosition, int gradePosition);

        void onClickNextDay(int position);

        void onClickPrevDay(int position);

        void onClickCalender(int position);

        void onClickMap(int position);

        void onClickProfile(int position);

    }
}
