package com.agriiprince.adapter.farmer;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import com.agriiprince.R;
import com.agriiprince.databinding.ItemDiseaseListBinding;
import com.agriiprince.models.DiseaseListModel;
import com.agriiprince.utils.StringUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class DiseaseListAdapter extends RecyclerView.Adapter<DiseaseListAdapter.DiseaseListViewHolder>
        implements Filterable {

    private static final String TAG = DiseaseListAdapter.class.getSimpleName();

    private final int MAX_TEXT_LENGTH = 200;

    private OnDiseaseListListener mListener;

    private StringUtils mStringUtils;

    private List<DiseaseListModel> mList;
    private List<DiseaseListModel> mFilterList;

    public DiseaseListAdapter(StringUtils stringUtils, OnDiseaseListListener listener) {
        this.mStringUtils = stringUtils;
        this.mList = new ArrayList<>();
        this.mFilterList = new ArrayList<>();
        this.mListener = listener;
    }

    public void addItem(DiseaseListModel item) {
        int index = getItemCount();
        mList.add(item);
        mFilterList.add(item);
        notifyItemInserted(index);
    }

    public void addAllItem(List<DiseaseListModel> list) {
        int index = getItemCount();
        mList.addAll(list);
        mFilterList.addAll(list);
        notifyItemRangeInserted(index, list.size());
    }

    public DiseaseListModel getItem(int position) {
        return mFilterList.get(position);
    }

    @NonNull
    @Override
    public DiseaseListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemDiseaseListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_disease_list
                , viewGroup, false);
        return new DiseaseListViewHolder(binding, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull DiseaseListViewHolder holder, int i) {
        DiseaseListModel diseaseListModel = getItem(i);
        holder.binding.diseaseNumber.setText(String.valueOf(i + 1));
        holder.binding.diseaseName.setText(mStringUtils.getBilingualStringForApi(
                diseaseListModel.getDisease_name(), diseaseListModel.getDisease_name_bi())
        );


        String symptoms = diseaseListModel.getDisease_symptoms();
        String symptoms_bi = diseaseListModel.getDisease_symptoms_bi();
        if (!TextUtils.isEmpty(diseaseListModel.getDisease_symptoms()) &&
                diseaseListModel.getDisease_symptoms().length() > MAX_TEXT_LENGTH) {
            symptoms = diseaseListModel.getDisease_symptoms().substring(0, MAX_TEXT_LENGTH);
        }
        if (!TextUtils.isEmpty(diseaseListModel.getDisease_symptoms_bi()) &&
                diseaseListModel.getDisease_symptoms_bi().length() > MAX_TEXT_LENGTH) {
            symptoms_bi = diseaseListModel.getDisease_symptoms_bi().substring(0, MAX_TEXT_LENGTH);
        }

        holder.binding.diseaseSymptoms.setText(mStringUtils.getBilingualStringForApi(symptoms, symptoms_bi));

        holder.binding.imageScrollView.removeAllViews();
        if (!TextUtils.isEmpty(diseaseListModel.getCrop_images())) {
            List<String> urls = DiseaseListModel.getImageUrls(diseaseListModel.getCrop_images());
            for (String url : urls) {

                ViewGroup.LayoutParams layoutParams =
                        new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                AppCompatImageView imageView = new AppCompatImageView(holder.binding.imageScrollView.getContext());
                imageView.setLayoutParams(layoutParams);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setAdjustViewBounds(true);
                imageView.setPadding(2, 2, 2, 2);

                holder.binding.imageScrollView.addView(imageView);

                Glide.with(imageView)
                        .load(url)
                        .apply(new RequestOptions()
                                .diskCacheStrategy(DiskCacheStrategy.ALL))
                        .into(imageView);
            }
        }

        holder.binding.diseaseNext.setText(mStringUtils.getLocalizedString(R.string.learn_more, true));
        holder.binding.listLayout.setOnClickListener(holder.onClickItem);
        holder.binding.diseaseNext.setOnClickListener(holder.onClickItem);
    }

    @Override
    public int getItemCount() {
        return mFilterList.size();
    }


    @Override
    public Filter getFilter() {
        return filter;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString().toLowerCase();
                ArrayList<DiseaseListModel> filteredList = new ArrayList<>();

                for (DiseaseListModel model : mList) {
                    String string = mStringUtils.getBilingualStringForApi(model.getDisease_name(), model.getDisease_name_bi());
                    if (string.toLowerCase().contains(constraint))
                        filteredList.add(model);
                }
                results.count = filteredList.size();
                results.values = filteredList;

            } else {
                results.count = mList.size();
                results.values = mList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mFilterList = (ArrayList<DiseaseListModel>) results.values;
            notifyDataSetChanged();
        }
    };

    class DiseaseListViewHolder extends RecyclerView.ViewHolder {

        WeakReference<OnDiseaseListListener> listener;
        ItemDiseaseListBinding binding;

        private DiseaseListViewHolder(ItemDiseaseListBinding binding, OnDiseaseListListener listListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.listener = new WeakReference<>(listListener);
        }

        private View.OnClickListener onClickItem = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.get().onClickItem(getAdapterPosition());
            }
        };

    }

    public interface OnDiseaseListListener {
        void onClickItem(int position);
    }
}
