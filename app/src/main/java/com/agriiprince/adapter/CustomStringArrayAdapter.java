package com.agriiprince.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

public class CustomStringArrayAdapter extends ArrayAdapter<String> implements Filterable {
    private static final String TAG = "CustomStringAdapter";

    private List<String> originalList;

    public CustomStringArrayAdapter(@NonNull Context context, int resource, @NonNull List<String> objects) {
        super(context, resource, objects);
        this.originalList = new ArrayList<>(objects);
    }


    public void addAllString(List<String> data) {
        originalList.clear();
        originalList.addAll(data);
    }

    public List<String> getAllItems() {
        return originalList;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return filter;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            Log.d(TAG, constraint.toString());
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString().toLowerCase();
                ArrayList<String> filteredValues = new ArrayList<>();

                for (String value : originalList) {
                    if (value.toLowerCase().contains(constraint)) {
                        filteredValues.add(value);
                    }
                }
                results.count = filteredValues.size();
                results.values = filteredValues;

            } else {
                results.count = originalList.size();
                results.values = originalList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<String> filterList = (ArrayList<String>) results.values;
            if (results.count > 0) {
                clear();
                for (String names : filterList) {
                    add(names);
                }
                notifyDataSetChanged();
            } else {
                clear();
            }
        }
    };
}
