package com.agriiprince.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.TopicModel;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.agriiprince.utils.Utils.showLog;

public class RssChooseAdapter extends RecyclerView.Adapter<RssChooseAdapter.FeedModelViewHolder> {

    static List<TopicModel> topics;
    Context c;
    PrefManager prefManager;

    ProgressDialog progressDialog;

    private UserProfile mUserProfile;

    public class FeedModelViewHolder extends RecyclerView.ViewHolder {
        private View rssView;

        public FeedModelViewHolder(View v) {
            super(v);
            rssView = v;
            LinearLayout ll = rssView.findViewById(R.id.RssItemLL);
            ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    final boolean opted = topics.get(getAdapterPosition()).isInterested();
                    progressDialog.show();
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.POST_RSS_UPDATE,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    progressDialog.dismiss();
                                    showLog("Post RSS Update Response " + response);
                                    //JSONArray jsonArray = null;
                                    try {
                                        Log.d("TESTING_VOLLEY",response);
                                        //jsonArray = new JSONArray(response);
                                        //JSONObject jsonObject = jsonArray.getJSONObject(0);
                                        JSONObject object=new JSONObject(response);
                                        //int error_code = jsonObject.getInt("error_code");
                                        int code = object.getInt("code");
                                        Log.d("TESTING_VOLLEY", String.valueOf(code));
                                        String existing = prefManager.getRSStopics();
                                        TopicModel topic = topics.get(getAdapterPosition());
                                       // if (error_code == 100) {
                                        if (code == 200) {
                                            //success
                                            if (opted) {
                                                Toast.makeText(c.getApplicationContext(), "Unsubscribed from topic : " + topics.get(getAdapterPosition()).getTopic(), Toast
                                                        .LENGTH_SHORT).show();
                                                topics.get(getAdapterPosition()).setInterested(false);
                                                if (existing != null) {
                                                    existing = existing.replace(topic.getTopic(), "");
                                                    existing = existing.replace(",,", ",");
                                                }
                                            } else {
                                                Toast.makeText(c.getApplicationContext(), "Subscribed to topic : " + topics.get(getAdapterPosition()).getTopic(), Toast
                                                        .LENGTH_SHORT).show();
                                                topics.get(getAdapterPosition()).setInterested(true);
                                                if (existing == null) {
                                                    existing = topic.getTopic();
                                                } else {
                                                    if (existing.length() == 0) {
                                                        existing = topic.getTopic();
                                                    } else {
                                                        existing += "," + topic.getTopic();
                                                    }
                                                }
                                            }
                                            prefManager.setRSStopics(existing);
                                            notifyItemChanged(getAdapterPosition());
                                        }//else if (error_code == 107) {
                                         else if (code == 107) {
                                            Toast.makeText(c.getApplicationContext(), "Topic subscription failed", Toast
                                                    .LENGTH_SHORT).show();
                                       // } else if (error_code == 101) {
                                        } else if (code == 101) {
                                            Toast.makeText(c.getApplicationContext(), "Something went wrong.", Toast
                                                    .LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError) {
                                Toast.makeText(rssView.getContext(), "Slow internet...Kindly check internet to proceed seamlessly.", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof NoConnectionError) {
                                Toast.makeText(rssView.getContext(), "No internet connection ", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<>();
                            params.put("api_token", mUserProfile.getApiToken());

                            params.put("user_type", "farmer");
                            params.put("user_id", prefManager.getUserId());
                            params.put("rss_feed_id", topics.get(getAdapterPosition()).getTopic());
                            if (opted) {
                                params.put("mode", "opt_out");
                            } else {
                                params.put("mode", "opt_in");
                            }
                            showLog("RSS update params : " + params.toString());
                            return params;
                        }
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> header = new HashMap<>();
                            header.put("Authentication", mUserProfile.getApiToken());
                            return header;
                        }
                    };
                    AppController.getInstance().addToRequestQueue(stringRequest);
                }
            });
        }
    }

    public RssChooseAdapter(List<TopicModel> topics, Context context, PrefManager prefManager) {
        this.topics = topics;
        c = context;
        this.prefManager = prefManager;

        this.mUserProfile = new UserProfile(context);

        progressDialog = new ProgressDialog(c);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(c.getResources().getString(R.string.please_wait));
    }

    @Override
    public FeedModelViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_interested_rss, parent, false);
        FeedModelViewHolder holder = new FeedModelViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(FeedModelViewHolder holder, final int position) {
        final TopicModel topic = topics.get(position);
        ((TextView) holder.rssView.findViewById(R.id.liInterestedTopic)).setText(topic.getTopic());
        LinearLayout ll = holder.rssView.findViewById(R.id.RssItemLL);

        if (topic.isInterested()) {
            ll.setBackgroundColor(Color.parseColor("#C7EA46"));
        } else {
            ll.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
    }

    @Override
    public int getItemCount() {
        return topics.size();
    }
}