package com.agriiprince.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.databinding.ActivityColdStorageItemBinding;
import com.agriiprince.mvvm.retrofit.model.coldstorage.ColdStorageList;
import com.agriiprince.utils.StringUtils;

import java.lang.ref.WeakReference;
import java.util.List;

public class ColdStorageAdapter extends RecyclerView.Adapter<ColdStorageAdapter.CsViewHolder> {

    private OnClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    private List<ColdStorageList> objects;
    private StringUtils utils;
    Context context;

    public ColdStorageAdapter(Context context, List<ColdStorageList> objects) {
        this.objects = objects;
        this.context = context;
        this.utils = AppController.getInstance().getStringUtils();

    }

    @NonNull
    @Override
    public CsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_cold_storage_item, viewGroup, false);
        return new CsViewHolder(view, mOnItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CsViewHolder holder, final int i) {

        ColdStorageList model = objects.get(i);

//        Log.d("cs adapter:     ", model.cold_storage_name + "   " + model.cold_storage_address);

        if (model != null) {
            //holder.binding.name.setText(utils.getBilingualStringForApi(model.cold_storage_name, model.cold_storage_name_bi));
            holder.binding.name.setText(utils.getBilingualStringForApi(model.getColdStorageName(), model.getColdStorageNameBi()));
            //holder.binding.address.setText(utils.getBilingualStringForApi(model.cold_storage_address, model.cold_storage_address_bi));
            holder.binding.address.setText(utils.getBilingualStringForApi(model.getColdStorageAddress(), model.getColdStorageAddressBi()));

            //if (TextUtils.isEmpty(model.cold_storage_contact) || model.cold_storage_contact.length() < 10)
            if (TextUtils.isEmpty(model.getColdStorageContact()) || model.getColdStorageContact().length() < 10)

            {
                holder.binding.call.setVisibility(View.GONE);
            }
        }

    }


    @Override
    public int getItemCount() {
        if (objects == null) return 0;
        return objects.size();
    }


    public int getCount() {
        if (objects == null) return 0;

        return objects.size();
    }

    public ColdStorageList getItem(int position) {
        return objects.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    public static class CsViewHolder extends RecyclerView.ViewHolder {

        WeakReference<OnClickListener> mListener;
        ActivityColdStorageItemBinding binding;

        public CsViewHolder(@NonNull View itemView, final OnClickListener listener) {
            super(itemView);
            this.mListener = new WeakReference<>(listener);
            binding = DataBindingUtil.bind(itemView);

            itemView.findViewById(R.id.call).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null && getAdapterPosition() > -1) {
                        mListener.get().onClickCall(getAdapterPosition());
                    }
                }
            });

        }
    }

    public interface OnClickListener {
        void onClickCall(int position);
    }
}
