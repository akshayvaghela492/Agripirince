package com.agriiprince.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.activities.ChatOneToOneActivity;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.model.FarmersListModel;

import java.util.ArrayList;

public class PesticidesVendorFarmersListAdapter extends RecyclerView.Adapter<PesticidesVendorFarmersListAdapter.ViewHolder> implements Filterable {


    private ArrayList<String> names = new ArrayList<>();
    private ArrayList<String> location = new ArrayList<>();
    private ArrayList<String> subscribed_pesticide_vendor = new ArrayList<>();
    private ArrayList<String> invited_by = new ArrayList<>();
    private ArrayList<String> farmer_id = new ArrayList<>();
    private ArrayList<FarmersListModel> farmers_list = new ArrayList<>();

    private ArrayList<FarmersListModel> farmers_list_filter = new ArrayList<>();
    MyFilter myFilter;

    private Context context;
    private PrefManager prefManager;

    public PesticidesVendorFarmersListAdapter(ArrayList<String> names, ArrayList<String> location, Context context,
                                              ArrayList<String> subscribed_pesticide_vendor, ArrayList<String> invited_by, ArrayList<String> farmer_id) {
        this.names = names;
        this.location = location;
        this.subscribed_pesticide_vendor = subscribed_pesticide_vendor;
        this.invited_by = invited_by;
        this.farmer_id = farmer_id;
        this.context = context;
        prefManager = new PrefManager(context);
    }

    public PesticidesVendorFarmersListAdapter(Context context, ArrayList<FarmersListModel> farmers_list) {
        this.context = context;
        this.farmers_list = farmers_list;
        this.farmers_list_filter = farmers_list;
        prefManager = new PrefManager(context);
        Log.e("adapter", "" + farmers_list.toString());
    }

    public ArrayList<String> getName() {
        return names;
    }

    public ArrayList<String> getLocation() {
        return location;
    }

    public void setName(ArrayList<String> names) {
        this.names = names;
    }

    public void setLocation(ArrayList<String> location) {
        this.location = location;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_farmers_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        Log.e("position", "" + position);
        final FarmersListModel farmersListModel = farmers_list.get(position);
        if (prefManager.getUserType().equalsIgnoreCase("pv")) {
            holder.farmerName.setText("" + farmersListModel.getName());
            holder.location.setText("" + farmersListModel.getLocations());

            holder.chat.setVisibility(View.VISIBLE);

            holder.chat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //context.startActivity(new Intent(context, FarmerPesticideVendorChatActivity.class));
                    Intent intent = new Intent(context, ChatOneToOneActivity.class);
                    intent.putExtra("call_from", prefManager.getUserType());
                    intent.putExtra("receiver_id", farmersListModel.getFarmer_id());
                    intent.putExtra("receiver_name", farmersListModel.getName());
                    context.startActivity(intent);
                }
            });

        } else if (prefManager.getUserType().equalsIgnoreCase("farmer")) {

            if (!farmersListModel.getFarmer_id().equalsIgnoreCase(prefManager.getUserId())) {
                holder.farmerName.setText("" + farmersListModel.getName());
                if (farmersListModel.getLocations().equalsIgnoreCase("") || TextUtils.isEmpty(farmersListModel.getLocations())) {
                    holder.location.setText("Not Available");
                } else {
                    holder.location.setText("" + farmersListModel.getLocations());
                }
                if (farmersListModel.getFarmer_id().equalsIgnoreCase(prefManager.getUserId())) {
                    holder.chat.setVisibility(View.GONE);
                } else {
                    holder.chat.setVisibility(View.VISIBLE);
                    holder.chat.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(context, ChatOneToOneActivity.class);
                            intent.putExtra("call_from", prefManager.getUserType());
                            intent.putExtra("receiver_id", farmersListModel.getFarmer_id());
                            intent.putExtra("receiver_name", farmersListModel.getName());
                            context.startActivity(intent);
                        }
                    });
                }
            } else {
                holder.layout.setVisibility(View.GONE);
                holder.layout.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
            }
        }


        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context,"Farmer's Orders",Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return farmers_list.size();
    }

    /*public void filterList(ArrayList<FarmersListModel> filterdNames) {
        this.farmers_list = filterdNames;
        notifyDataSetChanged();
    }*/

    @Override
    public Filter getFilter() {
        if (myFilter == null) {
            myFilter = new MyFilter();
        }
        return myFilter;
    }

    /*public void filterList2(ArrayList<String> filterdNames) {
        this.location = filterdNames;
        notifyDataSetChanged();
    }*/

    private class MyFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (filterString.length() > 0 && filterString != null) {
                ArrayList<FarmersListModel> filtelist = new ArrayList<FarmersListModel>();
                for (int i = 0; i < farmers_list_filter.size(); i++) {
                    //filerValue = filterdata.get(i);
                    //HashMap<String,String> filterdatahash;
                    if ((farmers_list_filter.get(i).getName().toLowerCase()).startsWith(filterString) || farmers_list_filter.get(i).getFarmer_contact().startsWith(filterString)) {

                        FarmersListModel filterFarmersListModel = farmers_list_filter.get(i);
                        filtelist.add(filterFarmersListModel);
                    }


                }

                result.count = filtelist.size();
                result.values = filtelist;
            } else {

                result.count = farmers_list_filter.size();
                result.values = farmers_list_filter;
            }
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            farmers_list = (ArrayList<FarmersListModel>) results.values;
            notifyDataSetChanged();
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView farmerName, location, chat;
        RelativeLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            farmerName = itemView.findViewById(R.id.name);
            location = itemView.findViewById(R.id.location);
            chat = itemView.findViewById(R.id.chat);
            layout = itemView.findViewById(R.id.parent_layout);
        }
    }

}
