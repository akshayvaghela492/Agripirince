package com.agriiprince.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.retrofit.model.ca.CaListModel;
import com.agriiprince.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class CAListAdapter extends RecyclerView.Adapter<CAListAdapter.MyViewHolder> {

    private ArrayList<CaListModel> mCAList = new ArrayList<>();

    private StringUtils utils;

    public CAListAdapter(Context context) {
        this.utils = AppController.getInstance().getStringUtils();
    }

    public void setOnItemClickListener(OnClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public interface OnClickListener {
        void onClickCall(CaListModel position, int index);
    }

    private OnClickListener mOnItemClickListener;

    public void setData(List<CaListModel> caListModelArrayList) {
        mCAList.clear();
        mCAList.addAll(caListModelArrayList);
        notifyDataSetChanged();
    }

    public void clearData() {
        mCAList.clear();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CAListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ca_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final CAListAdapter.MyViewHolder holder, final int position) {
        CaListModel item = mCAList.get(position);

        if (TextUtils.isEmpty(item.getCaName())) {
            holder.caName.setVisibility(View.GONE);
        } else {
            holder.caName.setVisibility(View.VISIBLE);
            holder.caName.setText(utils.getBilingualStringForApi(item.getCaName(), item.getCaNameBi()));
        }

        if (TextUtils.isEmpty(item.getCa_business_name())) {
            if (TextUtils.isEmpty(item.getCaAddress())) {
                holder.ca_business_name.setVisibility(View.GONE);
            } else {
                holder.ca_business_name.setVisibility(View.VISIBLE);
                holder.ca_business_name.setText(utils.getBilingualStringForApi(item.getCaAddress(), item.getCaAddressBi()));
            }
        } else {
            holder.ca_business_name.setVisibility(View.VISIBLE);
            holder.ca_business_name.setText(utils.getBilingualStringForApi(item.getCa_business_name(), item.getCa_business_name_bi()));
        }

        if (!TextUtils.isEmpty(item.getCommodity())) {
            holder.ca_commodity.setVisibility(View.VISIBLE);
            holder.ca_commodity.setText(utils.getBilingualStringForApi(item.getCommodity(), item.getCommodityBi()));
        } else {
            holder.ca_commodity.setVisibility(View.GONE);
        }

        holder.ca_contact_one.setVisibility(View.GONE);
        holder.ca_contact_two.setVisibility(View.GONE);

        // check how many phone number are there
        if (!TextUtils.isEmpty(item.getCaContact())) {
            String[] phones = item.getCaContact().split(",");
            if (phones.length > 0 && Patterns.PHONE.matcher(phones[0]).matches()) {
                holder.ca_contact_one.setVisibility(View.VISIBLE);

                if (phones.length > 1 && Patterns.PHONE.matcher(phones[1]).matches())
                    holder.ca_contact_two.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mCAList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView caName, ca_commodity, ca_business_name;
        ImageView ca_contact_one;
        ImageView ca_contact_two;

        MyViewHolder(View itemView) {
            super(itemView);

            caName = itemView.findViewById(R.id.ca_name);
            ca_contact_one = itemView.findViewById(R.id.ca_contact_one);
            ca_contact_two = itemView.findViewById(R.id.ca_contact_two);
            ca_commodity = itemView.findViewById(R.id.ca_commodity);
            ca_business_name = itemView.findViewById(R.id.ca_business_name);

            ca_contact_one.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null && getAdapterPosition() > -1) {
                        mOnItemClickListener.onClickCall(mCAList.get(getAdapterPosition()), 0);
                    }
                }
            });

            ca_contact_two.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null && getAdapterPosition() > -1) {
                        mOnItemClickListener.onClickCall(mCAList.get(getAdapterPosition()), 1);
                    }
                }
            });
        }
    }
}
