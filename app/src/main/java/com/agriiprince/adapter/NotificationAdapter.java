package com.agriiprince.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.databinding.ItemNotificationBinding;
import com.agriiprince.models.NotificationModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    private Context context;
    List<com.agriiprince.models.NotificationModel.NotificationData> notificationDataList;

    private SimpleDateFormat inputFormat;
    private SimpleDateFormat outputFormat;

    public NotificationAdapter(Context context, List<com.agriiprince.models.NotificationModel.NotificationData> notificationData) {
        this.context = context;
        this.notificationDataList = notificationData;
        this.inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        this.outputFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);

        Collections.sort(this.notificationDataList, new NotificationComparator());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        com.agriiprince.models.NotificationModel.NotificationData notificationData = notificationDataList.get(position);
        holder.itemNotificationBinding.txtNotificationMessage.setText(notificationData.getMessage());
        holder.itemNotificationBinding.txtNotificationTime.setText(formatDate(notificationData.getCreated_on()));
        holder.itemNotificationBinding.txtNotificationType.setText(notificationData.getMessage_type());
    }

    @Override
    public int getItemCount() {
        return notificationDataList.size();
    }

    private String formatDate(String date) {
        try {
            return outputFormat.format(inputFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ItemNotificationBinding itemNotificationBinding;

        MyViewHolder(View view) {
            super(view);
            itemNotificationBinding = DataBindingUtil.bind(view);
        }

        public ItemNotificationBinding getItemNotificationBinding() {
            return itemNotificationBinding;
        }
    }


    private class NotificationComparator implements Comparator<NotificationModel.NotificationData> {
        @Override
        public int compare(NotificationModel.NotificationData o1, NotificationModel.NotificationData o2) {
            return o1.getId().compareTo(o2.getId());
        }

    }
}

