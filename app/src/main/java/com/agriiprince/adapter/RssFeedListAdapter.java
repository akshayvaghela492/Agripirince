package com.agriiprince.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.model.RssFeedModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class RssFeedListAdapter
        extends RecyclerView.Adapter<RssFeedListAdapter.FeedModelViewHolder> {

    private Context mContext;

    private OnClickRssFeedItemListener mListener;

    private List<RssFeedModel> mRssFeedModels;

    private SimpleDateFormat simpleDateFormat;

    protected static class FeedModelViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView source;
        private TextView date;

        private AppCompatTextView title;
        private TextView description;
        private TextView link;

        private ImageView image;

        private WeakReference<OnClickRssFeedItemListener> mListener;

        private FeedModelViewHolder(View v, OnClickRssFeedItemListener listener) {
            super(v);

            this.mListener = new WeakReference<>(listener);

            image = v.findViewById(R.id.item_rss_feed_image);

            source = v.findViewById(R.id.item_rss_feed_source);
            date = v.findViewById(R.id.item_rss_feed_date);

            title = v.findViewById(R.id.item_rss_feed_title);
            description = v.findViewById(R.id.rssDescriptionText);
            link = v.findViewById(R.id.rssLinkText);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null)
                mListener.get().onClickItem(getAdapterPosition());
        }
    }

    public RssFeedListAdapter(Context context, List<RssFeedModel> rssFeedModels) {
        this.mContext = context;
        this.mRssFeedModels = rssFeedModels;

        this.simpleDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
    }

    public void addAll(List<RssFeedModel> models) {
        int index = getItemCount();
        mRssFeedModels.addAll(models);
        notifyItemRangeInserted(index, models.size());
    }


    public void addOnClickListener(OnClickRssFeedItemListener listener) {
        this.mListener = listener;
    }

    @NonNull
    @Override
    public FeedModelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int type) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_rss_feed, parent, false);

        return new FeedModelViewHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedModelViewHolder holder, int position) {
        final RssFeedModel rssFeedModel = mRssFeedModels.get(position);

        holder.title.setText(rssFeedModel.getTitle());
        holder.description.setText(rssFeedModel.getDescription());
        holder.source.setText(rssFeedModel.getSource());

        holder.date.setText(simpleDateFormat.format(rssFeedModel.getDate()));

        if (TextUtils.isEmpty(rssFeedModel.getImg())) {
            Glide.with(mContext)
                    .load(R.drawable.ic_image_black_24dp)
                    .into(holder.image);

        } else {
            Glide.with(mContext)
                    .setDefaultRequestOptions(
                            new RequestOptions()
                                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                                    .placeholder(R.drawable.ic_ap_logo))
                    .load(rssFeedModel.getImg())
                    .into(holder.image);
        }

    }

    @Override
    public int getItemCount() {
        return mRssFeedModels.size();
    }

    public RssFeedModel get(int position) {
        return mRssFeedModels.get(position);
    }


    public interface OnClickRssFeedItemListener {
        void onClickItem(int position);
    }
}