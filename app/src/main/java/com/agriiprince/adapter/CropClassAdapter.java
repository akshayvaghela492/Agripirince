package com.agriiprince.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.databinding.ItemCropClassBinding;
import com.agriiprince.mvvm.model.crop.CropClass;
import com.agriiprince.utils.StringUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.lang.ref.WeakReference;
import java.util.List;

public class CropClassAdapter extends RecyclerView.Adapter<CropClassAdapter.CropClassViewHolder> {

    private final String TAG = CropClassAdapter.class.getSimpleName();
    private CropClassListener mListener;

    private List<CropClass> mList;
    private StringUtils utils;

    public CropClassAdapter(Context context, List<CropClass> cropClassList, CropClassListener listener) {
        this.mList = cropClassList;
        this.utils = AppController.getInstance().getStringUtils();
        this.mListener = listener;
    }

    public void add(CropClass cropClass) {
        int index = getItemCount();
        mList.add(cropClass);
        notifyItemInserted(index);
    }

    public void addAll(List<CropClass> list) {
        int index = getItemCount();
        mList.addAll(list);
        notifyItemRangeInserted(index, list.size());
    }

    public CropClass getItem(int position) {
        return mList.get(position);
    }

    @NonNull
    @Override
    public CropClassViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_crop_class, viewGroup, false);
        return new CropClassViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CropClassViewHolder holder, int i) {
        CropClass cropClass = mList.get(i);
//        Log.d(TAG, "position: " + i + " name: " + cropClass.getCrop_class());
        holder.binding.className.setText(utils.getBilingualStringForApi(cropClass.getCrop_class(), cropClass.getCrop_class_bi()));

        String url = CropClass.getImageUrl(cropClass.getCrop_class());
        Log.d("crop_image_url ","class "+url);

        Glide.with(holder.binding.classImage)
                .setDefaultRequestOptions(new RequestOptions()
                        .placeholder(R.drawable.ap_logo_final)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .load(url)
                .into(holder.binding.classImage);

        holder.binding.cropClassContainer.setOnClickListener(holder.onClickClass);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class CropClassViewHolder extends RecyclerView.ViewHolder {

        WeakReference<CropClassListener> mItemListener;
        ItemCropClassBinding binding;

        private CropClassViewHolder(@NonNull View itemView, CropClassListener listener) {
            super(itemView);
            this.mItemListener = new WeakReference<>(listener);
            this.binding = DataBindingUtil.bind(itemView);
        }

        private View.OnClickListener onClickClass = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemListener.get().onClickItem(getAdapterPosition());
            }
        };
    }

    public interface CropClassListener {
        void onClickItem(int position);
    }
}
