package com.agriiprince.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.model.ChatMessage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ChatMessageAdapter extends RecyclerView.Adapter<ChatMessageAdapter.ViewHolder> {

    private String receiver_id;
    private int SELF = 786;
    private List<ChatMessage> messages = new ArrayList<>();


    private SimpleDateFormat dateInputFormat;
    private SimpleDateFormat dateOutputFormat;

    public ChatMessageAdapter(List<ChatMessage> messages, String receiver_id) {
        this.receiver_id = receiver_id;
        this.messages.clear();
        this.messages.addAll(messages);


        this.dateInputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        this.dateOutputFormat = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.ENGLISH);
    }

    public void setData(List<ChatMessage> messages) {
        this.messages.clear();
        this.messages.addAll(messages);
        notifyDataSetChanged();
    }

    public void addData(ChatMessage message) {
        this.messages.add(message);
        notifyItemInserted(messages.size() - 1);
    }

    @Override
    public int getItemViewType(int position) {
        ChatMessage message = messages.get(position);
        if (!message.getReceiverId().equalsIgnoreCase(receiver_id)) {
            return SELF;
        }
        return position;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layoutId = R.layout.item_chat_flip;
        if (viewType == SELF) {
            layoutId = R.layout.item_chat_self;
        }
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ChatMessage message = messages.get(position);
        holder.textViewMessage.setText(message.getMessage());

        try {
            String tag = dateOutputFormat.format(dateInputFormat.parse(message.getTime()));
            if (!message.getChat_type().equalsIgnoreCase("personal") &&
                    !message.getSender_id().equalsIgnoreCase(receiver_id)) {
                tag = message.getSender_name() + " @ " + tag;
            }

            holder.textViewTime.setText(tag);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewMessage;
        public TextView textViewTime;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewMessage = itemView.findViewById(R.id.textViewMessage);
            textViewTime = itemView.findViewById(R.id.textViewTime);
        }
    }
}