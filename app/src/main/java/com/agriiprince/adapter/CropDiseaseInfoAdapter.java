package com.agriiprince.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.model.CropDiseaseInfoModel;

import java.util.List;

public class CropDiseaseInfoAdapter extends RecyclerView.Adapter<CropDiseaseInfoAdapter.FeedModelViewHolder> {

    List<CropDiseaseInfoModel> entries;

    ProgressDialog progressDialog;

    public CropDiseaseInfoAdapter(List<CropDiseaseInfoModel> entries, Context context) {
        this.entries = entries;
    }

    @Override
    public FeedModelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int type) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_diseaseinfo, parent, false);

        return new CropDiseaseInfoAdapter.FeedModelViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedModelViewHolder holder, final int position) {
        final CropDiseaseInfoModel entry = entries.get(position);
        TextView heading = (TextView) holder.rssView.findViewById(R.id.tv_disease_heading);
        TextView content = (TextView) holder.rssView.findViewById(R.id.tv_disease_content);
        heading.setText(entry.heading);
        content.setText(entry.content);

    }

    @Override
    public int getItemCount() {
        return entries.size();
    }


    public void addItem(CropDiseaseInfoModel model) {
        int index = getItemCount();
        entries.add(model);
        notifyItemInserted(index);
    }


    public class FeedModelViewHolder extends RecyclerView.ViewHolder {
        private View rssView;

        private FeedModelViewHolder(View v) {
            super(v);
            rssView = v;
        }
    }
}