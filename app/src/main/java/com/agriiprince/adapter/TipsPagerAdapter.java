package com.agriiprince.adapter;

import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.model.FarmerTip;

import java.util.ArrayList;
import java.util.List;

public class TipsPagerAdapter extends PagerAdapter {

    private OnClickTipListener mListener;

    private List<CardView> mCardViews;

    private List<FarmerTip> mFarmerTips;

    public TipsPagerAdapter() {
        this.mCardViews = new ArrayList<>();
        this.mFarmerTips = new ArrayList<>();
    }



    public void setOnTipClickListener(OnClickTipListener listener) {
        mListener = listener;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.item_farmer_tip, container, false);

        container.addView(view);

        FarmerTip farmerTip = mFarmerTips.get(position);

        CardView cardView = view.findViewById(R.id.item_farmer_tip_card_view);
        LinearLayout linearLayout = view.findViewById(R.id.item_farmer_tip_linear_layout);
        ImageView imageView = view.findViewById(R.id.item_farmer_tip_logo);
        AppCompatButton button = view.findViewById(R.id.item_farmer_tip_try_button);
        TextView title = view.findViewById(R.id.item_farmer_tip_title);
        TextView body = view.findViewById(R.id.item_farmer_tip_body);

        int[] colors = farmerTip.getGradientColor();

        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setOrientation(GradientDrawable.Orientation.BL_TR);
        gradientDrawable.setColors(colors);

        linearLayout.setBackground(gradientDrawable);
        imageView.setImageResource(farmerTip.getImageRes());
        title.setText(farmerTip.getTitle());
        body.setText(farmerTip.getBody());
        button.setTag(position);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) mListener.onClickTry((int) v.getTag());
            }
        });

        mCardViews.set(position, cardView);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
        mCardViews.set(position, null);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return mFarmerTips.size();
    }

    public void addFarmerTip(FarmerTip farmerTip) {
        mCardViews.add(null);
        mFarmerTips.add(farmerTip);
        notifyDataSetChanged();
    }

    public void addFarmerTipByPos(FarmerTip farmerTip, int Pos) {
        mCardViews.remove(Pos);
        mCardViews.add(Pos, null);

        mFarmerTips.remove(Pos);
        mFarmerTips.add(Pos, farmerTip);
        notifyDataSetChanged();
    }

    public interface OnClickTipListener {
        void onClickTry(int position);
    }
}
