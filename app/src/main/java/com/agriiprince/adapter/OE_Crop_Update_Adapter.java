package com.agriiprince.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.activities.oe.OE_UpdateCropPrice;
import com.agriiprince.model.CropDetailModel;

import java.util.List;

import static com.agriiprince.utils.Utils.showLog;

public class OE_Crop_Update_Adapter extends RecyclerView.Adapter<OE_Crop_Update_Adapter.FeedModelViewHolder> {

    static List<CropDetailModel> entries;
    Context c;

    ProgressDialog progressDialog;


    public class FeedModelViewHolder extends RecyclerView.ViewHolder {
        private View view;

        public FeedModelViewHolder(View v) {
            super(v);
            view = v;
            TextView updateButton = (TextView) view.findViewById(R.id.buttonTV_update);
            ImageView callButton = (ImageView) view.findViewById(R.id.call);

            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showLog("update" + getAdapterPosition());
                    showLog("cropname:" + entries.get(getAdapterPosition()).crop_name);
                    showLog("variety:" + entries.get(getAdapterPosition()).crop_variety);

                    final AlertDialog alertDialog = new AlertDialog.Builder(c).create();
                    TextView title = new TextView(c);
                    title.setText("Update Price");
                    title.setPadding(20, 20, 20, 10);   // Set Position
                    title.setTextColor(Color.BLACK);
                    title.setTextSize(20);
                    alertDialog.setCustomTitle(title);
                    EditText etPrice = new EditText(c);
                    etPrice.setHint("Price");
                    etPrice.setPadding(20, 20, 20, 20);
                    alertDialog.setView(etPrice);
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Perform Action on Button = NOTHING
                        }
                    });
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Update", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //TODO Update price
                        }
                    });
                    new Dialog(c);
                    alertDialog.show();
                }
            });

            callButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO open directory
                }
            });
        }
    }

    public OE_Crop_Update_Adapter(List<CropDetailModel> entries, OE_UpdateCropPrice context) {
        this.entries = entries;
        c = context;
    }

    @Override
    public OE_Crop_Update_Adapter.FeedModelViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_oe_update_crop_price, parent, false);
        OE_Crop_Update_Adapter.FeedModelViewHolder holder = new OE_Crop_Update_Adapter.FeedModelViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(OE_Crop_Update_Adapter.FeedModelViewHolder holder, final int position) {
        final CropDetailModel entry = entries.get(position);
        TextView name = (TextView) holder.view.findViewById(R.id.tv_crop_name);
        TextView variety = (TextView) holder.view.findViewById(R.id.tv_crop_variety);
        name.setText(entry.crop_name);
        variety.setText(entry.crop_variety);

    }

    @Override
    public int getItemCount() {
        return entries.size();
    }


}