package com.agriiprince.adapter.rss;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.databinding.ItemRssArrangeByProviderBinding;
import com.agriiprince.model.RssModel;
import com.agriiprince.utils.StringUtils;

import java.lang.ref.WeakReference;
import java.util.List;

public class RssArrangeByProviderAdapter extends RecyclerView.Adapter<RssArrangeByProviderAdapter.RssProviderViewHolder> {

    List<RssModel> mList;

    StringUtils mStringUtils;

    OnClickRssProviderListener mListener;

    public RssArrangeByProviderAdapter(StringUtils stringUtils, List<RssModel> list, OnClickRssProviderListener listener) {
        this.mStringUtils = stringUtils;
        this.mList = list;
        this.mListener = listener;
    }

    public void addAll(List<RssModel> list) {
        int index = getItemCount();
        mList.addAll(list);
        notifyItemRangeChanged(index, list.size());
    }

    public void add(RssModel model) {
        int index = getItemCount();
        mList.add(model);
        notifyItemInserted(index);
    }

    public RssModel get(int position) {
        return mList.get(position);
    }

    @NonNull
    @Override
    public RssProviderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_rss_arrange_by_provider, viewGroup, false);
        return new RssProviderViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RssProviderViewHolder holder, int i) {
        RssModel rssModel = get(i);

        if (isNotEmpty(rssModel.rss_feed_description))
            holder.binding.name.setText(rssModel.rss_feed_description);
        else
            holder.binding.name.setText(mStringUtils.getLocalizedString(R.string.na));

        holder.binding.itemNumber.setText(String.valueOf(i + 1));
        holder.binding.itemContainer.setOnClickListener(holder.onClickProvider);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    private boolean isNotEmpty(String s) {
        return !TextUtils.isEmpty(s);
    }

    public static class RssProviderViewHolder extends RecyclerView.ViewHolder {

        ItemRssArrangeByProviderBinding binding;

        WeakReference<OnClickRssProviderListener> clickListener;

        private RssProviderViewHolder(@NonNull View itemView, OnClickRssProviderListener listener) {
            super(itemView);
            this.clickListener = new WeakReference<>(listener);
            this.binding = DataBindingUtil.bind(itemView);
        }

        private View.OnClickListener onClickProvider = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.get().OnClickRssProvider(getAdapterPosition());
            }
        };
    }

    public interface OnClickRssProviderListener {
        void OnClickRssProvider(int position);
    }
}
