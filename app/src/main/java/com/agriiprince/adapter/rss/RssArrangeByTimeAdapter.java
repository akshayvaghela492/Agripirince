package com.agriiprince.adapter.rss;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.databinding.ItemRssArrangeByTimeBinding;
import com.agriiprince.model.RssFeedModel;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RssArrangeByTimeAdapter extends RecyclerView.Adapter<RssArrangeByTimeAdapter.RssTimeViewHolder> {

    private List<RssFeedModel> mList;
    private List<RssFeedModel> originalList;
    private OnClickRssTimeListener mListener;
    private Calendar calendar;

    SimpleDateFormat simpleDateFormat;

    public RssArrangeByTimeAdapter(List<RssFeedModel> list, OnClickRssTimeListener listener, Calendar calendar) {
        this.originalList = list;
        this.mListener = listener;
        this.calendar = calendar;

        mList = new ArrayList<>();
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
    }

    public void add(RssFeedModel model) {
        originalList.add(model);
        if (validateDate(model.date)) {
            int index = getItemCount();
            mList.add(model);
            notifyItemInserted(index);
        }
    }

    public void addAll(List<RssFeedModel> list) {
        originalList.addAll(list);
        int index = getItemCount();
        for (RssFeedModel model : list) {
            if (validateDate(model.date)) {
                mList.add(model);
            }
        }
        notifyItemRangeInserted(index, getItemCount());
    }

    public RssFeedModel get(int position) {
        return mList.get(position);
    }

    public void setDate(Calendar calendar) {
        this.calendar = calendar;
        mList.clear();
        for (RssFeedModel model : originalList) {
            if (validateDate(model.date))
                mList.add(model);
        }
        notifyDataSetChanged();
    }

    private boolean validateDate(Date date) {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date);

        String date1 = simpleDateFormat.format(date);
        String date2 = simpleDateFormat.format(calendar.getTime());

        return date1.equalsIgnoreCase(date2);
    }

    @NonNull
    @Override
    public RssTimeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_rss_arrange_by_time, viewGroup, false);
        return new RssTimeViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RssTimeViewHolder holder, int i) {
        RssFeedModel model = get(i);

        holder.binding.providerId.setText(String.valueOf(i + 1));
        holder.binding.provider.setText(model.source);

        Log.d("Rss Time", "Model    " + i + "   " + model.title);
        String[] topics = model.getTopics().split(",");

        holder.binding.topicsContainer.removeAllViews();
        for (String topic : topics) {
            if (isEmpty(topic))
                continue;

            View view = LayoutInflater.from(holder.binding.topicsContainer.getContext())
                    .inflate(R.layout.item_text_border_grey, holder.binding.topicsContainer, false);
            TextView textView = view.findViewById(R.id.text_view);
            textView.setText(topic);
            holder.binding.topicsContainer.addView(view);
        }

        holder.binding.firstLine.setText(model.title);
        holder.binding.itemContainer.setOnClickListener(holder.showRssListener);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    private boolean isEmpty(String s) {
        return TextUtils.isEmpty(s);
    }

    public static class RssTimeViewHolder extends RecyclerView.ViewHolder {

        WeakReference<OnClickRssTimeListener> listener;
        ItemRssArrangeByTimeBinding binding;

        private RssTimeViewHolder(@NonNull View itemView, OnClickRssTimeListener listener) {
            super(itemView);
            this.binding = DataBindingUtil.bind(itemView);
            this.listener = new WeakReference<>(listener);

        }

        private View.OnClickListener showRssListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.get().onClickRssTimeItem(getAdapterPosition());
            }
        };
    }

    public interface OnClickRssTimeListener {
        void onClickRssTimeItem(int position);
    }
}
