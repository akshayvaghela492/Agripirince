package com.agriiprince.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.agriiprince.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class SortedStringSimpleAdapter extends RecyclerView.Adapter<SortedStringSimpleAdapter.ViewHolder>
        implements Filterable {

    private List<String> mOriginalList;

    private List<String> mFilterList;

    private ItemClickListener listener;

    public SortedStringSimpleAdapter(ItemClickListener clickListener) {
        this.listener = clickListener;

        this.mOriginalList = new ArrayList<>();
        this.mFilterList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_simple_text_view, parent, false);
        return new ViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String cropName = mFilterList.get(position);
        holder.cropName.setText(cropName);
    }

    @Override
    public int getItemCount() {
        return mFilterList.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public String get(int position) {
        return mFilterList.get(position);
    }

    public void add(String value) {
        int position = getItemCount();
        mOriginalList.add(value);
        mFilterList.add(value);
        notifyItemChanged(position);
    }

    public void remove(String value) {
        int index = mFilterList.indexOf(value);
        mOriginalList.remove(value);
        mFilterList.remove(value);

        if (index != -1) notifyItemChanged(index);
    }

    public void addAll(List<String> list) {
        int index = mFilterList.size();

        mOriginalList.addAll(list);
        mFilterList.clear();
        mFilterList.addAll(mOriginalList);

        notifyItemRangeChanged(index, list.size());
    }

    public void replaceAll(List<String> list) {
        mOriginalList.clear();
        mOriginalList.addAll(list);

        mFilterList.clear();
        mFilterList.addAll(list);

        notifyDataSetChanged();
    }


    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString().toLowerCase();
                ArrayList<String> filteredPlayers = new ArrayList<>();

                for (int i = 0; i < mOriginalList.size(); i++) {
                    if (mOriginalList.get(i).toLowerCase().contains(constraint)) {
                        filteredPlayers.add(mOriginalList.get(i));
                    }
                }
                results.count = filteredPlayers.size();
                results.values = filteredPlayers;

            } else {
                results.count = mOriginalList.size();
                results.values = mOriginalList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mFilterList = (ArrayList<String>) results.values;
            notifyDataSetChanged();
        }
    };


    public static class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private TextView cropName;

        private WeakReference<ItemClickListener> mListener;

        private ViewHolder(View itemView) {
            super(itemView);

            cropName = itemView.findViewById(R.id.item_simple_text);
        }

        private ViewHolder(View itemView, ItemClickListener listener) {
            super(itemView);

            this.mListener = new WeakReference<>(listener);

            cropName = itemView.findViewById(R.id.item_simple_text);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.get().onClickSimpleItem(getAdapterPosition());
        }
    }

    public interface ItemClickListener {

        void onClickSimpleItem(int position);
    }
}