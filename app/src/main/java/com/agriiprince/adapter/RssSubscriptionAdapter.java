package com.agriiprince.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.model.RssModel;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class RssSubscriptionAdapter extends RecyclerView.Adapter<RssSubscriptionAdapter.RssCategoryViewHolder> {

    private List<String> mCategoryList;

    private List<RssModel> mRssModels;

    private SparseBooleanArray mSelectedRss;

    private OnClickRssSubscriptionListener mListener;

    // remember the sub category open positions, or recycler flickers with updates
    private static SparseBooleanArray mSelectedRssPosition = new SparseBooleanArray();

    public RssSubscriptionAdapter(Context context) {
        this.mCategoryList = new ArrayList<>();
        this.mRssModels = new ArrayList<>();
        mSelectedRssPosition.clear();
    }

    public void addOnClickListener(OnClickRssSubscriptionListener listener) {
        this.mListener = listener;

    }

    public void removeOnClickListener() {
        this.mListener = null;

    }

    @NonNull
    @Override
    public RssCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_rss_category, viewGroup, false);

        return new RssCategoryViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RssCategoryViewHolder holder, int position) {
        String category = mCategoryList.get(position);

        holder.categoryName.setText(category);

        holder.subsLayout.removeAllViews();

        for (RssModel model : mRssModels) {
            if (model.categories.contains(category)) {

                View view = LayoutInflater.from(holder.subsLayout.getContext())
                        .inflate(R.layout.item_rss_sub, holder.subsLayout, false);

                TextView title = view.findViewById(R.id.item_rss_sub_title);
                AppCompatCheckBox checkBox = view.findViewById(R.id.item_rss_sub_check);

                title.setText(model.rss_feed_description);

                // if already subscribed then set different background
                if (mSelectedRss.get(model.id)) {
                    checkBox.setChecked(true);
                    view.setBackgroundResource(R.drawable.bg_rss_selected);

                } else {
                    checkBox.setChecked(false);
                    view.setBackgroundResource(R.color.transparent);
                }

                view.setTag(model.id);
                checkBox.setTag(model.id);

                view.setOnClickListener(holder);
                checkBox.setOnCheckedChangeListener(holder);
                holder.subsLayout.addView(view);

            }
        }

        if (mSelectedRssPosition.get(position)) {
            holder.subsDropDown.setImageResource(R.drawable.ic_keyboard_arrow_up);
            holder.subsLayout.setVisibility(View.VISIBLE);
        } else {
            holder.subsDropDown.setImageResource(R.drawable.ic_keyboard_arrow_down);
            holder.subsLayout.setVisibility(View.GONE);
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }

    public void add(String category) {
        mCategoryList.add(category);
    }

    public void addAllCategory(List<String> categories) {
        mCategoryList = categories;
    }

    public void addRssList(List<RssModel> rssList) {
        mRssModels = rssList;
    }

    public void addSelectedRss(SparseBooleanArray booleanArray) {
        mSelectedRss = booleanArray;
    }


    public static class RssCategoryViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, AppCompatCheckBox.OnCheckedChangeListener {

        private TextView categoryName;

        private ImageView subsDropDown;

        private LinearLayout subsLayout;

        private WeakReference<OnClickRssSubscriptionListener> mListener;

        private RssCategoryViewHolder(@NonNull View itemView, final OnClickRssSubscriptionListener onClickListener) {
            super(itemView);
            this.mListener = new WeakReference<>(onClickListener);

            this.categoryName = itemView.findViewById(R.id.item_rss_category_name);
            this.subsDropDown = itemView.findViewById(R.id.item_rss_subs_drop_down);
            this.subsLayout = itemView.findViewById(R.id.item_rss_sub_category_layout);

            itemView.findViewById(R.id.item_rss_layout).setOnClickListener(this);

            subsDropDown.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.item_rss_subs_drop_down || v.getId() == R.id.item_rss_layout) {
                if (subsLayout.getVisibility() == View.VISIBLE) {
                    subsDropDown.setImageResource(R.drawable.ic_keyboard_arrow_down);
                    subsLayout.setVisibility(View.GONE);

                    mSelectedRssPosition.put(getAdapterPosition(), false);

                } else {
                    subsDropDown.setImageResource(R.drawable.ic_keyboard_arrow_up);
                    subsLayout.setVisibility(View.VISIBLE);

                    mSelectedRssPosition.put(getAdapterPosition(), true);
                }

            } else {
                mListener.get().onSubscribe((int) v.getTag(), getAdapterPosition());

            }
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            mListener.get().onSubscribe((int) buttonView.getTag(), getAdapterPosition());

        }
    }

    public interface OnClickRssSubscriptionListener {

        void onSubscribe(int rssId, int position);
    }
}
