package com.agriiprince.model;

import com.google.gson.annotations.SerializedName;

public class PlaceModel {

    @SerializedName("id")
    public String id;
    @SerializedName("state")
    public String state;
    @SerializedName("name")
    public String city;
}
