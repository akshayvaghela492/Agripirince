package com.agriiprince.model;

import com.agriiprince.utils.UnitUtils;

public class GovReportedPrice {

    private String id;

    private String district;

    private String market;

    private String variety;

    private String grade;

    private double mandi_lat;

    private double mandi_lng;

    private int minP;

    private int maxP;

    private int modP;

    private double tonnage;

    private boolean isInterested;

    private double distance;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMandiDistrict() {
        return district;
    }

    public String getMandiName() {
        return market;
    }

    public String getVariety() {
        return variety;
    }

    public String getGrade() {
        return grade;
    }

    public double getMandiLat() {
        return mandi_lat;
    }

    public double getMandiLng() {
        return mandi_lng;
    }

    public void setMandiLat(double mandi_lat) {
        this.mandi_lat = mandi_lat;
    }

    public void setMandiLng(double mandi_lng) {
        this.mandi_lng = mandi_lng;
    }

    public double getMinPrice() {
        return minP;
    }

    public double getMaxPrice() {
        return maxP;
    }

    public double getModalPrice() {
        return modP;
    }

    public double getTonnage() {
        return tonnage;
    }

    public boolean isInterested() {
        return isInterested;
    }

    public void setInterested(boolean interested) {
        isInterested = interested;
    }

    public double getDistance() {
        return distance;
    }

    public double getDistanceInKm() {
        return distance / 1000;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
