package com.agriiprince.model;

import com.google.gson.annotations.SerializedName;

public class CAListModel {

    @SerializedName("id")
    public String id;
    @SerializedName("ca_name")
    public String ca_name;
    @SerializedName("ca_name_bi")
    public String ca_name_bi;
    @SerializedName("ca_business")
    public String ca_business_name;
    @SerializedName("ca_business_bi")
    public String ca_business_name_bi;
    @SerializedName("ca_mandi")
    public String ca_mandi;
    @SerializedName("ca_mandi_bi")
    public String ca_mandi_bi;
    @SerializedName("ca_contact")
    public String ca_contact;
    @SerializedName("ca_address")
    public String ca_address;
    @SerializedName("ca_address_bi")
    public String ca_address_bi;
    @SerializedName("commodity")
    public String ca_commodity;
    @SerializedName("commodity_bi")
    public String ca_commodity_bi;

}
