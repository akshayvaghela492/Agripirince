package com.agriiprince.model;

public class WarehouseResponse {

    public String warehouse_name;
    public String warehouse_latitude;
    public String warehouse_longitude;
    public String warehouse_state;
    public String distance;
}
