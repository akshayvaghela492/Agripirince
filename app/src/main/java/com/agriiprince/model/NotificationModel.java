package com.agriiprince.model;

public class NotificationModel {

    private String message;
    private String sender;
    private String created_on;

    public NotificationModel() {
    }

    public String getMessage() {
        return message;
    }

    public String getSender() {
        return sender;
    }

    public String getCreated_on() {
        return created_on;
    }

}
