package com.agriiprince.model;

public class SelectLanguageModel {
    public String languageTitle;
    public String languageLetter;
    public String languageCode;
    public transient boolean isSelected;

}
