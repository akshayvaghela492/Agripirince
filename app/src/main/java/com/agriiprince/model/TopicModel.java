package com.agriiprince.model;

public class TopicModel {
    public String topic;
    public String link;
    public boolean interested;

    public TopicModel(String topic, String link) {
        this.topic = topic;
        this.link = link;
    }

    public TopicModel(String topic) {
        this.topic = topic;
        this.interested = false;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public boolean isInterested() {
        return interested;
    }

    public void setInterested(boolean interested) {
        this.interested = interested;
    }
}
