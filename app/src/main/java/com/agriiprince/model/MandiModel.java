package com.agriiprince.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.agriiprince.appcontroller.AppController;
import com.agriiprince.utils.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MandiModel implements Parcelable {

    public String mandi_id;
    public String mandi_lat;
    public String mandi_lng;
    public String postal_code;

    public String mandi_name_en;
    public String mandi_city_en;
    public String mandi_district_en;
    public String mandi_state_en;
    public String mandi_city_combi_en;
    public String city_mandi_combi_en;

    public String mandi_name_bi;
    public String mandi_city_bi;
    public String mandi_district_bi;
    public String mandi_state_bi;
    public String mandi_city_combi_bi;
    public String city_mandi_combi_bi;
    //public String status;

    public MandiModel() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mandi_id);
        dest.writeString(mandi_lat);
        dest.writeString(mandi_lng);
        dest.writeString(postal_code);

        dest.writeString(mandi_name_en);
        dest.writeString(mandi_city_en);
        dest.writeString(mandi_district_en);
        dest.writeString(mandi_state_en);
        dest.writeString(mandi_city_combi_en);
        dest.writeString(city_mandi_combi_en);

        dest.writeString(mandi_name_bi);
        dest.writeString(mandi_city_bi);
        dest.writeString(mandi_district_bi);
        dest.writeString(mandi_state_bi);
        dest.writeString(mandi_city_combi_bi);
        dest.writeString(city_mandi_combi_bi);
    }

    private MandiModel(Parcel parcel) {
        this.mandi_id = parcel.readString();
        this.mandi_lat = parcel.readString();
        this.mandi_lng = parcel.readString();
        this.postal_code = parcel.readString();

        this.mandi_name_en = parcel.readString();
        this.mandi_city_en = parcel.readString();
        this.mandi_district_en = parcel.readString();
        this.mandi_state_en = parcel.readString();
        this.mandi_city_combi_en = parcel.readString();
        this.city_mandi_combi_en = parcel.readString();

        this.mandi_name_bi = parcel.readString();
        this.mandi_city_bi = parcel.readString();
        this.mandi_district_bi = parcel.readString();
        this.mandi_state_bi = parcel.readString();
        this.mandi_city_combi_bi = parcel.readString();
        this.city_mandi_combi_bi = parcel.readString();
    }

    public static final Parcelable.Creator<MandiModel> CREATOR = new Creator<MandiModel>() {
        @Override
        public MandiModel createFromParcel(Parcel source) {
            return new MandiModel(source);
        }

        @Override
        public MandiModel[] newArray(int size) {
            return new MandiModel[size];
        }
    };

    public static List<String> getMandiNames(List<MandiModel> list) {
        List<String> names = new ArrayList<>();

        for (MandiModel model : list) {
            names.add(model.mandi_name_en);
        }

        return names;
    }

    public static List<String> getMandiCityNames(Context context, List<MandiModel> list) {
        StringUtils utils = AppController.getInstance().getStringUtils();
        List<String> names = new ArrayList<>();

        for (MandiModel model : list) {
            names.add(utils.getBilingualStringForApi(model.mandi_city_combi_en, model.mandi_city_combi_bi));
        }

        return names;
    }

    public static List<String> getMandiCityNamesAndIds(Context context, List<MandiModel> list) {
        StringUtils utils = AppController.getInstance().getStringUtils();
        List<String> names = new ArrayList<>();

        for (MandiModel model : list) {
            String tempNamesAndId = utils.getBilingualStringForApi(model.mandi_city_combi_en, model.mandi_city_combi_bi);
            tempNamesAndId = tempNamesAndId + "|" + model.mandi_id;
            names.add(tempNamesAndId);
        }

        return names;
    }

    public static String getMandiNameById(Context context, List<MandiModel> list, String mandiId) {
        StringUtils utils = AppController.getInstance().getStringUtils();
        for (MandiModel model : list) {
            if ((model.mandi_id).equals(mandiId)) {
                return utils.getBilingualStringForApi(model.mandi_city_combi_en, model.mandi_city_combi_bi);
                //return model.mandi_name_en;
            }
        }
        return "Not Selected";
    }

    public static String getMandiNameOnlyById(Context context, List<MandiModel> list, String mandiId) {
        StringUtils utils = AppController.getInstance().getStringUtils();
        for (MandiModel model : list) {
            if ((model.mandi_id).equals(mandiId)) {
                return model.mandi_name_en;
                //return model.mandi_name_en;
            }
        }
        return "Not Selected";
    }


    public static String getMandiEnglishNameOnlyById(List<MandiModel> list, String mandiId) {
        for (MandiModel model : list) {
            if ((model.mandi_id).equals(mandiId)) {
                return model.mandi_name_en;
                //return model.mandi_name_en;
            }
        }
        return "Not Selected";
    }


    public static String getMandiNameByMandiCity(Context context, List<MandiModel> list, String mandiCity) {
        StringUtils utils = AppController.getInstance().getStringUtils();
        for (MandiModel model : list) {
            if (utils.getBilingualStringForApi(model.mandi_city_combi_en, model.mandi_city_combi_bi)
                    .equals(mandiCity)) {

                return model.mandi_name_en;
            }
        }

        return mandiCity;
    }

    public static MandiModel getByMandiCity(Context context, List<MandiModel> list, String mandiCity) {
        StringUtils utils = AppController.getInstance().getStringUtils();
        for (MandiModel model : list) {
            if (utils.getBilingualStringForApi(model.mandi_city_combi_en, model.mandi_city_combi_bi).equals(mandiCity)) {
                return model;
            }

        }
        return null;
    }

    public static MandiModel getByMandiName(List<MandiModel> list, String mandiName) {
        for (MandiModel model : list) {
            if (model.mandi_name_en.equals(mandiName)) return model;

        }
        return null;
    }

    public static MandiModel getByBilingualMandiName(Context context, List<MandiModel> list, String mandiName) {
        StringUtils utils = AppController.getInstance().getStringUtils();
        for (MandiModel model : list) {
            if (utils.getBilingualStringForApi(model.mandi_name_en, model.mandi_name_bi).equals(mandiName))
                return model;

        }
        return null;
    }


    public static List<String> getMandiNamesByIds(List<MandiModel> list, String[] ids) {
        return getMandiNamesByIds(list, Arrays.asList(ids));
    }

    public static List<String> getMandiNamesByIds(List<MandiModel> list, List<String> ids) {
        List<String> mandiNames = new ArrayList<>();
        for (MandiModel model : list) {
            if (ids.contains(model.mandi_id))
                mandiNames.add(model.mandi_name_en);
        }

        return mandiNames;
    }

    public static List<MandiModel> getMandisByIds(List<MandiModel> list, List<String> ids) {
        List<MandiModel> mandis = new ArrayList<>();
        for (MandiModel model : list) {
            if (ids.contains(model.mandi_id))
                mandis.add(model);
        }

        return mandis;
    }
}
