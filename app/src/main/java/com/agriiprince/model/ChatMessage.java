package com.agriiprince.model;

public class ChatMessage {

    //private int usersId;
    //private String message;
    //private String sentAt;
    //private String name;

    private String message_id, sender_id, receiver_id, message, time, sender_name;
    private String read_status, read_time;
    private String chat_type;

    public ChatMessage(String sender_id, String receiver_id, String message, String time) {
        this.sender_id = sender_id;
        this.receiver_id = receiver_id;
        this.message = message;
        this.time = time;
    }

    public ChatMessage(String message_id, String sender_id, String receiver_id, String message, String time) {
        this.message_id = message_id;
        this.sender_id = sender_id;
        this.receiver_id = receiver_id;
        this.message = message;
        this.time = time;
    }

    public ChatMessage(String message_id, String sender_id, String receiver_id, String message, String time, String sender_name) {
        this.message_id = message_id;
        this.sender_id = sender_id;
        this.receiver_id = receiver_id;
        this.message = message;
        this.time = time;
        this.sender_name = sender_name;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public String getChat_type() {
        return chat_type;
    }

    public void setChat_type(String chat_type) {
        this.chat_type = chat_type;
    }

    public String getRead_status() {
        return read_status;
    }

    public void setRead_status(String read_status) {
        this.read_status = read_status;
    }

    public String getRead_time() {
        return read_time;
    }

    public void setRead_time(String read_time) {
        this.read_time = read_time;
    }

    public String getSenderId() {
        return sender_id;
    }

    public String getReceiverId() {
        return receiver_id;
    }

    public String getMessage() {
        return message;
    }

    public String getTime() {
        return time;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(String receiver_id) {
        this.receiver_id = receiver_id;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

  /*public ChatMessage(int usersId, String message, String sentAt, String name) {
    this.usersId = usersId;
    this.message = message;
    this.sentAt = sentAt;
    this.name = name;
  }*/

  /*public int getUsersId() {
    return usersId;
  }

  public String getMessage() {
    return message;
  }

  public String getSentAt() {
    return sentAt;
  }

  public String getName() {
    return name;
  }*/
}