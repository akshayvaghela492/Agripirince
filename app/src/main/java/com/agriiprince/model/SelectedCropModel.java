package com.agriiprince.model;

import java.util.List;

/**
 * Created by dtrah on 11/20/2018.
 */

public class SelectedCropModel {

    private boolean isSelected;
    private String crop;
    private String cropName;
    private String cropId;

    public String getCropName() {
        return cropName;
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    private List<SelectedCropModel> varieties;

    private List<String> variety;

    public List<String> getVariety() {
        return variety;
    }

    public void setVariety(List<String> variety) {
        this.variety = variety;
    }

    public String getCropId() {
        return cropId;
    }

    public void setCropId(String cropId) {
        this.cropId = cropId;
    }

    public List<SelectedCropModel> getVarieties() {
        return varieties;
    }

    public void setVarieties(List<SelectedCropModel> varieties) {
        this.varieties = varieties;
    }

    public String getCrop() {
        return crop;
    }

    public void setCrop(String crop) {
        this.crop = crop;
    }

    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
