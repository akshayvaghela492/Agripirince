package com.agriiprince.model;

import java.text.DecimalFormat;

public class OeReportedPrice {

    private String id;

    private String mandi_district;

    private String mandi_name;

    private String commodity_variety;

    private String commodity_grade;

    private double mandi_lat;

    private double mandi_lng;

    private int min_price;

    private int max_price;

    private int modal_price;

    private boolean isInterested;

    private double distance;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isInterested() {
        return isInterested;
    }

    public void setInterested(boolean interested) {
        isInterested = interested;
    }

    public String getMandiDistrict() {
        return mandi_district;
    }

    public String getMandiName() {
        return mandi_name;
    }

    public String getCommodityVariety() {
        return commodity_variety;
    }

    public String getCommodityGrade() {
        return commodity_grade;
    }

    public double getMandiLat() {
        return mandi_lat;
    }

    public double getMandiLng() {
        return mandi_lng;
    }

    public void setMandiLat(double mandi_lat) {
        this.mandi_lat = mandi_lat;
    }

    public void setMandiLng(double mandi_lng) {
        this.mandi_lng = mandi_lng;
    }

    public double getMinPrice() {
        return min_price;
    }

    public double getMaxPrice() {
        return max_price;
    }

    public double getModalPrice() {
        return modal_price;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getDistanceInKm() {
        return distance / 1000;
    }

    public double getMedian() {
        double sum = getMinPrice() + getMaxPrice() + getModalPrice();
        return sum / 3;
    }

    private static double convertQintalToKg(int value) {
        return convertQintalToKg((double) value);
    }

    private static double convertQintalToKg(double value) {
        try {
            //return value / 100;
            return value;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0d;
        }
    }

    public static double formatDecimal(double value) {
        DecimalFormat decimalFormat = new DecimalFormat("#.0");
        return Double.parseDouble(decimalFormat.format(value));
    }

}
