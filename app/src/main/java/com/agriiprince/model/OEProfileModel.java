package com.agriiprince.model;


//user_id, password, oe_name, oe_contact, oe_address,
//        oe_location, oe_pincode, preferred_language, bank_name,
//        bank_account_number, secondary_language, allow_gps, geo_tag_location, screen_saver_enabled

public class OEProfileModel {
    public String id;
    public String oe_id;
    public String fcm_token;
    public String oe_name;
    public String oe_business_name;
    public String oe_contact;
    public String oe_dob;
    public String oe_license;
    public String oe_pan;
    public String oe_otp;
    public String oe_password;
    public String oe_address;
    public String oe_location;//City
    public String pincode;
    public String mandi_address;
    public String my_mandi_list;
    public String my_crop_list;
    public String interested_crop_list;
    public String interested_mandi_list;
    public String status;
    public String preferred_language;
    public String secondary_language;

//    public String bank_name;
//    public String bank_account_number;
//    public String has_net_banking;
//    public String has_paytm;
//    public String has_bhim;
//
//    public String screen_saver_enabled;

    public String terms_and_conditions;
    public String created_on;
    public String updated_on;
    public String ap_certified;
    public String login_status;
    public String asked_for_invite;


}
