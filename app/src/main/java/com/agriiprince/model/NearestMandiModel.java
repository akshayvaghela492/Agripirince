package com.agriiprince.model;


public class NearestMandiModel implements Comparable<NearestMandiModel> {
    public String mandi_id;
    public String mandi_lat;
    public String mandi_lng;
    public String mandi_name_en;
    public double mandi_distance_from_user;

    public NearestMandiModel(String mandi_lat, String mandi_lng, String mandi_name_en, double mandi_distance_from_user) {
        this.mandi_lat = mandi_lat;
        this.mandi_lng = mandi_lng;
        this.mandi_name_en = mandi_name_en;
        this.mandi_distance_from_user = mandi_distance_from_user;
    }

    public NearestMandiModel(String mandi_id, String mandi_lat, String mandi_lng, String mandi_name_en, double mandi_distance_from_user) {
        this.mandi_id = mandi_id;
        this.mandi_lat = mandi_lat;
        this.mandi_lng = mandi_lng;
        this.mandi_name_en = mandi_name_en;
        this.mandi_distance_from_user = mandi_distance_from_user;
    }

    public String getMandi_id() {
        return mandi_id;
    }

    public void setMandi_id(String mandi_id) {
        this.mandi_id = mandi_id;
    }

    public String getMandi_lat() {
        return mandi_lat;
    }

    public void setMandi_lat(String mandi_lat) {
        this.mandi_lat = mandi_lat;
    }

    public String getMandi_lng() {
        return mandi_lng;
    }

    public void setMandi_lng(String mandi_lng) {
        this.mandi_lng = mandi_lng;
    }

    public String getMandi_name_en() {
        return mandi_name_en;
    }

    public void setMandi_name_en(String mandi_name_en) {
        this.mandi_name_en = mandi_name_en;
    }

    public double getMandi_distance_from_user() {
        return mandi_distance_from_user;
    }

    public void setMandi_distance_from_user(double mandi_distance_from_user) {
        this.mandi_distance_from_user = mandi_distance_from_user;
    }

    @Override
    public int compareTo(NearestMandiModel o) {
        if (this.getMandi_distance_from_user() < o.getMandi_distance_from_user())
            return -1;
        else if (this.getMandi_distance_from_user() > o.getMandi_distance_from_user())
            return 1;
        return 0;
    }
}
