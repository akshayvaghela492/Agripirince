package com.agriiprince.model;

import java.io.Serializable;

public class FarmersListModel implements Serializable {

    String name, locations, subscribed_pesticide_vendor, invited_by, farmer_id, farmer_contact;

    public FarmersListModel() {
    }

    public FarmersListModel(String name, String locations, String subscribed_pesticide_vendor, String invited_by, String farmer_id, String farmer_contact) {
        this.name = name;
        this.locations = locations;
        this.subscribed_pesticide_vendor = subscribed_pesticide_vendor;
        this.invited_by = invited_by;
        this.farmer_id = farmer_id;
        this.farmer_contact = farmer_contact;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }

    public String getSubscribed_pesticide_vendor() {
        return subscribed_pesticide_vendor;
    }

    public void setSubscribed_pesticide_vendor(String subscribed_pesticide_vendor) {
        this.subscribed_pesticide_vendor = subscribed_pesticide_vendor;
    }

    public String getInvited_by() {
        return invited_by;
    }

    public void setInvited_by(String invited_by) {
        this.invited_by = invited_by;
    }

    public String getFarmer_id() {
        return farmer_id;
    }

    public void setFarmer_id(String farmer_id) {
        this.farmer_id = farmer_id;
    }

    public String getFarmer_contact() {
        return farmer_contact;
    }

    public void setFarmer_contact(String farmer_contact) {
        this.farmer_contact = farmer_contact;
    }
}
