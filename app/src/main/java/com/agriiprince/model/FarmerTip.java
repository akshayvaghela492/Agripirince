package com.agriiprince.model;

public class FarmerTip {

    private String title;
    private String body;

    private int imageRes;
    private int[] gradientColor;

    public FarmerTip(String title, String body, int imageRes, int[] gradientColor) {
        this.title = title;
        this.body = body;
        this.imageRes = imageRes;
        this.gradientColor = gradientColor;
    }


    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public int getImageRes() {
        return imageRes;
    }

    public int[] getGradientColor() {
        return gradientColor;
    }
}
