package com.agriiprince.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;
import java.util.Date;

public class RssFeedModel implements Parcelable {

    public String title;
    public String link;
    public String description;
    public String img;
    public String source;
    public String topics;
    public Date date;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(link);
        dest.writeString(description);
        dest.writeString(img);
        dest.writeString(source);
        dest.writeString(topics);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        dest.writeLong(calendar.getTimeInMillis());
    }

    private RssFeedModel(Parcel parcel) {
        this.title = parcel.readString();
        this.link = parcel.readString();
        this.description = parcel.readString();
        this.img = parcel.readString();
        this.source = parcel.readString();
        this.topics = parcel.readString();

        Long time = parcel.readLong();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        this.date = calendar.getTime();
    }

    public RssFeedModel(String title, String link, String description, Date date) {
        this.title = title;
        this.link = link;
        this.date = date;
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTopics() {
        return topics;
    }

    public void setTopics(String topics) {
        this.topics = topics;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public static final Parcelable.Creator<RssFeedModel> CREATOR = new Creator<RssFeedModel>() {
        @Override
        public RssFeedModel createFromParcel(Parcel source) {
            return new RssFeedModel(source);
        }

        @Override
        public RssFeedModel[] newArray(int size) {
            return new RssFeedModel[size];
        }
    };
}