package com.agriiprince.model;

public class CropDetailModel {
    public String crop_id;
    public String crop_name;
    public String crop_variety;
    public String crop_grade;
    public String crop_class;
}
