package com.agriiprince.model;

/*Sample response*/
/*
  [{"error_code":100,"message":"Success.",
          "user_details":{"id":"45","farmer_id":"KKJS4543","fcm_token":"dXrteprjsfY:APA91bGyVXz5Y-it9y8VB5Szg2KK1PKYHqfxYsbCTb9-YAJWY9lRJAaiAUiKK4U2gJ9vcrJvGmWMTHGmMG9uJpDvvL6dghIgOnxpOEQt1k1a7USeWy9hmMXct60o6ndbQqRuiKwqW_KO",
          "farmer_name":"subbu","farmer_contact":"8296881424","farmer_otp":"","farmer_password":"testingfarmer","farmer_address":"",
          "farmer_location":"Bengaluru","farmer_pincode":"560075","status":"A","preferred_language":"English",
          "bank_name":"","bank_account_number":"","created_on":"2018-09-23 14:26:28","updated_on":"2018-09-23 14:36:31",
          "subscription_services":"","subscribed_pesticide_vendor":"","login_status":"logout","invited_by":"","asked_for_invite":"N",
          "my_crop_list":"","interested_crop_list":"","secondary_language":"","nearest_mandi_name":"","nearest_mandi_lat":"0","nearest_mandi_log":"0",
          "allow_gps":"","geo_tag_location":"","has_net_banking":"","has_paytm":"","has_bhim":"","screen_saver_enabled":"","interested_mandi_list":"",
          "average_annual_income":"0","land_in_acres":"0","price_measuring_unit":"0","interested_rss_topics":"","bpl_image":"","bpl_status":""},
          "data_of_my_crop_list":[null],"data_of_interested_crop_list":[]}]
*/

public class FarmerProfileModel {

    public static final String[] areaUnits = {
            "Acre",
            "Hectare",
            "Bigha"
    };

    public static final String[] areas = new String[] {
            "< 1",
            "1-2",
            "2-3",
            "3-5",
            "5-10",
            "> 10"
    };

    public static final String[] priceUnits = {
            "Kg",
            "Quintal",
            "Tonne"
    };

    public static final String[] incomeRange = {
            "< 1.5",
            "1.5 - 2.5",
            "2.5 - 4",
            "4 - 6",
            "6 - 10",
            "> 10"
    };

    public static String getOneDigitIncome(int position) {
        switch (position) {
            case 0:
                return "1";
            case 1:
                return "2";
            case 2:
                return "4";
            case 3:
                return "6";
            case 4:
                return "10";
            case 5:
                return "11";
            default:
                return "2";
        }
    }

    public static String getIncomeByOneDigit(String income) {
        switch (income) {
            case "1":
                return incomeRange[0];
            case "2":
                return incomeRange[1];
            case "4":
                return incomeRange[2];
            case "6":
                return incomeRange[3];
            case "10":
                return incomeRange[4];
            case "11":
                return incomeRange[5];
            default:
                return incomeRange[0];
        }
    }

    public String created_on;
    public String updated_on;

    public String farmer_id;
    public String farmer_name;
    public String farmer_contact;
    public String farmer_otp;
    public String farmer_password;
    public String farmer_address;
    public String farmer_location;//city
    public String farmer_pincode;

    public String preferred_language;
    public String secondary_language;

    public String bank_name;
    public String bank_account_number;
    public String has_net_banking;
    public String has_paytm;
    public String has_bhim;

    public String screen_saver_enabled;

    public String average_annual_income;
    public String land_in_acres;
    public String price_measuring_unit;
    public String area_measuring_unit;

    public String nearest_mandi_name;
    public String nearest_mandi_lat;
    public String nearest_mandi_log;

    public String interested_mandi_list;
    public String interested_crop_list;
    public String interested_rss_topics;

    public String bpl_image;
    public String bpl_status;
    public String login_status;
}
