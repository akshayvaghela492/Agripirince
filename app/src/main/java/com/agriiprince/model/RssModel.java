package com.agriiprince.model;

/*
"id":"1",
"rss_feed_link":"https:\/\/economictimes.INDIA_TIMES.com\/rssfeeds\/1202099874.cms",
"rss_feed_description":"India Times",
"rss_feed_ratings":"2.0",
"govt_private_ngo":"Private",
"language":"English",
"categories":"General News"
*/

import android.os.Parcel;
import android.os.Parcelable;

public class RssModel implements Parcelable {

    public int id;
    public String rss_feed_link;
    public String rss_feed_description;
    public String rss_feed_ratings;
    public String govt_private_ngo;
    public String language;
    public String categories;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(rss_feed_link);
        dest.writeString(rss_feed_description);
        dest.writeString(rss_feed_ratings);
        dest.writeString(govt_private_ngo);
        dest.writeString(language);
        dest.writeString(categories);
    }

    private RssModel(Parcel parcel) {
        this.id = parcel.readInt();
        this.rss_feed_link = parcel.readString();
        this.rss_feed_description = parcel.readString();
        this.rss_feed_ratings = parcel.readString();
        this.govt_private_ngo = parcel.readString();
        this.language = parcel.readString();
        this.categories = parcel.readString();
    }

    public static final Parcelable.Creator<RssModel> CREATOR = new Creator<RssModel>() {
        @Override
        public RssModel createFromParcel(Parcel source) {
            return new RssModel(source);
        }

        @Override
        public RssModel[] newArray(int size) {
            return new RssModel[size];
        }
    };
}
