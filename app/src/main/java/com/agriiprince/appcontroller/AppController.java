package com.agriiprince.appcontroller;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.app.Service;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;

import com.agriiprince.activities.ChatOneToOneActivity;
import com.agriiprince.activities.CommunicateToAP;
import com.agriiprince.db.DatabaseManager;
import com.agriiprince.mvvm.di.component.DaggerAppComponent;
import com.agriiprince.mvvm.retrofit.model.tutorial.TutorialSections;
import com.agriiprince.utils.StringUtils;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.evernote.android.job.JobManager;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasServiceInjector;
import io.fabric.sdk.android.Fabric;

public class AppController extends Application implements HasActivityInjector, HasServiceInjector, ActivityLifecycleCallbacks {

    @Inject
    DispatchingAndroidInjector<Activity> mActivityInjector;

    @Inject
    DispatchingAndroidInjector<Service> mServiceInjector;

    private static boolean isInterestingActivityVisible;
    private static boolean isChatOneToOneActivityVisible;
    private RequestQueue mRequestQueue;
    private static AppController mInstance;
    public static final String TAG = AppController.class.getSimpleName();

    public static int currentScreen = 1;
    public static int loggedOut = 0;

    private static StringUtils mStringUtils;

    private static String chatIdentifier;

    private DatabaseManager databaseManager;

    public static List<TutorialSections> gTutorialSectionsEn;
    public static List<TutorialSections> gTutorialSectionsHi;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mInstance = this;

        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this);

        // evernote job creator
        JobManager.create(this).addJobCreator(new AppJobCreator());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Kitkat and lower has a bug that can cause in correct strict mode
            // warnings about expected activity counts
            enableStrictMode();
        }

        // Register to be notified of activity state changes
        registerActivityLifecycleCallbacks(this);

    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return mActivityInjector;
    }

    @Override
    public AndroidInjector<Service> serviceInjector() {
        return mServiceInjector;
    }



    public DatabaseManager getDataBase() {
        if (databaseManager == null) databaseManager = new DatabaseManager(this);
        return databaseManager;
    }

    public void onPrimaryLocaleChange(String locale) {
        getStringUtils().setPrimaryLocale(locale);
    }

    public void onSecondaryLocaleChange(String locale) {
        getStringUtils().setSecondaryLocale(locale);
    }

    public StringUtils getStringUtils() {
        if (mStringUtils == null) mStringUtils = new StringUtils(getBaseContext());

        return mStringUtils;
    }

    public void setChatIdentifier(String identifier) {
        chatIdentifier = identifier;
    }

    public void removeChatIdentifier() {
        chatIdentifier = null;
    }

    public String getChatIdentifier() {
        return chatIdentifier;
    }

    public void enableStrictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                //.penaltyDeath()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                //.penaltyDeath()
                .build());
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public boolean isCommunicateToAPActivityVisible() {
        return isInterestingActivityVisible;
    }

    public boolean isChatOneToOneActivityVisible() {
        return isChatOneToOneActivityVisible;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        isInterestingActivityVisible = activity instanceof CommunicateToAP;
        isChatOneToOneActivityVisible = activity instanceof ChatOneToOneActivity;
    }

    @Override
    public void onActivityStarted(Activity activity) {
        isInterestingActivityVisible = activity instanceof CommunicateToAP;
        isChatOneToOneActivityVisible = activity instanceof ChatOneToOneActivity;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        isInterestingActivityVisible = activity instanceof CommunicateToAP;
        isChatOneToOneActivityVisible = activity instanceof ChatOneToOneActivity;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        isInterestingActivityVisible = !(activity instanceof CommunicateToAP);
        isChatOneToOneActivityVisible = !(activity instanceof ChatOneToOneActivity);
    }

    @Override
    public void onActivityStopped(Activity activity) {
        isInterestingActivityVisible = !(activity instanceof CommunicateToAP);
        isChatOneToOneActivityVisible = !(activity instanceof ChatOneToOneActivity);
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        //auto created methods
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        isInterestingActivityVisible = !(activity instanceof CommunicateToAP);
        isChatOneToOneActivityVisible = !(activity instanceof ChatOneToOneActivity);
    }
}
