package com.agriiprince.appcontroller;

import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.agriiprince.db.DatabaseManager;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.dataservice.DataResponse;
import com.agriiprince.dataservice.DiseaseListDataService;
import com.agriiprince.db.DiseaseDao;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.models.DiseaseListModel;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static com.facebook.accountkit.internal.AccountKitController.getApplicationContext;

public class DiseaseDiagnosisJob extends Job {

    static final String TAG = "DiseaseDiagnosisJob";


    public static void scheduleJob() {
        JobManager.instance().cancelAllForTag(DiseaseDiagnosisJob.TAG);
        new JobRequest.Builder(DiseaseDiagnosisJob.TAG)
                .setExecutionWindow(TimeUnit.HOURS.toMillis(1), TimeUnit.DAYS.toMillis(7))
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setRequirementsEnforced(true)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

    private Handler handler;

    @NonNull
    @Override
    protected Result onRunJob(@NonNull Params params) {
        PrefManager prefManager = new PrefManager(getApplicationContext());
        UserProfile userProfile = new UserProfile(getApplicationContext());
        DatabaseManager databaseManager = AppController.getInstance().getDataBase();
        DiseaseDao diseaseDao = new DiseaseDao(databaseManager);

        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();

        handler = new Handler(handlerThread.getLooper());

        List<String> cropNames = diseaseDao.getDownloadRequests();

        DiseaseListDataService diseaseListDataService = new DiseaseListDataService(prefManager, databaseManager);

        for (String cropName : cropNames) {
            Log.d(TAG, "fetchDiseaseInfo: " + cropName);
            diseaseListDataService.getCropDiseaseList(cropName, userProfile.getApiToken(), dataResponse);
            diseaseDao.updateDownloadStatus(cropName, "en");
        }

        return Result.SUCCESS;
    }



    private DataResponse<List<DiseaseListModel>> dataResponse = new DataResponse<List<DiseaseListModel>>() {
        @Override
        public void onSuccessResponse(final List<DiseaseListModel> response) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    for (DiseaseListModel model : response) {
                        if ( ! TextUtils.isEmpty(model.getCrop_images())) {
                            List<String> urls = DiseaseListModel.getImageUrls(model.getCrop_images());

                            for (String url : urls) {
                                try {
                                    Glide.with(getApplicationContext())
                                            .downloadOnly()
                                            .load(url)
                                            .submit(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                                            .get();
                                } catch (ExecutionException e) {
                                    e.printStackTrace();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            });
        }

        @Override
        public void onParseError() {

        }

        @Override
        public void onErrorResponse(VolleyError error) {

        }
    };

}
