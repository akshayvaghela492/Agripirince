package com.agriiprince.utils;

import com.agriiprince.activities.checkprice.PriceUnit;

import java.text.DecimalFormat;

/**
 * Created by Manu Sajjan on 20-12-2018.
 *
 * @version 1.2.2
 */
public class MathUtils {


    /**
     * returns the median index of an array of lenght 0 - size
     *
     * @param size int Length of an array
     * @return int Returns -1 if size is 0, if size >0 then median index
     * @since 1.2.2
     */
    public static int medianIndex(int size) {
        int median = median(size);
        if (median == -1) return median;
        else return median - 1;
    }

    /**
     * returns the median of number 1 to size.
     * If the size is even then return (size+1)/2 else returns size/2
     *
     * @param size int Total length of the array
     * @return int Returns -1 if size is 0 or else >0
     * @since 1.2.2
     */
    public static int median(int size) {
        if (size == 0) return -1;
        int median = size / 2;
        if (size % 2 == 0 || median == 0) median = median + 1;
        return median;
    }

    public static String roundDecimalString(double value, DoubleFormat format) {
        return String.valueOf(roundDecimal(value, format));
    }

    public static double roundDecimal(String value, DoubleFormat format) {
        return roundDecimal(Double.valueOf(value), format);
    }

    public static double roundDecimal(double value, DoubleFormat format) {
        DecimalFormat decimalFormat = new DecimalFormat(DoubleFormat.getFormat(format));
        return Double.parseDouble(decimalFormat.format(value));
    }

    public static String removeDecimal(double value) {
        DecimalFormat decimalFormat = new DecimalFormat(DoubleFormat.getFormat(DoubleFormat.ZERO_DECIMAL));
        return decimalFormat.format(value);
    }

    public static String convertQuintalToString(double value, UnitUtils.Weight price_unit) {
        switch (price_unit) {
            case KG:
                //value = value / 100;
                //value = roundDecimal(value, DoubleFormat.ONE_DECIMAL);
                return value + " / Kg";
            case QUINTAL:
                value = value / 1000;
                value = roundDecimal(value, DoubleFormat.ONE_DECIMAL);
                return value + "k / Qtl";
            case TON:
                value = value / 100;
                return (int) value + "k / Ton";
        }
        return "";
    }

    public static String convertQuintalToStringWithoutUnit(double value, UnitUtils.Weight price_unit) {
        switch (price_unit) {
            case KG:
                //value = value / 100;
                //value = roundDecimal(value, DoubleFormat.ONE_DECIMAL);
                return value + "";
            case QUINTAL:
                value = value / 1000;
                value = roundDecimal(value, DoubleFormat.ONE_DECIMAL);
                return value + "k";
            case TON:
                value = value / 100;
                return (int) value + "k";
        }
        return "";
    }

    public static String convertToStringWithoutUnit(double value, UnitUtils.Weight price_unit) {
        switch (price_unit) {
            case KG:
                value = roundDecimal(value, DoubleFormat.ONE_DECIMAL);
                return value + "";
            case QUINTAL:
                value = value / 10;
                value = roundDecimal(value, DoubleFormat.ONE_DECIMAL);
                return value + "k";
            case TON:
                return (int) value + "k";
        }
        return "";
    }

    public static double convertQuintal(double value, int price_unit) {
        double denominator = 0;
        switch (price_unit) {
            case PriceUnit.KG:
                denominator = 100;
                break;
            case PriceUnit.QUINTAL:
                denominator = 1;
                break;
            case PriceUnit.TONNE:
                denominator = 0.1;
                break;
        }

        return value / denominator;
    }

    public enum DoubleFormat {
        ZERO_DECIMAL,
        ONE_DECIMAL,
        TWO_DECIMAL;

        public static String getFormat(DoubleFormat format) {
            switch (format) {
                case ZERO_DECIMAL:
                    return "#";
                case ONE_DECIMAL:
                    return "#.0";
                case TWO_DECIMAL:
                    return "#.00";
                default:
                    return "#.0";
            }
        }
    }
}
