package com.agriiprince.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DateUtil {

    public static final int HOURS_PER_DAY = 24;
    public static final int MINUTES_PER_HOUR = 60;
    public static final int SECONDS_PER_MINUTE = 60;
    public static final int MILLI_SECONDS_PER_SECOND = 1000;

    public static String dateFormat = "dd-MM-yyyy hh:mm";

    public static String ConvertMilliSecondsToFormattedDate(String format, String milliSeconds) {
        return ConvertMilliSecondsToFormattedDate(format, Long.parseLong(milliSeconds));
    }

    public static String ConvertMilliSecondsToFormattedDate(String format, long milliSeconds) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return simpleDateFormat.format(calendar.getTime());
    }


    public static boolean isDateBeforeToday(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return isDateBeforeToday(calendar);
    }

    public static boolean isDateBeforeToday(Calendar calendar) {
        Calendar today = Calendar.getInstance();
        if (calendar.before(today)) {
            return calendar.get(Calendar.DATE) != today.get(Calendar.DATE);
        } else {
            return false;
        }
    }

    public static boolean isTimeBeforeToday(Calendar calendar) {
        Calendar today = Calendar.getInstance();
        return calendar.before(today);
    }

    public static long getTimeForServerDate(String value) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
            return dateFormat.parse(value).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public static String getServerDate(Calendar calendar) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        return dateFormat.format(calendar.getTime());
    }

    public interface Fomat {
        String DD_MMM_YYYY =  "dd MMM yyyy";
    }
}
