package com.agriiprince.utils;

import android.text.Html;
import android.util.Log;
import android.util.Xml;

import com.agriiprince.model.RssFeedModel;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.agriiprince.utils.Utils.showLog;

public class DateConvertUtils {

    private final String TAG = DateConvertUtils.class.getSimpleName();

    private List<String> dateFormats = Arrays.asList(
            "yyyy-MM-dd HH:mm:ss",
            "EEE, dd MMM yyyy HH:mm:ss Z",
            "yyyy-MM-dd"
    );

    DateFormat dateFormatterIndiaTimes = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
    //Tue, 11 Sep 2018 14:27:20 +0530
//    DateFormat dateFormatterHindu = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
    DateFormat dateFormatterDef = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);

    private Date parseDate(String pub) {
        Date date = null;

        pub = pub.split("T")[0].trim();

        for (String format : dateFormats) {
            try {

                date = new SimpleDateFormat(format, Locale.ENGLISH).parse(pub);

                Log.d(TAG, format);
                return date;

            } catch (ParseException e) {
            }
        }

        return date;
    }

    public List<RssFeedModel> parseFeed(String source, InputStream inputStream) throws XmlPullParserException,
            IOException {
        String title = null;
        String link = null;
        String description = null;
        String img = null;
        Date date = null;
        boolean isItem = false;
        List<RssFeedModel> items = new ArrayList<>();

        try {
            XmlPullParser xmlPullParser = Xml.newPullParser();
            xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            xmlPullParser.setInput(inputStream, null);

            xmlPullParser.nextTag();
            while (xmlPullParser.next() != XmlPullParser.END_DOCUMENT) {
                int eventType;
                String name;
                String result;

                eventType = xmlPullParser.getEventType();

                name = xmlPullParser.getName();
                if (name == null)
                    continue;

                if (eventType == XmlPullParser.END_TAG) {
                    if (name.equalsIgnoreCase("item")) {
                        isItem = false;
                    }
                    continue;
                }

                if (eventType == XmlPullParser.START_TAG) {
                    if (name.equalsIgnoreCase("item")) {
                        isItem = true;
                        continue;
                    }
                }


                result = "";
                if (xmlPullParser.next() == XmlPullParser.TEXT) {
                    result = xmlPullParser.getText();
                    xmlPullParser.nextTag();
                }

                if (name.equalsIgnoreCase("title")) {
                    title = result;
                } else if (name.equalsIgnoreCase("link")) {
                    link = result;
                } else if (name.equalsIgnoreCase("description")) {
                    description = result;
                    switch (source) {
                        case "Down To Earth":
                            description = result;
                            if (description.contains("<a href")) {
                                int d = description.indexOf("</a>");
                                description = description.substring(d + 4);
                            }
                            description = description.replace("&#039;", "'").replace("&amp;nbsp;", " ")
                                    .replace("&amp;rsquo;", "'");
                            break;

                        default:
                            description = Html.fromHtml(result).toString();
                            break;
                    }


                } else if (name.equalsIgnoreCase("image")) {
                    img = result;
                } else if (name.equalsIgnoreCase("pubDate")) {
                    date = parseDate(result);

                } else if (name.equalsIgnoreCase("dc:date")) {
                    date = parseDate(result);

                }

                if (title != null && link != null && description != null && date != null && isItem) {
                    RssFeedModel item = new RssFeedModel(title, link, description, date);
                    item.setDate(date);
                    item.setSource(source);
                    item.setImg(img);
                    items.add(item);

                    title = null;
                    link = null;
                    description = null;
                    date = null;
                    isItem = false;
                }
            }


        } catch (Exception e) {
            showLog("exception " + e.getMessage());
        } finally {
            inputStream.close();
        }
        return items;
    }
}
