package com.agriiprince.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;

import com.agriiprince.R;
import com.agriiprince.mvvm.data.prefs.PrefManager;

import java.util.ArrayList;
import java.util.Locale;

import static android.text.Spanned.SPAN_INCLUSIVE_INCLUSIVE;

public class StringUtils {

    private static final String TAG = "StringUtils";

    private PrefManager pref;
    private boolean isBilingual;
    private boolean isSameLanguage;
    private String mPrimaryLocale;
    private String mSecondaryLocale;
    private Context mPrimaryContext;
    private Context mSecondaryContext;


    public StringUtils(Context context) {
        this.mPrimaryContext = context;
        this.pref = new PrefManager(context);

        mPrimaryLocale = pref.getLocale();
        mSecondaryLocale = pref.getSecondaryLocale();

        Log.d(TAG, "StringUtils: " + mPrimaryLocale);

        setPrimaryContext();
        setSecondaryContext();
    }


    /**
     * this method will update the preferences and bilingual context to deliver the right primary strings
     * @param primaryLocale primary language code
     */
    public void setPrimaryLocale(String primaryLocale) {
        this.mPrimaryLocale = primaryLocale;
//        pref.setLocale(primaryLocale);
        setPrimaryContext();
    }

    /**
     * this method will update the preferences and bilingual context to deliver the right secondary strings
     * @param secondaryLocale secondary language code
     */
    public void setSecondaryLocale(String secondaryLocale) {
        this.mSecondaryLocale = secondaryLocale;
//        pref.setSecondaryLocale(secondaryLocale);
        setSecondaryContext();
    }

    /**
     * update the primary context on locale change
     */
    private void setPrimaryContext() {
        Locale locale = new Locale(mPrimaryLocale);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        mPrimaryContext.getResources().updateConfiguration(config,
                mPrimaryContext.getResources().getDisplayMetrics());

        setSecondaryContext();
    }

    /**
     * check for bilingual and update the secondary context if bilingual
     */
    private void setSecondaryContext() {
        setBilingual();
        if (isBilingual) {
            Configuration config = new Configuration(mPrimaryContext.getResources().getConfiguration());
            config.setLocale(new Locale(mSecondaryLocale));
            mSecondaryContext = mPrimaryContext.createConfigurationContext(config);
            Log.d(TAG, "setSecondaryContext: " + mSecondaryLocale);
        } else {
            mSecondaryContext = null;
        }
    }

    /**
     * checks if secondary locale is valid and not equal to primary locale and set the secondary locale value
     */
    private void setBilingual() {
        isBilingual = !TextUtils.isEmpty(mSecondaryLocale) && !mPrimaryLocale.equalsIgnoreCase(mSecondaryLocale);
    }



    public PrefManager getPref() {
        return pref;
    }

    public String getBilingualStringForApi(String enLng, String hiLng) {
        return getBilingualStringForApi(enLng, hiLng, 1, false);
    }

    public String getBilingualStringForApi(String enLng, String hiLng, boolean singleLine) {
        return getBilingualStringForApi(enLng, hiLng, 1, singleLine);
    }

    public String getBilingualStringForApi(String enLng, String hiLng, int spaceLines) {
        return getBilingualStringForApi(enLng, hiLng, spaceLines, false);
    }

    public String getBilingualStringForApi(String enLng, String hiLng, int spaceLines, boolean singleLine) {
        if (TextUtils.isEmpty(hiLng)) return enLng;
        if (!isBilingual || isSameLanguage) {
            if (mPrimaryLocale.equalsIgnoreCase("en"))
                return TextUtils.isEmpty(enLng) ? "" : enLng;
            else
                return TextUtils.isEmpty(hiLng) ? "" : hiLng;
        } else {
            if (mPrimaryLocale.equalsIgnoreCase("en")) {
                if (singleLine) {
                    return enLng + " / " + hiLng;
                } else {
                    String text = enLng;
                    while (spaceLines > 0) {
                        text = text.concat("\n");
                        spaceLines--;
                    }
                    return text + hiLng;
                }
            } else {
                if (singleLine) {
                    return hiLng + " / " + enLng;
                } else {
                    String text = hiLng;
                    while (spaceLines > 0) {
                        text = text.concat("\n");
                        spaceLines--;
                    }
                    return text + enLng;
                }
            }
        }
    }

    public String getPrimaryString(int id) {
        return mPrimaryContext.getResources().getString(id);
    }

    @Nullable
    public String getSecondaryString(int id) {
        if (! isSameLanguage && isBilingual) return mSecondaryContext.getResources().getString(id);
        else return null;
    }

    public boolean showSecondaryText() {
        return ! isSameLanguage && isBilingual;
    }

    public SpannableString getLocalizedString(int id) {
        //return getLocalizedStringWithSize(id, mPrimaryContext.getResources().getDimensionPixelSize(R.dimen.default_text_size));
        return getLocalizedStringWithSize(id, false, 0);
    }

    public ArrayList<SpannableString>  getLocalizedStringArray(int id) {
        //return getLocalizedStringWithSize(id, mPrimaryContext.getResources().getDimensionPixelSize(R.dimen.default_text_size));
        return getLocalizedStringArrayWithSize(id, false, 0);
    }

    /*public ArrayList<SpannableString> getLocalizedStringArray(int id, Context context) {
        //return getLocalizedStringWithSize(id, mPrimaryContext.getResources().getDimensionPixelSize(R.dimen.default_text_size));
        //ArrayList<SpannableString> arrayListToReturn = new ArrayList<>();
        //String[] forSize = context.getResources().getStringArray(R.array.farmer_tips_title);
        for(int i = 0; i < forSize.length; i++)
            arrayListToReturn.add(getLocalizedStringWithSize(id[i], false, 0));
        return arrayListToReturn;
    }*/

    // use this for single line
    public SpannableString getLocalizedString(int id, boolean isSingleLine) {
        return getLocalizedStringWithSize(id, isSingleLine, 0);
    }

    public SpannableString getLocalizedStringWithSize(int id, boolean isSingleLine, int textSize) {
        return getLocalizedStringCustomized(id, isSingleLine, textSize, ContextCompat.getColor(mPrimaryContext, R.color.black), Typeface.NORMAL);
    }

    public ArrayList<SpannableString> getLocalizedStringArrayWithSize(int id, boolean isSingleLine, int textSize) {
        return getLocalizedStringArrayCustomized(id, isSingleLine, textSize, ContextCompat.getColor(mPrimaryContext, R.color.black), Typeface.NORMAL);
    }

    public SpannableString getLocalizedStringWithColor(int id, boolean isSingleLine, int textColor) {
        return getLocalizedStringCustomized(id, isSingleLine, mPrimaryContext.getResources().getDimensionPixelSize(R.dimen.default_text_size),
                textColor, Typeface.NORMAL);
    }


    public SpannableString getLocalizedStringCustomized(int id, boolean isSingleLine, int textSize, int textColor, int typeface) {
        return getBilingualString(isSingleLine, mPrimaryContext.getResources().getString(id), isBilingual && !isSameLanguage ? mSecondaryContext.getText(id) : null, textSize, textColor, typeface);//String.format("%s%s", mPrimaryContext.getString(id), isBilingual && !isSameLanguage ? "\n" + mSecondaryContext.getText(id) : "");
    }

    public ArrayList<SpannableString> getLocalizedStringArrayCustomized(int id, boolean isSingleLine, int textSize, int textColor, int typeface) {
        ArrayList<SpannableString> listtoReturn = new ArrayList<>();

        String[] forSize = mPrimaryContext.getResources().getStringArray(id);
        for(int i = 0; i < forSize.length; i++)
            listtoReturn.add(getBilingualStringArray(isSingleLine, mPrimaryContext.getResources().getStringArray(id)[i], isBilingual && !isSameLanguage ? mSecondaryContext.getResources().getStringArray(id)[i] : null, textSize, textColor, typeface));

        return listtoReturn;
    }

    public SpannableString getLocalizedStringCustomized(String primaryLanguageText, String secondaryLanguageText, boolean isSingleLine) {
        return getBilingualString(isSingleLine, primaryLanguageText, secondaryLanguageText,
                0, ContextCompat.getColor(mPrimaryContext, R.color.black), Typeface.NORMAL);//String.format("%s%s", mPrimaryContext.getString(id), isBilingual && !isSameLanguage ? "\n" + mSecondaryContext.getText(id) : "");
    }

    public SpannableString getBilingualString(boolean isSingleLine, CharSequence primaryLanguageText, CharSequence secondaryLanguageText, int textSize, int textColor, int typeface) {

        String localString = " ";
        if (isSingleLine) {
            localString = String.format("%s%s", primaryLanguageText, isNotEmpty(secondaryLanguageText) ? "/" + secondaryLanguageText : "");

        } else {
            localString = String.format("%s%s", primaryLanguageText, isNotEmpty(secondaryLanguageText) ? "\n" + secondaryLanguageText : "");
        }

        SpannableString spannableString = new SpannableString(localString);
        if (textSize > 0)
            spannableString.setSpan(new AbsoluteSizeSpan(textSize), 0, spannableString.length(), SPAN_INCLUSIVE_INCLUSIVE);//set Text size

        //spannableString.setSpan(new ForegroundColorSpan(textColor), 0, spannableString.length(), SPAN_INCLUSIVE_INCLUSIVE);//set Text color
        spannableString.setSpan(new StyleSpan(typeface), 0, spannableString.length(), SPAN_INCLUSIVE_INCLUSIVE);// set Text type - Normal, Bold, Italic
        return spannableString;
    }

    public SpannableString getBilingualStringArray(boolean isSingleLine, CharSequence primaryLanguageText, CharSequence secondaryLanguageText, int textSize, int textColor, int typeface) {

        String localString = " ";
        if (isSingleLine) {
            localString = String.format("%s%s", primaryLanguageText, isNotEmpty(secondaryLanguageText) ? "/" + secondaryLanguageText : "");

        } else {
            localString = String.format("%s%s", primaryLanguageText, isNotEmpty(secondaryLanguageText) ? "\n" + secondaryLanguageText : "");
        }

        SpannableString spannableString = new SpannableString(localString);
        if (textSize > 0)
            spannableString.setSpan(new AbsoluteSizeSpan(textSize), 0, spannableString.length(), SPAN_INCLUSIVE_INCLUSIVE);//set Text size

        //spannableString.setSpan(new ForegroundColorSpan(textColor), 0, spannableString.length(), SPAN_INCLUSIVE_INCLUSIVE);//set Text color
        spannableString.setSpan(new StyleSpan(typeface), 0, spannableString.length(), SPAN_INCLUSIVE_INCLUSIVE);// set Text type - Normal, Bold, Italic
        return spannableString;
    }


    public SpannableString getDefaultBilingualString(boolean isSingleLine, CharSequence primaryLanguageText, CharSequence secondaryLanguageText) {
        return getBilingualString(isSingleLine, primaryLanguageText, secondaryLanguageText, mPrimaryContext.getResources().getDimensionPixelSize(R.dimen.default_text_size),
                ContextCompat.getColor(mPrimaryContext, R.color.black), Typeface.NORMAL);
    }

    public boolean isNotEmpty(CharSequence text) {
        return text != null && text.length() > 0;
    }


    public SpannableString getLocalizedStringWithTypeface(int id, boolean isSingleLine, int typeface) {
        return getLocalizedStringCustomized(id, isSingleLine, mPrimaryContext.getResources().getDimensionPixelSize(R.dimen.default_text_size),
                ContextCompat.getColor(mPrimaryContext, R.color.black), typeface);
    }

    //Without defaults
    public String getBilingualString(int id) {
        return String.format("%s%s", mPrimaryContext.getResources().getString(id),
                isBilingual && !isSameLanguage && isNotEmpty(mSecondaryContext.getText(id)) ? "\n" + mSecondaryContext.getText(id) : "");
    }

    public String getBilingualString(String primaryText, String secondaryText) {
        return getBilingualString(primaryText, secondaryText, false);
    }

    public String getBilingualString(String primaryText, String secondaryText, boolean isSingleLine) {
        if (isSingleLine) {
            return String.format("%s%s", primaryText,
                    isBilingual && !isSameLanguage && isNotEmpty(secondaryText) ? " / " + secondaryText : "");
        } else {
            return String.format("%s%s", primaryText,
                    isBilingual && !isSameLanguage && isNotEmpty(secondaryText) ? "\n" + secondaryText : "");
        }
    }
}
