package com.agriiprince.utils;

import java.util.ArrayList;
import java.util.List;

public class UnitUtils {

    public static final String[] area = {
            "Acre",
            "Hectare",
            "Bigha"
    };

    public static final String[] weight = {
            "Kg",
            "Qtl",
            "Ton"
    };

    public static final String[] lenght = {
            "Km",
            "Mtr"
    };

    public static List<String> getWastages() {
        List<String> wastages = new ArrayList<>();
        for (int i = 0; i <= 20; i++) {
            wastages.add(i * 5 + " %");
        }
        return wastages;
    }


    public enum Area {
        ACRE,
        HECTARE,
        BIGHA
    }

    public enum Weight {
        KG,
        QUINTAL,
        TON
    }

    public enum Length {
        KM,
        METERS
    }
}
