package com.agriiprince.utils;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Utils {

    /**
     * @param s - msg to print in log
     */
    public static void showLog(String s) {
        Log.d("AgriPrinceInit", s);
    }

    /**
     * Custom toast!
     *
     * @param activity   - calling activity
     * @param strMessage - message to display
     */
    public static void showToast(@NonNull Activity activity, String strMessage) {
        if (activity == null) return;

        LayoutInflater inflater = activity.getLayoutInflater();
        View layout_toast = inflater
                .inflate(R.layout.layout_toast, (ViewGroup) activity.findViewById(R.id
                        .custom_toast_container));

        TextView tvToast = layout_toast.findViewById(R.id.text);
        tvToast.setText(strMessage);

        //mToast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
        Toast mToast = new Toast(activity);
        mToast.setDuration(Toast.LENGTH_LONG);
        mToast.setView(layout_toast);

        if (mToast.getView().isShown()) {
            Log.d("TAG", "cancelled previous toast");
            mToast.cancel();
        }
        mToast.show();
    }

    /**
     * load json from asset
     *
     * @param activity - activity to be passed
     * @return json - output in string
     */
    public static String loadJSONFromAsset(Activity activity) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open("cities.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

   /* InputMethodManager imm = (InputMethodManager) getApplicationContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(mobileNoInput.getWindowToken(), 0);
            imm.hideSoftInputFromWindow(userPasswordInput.getWindowToken(), 0);*/


    public static List<String> getCommodityGrades() {
        List<String> grades = new ArrayList<>();

        grades.add("All");
        grades.add("Small");
        grades.add("Medium");
        grades.add("Large");

        return grades;
    }

    public static List<String> getWeightUnits() {
        List<String> units = new ArrayList<>();

        units.add("Kilos");
        units.add("Quintal");
        units.add("Tonnes");

        return units;
    }


    // Converting InputStream to String
    private static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    public static class GetMethodDemo extends AsyncTask<String, Void, String> {

        String server_response;

        @Override
        protected String doInBackground(String... strings) {

            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();

                int responseCode = urlConnection.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {
                    server_response = readStream(urlConnection.getInputStream());
                    Log.v("CatalogClient", server_response);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.e("Response", "" + server_response);


        }
    }

    public static void showVolleyError(Context context, VolleyError error) {
        StringUtils stringUtils = AppController.getInstance().getStringUtils();

        if (error instanceof TimeoutError) {
            Toast.makeText(context, stringUtils.getLocalizedString(R.string.volley_time_out_error), Toast.LENGTH_SHORT).show();
        } else if (error instanceof NoConnectionError) {
            Toast.makeText(context, stringUtils.getBilingualString(R.string.volley_no_internet_error), Toast.LENGTH_SHORT).show();
        } else if (error instanceof ServerError) {
            Toast.makeText(context, stringUtils.getBilingualString(R.string.volley_server_error), Toast.LENGTH_SHORT).show();
        } else if (error instanceof NetworkError) {
            Toast.makeText(context, stringUtils.getBilingualString(R.string.volley_network_error), Toast.LENGTH_SHORT).show();
        } else if (error instanceof ParseError) {
            Toast.makeText(context, stringUtils.getBilingualString(R.string.volley_parse_error), Toast.LENGTH_SHORT).show();
        }
    }


    public static float dpToPx(Context context, int dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dp, context.getResources().getDisplayMetrics());
    }

    public static int extractInt(String str)
    {
        String splitArr[] = str.split(",");
        String realValue = splitArr[0];

        if(realValue.endsWith("}})"))
        {
            int len = realValue.length();
            realValue = realValue.substring(0,len-3);
        }
        else if(realValue.endsWith("}}"))
        {
            int len = realValue.length();
            realValue = realValue.substring(0,len-2);
        }
        int res = Integer.parseInt(realValue);

        return res;
    }

}