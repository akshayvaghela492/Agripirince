package com.agriiprince.coldstorage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agriiprince.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ColdStorageDetailFragment extends Fragment {

    public TextView name, distance, address, contact, address_full, pincode;
    public LinearLayout fragment_cold_storage_detail;

    public ColdStorageDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_cold_storage_detail, container, false);
        name = view.findViewById(R.id.name);
        distance = view.findViewById(R.id.distance);
        address = view.findViewById(R.id.address);
        contact = view.findViewById(R.id.contact);
        address_full = view.findViewById(R.id.address_full);
        pincode = view.findViewById(R.id.pincode);
        fragment_cold_storage_detail = view.findViewById(R.id.fragment_cold_storage_detail);
        return view;
    }

}
