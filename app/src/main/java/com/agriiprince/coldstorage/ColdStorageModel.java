package com.agriiprince.coldstorage;

/**
 * Created by anuj on 22-03-2017.
 */


import com.google.gson.annotations.SerializedName;


public class ColdStorageModel {

    @SerializedName("cold_storage_id")
    public String cold_storage_id;
    @SerializedName("cold_storage_name")
    public String cold_storage_name;
    @SerializedName("cold_storage_address")
    public String cold_storage_address;
    @SerializedName("cold_storage_name_bi")
    public String cold_storage_name_bi;
    @SerializedName("cold_storage_address_bi")
    public String cold_storage_address_bi;
    @SerializedName("cold_storage_contact")
    public String cold_storage_contact;
    @SerializedName("cold_storage_lat")
    public String cold_storage_lat;
    @SerializedName("cold_storage_long")
    public String cold_storage_lng;
    @SerializedName("distance")
    public String cold_storage_distance;
}
