package com.agriiprince.coldstorage;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.utils.MathUtils;

public class MapMarker {

    private static final String TAG = "MapMarker";

    public static Bitmap CustomMarker(Context context, String storageName, String storageDistance) {

        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.map_marker_with_text, null);

        TextView name = marker.findViewById(R.id.name);
        TextView distance = marker.findViewById(R.id.distance);

        if (!TextUtils.isEmpty(storageName)) name.setText(storageName);
        if (!TextUtils.isEmpty(storageDistance)) {
            try {
                double value = Double.parseDouble(storageDistance) / 1000; // to km from m
                value = MathUtils.roundDecimal(value, MathUtils.DoubleFormat.ONE_DECIMAL);

                String distanceKm = String.valueOf(value) + " Km";
                distance.setText(distanceKm);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        marker.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        marker.draw(canvas);


        return bitmap;
    }
}
