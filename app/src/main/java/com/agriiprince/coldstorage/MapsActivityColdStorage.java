package com.agriiprince.coldstorage;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.activities.TutorialActivity;
import com.agriiprince.adapter.ColdStorageAdapter;
import com.agriiprince.adapter.CustomStringArrayAdapter;
import com.agriiprince.mvvm.applevel.analytics.FarmerRealtimeDBAnalytics;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.helper.UserLocation;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.coldstorage.ColdStorageResponse;
import com.agriiprince.mvvm.retrofit.model.coldstorage.ColdStorageList;
import com.agriiprince.mvvm.retrofit.service.ColdStorages;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CALL_PHONE;

public class MapsActivityColdStorage extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final String TAG = "MapsActivityColdStorage";

    private static final int PERMISSION_REQUEST_CODE = 19;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private final String MAX_DISTANCE = "300000";

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;

    UserProfile mUserProfile;
    FirebaseDatabase mFDatabase;
    DatabaseReference mRef;
    DatabaseReference mFarmersRef;
    DatabaseReference mNumRef;
    DatabaseReference mKeyRef;
    long mTimeStart;
    String mUserId;

    private double lat, lng;

    private LinearLayout expand, linearLayout;
    private RecyclerView recyclerView;
    private ImageView expand_image, back;
    private AutoCompleteTextView search_text;
    private ImageView search_open_button, drawer;
    private TextView header;

    int flag = 0, flag_search = 0;
    ArrayList<ColdStorageList> coldStorageList;
    ColdStorageDetailFragment coldStorageDetailFragment;
    ColdStorageAdapter adapter;
    CustomStringArrayAdapter adapter_names;

    private StringUtils utils;
    private TextView emptyText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_cold_storage);
        utils = AppController.getInstance().getStringUtils();
        mUserProfile = new UserProfile(this);
        mUserId = mUserProfile.getUserId();
        mFDatabase = FirebaseDatabase.getInstance();
        mRef = mFDatabase.getReference("analytics");
        mFarmersRef = mRef.child("farmers");
        mNumRef = mFarmersRef.child(mUserProfile.getUserMobile());

        emptyText = findViewById(R.id.empty_text);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        coldStorageDetailFragment = (ColdStorageDetailFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        coldStorageDetailFragment.fragment_cold_storage_detail.setVisibility(View.INVISIBLE);
        expand = findViewById(R.id.expand);
        linearLayout = findViewById(R.id.linearlayout);
        expand_image = findViewById(R.id.expand_image);
        back = findViewById(R.id.back);
        search_text = findViewById(R.id.search_text);
        search_open_button = findViewById(R.id.search_open_button);
        header = findViewById(R.id.switch_layout);
        drawer = findViewById(R.id.drawer);
        recyclerView = findViewById(R.id.listview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ImageView imgview_help =  findViewById(R.id.iv_help);

        imgview_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MapsActivityColdStorage.this, TutorialActivity.class);
                intent.putExtra("code", "SEARCS");
                startActivity(intent);
            }
        });

        search_open_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag_search == 0) {
                    header.setVisibility(View.GONE);
                    drawer.setVisibility(View.GONE);
                    search_text.setVisibility(View.VISIBLE);
                    search_text.setText(null);
                    search_text.setFocusableInTouchMode(true);
                    search_text.requestFocus();
                    search_open_button.setImageResource(R.drawable.close);
                } else {
                    header.setVisibility(View.VISIBLE);
                    drawer.setVisibility(View.VISIBLE);
                    search_text.setVisibility(View.GONE);
                    search_open_button.setImageResource(R.drawable.search);
                    adapter = new ColdStorageAdapter(MapsActivityColdStorage.this, coldStorageList);
                    adapter.setOnItemClickListener(onCallClick);
                    recyclerView.setAdapter(adapter);
                    closekeyboard();
                }
                flag_search = 1 - flag_search;

            }
        });

        expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewGroup.LayoutParams params = linearLayout.getLayoutParams();
//                Animation bottomUp = AnimationUtils.loadAnimation(MapsActivity.this,
//                        R.anim.bottom_up);
//                Animation bottomDown = AnimationUtils.loadAnimation(MapsActivity.this,
//                        R.anim.bottom_down);
                if (flag == 0) {
                    params.height = WindowManager.LayoutParams.MATCH_PARENT;
                    expand_image.setImageResource(R.drawable.down);
                    // linearLayout.startAnimation(bottomUp);
                } else {
                    DisplayMetrics displayMetrics = MapsActivityColdStorage.this.getResources().getDisplayMetrics();
                    float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
                    params.height = (int) (0.65 * dpHeight);
                    expand_image.setImageResource(R.drawable.up);
                    //  linearLayout.startAnimation(bottomDown);

                }
                params.width = WindowManager.LayoutParams.MATCH_PARENT;

                linearLayout.setLayoutParams(params);
                flag = 1 - flag;

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back.setVisibility(View.GONE);
                expand.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.VISIBLE);
                coldStorageDetailFragment.fragment_cold_storage_detail.setVisibility(View.INVISIBLE);
                ViewGroup.LayoutParams params = linearLayout.getLayoutParams();
                if (flag == 1) {
                    params.height = WindowManager.LayoutParams.MATCH_PARENT;
                    expand_image.setImageResource(R.drawable.down);
                } else {
                    DisplayMetrics displayMetrics = MapsActivityColdStorage.this.getResources().getDisplayMetrics();
                    float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
                    params.height = (int) (0.65 * dpHeight);
                    expand_image.setImageResource(R.drawable.up);
                }
                params.width = WindowManager.LayoutParams.MATCH_PARENT;

                linearLayout.setLayoutParams(params);
            }
        });
        search_text.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selected_name = adapter_names.getItem(position);
                ColdStorageList ob = null;
                for (ColdStorageList model : coldStorageList) {
                    if (model.getColdStorageName().equals(selected_name)) {
                        ob = model;
                        break;
                    }
                }
                ArrayList<ColdStorageList> temp = new ArrayList<>();
                temp.add(ob);
                adapter = new ColdStorageAdapter(MapsActivityColdStorage.this, temp);
                adapter.setOnItemClickListener(onCallClick);
                recyclerView.setAdapter(adapter);
                closekeyboard();

            }
        });

    }

    private void callColdStorage(ColdStorageList model) {
        Intent i = new Intent();
        i.setAction(Intent.ACTION_CALL);
        i.setData(Uri.parse("tel:" + model.getColdStorageContact().split(",")[0]));
        if (ActivityCompat.checkSelfPermission(this, CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        } else {
            startActivity(i);
        }
    }

    private ColdStorageAdapter.OnClickListener onCallClick = new ColdStorageAdapter.OnClickListener() {
        @Override
        public void onClickCall(int position) {
            callColdStorage(adapter.getItem(position));

        }
    };

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CALL_PHONE}, PERMISSION_REQUEST_CODE);
    }


    private void showAlertDialog(String message, String positiveButton, DialogInterface.OnClickListener positiveButtonClickListener,
                                 String negativeButton, DialogInterface.OnClickListener negativeButtonClickListener,
                                 boolean isCancellable) {
        android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(MapsActivityColdStorage.this);
        alert.setCancelable(isCancellable);
        alert.setMessage(message)
                .setPositiveButton(positiveButton, positiveButtonClickListener)
                .setNegativeButton(negativeButton, negativeButtonClickListener)
                .create()
                .show();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        try {
            mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style));
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);

        getData();
    }

    void getData() {
        new UserLocation(this).getLastKnowLocation(
                new UserLocation.OnUserLocationResults() {
                    @Override
                    public void onLocationResult(Location lastLocation) {
                        lat = lastLocation.getLatitude();
                        lng = lastLocation.getLongitude();
                        markUserLocation();
                        getColdStorages();
                    }

                    @Override
                    public void onLocationFail() {
                        try {
                            String city = mUserProfile.getUserCity();
                            Geocoder geocoder = new Geocoder(MapsActivityColdStorage.this);
                            Address address = geocoder.getFromLocationName(city, 1).get(0);
                            lat = address.getLatitude();
                            lng = address.getLongitude();
                            markUserLocation();
                            getColdStorages();

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );

    }

    private ProgressDialog dialog;


    void showEmptyText() {
        emptyText.setText(utils.getLocalizedString(R.string.no_cold_storage_found));
        emptyText.setVisibility(View.VISIBLE);
    }

    void hideEmptyText() {
        emptyText.setVisibility(View.GONE);
    }

    void getColdStorages() {

        if (dialog == null) {
            dialog = new ProgressDialog(this);
            dialog.setCancelable(false);
        }
        dialog.show();

        hideEmptyText();

 /*       StringRequest stringRequest = new StringRequest(
                Request.Method.POST, Config.GET_COLD_STORAGE_LIST_BI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // mProgressDialog.dismiss();
                        Log.d("cold_storage_list", response);
                        try {
                            JSONArray responseArray = new JSONArray(response);
                            JSONObject responseObject = responseArray.getJSONObject(0);
                            int errorCode = responseObject.getInt("error_code");
                            if (errorCode == 100) {
                                String data = responseObject.getString("data");
                                if (!TextUtils.isEmpty(data) && !data.equalsIgnoreCase("null")) {
                                    coldStorageList = new Gson().fromJson(data,
                                            new TypeToken<ArrayList<ColdStorageModel>>() {
                                            }.getType());

                                    if (coldStorageList != null) {
                                        adapter = new ColdStorageAdapter(MapsActivityColdStorage.this, coldStorageList);
                                        adapter.setOnItemClickListener(onCallClick);
                                        recyclerView.setAdapter(adapter);
                                        initialize_autocomplete();

                                        plotColdStoragesOnMap();

                                    } else {
                                        showEmptyText();
                                        Toast.makeText(MapsActivityColdStorage.this,
                                                utils.getLocalizedString(R.string.no_cold_storage_found),
                                                Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    showEmptyText();
                                    Toast.makeText(MapsActivityColdStorage.this,
                                            utils.getLocalizedString(R.string.no_cold_storage_found),
                                            Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                showEmptyText();
                                Toast.makeText(MapsActivityColdStorage.this,
                                        utils.getLocalizedString(R.string.something_went_wrong_try_again),
                                        Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            dialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        GeneralUtils.showVolleyError(MapsActivityColdStorage.this, error);
                        dialog.dismiss();

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());
                params.put("latitude", String.valueOf(lat));
                params.put("longitude", String.valueOf(lng));
                params.put("max_distance", MAX_DISTANCE);

                PrefManager prefManager = new PrefManager(MapsActivityColdStorage.this);
                String primaryLocale = prefManager.getLocale();
                String secondaryLocale = prefManager.getSecondaryLocale();

                if (TextUtils.isEmpty(secondaryLocale)) {
                    secondaryLocale = primaryLocale.equals("en") ? "hi" : primaryLocale;
                } else {
                    secondaryLocale = primaryLocale.equals("en") ? secondaryLocale : primaryLocale;
                }

                params.put("primary_language", "en");
                params.put("secondary_language", secondaryLocale);


                Log.d("params cs:   ", params.toString());
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

        AppController.getInstance().addToRequestQueue(stringRequest);
        */
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id",mUserProfile.getUserId());
        params.put("user_type",mUserProfile.getUserType());

        params.put("api_token", mUserProfile.getApiToken());
        params.put("latitude", String.valueOf(lat));
        params.put("longitude", String.valueOf(lng));
        params.put("max_distance", MAX_DISTANCE);

        PrefManager prefManager = new PrefManager(MapsActivityColdStorage.this);
        String primaryLocale = prefManager.getLocale();
        String secondaryLocale = prefManager.getSecondaryLocale();

        if (TextUtils.isEmpty(secondaryLocale)) {
            secondaryLocale = primaryLocale.equals("en") ? "hi" : primaryLocale;
        } else {
            secondaryLocale = primaryLocale.equals("en") ? secondaryLocale : primaryLocale;
        }

        params.put("primary_language", "en");
        params.put("secondary_language", secondaryLocale);


        Log.d("params cs:   ", params.toString());
        API_Manager api_manager=new API_Manager();
        ColdStorages retrofit_interface = api_manager.getClient4().create(ColdStorages.class);

            Logs.d("CheckStatus", "coldstorage call");
            Call<ColdStorageResponse> call=retrofit_interface.getcoldstorage(mUserProfile.getApiToken(),params);
            call.enqueue(new Callback<ColdStorageResponse>() {
                @Override
                public void onResponse(Call<ColdStorageResponse> call, Response<ColdStorageResponse> response) {
                    try {
                        Logs.d("CheckStatus",response.toString());
                        if (response.body() != null && response.body().getCode() == 200) {
                            if (response.body().getData() != null) {
                                coldStorageList = (ArrayList<ColdStorageList>) response.body().getData();
                                Logs.d("CheckStatus", "coldstorage list" + coldStorageList);
                                adapter = new ColdStorageAdapter(MapsActivityColdStorage.this, coldStorageList);
                                adapter.setOnItemClickListener(onCallClick);
                                recyclerView.setAdapter(adapter);
                                initialize_autocomplete();

                                plotColdStoragesOnMap();

                            } else {
                                showEmptyText();
                                Toast.makeText(MapsActivityColdStorage.this,
                                        utils.getLocalizedString(R.string.no_cold_storage_found),
                                        Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            showEmptyText();
                            Toast.makeText(MapsActivityColdStorage.this,
                                    utils.getLocalizedString(R.string.something_went_wrong_try_again),
                                    Toast.LENGTH_LONG).show();
                    }
                    }catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ColdStorageResponse> call, Throwable t) {
                    Log.d("CheckStatus","cold storage error "+t.getMessage());
                    Utils.showToast(MapsActivityColdStorage.this, t.getMessage());
                    dialog.dismiss();
                }
            });
        }
    void markUserLocation() {
        LatLng latLng = new LatLng(lat, lng);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
    }

    void closekeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputManager != null && getCurrentFocus() != null)
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void plotColdStoragesOnMap() {
        if (coldStorageList == null) return;

        for (ColdStorageList model : coldStorageList) {
            //if (TextUtils.isEmpty(model.cold_storage_lat) || TextUtils.isEmpty(model.cold_storage_lng))
            if (TextUtils.isEmpty(model.getColdStorageLat()) || TextUtils.isEmpty(model.getColdStorageLong()))
                continue;

//            Log.d(TAG, "plotColdStoragesOnMap: " + model.cold_storage_name);

            try {
                //LatLng sydney = new LatLng(Double.parseDouble(model.cold_storage_lat), Double.parseDouble(model.cold_storage_lng));
                LatLng sydney = new LatLng(Double.parseDouble(model.getColdStorageLat()), Double.parseDouble(model.getColdStorageLong()));
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(sydney);
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(MapMarker
                        //.CustomMarker(MapsActivityColdStorage.this, model.cold_storage_name, model.cold_storage_distance)));
                        .CustomMarker(MapsActivityColdStorage.this, model.getColdStorageName(), String.valueOf(model.getDistance()))));
                mMap.addMarker(markerOptions);
            } catch (NumberFormatException | NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    void initialize_autocomplete() {
        ArrayList<String> names = new ArrayList<>();
        for (ColdStorageList model : coldStorageList) {
            names.add(utils.getBilingualStringForApi(model.getColdStorageName(), model.getColdStorageNameBi()));
        }
        adapter_names = new CustomStringArrayAdapter(this, android.R.layout.simple_list_item_1, names);
        search_text.setAdapter(adapter_names);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
//        mLocationRequest.setInterval(1000000000);
//        mLocationRequest.setFastestInterval(1000000000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } catch (Exception e) {
                //  Toast.makeText(MapLocationActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        // getAdresses(lat,lng);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
    }

    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
/*
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, MapsActivityColdStorage.this);
        }
*/
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MapsActivityColdStorage.this,
                                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;

            }
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getApplicationContext(), "Permission Granted, Now you can proceed ", Toast.LENGTH_LONG).show();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CALL_PHONE)) {
                                showAlertDialog("You need to allow access to the permissions",
                                        "OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermission();
                                                }
                                            }
                                        },
                                        "Cancel", null, true);
                                return;
                            }
                        }
                    }
                }
                break;

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTimeStart = System.currentTimeMillis();
    }

    @Override
    protected void onStop() {
        super.onStop();
        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - mTimeStart;
        final int elapsedSeconds = (int)(tDelta / 1000.0);

        Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    FarmerRealtimeDBAnalytics.saveEventFarmerTimeSpent(
                            dataSnapshot, "time_spent_cold_storage", mNumRef, elapsedSeconds);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
}
