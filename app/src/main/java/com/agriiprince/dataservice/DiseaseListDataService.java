package com.agriiprince.dataservice;

import android.text.TextUtils;
import android.util.Log;

import com.agriiprince.db.DatabaseManager;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.db.DatabaseCallback;
import com.agriiprince.db.DiseaseDao;
import com.agriiprince.models.CropDiseaseModel;
import com.agriiprince.models.DiseaseListModel;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.disease.DiseaseDetailResponse;
import com.agriiprince.mvvm.retrofit.model.disease.DiseaseDetailModel;
import com.agriiprince.mvvm.retrofit.service.Disease;
import com.agriiprince.mvvm.util.Logs;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Manu Sajjan on 15-12-2018.
 */
public class DiseaseListDataService implements DatabaseCallback<DiseaseListModel> {

    private final String TAG = DiseaseListDataService.class.getSimpleName();

    private DataResponse<List<DiseaseListModel>> diseaseCropListModelDataResponse;

    private DiseaseDao diseaseDao;

    private String primaryLocale;
    private String secondaryLocale;

    private String cropName;

    @Override
    public void onInsert() {

    }

    @Override
    public void onInsertAll() {
        List<DiseaseListModel> diseaseListModels = diseaseDao.getDiseaseListModelList(cropName, primaryLocale, secondaryLocale);
        if (diseaseCropListModelDataResponse != null)
            diseaseCropListModelDataResponse.onSuccessResponse(diseaseListModels);
    }

    @Override
    public void onDelete() {

    }

    @Override
    public void onDeleteAll() {

    }

    @Override
    public void onFetch(DiseaseListModel results) {

    }

    @Override
    public void onFetchAll(List<DiseaseListModel> results) {

    }

    public DiseaseListDataService(PrefManager prefManager, DatabaseManager databaseManager) {
        diseaseDao = new DiseaseDao(databaseManager);
        primaryLocale = prefManager.getLocale();
        secondaryLocale = prefManager.getSecondaryLocale();
        if (TextUtils.isEmpty(secondaryLocale)) {
            secondaryLocale = primaryLocale.equals("en") ? "hi" : primaryLocale;
        } else {
            secondaryLocale = primaryLocale.equals("en") ? secondaryLocale : primaryLocale;
        }

        primaryLocale = "en";
    }

    public void getCropDiseaseList(String cropName, String api_token, DataResponse<List<DiseaseListModel>> dataResponse) {
        this.cropName = cropName;
        this.diseaseCropListModelDataResponse = dataResponse;
        Log.d("TESTING_VOLLEY", "fetchDiseaseInfo-" + cropName);
        getCropDiseaseInfo(cropName, api_token);
    }

    private void getCropDiseaseInfo(final String cropName, final String api_token) {

        Log.d("TESTING_VOLLEY", "fetchDiseaseInfo-1");
        List<DiseaseListModel> diseaseListModels = diseaseDao.getDiseaseListModelList(cropName, primaryLocale, secondaryLocale);
        if (diseaseListModels != null && diseaseListModels.size() > 0) {
            Log.d("TESTING_VOLLEY", "fetchDiseaseInfo0a");
            if (diseaseCropListModelDataResponse != null)
                diseaseCropListModelDataResponse.onSuccessResponse(diseaseListModels);
        } else {
            Log.d("TESTING_VOLLEY", "fetchDiseaseInfo0b");
            fetchDiseaseInfoNew(cropName, api_token);
            //fetchDiseaseInfo(cropName, api_token);
        }
    }

    public void requestToDowload(String cropName) {
        diseaseDao.requestForDownloadLater(cropName, primaryLocale);
    }

    API_Manager api_manager=new API_Manager();
    Disease retrofit_interface = api_manager.getClient6().create(Disease.class);

    public void fetchDiseaseInfoNew(final String cropName, final String api_token) {

        Call<DiseaseDetailResponse> call = retrofit_interface.getDiseasedetail(api_token, cropName,"en","hi");
        call.enqueue(new Callback<DiseaseDetailResponse>() {
            @Override
            public void onResponse(Call<DiseaseDetailResponse> call, retrofit2.Response<DiseaseDetailResponse> response) {
                Log.d("CHECK","onResponse: Disease details"+response);
                Log.d("CHECK","onResponse: Disease details"+response.body().getStatus());
                try {
                    if (response.body() != null && response.body().getStatus().equalsIgnoreCase("success")) {
                        Logs.d("CHECK", response.body().getStatus());
                        Logs.d("CHECK", response.body().getData().getPrimary().getData().get(0).getCropName());
                        Logs.d("CHECK", response.body().getData().getPrimary().getData().get(0).getCropDiseases());

                        List<DiseaseDetailModel> diseaseDetailModelArrayListPrimary = (ArrayList) response.body().getData().getPrimary().getData();
                        List<DiseaseDetailModel> diseaseDetailModelArrayLisSecondary = (ArrayList) response.body().getData().getSecondary().getData();
                        Logs.d("CHECK ---> ", ""+diseaseDetailModelArrayListPrimary + " " + diseaseDetailModelArrayListPrimary.get(0).getCropDiseases() + " " + diseaseDetailModelArrayListPrimary.get(1).getCropDiseases());

                        diseaseDao.updateDownloadStatus(cropName, primaryLocale);
                        Log.d("TESTING_VOLLEY", "fetchDiseaseInfo5");
                        diseaseDao.insertDiseaseInfoList(diseaseDetailModelArrayListPrimary, primaryLocale,
                                diseaseDetailModelArrayLisSecondary, secondaryLocale, DiseaseListDataService.this);

                    }
                }
                catch (Exception e){e.getMessage();}
            }

            @Override
            public void onFailure(Call<DiseaseDetailResponse> call, Throwable t) {
                Log.d("CHECK", "Disease details"+t.getMessage());
            }
        });
    }

    /*private void fetchDiseaseInfo(final String cropName, final String api_token) {//
        Log.d("TESTING_VOLLEY", "fetchDiseaseInfo1");
        StringRequest request = new StringRequest(Request.Method.POST, Config.POST_DISEASES_BI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY", "fetchDiseaseInfo2");
                        Log.d("TESTING_VOLLEY", response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            //JSONArray jsonArray = new JSONArray(response);
                            //JSONObject jsonObject = jsonArray.getJSONObject(0);
                            String data=jsonObject.getString("data");
                            Log.d("TESTING_VOLLEY", "fetchDiseaseInfo3");
                            Log.d("TESTING_VOLLEY",data);
                            JSONObject array = jsonObject.getJSONObject("data");
                            Log.d("TESTING_VOLLEY", "fetchDiseaseInfoArray" + array.toString());
                            CropDiseaseModel cropDiseaseModel =
                                   // new Gson().fromJson(jsonObject.toString(), new TypeToken<CropDiseaseModel>() {
                            new Gson().fromJson(String.valueOf(array), new TypeToken<CropDiseaseModel>() {
                                    }.getType());
                            Log.d("TESTING_VOLLEY", "fetchDiseaseInfoCropDisModel" + cropDiseaseModel);

                            //if (cropDiseaseModel.getError_code() == 100)
                            if (jsonObject.getInt("code") == 200)
                            {
                                Log.d("TESTING_VOLLEY", "fetchDiseaseInfo4");
                                diseaseDao.updateDownloadStatus(cropName, primaryLocale);
                                Log.d("TESTING_VOLLEY", "fetchDiseaseInfo5");
                                diseaseDao.insertDiseaseInfoList(cropDiseaseModel.getData_primary(), primaryLocale,
                                        cropDiseaseModel.getData_secondary(), secondaryLocale, DiseaseListDataService.this);
                                Log.d("TESTING_VOLLEY", "fetchDiseaseInfo" + (diseaseDao.getDiseaseNames("en","hi")).toString());
                                Log.d("TESTING_VOLLEY", "fetchDiseaseInfo" + (diseaseDao.getDiseaseInfoModel("18","en","hi")).toString());
                                Log.d("TESTING_VOLLEY", "fetchDiseaseInfo" + (diseaseDao.getDiseaseInfoModel("18","en","hi")).getDiseaseName());
                                Log.d("TESTING_VOLLEY", "fetchDiseaseInfo" + (diseaseDao.getDiseaseInfoModel("18","en","hi")).getDiseaseNameBi());
                                Log.d("TESTING_VOLLEY", "fetchDiseaseInfoDiseaseName - " + cropName);

                                Log.d("TESTING_VOLLEY", "fetchDiseaseInfo6");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (diseaseCropListModelDataResponse != null)
                            diseaseCropListModelDataResponse.onErrorResponse(error);

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", api_token);

                params.put("crop_diseases_id", "18");
                //params.put("crop_diseases_id", cropName);

                params.put("primary_language", primaryLocale);
                params.put("secondary_language", secondaryLocale);
                Log.d(TAG, params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",api_token);
                return header;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }*/

}
