package com.agriiprince.dataservice;

import android.util.Log;

import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.RssModel;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RssDataService {

    private static final String TAG = RssDataService.class.getName();

    public static void get(DataResponse<List<RssModel>> dataResponse, UserProfile userProfile) {

        final DataResponse<List<RssModel>> listener = dataResponse;
        final UserProfile mUserProfile = userProfile;

        StringRequest request = new StringRequest(Request.Method.POST, Config.POST_RSS_LIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY", "get Rss dataservice"+response);
                        try {
                            //JSONArray responseArray = new JSONArray(response);
                            JSONObject Object = new JSONObject(response);
                            //JSONObject responseObject = responseArray.getJSONObject(0);
                            //if (responseObject.getInt("error_code") == 100) {
                            if (Object.getInt("code") == 200)
                            {Log.d("TESTING_VOLLEY", "get rss dataservice"+Object.getString("message"));
                                //String data = responseObject.getString("data");
                                JSONObject object=Object.getJSONObject("data");
                                Log.d("TESTING_VOLLEY", "get rss dataservice"+Object.getString("data"));
                                List<RssModel> mRssModels = new Gson().fromJson(String.valueOf(object.getJSONArray("data")),
                                        new TypeToken<List<RssModel>>() {
                                        }.getType());
                                Log.d("TESTING_VOLLEY", "get rss dataservice list "+mRssModels);
                                listener.onSuccessResponse(mRssModels);

                            } else {
                                listener.onParseError();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("TESTING_VOLLEY", "get rss dataservice exceptions "+e.getMessage());
                            listener.onParseError();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onErrorResponse(error);
                        Log.d("TESTING_VOLLEY", "get rss dataservice error "+error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication", mUserProfile.getApiToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(request);

    }

}