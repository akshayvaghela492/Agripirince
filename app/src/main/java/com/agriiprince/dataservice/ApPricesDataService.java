package com.agriiprince.dataservice;

import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.util.Log;

import com.agriiprince.activities.checkprice.SingleEvenLiveData;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.model.OeReportedPrice;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.mvvm.model.crop.Grade;
import com.agriiprince.mvvm.model.crop.Variety;
import com.agriiprince.utils.Utils;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Manu Sajjan on 20-12-2018.
 *
 * @version 1.2.2
 */
public class ApPricesDataService {

    public static final String TAG = ApPricesDataService.class.getSimpleName();

    private MutableLiveData<List<OeReportedPrice>> oeList;

    private MutableLiveData<Boolean> dataAvailable;

    private MutableLiveData<Boolean> isLoading;

    public ApPricesDataService() {
        oeList = new MutableLiveData<>();
        this.dataAvailable = new SingleEvenLiveData<>();
        this.isLoading = new MutableLiveData<>();
    }

    public MutableLiveData<Boolean> isLoading() {
        return isLoading;
    }

    public MutableLiveData<Boolean> isDataAvailable() {
        return dataAvailable;
    }


    public MutableLiveData<List<OeReportedPrice>> getApPrices() {
        return oeList;
    }

    /**
     * Makes Volley Request to server to get OE Reported prices for the given crop values
     *
     * @param api_token   {@link String} api access token
     * @param user_id     {@link String} id of the user requesting
     * @param cropName    {@link Crop} Crop name for which prices are queried
     * @param varietyName {@link Variety}name for which prices are queried
     * @param gradeName   {@link Grade} name for which prices are queried
     */
    public void getPrices(final String api_token, final String user_id, final String date,
                          final String cropName, final String varietyName, final String gradeName) {

        isLoading.setValue(true);

       StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_OE_REPORTED_PRICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        isLoading.setValue(false);
                        Utils.showLog(response);
                        try {
                            Log.d("TESTING_VOLLEY","oe "+response);
                            JSONObject jsonObject=new JSONObject(response);
                            //JSONArray array = new JSONArray(response);
                            //JSONObject object = array.getJSONObject(0);
                            //int error = object.getInt("error_code");
                            int error = jsonObject.getInt("code");
                            //if (error == 100)
                            if (error == 200)
                            {
                                //String data = object.getString("data");
                                JSONObject data=jsonObject.getJSONObject("data");
                                JSONArray array=data.getJSONArray("data");
                                //List<OeReportedPrice> mOeReported = new Gson().fromJson(data,
                                List<OeReportedPrice> mOeReported = new Gson().fromJson(String.valueOf(array),
                                        new TypeToken<ArrayList<OeReportedPrice>>() {
                                        }.getType());
                                Log.d("TESTING_VOLLEY",mOeReported.get(0).getCommodityGrade());

                                if (mOeReported != null && mOeReported.size() > 0) {
                                    dataAvailable.setValue(true);
                                    new FixMandiLocationForMultiple(mOeReported).execute();
                                } else {
                                    dataAvailable.setValue(false);
                                }

                            } else {
                                dataAvailable.setValue(false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dataAvailable.setValue(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        isLoading.setValue(false);
                        dataAvailable.setValue(false);
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", api_token);
                params.put("user_id", user_id);
                params.put("commodity_name", cropName);
                params.put("commodity_variety", varietyName);
                params.put("commodity_grade", gradeName);

                params.put("arrival_date", date);

                Log.d("OE prices:   ", params.toString());

                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",api_token);
                return header;
            }
        };

        AppController.getInstance().getRequestQueue().cancelAll(stringRequest);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private class FixMandiLocationForMultiple extends AsyncTask<Void, Void, Void> {

        private List<OeReportedPrice> reportedPrices;

        public FixMandiLocationForMultiple(List<OeReportedPrice> reportedPrices) {
            this.reportedPrices = reportedPrices;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            int count;
            for (int i = 0; i < reportedPrices.size(); i++) {
                OeReportedPrice m1 = reportedPrices.get(i);
                count = 0;
                double offset = 0.00005;
                for (int j = i + 1; j < reportedPrices.size(); j++) {
                    OeReportedPrice m2 = reportedPrices.get(j);
                    double lat = m2.getMandiLat();
                    double lng = m2.getMandiLng();
                    if (m1.getMandiLat() == lat && m1.getMandiLng() == lng) {
                        switch (count) {
                            case 0:
                                m2.setMandiLat(lat + offset);
                                m2.setMandiLng(lng + offset);
                                break;
                            case 1:
                                m2.setMandiLat(lat + offset);
                                m2.setMandiLng(lng - offset);
                                break;
                            case 2:
                                m2.setMandiLat(lat - offset);
                                m2.setMandiLng(lng - offset);
                                break;
                            case 3:
                                m2.setMandiLat(lat - offset);
                                m2.setMandiLng(lng + offset);
                                break;
                        }
                        count++;
                        if (count == 4) {
                            count = 0;
                            offset = offset + 0.00005;
                        }
                    }
                }
            }
            oeList.postValue(reportedPrices);
            return null;
        }
    }
}
