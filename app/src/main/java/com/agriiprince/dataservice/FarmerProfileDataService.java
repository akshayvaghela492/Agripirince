package com.agriiprince.dataservice;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.FarmerProfileModel;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.profile.UserUpdate;
import com.agriiprince.mvvm.retrofit.model.login.Profile;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.utils.Utils;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class FarmerProfileDataService {

    private static final String TAG = FarmerProfileDataService.class.getSimpleName();

    private Context context;

    private UserProfile mUserProfile;

    private OnFarmerProfileResult onFarmerProfileResult;

    public FarmerProfileDataService(Context context) {
        this.context = context;
        this.mUserProfile = new UserProfile(context);
    }

    public void getFarmerProfile(OnFarmerProfileResult listener) {
        this.onFarmerProfileResult = listener;

        StringRequest stringRequest = new StringRequest(
                Request.Method.POST, Config.GET_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY", response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            //JSONArray responseArray = new JSONArray(response);
                            //JSONObject responseObject = responseArray.getJSONObject(0);
                            //int errorCode = responseObject.getInt("error_code");
                            int errorCode = jsonObject.getInt("code");
                            //if (errorCode == 100)
                            if (errorCode == 200)
                            {
                                //String data = responseObject.getString("user_details");
                                JSONObject dataobj = jsonObject.getJSONObject("data");
                                String data = dataobj.getString("user_details");
                                FarmerProfileModel profile = new Gson().fromJson(data, new TypeToken<FarmerProfileModel>() {
                                }.getType());
                                onFarmerProfileResult.onSuccess(profile);
                            } else {
                                Toast.makeText(context, "Sorry something went wrong. Please try after sometime.", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showVolleyError(context, error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());

                params.put("user_id", mUserProfile.getUserId());
                params.put("user_type", mUserProfile.getUserType());
                System.out.println("Get profile body: " + params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders()  {
                HashMap<String, String> header = new HashMap<>();
                header.put("api_token", mUserProfile.getApiToken());
                return header;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);

    }

    public void updateInterestMandiIds(final List<String> ids) {
    /*    StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_FARMER_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("farmer_profile", response);

                        mUserProfile.setInterestedMandiIds(TextUtils.join(",", ids));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        GeneralUtils.showVolleyError(context, error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());

                params.put("user_id", mUserProfile.getUserId());
                params.put("password", mUserProfile.getUserPassword());

                final JSONArray jsonArray = new JSONArray();

                List<String> extractedList = new ArrayList<>();
                for (String tempStr : ids) extractedList.add(tempStr.trim());

                for (String item : extractedList) if (item != null) jsonArray.put(item);
                params.put("interested_mandi_list", "" + jsonArray);

                Log.d(TAG, "getParams: " + params.toString());
                return params;
            }
        };

        AppController.getInstance().getRequestQueue().cancelAll(stringRequest);
        AppController.getInstance().addToRequestQueue(stringRequest);
        */

        Profile params= new Profile();
        //params.setFarmerId("UKYVLUT5");
        //params.setFarmerContact("1478523691");
        params.setFarmerId(mUserProfile.getUserId());
        params.setFarmerContact(mUserProfile.getUserMobile());

       List<String> extractedList = new ArrayList<>();
        for (String tempStr : ids) extractedList.add(tempStr.trim());

        for (String item : extractedList) if (item != null)
            extractedList.add(item);
        params.setInterestedMandiList(extractedList);
        Log.d(TAG, "getParams: " + params.toString());

        API_Manager apiManager=new API_Manager();
        UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);
        Call<UserUpdate> call=retrofit_interface.updateprofile(mUserProfile.getApiToken(),params);
        call.enqueue(new Callback<UserUpdate>() {
            @Override
            public void onResponse(Call<UserUpdate> call, retrofit2.Response<UserUpdate> response) {
                Log.d("farmer_profile","dataservice response"+response.body());

                mUserProfile.setInterestedMandiIds(TextUtils.join(",", ids));
            }
            @Override
            public void onFailure(Call<UserUpdate> call, Throwable t) {
                //GeneralUtils.showVolleyError(context, error);
                Utils.showLog(t.getMessage());
                Log.d("farmer_profile","dataservice fail"+t.getMessage());
                }
        });
    }


    public interface OnFarmerProfileResult {
        void onSuccess(FarmerProfileModel farmerProfileModel);

        void onResultError();
    }
}
