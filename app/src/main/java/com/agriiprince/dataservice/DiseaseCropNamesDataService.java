package com.agriiprince.dataservice;

import android.text.TextUtils;
import android.util.Log;

import com.agriiprince.db.DatabaseManager;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.db.DatabaseCallback;
import com.agriiprince.db.DiseaseDao;
import com.agriiprince.models.DiseaseCropListModel;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.disease.DistinctDisease;
import com.agriiprince.mvvm.retrofit.service.Disease;
import com.android.volley.NoConnectionError;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Manu Sajjan on 31-12-2018.
 */
public class DiseaseCropNamesDataService implements DatabaseCallback<DiseaseCropListModel> {

    private static final String TAG = "DiseaseCropNamesDataSer";

    private DataResponse<List<DiseaseCropListModel>> cropListDataRespose;

    private DiseaseDao diseaseDao;

    private String primaryLocale;
    private String secondaryLocale;

    @Override
    public void onInsert() {

    }

    @Override
    public void onInsertAll() {

    }

    @Override
    public void onDelete() {

    }

    @Override
    public void onDeleteAll() {

    }

    @Override
    public void onFetch(DiseaseCropListModel results) {

    }

    @Override
    public void onFetchAll(List<DiseaseCropListModel> results) {

    }

    public DiseaseCropNamesDataService(PrefManager prefManager, DatabaseManager databaseManager) {
        diseaseDao = new DiseaseDao(databaseManager);
        primaryLocale = prefManager.getLocale();
        secondaryLocale = prefManager.getSecondaryLocale();
        if (TextUtils.isEmpty(secondaryLocale)) {
            secondaryLocale = primaryLocale.equals("en") ? "hi" : primaryLocale;
        } else {
            secondaryLocale = primaryLocale.equals("en") ? secondaryLocale : primaryLocale;
        }

        primaryLocale = "en";
    }

    public void getCropNames(String api_token, String user_id, String user_type, DataResponse<List<DiseaseCropListModel>> dataResponse) {
        this.cropListDataRespose = dataResponse;

        fetchDiseaseList(api_token, user_id, user_type);
    }

    private void fetchDiseaseList(final String api_token, final String user_id, final String user_type) {
  /*      StringRequest stringRequest = new StringRequest(
                Request.Method.POST, Config.POST_GET_DISEASE_CROP_NAMES_BI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d(TAG, response);
                            JSONArray responseArray = new JSONArray(response);
                            JSONObject responseObject = responseArray.getJSONObject(0);
                            int errorCode = responseObject.getInt("error_code");
                            if (errorCode == 100) {
                                String data = responseObject.getString("data");

                                List<DiseaseCropListModel> list = new Gson().fromJson(data,
                                        new TypeToken<List<DiseaseCropListModel>>() {
                                        }.getType());

                                if (cropListDataRespose != null) {
                                    diseaseDao.insertDiseaseNamesList(list, primaryLocale, secondaryLocale);
                                    cropListDataRespose.onSuccessResponse(list);
                                }

                            } else {
                                if (cropListDataRespose != null)
                                    cropListDataRespose.onParseError();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            if (cropListDataRespose != null)
                                cropListDataRespose.onParseError();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NoConnectionError) {
                            fetchCropNamesFromDb();
                        } else {
                            if (cropListDataRespose != null)
                                cropListDataRespose.onErrorResponse(error);
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", api_token);
                params.put("user_id", user_id);
                params.put("user_type", user_type);


                params.put("primary_language", primaryLocale);
                params.put("secondary_language", secondaryLocale);
                return params;

            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
        */

        API_Manager api_manager=new API_Manager();
        Disease retrofit_interface = api_manager.getClient6().create(Disease.class);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id",user_id);
        params.put("user_type",user_type);
        params.put("primary_language",primaryLocale);
        params.put("secondary_language",secondaryLocale);

        Call<DistinctDisease> call = retrofit_interface.getdistinctdisease(api_token,params);
        call.enqueue(new Callback<DistinctDisease>() {
            @Override
            public void onResponse(Call<DistinctDisease> call, Response<DistinctDisease> response) {
                Log.d("CheckStatus", "onResponse: DistinctDisease  "+response);
                try {
                    if (response.body() != null && response.body().getCode() == 200) {
                        Log.d("CheckStatus", "onResponse: DistinctDisease  code "+response.body().getCode());

                        List<DiseaseCropListModel> list = response.body().getData();
                        if (cropListDataRespose != null) {
                            diseaseDao.insertDiseaseNamesList(list, primaryLocale, secondaryLocale);//
                            cropListDataRespose.onSuccessResponse(list);
                        }

                    } else {
                        if (cropListDataRespose != null)
                            cropListDataRespose.onParseError();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (cropListDataRespose != null)
                        cropListDataRespose.onParseError();
                }
            }

            @Override
            public void onFailure(Call<DistinctDisease> call, Throwable t) {
                Log.d("CheckStatus", "DistinctDisease "+t.getMessage());
                if (t instanceof NoConnectionError) {
                    fetchCropNamesFromDb();
                } else {
                    if (cropListDataRespose != null)
                        cropListDataRespose.onParseError();
                        //cropListDataRespose.onErrorResponse(t);
                }
            }
        });
    }


    private void fetchCropNamesFromDb() {
        if (cropListDataRespose != null)
            cropListDataRespose.onSuccessResponse(diseaseDao.getDiseaseNames(primaryLocale, secondaryLocale));
    }
}
