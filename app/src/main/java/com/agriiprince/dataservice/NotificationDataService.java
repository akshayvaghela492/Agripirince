package com.agriiprince.dataservice;

import android.support.annotation.NonNull;

import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.models.NotificationModel;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.agriiprince.utils.Utils.showLog;

/**
 * Created by Manu Sajjan on 21-12-2018.
 */
public class NotificationDataService {

    private DataResponse<NotificationModel> mCallBack;

    public NotificationDataService(DataResponse<NotificationModel> dataResponse) {
        this.mCallBack = dataResponse;

    }

    public void getNotifications(@NonNull final String user_id, @NonNull final String user_type, @NonNull final String api_token) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_UNREAD_NOTIFICATIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray responseArray = new JSONArray(response);
                            JSONObject responseObject = responseArray.getJSONObject(0);
                            if (responseObject != null) {
                                NotificationModel notificationModel = new Gson().fromJson(responseObject.toString(), NotificationModel.class);
                                if (notificationModel != null) {
                                    mCallBack.onSuccessResponse(notificationModel);
                                } else {
                                    mCallBack.onParseError();
                                }
                            }
                        } catch (Exception e) {
                            mCallBack.onParseError();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mCallBack.onErrorResponse(error);
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                params.put("user_type", user_type);
                params.put("api_token", api_token);

                //todo update this token and delete else where (on server)
                showLog("Logot params " + params.toString());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);

    }
}
