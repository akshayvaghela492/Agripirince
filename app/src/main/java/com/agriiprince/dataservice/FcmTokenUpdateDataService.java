package com.agriiprince.dataservice;

import android.util.Log;

import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.login.FcmTokenUpdate;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.mvvm.util.Logs;

import retrofit2.Call;
import retrofit2.Callback;

public class FcmTokenUpdateDataService {

    API_Manager apiManager=new API_Manager();
    UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);

    public FcmTokenUpdateDataService(final String userId, final String userType, final String apiToken, final String fcmToken) {

        Call<com.agriiprince.mvvm.retrofit.dto.login.FcmTokenUpdate> call = retrofit_interface.FcmTokenUpdate(apiToken, userType.toLowerCase(), userId, fcmToken);
        call.enqueue(new Callback<FcmTokenUpdate>() {
            @Override
            public void onResponse(Call<FcmTokenUpdate> call, retrofit2.Response<FcmTokenUpdate> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code FCM token update ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus", response.body().getMessage());
                } catch (Exception e) {
                    e.getMessage();
                }
            }

            @Override
            public void onFailure(Call<FcmTokenUpdate> call, Throwable t) {
                Log.d("CheckStatus", "FCM token update error " + t.getMessage());
            }
        });
    }
}
