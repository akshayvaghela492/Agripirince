package com.agriiprince.dataservice;

import android.util.Log;

import com.agriiprince.db.DatabaseCallback;
import com.agriiprince.db.InfoTabPricesDao;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.models.HistoricPriceModel;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.checkprice.HistoricalPrices;
import com.agriiprince.mvvm.retrofit.model.checkprice.HistoricalPriceData;
import com.agriiprince.mvvm.retrofit.service.CheckPrice;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.utils.DateUtil;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Manu Sajjan on 21-12-2018.
 *
 * @version 1.2.2
 */
public class FarmerInfoTabPriceDataService implements DatabaseCallback<HistoricPriceModel> {

    private static final String TAG = FarmerInfoTabPriceDataService.class.getSimpleName();

    private final String TOTAL_HISTORIC_PRICE_DAYS = "10";

    private static final int MIN_HOURS_TO_REFRESH = 6;

    private InfoTabPricesDao infoTabPricesDao;
    private OnFarmerInfoTabPriceDataService callBack;

    private UserProfile userProfile;

    public FarmerInfoTabPriceDataService(UserProfile userProfile, InfoTabPricesDao infoTabPricesDao, OnFarmerInfoTabPriceDataService callBack) {
        this.userProfile = userProfile;
        this.infoTabPricesDao = infoTabPricesDao;
        this.callBack = callBack;

        infoTabPricesDao.setCallback(this);
    }

    @Override
    public void onInsert() {

    }

    @Override
    public void onInsertAll() {
        if (callBack != null) callBack.onRefresh();
    }

    @Override
    public void onDelete() {

    }

    @Override
    public void onDeleteAll() {

    }

    @Override
    public void onFetch(HistoricPriceModel results) {
    }

    @Override
    public void onFetchAll(List<HistoricPriceModel> results) {
        if (callBack != null)
            callBack.onGetHistoricPrices(results);
    }

    private boolean isDataLoadedToday() {
        if (userProfile.getInfoTabLastLoadTime() != -1) {
            Calendar current = Calendar.getInstance();
            Calendar last = Calendar.getInstance();

            last.setTimeInMillis(userProfile.getInfoTabLastLoadTime());

            long diff = current.getTimeInMillis() - userProfile.getInfoTabLastLoadTime();
            long milliSecondsPerHour = DateUtil.MILLI_SECONDS_PER_SECOND * DateUtil.SECONDS_PER_MINUTE * DateUtil.MINUTES_PER_HOUR;

            return diff / (milliSecondsPerHour) <= MIN_HOURS_TO_REFRESH;
        }
        return false;
    }

    private void setDataLoadedTime() {
        userProfile.setInfoTabLastLoadTime(Calendar.getInstance().getTimeInMillis());
    }

    private void deleteTableEntries() {
        infoTabPricesDao.deleteAll();
    }

    public void refreshData(List<String> cropList, List<String> mandiList, long timeInMilliSeconds) {
        getPrices(cropList, mandiList, timeInMilliSeconds);
    }


    public void getHistoricPrices(List<String> cropList, List<String> mandiList, long timeInMilliSeconds) {
        if (isDataLoadedToday()) {
            Log.d(TAG, "getHistoricPrices:   Loaded today");
            if (callBack != null)
                callBack.onRefresh();
        } else {
            Log.d(TAG, "getHistoricPrices:  not Loaded today");
            getPrices(cropList, mandiList, timeInMilliSeconds);
        }
    }

    public void getHistoricPricesForCrop(final String crop, final String variety, final String grade, final long timeInMilliSeconds) {
        Log.d(TAG, "getHistoricPricesForCrop:   " + crop);
        infoTabPricesDao.getAll(timeInMilliSeconds, crop, variety, grade);
    }

    private void getPrices(List<String> cropList, List<String> mandiList, long timeInMilliSeconds) {
        Log.d(TAG, "getPrices");
        final String date = HistoricPriceModel.getApiDate(timeInMilliSeconds);
        final String crops = cropList.toString().replace("[", "").replace("]", "");
        final String mandis = mandiList.toString().replace("[", "").replace("]", "");

        Log.d(TAG, "crops   " + crops);
        Log.d(TAG, "mandis   " + mandis);

        API_Manager api_manager = new API_Manager();
        CheckPrice retrofit_interface=api_manager.getClient10().create(CheckPrice.class);
        Call<HistoricalPrices> call=retrofit_interface.HistoricalPriceData(userProfile.getApiToken(),mandis,date,"60",crops);
        call.enqueue(new Callback<HistoricalPrices>() {
            @Override
            public void onResponse(Call<HistoricalPrices> call, Response<HistoricalPrices> response) {
                Logs.d("CheckStatus_hist", "onResponse Historical"+response);
                if (response.body() != null) {
                    Logs.d("CheckStatus_hist", response.body().getStatus().toString());
                    Logs.d("CheckStatus_hist", response.body().getMessage());
                    if (response.body()!=null && response.body().getStatus()== 200)
                    {
                        Logs.d("CheckStatus_hist", "if status=200");
                        List<HistoricalPriceData> historicPriceModels = response.body().getData().getResult();
                        Logs.d("CheckStatus_hist", "hist list "+historicPriceModels);
                        deleteTableEntries();
                        infoTabPricesDao.insertAll(historicPriceModels);
                        setDataLoadedTime();

                    } else {
                        if (callBack != null)
                            callBack.onResultError();
                    }
                }

            }
            @Override
            public void onFailure(Call<HistoricalPrices> call, Throwable t) {
                Logs.d("CheckStatus_hist","Historical error "+t.getMessage());
                callBack.onResultError();
            }
        });

   /*     StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.POST_HISTORICAL_PRICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY ", "historical "+response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            //JSONArray array = new JSONArray(response);
                            //JSONObject object = array.getJSONObject(0);
                            //int error = object.getInt("error_code");
                            int error = jsonObject.getInt("code");
                            //if (error == 100)
                            if (error == 200)
                            {
                                //String data = jsonObject.getString("data");
                                JSONObject dataobj=jsonObject.getJSONObject("data");
                                JSONArray dataArray=dataobj.getJSONArray("data");
                                //Log.d(TAG, data);

                                if (dataobj != null && !dataobj.equals("null")) {
                                    List<HistoricPriceModel> historicPriceModels =
                                            new Gson().fromJson(String.valueOf(dataArray), new TypeToken<List<HistoricPriceModel>>() {
                                            }.getType());

                                    deleteTableEntries();
                                    infoTabPricesDao.insertAll(historicPriceModels);
                                    setDataLoadedTime();

                                } else {
                                    if (callBack != null)
                                        callBack.onResultError();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            if (callBack != null)
                                callBack.onResultError();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "" + error);
                        callBack.onResultError();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", userProfile.getApiToken());
                params.put("crop_list", crops);
                params.put("mandi_list", mandis);
                params.put("date", date);
                params.put("days", TOTAL_HISTORIC_PRICE_DAYS);
                return params;
            }
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",userProfile.getApiToken());
                return header;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(stringRequest);
        */
    }

    public interface OnFarmerInfoTabPriceDataService {

        void onGetHistoricPrices(List<HistoricPriceModel> list);

        void onRefresh();

        void onResultError();
    }
}
