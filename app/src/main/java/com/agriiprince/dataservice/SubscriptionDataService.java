package com.agriiprince.dataservice;

import android.content.Context;
import android.util.Log;

import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.utils.DateUtil;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Manu Sajjan on 24-01-2019.
 */
public class SubscriptionDataService {

    private static final String TAG = SubscriptionDataService.class.getSimpleName();

    private String subscribedOn;

    private UserProfile mUserProfile;
    public SubscriptionDataService(Context context) {
        mUserProfile = new UserProfile(context);

    }

    public void saveSubscriptionStatus() {
        StringRequest request = new StringRequest(Request.Method.POST, Config.POST_SUBSCRIPTION_INFORMATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY", "onResponse: " + response);
                        try {
                            //JSONArray jsonArray = new JSONArray(response);
                            //JSONObject jsonObject = jsonArray.getJSONObject(0);
                            //int error_code = jsonObject.getInt("error_code");
                            //if (error_code == 100)
                            JSONObject jsonObject=new JSONObject(response);
                            int error = jsonObject.getInt("code");
                            if (error == 200)
                            {
                                JSONObject data = jsonObject.getJSONArray("data").getJSONObject(0);

                                subscribedOn = data.getString("subscription_expiry_date");

                                mUserProfile.setValidTill(DateUtil.getTimeForServerDate(subscribedOn));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());

                params.put("user_id", mUserProfile.getUserId());
                params.put("user_type", mUserProfile.getUserType());
                params.put("password", mUserProfile.getUserPassword());
                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUserProfile.getApiToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(request);

    }
}
