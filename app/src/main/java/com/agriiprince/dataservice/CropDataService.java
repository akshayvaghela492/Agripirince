package com.agriiprince.dataservice;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.mvvm.model.crop.Grade;
import com.agriiprince.mvvm.model.crop.Variety;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class CropDataService {

    private static final String TAG = CropDataService.class.getName();

    private Context mContext;

    private OnCropDataRequest mCallback;

    private String primaryLocale;
    private String secondaryLocale;

    private List<Crop> crops;

    private boolean isCheckPrice = false;

    public CropDataService(Context context) {
        this.mContext = context;
        this.crops = new ArrayList<>();

        PrefManager prefManager = new PrefManager(context);
        primaryLocale = prefManager.getLocale();
        secondaryLocale = prefManager.getSecondaryLocale();

        //getCropFromApi();
    }

    public void getCrops(OnCropDataRequest cropDataRequest) {
        this.isCheckPrice = false;
        this.mCallback = cropDataRequest;
        new GetCropsFromJson().execute();
    }

    public void getCheckPriceCrops(OnCropDataRequest cropDataRequest) {
        this.isCheckPrice = true;
        this.mCallback = cropDataRequest;
        new GetCropsFromJson().execute();
    }

    public void getInterestedCrops() {

    }

    private class GetCropsFromJson extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            String data = getCropJson();

            String secondaryCode = null;
            if (primaryLocale.equalsIgnoreCase("en")) {
                if (TextUtils.isEmpty(secondaryLocale)) secondaryCode = "hi";
                else secondaryCode = secondaryLocale;
            } else {
                secondaryCode = primaryLocale;
            }

            if (data != null) {
                try {
                    JSONArray jsonArray = new JSONArray(data);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject cropObject = jsonArray.getJSONObject(i);
                        String crop_id = cropObject.getString("crop_id");
                        String crop_name = cropObject.getString("crop_name_en");
                        String crop_variety = cropObject.getString("crop_variety_en");
                        String crop_grades = cropObject.getString("crop_grade_en");
                        String crop_class = cropObject.getString("crop_class_en");

                        String crop_image_name = cropObject.getString("crop_image_name");

                        String crop_name_bi;
                        String crop_variety_bi;
                        String crop_grades_bi;
                        String crop_class_bi;

                        if (secondaryCode.equalsIgnoreCase("hi")) {
                            crop_name_bi = cropObject.getString("crop_name_hi");
                            crop_variety_bi = cropObject.getString("crop_variety_hi");
                            crop_grades_bi = cropObject.getString("crop_grade_hi");
                            crop_class_bi = cropObject.getString("crop_class_hi");

                        } else if (secondaryCode.equalsIgnoreCase("kn")) {
                            crop_name_bi = cropObject.getString("crop_name_kn");
                            crop_variety_bi = cropObject.getString("crop_variety_kn");
                            crop_grades_bi = cropObject.getString("crop_grade_kn");
                            crop_class_bi = cropObject.getString("crop_class_kn");

                        } else { // default secondary language will be hindi, though
                            crop_name_bi = cropObject.getString("crop_name_hi");
                            crop_variety_bi = cropObject.getString("crop_variety_hi");
                            crop_grades_bi = cropObject.getString("crop_grade_hi");
                            crop_class_bi = cropObject.getString("crop_class_hi");
                        }

                        if (isEmpty(crop_name) && isEmpty(crop_name_bi))
                            continue;

                        Crop crop = Crop.getCropByCropName(crops, crop_name);

                        if (crop != null) {
                            Variety variety = Variety.getVarietyByVarietyName(crop.getVarieties(), crop_variety);

                            if (variety == null && isNotEmpty(crop_variety) && isNotEmpty(crop_variety_bi)) {
                                Variety newVariety = crop.addVariety(crop_variety, crop_variety_bi);
                                newVariety.id = crop_id;

                                String[] grades = crop_grades.split(",");
                                String[] grades_hi = crop_grades_bi.split(",");

                                if (grades.length > 0 && grades_hi.length > 0) {
                                    for (int k = 0; k < grades.length && k < grades_hi.length; k++) {
                                        if (isNotEmpty(grades[k].trim()) && isNotEmpty(grades_hi[k].trim()))
                                            newVariety.addGrade(grades[k].trim(), grades_hi[k].trim());
                                    }
                                }
                            }
                        } else {
                            Crop newCrop = new Crop(crop_name, crop_name_bi);
                            newCrop.cropClass = crop_class;
                            newCrop.cropClass_bi = crop_class_bi;

                            newCrop.cropImageName = crop_image_name;

                            if (isNotEmpty(crop_variety) && isNotEmpty(crop_variety_bi)) {
                                Variety newVariety = newCrop.addVariety(crop_variety, crop_variety_bi);
                                newVariety.id = crop_id;

                                String[] grades = crop_grades.split(",");
                                String[] grades_hi = crop_grades_bi.split(",");

                                if (grades.length > 0 && grades_hi.length > 0) {
                                    for (int k = 0; k < grades.length && k < grades_hi.length; k++) {
                                        if (isNotEmpty(grades[k].trim()) && isNotEmpty(grades_hi[k].trim()))
                                            newVariety.addGrade(grades[k].trim(), grades_hi[k].trim());
                                    }
                                    crops.add(newCrop);
                                }
                            }
                        }
                    }
                    return true;
                } catch (JSONException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) {
                if (isCheckPrice) new SetupCrops().execute();
                else try { mCallback.onCropDataResults(crops); } catch (Exception ex) { ex.printStackTrace(); }
            } else try {  mCallback.onCropDataError(); } catch (Exception ex) { ex.printStackTrace(); }
        }
    }

    private boolean isNotEmpty(String s) {
        return !TextUtils.isEmpty(s);
    }

    private boolean isEmpty(String s) {
        return TextUtils.isEmpty(s);
    }

    private String getCropJson() {
        String json = null;
        try {
            InputStream is = mContext.getAssets().open("crops.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            mCallback.onCropDataError();
            return null;
        }
        return json;

    }

    private class SetupCrops extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {

            if (TextUtils.isEmpty(secondaryLocale)) {
                secondaryLocale = primaryLocale.equals("en") ? "hi" : primaryLocale;
            } else {
                secondaryLocale = primaryLocale.equals("en") ? secondaryLocale : primaryLocale;
            }

            primaryLocale = "en";

            String primaryAll = getAllForSelectedLocale(primaryLocale);
            String secondaryAll = getAllForSelectedLocale(secondaryLocale);

            List<Grade> sets = new ArrayList<>();
            for (Crop crop : crops) {
                sets.clear();
                for (Variety variety : crop.getVarieties()) {
                    variety.addGradeAt(0, primaryAll, secondaryAll);

                    List<Grade> grades = variety.getGrades();
                    if (sets.size() == 0) {
                        sets.addAll(grades);
                    } else {
                        for (Grade grade : grades) {
                            Grade grade1 = Grade.getGradeByGradeName(sets, grade.gradeName);
                            if (grade1 == null) {
                                sets.add(grade);
                            }
                        }
                    }
                }
                Variety variety = crop.addVarietyAt(0, primaryAll, secondaryAll);
                variety.addGrades(new ArrayList<>(sets));
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) mCallback.onCropDataResults(crops);
            else mCallback.onCropDataError();
        }
    }

    private String getAllForSelectedLocale(String locale) {
        switch (locale) {
            case "en":
                return "ALL";
            case "hi":
                return "सब";
            case "kn":
                return "ಎಲ್ಲಾ";
            default:
                return "ALL";
        }
    }

    public interface OnCropDataRequest {
        void onCropDataResults(List<Crop> crops);

        void onCropDataError();
    }

}
