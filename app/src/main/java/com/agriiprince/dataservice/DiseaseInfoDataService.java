package com.agriiprince.dataservice;

import android.text.TextUtils;

import com.agriiprince.db.DatabaseManager;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.db.DiseaseDao;
import com.agriiprince.models.DiseaseInfoModel;

/**
 * Created by Manu Sajjan on 31-12-2018.
 */
public class DiseaseInfoDataService {

    private DataResponse<DiseaseInfoModel> diseaseInfoModelDataResponse;

    private DiseaseDao diseaseDao;

    private String primaryLocale;
    private String secondaryLocale;

    public DiseaseInfoDataService(PrefManager prefManager, DatabaseManager databaseManager) {
        diseaseDao = new DiseaseDao(databaseManager);
        primaryLocale = prefManager.getLocale();
        secondaryLocale = prefManager.getSecondaryLocale();
        if (TextUtils.isEmpty(secondaryLocale)) {
            secondaryLocale = primaryLocale.equals("en") ? "hi" : primaryLocale;
        } else {
            secondaryLocale = primaryLocale.equals("en") ? secondaryLocale : primaryLocale;
        }

        primaryLocale = "en";
    }


    public void getCropDiseaseInfo(String diseaseId, DataResponse<DiseaseInfoModel> dataResponse) {
        this.diseaseInfoModelDataResponse = dataResponse;
        DiseaseInfoModel model = diseaseDao.getDiseaseInfoModel(diseaseId, primaryLocale, secondaryLocale);
        if (model != null) {
            diseaseInfoModelDataResponse.onSuccessResponse(model);
        } else {
            diseaseInfoModelDataResponse.onParseError();
        }
    }
}
