package com.agriiprince.dataservice;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.model.MandiModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class MandiDataService {

    private static String TAG = MandiDataService.class.getSimpleName();

    private Context mContext;

    private List<MandiModel> list;

    private String primaryLocale;
    private String secondaryLocale;

    private OnMandiResponse mCallback;

    public MandiDataService(Context context) {
        this.mContext = context;
        list = new ArrayList<>();

        PrefManager prefManager = new PrefManager(context);
        primaryLocale = prefManager.getLocale();
        secondaryLocale = prefManager.getSecondaryLocale();
    }

    public void getMandis(OnMandiResponse onMandiResponse) {
        Log.d(TAG, "getInterestedMandis: " + 100);
        this.mCallback = onMandiResponse;
        Log.d(TAG, "getInterestedMandis: " + 101);
        new GetMandiFromJSON().execute();
        Log.d(TAG, "getInterestedMandis: " + 102);
    }

    private String getMandisJSONAsset() {
        String json = null;
        try {
            InputStream is = mContext.getAssets().open("mandis.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            mCallback.onMandiFailure();
            return null;
        }
        return json;
    }

    private class GetMandiFromJSON extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            Log.d(TAG, "getInterestedMandis: " + 110);
            String data = getMandisJSONAsset();
            Log.d(TAG, "getInterestedMandis: " + 111 + primaryLocale);

            String secondaryCode;
            if (primaryLocale.equalsIgnoreCase("en")) {
                Log.d(TAG, "getInterestedMandis: " + 112);
                if (TextUtils.isEmpty(secondaryLocale)) secondaryCode = "hi";
                else secondaryCode = secondaryLocale;
            } else {
                secondaryCode = primaryLocale;
            }

            try {
                JSONArray jsonArray = new JSONArray(data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    String mandi_id = object.getString("mandi_id");
                    String mandi_lat = object.getString("mandi_lat");
                    String mandi_lng = object.getString("mandi_lng");
                    //String postal_code = object.getString("postal_code");

                    String mandi_name_en = object.getString("mandi_name_en");
                    String mandi_city_en = object.getString("mandi_city_en");
                    String mandi_district_en = object.getString("mandi_district_en");
                    String mandi_state_en = object.getString("mandi_state_en");
                    String mandi_city_combi_en = object.getString("mandi_city_combi_en");
                    String city_mandi_combi_en = object.getString("city_mandi_combi_en");

                    String mandi_name_bi;
                    String mandi_city_bi;
                    String mandi_district_bi;
                    String mandi_state_bi;
                    String mandi_city_combi_bi;
                    String city_mandi_combi_bi;

                    if (secondaryCode.equalsIgnoreCase("hi")) {
                        mandi_name_bi = object.getString("mandi_name_hi");
                        mandi_city_bi = object.getString("mandi_city_hi");
                        mandi_district_bi = object.getString("mandi_district_hi");
                        mandi_state_bi = object.getString("mandi_state_hi");
                        mandi_city_combi_bi = object.getString("mandi_city_combi_hi");
                        city_mandi_combi_bi = object.getString("city_mandi_combi_hi");

                    } else if (secondaryCode.equalsIgnoreCase("kn")) {
                        mandi_name_bi = object.getString("mandi_name_kn");
                        mandi_city_bi = object.getString("mandi_city_kn");
                        mandi_district_bi = object.getString("mandi_district_kn");
                        mandi_state_bi = object.getString("mandi_state_kn");
                        mandi_city_combi_bi = object.getString("mandi_city_combi_kn");
                        city_mandi_combi_bi = object.getString("city_mandi_combi_kn");

                    } else { // default secondary language will be hindi, though
                        mandi_name_bi = object.getString("mandi_name_hi");
                        mandi_city_bi = object.getString("mandi_city_hi");
                        mandi_district_bi = object.getString("mandi_district_hi");
                        mandi_state_bi = object.getString("mandi_state_hi");
                        mandi_city_combi_bi = object.getString("mandi_city_combi_hi");
                        city_mandi_combi_bi = object.getString("city_mandi_combi_hi");
                    }

                    MandiModel mandiModel = new MandiModel();
                    mandiModel.mandi_id = mandi_id;
                    mandiModel.mandi_lat = mandi_lat;
                    mandiModel.mandi_lng = mandi_lng;
                    //mandiModel.postal_code = postal_code;

                    mandiModel.mandi_name_en = mandi_name_en;
                    mandiModel.mandi_city_en = mandi_city_en;
                    mandiModel.mandi_district_en = mandi_district_en;
                    mandiModel.mandi_state_en = mandi_state_en;
                    mandiModel.mandi_city_combi_en = mandi_city_combi_en;
                    mandiModel.city_mandi_combi_en = city_mandi_combi_en;

                    mandiModel.mandi_name_bi = mandi_name_bi;
                    mandiModel.mandi_city_bi = mandi_city_bi;
                    mandiModel.mandi_district_bi = mandi_district_bi;
                    mandiModel.mandi_state_bi = mandi_state_bi;
                    mandiModel.mandi_city_combi_bi = mandi_city_combi_bi;
                    mandiModel.city_mandi_combi_bi = city_mandi_combi_bi;

                    list.add(mandiModel);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                if (mCallback != null) mCallback.onMandiSuccess(list);
            } else {
                if (mCallback != null) mCallback.onMandiFailure();
            }
        }
    }


    public interface OnMandiResponse {

        void onMandiSuccess(List<MandiModel> mandies);

        void onMandiFailure();
    }
}
