package com.agriiprince.dataservice;

import android.content.Context;

import com.agriiprince.model.PlaceModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class PlaceDataService {

    private final String TAG = PlaceDataService.class.getSimpleName();

    private DataResponse<List<PlaceModel>> mListener;

    public PlaceDataService(Context context, DataResponse<List<PlaceModel>> dataResponse) {
        this.mListener = dataResponse;

        String jsonString = getCitiesJson(context);
        List<PlaceModel> placeModels = new Gson().fromJson(jsonString, new TypeToken<List<PlaceModel>>() {
        }.getType());
        if (mListener != null) {
            if (placeModels != null) {
                mListener.onSuccessResponse(placeModels);
            } else {
                mListener.onParseError();
            }
        }
    }


    private String getCitiesJson(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("cities.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
