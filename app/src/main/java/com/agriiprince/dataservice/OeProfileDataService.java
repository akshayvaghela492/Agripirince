package com.agriiprince.dataservice;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.util.Logs;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OeProfileDataService {

    private static final String TAG = OeProfileDataService.class.getSimpleName();

    private Context context;

    public UserProfile userProfile;

    public OeProfileDataService(Context context) {
        this.context = context;
        this.userProfile = new UserProfile(context);
    }


    public void updateInterestedMandiIds(final List<String> ids) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_OE_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Logs.d("CheckStatus_oe", "onResponse: update profile Success"+response);
                        try {
                            //JSONArray responseArray = new JSONArray(response);
                            //JSONObject responseObject = responseArray.getJSONObject(0);
                            //int errorCode = responseObject.getInt("error_code");
                            //if (errorCode == 100)
                            JSONObject jsonObject=new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            if(code==200)
                            {Logs.d("CheckStatus_oe", "onResponse: update profile code "+code);
                                userProfile.setInterestedMandiIds(TextUtils.join(",", ids));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logs.d("CheckStatus_oe", "update profile fail "+error.getMessage());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("api_token", userProfile.getApiToken());

                //Mandatory
                params.put("user_id", userProfile.getUserId());
                params.put("password", userProfile.getUserPassword());

                final JSONArray cropArray = new JSONArray();
                for (String item : ids) {
                    if (item != null) cropArray.put(item.trim());
                }

                params.put("interested_mandi_list", "" + cropArray);

                Log.d(TAG, "getParams: " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",userProfile.getApiToken());
                return header;
            }
        };


        AppController.getInstance().getRequestQueue().cancelAll(stringRequest);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
