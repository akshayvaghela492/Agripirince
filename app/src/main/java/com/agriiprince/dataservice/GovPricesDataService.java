package com.agriiprince.dataservice;

import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;

import com.agriiprince.activities.checkprice.SingleEvenLiveData;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.checkprice.GovReportPrices;
import com.agriiprince.mvvm.retrofit.model.checkprice.GovReportedPriceData;
import com.agriiprince.mvvm.retrofit.model.checkprice.GovRequestBody;
import com.agriiprince.mvvm.retrofit.service.CheckPrice;
import com.agriiprince.mvvm.util.Logs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Manu Sajjan on 19-12-2018.
 */
public class GovPricesDataService {

    public static final String TAG = GovPricesDataService.class.getSimpleName();

    private MutableLiveData<List<GovReportedPriceData>> mList;

    private MutableLiveData<Boolean> dataAvailable;

    private MutableLiveData<Boolean> isLoading;

    public GovPricesDataService() {
        this.mList = new MutableLiveData<>();
        this.dataAvailable = new SingleEvenLiveData<>();
        this.isLoading = new MutableLiveData<>();
    }

    public MutableLiveData<Boolean> isLoading() {
        return isLoading;
    }

    public MutableLiveData<Boolean> isDataAvailable() {
        return dataAvailable;
    }

    public MutableLiveData<List<GovReportedPriceData>> getGovPrices() {
        return mList;
    }

    public void getPrices(final String api_toke, final String user_id, final String date,
                          final String cropName, final String varietyName, final String gradeName) {

        isLoading.setValue(true);

      /*  StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_GOV_REPORTED_PRICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        isLoading.setValue(false);
                        Log.d("TESTING_VOLLEY", "gov "+response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            //JSONArray array = new JSONArray(response);
                            //JSONObject object = array.getJSONObject(0);
                            //int error = object.getInt("error_code");
                            int error = jsonObject.getInt("code");
                            //if (error == 100) {
                            if (error == 200) {
                                //String data = object.getString("totaldata");
                                JSONObject dataobj=jsonObject.getJSONObject("data");
                                JSONArray dataArray=dataobj.getJSONArray("data");
                                List<GovReportedPrice> mGovReports = new Gson().fromJson(String.valueOf(dataArray),
                                        new TypeToken<ArrayList<GovReportedPrice>>() {
                                        }.getType());

                                if (mGovReports != null && mGovReports.size() > 0) {
                                    dataAvailable.setValue(true);
                                    new FixMandiLocationForMultiple(mGovReports).execute();
                                } else {
                                    dataAvailable.setValue(false);
                                }

                            } else {
                                dataAvailable.setValue(false);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            dataAvailable.setValue(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dataAvailable.setValue(false);
                        isLoading.setValue(false);
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", api_toke);
                params.put("user_id", user_id);

                params.put("crop_name", cropName);
                params.put("crop_variety", varietyName);
                params.put("crop_grade", gradeName);

                params.put("date", date);

                Log.d(TAG, params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",api_toke);
                return header;
            }
        };

        AppController.getInstance().getRequestQueue().cancelAll(stringRequest);
        AppController.getInstance().addToRequestQueue(stringRequest); */


      // 19/9/2019 NodeJs Api not responding expected so being replaced by php Api for now
        API_Manager api_manager = new API_Manager();
        CheckPrice retrofit_interface=api_manager.phpClient().create(CheckPrice.class);

       /* List<String> cropGrade=new ArrayList<>();
        cropGrade.add(gradeName);
        List<String> cropVariety=new ArrayList<>();
        cropVariety.add(varietyName);
        List<String> cropname=new ArrayList<>();
        cropname.add(cropName);
        final GovRequestBody requestBody = new GovRequestBody();
        requestBody.setLimit(20);
        requestBody.setOffset(0);
        requestBody.setDate(date);
        requestBody.setUserId(user_id);
        requestBody.setCropGrade(cropGrade);
        requestBody.setCropVariety(cropVariety);
        requestBody.setCropName(cropname);
        */
        HashMap<String, String> params = new HashMap<>();
        params.put("api_token", api_toke);
        params.put("user_id", user_id);

        params.put("crop_name", cropName);
        params.put("crop_variety", varietyName);
        params.put("crop_grade", gradeName);

        params.put("date", date);

        Call<GovReportPrices> call=retrofit_interface.GovReportedPriceData(api_toke,params);
        call.enqueue(new Callback<GovReportPrices>() {

            @Override
            public void onResponse(Call<GovReportPrices> call, Response<GovReportPrices> response) {
                Logs.d("CheckStatus_gov", "onResponse: Gov" + response);
                Logs.d("CheckStatus_gov", "onResponse: Gov code " + response.body().getStatus());
                isLoading.setValue(false);
                try {
                    if (response.body() != null && response.body().getCode() == 200)
                    {
                        List<GovReportedPriceData> mGovReports = response.body().getData().getResult();
                        Logs.d("CheckStatus_gov", "onResponse: Gov list " + mGovReports);
                        if (mGovReports != null && mGovReports.size() > 0) {
                            dataAvailable.setValue(true);
                            new FixMandiLocationForMultiple(mGovReports).execute();
                        } else {
                            dataAvailable.setValue(false);
                        }
                    }

            }catch (Exception e)
                {
                    e.printStackTrace();
                    Logs.d("CheckStatus_gov", "exception : Gov" + e.getMessage());
                }

            }
            @Override
            public void onFailure(Call<GovReportPrices> call, Throwable t) {
                Logs.d("CheckStatus_gov","Gov error message  "+t.getMessage());
                dataAvailable.setValue(false);
                isLoading.setValue(false);

            }
        });

    }


    private class FixMandiLocationForMultiple extends AsyncTask<Void, Void, Void> {

        private List<GovReportedPriceData> reportedPrices;

        public FixMandiLocationForMultiple(List<GovReportedPriceData> reportedPrices) {
            this.reportedPrices = reportedPrices;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            int count;
            for (int i = 0; i < reportedPrices.size(); i++) {
                GovReportedPriceData m1 = reportedPrices.get(i);
                count = 0;
                double offset = 0.00005;
                for (int j = i + 1; j < reportedPrices.size(); j++) {
                    GovReportedPriceData m2 = reportedPrices.get(j);
                    double lat = Double.parseDouble(m2.getMandiLat());
                    double lng = Double.parseDouble(m2.getMandiLng());
                    if (m1.getMandiLat() == String.valueOf(lat) && m1.getMandiLng() == String.valueOf(lng)) {
                        switch (count) {
                            case 0:
                                m2.setMandiLat(String.valueOf(lat + offset));
                                m2.setMandiLng(String.valueOf(lng + offset));
                                break;
                            case 1:
                                m2.setMandiLat(String.valueOf(lat + offset));
                                m2.setMandiLng(String.valueOf(lng - offset));
                                break;
                            case 2:
                                m2.setMandiLat(String.valueOf(lat - offset));
                                m2.setMandiLng(String.valueOf(lng - offset));
                                break;
                            case 3:
                                m2.setMandiLat(String.valueOf(lat - offset));
                                m2.setMandiLng(String.valueOf(lng + offset));
                                break;
                        }
                        count++;
                        if (count == 4) {
                            count = 0;
                            offset = offset + 0.00005;
                        }
                    }
                }
            }
            mList.postValue(reportedPrices);
            return null;
        }
    }
}
