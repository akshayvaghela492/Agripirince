package com.agriiprince.dataservice;

import com.android.volley.VolleyError;

public interface DataResponse<T> {

    void onSuccessResponse(T response);

    void onParseError();

    void onErrorResponse(VolleyError error);
}
