package com.agriiprince.db;

/**
 * Created by Manu Sajjan on 22-12-2018.
 */
//  TODO - replace by room
public class InfoTabPriceTable {

    public static final String TABLE_NAME = "price_historic";

    public static final String COLUMN_ARRIVAL = "arrival";
    public static final String COLUMN_PID = "id";
    public static final String COLUMN_MARKET = "market";
    public static final String COLUMN_COMMODITY = "commodity";
    public static final String COLUMN_VARIETY = "variety";
    public static final String COLUMN_GRADE = "grade";
    public static final String COLUMN_MINP = "minP";
    public static final String COLUMN_MAXP = "maxP";
    public static final String COLUMN_MODP = "modP";

    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
            COLUMN_PID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLUMN_ARRIVAL + " TEXT," +
            COLUMN_MARKET + " TEXT," +
            COLUMN_COMMODITY + " TEXT," +
            COLUMN_GRADE + " TEXT," +
            COLUMN_MINP + " TEXT," +
            COLUMN_MAXP + " TEXT," +
            COLUMN_MODP + " TEXT," +
            COLUMN_VARIETY + " TEXT" +
            ");";
}
