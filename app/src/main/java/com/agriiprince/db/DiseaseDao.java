package com.agriiprince.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.agriiprince.models.DiseaseCropListModel;
import com.agriiprince.models.DiseaseInfoModel;
import com.agriiprince.models.DiseaseListModel;
import com.agriiprince.mvvm.retrofit.model.disease.DiseaseDetailModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manu Sajjan on 15-12-2018.
 */
//  TODO - replace by room
public class DiseaseDao {

    private final String TAG = DiseaseDao.class.getSimpleName();

    private SQLiteDatabase db;

    private Handler handler;

    public DiseaseDao(DatabaseManager databaseManager) {
        this.db = databaseManager.getWritableDatabase();

        handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                if (msg.obj == DatabaseCallback.TYPE.ON_INSERT) {

                } else if (msg.obj == DatabaseCallback.TYPE.ON_INSERT_ALL) {

                } else if (msg.obj == DatabaseCallback.TYPE.ON_FETCH) {

                } else if (msg.obj == DatabaseCallback.TYPE.ON_FETCH_ALL) {

                } else if (msg.obj == DatabaseCallback.TYPE.ON_DELETE) {

                } else if (msg.obj == DatabaseCallback.TYPE.ON_DELETE_ALL) {

                }
            }
        };
    }

    public void insertDiseaseNamesList(final List<DiseaseCropListModel> list,
                                       final String primaryLanguage, final String secondaryLanguage) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                db.delete(DiseaseDb.TABLE_DISEASE_NAMES, null, null);
                for (DiseaseCropListModel model : list) {
                    insertDiseaseName(model, primaryLanguage, secondaryLanguage);
                }
            }
        }).start();
    }

    private void insertDiseaseName(DiseaseCropListModel model, String primaryLanguage, String secondaryLanguage) {
        insertDiseaseName(model.getDiseaseId(), model.getCrop_name(), primaryLanguage);
        insertDiseaseName(model.getDiseaseId(), model.getCrop_name_bi(), secondaryLanguage);
    }

    private synchronized void insertDiseaseName(String id, String cropName, String languageCode) {
        Cursor cursorOld = db.query(DiseaseDb.TABLE_DISEASE_NAMES, new String[] {DiseaseDb.id},
                DiseaseDb.id + "=?" + " AND " + DiseaseDb.language + "=?",
                new String[] {id, languageCode}, null, null, null);
        if (cursorOld.getCount() == 0) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DiseaseDb.id, id);
            contentValues.put(DiseaseDb.crop_name, cropName);
            contentValues.put(DiseaseDb.language, languageCode);

            db.insert(DiseaseDb.TABLE_DISEASE_NAMES, null, contentValues);
        }

        cursorOld.close();
    }

    public List<DiseaseCropListModel> getDiseaseNames(String primaryLocale, String secondaryLocale) {
        List<DiseaseCropListModel> names = new ArrayList<>();

        String query = "SELECT " + "a." + DiseaseDb.id + ", " + "a." + DiseaseDb.crop_name + ", " +
                "b." + DiseaseDb.crop_name + " AS " + DiseaseDb.crop_name + "_bi" +
                " FROM " + DiseaseDb.TABLE_DISEASE_NAMES + " a" + " JOIN " + DiseaseDb.TABLE_DISEASE_NAMES + " b" +
                " ON " + "a." + DiseaseDb.id + " = " + "b." + DiseaseDb.id +
                " WHERE " + "a." + DiseaseDb.language + " =?" + " AND " + "b." + DiseaseDb.language + " =?";

        Cursor cursor = db.rawQuery(query, new String[]{primaryLocale, secondaryLocale});

        if (cursor.moveToFirst()) {
            do {
                DiseaseCropListModel model = new DiseaseCropListModel();
                model.setDiseaseId(cursor.getString(cursor.getColumnIndex(DiseaseDb.id)));
                model.setCrop_name(cursor.getString(cursor.getColumnIndex(DiseaseDb.crop_name)));
                model.setCrop_name_bi(cursor.getString(cursor.getColumnIndex(DiseaseDb.crop_name + "_bi")));

                names.add(model);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return names;
    }

    public void requestForDownloadLater(String cropName, String language) {
        ContentValues values = new ContentValues();
        values.put(DiseaseDb.download_status, DiseaseDb.DownloadStatus.REQUESTED);

        db.update(DiseaseDb.TABLE_DISEASE_NAMES, values,
                DiseaseDb.crop_name + "=?" + " AND " + DiseaseDb.language + "=?",
                new String[] {cropName, language});

    }


    public void updateDownloadStatus(String cropName, String language) {

        ContentValues values = new ContentValues();
        values.put(DiseaseDb.download_status, DiseaseDb.DownloadStatus.COMPLETE);

        db.update(DiseaseDb.TABLE_DISEASE_NAMES, values,
                DiseaseDb.crop_name + "=?" + " AND " + DiseaseDb.language + "=?",
                new String[] {cropName, language});

    }

    public List<String> getDownloadRequests() {
        Cursor cursor = db.query(DiseaseDb.TABLE_DISEASE_NAMES, new String[] {DiseaseDb.crop_name},
                DiseaseDb.download_status + "=?" + " AND " + DiseaseDb.language + "=?",
                new String[] {String.valueOf(DiseaseDb.DownloadStatus.REQUESTED), "en"},
                null, null, null);

        List<String> names = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                names.add(cursor.getString(cursor.getColumnIndex(DiseaseDb.crop_name)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return names;
    }


    public void insertDiseaseInfoList(List<DiseaseDetailModel> primaryList, String primaryLocale,
                                      List<DiseaseDetailModel> secondaryList, String secondaryLocale,
                                      DatabaseCallback<DiseaseListModel> callback) {

        new InsertDiseaseInfoLists(primaryList, primaryLocale, secondaryList, secondaryLocale, callback).execute();

    }

    private class InsertDiseaseInfoLists extends AsyncTask<Void, Void, Void> {

        private List<DiseaseDetailModel> primaryList;
        private List<DiseaseDetailModel> secondaryList;

        private String primaryLocale;
        private String secondaryLocale;

        private DatabaseCallback<DiseaseListModel> callback;

        private InsertDiseaseInfoLists(List<DiseaseDetailModel> primaryList, String primaryLocale,
                                       List<DiseaseDetailModel> secondaryList, String secondaryLocale,
                                       DatabaseCallback<DiseaseListModel> callback) {

            this.primaryList = primaryList;
            this.secondaryList = secondaryList;
            this.primaryLocale = primaryLocale;
            this.secondaryLocale = secondaryLocale;
            this.callback = callback;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            insertDiseaseInfoList(primaryList, primaryLocale);
            insertDiseaseInfoList(secondaryList, secondaryLocale);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            callback.onInsertAll();
        }
    }

    private synchronized void insertDiseaseInfoList(List<DiseaseDetailModel> list, String language) {
        Log.d(TAG, "insert List<DiseaseInfoModel> list" + list);
        for (DiseaseDetailModel model : list) {
            Log.d(TAG, "insert List<DiseaseInfoModel> list" + model + " " + model.getId());

            ContentValues values = getContentValues(model, language);

            Cursor cursor = db.query(DiseaseDb.TABLE_DISEASE_INFO, new String[] {DiseaseDb.id},
                    DiseaseDb.id + " =?" + " AND " + DiseaseDb.language + " =?",
                    new String[]{model.getId().toString(), language}, null, null, null, "1");

            if (cursor.getCount() == 0) {
                db.insert(DiseaseDb.TABLE_DISEASE_INFO, null, values);
            }
            cursor.close();
        }
    }

    private ContentValues getContentValues(DiseaseDetailModel model, String language) {
        ContentValues values = new ContentValues();

        values.put(DiseaseDb.id, model.getId());
        values.put(DiseaseDb.crop_name, model.getCropName());
        values.put(DiseaseDb.disease_name, model.getCropDiseases());
        values.put(DiseaseDb.scientific_name, model.getCropDiseasesScientificName());
        values.put(DiseaseDb.category, model.getCropDiseasesCategory());
        values.put(DiseaseDb.cause, model.getCropCauses());
        values.put(DiseaseDb.management, model.getCropDiseasesManagement());
        values.put(DiseaseDb.symptoms, model.getCropSymptom());
        values.put(DiseaseDb.comments, model.getComments());
        values.put(DiseaseDb.crop_images, model.getCropImages());
        values.put(DiseaseDb.language, language);

        return values;
    }

    public List<DiseaseListModel> getDiseaseListModelList(String cropName, String primaryLocale, String secondaryLocale) {
        Log.d(TAG, "getDiseaseListModelList: " + cropName);

        String query = "SELECT " + "a." + DiseaseDb.id + ", " + "a." + DiseaseDb.disease_name + ", " +
                "a." + DiseaseDb.crop_images + ", " + "a." + DiseaseDb.symptoms + ", " +
                "b." + DiseaseDb.disease_name + " AS " + DiseaseDb.disease_name + "_bi" + ", " +
                "b." + DiseaseDb.symptoms + " AS " + DiseaseDb.symptoms + "_bi" +
                " FROM " + DiseaseDb.TABLE_DISEASE_INFO + " a" + " JOIN " + DiseaseDb.TABLE_DISEASE_INFO + " b" +
                " ON " + "a." + DiseaseDb.id + " = " + "b." + DiseaseDb.id +
                " WHERE " + "a." + DiseaseDb.crop_name + " =?" + " AND " +
                "a." + DiseaseDb.language + " =?" + " AND " + "b." + DiseaseDb.language + " =?";


        Cursor cursor = db.rawQuery(query, new String[]{cropName, primaryLocale, secondaryLocale});
        List<DiseaseListModel> list = new ArrayList<>();
        Log.d(TAG, "cursor count : " + cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                DiseaseListModel model = getDiseaseListModelFromCursor(cursor);
//                Log.d(TAG, "disease name : " + model.getDisease_name() + "  " + model.getDisease_name_bi());
                list.add(model);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return list;
    }

    private DiseaseListModel getDiseaseListModelFromCursor(Cursor cursor) {
        DiseaseListModel model = new DiseaseListModel();
        model.setDisease_id(cursor.getString(cursor.getColumnIndex(DiseaseDb.id)));
        model.setDisease_name(cursor.getString(cursor.getColumnIndex(DiseaseDb.disease_name)));
        model.setDisease_symptoms(cursor.getString(cursor.getColumnIndex(DiseaseDb.symptoms)));
        model.setDisease_name_bi(cursor.getString(cursor.getColumnIndex(DiseaseDb.disease_name + "_bi")));
        model.setDisease_symptoms_bi(cursor.getString(cursor.getColumnIndex(DiseaseDb.symptoms + "_bi")));
        model.setCrop_images(cursor.getString(cursor.getColumnIndex(DiseaseDb.crop_images)));
        return model;
    }


    public DiseaseInfoModel getDiseaseInfoModel(String diseaseId, String primaryLocale, String secondaryLocale) {

        String query_en = "SELECT * FROM " + DiseaseDb.TABLE_DISEASE_INFO + " WHERE " + DiseaseDb.id + " =?" +
                " AND " + DiseaseDb.language + " =?";

        String query_hi = "SELECT * FROM " + DiseaseDb.TABLE_DISEASE_INFO + " WHERE " + DiseaseDb.id + " =?" +
                " AND " + DiseaseDb.language + " =?";

        Cursor cursor = db.rawQuery(query_en, new String[]{diseaseId, primaryLocale});
        Log.d(TAG, "cursor count : " + cursor.getCount());

        DiseaseInfoModel model = null;

        if (cursor.moveToFirst()) {
            String id = cursor.getString(cursor.getColumnIndex(DiseaseDb.id));
            Cursor cursor1 = db.rawQuery(query_hi, new String[]{id, secondaryLocale});

            if (cursor1.moveToFirst()) {
                model = getDiseaseInfoModelFromCursor(cursor, cursor1);
//                Log.d(TAG, "disease name : " + model.getCrop_diseases() + "  " + model.getCrop_diseases_hindi());
            }
            cursor1.close();
        }

        cursor.close();

        return model;
    }

    private DiseaseInfoModel getDiseaseInfoModelFromCursor(Cursor cursor, Cursor cursor1) {
        DiseaseInfoModel model = new DiseaseInfoModel();
        model.setId(cursor.getString(cursor.getColumnIndex(DiseaseDb.id)));
        model.setCropName(cursor.getString(cursor.getColumnIndex(DiseaseDb.crop_name)));
        model.setDiseaseName(cursor.getString(cursor.getColumnIndex(DiseaseDb.disease_name)));
        model.setScientificName(cursor.getString(cursor.getColumnIndex(DiseaseDb.scientific_name)));
        model.setCategory(cursor.getString(cursor.getColumnIndex(DiseaseDb.category)));
        model.setCause(cursor.getString(cursor.getColumnIndex(DiseaseDb.cause)));
        model.setManagement(cursor.getString(cursor.getColumnIndex(DiseaseDb.management)));
        model.setSymptom(cursor.getString(cursor.getColumnIndex(DiseaseDb.symptoms)));
        model.setComments(cursor.getString(cursor.getColumnIndex(DiseaseDb.comments)));
        model.setImages(cursor.getString(cursor.getColumnIndex(DiseaseDb.crop_images)));


        model.setCropNameBi(cursor1.getString(cursor1.getColumnIndex(DiseaseDb.crop_name)));
        model.setDiseaseNameBi(cursor1.getString(cursor1.getColumnIndex(DiseaseDb.disease_name)));
        model.setScientificNameBi(cursor1.getString(cursor1.getColumnIndex(DiseaseDb.scientific_name)));
        model.setCategoryBi(cursor1.getString(cursor1.getColumnIndex(DiseaseDb.category)));
        model.setCauseBi(cursor1.getString(cursor1.getColumnIndex(DiseaseDb.cause)));
        model.setManagementBi(cursor1.getString(cursor1.getColumnIndex(DiseaseDb.management)));
        model.setSymptomBi(cursor1.getString(cursor1.getColumnIndex(DiseaseDb.symptoms)));
        model.setCommentsBi(cursor1.getString(cursor1.getColumnIndex(DiseaseDb.comments)));

        return model;
    }
}
