package com.agriiprince.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.agriiprince.models.HistoricPriceModel;
import com.agriiprince.mvvm.retrofit.model.checkprice.HistoricalPriceData;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Manu Sajjan on 22-12-2018.
 */
//  TODO - replace by room
public class InfoTabPricesDao {

    private static final String TAG = InfoTabPricesDao.class.getSimpleName();

    private final String DATA = "data";

    private DatabaseCallback<HistoricPriceModel> callback;

    private SQLiteDatabase db;

    private Handler uiHandler;

    private ExecutorService executorService;

    public InfoTabPricesDao(DatabaseManager databaseManager) {
        this.db = databaseManager.getWritableDatabase();
        executorService = Executors.newSingleThreadExecutor();
        uiHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                if (msg.obj == DatabaseCallback.TYPE.ON_INSERT) {

                } else if (msg.obj == DatabaseCallback.TYPE.ON_INSERT_ALL) {

                } else if (msg.obj == DatabaseCallback.TYPE.ON_FETCH) {

                } else if (msg.obj == DatabaseCallback.TYPE.ON_FETCH_ALL) {
                    List<HistoricPriceModel> historicPriceModels = msg.getData().getParcelableArrayList(DATA);
                    if (callback != null) callback.onFetchAll(historicPriceModels);
                } else if (msg.obj == DatabaseCallback.TYPE.ON_DELETE) {

                } else if (msg.obj == DatabaseCallback.TYPE.ON_DELETE_ALL) {

                }
            }
        };
    }

    public void setCallback(DatabaseCallback<HistoricPriceModel> callback) {
        this.callback = callback;
    }

    public void insert(HistoricalPriceData historicPriceModel) {
        ContentValues contentValues = getContentValues(historicPriceModel);
        db.insert(InfoTabPriceTable.TABLE_NAME, null, contentValues);

        if (callback != null) callback.onInsert();
    }

    public void insertAll(List<HistoricalPriceData> historicPriceModels) {
        new InsertAll(historicPriceModels).execute();
    }

    private class InsertAll extends AsyncTask<Void, Void, Void> {
        private List<HistoricalPriceData> historicPriceModels;

        private InsertAll(List<HistoricalPriceData> historicPriceModels) {
            this.historicPriceModels = historicPriceModels;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Log.d(TAG, "insertAll: " + historicPriceModels.size());
            for (HistoricalPriceData model : historicPriceModels) {
                ContentValues contentValues = getContentValues(model);
                db.insert(InfoTabPriceTable.TABLE_NAME, null, contentValues);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (callback != null) callback.onInsertAll();
        }
    }

    public synchronized void deleteAll() {
        String query = "DELETE FROM " + InfoTabPriceTable.TABLE_NAME;
        db.execSQL(query);
        if (callback != null) callback.onDeleteAll();
    }

    public synchronized void getAll(final long timeInMillis, final String cropName, final String varietyName, final String gradeName) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                String date = HistoricPriceModel.getApiDate(timeInMillis);
                String query = "SELECT *" +
                        " FROM " + InfoTabPriceTable.TABLE_NAME +
                        " WHERE " + InfoTabPriceTable.COLUMN_ARRIVAL + " =?" +
                        " AND " + InfoTabPriceTable.COLUMN_COMMODITY + " =?" +
                        " AND " + InfoTabPriceTable.COLUMN_VARIETY + " =?" +
                        " AND " + InfoTabPriceTable.COLUMN_GRADE + " =?";

                Cursor cursor = db.rawQuery(query, new String[]{date, cropName, varietyName, gradeName});
                Log.d(TAG, "cursor count : " + cursor.getCount() + " date: " + date + " crop: " + cropName + " variety: " + varietyName + " grade: " + gradeName);

                ArrayList<HistoricPriceModel> historicPriceModels = new ArrayList<>();
                if (cursor.moveToFirst()) {
                    do {
                        historicPriceModels.add(getHistoricPriceModel(cursor));
                    } while (cursor.moveToNext());
                }
                cursor.close();

                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(DATA, historicPriceModels);

                Message message = Message.obtain();
                message.obj = DatabaseCallback.TYPE.ON_FETCH_ALL;
                message.setData(bundle);
                uiHandler.sendMessage(message);
            }
        };
        executorService.submit(runnable);
    }

    private HistoricPriceModel getHistoricPriceModel(Cursor cursor) {
        HistoricPriceModel historicPriceModel = new HistoricPriceModel();

        historicPriceModel.setDate(cursor.getString(cursor.getColumnIndex(InfoTabPriceTable.COLUMN_ARRIVAL)));
        historicPriceModel.setMarket(cursor.getString(cursor.getColumnIndex(InfoTabPriceTable.COLUMN_MARKET)));
        historicPriceModel.setCrop(cursor.getString(cursor.getColumnIndex(InfoTabPriceTable.COLUMN_COMMODITY)));
        historicPriceModel.setVariety(cursor.getString(cursor.getColumnIndex(InfoTabPriceTable.COLUMN_VARIETY)));
        historicPriceModel.setGrade(cursor.getString(cursor.getColumnIndex(InfoTabPriceTable.COLUMN_GRADE)));
        historicPriceModel.setMinPrice(cursor.getString(cursor.getColumnIndex(InfoTabPriceTable.COLUMN_MINP)));
        historicPriceModel.setMaxPrice(cursor.getString(cursor.getColumnIndex(InfoTabPriceTable.COLUMN_MAXP)));
        historicPriceModel.setModalPrice(cursor.getString(cursor.getColumnIndex(InfoTabPriceTable.COLUMN_MODP)));

        return historicPriceModel;
    }

    private ContentValues getContentValues(HistoricalPriceData historicPriceModel) {
        ContentValues contentValues = new ContentValues();
        //contentValues.put(InfoTabPriceTable.COLUMN_ARRIVAL, historicPriceModel.getDate());
        contentValues.put(InfoTabPriceTable.COLUMN_ARRIVAL, historicPriceModel.getArrival());
        contentValues.put(InfoTabPriceTable.COLUMN_MARKET, historicPriceModel.getMarket());
        //contentValues.put(InfoTabPriceTable.COLUMN_COMMODITY, historicPriceModel.getCrop());
        contentValues.put(InfoTabPriceTable.COLUMN_COMMODITY, historicPriceModel.getCommodity());
        contentValues.put(InfoTabPriceTable.COLUMN_VARIETY, historicPriceModel.getVariety());
        contentValues.put(InfoTabPriceTable.COLUMN_GRADE, historicPriceModel.getGrade());
        //contentValues.put(InfoTabPriceTable.COLUMN_MINP, historicPriceModel.getMinPricePerKg());
        contentValues.put(InfoTabPriceTable.COLUMN_MINP, historicPriceModel.getMinP());
        //contentValues.put(InfoTabPriceTable.COLUMN_MAXP, historicPriceModel.getMaxPricePerKg());
        contentValues.put(InfoTabPriceTable.COLUMN_MAXP, historicPriceModel.getMaxP());
        //contentValues.put(InfoTabPriceTable.COLUMN_MODP, historicPriceModel.getModalPricePerKg());
        contentValues.put(InfoTabPriceTable.COLUMN_MODP, historicPriceModel.getModP());
        return contentValues;
    }
}
