package com.agriiprince.db;

import java.util.List;

/**
 * Created by Manu Sajjan on 24-12-2018.
 */
//  TODO - replace by room
public interface DatabaseCallback<T> {

    enum TYPE {
        ON_INSERT,
        ON_INSERT_ALL,
        ON_DELETE,
        ON_DELETE_ALL,
        ON_FETCH,
        ON_FETCH_ALL
    }

    void onInsert();

    void onInsertAll();

    void onDelete();

    void onDeleteAll();

    void onFetch(T results);

    void onFetchAll(List<T> results);
}
