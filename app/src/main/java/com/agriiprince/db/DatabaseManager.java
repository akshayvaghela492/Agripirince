package com.agriiprince.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.agriiprince.db.DiseaseDb;
import com.agriiprince.db.InfoTabPriceTable;

/**
 * Created by dtrah on 12/1/2018.
 */

public class DatabaseManager extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseManager";

    /*
     * This time we will not be using the hardcoded string values
     * Instead here we are defining all the Strings that is required for our database
     * for example databasename, table name and column names.
     * */
    private static final String DATABASE_NAME = "ap";
    private static final int DATABASE_VERSION = 4;

    private static final String CROP_TABLE = "cropvariety";

    private static final String COLUMN_CID = "id";
    private static final String COLUMN_CROPID = "crop_id";
    private static final String COLUMN_CROPVARIETY = "crop_variety";
    private static final String COLUMN_CROPNAME = "crop_name";

    /*
     * We need to call the super i.e. parent class constructur
     * And we need to pass 4 parameters
     * 1. Context context -> It is the context object we will get it from the activity while creating the instance
     * 2. String databasename -> It is the name of the database and here we are passing the constant that we already defined
     * 3. CursorFactory cursorFactory -> If we want a cursor to be initialized on the creation we can use cursor factory, it is optionall and that is why we passed null here
     * 4. int version -> It is an int defining our database version
     * */
    public DatabaseManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        /*
         * The query to create our table
         * It is same as we had in the previous post
         * The only difference here is we have changed the
         * hardcoded string values with String Variables
         * */

        String crop_sql = "CREATE TABLE " + CROP_TABLE + " (\n" +
                "    " + COLUMN_CID + " INTEGER NOT NULL CONSTRAINT crop_pk PRIMARY KEY AUTOINCREMENT,\n" +
                "    " + COLUMN_CROPID + " varchar(200) NOT NULL,\n" +
                "    " + COLUMN_CROPVARIETY + " varchar(200) NOT NULL,\n" +
                "    " + COLUMN_CROPNAME + " varchar(200) NOT NULL\n" +
                ");";
        /*
         * Executing the string to create the table
         * */
        sqLiteDatabase.execSQL(crop_sql);
        sqLiteDatabase.execSQL(InfoTabPriceTable.CREATE_TABLE);
        sqLiteDatabase.execSQL(DiseaseDb.CREATE_DISEASE_INFO_TABLE);
        sqLiteDatabase.execSQL(DiseaseDb.CREATE_DISEASE_NAME_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        /*
         * We are doing nothing here
         * Just dropping and creating the table
         * */
        String crop_sql = "DROP TABLE IF EXISTS " + CROP_TABLE + ";";
        sqLiteDatabase.execSQL(crop_sql);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DiseaseDb.TABLE_DISEASE_INFO);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DiseaseDb.TABLE_DISEASE_NAMES);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + InfoTabPriceTable.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    /*
     * CREATE OPERATION
     * ====================
     * This is the first operation of the CRUD.
     * This method will create a new employee in the table
     * Method is taking all the parameters required
     *
     * Operation is very simple, we just need a content value objects
     * Inside this object we will put everything that we want to insert.
     * So each value will take the column name and the value that is to inserted
     * for the column name we are using the String variables that we defined already
     * And that is why we converted the hardcoded string to variables
     *
     * Once we have the contentValues object with all the values required
     * We will call the method getWritableDatabase() and it will return us the SQLiteDatabase object and we can write on the database using it.
     *
     * With this object we will call the insert method it takes 3 parameters
     * 1. String -> The table name where the value is to be inserted
     * 2. String -> The default values of null columns, it is null here as we don't have any default values
     * 3. ContentValues -> The values that is to be inserted
     *
     * insert() will return the inserted row id, if there is some error inserting the row
     * it will return -1
     *
     * So here we are returning != -1, it will be true of record is inserted and false if not inserted
     * */

    public boolean addToCropTable(String cropid, String variety, String crop_name) {
        Log.d(TAG, cropid + " " + variety + " " + crop_name);
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_CROPID, cropid);
        contentValues.put(COLUMN_CROPVARIETY, variety);
        contentValues.put(COLUMN_CROPNAME, crop_name);
        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(CROP_TABLE, null, contentValues);
        return id != -1;
    }

    /*
     * READ OPERATION
     * =================
     * Here we are reading values from the database
     * First we called the getReadableDatabase() method it will return us the SQLiteDatabase instance
     * but using it we can only perform the read operations.
     *
     * We are running rawQuery() method by passing the select query.
     * rawQuery takes two parameters
     * 1. The query
     * 2. String[] -> Arguments that is to be binded -> We use it when we have a where clause in our query to bind the where value
     *
     * rawQuery returns a Cursor object having all the data fetched from database
     * */

    public Cursor getDataByCropName(String commodity) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + CROP_TABLE + " WHERE crop_name = '" + commodity + "'", null);
        return cursor;
    }

    public Cursor getAllData() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + CROP_TABLE, null);
        return cursor;
    }
    /*
     * UPDATE OPERATION
     * ==================
     * Here we are performing the update operation. The proecess is same as the Create operation.
     * We are first getting a database instance using getWritableDatabase() method as the operation we need to perform is a write operation
     * Then we have the contentvalues object with the new values
     *
     * to update the row we use update() method. It takes 4 parameters
     * 1. String -> It is the table name
     * 2. ContentValues -> The new values
     * 3. String -> Here we pass the column name = ?, the column we want to use for putting the where clause
     * 4. String[] -> The values that is to be binded with the where clause
     * */
//    boolean updateEmployee(int id, String name, String dept, double salary) {
//        SQLiteDatabase db = getWritableDatabase();
//        ContentValues contentValues = new ContentValues();
//        contentValues.put(COLUMN_NAME, name);
//        contentValues.put(COLUMN_DEPT, dept);
//        contentValues.put(COLUMN_SALARY, salary);
//        return db.update(TABLE_DISEASE_INFO, contentValues, COLUMN_ID + "=?", new String[]{String.valueOf(id)}) == 1;
//    }


    /*
     * DELETE OPERATION
     * ======================
     *
     * This is the last delete operation.  To delete again we need a writable database using getWritableDatabase()
     * Then we will call the delete method. It takes 3 parameters
     * 1. String -> Table name
     * 2. String -> The where clause passed as columnname = ?
     * 3. String[] -> The values to be binded on the where clause
     * */

    public boolean deleteTable(String tablename, String whereClause) {
        SQLiteDatabase db = getWritableDatabase();
        int status = db.delete(tablename, whereClause, null);
        db.close();
        return status != 0;
    }

    public void deleteInterestedCropEntries() {
        String query = "DELETE FROM " + CROP_TABLE;
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(query);
        db.close();
    }
}
