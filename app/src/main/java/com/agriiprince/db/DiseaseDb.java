package com.agriiprince.db;

/**
 * Created by Manu Sajjan on 15-12-2018.
 */

//  TODO - replace by room
public class DiseaseDb {

    public static final String KEY_ID = "id";

    public static final String id = "crop_id";
    public static final String crop_name = "crop_name";
    public static final String disease_name = "disease_name";
    public static final String scientific_name = "scientific_name";
    public static final String category = "category";
    public static final String cause = "cause";
    public static final String management = "management";
    public static final String symptoms = "symptoms";
    public static final String comments = "comments";

    public static final String crop_images = "crop_images";

    public static final String download_status = "download_status";

    public static final String language = "language";

    public static final String TABLE_DISEASE_INFO = "disease_info";

    public static final String TABLE_DISEASE_NAMES = "disease_names";

    public static final String CREATE_DISEASE_INFO_TABLE =
            "CREATE TABLE " + TABLE_DISEASE_INFO + " (" +
                    KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + id + " TEXT," +
                    crop_name + " TEXT," + disease_name + " TEXT," +
                    scientific_name + " TEXT," + category + " TEXT," +
                    cause + " TEXT," + management + " TEXT," +
                    symptoms + " TEXT," + comments + " TEXT," +
                    crop_images + " TEXT," + language + " TEXT" +
                    ")";


    public static final String CREATE_DISEASE_NAME_TABLE =
            "CREATE TABLE " + TABLE_DISEASE_NAMES + " (" +
                    KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + id + " TEXT," +
                    crop_name + " TEXT," + language + " TEXT," +
                    download_status + " INTEGER DEFAULT " + + DownloadStatus.NONE +
                    ")";

    public interface DownloadStatus {
        int NONE = 0;
        int REQUESTED = 1;
        int COMPLETE = 2;
    }
}
