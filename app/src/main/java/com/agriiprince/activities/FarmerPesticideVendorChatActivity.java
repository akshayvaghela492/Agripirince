package com.agriiprince.activities;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.utils.StringUtils;

public class FarmerPesticideVendorChatActivity extends AppCompatActivity {

    private StringUtils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        utils = AppController.getInstance().getStringUtils();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_chat);

        new Builder(this)
                .setMessage(utils.getLocalizedString(R.string.you_have_not_subscribed_to_this_feature))
                .setPositiveButton("Okay", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FarmerPesticideVendorChatActivity.super.onBackPressed();
                    }
                }).setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                FarmerPesticideVendorChatActivity.super.onBackPressed();
            }
        }).show();
    }
}
