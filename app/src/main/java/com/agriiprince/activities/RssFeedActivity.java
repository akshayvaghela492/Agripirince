package com.agriiprince.activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.adapter.RssFeedListAdapter;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.model.RssFeedModel;
import com.agriiprince.utils.DateConvertUtils;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.agriiprince.utils.Utils.showLog;

public class RssFeedActivity extends AppCompatActivity {


    RecyclerView rss_rv;
    String mobileNo;
    DateConvertUtils dateConvertUtils;
    HashMap<String, String> links;
    String[] interested;
    RssFeedListAdapter rssFeedListAdapter;
    ArrayList<RssFeedModel> mFeedModelList;
    private ProgressDialog progressDialog;
    PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rssfarmers);
        setTitle("RSS Feed");

        rss_rv = (RecyclerView) findViewById(R.id.rss_feed_list);
        rss_rv.setLayoutManager(new LinearLayoutManager(this));
        prefManager = new PrefManager(this);

        mFeedModelList = new ArrayList<>();
        rssFeedListAdapter = new RssFeedListAdapter(this, mFeedModelList);
        rss_rv.setAdapter(rssFeedListAdapter);

        mobileNo = prefManager.getMobileNo();
        links = new HashMap<>();

        dateConvertUtils = new DateConvertUtils();

        links.put("India Times", Config.INDIA_TIMES);
        links.put("The Hindu", Config.THE_HINDU);
        links.put("Down To Earth", Config.DOWN_TO_EARTH);
        links.put("Open The Magazine", Config.OPEN_THE_MAGAZINE);
        links.put("OECD News", Config.OECD);
        links.put("Govt RSS Agriculture English", Config.RSS_EN);
        links.put("Govt RSS Agriculture Hindi", Config.RSS_HI);
        links.put("GOI Ministry of Agriculture", Config.GOI_Directory);
        links.put("GOI Ministry of AYUSH", Config.GOI_DIRECTORY_AYUSH);
        links.put("GOI Ministry of Chemicals and Fertilizers", Config.GOI_DIRECTORY_CHEMICAL);
        links.put("Nature Journals", Config.NATURE);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));

        String s = prefManager.getRSStopics();
        if (s == null) {
            Toast.makeText(RssFeedActivity.this, "No topics subscribed.", Toast.LENGTH_SHORT).show();
        } else {
            interested = s.split(",");
            if (interested.length > 0) {
                progressDialog.show();
                for (String t : interested) {
                    String source = t;
                    String link = links.get(t);
                    if (source != null && link != null) {
                        new FetchFeedTask(source, link).execute((Void) null);
                    }
                }
            } else {
                Toast.makeText(RssFeedActivity.this, "No topics subscribed.", Toast.LENGTH_SHORT).show();
            }
        }


    }

    class FetchFeedTask extends AsyncTask<Void, Void, Boolean> {

        String TAG = "rss-log";
        String source;

        private String urlLink;

        public FetchFeedTask(String s, String link) {
            source = s;
            urlLink = link;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                if (!urlLink.startsWith("http://") && !urlLink.startsWith("https://"))
                    urlLink = "http://" + urlLink;

                showLog("Fetching feed from " + source);
                URL url = new URL(urlLink);
                InputStream inputStream = url.openConnection().getInputStream();
                showLog("Fetch complete. Begin Parsing");

                List<RssFeedModel> ls = dateConvertUtils.parseFeed(source, inputStream);
                showLog("parsing finsihed. Size of parsed items : " + ls.size());
                if (ls.size() > 0) {
                    mFeedModelList.addAll(ls);
                }

                return true;
            } catch (IOException e) {
                Log.e(TAG, "Error", e);
            } catch (XmlPullParserException e) {
                Log.e(TAG, "Error", e);
            }
            return false;
        }


        @Override
        protected void onPostExecute(Boolean success) {

            progressDialog.cancel();

            if (success) {
                // Fill RecyclerView
                rssFeedListAdapter.notifyDataSetChanged();
                Toast.makeText(RssFeedActivity.this, "Feed fetch complete: " + source, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(RssFeedActivity.this,
                        "Invalid Rss feed url",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}

