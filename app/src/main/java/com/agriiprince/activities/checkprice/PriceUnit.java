package com.agriiprince.activities.checkprice;

public interface PriceUnit {
    int KG = 1;
    int QUINTAL = 2;
    int TONNE = 3;
}
