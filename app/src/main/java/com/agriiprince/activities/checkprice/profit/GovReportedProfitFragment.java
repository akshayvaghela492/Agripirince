package com.agriiprince.activities.checkprice.profit;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.activities.checkprice.CheckPriceAndroidViewModel;
import com.agriiprince.activities.checkprice.SortBy;
import com.agriiprince.activities.checkprice.price.CheckPriceBaseFragment;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.FragmentGovReportedProfitBinding;
import com.agriiprince.databinding.ItemCheckProfitBinding;
import com.agriiprince.helper.UserLocation;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.helper.PriceDistribution;
import com.agriiprince.mvvm.retrofit.model.checkprice.GovReportedPriceData;
import com.agriiprince.utils.MathUtils;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.UnitUtils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.agriiprince.activities.checkprice.SortBy.MAX_PRICE;
import static com.agriiprince.activities.checkprice.SortBy.MIN_PRICE;
import static com.agriiprince.activities.checkprice.SortBy.MODAL_PRICE;

public class GovReportedProfitFragment extends CheckPriceBaseFragment implements OnMapReadyCallback {

    private static final String TAG = "GovReportedProfit";


    public GovReportedProfitFragment() {
        // Required empty public constructor
    }

    public static GovReportedProfitFragment newInstance() {
        GovReportedProfitFragment fragment = new GovReportedProfitFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private int SORT_MODE = SortBy.MODAL_PRICE;
    private UnitUtils.Weight SORT_UNIT = UnitUtils.Weight.KG;

    private boolean showNoDataDialog;

    private SupportMapFragment mMapFragment;
    private GoogleMap mGoogleMap;

    private UserProfile mUserProfile;

    private PrefManager prefManager;

    private List<GovReportedPriceData> mGovReports;
    private List<CheckProfitModel> mCheckProfits;

    private Location userLocation;
    private Location nearestMandiLocation;

    private StringUtils utils;

    private CheckProfitAdapter adapter;

    private PriceDistribution<CheckProfitModel> mPriceDistribution;

    private CheckPriceAndroidViewModel viewModel;

    private FragmentGovReportedProfitBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gov_reported_profit, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = DataBindingUtil.bind(view);
        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        viewModel = ViewModelProviders.of(getActivity()).get(CheckPriceAndroidViewModel.class);

        utils = AppController.getInstance().getStringUtils();

        mGovReports = new ArrayList<>();
        mCheckProfits = new ArrayList<>();
        mUserProfile = new UserProfile(getContext());
        prefManager = new PrefManager(getContext());
        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        mPriceDistribution = new PriceDistribution<>(mCheckProfits, distributionComparator, distributeListener);

        adapter = new CheckProfitAdapter(utils);

        binding.recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recycler.setAdapter(adapter);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mMapFragment.getMapAsync(this);

        binding.setViewModel(viewModel);
        binding.executePendingBindings();


        binding.priceReportBanner.setText(utils.getLocalizedString(R.string.check_price_banner, true));
        binding.priceReportBanner.setSelected(true);

        setupBottomSortTab();

        setObservers();
    }

    @Override
    protected String getPrimaryLocale() {
        return prefManager.getLocale();
    }

    @Override
    protected String getSecondaryLocale() {
        return prefManager.getSecondaryLocale();
    }

    @Override
    protected void onSetupCrops() {

    }

    private void setObservers() {
        viewModel.onFetchGovPrices().observe(this, new Observer<List<GovReportedPriceData>>() {
            @Override
            public void onChanged(@Nullable List<GovReportedPriceData> list) {
                Log.d(TAG, "onChanged: data");
                showNoDataDialog = false;

                if (mGoogleMap != null) mGoogleMap.clear();

                adapter.clear();

                if (list != null && list.size() > 0) {
                    mGovReports = list;
                    new CalculateProfit().execute();
                }
            }
        });

        viewModel.isGovPricesAvailable().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                Log.d(TAG, "onChanged: is available");
                if (aBoolean != null && !aBoolean) {
                    adapter.clear();
                    if (mGoogleMap != null) mGoogleMap.clear();
                    zoomOutIndiaMap();

                    showNoDataDialog = true;
                    showDialogForNoData();
                }
            }
        });

        viewModel.getOnInputValueChange().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean != null && aBoolean) new CalculateProfit().execute();
            }
        });


        viewModel.getUserVisibleFragmentIndex().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                if (integer != null) setUserVisibleHint(integer == 0);
                else setUserVisibleHint(false);
                showDialogForNoData();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        try {
            if (getContext() != null)
                mGoogleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.map_style));
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.setInfoWindowAdapter(infoWindowAdapter);


        zoomOutIndiaMap();

        UserLocation userLocation = new UserLocation(getActivity());
        userLocation.getLastKnowLocation(onUserLocationResults);
    }

    private GoogleMap.InfoWindowAdapter infoWindowAdapter = new GoogleMap.InfoWindowAdapter() {
        @Override
        public View getInfoWindow(Marker marker) {
            CheckProfitModel checkProfitModel = (CheckProfitModel) marker.getTag();

            if (checkProfitModel == null || getActivity() == null) return null;

            View view = getLayoutInflater().inflate(R.layout.item_check_profit, null);
            ItemCheckProfitBinding binding = DataBindingUtil.bind(view);

            if (binding == null) return null;

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            binding.root.setMinWidth(displayMetrics.widthPixels);

            binding.minLabel.setText(utils.getLocalizedString(R.string.min));
            binding.modalLabel.setText(utils.getLocalizedString(R.string.modal));
            binding.maxLabel.setText(utils.getLocalizedString(R.string.max));

            binding.netExpenseLabel.setText(utils.getLocalizedString(R.string.net_expense));
            binding.breakupLabel.setText(utils.getLocalizedString(R.string.net_expense_breakup, true));
            binding.harvestLabel.setText(utils.getLocalizedString(R.string.harvesting_and_loading));
            binding.transportationLabel.setText(utils.getLocalizedString(R.string.transportation));
            binding.wastageLabel.setText(utils.getLocalizedString(R.string.wastage));
            binding.otherLabel.setText(utils.getLocalizedString(R.string.other));


            String name = checkProfitModel.getMandi() + " (" + checkProfitModel.getDistance() + " Km)";

            binding.mandi.setText(name);
            binding.min.setText(checkProfitModel.getMinProfitString(SORT_UNIT));
            binding.modal.setText(checkProfitModel.getModalProfitString(SORT_UNIT));
            binding.max.setText(checkProfitModel.getMaxProfitString(SORT_UNIT));
            binding.netExpense.setText(checkProfitModel.getNetExpenseString(SORT_UNIT));

            binding.transportationFormula.setText(CheckProfitModel.getTransporationFormaula(checkProfitModel.getDistance()));
            binding.wastageFormula.setText(CheckProfitModel.getWastageFormula(checkProfitModel.getDistance()));

            if (CheckProfitModel.fixed) binding.otherFormula.setVisibility(View.GONE);
            else binding.otherFormula.setText(CheckProfitModel.getOtherFormula(checkProfitModel.getDistance()));

            binding.harvest.setText(CheckProfitModel.getHarvestingCost(SORT_UNIT));
            binding.transportation.setText(CheckProfitModel.getTranportationCost(checkProfitModel.getDistance(), SORT_UNIT));
            binding.wastage.setText(CheckProfitModel.getWastageCostString(checkProfitModel.getDistance(), SORT_UNIT));
            binding.other.setText(CheckProfitModel.getOtherCostString(checkProfitModel.getDistance(), SORT_UNIT));

            view.invalidate();
            return view;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }
    };

    private UserLocation.OnUserLocationResults onUserLocationResults = new UserLocation.OnUserLocationResults() {
        @Override
        public void onLocationResult(Location lastLocation) {
            GovReportedProfitFragment.this.userLocation = lastLocation;
            if (mCheckProfits.size() > 0) new CalculateProfit().execute();
        }

        @Override
        public void onLocationFail() {
            LatLng latLng = new LatLng(20.5937, 78.9629); // new delhi co ordinates
            GovReportedProfitFragment.this.userLocation = new Location("default");
            GovReportedProfitFragment.this.userLocation.setLatitude(latLng.latitude);
            GovReportedProfitFragment.this.userLocation.setLongitude(latLng.longitude);
            if (mCheckProfits.size() > 0) new CalculateProfit().execute();
        }
    };

    private void zoomInToNearestMandi(Location location) {
        Log.d(TAG, "zoomInToNearestMandi: ");
        if (location != null && mGoogleMap != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory
                    .newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), ZOOM_IN);
            mGoogleMap.animateCamera(cameraUpdate);
        }
    }


    private class CalculateProfit extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (mGovReports == null) return null;

            double nearestDistance = 100000000d;
            GovReportedPriceData nearestReport = null;

            mCheckProfits.clear();

            for (GovReportedPriceData price : mGovReports) {

                if (userLocation != null) {
                    Location dest = new Location("dest");
                    dest.setLatitude(Double.parseDouble(price.getMandiLat()));
                    dest.setLongitude(Double.parseDouble(price.getMandiLng()));
                    double distance = getDistance(userLocation, dest);
                    price.setDistance(distance);

                    if (nearestDistance > distance) {
                        nearestDistance = distance;
                        nearestReport = price;
                    }
                }

                CheckProfitModel profitModel = new CheckProfitModel();

                profitModel.setId(price.getId());
                profitModel.setMandi(price.getMarket());
                profitModel.setLat(Double.parseDouble(price.getMandiLat()));
                profitModel.setLng(Double.parseDouble(price.getMandiLng()));
                profitModel.setMinProfit(CheckProfitModel.getProfit(price.getMinP(), price.getDistanceInKm()));
                profitModel.setModalProfit(CheckProfitModel.getProfit(price.getModP(), price.getDistanceInKm()));
                profitModel.setMaxProfit(CheckProfitModel.getProfit(price.getMaxP(), price.getDistanceInKm()));
                profitModel.setNetExpense(CheckProfitModel.getNetExpenses(price.getDistanceInKm()));
                profitModel.setDistance(price.getDistanceInKm());

                mCheckProfits.add(profitModel);
            }

            if (nearestReport != null) {
                if (nearestMandiLocation == null) nearestMandiLocation = new Location("nearest location");
                nearestMandiLocation.setLatitude(Double.parseDouble(nearestReport.getMandiLat()));
                nearestMandiLocation.setLongitude(Double.parseDouble(nearestReport.getMandiLng()));
            } else {
                nearestMandiLocation = null;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d(TAG, "onPostExecute: calculate profits" + mCheckProfits.size());
            dismissProgressDialog();
            distributeData();

            if (nearestMandiLocation != null) zoomInToNearestMandi(nearestMandiLocation);
            else zoomInToNearestMandi(userLocation);
        }
    }

    private void showDialogForNoData() {
        if (getUserVisibleHint() && showNoDataDialog) {
            if (getContext() == null) return;

            showNoDataDialog = false;
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle(utils.getLocalizedString(R.string.check_prices_profit_title));
            alertDialog.setMessage(utils.getLocalizedString(R.string.data_awaited_please_check_later));
            alertDialog.setCancelable(true);
            alertDialog.setPositiveButton(utils.getLocalizedString(R.string.okay), null);
            alertDialog.show();
        }
    }

    private void zoomOutIndiaMap() {
        Log.d(TAG, "zoomOutIndiaMap");
        if (mGoogleMap != null) {
            LatLng latLng = new LatLng(20.5937, 78.9629);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_OUT);
            mGoogleMap.animateCamera(cameraUpdate);
        }
    }

/*
    private void getInput() {
        area = Integer.valueOf(viewModel.getArea());
        totalYield = Integer.valueOf(viewModel.getYield());
        harvestCost = Integer.valueOf(viewModel.getHarvestCost());
        weight = Integer.valueOf(viewModel.getTransportWeight());
        transportDistance = Integer.valueOf(viewModel.getTransportDistance());
        transportCost = Integer.valueOf(viewModel.getTransportCost());
        wasteDistance = Integer.valueOf(viewModel.getWasteDistance());
        waste = viewModel.getWaste();

        mHarvestAreaUnit = viewModel.getHarvestAreaUnit();
        mHarvestWeightUnit = viewModel.getHarvestWeightUnit();
        mTransportationWeightUnit = viewModel.getTransportationWeightUnit();
        mTransportationLengthUnit = viewModel.getTransportationLengthUnit();
        mWastageLengthUnit = viewModel.getWastageLengthUnit();
    }

*/

    private void setupBottomSortTab() {
        binding.sort.sortByLabel.setText(utils.getLocalizedString(R.string.sort_by, true));
        binding.sort.sortMinPrice.setText(utils.getLocalizedString(R.string.min_price));
        binding.sort.sortModalPrice.setText(utils.getLocalizedString(R.string.modal_price));
        binding.sort.sortMaxPrice.setText(utils.getLocalizedString(R.string.max_price));
        binding.sort.sortTonnage.setText(utils.getLocalizedString(R.string.tonnage));

        binding.sort.sortTonnage.setVisibility(View.GONE);

        binding.sort.sortOptionLayout.setVisibility(View.GONE);
        binding.sort.sortToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.sort.sortOptionLayout.getVisibility() == View.VISIBLE) {
                    binding.sort.sortOptionLayout.setVisibility(View.GONE);
                    binding.sort.sortToggle.setImageResource(R.drawable.ic_keyboard_arrow_up);
                } else {
                    binding.sort.sortOptionLayout.setVisibility(View.VISIBLE);
                    binding.sort.sortToggle.setImageResource(R.drawable.ic_keyboard_arrow_down);
                }
            }
        });

        binding.sort.sortMinPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_MODE != MIN_PRICE) {
                    SORT_MODE = MIN_PRICE;
                    updateSortMode();
                    distributeData();
                }
            }
        });

        binding.sort.sortModalPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_MODE != SortBy.MODAL_PRICE) {
                    SORT_MODE = SortBy.MODAL_PRICE;
                    updateSortMode();
                    distributeData();
                }
            }
        });

        binding.sort.sortMaxPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_MODE != SortBy.MAX_PRICE) {
                    SORT_MODE = SortBy.MAX_PRICE;
                    updateSortMode();
                    distributeData();
                }
            }
        });

        binding.sort.sortKg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_UNIT != UnitUtils.Weight.KG) {
                    SORT_UNIT = UnitUtils.Weight.KG;
                    updateSortUnit();
                    distributeData();
                }
            }
        });

        binding.sort.sortQuintal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_UNIT != UnitUtils.Weight.QUINTAL) {
                    SORT_UNIT = UnitUtils.Weight.QUINTAL;
                    updateSortUnit();
                    distributeData();
                }
            }
        });

        binding.sort.sortTonne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_UNIT != UnitUtils.Weight.TON) {
                    SORT_UNIT = UnitUtils.Weight.TON;
                    updateSortUnit();
                    distributeData();
                }
            }
        });

        updateSortMode();
        updateSortUnit();
    }

    private void updateSortMode() {
        binding.sort.sortMinPrice.setChecked(false);
        binding.sort.sortModalPrice.setChecked(false);
        binding.sort.sortMaxPrice.setChecked(false);
        binding.sort.sortTonnage.setChecked(false);

        binding.sort.sortMinPrice.setTextColor(getResources().getColor(R.color.primaryTextColor));
        binding.sort.sortModalPrice.setTextColor(getResources().getColor(R.color.primaryTextColor));
        binding.sort.sortMaxPrice.setTextColor(getResources().getColor(R.color.primaryTextColor));
        binding.sort.sortTonnage.setTextColor(getResources().getColor(R.color.primaryTextColor));

        switch (SORT_MODE) {
            case MIN_PRICE:
                binding.sort.sortMinPrice.setChecked(true);
                binding.sort.sortMinPrice.setTextColor(getResources().getColor(R.color.white));
                break;
            case SortBy.MODAL_PRICE:
                binding.sort.sortModalPrice.setChecked(true);
                binding.sort.sortModalPrice.setTextColor(getResources().getColor(R.color.white));
                break;
            case SortBy.MAX_PRICE:
                binding.sort.sortMaxPrice.setChecked(true);
                binding.sort.sortMaxPrice.setTextColor(getResources().getColor(R.color.white));
                break;
            case SortBy.TONNAGE:
                binding.sort.sortTonnage.setChecked(true);
                binding.sort.sortTonnage.setTextColor(getResources().getColor(R.color.white));
                break;
        }
    }

    private void updateSortUnit() {

        adapter.setSortUnit(SORT_UNIT);

        binding.sort.sortKg.setChecked(false);
        binding.sort.sortQuintal.setChecked(false);
        binding.sort.sortTonne.setChecked(false);

        binding.sort.sortKg.setTextColor(getResources().getColor(R.color.primaryTextColor));
        binding.sort.sortQuintal.setTextColor(getResources().getColor(R.color.primaryTextColor));
        binding.sort.sortTonne.setTextColor(getResources().getColor(R.color.primaryTextColor));

        switch (SORT_UNIT) {
            case KG:
                binding.sort.sortKg.setChecked(true);
                binding.sort.sortKg.setTextColor(getResources().getColor(R.color.white));
                break;
            case QUINTAL:
                binding.sort.sortQuintal.setChecked(true);
                binding.sort.sortQuintal.setTextColor(getResources().getColor(R.color.white));
                break;
            case TON:
                binding.sort.sortTonne.setChecked(true);
                binding.sort.sortTonne.setTextColor(getResources().getColor(R.color.white));
                break;
        }
    }

    private void distributeData() {
        mPriceDistribution.distribute(mCheckProfits);
    }


    private Comparator<CheckProfitModel> distributionComparator = new Comparator<CheckProfitModel>() {
        @Override
        public int compare(CheckProfitModel o1, CheckProfitModel o2) {
            if (SORT_MODE == MIN_PRICE) {
                return Double.compare(o1.getMinProfit(), o2.getMinProfit());
            } else if (SORT_MODE == MODAL_PRICE) {
                return Double.compare(o1.getModalProfit(), o2.getModalProfit());
            } else if (SORT_MODE == MAX_PRICE) {
                return Double.compare(o1.getMaxProfit(), o2.getMaxProfit());
            }
            return 0;
        }
    };

    private PriceDistribution.Distribute<CheckProfitModel> distributeListener = new PriceDistribution.Distribute<CheckProfitModel>() {
        @Override
        public double getIntervalPrice(CheckProfitModel o) {
            if (SORT_MODE == MIN_PRICE) {
                return o.getMinProfit();
            } else if (SORT_MODE == MODAL_PRICE) {
                return o.getModalProfit();
            } else if (SORT_MODE == MAX_PRICE) {
                return o.getMaxProfit();
            }
            return 0;
        }

        @Override
        public void onSuccess(int maxInterval, List<Double> intervals, List<CheckProfitModel> list) {
            Log.d(TAG, "" + list.size());

            adapter.replaceAll(list);
            setPriceRanges(maxInterval, intervals, list);

        }

        @Override
        public void failure() {

        }
    };

    private void setPriceRanges(int maxInterval, List<Double> intervals, List<CheckProfitModel> list) {
        if (intervals.size() <= 7) return;
        showProgressDialog();

        String range = "";

        double lower;
        double upper;
        lower = intervals.get(0);
        if (intervals.get(0).equals(intervals.get(1))) upper = intervals.get(1);
        else upper = intervals.get(1) - 0.1;

        if (SORT_MODE == 4) {
            range = MathUtils.roundDecimal(lower, MathUtils.DoubleFormat.ONE_DECIMAL) + " - " +
                    MathUtils.roundDecimal(upper, MathUtils.DoubleFormat.ONE_DECIMAL);
        } else {
            range = MathUtils.convertToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                    MathUtils.convertToStringWithoutUnit(upper, SORT_UNIT);
        }

        binding.sort.sortViolet.setText(range);

        lower = intervals.get(1);
        if (intervals.get(1).equals(intervals.get(2))) upper = intervals.get(2);
        else upper = intervals.get(2) - 0.1;

        if (SORT_MODE == 4) {
            range = MathUtils.roundDecimal(lower, MathUtils.DoubleFormat.ONE_DECIMAL) + " - " +
                    MathUtils.roundDecimal(upper, MathUtils.DoubleFormat.ONE_DECIMAL);
        } else {
            range = MathUtils.convertToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                    MathUtils.convertToStringWithoutUnit(upper, SORT_UNIT);
        }

        binding.sort.sortIndigo.setText(range);

        lower = intervals.get(2);
        if (intervals.get(2).equals(intervals.get(3))) upper = intervals.get(3);
        else upper = intervals.get(3) - 0.1;

        if (SORT_MODE == 4) {
            range = MathUtils.roundDecimal(lower, MathUtils.DoubleFormat.ONE_DECIMAL) + " - " +
                    MathUtils.roundDecimal(upper, MathUtils.DoubleFormat.ONE_DECIMAL);
        } else {
            range = MathUtils.convertToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                    MathUtils.convertToStringWithoutUnit(upper, SORT_UNIT);
        }

        binding.sort.sortBlue.setText(range);

        lower = intervals.get(3);
        if (intervals.get(3).equals(intervals.get(4))) upper = intervals.get(4);
        else upper = intervals.get(4) - 0.1;

        if (SORT_MODE == 4) {
            range = MathUtils.roundDecimal(lower, MathUtils.DoubleFormat.ONE_DECIMAL) + " - " +
                    MathUtils.roundDecimal(upper, MathUtils.DoubleFormat.ONE_DECIMAL);
        } else {
            range = MathUtils.convertToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                    MathUtils.convertToStringWithoutUnit(upper, SORT_UNIT);
        }

        binding.sort.sortGreen.setText(range);

        lower = intervals.get(4);
        if (intervals.get(4).equals(intervals.get(5))) upper = intervals.get(5);
        else upper = intervals.get(5) - 0.1;

        if (SORT_MODE == 4) {
            range = MathUtils.roundDecimal(lower, MathUtils.DoubleFormat.ONE_DECIMAL) + " - " +
                    MathUtils.roundDecimal(upper, MathUtils.DoubleFormat.ONE_DECIMAL);
        } else {
            range = MathUtils.convertToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                    MathUtils.convertToStringWithoutUnit(upper, SORT_UNIT);
        }

        binding.sort.sortYellow.setText(range);

        lower = intervals.get(5);
        if (intervals.get(5).equals(intervals.get(6))) upper = intervals.get(6);
        else upper = intervals.get(6) - 0.1;

        if (SORT_MODE == 4) {
            range = MathUtils.roundDecimal(lower, MathUtils.DoubleFormat.ONE_DECIMAL) + " - " +
                    MathUtils.roundDecimal(upper, MathUtils.DoubleFormat.ONE_DECIMAL);
        } else {
            range = MathUtils.convertToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                    MathUtils.convertToStringWithoutUnit(upper, SORT_UNIT);
        }

        binding.sort.sortOrange.setText(range);

        lower = intervals.get(6);
        upper = intervals.get(7);

        if (SORT_MODE == 4) {
            range = MathUtils.roundDecimal(lower, MathUtils.DoubleFormat.ONE_DECIMAL) + " - " +
                    MathUtils.roundDecimal(upper, MathUtils.DoubleFormat.ONE_DECIMAL);
        } else {
            range = MathUtils.convertToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                    MathUtils.convertToStringWithoutUnit(upper, SORT_UNIT);
        }

        binding.sort.sortRed.setText(range);

        if (mGoogleMap != null)
            mGoogleMap.clear();

        int size = list.size();
        for (int i = 0; i < size; i++) {
            CheckProfitModel checkProfitModel = mCheckProfits.get(i);

            double checkPrice = 0;
            if (SORT_MODE == 1) {
                checkPrice = checkProfitModel.getMinProfit();
            } else if (SORT_MODE == 2) {
                checkPrice = checkProfitModel.getMinProfit();
            } else if (SORT_MODE == 3) {
                checkPrice = checkProfitModel.getMinProfit();
            }

//            Log.d(TAG, "map : " + i + "  price: " + checkPrice);

            for (int j = 0; j < maxInterval; j++) {
                double limit = intervals.get(j + 1); // consider upper limit

                if (j != maxInterval - 1)
                    limit = limit - 0.1;

                if (checkPrice <= limit) {
//                    Log.d(TAG, "to : " + j + " limit " + limit);
                    addMarker(checkProfitModel, j);

                    break;
                }
            }
        }

        dismissProgressDialog();
    }

    private void addMarker(CheckProfitModel checkProfitModel, int color) {
        if (mGoogleMap == null) return;

        double lat = checkProfitModel.getLat();
        double lng = checkProfitModel.getLng();

        if (lat == 0 || lng == 0) return;

        try {
            BitmapDescriptor bitmapDescriptor = getMarkerIcon(colors[color]);
            if (bitmapDescriptor != null) {
                mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng))
                        .icon(bitmapDescriptor)
                ).setTag(checkProfitModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
