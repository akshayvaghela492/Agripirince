package com.agriiprince.activities.checkprice;

import android.content.Context;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.databinding.ItemCheckPriceTextBinding;
import com.agriiprince.model.OeReportedPrice;
import com.agriiprince.utils.MathUtils;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.UnitUtils;

import java.util.ArrayList;
import java.util.List;

import static com.agriiprince.utils.UnitUtils.Weight.KG;

public class TextualAgentPriceAdapter extends RecyclerView.Adapter<TextualAgentPriceAdapter.TextualAgentViewHolder> {

    private StringUtils utils;

    private ApTextListener listener;

    private UnitUtils.Weight SORT_UNIT = KG;

    private List<OeReportedPrice> prices;
    private Context context;

    public TextualAgentPriceAdapter(Context context) {
        this.context = context;
        this.utils = AppController.getInstance().getStringUtils();

        prices = new ArrayList<>();
    }

    public void setListener(ApTextListener listener) {
        this.listener = listener;
    }

    public void removeListener() {
        this.listener = null;
    }

    public void setSortUnit(UnitUtils.Weight SORT_UNIT) {
        this.SORT_UNIT = SORT_UNIT;
    }

    public void clear() {
        prices.clear();
        notifyDataSetChanged();
    }

    public void addAllItems(List<OeReportedPrice> list) {
        prices.clear();

        if (list != null) prices.addAll(list);

        notifyDataSetChanged();
    }

    public OeReportedPrice getItem(int position) {
        return prices.get(position);
    }

    @NonNull
    @Override
    public TextualAgentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_check_price_text, viewGroup, false);
        return new TextualAgentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TextualAgentViewHolder holder, int i) {
        OeReportedPrice price = prices.get(i);

        holder.binding.textMandiLabel.setText(utils.getLocalizedString(R.string.mandi));
        holder.binding.textVarietyLabel.setText(utils.getLocalizedString(R.string.variety));
        holder.binding.textGradeLabel.setText(utils.getLocalizedString(R.string.grade));

        holder.binding.textMinLabel.setText(utils.getLocalizedString(R.string.min));
        holder.binding.textModelLabel.setText(utils.getLocalizedString(R.string.modal));
        holder.binding.textMaxLabel.setText(utils.getLocalizedString(R.string.max));

        holder.binding.textTonnageLabel.setText(utils.getLocalizedString(R.string.tonnage));
        holder.binding.textTonnageLabel.setVisibility(View.INVISIBLE);

        String mandi = price.getMandiName() + ", " + price.getMandiDistrict();
        holder.binding.textMandi.setText(mandi);
        holder.binding.textVariety.setText(price.getCommodityVariety());
        holder.binding.textGrade.setText(price.getCommodityGrade());

        String min = "Rs. " + MathUtils.convertQuintalToString(price.getMinPrice(), SORT_UNIT);
        String modal = "Rs. " + MathUtils.convertQuintalToString(price.getModalPrice(), SORT_UNIT);
        String max = "Rs. " + MathUtils.convertQuintalToString(price.getMaxPrice(), SORT_UNIT);
        holder.binding.textMinPrice.setText(min);
        holder.binding.textModalPrice.setText(modal);
        holder.binding.textMaxPrice.setText(max);

        if (price.isInterested()) {
            holder.binding.imageBookmark.setImageResource(R.drawable.ic_bookmark_black);
            ImageViewCompat.setImageTintList(holder.binding.imageBookmark,
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorAccent)));
        } else {
            holder.binding.imageBookmark.setImageResource(R.drawable.ic_bookmark_border);
            ImageViewCompat.setImageTintList(holder.binding.imageBookmark,
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.primaryTextColor)));
        }

        holder.binding.imageBookmark.setOnClickListener(holder.onClickBookmark);
    }

    @Override
    public int getItemCount() {
        return prices.size();
    }


    class TextualAgentViewHolder extends RecyclerView.ViewHolder {

        private ItemCheckPriceTextBinding binding;

        private TextualAgentViewHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }

        private View.OnClickListener onClickBookmark = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onClickBookmark(getAdapterPosition());
            }
        };
    }

    public interface ApTextListener {
        void onClickBookmark(int position);
    }
}
