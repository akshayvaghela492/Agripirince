package com.agriiprince.activities.checkprice.profit;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.databinding.ItemCheckProfitBinding;
import com.agriiprince.utils.MathUtils;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.UnitUtils;

import java.util.ArrayList;
import java.util.List;

public class CheckProfitAdapter extends RecyclerView.Adapter<CheckProfitAdapter.CheckProfitViewHolder>
        implements OnProfitClickListener{

    private StringUtils utils;

    private int currentExpandPosition = -1;

    private UnitUtils.Weight SORT_UNIT = UnitUtils.Weight.KG;

    private List<CheckProfitModel> list;

    public CheckProfitAdapter(StringUtils utils) {
        this.list = new ArrayList<>();
        this.utils = utils;
    }

    public void setSortUnit(UnitUtils.Weight unit) {
        this.SORT_UNIT = unit;
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }

    public void replaceAll(List<CheckProfitModel> items) {
        list.clear();
        list.addAll(items);
        notifyDataSetChanged();
    }

    public CheckProfitModel get(int position) {
        return list.get(position);
    }

    @Override
    public void onClickItem(int position) {
        CheckProfitModel model = get(position);
        if (position == currentExpandPosition) {
            currentExpandPosition = -1;
            model.isExpanded = false;
            notifyItemChanged(position, model);
        } else {
            if (currentExpandPosition != -1 && currentExpandPosition < getItemCount()) {
                CheckProfitModel old = get(currentExpandPosition);
                old.isExpanded = false;
                notifyItemChanged(currentExpandPosition, old);
            }

            model.isExpanded = true;
            notifyItemChanged(position, model);
            currentExpandPosition = position;
        }
    }

    @NonNull
    @Override
    public CheckProfitViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view =  LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_check_profit, viewGroup, false);
        return new CheckProfitViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(@NonNull CheckProfitViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.size() > 0 && payloads.get(0) instanceof CheckProfitModel) {
            holder.expandBreakUp(list.get(position));
        } else {
            super.onBindViewHolder(holder, position, payloads);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull CheckProfitViewHolder holder, int i) {
        holder.bindData(list.get(i));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CheckProfitViewHolder extends RecyclerView.ViewHolder {

        private OnProfitClickListener listener;

        private ItemCheckProfitBinding binding;

        public CheckProfitViewHolder(@NonNull View itemView, OnProfitClickListener listener) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            this.listener = listener;
        }


        private void bindData(CheckProfitModel model) {
            binding.minLabel.setText(utils.getLocalizedString(R.string.min));
            binding.modalLabel.setText(utils.getLocalizedString(R.string.modal));
            binding.maxLabel.setText(utils.getLocalizedString(R.string.max));

            binding.netExpenseLabel.setText(utils.getLocalizedString(R.string.net_expense));
            binding.breakupLabel.setText(utils.getLocalizedString(R.string.net_expense_breakup, true));
            binding.harvestLabel.setText(utils.getLocalizedString(R.string.harvesting_and_loading));
            binding.transportationLabel.setText(utils.getLocalizedString(R.string.transportation));
            binding.wastageLabel.setText(utils.getLocalizedString(R.string.wastage));
            binding.otherLabel.setText(utils.getLocalizedString(R.string.other));

            String name = model.getMandi() + " (" + model.getDistance() + " Km)";

            binding.mandi.setText(name);
            binding.min.setText(model.getMinProfitString(SORT_UNIT));
            binding.modal.setText(model.getModalProfitString(SORT_UNIT));
            binding.max.setText(model.getMaxProfitString(SORT_UNIT));
            binding.netExpense.setText(model.getNetExpenseString(SORT_UNIT));

            binding.root.setOnClickListener(onClickItem);

            expandBreakUp(model);
        }

        private void expandBreakUp(CheckProfitModel model) {
            if (model.isExpanded) {
                binding.transportationFormula.setText(CheckProfitModel.getTransporationFormaula(model.getDistance()));
                binding.wastageFormula.setText(CheckProfitModel.getWastageFormula(model.getDistance()));

                if (CheckProfitModel.fixed) binding.otherFormula.setVisibility(View.GONE);
                else binding.otherFormula.setText(CheckProfitModel.getOtherFormula(model.getDistance()));

                binding.harvest.setText(CheckProfitModel.getHarvestingCost(SORT_UNIT));
                binding.transportation.setText(CheckProfitModel.getTranportationCost(model.getDistance(), SORT_UNIT));
                binding.wastage.setText(CheckProfitModel.getWastageCostString(model.getDistance(), SORT_UNIT));
                binding.other.setText(CheckProfitModel.getOtherCostString(model.getDistance(), SORT_UNIT));

                binding.expenseBreakupLayout.setVisibility(View.VISIBLE);
            } else {
                binding.expenseBreakupLayout.setVisibility(View.GONE);
            }
        }

        private View.OnClickListener onClickItem = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickItem(getAdapterPosition());
            }
        };
    }
}
