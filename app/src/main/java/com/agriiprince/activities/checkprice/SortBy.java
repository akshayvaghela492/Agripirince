package com.agriiprince.activities.checkprice;

public interface SortBy {
    int MIN_PRICE = 1;
    int MODAL_PRICE = 2;
    int MAX_PRICE = 3;
    int TONNAGE = 4;
}
