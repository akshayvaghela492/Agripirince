package com.agriiprince.activities.checkprice.profit;

import com.agriiprince.utils.MathUtils;
import com.agriiprince.utils.UnitUtils;

public class CheckProfitModel {

    public static UnitUtils.Weight unit = UnitUtils.Weight.KG;

    public static boolean fixed;

    public static double area;
    public static int yield;

    public static int totalYield;
    public static int harvestCost;
    public static int transportWeight;
    public static int transportDistance;
    public static double transportCost;
    public static int wastePercent ;
    public static int wasteDistance;
    public static double otherCost;
    public static int otherDistance;

    public boolean isExpanded = false;

    private String id;

    private String mandi;

    private double netExpense;

    private double minProfit;

    private double modalProfit;

    private double maxProfit;

    private double lat;

    private double lng;

    private double distance;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMandi() {
        return mandi;
    }

    public void setMandi(String mandi) {
        this.mandi = mandi;
    }

    public double getNetExpense() {
        return netExpense;
    }

    public String getNetExpenseString(UnitUtils.Weight unit) {
        return formatPerUnit(netExpense, unit);
    }

    public void setNetExpense(double netExpense) {
        this.netExpense = netExpense;
    }

    public double getMinProfit() {
        return minProfit;
    }

    public String getMinProfitString(UnitUtils.Weight unit) {
        return formatPerUnit(minProfit, unit);
    }

    public void setMinProfit(double minProfit) {
        this.minProfit = minProfit;
    }

    public double getModalProfit() {
        return modalProfit;
    }

    public String getModalProfitString(UnitUtils.Weight unit) {
        return formatPerUnit(modalProfit, unit);
    }

    public void setModalProfit(double modalProfit) {
        this.modalProfit = modalProfit;
    }

    public double getMaxProfit() {
        return maxProfit;
    }

    public String getMaxProfitString(UnitUtils.Weight unit) {
        return formatPerUnit(maxProfit, unit);
    }

    public void setMaxProfit(double maxProfit) {
        this.maxProfit = maxProfit;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getDistance() {
        return (int) distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public static String getHarvestingCost(UnitUtils.Weight unit) {
        return formatPerUnit(getHarvestingCostPerUnit(), unit);
    }

    public static String getTranportationCost(double totalDistance, UnitUtils.Weight unit) {
        return formatPerUnit(getTransportationCostPerUnit(totalDistance), unit);
    }

    public static String getWastageCostString(double totalDistance, UnitUtils.Weight unit) {
        return formatPerUnit(getWastagePerUnit(totalDistance), unit);
    }

    public static String getOtherCostString(double totalDistance, UnitUtils.Weight unit) {
        return formatPerUnit(getOtherPerUnit(totalDistance), unit);
    }

    /**
     *
     * @param price in Rs/100 kg
     * @param totalDistance in km
     * @return
     */
    public static double getProfit(double price, double totalDistance) {
        double expense = getNetExpenses(totalDistance);
        return price / 100 - expense;
    }

    public static double getNetExpenses(double totalDistance) {
        return getHarvestingCostPerUnit() + getTransportationCostPerUnit(totalDistance) +
                getWastagePerUnit(totalDistance) + getOtherPerUnit(totalDistance);
    }

    public static double getTransportationCostPerUnit(double actualDistance) {
        if (transportWeight == 0 || transportDistance == 0) return 0;
        double perUnit = (double) transportCost / transportWeight / transportDistance * actualDistance;
        return valuePerUnit(perUnit);
    }

    public static double getHarvestingCostPerUnit() {
        if (totalYield == 0) return 0;
        double perUnit = (double) harvestCost / totalYield;
        return valuePerUnit(perUnit);
    }

    public static double getWastagePerUnit(double totalDistance) {
        if (wasteDistance == 0) return 0;
        double perUnit = (double) wastePercent / wasteDistance * totalDistance;
        return valuePerUnit(perUnit);
    }

    public static double getOtherPerUnit(double totalDistance) {
        double perUnit;
        if (fixed) {
            if (totalYield == 0) return 0;
            perUnit = (double) otherCost / totalYield;
        } else {
            if (otherDistance == 0) return 0;
            perUnit = (double) otherCost / otherDistance * totalDistance;
        }

        return valuePerUnit(perUnit);
    }

    private static double valuePerUnit(double perUnit) {
        switch (unit) {
            case KG:
                return perUnit;
            case QUINTAL:
                perUnit = perUnit * 100;
                return perUnit;
            case TON:
                return perUnit;
            default:
                return perUnit;
        }
    }

    private static String formatPerUnit(double perUnit, UnitUtils.Weight unit) {
        String value = "Rs. ";
        double amt = MathUtils.roundDecimal(perUnit, MathUtils.DoubleFormat.ZERO_DECIMAL);
        switch (unit) {
            case KG:
                return value + (int) amt + "/Kg";
            case QUINTAL:
                return value + MathUtils.roundDecimal( amt / 10, MathUtils.DoubleFormat.ONE_DECIMAL)  + "k/Qtl";
            case TON:
                return value + (int) amt + "k/Ton";
            default:
                return value + (int) amt + "/Kg";
        }
    }


    public static String getTransporationFormaula(int distanceInKM) {
        double value = 0;
        if (transportWeight == 0 || transportDistance == 0) value = 0;
        else value = (double) transportCost / transportWeight / transportDistance;

        return  "(Rs. " + CheckProfitModel.transportCost + " per " +
                CheckProfitModel.transportWeight + " Kg per " +
                CheckProfitModel.transportDistance + " Km = Rs. " +
                MathUtils.roundDecimal(value, MathUtils.DoubleFormat.ONE_DECIMAL) +
                "/Kg per Km * " + distanceInKM + " Km)";

    }

    public static String getWastageFormula(int distanceInKM) {
        double value = 0;
        if (wasteDistance == 0) value = 0;
        else value = (double) wastePercent / wasteDistance;

        return "(" + CheckProfitModel.wastePercent + "% of Rs. 100 per " + CheckProfitModel.wasteDistance + " Km = Rs. " +
                MathUtils.roundDecimal(value, MathUtils.DoubleFormat.TWO_DECIMAL) + " * " + distanceInKM + " Km)";
    }


    public static String getOtherFormula(int distance) {
        double value = 0;
        if (otherDistance == 0) value = 0;
        else value = (double) otherCost / otherDistance;

        return "(Rs. " + CheckProfitModel.otherCost + "/Kg per " +
                CheckProfitModel.otherDistance + " Km = Rs. " +
                MathUtils.roundDecimal(value, MathUtils.DoubleFormat.TWO_DECIMAL) +
                "/Kg per Km * " + distance + " Km)";
    }
}

