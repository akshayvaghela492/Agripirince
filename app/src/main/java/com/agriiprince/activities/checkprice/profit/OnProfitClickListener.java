package com.agriiprince.activities.checkprice.profit;

public interface OnProfitClickListener {

    void onClickItem(int position);

}
