package com.agriiprince.activities.checkprice;

import android.content.Context;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.databinding.ItemCheckPriceTextBinding;
import com.agriiprince.mvvm.retrofit.model.checkprice.GovReportedPriceData;
import com.agriiprince.utils.MathUtils;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.UnitUtils;

import java.util.ArrayList;
import java.util.List;

import static com.agriiprince.utils.UnitUtils.Weight.KG;

public class TextualGovPriceAdapter extends RecyclerView.Adapter<TextualGovPriceAdapter.TextualGovViewHolder> {

    private List<GovReportedPriceData> govReportedPrices;
    private StringUtils utils;
    private Context context;

    private GovTextListener listener;

    private UnitUtils.Weight SORT_UNIT = KG;

    public TextualGovPriceAdapter(Context context) {
        this.utils = AppController.getInstance().getStringUtils();
        this.context = context;
        govReportedPrices = new ArrayList<>();
    }

    public void setListener(GovTextListener listener) {
        this.listener = listener;
    }

    public void removeListener() {
        this.listener = null;
    }

    public void setSortUnit(UnitUtils.Weight SORT_UNIT) {
        this.SORT_UNIT = SORT_UNIT;
    }

    public void clear() {
        govReportedPrices.clear();
        notifyDataSetChanged();
    }

    public void addAllItems(List<GovReportedPriceData> list) {
        govReportedPrices.clear();
        if (list != null) govReportedPrices.addAll(list);
        notifyDataSetChanged();
    }

    public GovReportedPriceData getItem(int position) {
        return govReportedPrices.get(position);
    }

    @NonNull
    @Override
    public TextualGovViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_check_price_text, viewGroup, false);
        return new TextualGovViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TextualGovViewHolder holder, int i) {
        GovReportedPriceData govReportedPrice = govReportedPrices.get(i);

        holder.binding.textMandiLabel.setText(utils.getLocalizedString(R.string.mandi));
        holder.binding.textVarietyLabel.setText(utils.getLocalizedString(R.string.variety));
        holder.binding.textGradeLabel.setText(utils.getLocalizedString(R.string.grade));

        holder.binding.textMinLabel.setText(utils.getLocalizedString(R.string.min));
        holder.binding.textModelLabel.setText(utils.getLocalizedString(R.string.modal));
        holder.binding.textMaxLabel.setText(utils.getLocalizedString(R.string.max));
        holder.binding.textTonnageLabel.setText(utils.getLocalizedString(R.string.tonnage));

        String mandi = govReportedPrice.getMarket() + ", " + govReportedPrice.getDistrict();
        holder.binding.textMandi.setText(mandi);
        holder.binding.textVariety.setText(govReportedPrice.getVariety());
        holder.binding.textGrade.setText(govReportedPrice.getGrade());

        String min = "Rs. " + MathUtils.convertQuintalToString(govReportedPrice.getMinP(), SORT_UNIT);
        String modal = "Rs. " + MathUtils.convertQuintalToString(govReportedPrice.getModP(), SORT_UNIT);
        String max = "Rs. " + MathUtils.convertQuintalToString(govReportedPrice.getMaxP(), SORT_UNIT);
        holder.binding.textMinPrice.setText(min);
        holder.binding.textModalPrice.setText(modal);
        holder.binding.textMaxPrice.setText(max);

        String ton = govReportedPrice.getTonnage() + " Ton";
        holder.binding.textTonnage.setText(ton);

        if (govReportedPrice.isInterested()) {
            holder.binding.imageBookmark.setImageResource(R.drawable.ic_bookmark_black);
            ImageViewCompat.setImageTintList(holder.binding.imageBookmark,
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorAccent)));
        } else {
            holder.binding.imageBookmark.setImageResource(R.drawable.ic_bookmark_border);
            ImageViewCompat.setImageTintList(holder.binding.imageBookmark,
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.primaryTextColor)));
        }

        holder.binding.imageBookmark.setOnClickListener(holder.onClickBookmark);
    }

    @Override
    public int getItemCount() {
        return govReportedPrices.size();
    }

    class TextualGovViewHolder extends RecyclerView.ViewHolder {

        private ItemCheckPriceTextBinding binding;

        private TextualGovViewHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }

        private View.OnClickListener onClickBookmark = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onClickBookmark(getAdapterPosition());
            }
        };
    }

    public interface GovTextListener {
        void onClickBookmark(int position);
    }
}
