package com.agriiprince.activities.checkprice;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.agriiprince.R;
import com.agriiprince.activities.checkprice.price.APReportedPriceFragment;
import com.agriiprince.activities.checkprice.profit.AgentReportedProfitFragment;

import org.jetbrains.annotations.NotNull;

public class AgentReportedBase extends Fragment {
    public AgentReportedBase() {
        // Required empty public constructor
    }

    public static AgentReportedBase newInstance() {
        AgentReportedBase fragment = new AgentReportedBase();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private int currentOption = 0;

    private FrameLayout root;

    private CheckPriceAndroidViewModel viewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_agent_reported_base, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        root = view.findViewById(R.id.root);
        viewModel = ViewModelProviders.of(getActivity()).get(CheckPriceAndroidViewModel.class);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel.getCheckOption().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                if (integer != null && currentOption != integer) replaceFragment(integer);
            }
        });

        replaceFragment(currentOption);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (viewModel != null) viewModel.setUserVisibleFragmentIndex(1);
    }

    private void replaceFragment(int option) {
        currentOption = option;

        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (option == 0)
            fragmentTransaction.replace(root.getId(), APReportedPriceFragment.newInstance());
        else if (option == 1)
            fragmentTransaction.replace(root.getId(), AgentReportedProfitFragment.newInstance());

        fragmentTransaction.commit();
    }
}
