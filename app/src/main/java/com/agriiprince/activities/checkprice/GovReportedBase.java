package com.agriiprince.activities.checkprice;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.agriiprince.R;
import com.agriiprince.activities.checkprice.price.GovReportedPriceFragment;
import com.agriiprince.activities.checkprice.profit.GovReportedProfitFragment;

import org.jetbrains.annotations.NotNull;

public class GovReportedBase extends Fragment {

    public GovReportedBase() {
        // Required empty public constructor
    }

    public static GovReportedBase newInstance() {
        GovReportedBase fragment = new GovReportedBase();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private int currentOption = 0;

    private FrameLayout root;

    private CheckPriceAndroidViewModel viewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gov_reported_base, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        root = view.findViewById(R.id.root);
        viewModel = ViewModelProviders.of(getActivity()).get(CheckPriceAndroidViewModel.class);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        replaceFragment(currentOption);

        viewModel.getCheckOption().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                if (integer != null && currentOption != integer) replaceFragment(integer);
            }
        });

        viewModel.setUserVisibleFragmentIndex(0);

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (viewModel != null) viewModel.setUserVisibleFragmentIndex(0);
    }

    private void replaceFragment(int option) {
        currentOption = option;

        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (option == 0)
            fragmentTransaction.replace(root.getId(), GovReportedPriceFragment.newInstance());
        else if (option == 1)
            fragmentTransaction.replace(root.getId(), GovReportedProfitFragment.newInstance());

        fragmentTransaction.commit();
    }
}
