package com.agriiprince.activities.checkprice.price;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.TextUtils;

import com.agriiprince.R;
import com.agriiprince.fragments.BaseFragment;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.mvvm.model.crop.Grade;
import com.agriiprince.mvvm.model.crop.Variety;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manu Sajjan on 20-12-2018.
 */
public abstract class CheckPriceBaseFragment extends BaseFragment {

    protected final int colors[] = {
            R.color.rangeViolet,
            R.color.rangeIndigo,
            R.color.rangeBlue,
            R.color.rangeGreen,
            R.color.rangeYellow,
            R.color.rangeOrange,
            R.color.rangeRed
    };

    protected final float ZOOM_IN = 7f;

    protected final float ZOOM_OUT = 4.25f;

/*
    protected BitmapDescriptor getMarkerIcon(String color) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        return BitmapDescriptorFactory.fromResource(R.drawable.ic_place_black);
    }*/

    protected BitmapDescriptor getMarkerIcon(@ColorRes  int color) {
        if (getContext() == null) {
            return BitmapDescriptorFactory.fromResource(R.drawable.ic_place_black);
        } else {
            Drawable vectorDrawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_place_black);
            Bitmap bitmap = Bitmap.createBitmap(128, 128, Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(bitmap);
            vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            vectorDrawable.setAlpha(255);
            DrawableCompat.setTint(vectorDrawable, getResources().getColor(color));
            vectorDrawable.draw(canvas);
            return BitmapDescriptorFactory.fromBitmap(bitmap);
        }
    }

    protected abstract String getPrimaryLocale();

    protected abstract String getSecondaryLocale();

    protected double getDistance(Location position1, Location position2) {
        return position1.distanceTo(position2);
    }


    /**
     * This method is runs in asynchronously to add new {@link Variety} ALL and new {@link Grade} ALL
     * to each Crop
     *
     * @param crops List of Crop type
     */
    protected void setupCrops(List<Crop> crops) {
        new SetupCrops(crops).execute();
    }

    /**
     * this call back method is called after setup crops is complete
     */
    protected abstract void onSetupCrops();

    /**
     * crops are added with the new variety ALL and new Grade ALL
     */
    private class SetupCrops extends AsyncTask<Void, Void, Boolean> {

        List<Crop> crops;

        private SetupCrops(List<Crop> crops) {
            this.crops = crops;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            String primaryLocale = getPrimaryLocale();
            String secondaryLocale = getSecondaryLocale();

            if (TextUtils.isEmpty(secondaryLocale)) {
                secondaryLocale = primaryLocale.equals("en") ? "hi" : primaryLocale;
            } else {
                secondaryLocale = primaryLocale.equals("en") ? secondaryLocale : primaryLocale;
            }

            primaryLocale = "en";

            String primaryAll = getAllForSelectedLocale(primaryLocale);
            String secondaryAll = getAllForSelectedLocale(secondaryLocale);

            List<Grade> sets = new ArrayList<>();
            for (Crop crop : crops) {
                sets.clear();
                for (Variety variety : crop.getVarieties()) {
                    variety.addGradeAt(0, primaryAll, secondaryAll);

                    List<Grade> grades = variety.getGrades();
                    if (sets.size() == 0) {
                        sets.addAll(grades);
                    } else {
                        for (Grade grade : grades) {
                            Grade grade1 = Grade.getGradeByGradeName(sets, grade.gradeName);
                            if (grade1 == null) {
                                sets.add(grade);
                            }
                        }
                    }
                }
                Variety variety = crop.addVarietyAt(0, primaryAll, secondaryAll);
                variety.addGrades(new ArrayList<>(sets));
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            onSetupCrops();
        }
    }

    private String getAllForSelectedLocale(String locale) {
        switch (locale) {
            case "en":
                return "ALL";
            case "hi":
                return "सब";
            case "kn":
                return "ಎಲ್ಲಾ";
            default:
                return "ALL";
        }
    }
}
