package com.agriiprince.activities.checkprice;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.databinding.Bindable;
import android.support.annotation.IntRange;
import android.text.TextUtils;
import android.util.Log;

import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.dataservice.ApPricesDataService;
import com.agriiprince.dataservice.CropDataService;
import com.agriiprince.dataservice.FarmerProfileDataService;
import com.agriiprince.dataservice.GovPricesDataService;
import com.agriiprince.dataservice.MandiDataService;
import com.agriiprince.dataservice.OeProfileDataService;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.MandiModel;
import com.agriiprince.model.OeReportedPrice;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.mvvm.retrofit.model.checkprice.GovReportedPriceData;
import com.agriiprince.utils.DateUtil;
import com.agriiprince.utils.UnitUtils;
import com.android.databinding.library.baseAdapters.BR;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class CheckPriceAndroidViewModel extends BaseAndroidViewModel {

    private static final String TAG = CheckPriceAndroidViewModel.class.getSimpleName();

    private boolean isMapView = true;

    private String area = "0";
    private String yield = "0";
    private String harvestCost = "0";
    private String transportWeight = "0";
    private String transportDistance = "0";
    private String transportCost = "0";
    private String wasteDistance = "0";
    private String otherCost = "0";
    private String otherDistance = "0";
    private boolean isFixed;

    private int waste;

    private UnitUtils.Area mHarvestAreaUnit;
    private UnitUtils.Weight mHarvestWeightUnit;

    private UnitUtils.Weight mTransportationWeightUnit;
    private UnitUtils.Length mTransportationLengthUnit;

    private UnitUtils.Length mWastageLengthUnit;

    private UnitUtils.Length mOtherLengthUnit;

    private String cropName;
    private String varietyName;
    private String gradeName;

    private MutableLiveData<Integer> checkOption;

    private MutableLiveData<Boolean> isInputValueChanged;

    private MutableLiveData<Boolean> mMapView;

    private MutableLiveData<Calendar> date;

    private MutableLiveData<List<Crop>> mCrops;
    private MutableLiveData<List<MandiModel>> mMandis;

    private MutableLiveData<List<String>> mInterestedMandiIds;

    private GovPricesDataService mGovPricesDataService;
    private ApPricesDataService mApPricesDataService;

    private MutableLiveData<Integer> userVisibleFragmentIndex;

    private FarmerProfileDataService mFarmerProfileDataService;
    private OeProfileDataService mOeProfileDataService;


    private PrefManager mPrefManager;

    private UserProfile userProfile;

    public CheckPriceAndroidViewModel(Application application) {
        super(application);
        this.mPrefManager = new PrefManager(application);

        this.mCrops = new MutableLiveData<>();
        this.mMandis = new MutableLiveData<>();

        checkOption = new MutableLiveData<>();
        isInputValueChanged = new MutableLiveData<>();

        userVisibleFragmentIndex = new MutableLiveData<>();

        mMapView = new MutableLiveData<>();
        mMapView.setValue(isMapView);

        userProfile = new UserProfile(application);

        mGovPricesDataService = new GovPricesDataService();
        mApPricesDataService = new ApPricesDataService();

        mFarmerProfileDataService = new FarmerProfileDataService(application);
        mOeProfileDataService = new OeProfileDataService(application);

        date = new MutableLiveData<>();
        mInterestedMandiIds = new MutableLiveData<>();

        CropDataService cropDataService = new CropDataService(application);
        cropDataService.getCheckPriceCrops(onCropDataRequest);

        MandiDataService mandiDataService = new MandiDataService(application);
        mandiDataService.getMandis(onMandiResponse);

        setInterestedMandiIds();
    }

    @Bindable
    public boolean isMapView() {
        return isMapView;
    }

    public void setMapView(boolean mapView) {
        isMapView = mapView;
        mMapView.setValue(mapView);
        notifyPropertyChanged(BR.mapView);
    }


    public void setHarvestAreaUnit(UnitUtils.Area area) {
        mHarvestAreaUnit = (area);
    }

    public void setHarvestWeightUnit(UnitUtils.Weight weight) {
        mHarvestWeightUnit = (weight);
    }

    public void setTransportationLengthUnit(UnitUtils.Length length) {
        mTransportationLengthUnit = (length);
    }

    public void setTransportationWeightUnit(UnitUtils.Weight weight) {
        mTransportationWeightUnit = (weight);
    }

    public void setWastageLengthUnit(UnitUtils.Length length) {
        mWastageLengthUnit = (length);
    }


    public void setOtherLengthUnit(UnitUtils.Length length) {
        mOtherLengthUnit = (length);
    }

    public UnitUtils.Area getHarvestAreaUnit() {
        return mHarvestAreaUnit;
    }

    public UnitUtils.Weight getHarvestWeightUnit() {
        return mHarvestWeightUnit;
    }

    public UnitUtils.Weight getTransportationWeightUnit() {
        return mTransportationWeightUnit;
    }

    public UnitUtils.Length getTransportationLengthUnit() {
        return mTransportationLengthUnit;
    }

    public UnitUtils.Length getWastageLengthUnit() {
        return mWastageLengthUnit;
    }

    public UnitUtils.Length getOtherLengthUnit() {
        return mOtherLengthUnit;
    }

    public LiveData<Boolean> onChangeMapView() {
        return mMapView;
    }

    public LiveData<List<Crop>> getCrops() {
        return mCrops;
    }

    public LiveData<List<MandiModel>> getMandis() {
        return mMandis;
    }

    public void onChangeCheckOption(@IntRange(from = 0, to = 1) int option) {
        checkOption.setValue(option);
    }

    public LiveData<Integer> getCheckOption() {
        return checkOption;
    }

    public void onInputValueChanged() {
        isInputValueChanged.setValue(true);
    }

    public void setUserVisibleFragmentIndex(int index) {
        userVisibleFragmentIndex.setValue(index);
    }

    public LiveData<Integer> getUserVisibleFragmentIndex() {
        return userVisibleFragmentIndex;
    }

    public LiveData<Boolean> getOnInputValueChange() {
        return isInputValueChanged;
    }

    private void setInterestedMandiIds() {
        String s = mPrefManager.getInterestedMandiIds();
        String[] ids = s.replace("[", "").replace("]", "")
                .replace(" ", "").split(",");

        Log.d(TAG, "setInterestedMandiIds: " + Arrays.asList(ids));

        List<String> list = new ArrayList<>(Arrays.asList(ids));
        mInterestedMandiIds.setValue(list);
    }

    public LiveData<List<String>> getInterestedMandiIds() {
        return mInterestedMandiIds;
    }

    public void removeInterested(String id) {
        List<String> ids = mInterestedMandiIds.getValue();
        if (ids != null) {
            ids.remove(id);
            mInterestedMandiIds.setValue(ids);
            updateInterestedMandiIds(ids);
        }
    }

    public void addInterested(String id) {
        List<String> ids = mInterestedMandiIds.getValue();
        if (ids != null) {
            ids.add(id);
            mInterestedMandiIds.setValue(ids);
            updateInterestedMandiIds(ids);
        }
    }

    public LiveData<Boolean> isGovPricesAvailable() {
        return mGovPricesDataService.isDataAvailable();
    }

    public LiveData<Boolean> isApPricesAvailable() {
        return mApPricesDataService.isDataAvailable();
    }

    public LiveData<Calendar> getDate() {
        return date;
    }

    public void setDate(Calendar calendar) {
        date.setValue(calendar);
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public void setVarietyName(String varietyName) {
        this.varietyName = varietyName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public void getPrices() {
        if (isValid() && date.getValue() != null) {
            mGovPricesDataService.getPrices(userProfile.getApiToken(), userProfile.getUserId(),
                    DateUtil.getServerDate(date.getValue()), cropName, varietyName, gradeName);

            mApPricesDataService.getPrices(userProfile.getApiToken(), userProfile.getUserId(),
                    DateUtil.getServerDate(date.getValue()), cropName, varietyName, gradeName);
        }
    }


    public LiveData<Boolean> isGovDataLoading() {
        return mGovPricesDataService.isLoading();
    }

    public LiveData<Boolean> isApDataLoading() {
        return mApPricesDataService.isLoading();
    }

    public LiveData<List<GovReportedPriceData>> onFetchGovPrices() {
        return mGovPricesDataService.getGovPrices();
    }

    public LiveData<List<OeReportedPrice>> onFetchAgentPrices() {
        return mApPricesDataService.getApPrices();
    }

    private void updateInterestedMandiIds(List<String> ids) {
        String userType = mPrefManager.getUserType();
        if (userType.equalsIgnoreCase("farmer")) {
            mFarmerProfileDataService.updateInterestMandiIds(ids);
        } else if (userType.equalsIgnoreCase("oe")) {
            mOeProfileDataService.updateInterestedMandiIds(ids);
        }
    }

    private boolean isValid() {
        return !TextUtils.isEmpty(cropName) && !TextUtils.isEmpty(varietyName) && !TextUtils.isEmpty(gradeName);
    }

    private CropDataService.OnCropDataRequest onCropDataRequest = new CropDataService.OnCropDataRequest() {
        @Override
        public void onCropDataResults(List<Crop> crops) {
            mCrops.setValue(crops);
        }

        @Override
        public void onCropDataError() {

        }
    };

    private MandiDataService.OnMandiResponse onMandiResponse = new MandiDataService.OnMandiResponse() {
        @Override
        public void onMandiSuccess(List<MandiModel> mandies) {
            mMandis.setValue(mandies);
        }

        @Override
        public void onMandiFailure() {

        }
    };


    @Bindable
    public String getArea() {
        return TextUtils.isEmpty(area) ? "0" : area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Bindable
    public String getYield() {
        return TextUtils.isEmpty(yield) ? "0" : yield;
    }

    public void setYield(String yield) {
        this.yield = yield;
    }

    @Bindable
    public String getHarvestCost() {
        return harvestCost;
    }

    public void setHarvestCost(String harvestCost) {
        this.harvestCost = harvestCost;
    }

    @Bindable
    public String getTransportWeight() {
        return transportWeight;
    }

    @Bindable
    public void setTransportWeight(String transportWeight) {
        this.transportWeight = transportWeight;
    }

    public String getTransportDistance() {
        return transportDistance;
    }

    @Bindable
    public void setTransportDistance(String transportDistance) {
        this.transportDistance = transportDistance;
    }

    @Bindable
    public String getTransportCost() {
        return TextUtils.isEmpty(transportCost) ? "0" : transportCost;
    }

    @Bindable
    public void setTransportCost(String transportCost) {
        this.transportCost = transportCost;
    }

    @Bindable
    public int getWaste() {
        return waste;
    }

    public void setWaste(int waste) {
        this.waste = waste;
    }

    @Bindable
    public String getWasteDistance() {
        return wasteDistance;
    }

    public void setWasteDistance(String wasteDistance) {
        this.wasteDistance = wasteDistance;
    }

    @Bindable
    public String getOtherCost() {
        return otherCost;
    }

    public void setOtherCost(String otherCost) {
        this.otherCost = otherCost;
    }

    @Bindable
    public String getOtherDistance() {
        return otherDistance;
    }

    public void setOtherDistance(String otherDistance) {
        this.otherDistance = otherDistance;
    }

    @Bindable
    public boolean isFixed() {
        return isFixed;
    }

    public void setFixed(boolean fixed) {
        isFixed = fixed;
        notifyPropertyChanged(BR.fixed);
    }
}
