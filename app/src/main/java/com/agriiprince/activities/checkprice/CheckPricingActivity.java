package com.agriiprince.activities.checkprice;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.DatePicker;

import com.agriiprince.R;
import com.agriiprince.activities.TutorialActivity;
import com.agriiprince.activities.checkprice.profit.CheckProfitModel;
import com.agriiprince.adapter.CustomStringArrayAdapter;
import com.agriiprince.adapter.ViewPagerAdapter;
import com.agriiprince.mvvm.applevel.analytics.FarmerRealtimeDBAnalytics;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.ActivityCheckPriceBinding;
import com.agriiprince.mvvm.ui.customwidgets.DatePickerDialog;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.mvvm.model.crop.Grade;
import com.agriiprince.mvvm.model.crop.Variety;
import com.agriiprince.utils.DateUtil;
import com.agriiprince.utils.MathUtils;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.UnitUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.agriiprince.utils.UnitUtils.Area.ACRE;
import static com.agriiprince.utils.UnitUtils.Area.BIGHA;
import static com.agriiprince.utils.UnitUtils.Area.HECTARE;
import static com.agriiprince.utils.UnitUtils.Length.KM;
import static com.agriiprince.utils.UnitUtils.Length.METERS;
import static com.agriiprince.utils.UnitUtils.Weight.KG;
import static com.agriiprince.utils.UnitUtils.Weight.QUINTAL;
import static com.agriiprince.utils.UnitUtils.Weight.TON;


public class CheckPricingActivity extends AppCompatActivity {

    public static final String TAG = CheckPricingActivity.class.getSimpleName();

    public static final String KEY_TIME_MILLI = "time";
    public static final String KEY_CROP_NAME = "crop_name";
    public static final String KEY_CROP_VARIETY = "crop_variety";
    public static final String KEY_CROP_GRADE = "crop_grade";

    private StringUtils utils;

    private boolean isCheckPrice = true;

    private boolean isSelectionChanged = false;

    private String cropName, varietyName, gradeName;
    private long timeInMillis;

    private Calendar calendar;

    private Crop selectedCrop;

    private int mSelectedCrop = 0;
    private int mSelectedVariety = 0;
    private int mSelectedGrade = 0;

    private List<Crop> mCrops;

    private PrefManager prefManager;

    private AlertDialog mAlertDialog;
    private CheckBox mCb;
    //private int mShow_Dailof_Flag = 1;

    UserProfile mUserProfile;
    FirebaseDatabase mFDatabase;
    DatabaseReference mRef;
    DatabaseReference mFarmersRef;
    DatabaseReference mNumRef;
    DatabaseReference mKeyRef;
    long mTimeStart;
    String mUserId;

    private ActivityCheckPriceBinding binding;

    private CheckPriceAndroidViewModel viewModel;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_check_price);
        binding.executePendingBindings();

        prefManager = new PrefManager(this);
        mUserProfile = new UserProfile(this);
        mUserId = mUserProfile.getUserId();
        mFDatabase = FirebaseDatabase.getInstance();
        mRef = mFDatabase.getReference("analytics");
        mFarmersRef = mRef.child("farmers");
        mNumRef = mFarmersRef.child(mUserProfile.getUserMobile());

        utils = AppController.getInstance().getStringUtils();

        viewModel = ViewModelProviders.of(this).get(CheckPriceAndroidViewModel.class);
        binding.setViewModel(viewModel);
        binding.selection.setViewModel(viewModel);

        calendar = Calendar.getInstance();
        viewModel.setDate(calendar);
        setDateTextView();

        binding.imageFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: filter");
                if (binding.selectionRoot.getVisibility() == View.VISIBLE) {
                    //hideSearchView();
                    Intent intent=new Intent(CheckPricingActivity.this, TutorialActivity.class);
                    intent.putExtra("code", "CHECP");
                    startActivity(intent);
                } else {
                    openSearchView();
                }
            }
        });

        binding.selection.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSearchView();
                formatProfitInput();

                if (isSelectionChanged) getPrices();

                if(!isCheckPrice && prefManager.getShowDialogFlag() == 1)
                    showCustomDialog();
            }
        });

        openSearchView();

        setupTabLayout();
        setUpViewPager();

        setTexts();
        setSelectionAdapters();
        restoreSelection();

        setListeners();
        setObservables();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_check_profit, viewGroup, false);

        mCb = (CheckBox) dialogView.findViewById(R.id.dont_show_checkbox);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        mAlertDialog = builder.create();
        mAlertDialog.show();
    }

    private void setTexts() {
        binding.checkPriceLabel.setText(utils.getLocalizedString(R.string.check_prices_profit_title));
        binding.selectionLabel.cropLabel.setText(utils.getLocalizedString(R.string.crop, true));
        binding.selectionLabel.varietyLabel.setText(utils.getLocalizedString(R.string.variety, true));
        binding.selectionLabel.gradeLabel.setText(utils.getLocalizedString(R.string.grade, true));
        binding.selection.cropSelectLabel.setText(utils.getLocalizedString(R.string.crop, true));
        binding.selection.varietySelectLabel.setText(utils.getLocalizedString(R.string.variety, true));
        binding.selection.gradeSelectLabel.setText(utils.getLocalizedString(R.string.grade, true));

        binding.selection.acCropSelect.setHint(utils.getLocalizedString(R.string.select_crop, true));

        binding.selection.optionLabel.setText(utils.getLocalizedString(R.string.choose_one_of_following_option));
        binding.selection.optionCheckPrice.setText(utils.getLocalizedString(R.string.check_prices));
        binding.selection.optionCheckProfit.setText(utils.getLocalizedString(R.string.check_profit));

        binding.selection.harvestingLoadingLabel.setText(utils.getLocalizedString(R.string.cost_of_harvesting_loading));
        binding.selection.areaCultivationLabel.setText(utils.getLocalizedString(R.string.area_of_cultivation));
        binding.selection.expectedYieldLabel.setText(utils.getLocalizedString(R.string.expected_yield));
        binding.selection.harvestingCostLabel.setText(utils.getLocalizedString(R.string.cost));

        binding.selection.transportationLabel.setText(utils.getLocalizedString(R.string.cost_of_transportation));
        binding.selection.weightLabel.setText(utils.getLocalizedString(R.string.weight));
        binding.selection.distanceLabel.setText(utils.getLocalizedString(R.string.distance));
        binding.selection.transportationCostLabel.setText(utils.getLocalizedString(R.string.cost));

        binding.selection.expectedWastageLabel.setText(utils.getLocalizedString(R.string.expected_wastage));
        binding.selection.wasteLabel.setText(utils.getLocalizedString(R.string.waste));
        binding.selection.wastageDistanceLabel.setText(utils.getLocalizedString(R.string.distance));

        binding.selection.otherLabel.setText(utils.getLocalizedString(R.string.other_costs));
        binding.selection.otherCostLabel.setText(utils.getLocalizedString(R.string.cost));
        binding.selection.otherFixed.setText(utils.getLocalizedString(R.string.fixed_charge, true));
        binding.selection.otherDistanceLabel.setText(utils.getLocalizedString(R.string.distance));

        binding.selection.submit.setText(utils.getLocalizedString(R.string.submit, true));
    }

    private void setSelectionAdapters() {

        List<String> areas = new ArrayList<>(Arrays.asList(UnitUtils.area));
        ArrayAdapter<String> areaAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, areas);
        areaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        List<String> weights = new ArrayList<>(Arrays.asList(UnitUtils.weight));
        ArrayAdapter<String> weightAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, weights);
        weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        List<String> lengths = new ArrayList<>(Arrays.asList(UnitUtils.lenght));
        ArrayAdapter<String> lengthAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, lengths);
        lengthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        List<String> wastages = new ArrayList<>(UnitUtils.getWastages());
        ArrayAdapter<String> wastageAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, wastages);
        wastageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        binding.selection.cultivationAreaSpinner.setAdapter(areaAdapter);
        binding.selection.cultivationAreaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                UnitUtils.Area area = ACRE;
                switch (position) {
                    case 0:
                        area = ACRE;
                        break;
                    case 1:
                        area = HECTARE;
                        break;
                    case 2:
                        area = BIGHA;
                        break;
                }
                viewModel.setHarvestAreaUnit(area);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        binding.selection.cultivationAreaSpinner.setSelection(prefManager.getHarvestAreaUnit());


        binding.selection.yieldSpinner.setAdapter(weightAdapter);
        binding.selection.yieldSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                UnitUtils.Weight weight = KG;
                switch (position) {
                    case 0:
                        weight = KG;
                        break;
                    case 1:
                        weight = QUINTAL;
                        break;
                    case 2:
                        weight = TON;
                        break;
                }
                viewModel.setHarvestWeightUnit(weight);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.selection.yieldSpinner.setSelection(prefManager.getHarvestYieldUnit());


        binding.selection.transportationWeightSpinner.setAdapter(weightAdapter);
        binding.selection.transportationWeightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                UnitUtils.Weight weight = KG;
                switch (position) {
                    case 0:
                        weight = KG;
                        break;
                    case 1:
                        weight = QUINTAL;
                        break;
                    case 2:
                        weight = TON;
                        break;
                }
                viewModel.setTransportationWeightUnit(weight);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.selection.transportationWeightSpinner.setSelection(prefManager.getTransportWeightUnit());


        binding.selection.transportationDistanceSpinner.setAdapter(lengthAdapter);
        binding.selection.transportationDistanceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                UnitUtils.Length length = KM;
                switch (position) {
                    case 0:
                        length = KM;
                        break;
                    case 1:
                        length = METERS;
                        break;
                }
                viewModel.setTransportationLengthUnit(length);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.selection.transportationDistanceSpinner.setSelection(prefManager.getTransportDistanceUnit());


        binding.selection.wastageDistanceSpinner.setAdapter(lengthAdapter);
        binding.selection.wastageDistanceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                UnitUtils.Length length = KM;
                switch (position) {
                    case 0:
                        length = KM;
                        break;
                    case 1:
                        length = METERS;
                        break;
                }
                viewModel.setWastageLengthUnit(length);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.selection.wastageDistanceSpinner.setSelection(prefManager.getWasteDistanceUnit());



        binding.selection.wastagePercentSpinner.setAdapter(wastageAdapter);
        binding.selection.wastagePercentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int waste = position * 5;
                viewModel.setWaste(waste);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.selection.wastagePercentSpinner.setSelection(prefManager.getWastePercentIndex());

        binding.selection.otherDistanceSpinner.setAdapter(lengthAdapter);
        binding.selection.otherDistanceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                UnitUtils.Length length = KM;
                switch (position) {
                    case 0:
                        length = KM;
                        break;
                    case 1:
                        length = METERS;
                        break;
                }
                viewModel.setOtherLengthUnit(length);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.selection.otherDistanceSpinner.setSelection(prefManager.getOtherDistanceUnit());
    }

    private void setupTabLayout() {
        binding.tab1.setText(utils.getLocalizedString(R.string.gov_reported));
        binding.tab2.setText(utils.getLocalizedString(R.string.ap_agent_reported));

        binding.tab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.imageTab1.setColorFilter(getResources().getColor(R.color.loginBtn1));
                binding.imageTab2.setColorFilter(getResources().getColor(R.color.backgroundLight));
                binding.tab1.setTextColor(getResources().getColor(R.color.white));
                binding.tab2.setTextColor(getResources().getColor(R.color.primaryTextColor));

                binding.viewPager.setCurrentItem(0);
            }
        });

        binding.tab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.imageTab2.setColorFilter(getResources().getColor(R.color.loginBtn1));
                binding.imageTab1.setColorFilter(getResources().getColor(R.color.backgroundLight));
                binding.tab2.setTextColor(getResources().getColor(R.color.white));
                binding.tab1.setTextColor(getResources().getColor(R.color.primaryTextColor));

                binding.viewPager.setCurrentItem(1);
            }
        });

        binding.tab1.performClick();
    }

    private void setUpViewPager() {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        viewPagerAdapter.addFragment(GovReportedBase.newInstance(),
                utils.getLocalizedString(R.string.gov_reported).toString());
        viewPagerAdapter.addFragment(AgentReportedBase.newInstance(),
                utils.getLocalizedString(R.string.our_agent_reported).toString());

        binding.viewPager.setAdapter(viewPagerAdapter);
    }


    private void openSearchView() {
        binding.tabLayout.setAlpha(0.5f);
        binding.tabLayout.setClickable(false);
        binding.tabLayout.setEnabled(false);
        binding.tabLayout.setFocusable(false);

        binding.layoutLabel.setVisibility(View.GONE);

        binding.selectionRoot.setVisibility(View.VISIBLE);
        binding.checkPriceLabel.setVisibility(View.VISIBLE);

        binding.switchLayout.setVisibility(View.GONE);
        binding.dividerSwitch.setVisibility(View.GONE);
        binding.viewPager.setVisibility(View.GONE);

        binding.edit.setImageResource(R.drawable.ic_info_i);

        //binding.edit.setImageResource(R.drawable.ic_clear_black_24dp);

        binding.tab.setBackgroundColor(getResources().getColor(R.color.white));

        if (binding.selection.optionCheckPrice.isChecked()) onCheckPriceOptionSelect();
        else onCheckProfitOptionSelect();

    }

    private void hideSearchView() {
        binding.tabLayout.setAlpha(1f);
        binding.tabLayout.setClickable(true);
        binding.tabLayout.setEnabled(true);
        binding.tabLayout.setFocusable(true);

        binding.layoutLabel.setVisibility(View.VISIBLE);
        binding.selectionRoot.setVisibility(View.GONE);
        binding.checkPriceLabel.setVisibility(View.GONE);

        binding.dividerSwitch.setVisibility(View.VISIBLE);
        binding.switchLayout.setVisibility(View.VISIBLE);
        binding.viewPager.setVisibility(View.VISIBLE);
        binding.edit.setImageResource(R.drawable.ic_edit_white_24dp);

        binding.tab.setBackgroundColor(getResources().getColor(R.color.colorPrimaryFarmer));
    }

    private void onCheckPriceOptionSelect() {
        binding.selection.checkProfitForm.setVisibility(View.GONE);
    }

    private void onCheckProfitOptionSelect() {
        binding.selection.checkProfitForm.setVisibility(View.VISIBLE);
    }

    private void formatProfitInput() {
        double unitConverter = 1;
        if (viewModel.getHarvestWeightUnit() == QUINTAL) unitConverter = 100;
        else if (viewModel.getHarvestWeightUnit() == TON) unitConverter = 1000;

        if (TextUtils.isEmpty(viewModel.getYield())) CheckProfitModel.yield = 0;
        else CheckProfitModel.yield = (int) (Integer.parseInt(viewModel.getYield()) * unitConverter);

        if (TextUtils.isEmpty(viewModel.getArea())) CheckProfitModel.area = 0;
        else CheckProfitModel.area = Double.parseDouble(viewModel.getArea());

        CheckProfitModel.totalYield = (int) (CheckProfitModel.yield * CheckProfitModel.area);

        if (TextUtils.isEmpty(viewModel.getHarvestCost())) CheckProfitModel.harvestCost = 0;
        else CheckProfitModel.harvestCost = Integer.parseInt(viewModel.getHarvestCost());

        unitConverter = 1;
        if (viewModel.getTransportationWeightUnit() == QUINTAL) unitConverter = 100;
        else if (viewModel.getTransportationWeightUnit() == TON) unitConverter = 1000;

        if (TextUtils.isEmpty(viewModel.getTransportWeight())) CheckProfitModel.transportWeight = 0;
        else CheckProfitModel.transportWeight = (int) (Integer.parseInt(viewModel.getTransportWeight()) * unitConverter);

        unitConverter = 1;
        if (viewModel.getTransportationLengthUnit() == METERS) unitConverter = 1 / 1000d;
        if (TextUtils.isEmpty(viewModel.getTransportDistance())) CheckProfitModel.transportDistance = 0;
        else CheckProfitModel.transportDistance = (int) (Integer.parseInt(viewModel.getTransportDistance()) * unitConverter);

        if (TextUtils.isEmpty(viewModel.getTransportCost())) CheckProfitModel.transportCost = 0;
        else CheckProfitModel.transportCost = Double.parseDouble(viewModel.getTransportCost());

        CheckProfitModel.wastePercent = viewModel.getWaste();

        unitConverter = 1;
        if (viewModel.getWastageLengthUnit() == METERS) unitConverter = 1 / 1000d;
        if (TextUtils.isEmpty(viewModel.getWasteDistance())) CheckProfitModel.wasteDistance = 0;
        else CheckProfitModel.wasteDistance = (int) (Integer.parseInt(viewModel.getWasteDistance()) * unitConverter);

        CheckProfitModel.fixed = viewModel.isFixed();
        if (TextUtils.isEmpty(viewModel.getOtherCost())) CheckProfitModel.otherCost = 0;
        else CheckProfitModel.otherCost = Double.parseDouble(viewModel.getOtherCost());

        unitConverter = 1;
        if (viewModel.getWastageLengthUnit() == METERS) unitConverter = 1 / 1000d;
        if (TextUtils.isEmpty(viewModel.getOtherDistance())) CheckProfitModel.otherDistance = 0;
        else CheckProfitModel.otherDistance = (int) (Integer.parseInt(viewModel.getOtherDistance()) * unitConverter);

        saveSelection();
        viewModel.onInputValueChanged();
    }

    private void saveSelection() {
        prefManager.setHarvestAreaUnit(binding.selection.cultivationAreaSpinner.getSelectedItemPosition());
        prefManager.setHarvestArea(viewModel.getArea());
        prefManager.setHarvestYieldUnit(binding.selection.yieldSpinner.getSelectedItemPosition());
        prefManager.setHarvestYield(viewModel.getYield());
        prefManager.setHarvestCost(viewModel.getHarvestCost());

        prefManager.setTransportWeightUnit(binding.selection.transportationWeightSpinner.getSelectedItemPosition());
        prefManager.setTransportWeight(viewModel.getTransportWeight());
        prefManager.setTransportDistanceUnit(binding.selection.transportationDistanceSpinner.getSelectedItemPosition());
        prefManager.setTransportDistance(viewModel.getTransportDistance());
        prefManager.setTransportCost(viewModel.getTransportCost());

        prefManager.setWastePercentIndex(binding.selection.wastagePercentSpinner.getSelectedItemPosition());
        prefManager.setWasteDistanceUnit(binding.selection.wastageDistanceSpinner.getSelectedItemPosition());
        prefManager.setWasteDistance(viewModel.getWasteDistance());

        prefManager.setOtherFixed(viewModel.isFixed());
        prefManager.setOtherCost(viewModel.getOtherCost());
        prefManager.setOtherDistanceUnit(binding.selection.otherDistanceSpinner.getSelectedItemPosition());
        prefManager.setOtherDistance(viewModel.getOtherDistance());
    }

    private void restoreSelection() {
        binding.selection.cultivationArea.setText(prefManager.getHarvestArea());
        binding.selection.yield.setText(prefManager.getHarvestYield());
        binding.selection.harvestingCost.setText(prefManager.getHarvestCost());

        binding.selection.transportWeight.setText(prefManager.getTransportWeight());
        binding.selection.distance.setText(prefManager.getTransportDistance());
        binding.selection.transportationCost.setText(prefManager.getTransportCost());

        binding.selection.wastageDistance.setText(prefManager.getWasteDistance());

        viewModel.setFixed(prefManager.getOtherFixed());
        binding.selection.otherCost.setText(prefManager.getOtherCost());
        binding.selection.otherDistance.setText(prefManager.getOtherDistance());
    }

    private void setListeners() {
        binding.prevDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPrevDate();
            }
        });

        binding.nextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNextDate();
            }
        });

        binding.date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(CheckPricingActivity.this, onDateSetListener, calendar);
                datePickerDialog.setButton(DatePickerDialog.BUTTON_POSITIVE, getString(R.string.okay), datePickerDialog);
                datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                datePickerDialog.show();
            }
        });

        binding.selection.optionCheckPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCheckPrice = true;
                onCheckPriceOptionSelect();

                binding.selection.optionCheckPrice.setChecked(true);
                binding.selection.optionCheckProfit.setChecked(false);

                binding.selection.optionCheckPrice.setTextColor(getResources().getColor(R.color.white));
                binding.selection.optionCheckProfit.setTextColor(getResources().getColor(R.color.primaryTextColor));

                viewModel.onChangeCheckOption(0);
            }
        });

        binding.selection.optionCheckProfit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCheckPrice = false;
                onCheckProfitOptionSelect();

                binding.selection.optionCheckProfit.setChecked(true);
                binding.selection.optionCheckPrice.setChecked(false);

                binding.selection.optionCheckProfit.setTextColor(getResources().getColor(R.color.white));
                binding.selection.optionCheckPrice.setTextColor(getResources().getColor(R.color.primaryTextColor));

                viewModel.onChangeCheckOption(1);
            }
        });

    }

    private DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(year, month, dayOfMonth);
            getGovPrices();
        }
    };

    private void setObservables() {
        viewModel.getCrops().observe(this, new Observer<List<Crop>>() {
            @Override
            public void onChanged(@Nullable List<Crop> crops) {
                mCrops = crops;
                setAdaptersDefault();
                getPricesForIntentArguments();
            }
        });

        viewModel.isGovDataLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean != null) {
                    if (aBoolean) showProgressDialog();
                    else dismissProgressDialog();
                }
            }
        });
    }

    private void setNextDate() {
        if (isDateBeforeToday(calendar)) {
            calendar.add(Calendar.DATE, 1);
            getGovPrices();
        }

    }

    private void setPrevDate() {
        calendar.add(Calendar.DATE, -1);
        getGovPrices();
    }

    private void setDateTextView() {
        SimpleDateFormat outDateFormat = new SimpleDateFormat("d MMM yyyy", Locale.ENGLISH);
        String date = outDateFormat.format(calendar.getTime());
        binding.date.setText(date);
    }

    private boolean isDateBeforeToday(Calendar calendar) {
        if (calendar == null) return false;
        return DateUtil.isDateBeforeToday(calendar);
    }


    protected void hideSoftInputLayout(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null && view != null)
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void setAdaptersDefault() {
        setCommodityNameAdapter();

        List<String> varietyLabel = new ArrayList<>();
        varietyLabel.add(utils.getLocalizedString(R.string.select_variety, true).toString());
        setCommodityVarietyAdapter(varietyLabel);

        List<String> gradeLabel = new ArrayList<>();
        gradeLabel.add(utils.getLocalizedString(R.string.select_grade, true).toString());
        setCommodityGradeAdapter(gradeLabel);
    }

    private void setCommodityNameAdapter() {
        List<String> cropNames = Crop.getBilingualNames(utils, mCrops);

        CustomStringArrayAdapter commodityNameAdapter =
                new CustomStringArrayAdapter(this, android.R.layout.simple_list_item_1, cropNames);

        binding.selection.acCropSelect.setThreshold(0);
        binding.selection.acCropSelect.setAdapter(commodityNameAdapter);
        binding.selection.acCropSelect.setValidator(commodityNameValidator);

        binding.selection.acCropSelect.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) hideSoftInputLayout(v);
            }
        });

        binding.selection.acCropSelect.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedCropName = (String) parent.getItemAtPosition(position);
                for (int i = 0; i < mCrops.size(); i++) {
                    Crop crop = mCrops.get(i);
                    if (utils.getBilingualStringForApi(crop.cropName, crop.cropName_bi).equals(selectedCropName)) {
                        selectedCrop = crop;
                        mSelectedCrop = i;
                        viewModel.setCropName(selectedCrop.cropName);
                        binding.selection.spinnerVarietySelect.setEnabled(true);

                        break;
                    }
                }
                setCommodityVarietyAdapter();
            }
        });
    }

    private void setCommodityVarietyAdapter() {
        List<String> mVarieties = mCrops.get(mSelectedCrop).getBilingualVarietyNames(this);
        setCommodityVarietyAdapter(mVarieties);
    }

    private void setCommodityVarietyAdapter(List<String> varieties) {
        CustomStringArrayAdapter commodityVarietyAdapter =
                new CustomStringArrayAdapter(this, android.R.layout.simple_spinner_item, varieties);

        commodityVarietyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.selection.spinnerVarietySelect.setAdapter(commodityVarietyAdapter);
        binding.selection.spinnerVarietySelect.setSelection(0);

        if (selectedCrop != null && mSelectedVariety != 0 && selectedCrop.getVarieties().size() > mSelectedVariety) {
            int index = varieties.indexOf(selectedCrop.getVariety(mSelectedVariety).varietyName);
            if (index == -1) {
                int median = MathUtils.medianIndex(varieties.size());
                if (median != -1) {
                    mSelectedVariety = median;
                    viewModel.setVarietyName(varieties.get(median));
                    binding.selection.spinnerVarietySelect.setSelection(median);
                } else {
                    binding.selection.spinnerVarietySelect.setSelection(0);
                }
            } else {
                binding.selection.spinnerVarietySelect.setSelection(index);
            }
        } else {
            binding.selection.spinnerVarietySelect.setSelection(0);
        }

        binding.selection.spinnerVarietySelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (selectedCrop != null) {
                    mSelectedVariety = position;
                    Variety variety = selectedCrop.getVariety(mSelectedVariety);
                    viewModel.setVarietyName(variety.varietyName);
                    setCommodityGradeAdapter();
                    binding.selection.spinnerGradeSelect.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setCommodityGradeAdapter() {
        List<String> grades = mCrops.get(mSelectedCrop).getVariety(mSelectedVariety).getBilingualGradeNames(this);
        setCommodityGradeAdapter(grades);
    }

    private void setCommodityGradeAdapter(List<String> grades) {
        CustomStringArrayAdapter adapterCommGrade =
                new CustomStringArrayAdapter(this, android.R.layout.simple_spinner_item, grades);

        adapterCommGrade.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.selection.spinnerGradeSelect.setAdapter(adapterCommGrade);

        if (selectedCrop != null && mSelectedGrade != 0 &&
                selectedCrop.getVariety(mSelectedVariety).getGrades().size() > mSelectedGrade ) {
            int index = grades.indexOf(selectedCrop.getVariety(mSelectedVariety).getGrade(mSelectedGrade).gradeName);
            if (index == -1) {
                int median = MathUtils.medianIndex(grades.size());
                if (median != -1) {
                    mSelectedGrade = median;
                    viewModel.setGradeName(grades.get(median));
                    binding.selection.spinnerGradeSelect.setSelection(median);
                } else {
                    binding.selection.spinnerGradeSelect.setSelection(0);
                }
            } else {
                binding.selection.spinnerGradeSelect.setSelection(index);
            }
        } else {
            binding.selection.spinnerGradeSelect.setSelection(0);
        }

        binding.selection.spinnerGradeSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (selectedCrop != null) {
                    mSelectedGrade = position;
                    isSelectionChanged = true;
                    Grade grade = selectedCrop.getVariety(mSelectedVariety).getGrade(mSelectedGrade);
                    viewModel.setGradeName(grade.gradeName);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getPricesForIntentArguments() {
        Intent intent = getIntent();
        if (intent.hasExtra(KEY_TIME_MILLI)) {
            timeInMillis = intent.getLongExtra(KEY_TIME_MILLI, Calendar.getInstance().getTimeInMillis());
            cropName = intent.getStringExtra(KEY_CROP_NAME);
            varietyName = intent.getStringExtra(KEY_CROP_VARIETY);
            gradeName = intent.getStringExtra(KEY_CROP_GRADE);

            Log.d(TAG, "getPricesForIntentArguments: " + cropName + " " + varietyName + " " + gradeName);

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(timeInMillis);
            viewModel.setDate(calendar);

            viewModel.setCropName(cropName);
            viewModel.setVarietyName(varietyName);
            viewModel.setGradeName(gradeName);

            binding.selectionLabel.cropName.setText(cropName);
            binding.selectionLabel.varietyName.setText(varietyName);
            binding.selectionLabel.gradeName.setText(gradeName);

            hideSearchView();

            viewModel.getPrices();
        }
    }

    public void dialog_ok(View view) {
        if(mCb.isChecked()) prefManager.setShowDialogFlag(0);
        mAlertDialog.hide();
    }

    private class GetCropForIntent extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(timeInMillis);

            selectedCrop = Crop.getCropByCropName(mCrops, cropName);
            if (selectedCrop != null) {
                Variety variety = Variety.getVarietyByVarietyName(selectedCrop.getVarieties(), varietyName);
                if (variety != null) {
                    mSelectedVariety = selectedCrop.getVarieties().indexOf(variety);
                    Grade grade = Grade.getGradeByGradeName(
                            selectedCrop.getVariety(mSelectedVariety).getGrades(), gradeName);
                    if (grade != null) {
                        mSelectedGrade = selectedCrop.getVariety(mSelectedVariety).getGrades().indexOf(grade);

                        viewModel.setDate(calendar);

                        return true;
                    }
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                String name = utils.getBilingualStringForApi(selectedCrop.cropName, selectedCrop.cropName_bi);
                binding.selection.acCropSelect.setText(name);
            }
        }
    }

    private void getPrices() {
        setSelectionLabels();
        isSelectionChanged = false;

        viewModel.getPrices();
        hideSoftInputLayout(getCurrentFocus());
    }

    private void getGovPrices() {
        setDateTextView();
        viewModel.setDate(calendar);
        viewModel.getPrices();
    }

    private void setSelectionLabels() {
        binding.selectionLabel.cropName.setText(utils.getBilingualString(selectedCrop.cropName, selectedCrop.cropName_bi));
        Variety variety = selectedCrop.getVariety(mSelectedVariety);
        binding.selectionLabel.varietyName.setText(utils.getBilingualString(variety.varietyName, variety.varietyName_bi));
        Grade grade = variety.getGrade(mSelectedGrade);
        binding.selectionLabel.gradeName.setText(utils.getBilingualString(grade.gradeName, grade.gradeName_bi));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTimeStart = System.currentTimeMillis();
    }

    @Override
    protected void onStop() {
        super.onStop();
        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - mTimeStart;
        final int elapsedSeconds = (int)(tDelta / 1000.0);

        Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    FarmerRealtimeDBAnalytics.saveEventFarmerTimeSpent(
                            dataSnapshot, "time_spent_check_prices", mNumRef, elapsedSeconds);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private AutoCompleteTextView.Validator commodityNameValidator = new AutoCompleteTextView.Validator() {
        @Override
        public boolean isValid(CharSequence text) {
            return Crop.getNames(mCrops).indexOf(text.toString()) != -1;
        }

        @Override
        public CharSequence fixText(CharSequence invalidText) {
            ArrayAdapter<String> commodityAdapter = (ArrayAdapter<String>) binding.selection.acCropSelect.getAdapter();

            commodityAdapter.getFilter().filter(invalidText);
            if (commodityAdapter.getCount() > 0) {
                return commodityAdapter.getItem(0);
            } else {
                return invalidText;
            }
        }
    };

    protected void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.please_wait));
        }

        if (!mProgressDialog.isShowing()) {
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
    }

    protected void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }
}
