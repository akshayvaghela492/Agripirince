package com.agriiprince.activities.checkprice.price;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.activities.checkprice.CheckPriceAndroidViewModel;
import com.agriiprince.activities.checkprice.SortBy;
import com.agriiprince.activities.checkprice.TextualGovPriceAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.FragmentGovReportedPriceBinding;
import com.agriiprince.databinding.ItemCheckPriceTextBinding;
import com.agriiprince.helper.UserLocation;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.GovReportedPrice;
import com.agriiprince.model.MandiModel;
import com.agriiprince.helper.PriceDistribution;
import com.agriiprince.mvvm.retrofit.model.checkprice.GovReportedPriceData;
import com.agriiprince.utils.MathUtils;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.UnitUtils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.agriiprince.utils.UnitUtils.Weight.KG;
import static com.agriiprince.utils.UnitUtils.Weight.QUINTAL;
import static com.agriiprince.utils.UnitUtils.Weight.TON;

/**
 * A simple {@link Fragment} subclass.
 */
public class GovReportedPriceFragment extends CheckPriceBaseFragment implements OnMapReadyCallback {

    private static final String TAG = GovReportedPriceFragment.class.getSimpleName();

    private final int REQUEST_CODE_FORM = 1111;

    private int SORT_MODE = SortBy.MODAL_PRICE;
    private UnitUtils.Weight SORT_UNIT = KG;

    private List<MandiModel> mandis;

    private List<String> interestedMandiIds;

    private Calendar calendar;

    private SupportMapFragment mMapFragment;
    private GoogleMap mGoogleMap;

    private UserProfile mUserProfile;

    private PrefManager prefManager;

    private SimpleDateFormat apiDateFormat;

    private List<GovReportedPriceData> mGovReports;
    private List<GovReportedPriceData> mTextList;

    private ConstraintLayout clBottom;

    private Location userLocation;
    private Location nearestMandiLocation;

    private StringUtils utils;

    private TextualGovPriceAdapter adapter;

    private CheckPriceAndroidViewModel viewModel;

    private FragmentGovReportedPriceBinding binding;

    private PriceDistribution<GovReportedPriceData> mPriceDistribution;
    private boolean showNoDataDialog = false;

    public GovReportedPriceFragment() {

    }

    @Override
    protected void onSetupCrops() {

    }

    public static GovReportedPriceFragment newInstance() {
        GovReportedPriceFragment fragment = new GovReportedPriceFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mGovReports = new ArrayList<>();
        mTextList = new ArrayList<>();
        mPriceDistribution = new PriceDistribution<>(mGovReports, distributionComparator, distributeListener);
        utils = AppController.getInstance().getStringUtils();
        mUserProfile = new UserProfile(getContext());
        calendar = Calendar.getInstance();
        prefManager = new PrefManager(getContext());

        nearestMandiLocation = new Location("nearest_mandi");

        adapter = new TextualGovPriceAdapter(getContext());

        viewModel = ViewModelProviders.of(getActivity()).get(CheckPriceAndroidViewModel.class);

        return inflater.inflate(R.layout.fragment_gov_reported_price, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = DataBindingUtil.bind(view);
        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);


        clBottom = view.findViewById(R.id.linear_layout);

        binding.priceReportBanner.setText(utils.getLocalizedString(R.string.check_price_banner, true));
        binding.priceReportBanner.setSelected(true);

        binding.govRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.govRecycler.setAdapter(adapter);

        binding.sort.sortOptionLayout.setVisibility(View.GONE);
        binding.sort.sortToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.sort.sortOptionLayout.getVisibility() == View.VISIBLE) {
                    binding.sort.sortOptionLayout.setVisibility(View.GONE);
                    binding.sort.sortToggle.setImageResource(R.drawable.ic_keyboard_arrow_up);
                } else {
                    binding.sort.sortOptionLayout.setVisibility(View.VISIBLE);
                    binding.sort.sortToggle.setImageResource(R.drawable.ic_keyboard_arrow_down);
                }
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mMapFragment.getMapAsync(this);

        binding.setViewModel(viewModel);
        binding.executePendingBindings();

        setupBottomSortTab();

        initObservers();
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.setListener(govTextListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        adapter.removeListener();
    }

    @Override
    protected String getPrimaryLocale() {
        return prefManager.getLocale();
    }

    @Override
    protected String getSecondaryLocale() {
        return prefManager.getSecondaryLocale();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        Log.d(TAG, "setUserVisibleHint: ");
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && showNoDataDialog) {
            showDialogForNoData();
        }

    }

    private TextualGovPriceAdapter.GovTextListener govTextListener = new TextualGovPriceAdapter.GovTextListener() {
        @Override
        public void onClickBookmark(int position) {
            GovReportedPriceData govReportedPrice = adapter.getItem(position);
            if (govReportedPrice.isInterested()) {
                govReportedPrice.setInterested(false);
                viewModel.removeInterested(govReportedPrice.getId());
            } else {
                govReportedPrice.setInterested(true);
                viewModel.addInterested(govReportedPrice.getId());
            }
            adapter.notifyItemChanged(position);
        }
    };

    private void initObservers() {

        viewModel.getMandis().observe(this, new Observer<List<MandiModel>>() {
            @Override
            public void onChanged(@Nullable List<MandiModel> mandiModels) {
                GovReportedPriceFragment.this.mandis = mandiModels;
            }
        });

        viewModel.getInterestedMandiIds().observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(@Nullable List<String> strings) {
                interestedMandiIds = strings;
            }
        });

        viewModel.onFetchGovPrices().observe(this, new Observer<List<GovReportedPriceData>>() {
            @Override
            public void onChanged(@Nullable List<GovReportedPriceData> list) {
                showNoDataDialog = false;

                if (mGoogleMap != null) mGoogleMap.clear();

                adapter.clear();

                if (list != null && list.size() > 0) {
                    mGovReports = list;
                    distributeData();
                }
            }
        });

        viewModel.isGovPricesAvailable().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean != null && !aBoolean) {
                    adapter.clear();
                    if (mGoogleMap != null) mGoogleMap.clear();
                    zoomOutIndiaMap();
                    showNoDataDialog = true;
                    showDialogForNoData();
                }
            }
        });

        viewModel.getUserVisibleFragmentIndex().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                if (integer != null) setUserVisibleHint(integer == 0);
                else setUserVisibleHint(false);
                showDialogForNoData();
            }
        });
    }
/*

    private void onSelectMap() {
        binding.tabMap.setTextColor(getResources().getColor(R.color.loginBtn1));
        binding.tabText.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        binding.mapLayout.setVisibility(View.VISIBLE);
        binding.textualLayout.setVisibility(View.GONE);
    }

    private void onSelectText() {
        binding.tabMap.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        binding.tabText.setTextColor(getResources().getColor(R.color.loginBtn1));
        binding.mapLayout.setVisibility(View.GONE);
        binding.textualLayout.setVisibility(View.VISIBLE);
    }
*/

    @Override
    public void onDestroy() {
        super.onDestroy();
        dismissProgressDialog();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_FORM && resultCode == RESULT_OK) {

        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        try {
            if (getContext() != null)
                mGoogleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.map_style));
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        //mGoogleMap.setInfoWindowAdapter(infoWindowAdapter);
        //Commented Temporarily due to crash

        zoomOutIndiaMap();

        UserLocation userLocation = new UserLocation(getActivity());
        userLocation.getLastKnowLocation(onUserLocationResults);

    }

    private void distributeData() {
        mPriceDistribution.distribute(mGovReports);
    }

    private UserLocation.OnUserLocationResults onUserLocationResults = new UserLocation.OnUserLocationResults() {
        @Override
        public void onLocationResult(Location lastLocation) {
            GovReportedPriceFragment.this.userLocation = lastLocation;
        }

        @Override
        public void onLocationFail() {
            LatLng latLng = new LatLng(20.5937, 78.9629); // new delhi co ordinates
            GovReportedPriceFragment.this.userLocation = new Location("default");
            GovReportedPriceFragment.this.userLocation.setLatitude(latLng.latitude);
            GovReportedPriceFragment.this.userLocation.setLongitude(latLng.longitude);
        }
    };

    private void zoomInToNearestMandi(Location location) {
        if (location != null && mGoogleMap != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory
                    .newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), ZOOM_IN);
            mGoogleMap.animateCamera(cameraUpdate);
        }
    }

    private void zoomOutIndiaMap() {
        Log.d(TAG, "zoomOutIndiaMap");
        if (mGoogleMap != null) {
            LatLng latLng = new LatLng(20.5937, 78.9629); // new delhi co ordinates
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_OUT);
            mGoogleMap.animateCamera(cameraUpdate);
        }
    }

    private void setupBottomSortTab() {
        binding.sort.sortByLabel.setText(utils.getLocalizedString(R.string.sort_by, true));
        binding.sort.sortMinPrice.setText(utils.getLocalizedString(R.string.min_price));
        binding.sort.sortModalPrice.setText(utils.getLocalizedString(R.string.modal_price));
        binding.sort.sortMaxPrice.setText(utils.getLocalizedString(R.string.max_price));
        binding.sort.sortTonnage.setText(utils.getLocalizedString(R.string.tonnage));

        binding.sort.sortMinPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_MODE != SortBy.MIN_PRICE) {
                    SORT_MODE = SortBy.MIN_PRICE;
                    updateSortMode();
                    distributeData();
                }
            }
        });

        binding.sort.sortModalPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_MODE != SortBy.MODAL_PRICE) {
                    SORT_MODE = SortBy.MODAL_PRICE;
                    updateSortMode();
                    distributeData();
                }
            }
        });

        binding.sort.sortMaxPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_MODE != SortBy.MAX_PRICE) {
                    SORT_MODE = SortBy.MAX_PRICE;
                    updateSortMode();
                    distributeData();
                }
            }
        });

        binding.sort.sortTonnage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_MODE != SortBy.TONNAGE) {
                    SORT_MODE = SortBy.TONNAGE;
                    updateSortMode();
                    distributeData();
                }
            }
        });

        binding.sort.sortKg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_UNIT != KG) {
                    SORT_UNIT = KG;
                    updateSortUnit();
                    distributeData();
                }
            }
        });

        binding.sort.sortQuintal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_UNIT != QUINTAL) {
                    SORT_UNIT = QUINTAL;
                    updateSortUnit();
                    distributeData();
                }
            }
        });

        binding.sort.sortTonne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_UNIT != TON) {
                    SORT_UNIT = TON;
                    updateSortUnit();
                    distributeData();
                }
            }
        });

        updateSortMode();
        updateSortUnit();
    }

    private void updateSortMode() {
        binding.sort.sortMinPrice.setChecked(false);
        binding.sort.sortModalPrice.setChecked(false);
        binding.sort.sortMaxPrice.setChecked(false);
        binding.sort.sortTonnage.setChecked(false);

        binding.sort.sortMinPrice.setTextColor(getResources().getColor(R.color.primaryTextColor));
        binding.sort.sortModalPrice.setTextColor(getResources().getColor(R.color.primaryTextColor));
        binding.sort.sortMaxPrice.setTextColor(getResources().getColor(R.color.primaryTextColor));
        binding.sort.sortTonnage.setTextColor(getResources().getColor(R.color.primaryTextColor));

        switch (SORT_MODE) {
            case SortBy.MIN_PRICE:
                binding.sort.sortMinPrice.setChecked(true);
                binding.sort.sortMinPrice.setTextColor(getResources().getColor(R.color.white));
                break;
            case SortBy.MODAL_PRICE:
                binding.sort.sortModalPrice.setChecked(true);
                binding.sort.sortModalPrice.setTextColor(getResources().getColor(R.color.white));
                break;
            case SortBy.MAX_PRICE:
                binding.sort.sortMaxPrice.setChecked(true);
                binding.sort.sortMaxPrice.setTextColor(getResources().getColor(R.color.white));
                break;
            case SortBy.TONNAGE:
                binding.sort.sortTonnage.setChecked(true);
                binding.sort.sortTonnage.setTextColor(getResources().getColor(R.color.white));
                break;
        }
    }

    private void updateSortUnit() {
        adapter.setSortUnit(SORT_UNIT);

        binding.sort.sortKg.setChecked(false);
        binding.sort.sortQuintal.setChecked(false);
        binding.sort.sortTonne.setChecked(false);

        binding.sort.sortKg.setTextColor(getResources().getColor(R.color.primaryTextColor));
        binding.sort.sortQuintal.setTextColor(getResources().getColor(R.color.primaryTextColor));
        binding.sort.sortTonne.setTextColor(getResources().getColor(R.color.primaryTextColor));

        switch (SORT_UNIT) {
            case KG:
                binding.sort.sortKg.setChecked(true);
                binding.sort.sortKg.setTextColor(getResources().getColor(R.color.white));
                break;
            case QUINTAL:
                binding.sort.sortQuintal.setChecked(true);
                binding.sort.sortQuintal.setTextColor(getResources().getColor(R.color.white));
                break;
            case TON:
                binding.sort.sortTonne.setChecked(true);
                binding.sort.sortTonne.setTextColor(getResources().getColor(R.color.white));
                break;
        }
    }

    private void showDialogForNoData() {
        if (getUserVisibleHint() && showNoDataDialog) {
            if (getContext() == null) return;

            showNoDataDialog = false;

            String message = utils.getLocalizedString(R.string.gov_price_alert_dialog_message) + "\n\n" +
                    utils.getLocalizedString(R.string.please_try_back_later);

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle(utils.getLocalizedString(R.string.check_prices_profit_title));
            alertDialog.setMessage(message);
            alertDialog.setCancelable(true);
            alertDialog.setPositiveButton(utils.getLocalizedString(R.string.okay), null);
            alertDialog.show();
        }
    }


    private class SortByInterestedMandi extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            List<GovReportedPriceData> interestedList = new ArrayList<>();
            List<GovReportedPriceData> notInterestedList = new ArrayList<>();

            double nearestDistance = 100000000d;
            GovReportedPriceData nearestReport = null;

            if (mandis != null) {
                for (GovReportedPriceData price : mGovReports) {
                    for (MandiModel mandiModel : mandis) {
                        if (price.getMarket().equalsIgnoreCase(mandiModel.mandi_name_en)) {
                            price.setId(mandiModel.mandi_id);

                            for (String id : interestedMandiIds) {
                                if (price.getId().trim().equalsIgnoreCase(id.trim())) {
                                    price.setInterested(true);
                                    break;
                                }
                            }
                            break;
                        }
                    }

                    if (price.isInterested()) interestedList.add(price);
                    else notInterestedList.add(price);

                    if (userLocation != null) {
                        Location dest = new Location("dest");
                        dest.setLatitude(Double.parseDouble(price.getMandiLat()));
                        dest.setLongitude(Double.parseDouble(price.getMandiLng()));
                        double distance = getDistance(userLocation, dest);
                        price.setDistance(distance);

                        if (nearestDistance > distance) {
                            nearestDistance = distance;
                            nearestReport = price;
                        }
                    }
                }
            }

            Collections.sort(notInterestedList, distanceComparator);
            if (nearestReport != null) {
                if (nearestMandiLocation == null) nearestMandiLocation = new Location("nearest location");
                nearestMandiLocation.setLatitude(Double.parseDouble(nearestReport.getMandiLat()));
                nearestMandiLocation.setLongitude(Double.parseDouble(nearestReport.getMandiLng()));
            } else {
                nearestMandiLocation = null;
            }

            mTextList.clear();
            mTextList.addAll(interestedList);
            Log.d(TAG, "doInBackground: " + mTextList.size());
            mTextList.addAll(notInterestedList);
            Log.d(TAG, "doInBackground: " + mTextList.size());

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            adapter.addAllItems(mTextList);
            if (nearestMandiLocation != null) zoomInToNearestMandi(nearestMandiLocation);
            else zoomInToNearestMandi(userLocation);
        }
    }


    private GoogleMap.InfoWindowAdapter infoWindowAdapter = new GoogleMap.InfoWindowAdapter() {
        @Override
        public View getInfoWindow(Marker marker) {

            GovReportedPrice govReportedPrice = (GovReportedPrice) marker.getTag();

            if (govReportedPrice == null || getActivity() == null) return null;

            View view = getLayoutInflater().inflate(R.layout.item_check_price_text, null);
            ItemCheckPriceTextBinding binding = DataBindingUtil.bind(view);

            if (binding == null) return null;

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            binding.rootLayout.setMinWidth(displayMetrics.widthPixels);

            binding.textMandiLabel.setText(utils.getLocalizedString(R.string.mandi));
            binding.textVarietyLabel.setText(utils.getLocalizedString(R.string.variety));
            binding.textGradeLabel.setText(utils.getLocalizedString(R.string.grade));

            binding.textMinLabel.setText(utils.getLocalizedString(R.string.min));
            binding.textModelLabel.setText(utils.getLocalizedString(R.string.modal));
            binding.textMaxLabel.setText(utils.getLocalizedString(R.string.max));
            binding.textTonnageLabel.setText(utils.getLocalizedString(R.string.tonnage));

            String mandi = govReportedPrice.getMandiName() + ", " + govReportedPrice.getMandiDistrict();
            binding.textMandi.setText(mandi);
            binding.textVariety.setText(govReportedPrice.getVariety());
            binding.textGrade.setText(govReportedPrice.getGrade());

            String min = "Rs. " + MathUtils.convertQuintalToString(govReportedPrice.getMinPrice(), SORT_UNIT);
            String modal = "Rs. " + MathUtils.convertQuintalToString(govReportedPrice.getModalPrice(), SORT_UNIT);
            String max = "Rs. " + MathUtils.convertQuintalToString(govReportedPrice.getMaxPrice(), SORT_UNIT);
            binding.textMinPrice.setText(min);
            binding.textModalPrice.setText(modal);
            binding.textMaxPrice.setText(max);

            String ton = govReportedPrice.getTonnage() + " Ton";
            binding.textTonnage.setText(ton);

            if (getContext() != null) {
                if (govReportedPrice.isInterested()) {
                    binding.imageBookmark.setImageResource(R.drawable.ic_bookmark_black);
                    ImageViewCompat.setImageTintList(binding.imageBookmark,
                            ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.colorAccent)));
                } else {
                    binding.imageBookmark.setImageResource(R.drawable.ic_bookmark_border);
                    ImageViewCompat.setImageTintList(binding.imageBookmark,
                            ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.primaryTextColor)));
                }
            }

            view.invalidate();
            return view;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }
    };

    private void addMarker(GovReportedPriceData govReportedPrice, int color) {
        if (mGoogleMap == null) return;

        double lat = Double.parseDouble(govReportedPrice.getMandiLat());
        double lng = Double.parseDouble(govReportedPrice.getMandiLng());

        if (lat == 0 || lng == 0) return;

        try {
            BitmapDescriptor bitmapDescriptor = getMarkerIcon(colors[color]);
            if (bitmapDescriptor != null) {
                mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng))
                        .icon(getMarkerIcon(colors[color]))
                ).setTag(govReportedPrice);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPriceRanges(int maxInterval, List<Double> intervals, List<GovReportedPriceData> list) {
        if (intervals.size() <= 7) return;
        showProgressDialog();

        String range = "";

        double lower;
        double upper;
        lower = intervals.get(0);
        if (intervals.get(0).equals(intervals.get(1))) upper = intervals.get(1);
        else upper = intervals.get(1) - 0.1;

        if (SORT_MODE == 4) {
            range = MathUtils.roundDecimal(lower, MathUtils.DoubleFormat.ONE_DECIMAL) + " - " +
                    MathUtils.roundDecimal(upper, MathUtils.DoubleFormat.ONE_DECIMAL);
        } else {
            range = MathUtils.convertQuintalToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                    MathUtils.convertQuintalToStringWithoutUnit(upper, SORT_UNIT);
        }

        binding.sort.sortViolet.setText(range);

        lower = intervals.get(1);
        if (intervals.get(1).equals(intervals.get(2))) upper = intervals.get(2);
        else upper = intervals.get(2) - 0.1;


        if (SORT_MODE == 4) {
            range = MathUtils.roundDecimal(lower, MathUtils.DoubleFormat.ONE_DECIMAL) + " - " +
                    MathUtils.roundDecimal(upper, MathUtils.DoubleFormat.ONE_DECIMAL);
        } else {
            range = MathUtils.convertQuintalToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                    MathUtils.convertQuintalToStringWithoutUnit(upper, SORT_UNIT);
        }

        binding.sort.sortIndigo.setText(range);

        lower = intervals.get(2);
        if (intervals.get(2).equals(intervals.get(3))) upper = intervals.get(3);
        else upper = intervals.get(3) - 0.1;


        if (SORT_MODE == 4) {
            range = MathUtils.roundDecimal(lower, MathUtils.DoubleFormat.ONE_DECIMAL) + " - " +
                    MathUtils.roundDecimal(upper, MathUtils.DoubleFormat.ONE_DECIMAL);
        } else {
            range = MathUtils.convertQuintalToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                    MathUtils.convertQuintalToStringWithoutUnit(upper, SORT_UNIT);
        }

        binding.sort.sortBlue.setText(range);

        lower = intervals.get(3);
        if (intervals.get(3).equals(intervals.get(4))) upper = intervals.get(4);
        else upper = intervals.get(4) - 0.1;


        if (SORT_MODE == 4) {
            range = MathUtils.roundDecimal(lower, MathUtils.DoubleFormat.ONE_DECIMAL) + " - " +
                    MathUtils.roundDecimal(upper, MathUtils.DoubleFormat.ONE_DECIMAL);
        } else {
            range = MathUtils.convertQuintalToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                    MathUtils.convertQuintalToStringWithoutUnit(upper, SORT_UNIT);
        }

        binding.sort.sortGreen.setText(range);

        lower = intervals.get(4);
        if (intervals.get(4).equals(intervals.get(5))) upper = intervals.get(5);
        else upper = intervals.get(5) - 0.1;


        if (SORT_MODE == 4) {
            range = MathUtils.roundDecimal(lower, MathUtils.DoubleFormat.ONE_DECIMAL) + " - " +
                    MathUtils.roundDecimal(upper, MathUtils.DoubleFormat.ONE_DECIMAL);
        } else {
            range = MathUtils.convertQuintalToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                    MathUtils.convertQuintalToStringWithoutUnit(upper, SORT_UNIT);
        }

        binding.sort.sortYellow.setText(range);

        lower = intervals.get(5);
        if (intervals.get(5).equals(intervals.get(6))) upper = intervals.get(6);
        else upper = intervals.get(6) - 0.1;


        if (SORT_MODE == 4) {
            range = MathUtils.roundDecimal(lower, MathUtils.DoubleFormat.ONE_DECIMAL) + " - " +
                    MathUtils.roundDecimal(upper, MathUtils.DoubleFormat.ONE_DECIMAL);
        } else {
            range = MathUtils.convertQuintalToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                    MathUtils.convertQuintalToStringWithoutUnit(upper, SORT_UNIT);
        }

        binding.sort.sortOrange.setText(range);

        lower = intervals.get(6);
        upper = intervals.get(7);

        if (SORT_MODE == 4) {
            range = MathUtils.roundDecimal(lower, MathUtils.DoubleFormat.ONE_DECIMAL) + " - " +
                    MathUtils.roundDecimal(upper, MathUtils.DoubleFormat.ONE_DECIMAL);
        } else {
            range = MathUtils.convertQuintalToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                    MathUtils.convertQuintalToStringWithoutUnit(upper, SORT_UNIT);
        }

        binding.sort.sortRed.setText(range);

        if (mGoogleMap != null)
            mGoogleMap.clear();

        int size = list.size();
        for (int i = 0; i < size; i++) {
            GovReportedPriceData govReportedPrice = mGovReports.get(i);

            double checkPrice = 0;
            if (SORT_MODE == 1) {
                checkPrice = govReportedPrice.getMinP();
            } else if (SORT_MODE == 2) {
                checkPrice = govReportedPrice.getModP();
            } else if (SORT_MODE == 3) {
                checkPrice = govReportedPrice.getMaxP();
            } else if (SORT_MODE == 4) {
                checkPrice = govReportedPrice.getTonnage();
            }

//            Log.d(TAG, "map : " + i + "  price: " + checkPrice);

            for (int j = 0; j < maxInterval; j++) {
                double limit = intervals.get(j + 1); // consider upper limit

                if (j != maxInterval - 1)
                    limit = limit - 0.1;

                if (checkPrice <= limit) {
//                    Log.d(TAG, "to : " + j + " limit " + limit);
                    addMarker(govReportedPrice, j);

                    break;
                }
            }
        }

        dismissProgressDialog();
    }



    private int sort(GovReportedPriceData o1, GovReportedPriceData o2) {
        if (SORT_MODE == 1) {
            return Double.compare(o1.getMinP(), o2.getMinP());
        } else if (SORT_MODE == 2) {
            return Double.compare(o1.getModP(), o2.getModP());
        } else if (SORT_MODE == 3) {
            return Double.compare(o1.getMaxP(), o2.getMaxP());
        } else if (SORT_MODE == 4) {
            return Double.compare(o1.getTonnage(), o2.getTonnage());
        }
        return 0;
    }

    private Comparator<GovReportedPriceData> distributionComparator = new Comparator<GovReportedPriceData>() {
        @Override
        public int compare(GovReportedPriceData o1, GovReportedPriceData o2) {
            return sort(o1, o2);
        }
    };

    private Comparator<GovReportedPriceData> distanceComparator = new Comparator<GovReportedPriceData>() {
        @Override
        public int compare(GovReportedPriceData o1, GovReportedPriceData o2) {
            return Double.compare(o1.getDistance(), o2.getDistance());
        }
    };

    private PriceDistribution.Distribute<GovReportedPriceData> distributeListener = new PriceDistribution.Distribute<GovReportedPriceData>() {
        @Override
        public double getIntervalPrice(GovReportedPriceData o) {
            if (SORT_MODE == 1) {
                return o.getMinP();
            } else if (SORT_MODE == 2) {
                return o.getModP();
            } else if (SORT_MODE == 3) {
                return o.getMaxP();
            } else if (SORT_MODE == 4) {
                return o.getTonnage();
            }
            return 0;
        }

        @Override
        public void onSuccess(int maxInterval, List<Double> intervals, List<GovReportedPriceData> list) {
            Log.d(TAG, "" + list.size());
            new SortByInterestedMandi().execute();
            setPriceRanges(maxInterval, intervals, list);
        }

        @Override
        public void failure() {

        }
    };
}
