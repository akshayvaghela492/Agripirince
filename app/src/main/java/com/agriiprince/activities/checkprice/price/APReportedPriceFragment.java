package com.agriiprince.activities.checkprice.price;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agriiprince.R;
import com.agriiprince.activities.checkprice.CheckPriceAndroidViewModel;
import com.agriiprince.activities.checkprice.SortBy;
import com.agriiprince.activities.checkprice.TextualAgentPriceAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.FragmentAgentReportedPiceBinding;
import com.agriiprince.databinding.ItemCheckPriceTextBinding;
import com.agriiprince.helper.UserLocation;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.MandiModel;
import com.agriiprince.model.OeReportedPrice;
import com.agriiprince.helper.PriceDistribution;
import com.agriiprince.utils.MathUtils;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.UnitUtils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.agriiprince.utils.UnitUtils.Weight.KG;
import static com.agriiprince.utils.UnitUtils.Weight.QUINTAL;
import static com.agriiprince.utils.UnitUtils.Weight.TON;

/**
 * A simple {@link Fragment} subclass.
 */
public class APReportedPriceFragment extends CheckPriceBaseFragment implements OnMapReadyCallback {

    private static final String TAG = APReportedPriceFragment.class.getSimpleName();

    private int SORT_MODE = SortBy.MODAL_PRICE;
    private UnitUtils.Weight SORT_UNIT = KG;

    private PriceDistribution<OeReportedPrice> mPriceDistribution;

    private GoogleMap mGoogleMap;

    private List<MandiModel> mandis;

    private List<String> interestedMandiIds;

    private UserProfile mUserProfile;

    private PrefManager prefManager;

    private List<OeReportedPrice> mOeReported;
    private List<OeReportedPrice> mTextList;

    private Location userLocation;
    private Location nearestMandiLocation;

    StringUtils utils;

    private TextualAgentPriceAdapter adapter;

    private CheckPriceAndroidViewModel viewModel;

    private SupportMapFragment mMapFragment;

    private FragmentAgentReportedPiceBinding binding;
    private boolean showNoDataDialog;

    public APReportedPriceFragment() {

    }

    public static APReportedPriceFragment newInstance() {
        APReportedPriceFragment fragment = new APReportedPriceFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mPriceDistribution = new PriceDistribution<>(mOeReported, distributionComparator, distributeListener);
        utils = AppController.getInstance().getStringUtils();
        mUserProfile = new UserProfile(getContext());
        mOeReported = new ArrayList<>();
        mTextList = new ArrayList<>();
        prefManager = new PrefManager(getContext());

        nearestMandiLocation = new Location("nearest_mandi");

        adapter = new TextualAgentPriceAdapter(getContext());

        viewModel = ViewModelProviders.of(getActivity()).get(CheckPriceAndroidViewModel.class);

        return inflater.inflate(R.layout.fragment_agent_reported_pice, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = DataBindingUtil.bind(view);
        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        binding.oeBanner.setText(utils.getLocalizedString(R.string.oe_banner, true));
        binding.oeBanner.setSelected(true);

        binding.govRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.govRecycler.setAdapter(adapter);

        binding.sort.sortOptionLayout.setVisibility(View.GONE);
        binding.sort.sortToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.sort.sortOptionLayout.getVisibility() == View.VISIBLE) {
                    binding.sort.sortOptionLayout.setVisibility(View.GONE);
                    binding.sort.sortToggle.setImageResource(R.drawable.ic_keyboard_arrow_up);
                } else {
                    binding.sort.sortOptionLayout.setVisibility(View.VISIBLE);
                    binding.sort.sortToggle.setImageResource(R.drawable.ic_keyboard_arrow_down);
                }
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mMapFragment.getMapAsync(this);

        binding.setViewModel(viewModel);

        setupBottomSortTab();

        initObservers();
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.setListener(apTextListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        adapter.removeListener();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        Log.d(TAG, "setUserVisibleHint: ");
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && showNoDataDialog) showDialogForNoData();
    }

    private TextualAgentPriceAdapter.ApTextListener apTextListener = new TextualAgentPriceAdapter.ApTextListener() {
        @Override
        public void onClickBookmark(int position) {
            OeReportedPrice oeReportedPrice = adapter.getItem(position);
            if (oeReportedPrice.isInterested()) {
                oeReportedPrice.setInterested(false);
                viewModel.removeInterested(oeReportedPrice.getId());
            } else {
                oeReportedPrice.setInterested(true);
                viewModel.addInterested(oeReportedPrice.getId());
            }
            adapter.notifyItemChanged(position);
        }
    };


    private void setupBottomSortTab() {
        binding.sort.sortByLabel.setText(utils.getLocalizedString(R.string.sort_by, true));
        binding.sort.sortMinPrice.setText(utils.getLocalizedString(R.string.min_price));
        binding.sort.sortModalPrice.setText(utils.getLocalizedString(R.string.modal_price));
        binding.sort.sortMaxPrice.setText(utils.getLocalizedString(R.string.max_price));

        binding.sort.sortTonnage.setVisibility(View.GONE);

        binding.sort.sortMinPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_MODE != SortBy.MIN_PRICE) {
                    SORT_MODE = SortBy.MIN_PRICE;
                    updateSortMode();
                    distributeData();
                }
            }
        });

        binding.sort.sortModalPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_MODE != SortBy.MODAL_PRICE) {
                    SORT_MODE = SortBy.MODAL_PRICE;
                    updateSortMode();
                    distributeData();
                }
            }
        });

        binding.sort.sortMaxPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_MODE != SortBy.MAX_PRICE) {
                    SORT_MODE = SortBy.MAX_PRICE;
                    updateSortMode();
                    distributeData();
                }
            }
        });

        binding.sort.sortKg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_UNIT != KG) {
                    SORT_UNIT = KG;
                    updateSortUnit();
                    distributeData();
                }
            }
        });

        binding.sort.sortQuintal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_UNIT != QUINTAL) {
                    SORT_UNIT = QUINTAL;
                    updateSortUnit();
                    distributeData();
                }
            }
        });

        binding.sort.sortTonne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SORT_UNIT != TON) {
                    SORT_UNIT = TON;
                    updateSortUnit();
                    distributeData();
                }
            }
        });

        updateSortMode();
        updateSortUnit();
    }

    private void initObservers() {

        viewModel.getMandis().observe(this, new Observer<List<MandiModel>>() {
            @Override
            public void onChanged(@Nullable List<MandiModel> mandiModels) {
                APReportedPriceFragment.this.mandis = mandiModels;
            }
        });

        viewModel.getInterestedMandiIds().observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(@Nullable List<String> strings) {
                interestedMandiIds = strings;
            }
        });

        viewModel.onFetchAgentPrices().observe(this, new Observer<List<OeReportedPrice>>() {
            @Override
            public void onChanged(@Nullable List<OeReportedPrice> list) {
                showNoDataDialog = false;

                if (mGoogleMap != null) mGoogleMap.clear();

                adapter.clear();

                if (list != null && list.size() > 0) {
                    mOeReported = list;
                    distributeData();
                }
            }
        });

        viewModel.isApPricesAvailable().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                Log.d(TAG, "onChanged: ");
                if (aBoolean != null && !aBoolean) {
                    adapter.clear();
                    if (mGoogleMap != null) mGoogleMap.clear();
                    zoomOutIndiaMap();
                    showNoDataDialog = true;
                    showDialogForNoData();
                }
            }
        });

        viewModel.getUserVisibleFragmentIndex().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                if (integer != null) setUserVisibleHint(integer == 1);
                else setUserVisibleHint(false);
                showDialogForNoData();
            }
        });
    }

    @Override
    protected String getPrimaryLocale() {
        return prefManager.getLocale();
    }

    @Override
    protected String getSecondaryLocale() {
        return prefManager.getSecondaryLocale();
    }


    private View.OnFocusChangeListener onAutoCompleteCropNameFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) hideSoftInputLayout(v);
        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
        dismissProgressDialog();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        try {
            if (getContext() != null)
                mGoogleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.map_style));
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.setInfoWindowAdapter(infoWindowAdapter);

        zoomOutIndiaMap();

        UserLocation userLocation = new UserLocation(getActivity());
        userLocation.getLastKnowLocation(onUserLocationResults);

    }

    private void updateSortMode() {
        binding.sort.sortMinPrice.setChecked(false);
        binding.sort.sortModalPrice.setChecked(false);
        binding.sort.sortMaxPrice.setChecked(false);
        binding.sort.sortTonnage.setChecked(false);

        binding.sort.sortMinPrice.setTextColor(getResources().getColor(R.color.primaryTextColor));
        binding.sort.sortModalPrice.setTextColor(getResources().getColor(R.color.primaryTextColor));
        binding.sort.sortMaxPrice.setTextColor(getResources().getColor(R.color.primaryTextColor));
        binding.sort.sortTonnage.setTextColor(getResources().getColor(R.color.primaryTextColor));

        switch (SORT_MODE) {
            case SortBy.MIN_PRICE:
                binding.sort.sortMinPrice.setChecked(true);
                binding.sort.sortMinPrice.setTextColor(getResources().getColor(R.color.white));
                break;
            case SortBy.MODAL_PRICE:
                binding.sort.sortModalPrice.setChecked(true);
                binding.sort.sortModalPrice.setTextColor(getResources().getColor(R.color.white));
                break;
            case SortBy.MAX_PRICE:
                binding.sort.sortMaxPrice.setChecked(true);
                binding.sort.sortMaxPrice.setTextColor(getResources().getColor(R.color.white));
                break;
            case SortBy.TONNAGE:
                binding.sort.sortTonnage.setChecked(true);
                binding.sort.sortTonnage.setTextColor(getResources().getColor(R.color.white));
                break;
        }
    }

    private void updateSortUnit() {
        adapter.setSortUnit(SORT_UNIT);

        binding.sort.sortKg.setChecked(false);
        binding.sort.sortQuintal.setChecked(false);
        binding.sort.sortTonne.setChecked(false);

        binding.sort.sortKg.setTextColor(getResources().getColor(R.color.primaryTextColor));
        binding.sort.sortQuintal.setTextColor(getResources().getColor(R.color.primaryTextColor));
        binding.sort.sortTonne.setTextColor(getResources().getColor(R.color.primaryTextColor));

        switch (SORT_UNIT) {
            case KG:
                binding.sort.sortKg.setChecked(true);
                binding.sort.sortKg.setTextColor(getResources().getColor(R.color.white));
                break;
            case QUINTAL:
                binding.sort.sortQuintal.setChecked(true);
                binding.sort.sortQuintal.setTextColor(getResources().getColor(R.color.white));
                break;
            case TON:
                binding.sort.sortTonne.setChecked(true);
                binding.sort.sortTonne.setTextColor(getResources().getColor(R.color.white));
                break;
        }
    }


    private UserLocation.OnUserLocationResults onUserLocationResults = new UserLocation.OnUserLocationResults() {
        @Override
        public void onLocationResult(Location lastLocation) {
            APReportedPriceFragment.this.userLocation = lastLocation;
        }

        @Override
        public void onLocationFail() {
            LatLng latLng = new LatLng(20.5937, 78.9629); // new delhi co ordinates
            APReportedPriceFragment.this.userLocation = new Location("default");
            APReportedPriceFragment.this.userLocation.setLatitude(latLng.latitude);
            APReportedPriceFragment.this.userLocation.setLongitude(latLng.longitude);
        }
    };

    private void zoomInToNearestMandi(Location location) {
        if (location != null && mGoogleMap != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory
                    .newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), ZOOM_IN);
            mGoogleMap.animateCamera(cameraUpdate);
        }
    }

    private void showDialogForNoData() {
        if (getUserVisibleHint() && showNoDataDialog) {
            if (getContext() == null) return;

            showNoDataDialog = false;

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle(utils.getLocalizedString(R.string.check_prices_profit_title));
            alertDialog.setMessage(utils.getLocalizedString(R.string.data_awaited_please_check_later));
            alertDialog.setCancelable(true);
            alertDialog.setPositiveButton(utils.getLocalizedString(R.string.okay), null);
            alertDialog.show();
        }
    }


    private void zoomOutIndiaMap() {
        Log.d(TAG, "zoomOutIndiaMap");
        if (mGoogleMap != null) {
            LatLng latLng = new LatLng(20.5937, 78.9629);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_OUT);
            mGoogleMap.animateCamera(cameraUpdate);
        }
    }

    private class SortByInterestedMandi extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            List<OeReportedPrice> interestedList = new ArrayList<>();
            List<OeReportedPrice> notInterestedList = new ArrayList<>();

            double nearestDistance = 100000000d;
            OeReportedPrice nearestReport = null;

            if (mandis != null) {
                for (OeReportedPrice price : mOeReported) {
                    for (MandiModel mandiModel : mandis) {
                        if (price.getMandiName().equalsIgnoreCase(mandiModel.mandi_name_en)) {
                            price.setId(mandiModel.mandi_id);

                            for (String id : interestedMandiIds) {
                                if (price.getId().trim().equalsIgnoreCase(id.trim())) {
                                    price.setInterested(true);
                                    break;
                                }
                            }
                            break;
                        }
                    }


                    if (price.isInterested()) interestedList.add(price);
                    else notInterestedList.add(price);

                    if (userLocation != null) {
                        Location dest = new Location("dest");
                        dest.setLatitude(price.getMandiLat());
                        dest.setLongitude(price.getMandiLng());
                        double distance = getDistance(userLocation, dest);

                        price.setDistance(distance);

                        if (nearestDistance > distance) {
                            nearestDistance = distance;
                            nearestReport = price;
                        }
                    }
                }
            }


            Collections.sort(notInterestedList, distanceComparator);

            if (nearestReport != null) {
                if (nearestMandiLocation == null) nearestMandiLocation = new Location("nearest location");
                nearestMandiLocation.setLatitude(nearestReport.getMandiLat());
                nearestMandiLocation.setLongitude(nearestReport.getMandiLng());
            } else {
                nearestMandiLocation = null;
            }

            mTextList.clear();
            mTextList.addAll(interestedList);
            mTextList.addAll(notInterestedList);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            adapter.addAllItems(mTextList);

            if (nearestMandiLocation != null) zoomInToNearestMandi(nearestMandiLocation);
            else zoomInToNearestMandi(userLocation);
        }
    }

    private void distributeData() {
        mPriceDistribution.distribute(mOeReported);
    }

    private GoogleMap.InfoWindowAdapter infoWindowAdapter = new GoogleMap.InfoWindowAdapter() {
        @Override
        public View getInfoWindow(Marker marker) {
            OeReportedPrice price = (OeReportedPrice) marker.getTag();

            if (price == null || getActivity() == null) return null;

            View view = getLayoutInflater().inflate(R.layout.item_check_price_text, null);
            ItemCheckPriceTextBinding binding = DataBindingUtil.bind(view);

            if (binding == null) return null;

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            binding.rootLayout.setMinWidth(displayMetrics.widthPixels);

            binding.textMandiLabel.setText(utils.getLocalizedString(R.string.mandi));
            binding.textVarietyLabel.setText(utils.getLocalizedString(R.string.variety));
            binding.textGradeLabel.setText(utils.getLocalizedString(R.string.grade));

            binding.textMinLabel.setText(utils.getLocalizedString(R.string.min));
            binding.textModelLabel.setText(utils.getLocalizedString(R.string.modal));
            binding.textMaxLabel.setText(utils.getLocalizedString(R.string.max));

            binding.textTonnageLabel.setVisibility(View.INVISIBLE);
            binding.textTonnageLabel.setText(utils.getLocalizedString(R.string.tonnage));

            String mandi = price.getMandiName() + ", " + price.getMandiDistrict();
            binding.textMandi.setText(mandi);
            binding.textVariety.setText(price.getCommodityVariety());
            binding.textGrade.setText(price.getCommodityGrade());

            String min = "Rs. " + MathUtils.convertQuintalToString(price.getMinPrice(), SORT_UNIT);
            String modal = "Rs. " + MathUtils.convertQuintalToString(price.getModalPrice(), SORT_UNIT);
            String max = "Rs. " + MathUtils.convertQuintalToString(price.getMaxPrice(), SORT_UNIT);

            binding.textMinPrice.setText(min);
            binding.textModalPrice.setText(modal);
            binding.textMaxPrice.setText(max);

            if (getContext() != null) {
                if (price.isInterested()) {
                    binding.imageBookmark.setImageResource(R.drawable.ic_bookmark_black);
                    ImageViewCompat.setImageTintList(binding.imageBookmark,
                            ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.colorAccent)));
                } else {
                    binding.imageBookmark.setImageResource(R.drawable.ic_bookmark_border);
                    ImageViewCompat.setImageTintList(binding.imageBookmark,
                            ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.primaryTextColor)));
                }
            }

            view.invalidate();
            return view;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }
    };

    private void setPriceRanges(int maxInterval, List<Double> intervals, List<OeReportedPrice> list) {
        if (intervals.size() <= 7) return;

        double lower;
        double upper;

        String range = "";

        lower = intervals.get(0);
        if (intervals.get(0).equals(intervals.get(1))) upper = intervals.get(1);
        else upper = intervals.get(1) - 0.1;

        range = MathUtils.convertQuintalToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                MathUtils.convertQuintalToStringWithoutUnit(upper, SORT_UNIT);

        binding.sort.sortViolet.setText(range);

        lower = intervals.get(1);
        if (intervals.get(1).equals(intervals.get(2))) upper = intervals.get(2);
        else upper = intervals.get(2) - 0.1;

        range = MathUtils.convertQuintalToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                MathUtils.convertQuintalToStringWithoutUnit(upper, SORT_UNIT);

        binding.sort.sortIndigo.setText(range);

        lower = intervals.get(2);
        if (intervals.get(2).equals(intervals.get(3))) upper = intervals.get(3);
        else upper = intervals.get(3) - 0.1;

        range = MathUtils.convertQuintalToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                MathUtils.convertQuintalToStringWithoutUnit(upper, SORT_UNIT);

        binding.sort.sortBlue.setText(range);

        lower = intervals.get(3);
        if (intervals.get(3).equals(intervals.get(4))) upper = intervals.get(4);
        else upper = intervals.get(4) - 0.1;

        range = MathUtils.convertQuintalToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                MathUtils.convertQuintalToStringWithoutUnit(upper, SORT_UNIT);

        binding.sort.sortGreen.setText(range);

        lower = intervals.get(4);
        if (intervals.get(4).equals(intervals.get(5))) upper = intervals.get(5);
        else upper = intervals.get(5) - 0.1;

        range = MathUtils.convertQuintalToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                MathUtils.convertQuintalToStringWithoutUnit(upper, SORT_UNIT);

        binding.sort.sortYellow.setText(range);

        lower = intervals.get(5);
        if (intervals.get(5).equals(intervals.get(6))) upper = intervals.get(6);
        else upper = intervals.get(6) - 0.1;

        range = MathUtils.convertQuintalToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                MathUtils.convertQuintalToStringWithoutUnit(upper, SORT_UNIT);

        binding.sort.sortOrange.setText(range);

        lower = intervals.get(6);
        upper = intervals.get(7);

        range = MathUtils.convertQuintalToStringWithoutUnit(lower, SORT_UNIT) + " - " +
                MathUtils.convertQuintalToStringWithoutUnit(upper, SORT_UNIT);

        binding.sort.sortRed.setText(range);

        if (mGoogleMap != null)
            mGoogleMap.clear();

        int size = list.size();
        for (int i = 0; i < size; i++) {
            OeReportedPrice oeReportedPrice = mOeReported.get(i);

            double checkPrice = 0;
            if (SORT_MODE == 1) {
                checkPrice = oeReportedPrice.getMinPrice();
            } else if (SORT_MODE == 2) {
                checkPrice = oeReportedPrice.getModalPrice();
            } else if (SORT_MODE == 3) {
                checkPrice = oeReportedPrice.getMaxPrice();
            }

            for (int j = 0; j < maxInterval; j++) {
                double limit = intervals.get(j + 1);

                if (j != maxInterval - 1)
                    limit = limit - 0.1;

                if (checkPrice <= limit) {
                    addMarker(oeReportedPrice, j);
                    break;
                }
            }
        }

    }


    private void addMarker(OeReportedPrice oeReportedPrice, int color) {
        if (mGoogleMap == null) return;

        double lat = oeReportedPrice.getMandiLat();
        double lng = oeReportedPrice.getMandiLng();

        if (lat == 0 || lng == 0) return;

        try {
            BitmapDescriptor bitmap = getMarkerIcon(colors[color]);
            if (bitmap != null) {

                mGoogleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lat, lng))
                        .icon(getMarkerIcon(colors[color]))
                ).setTag(oeReportedPrice);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onSetupCrops() {
        dismissProgressDialog();
    }

    private int sort(OeReportedPrice o1, OeReportedPrice o2) {
        if (SORT_MODE == 1) {
            return Double.compare(o1.getMinPrice(), o2.getMinPrice());
        } else if (SORT_MODE == 2) {
            return Double.compare(o1.getModalPrice(), o2.getModalPrice());
        } else if (SORT_MODE == 3) {
            return Double.compare(o1.getMaxPrice(), o2.getMaxPrice());
        }
        return 0;
    }

    private Comparator<OeReportedPrice> distributionComparator = new Comparator<OeReportedPrice>() {
        @Override
        public int compare(OeReportedPrice o1, OeReportedPrice o2) {
            return sort(o1, o2);
        }
    };

    private Comparator<OeReportedPrice> distanceComparator = new Comparator<OeReportedPrice>() {
        @Override
        public int compare(OeReportedPrice o1, OeReportedPrice o2) {
            return Double.compare(o1.getDistance(), o2.getDistance());
        }
    };

    private PriceDistribution.Distribute<OeReportedPrice> distributeListener = new PriceDistribution.Distribute<OeReportedPrice>() {
        @Override
        public double getIntervalPrice(OeReportedPrice o) {
            if (SORT_MODE == 1) {
                return o.getMinPrice();
            } else if (SORT_MODE == 2) {
                return o.getModalPrice();
            } else if (SORT_MODE == 3) {
                return o.getMaxPrice();
            }

            return 0;
        }

        @Override
        public void onSuccess(int maxInterval, List<Double> intervals, List<OeReportedPrice> list) {
            new SortByInterestedMandi().execute();
            setPriceRanges(maxInterval, intervals, list);
        }

        @Override
        public void failure() {

        }
    };
}
