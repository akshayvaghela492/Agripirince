package com.agriiprince.activities.oe;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.adapter.CustomStringArrayAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.databinding.ActivityOeInfoBinding;
import com.agriiprince.dataservice.DataResponse;
import com.agriiprince.dataservice.FcmTokenUpdateDataService;
import com.agriiprince.dataservice.PlaceDataService;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.PlaceModel;
import com.agriiprince.mvvm.retrofit.dto.login.UserRegistration;
import com.agriiprince.mvvm.retrofit.dto.login.TokenGenerate;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.service.UserInterface;
import com.agriiprince.mvvm.ui.login.LoginActivity;
import com.agriiprince.mvvm.ui.oe.OE_MainActivity;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.utils.StringUtils;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.agriiprince.utils.Utils.showToast;

public class OeInfoActivity extends AppCompatActivity {

    private static final String TAG = "OeInfoActivity";

    public static final String MOBILE_NUMBER = "mobile_number";
    public static final String PASSWORD = "mPassword";

    private ProgressDialog mProgressDialog;

    private UserProfile mUserProfile;

    private String mPhoneNumber;
    private String mPassword;

    private String mName;

    private String mStreet;
    private String mCity;
    private String mState;
    private String mPinCode;

    private String mLanguage;

    private String mUserType = "oe"; // oe profile activity, so only oe :)

    private String mUserId;
    private StringUtils utils;

    private List<PlaceModel> mPlaces;
    private List<String> mStates;
    private List<String> mCities;

    private ActivityOeInfoBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_oe_info);

        Intent intent = getIntent();

        mPhoneNumber = intent.getStringExtra(MOBILE_NUMBER);
        mPassword = intent.getStringExtra(PASSWORD);

        utils = AppController.getInstance().getStringUtils();
        PrefManager prefManager = new PrefManager(this);
        mLanguage = prefManager.getLocale();

        mStates = new ArrayList<>();
        mCities = new ArrayList<>();

        mUserProfile = new UserProfile(this);

        mBinding.title.setText(utils.getLocalizedString(R.string.fill_your_personal_information));
        mBinding.infoSubmit.setText(utils.getLocalizedString(R.string.submit));

        mBinding.tilName.setHint(utils.getLocalizedString(R.string.name));
        mBinding.tilStreet.setHint(utils.getLocalizedString(R.string.street));
        mBinding.tilState.setHint(utils.getLocalizedString(R.string.select_state));
        mBinding.tilCity.setHint(utils.getLocalizedString(R.string.select_city));
        mBinding.tilPin.setHint(utils.getLocalizedString(R.string.pincode));

        mBinding.infoSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBinding.infoBasicName.getText() != null)
                    mName = mBinding.infoBasicName.getText().toString();
                if (mBinding.infoBasicStreet.getText() != null)
                    mStreet = mBinding.infoBasicStreet.getText().toString();
                if (mBinding.city.getText() != null)
                    mCity = mBinding.city.getText().toString();
                if (mBinding.acState.getText() != null)
                    mState = mBinding.acState.getText().toString();
                if (mBinding.infoBasicPin.getText() != null)
                    mPinCode = mBinding.infoBasicPin.getText().toString();

                if (validateValues(mName, mStreet, mCity, mState, mPinCode)) {
                    userRegistration();
                }
            }
        });

        getCities();
    }


    private void getCities() {
        new PlaceDataService(this, new DataResponse<List<PlaceModel>>() {
            @Override
            public void onSuccessResponse(List<PlaceModel> response) {
                mPlaces = response;
                setStateAdapter();
                if (!TextUtils.isEmpty(mState))
                    setCityAdapter(mState);
            }

            @Override
            public void onParseError() {

            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
    }


    private void setStateAdapter() {
        HashSet<String> hashSet = new HashSet<>();
        for (PlaceModel model : mPlaces) {
            hashSet.add(model.state);
        }

        mStates.clear();
        mStates.addAll(hashSet);

        Collections.sort(mStates);

        Log.d(TAG, "states: " + mStates.toString());

        CustomStringArrayAdapter adapter =
                new CustomStringArrayAdapter(this, android.R.layout.simple_list_item_1, mStates);

        mBinding.acState.setAdapter(adapter);
        mBinding.acState.setValidator(stateValidator);
        mBinding.acState.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String state = parent.getAdapter().getItem(position).toString();
                Log.d(TAG, "state selected: " + state);
                if (!state.equals(mState)) {
                    mState = state;
                    mBinding.city.getText().clear();
                    setCityAdapter(mState);
                }
            }
        });

        mBinding.acState.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                mBinding.acState.performValidation();
            }
        });
    }


    private void setCityAdapter(String state) {
        mCities.clear();
        for (PlaceModel model : mPlaces) {
            if (model.state.equalsIgnoreCase(state))
                mCities.add(model.city);
        }

        Log.d(TAG, "cities: " + mCities.toString());

        Collections.sort(mCities);

        CustomStringArrayAdapter adapter =
                new CustomStringArrayAdapter(this, android.R.layout.simple_list_item_1, mCities);

        mBinding.city.setAdapter(adapter);
        mBinding.city.setValidator(cityValidator);
        mBinding.city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mCity = parent.getAdapter().getItem(position).toString();
            }
        });

        mBinding.city.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                mBinding.city.performValidation();
            }
        });
    }


    private boolean validateValues(String name, String street, String city, String state, String pinCode) {
        if (TextUtils.isEmpty(name)) {
            mBinding.infoBasicName.setError(getString(R.string.enter_your_name));
            mBinding.infoBasicName.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(pinCode)) {
            mBinding.infoBasicPin.setError(getString(R.string.enter_pin_code));
            mBinding.infoBasicPin.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(state)) {
            mBinding.acState.setError(getString(R.string.select_state));
            mBinding.acState.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(city)) {
            mBinding.city.setError(getString(R.string.select_district_town));
            mBinding.city.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(street)) {
            mBinding.infoBasicStreet.setError(getString(R.string.enter_address));
            mBinding.infoBasicStreet.requestFocus();
            return false;
        }

        return true;
    }


    private void userRegistration() {

        Call<UserRegistration> call = retrofit_interface.registration(mUserType.toLowerCase(),mName,mPassword,mCity,
                mPinCode,mPhoneNumber,mLanguage," ");
        call.enqueue(new Callback<UserRegistration>() {
            @Override
            public void onResponse(Call<UserRegistration> call, retrofit2.Response<UserRegistration> response) {
                try {
                    Logs.d("CheckStatus", "onResponse: code registration ");
                    Logs.d("CheckStatus", String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus", response.body().getMessage());
                    Logs.d("CheckStatus", response.body().getData().getUserId());

                    int error_code = response.body().getCode();

                    if (error_code == 200) {

                        mUserId = response.body().getData().getUserId();

                        mUserProfile.setCreatedOn(Calendar.getInstance().getTimeInMillis());

                        onSignUpSuccess();

                        generateToken();

                    } else {
                        Toast.makeText(OeInfoActivity.this, utils.getLocalizedString(R.string.something_went_wrong_try_again), Toast
                                .LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.getMessage();
                }
            }
            @Override
            public void onFailure(Call<UserRegistration> call, Throwable t) {
                Log.d("CheckStatus", "UserRegistration "+ t.getMessage());
            }
        });
    }

    private void updateOeProfile() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_OE_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(mProgressDialog != null) {
                            mProgressDialog.dismiss();
                        }
                        Logs.d("CheckStatus_oe", "onResponse: update profile Success"+response);
                        try {
                            //JSONArray responseArray = new JSONArray(response);
                            //JSONObject responseObject = responseArray.getJSONObject(0);
                            //int errorCode = responseObject.getInt("error_code");
                            //if (errorCode == 100)
                            JSONObject jsonObject=new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            if(code==200)
                            {Logs.d("CheckStatus_oe", "onResponse: update profile code"+code);
                                onSignUpSuccess();
                                startOeMaintActivity();
                            } else {
                                Toast.makeText(OeInfoActivity.this, "Sorry something went wrong. Please try after sometime.", Toast.LENGTH_LONG).show();
                                Logs.d("CheckStatus_oe", "else: update profile Success");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(mProgressDialog != null) {
                            mProgressDialog.dismiss();
                        }
                        Logs.d("CheckStatus_oe", " update profile fail "+error.getMessage());
                        if (error instanceof TimeoutError) {
                            showToast(OeInfoActivity.this, "Slow internet...Kindly check internet to proceed seamlessly.");
                        } else if (error instanceof NoConnectionError) {
                            showToast(OeInfoActivity.this, "No internet connection ");
                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("api_token", mUserProfile.getApiToken());

                //Mandatory
                params.put("user_id", mUserId);
                params.put("password", mPassword);

                params.put("oe_address", mStreet);
                params.put("oe_location", mCity);
                params.put("oe_pincode", mPinCode);

                Log.d(TAG, "getParams: " + params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUserProfile.getApiToken());
                return header;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    API_Manager apiManager=new API_Manager();
    UserInterface retrofit_interface = apiManager.getClient().create(UserInterface.class);

    private void generateToken() {

        Call<TokenGenerate> call = retrofit_interface.generateToken(mUserProfile.getUserId(), mUserProfile.getUserType().toLowerCase(), mUserProfile.getUserPassword());
        call.enqueue(new Callback<TokenGenerate>() {
            @Override
            public void onResponse(Call<TokenGenerate> call, retrofit2.Response<TokenGenerate> response) {
                try {

                    Logs.d("CheckStatus", "onResponse: code token " );
                    Logs.d("CheckStatus",String.valueOf(response.body().getStatus()));
                    Logs.d("CheckStatus",response.body().getMessage());
                    Logs.d("CheckStatus",response.body().getData().getToken());

                    int errorCode = response.body().getCode();

                    Log.d("NEW FARMERINFO -> ", errorCode+"");
                    if (errorCode == 200) {

                        mUserProfile.setApiToken(response.body().getData().getToken());

                        updateOeProfile();
                        uploadFcmToken();

                    } else if (errorCode == 101) {
                        Toast.makeText(OeInfoActivity.this, utils.getLocalizedString(R.string.login_to_continue), Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(OeInfoActivity.this, LoginActivity.class));
                        OeInfoActivity.this.finish();
                    }

                }catch (Exception e){
                    e.getMessage();
                    e.printStackTrace();
                    Toast.makeText(OeInfoActivity.this, utils.getLocalizedString(R.string.login_to_continue), Toast.LENGTH_SHORT).show();

                    startActivity(new Intent(OeInfoActivity.this, LoginActivity.class));
                    OeInfoActivity.this.finish();
                }
            }

            @Override
            public void onFailure(Call<TokenGenerate> call, Throwable t) {
                Log.d("CheckStatus", "token "+ t.getMessage());
            }
        });
    }

    private void startOeMaintActivity() {
        startActivity(new Intent(OeInfoActivity.this, OE_MainActivity.class));
        finish();
    }

    private void uploadFcmToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        String token = instanceIdResult.getToken();

                        new FcmTokenUpdateDataService(mUserProfile.getUserId(), mUserProfile.getUserType(), mUserProfile.getApiToken(), token);
                    }
                });
    }

    private void onSignUpSuccess() {

        PrefManager mPrefManager = new PrefManager(this);

        mPrefManager.setUserName(mName);
        mPrefManager.setUserId(mUserId);
        mPrefManager.setUserPassword(mPassword);
        mPrefManager.setUserType(mUserType);
        mPrefManager.setCity(mCity);
        mPrefManager.setPinCode(mPinCode);
        mPrefManager.setMobileNo(mPhoneNumber);
        mPrefManager.setNewUser(true);

    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.please_wait));
        }

        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    private AutoCompleteTextView.Validator stateValidator = new AutoCompleteTextView.Validator() {
        @Override
        public boolean isValid(CharSequence text) {
            return mStates.contains(text.toString());
        }

        @Override
        public CharSequence fixText(CharSequence invalidText) {
            Log.d(TAG, "stateValidator fixText:  " + invalidText);
            ArrayAdapter<String> stateAdapter = (ArrayAdapter<String>) mBinding.acState.getAdapter();

            stateAdapter.getFilter().filter(invalidText);
            if (stateAdapter.getCount() > 0) {
                String state = stateAdapter.getItem(0);
                Log.d(TAG, "state selected: " + state);
                mState = state;
                mBinding.city.getText().clear();
                setCityAdapter(mState);
                return state;
            } else {
                return invalidText;
            }
        }
    };

    private AutoCompleteTextView.Validator cityValidator = new AutoCompleteTextView.Validator() {
        @Override
        public boolean isValid(CharSequence text) {
            return mCities.contains(text.toString());
        }

        @Override
        public CharSequence fixText(CharSequence invalidText) {
            Log.d(TAG, "cityValidator fixText:  " + invalidText);
            ArrayAdapter<String> cityAdapter = (ArrayAdapter<String>) mBinding.city.getAdapter();

            cityAdapter.getFilter().filter(invalidText);
            if (cityAdapter.getCount() > 0) {
                mCity = cityAdapter.getItem(0);
                Log.d(TAG, "city selected: " + mCity);
                return cityAdapter.getItem(0);
            } else {
                return invalidText;
            }
        }
    };

}
