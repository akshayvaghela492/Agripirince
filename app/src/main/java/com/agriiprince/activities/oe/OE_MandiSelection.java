package com.agriiprince.activities.oe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.agriiprince.R;
import com.agriiprince.mvvm.model.crop.Crop;

import java.util.List;

public class OE_MandiSelection extends AppCompatActivity {

    EditText etSearchbox;
    ListView lvMandiList;
    ArrayAdapter<String> adapter1;
    String[] data = {
            "Mandi 1",
            "Mandi 2",
            "Mandi 3",
            "Mandi 4",
            "Mandi 5",
            "Mandi 6",
            "Mandi 7",
            "Mandi 8",
            "Mandi 9",
            "Mandi 10"
    };
    private List<Crop> mCrops;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oe_mandi_selection);

        etSearchbox = (EditText) findViewById(R.id.et_search_mandi);
        lvMandiList = (ListView) findViewById(R.id.lv_search_mandi);
        lvMandiList.setTextFilterEnabled(true);

        //TODO Get mandis subscribed to

        adapter1 = new ArrayAdapter<String>(OE_MandiSelection.this, android.R.layout.simple_list_item_1, data);
        lvMandiList.setAdapter(adapter1);


        etSearchbox.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                OE_MandiSelection.this.adapter1.getFilter().filter(arg0);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });

        lvMandiList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String entry = (String) parent.getAdapter().getItem(position);
//                showLog(entry);
                Intent intent = new Intent(OE_MandiSelection.this, OE_UpdateCropPrice.class);
                intent.putExtra("mandiName", entry);
                intent.putExtra("mandiState", entry + "s");
                intent.putExtra("mandiDistrict", entry + "d");
                startActivity(intent);
            }
        });


    }
}
