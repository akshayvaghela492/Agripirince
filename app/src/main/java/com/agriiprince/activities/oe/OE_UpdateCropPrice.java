package com.agriiprince.activities.oe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.agriiprince.R;
import com.agriiprince.adapter.OE_Crop_Update_Adapter;
import com.agriiprince.model.CropDetailModel;

import java.util.ArrayList;

public class OE_UpdateCropPrice extends AppCompatActivity {

    private ArrayList<CropDetailModel> entries = new ArrayList<>();
    private OE_Crop_Update_Adapter adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oe_update_crop_price);

        Intent oldIntent = getIntent();
        final String mandiName = oldIntent.getStringExtra("mandiName");
        final String mandiState = oldIntent.getStringExtra("mandiState");
        final String mandiDistrict = oldIntent.getStringExtra("mandiDistrict");

        recyclerView = (RecyclerView) findViewById(R.id.rv);
        adapter = new OE_Crop_Update_Adapter(entries, OE_UpdateCropPrice.this);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);

        //TODO get crops subscribed corresponding to mandi
        for (int i = 0; i < 5; i++) {
            CropDetailModel test = new CropDetailModel();
            test.crop_id = "" + i;
            test.crop_name = "Name " + i;
            test.crop_variety = "Variety " + i;
            entries.add(test);
        }
        adapter.notifyDataSetChanged();

    }
}
