package com.agriiprince.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.agriiprince.R;
import com.agriiprince.adapter.RssChooseAdapter;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.model.TopicModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.agriiprince.utils.Utils.showLog;

public class RssChooseActivity extends AppCompatActivity {

    PrefManager prefManager;
    Context c;
    LinearLayoutManager linearLayoutManager;

    RecyclerView crssv;
    RssChooseAdapter adapter;
    ArrayList<TopicModel> topics;
    List<String> interested;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_rsstopics_farmer);

        c = RssChooseActivity.this;
        prefManager = new PrefManager(this);


        String rss_i = prefManager.getRSStopics();
        topics = new ArrayList<>();

        topics.add(new TopicModel("India Times"));
        topics.add(new TopicModel("The Hindu"));
        topics.add(new TopicModel("Down To Earth"));
        topics.add(new TopicModel("Open The Magazine"));
        topics.add(new TopicModel("OECD News"));
        topics.add(new TopicModel("Govt RSS Agriculture English"));
        topics.add(new TopicModel("Govt RSS Agriculture Hindi"));
        topics.add(new TopicModel("GOI Ministry of Agriculture"));
        topics.add(new TopicModel("GOI Ministry of AYUSH"));
        topics.add(new TopicModel("GOI Ministry of Chemicals and Fertilizers"));
        topics.add(new TopicModel("Nature Journals"));

        showLog(rss_i != null ? rss_i : "null");
        if (rss_i != null) {
            interested = Arrays.asList(rss_i.split(","));

            for (String t : interested) {
                for (TopicModel topic : topics) {
                    if (t.equals(topic.getTopic())) {
                        topic.setInterested(true);
                        break;
                    }
                }
            }
        }

        crssv = findViewById(R.id.choose_rss_rv);
        adapter = new RssChooseAdapter(topics, c, prefManager);
        linearLayoutManager = new LinearLayoutManager(this);
        crssv.setLayoutManager(linearLayoutManager);
        crssv.setAdapter(adapter);

    }

}
