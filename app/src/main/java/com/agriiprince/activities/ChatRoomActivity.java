package com.agriiprince.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.chat.MessageStatus;
import com.agriiprince.mvvm.retrofit.service.Chat;
import com.agriiprince.mvvm.ui.homescreen.FarmerHomeScreenActivity;
import com.agriiprince.adapter.ChatMessageAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.helper.UserLocation;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.ChatMessage;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.utils.Utils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.agriiprince.utils.Utils.showLog;
import static com.agriiprince.utils.Utils.showToast;

public class ChatRoomActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String KEY_MODE = "mode2";
    public static final String KEY_PINCODE = "pincode";
    public static final String KEY_CITY = "city";
    public static final String KEY_COMMODITY = "commodity";

    RecyclerView rvMessages;
    ImageView ivSend;
    EditText etMessage;
    PrefManager pref;
    //Broadcast receiver to receive broadcasts
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private RecyclerView.LayoutManager layoutManager;
    private ChatMessageAdapter adapter;
    //ArrayList of messages to store the thread messages
    private ArrayList<ChatMessage> messages;
    private ArrayList<String> commodityName;
    private ArrayList<String> commodityNameId;
    private ArrayList<String> arrayListCity;
    String receiverId, receiverType;
    private String mode2;
    private String commodityId;
    private AlertDialog firstAlert;
    private ProgressDialog progressdialog;
    private boolean firstLaunch;
    private String city;
    private String strCity;

    private String userPinCode;

    private UserProfile mUserProfile;

    private boolean isKeyboardOpen;

    //This method will return current timestamp
    public static String getTimeStamp() {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(new Date());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_one_to_one);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        isKeyboardOpen = false;

        mUserProfile = new UserProfile(this);
        pref = new PrefManager(this);
        mode2 = "";
        commodityId = "";
        city = "";
        progressdialog = new ProgressDialog(this);
        progressdialog.setCancelable(false);
        progressdialog.setMessage(getString(R.string.please_wait));
        commodityName = new ArrayList<>();
        commodityNameId = new ArrayList<>();
        arrayListCity = new ArrayList<>();
        firstLaunch = true;
    /*try{
      Bundle extras = getIntent().getExtras();
      String call_from = extras.getString("call_from");
      if (call_from.equalsIgnoreCase("farmer")){

      }else if (call_from.equalsIgnoreCase("pv")){

      }
    }catch(Exception e){

    }*/
        this.setTitle("Chat Room");

        if (getIntent().hasExtra(KEY_MODE)) {

            Intent intent = getIntent();
            mode2 = intent.getStringExtra(KEY_MODE);
            if (mode2.equalsIgnoreCase("pincode")) {
                userPinCode = intent.getStringExtra(KEY_PINCODE);
                getSupportActionBar().setTitle(userPinCode);
                this.setTitle(userPinCode + " Chat Room");
            } else if (mode2.equalsIgnoreCase("commodity_id")) {
                commodityId = intent.getStringExtra(KEY_COMMODITY);
                getSupportActionBar().setTitle(commodityId);
                this.setTitle(commodityId + " Chat Room");
            } else if (mode2.equalsIgnoreCase("city")) {
                city = intent.getStringExtra(KEY_CITY);
                this.setTitle(city + " Chat Room");
                getSupportActionBar().setTitle(city);
            }

            fetchMessages();

        } else {

            if (pref.getUserType().equalsIgnoreCase("farmer")) {
                showAlertToSelectChoiceOfFarmer();
            } else if (pref.getUserType().equalsIgnoreCase("pv")) {
                showAlertToSelectChoiceOfFarmer();
            }
        }

        messages = new ArrayList<>();

        adapter = new ChatMessageAdapter(messages, mUserProfile.getUserId());

        //Initializing recycler view
        rvMessages = findViewById(R.id.rvMessages);
        layoutManager = new LinearLayoutManager(this);
        rvMessages.setLayoutManager(layoutManager);

        rvMessages.setAdapter(adapter);

        //Initializing message arraylist

        //Calling function to fetch the existing messages on the thread
        //fetchMessages();

        ivSend = findViewById(R.id.ivSend);

        //initializing button and edittext
        ivSend = findViewById(R.id.ivSend);
        etMessage = findViewById(R.id.etMessage);

        //Adding listener to button
        ivSend.setOnClickListener(this);

        //Creating broadcast receiver
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
/*        if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_SUCCESS)) {

          //When gcm registration is success do something here if you need

        } else if (intent.getAction()
            .equals(GCMRegistrationIntentService.REGISTRATION_TOKEN_SENT)) {

          //When the registration token is sent to ther server displaying a toast
          Toast.makeText(getApplicationContext(), "Chatroom Ready...", Toast.LENGTH_SHORT).show();

          //When we received a notification when the app is in foreground
        } else if (intent.getAction().equals(Constants.PUSH_NOTIFICATION)) {
          //Getting message data
          String name = intent.getStringExtra("name");
          String message = intent.getStringExtra("message");
          String id = intent.getStringExtra("id");

          //processing the message to add it in current thread
          processMessage(name, message, id);
        }*/
                if (intent.getAction().equalsIgnoreCase(Config.NOTIFICATION_TAG3)) {
                    showLog("ChatRoom : " + "inside onReceive");
                    //Getting message data
                    String message = intent.getStringExtra("message");
                    String mode_2 = intent.getStringExtra("mode2");
                    String commodity_id = intent.getStringExtra("commodity_id");
                    String pincode = intent.getStringExtra("pincode");
                    String strcity = intent.getStringExtra("city");
                    String name = intent.getStringExtra("name");
                    String sender_id = intent.getStringExtra("sender_id");

                    if (mode_2.equalsIgnoreCase(mode2)
                            && (commodity_id.equalsIgnoreCase(commodityId) || pincode.equalsIgnoreCase(userPinCode) || strcity.equalsIgnoreCase(city))) {
                        //processing the message to add it in current thread
                        if (!sender_id.equalsIgnoreCase(mUserProfile.getUserId())) {
                            processMessage(message, name);
                            showLog("ChatRoom : " + "processMessage");
                        } else {
                            showLog("ChatRoom : " + "else processMessage");
                        }
                    } else {
                        Log.d("Chatroom", "Update chat room else");
                        Log.d("mode2", "mode_2 " + mode_2 + " mode2 " + mode2);
                        Log.d("commodity_id", "commodity_id " + commodity_id + " commodityId " + commodityId);
                        Log.d("pincode", "pincode " + pincode + " strPincode " + userPinCode);
                        Log.d("strcity", "strcity " + strcity + " city " + city);
                    }

                }
            }
        };

        //if the google play service is not in the device app won't work
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

        if (ConnectionResult.SUCCESS != resultCode) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(),
                        "Google Play Service is not install/enabled in this device!", Toast.LENGTH_LONG).show();
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());

            } else {
                Toast.makeText(getApplicationContext(),
                        "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
            }
        } else {
            //Intent itent = new Intent(this, GCMRegistrationIntentService.class);
            //startService(itent);
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //getting data from broad cast of fcm and adding to list
    private void processMessage(String message, String name) {
        ChatMessage chatMessage = new ChatMessage("-1", "-1 ", mUserProfile.getUserId(), message, getTimeStamp(), name);
        chatMessage.setChat_type(name);
        adapter.addData(chatMessage);
        rvMessages.smoothScrollToPosition(adapter.getItemCount() - 1);
    }

    //This method will fetch all the messages of the thread
    private void fetchMessages() {
        Log.d("one2one fetch msg url", Config.GET_FARMER_ONE_TO_ONE_CHAT_MESSAGE_AT_START_UP);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.GET_FARMER_ONE_TO_ONE_CHAT_MESSAGE_AT_START_UP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY ", response);
                        //dialog.dismiss();
                        //JSONArray array;
                        try {
                            //array = new JSONArray(response);
                            //JSONObject object = array.getJSONObject(0);
                            //int error = object.getInt("error_code");
                            //if (error == 100)
                            JSONObject jsonObject1=new JSONObject(response);
                            int error = jsonObject1.getInt("code");
                            if (error == 200)
                            {
                                if (messages.size() > 0) {
                                    messages.clear();
                                }
                                //String data = object.getString("data");
                                //JSONArray jsonArray = new JSONArray(data);
                                JSONObject dataobj=jsonObject1.getJSONObject("data");
                                JSONArray dataArray=dataobj.getJSONArray("data");
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject jsonObject = dataArray.getJSONObject(i);
                                    String receiver_id = jsonObject.getString("receiver");
                                    String id = jsonObject.getString("id");
                                    String sender_id = jsonObject.getString("sender");
                                    String message = jsonObject.getString("message");
                                    String time = jsonObject.getString("created_on");
                                    String sender_name;
                                    if (jsonObject.getString("final_name") != null
                                            && jsonObject.getString("final_name") != "null") {
                                        sender_name = jsonObject.getString("final_name");
                                    } else {
                                        sender_name = "AP(0)";
                                    }

                                    if (!sender_id.equalsIgnoreCase(pref.getUserId())) {
                                        receiver_id = pref.getUserId();
                                    }

                                    ChatMessage chatMessage = new ChatMessage(id, sender_id, receiver_id, message, time,
                                            sender_name);
                                    chatMessage.setChat_type("notpersonal");

                                    messages.add(chatMessage);

                                }

                                adapter.setData(messages);
                                //scrollToBottom();
                                if (adapter.getItemCount() > 0) {
                                    if (firstLaunch) {
                                        rvMessages.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                rvMessages.smoothScrollToPosition(adapter.getItemCount() - 1);
                                            }
                                        });
                                        firstLaunch = false;
                                    } else {
                                        rvMessages.scrollToPosition(adapter.getItemCount() - 1);
                                    }
                                }
                            } else if (error == 107) {
                                Toast.makeText(ChatRoomActivity.this, "No previous chat found", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showVolleyError(ChatRoomActivity.this, error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());

                params.put("sender_type", pref.getUserType());
                if (pref.getUserType().equalsIgnoreCase("ca")) {
                    params.put("user_id", pref.getUserId());
                } else if (pref.getUserType().equalsIgnoreCase("farmer")) {
                    params.put("sender_id", pref.getUserId());
                    params.put("receiver_id", "");
                    params.put("receiver_type", "");
                    params.put("mode1", "farmer_all");
                    params.put("mode2", mode2);
                    if (mode2.equalsIgnoreCase("pincode")) {
                        params.put("commodity_id", "");
                        params.put("pincode", "" + userPinCode);
                        params.put("city", "");
                    } else if (mode2.equalsIgnoreCase("commodity_id")) {
                        params.put("commodity_id", "" + commodityId);
                        params.put("pincode", "");
                        params.put("city", "");
                    } else if (mode2.equalsIgnoreCase("city")) {
                        params.put("commodity_id", "");
                        params.put("pincode", "");
                        params.put("city", city);
                    }
                } else if (pref.getUserType().equalsIgnoreCase("oe")) {
                    params.put("user_id", pref.getUserId());
                } else if (pref.getUserType().equalsIgnoreCase("fa")) {
                    params.put("user_id", pref.getUserId());
                } else if (pref.getUserType().equalsIgnoreCase("trucker")) {
                    params.put("user_id", pref.getUserId());
                } else if (pref.getUserType().equalsIgnoreCase("trader")) {
                    params.put("user_id", pref.getUserId());
                } else if (pref.getUserType().equalsIgnoreCase("pv")) {
                    params.put("sender_id", pref.getUserId());
                    params.put("receiver_id", receiverId);
                    params.put("receiver_type", receiverType);
                    params.put("mode1", "pv_farmer");
                    params.put("mode2", "no_mode");
                }
                Log.d("fetch msg params", params.toString());
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    //This method will send the new message to the thread
    private void sendMessage() {
        final String message = etMessage.getText().toString().trim();
        if (message.equalsIgnoreCase("")) {
            showToast(this, "Can't send empty message");
            return;
        }
        //int userId = AppController.getInstance().getUserId();
        //String name = AppController.getInstance().getUserName();
        final String sentAt = getTimeStamp();

        etMessage.setText("");

   /*     Log.d("send msg url", Config.INSERT_FARMER_ONE_TO_ONE_CHAT_MESSAGE_AT_START_UP);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.INSERT_FARMER_ONE_TO_ONE_CHAT_MESSAGE_AT_START_UP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY", "insert_msg"+response);
                        JSONArray array;
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            if(code==200) {
                                ChatMessage m = new ChatMessage("-1", mUserProfile.getUserId(), "AP (0)", message, sentAt,
                                        mUserProfile.getUserName());
                                m.setChat_type("notpersonal");

                                adapter.addData(m);
                                if (adapter.getItemCount() > 0) {
                                    rvMessages.scrollToPosition(adapter.getItemCount() - 1);
                                }
                                //fetchMessages();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showToast(ChatRoomActivity.this, "Slow internet...Kindly check internet to proceed seamlessly.");
                        } else if (error instanceof NoConnectionError) {
                            showToast(ChatRoomActivity.this, "No internet connection ");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());
                params.put("sender_type", pref.getUserType());
                params.put("receiver_type", "");
                params.put("name", mUserProfile.getUserName());
                if (pref.getUserType().equalsIgnoreCase("farmer")) {
                    params.put("sender_id", pref.getUserId());
                    params.put("mode1", "farmer_all");
                } else if (pref.getUserType().equalsIgnoreCase("pv")) {
                    params.put("sender_id", pref.getUserId());
                }
                params.put("receiver_id", "");
                params.put("message", message);
                params.put("mode2", "" + mode2);
                if (mode2.equalsIgnoreCase("pincode")) {
                    params.put("commodity_id", "");
                    params.put("city", "");
                    params.put("pincode", "" + userPinCode);
                } else if (mode2.equalsIgnoreCase("commodity_id")) {
                    params.put("commodity_id", "" + commodityId);
                    params.put("pincode", "");
                    params.put("city", "");
                } else if (mode2.equalsIgnoreCase("city")) {
                    params.put("city", "" + city);
                    params.put("pincode", "");
                    params.put("commodity_id", "");
                }

                Log.d("send msg params", params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUserProfile.getApiToken());
                return header;
            }
        };

        //Disabling retry to prevent duplicate messages
        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
        */

        API_Manager api_manager = new API_Manager();
        Chat retrofits=api_manager.getClient7().create(Chat.class);

        HashMap<String, String> params = new HashMap<>();
        params.put("sender_type", pref.getUserType());
        if (pref.getUserType().equalsIgnoreCase("farmer")) {
            params.put("sender_id", pref.getUserId());
            params.put("mode1", "farmer_all");
        } else if (pref.getUserType().equalsIgnoreCase("pv")) {
            params.put("sender_id", pref.getUserId());
        }
        params.put("receiver_type", "");
        params.put("receiver_id", "");
        params.put("message", message);
        params.put("mode2", "" + mode2);
        if (mode2.equalsIgnoreCase("pincode")) {
            params.put("commodity_id", "");
            params.put("city", "");
            params.put("pincode", "" + userPinCode);
        } else if (mode2.equalsIgnoreCase("commodity_id")) {
            params.put("commodity_id", "" + commodityId);
            params.put("pincode", "");
            params.put("city", "");
        } else if (mode2.equalsIgnoreCase("city")) {
            params.put("city", "" + city);
            params.put("pincode", "");
            params.put("commodity_id", "");
        }
        params.put("name", "" + mUserProfile.getUserName());

        Logs.d("CheckStatus_insertRoom", "params: insert room "+params);

        Call<MessageStatus> call=retrofits.insert_chat_message(mUserProfile.getApiToken(),params);
        call.enqueue(new Callback<MessageStatus>() {
            @Override
            public void onResponse(Call<MessageStatus> call, retrofit2.Response<MessageStatus> response) {
                Logs.d("CheckStatus_insertRoom", "onResponse: insert room "+response.body());
                Logs.d("CheckStatus_insertRoom", "onResponse: insert room "+response.body().getStatus());
                Logs.d("CheckStatus_insertRoom", "onResponse: insert room "+response.body().getMessage());
                try {
                    if(response.body()!=null && response.body().getStatus()== 200) {
                        Logs.d("CheckStatus_insertRoom", "insert room "+response.body().getStatus());
                        ChatMessage m = new ChatMessage("-1", mUserProfile.getUserId(), "AP (0)", message, sentAt,
                                mUserProfile.getUserName());
                        m.setChat_type("notpersonal");

                        adapter.addData(m);
                        if (adapter.getItemCount() > 0) {
                            rvMessages.scrollToPosition(adapter.getItemCount() - 1);
                        }
                        //fetchMessages();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<MessageStatus> call, Throwable t) {
                Log.d("CheckStatus_insertRoom","insert room error "+t.getMessage());
                if (t instanceof TimeoutError) {
                    showToast(ChatRoomActivity.this, "Slow internet...Kindly check internet to proceed seamlessly.");
                } else if (t instanceof NoConnectionError) {
                    showToast(ChatRoomActivity.this, "No internet connection ");
                }
            }
        });
    }

    //method to scroll the recyclerview to bottom
    private void scrollToBottom() {
        adapter.notifyDataSetChanged();
        if (adapter.getItemCount() > 1) {
            rvMessages
                    .getLayoutManager().smoothScrollToPosition(rvMessages, null, adapter.getItemCount() - 1);
        }
    }

    //Registering broadcast receivers
    @Override
    protected void onResume() {
        super.onResume();
        Log.w("MainActivity", "onResume");
    /*LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
        new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));
    LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
        new IntentFilter(GCMRegistrationIntentService.REGISTRATION_ERROR));
    LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
        new IntentFilter(GCMRegistrationIntentService.REGISTRATION_TOKEN_SENT));*/
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.NOTIFICATION_TAG3));

        if (mode2.equalsIgnoreCase("city")) {
            AppController.getInstance().setChatIdentifier(city);
        } else if (mode2.equalsIgnoreCase("pincode")) {
            AppController.getInstance().setChatIdentifier(userPinCode);
        } else if (mode2.equalsIgnoreCase("commodity_id")) {
            AppController.getInstance().setChatIdentifier(commodityId);

        }
    }


    //Unregistering receivers
    @Override
    protected void onPause() {
        super.onPause();
        Log.w("MainActivity", "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

        AppController.getInstance().removeChatIdentifier();
    }


    //Sending message onclick
    @Override
    public void onClick(View v) {
        if (v == ivSend) {
            sendMessage();
        }
    }

    //Creating option menu to add logout feature
  /*@Override
  public boolean onCreateOptionsMenu(Menu menu) {
    //Adding our menu to toolbar
    getMenuInflater().inflate(R.menu.menu, menu);
    return true;
  }*/

    //Adding logout option here
  /*@Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == R.id.menuLogout) {
      AppController.getInstance().logout();
      finish();
      startActivity(new Intent(this, MainActivity.class));
    }
    return super.onOptionsItemSelected(item);
  }*/


    private void showAlertToSelectWhomToChat() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);
        alert.setMessage("Select your choice to chat with...")
                .setPositiveButton("Farmer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showAlertToSelectChoiceOfFarmer();
                    }
                })
                .setNegativeButton("Pesticide Vender", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setNeutralButton("Cancel", null).show();
    }

    private void showAlertToSelectChoiceOfFarmer() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_alert_farmer_category_chat, null);
        dialogBuilder.setView(dialogView);
        firstAlert = dialogBuilder.create();
        dialogView.findViewById(R.id.btnPincode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
          /*fetchMessages();
          mode2="pincode";*/
                //firstAlert.dismiss();
                progressdialog.show();
                bringPincodeCityData("pincode");
            }
        });
        dialogView.findViewById(R.id.btnCity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
        /*AlertDialog.Builder alert = new AlertDialog.Builder(ChatRoomActivity.this);
        alert.setMessage("Feature coming soon...")
                .setPositiveButton("OK", new DialogInterface.OnClickRssSubscriptionListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {

                  }
                }).show();*/
                progressdialog.show();
                bringPincodeCityData("city");
            }
        });
        dialogView.findViewById(R.id.btnCommodity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressdialog.show();
                bringPincodeCityData("commodity");
            }
        });
        firstAlert.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                if (pref.getUserType().equalsIgnoreCase("farmer")) {
                    startActivity(new Intent(ChatRoomActivity.this, FarmerHomeScreenActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));

                } else if (pref.getUserType().equalsIgnoreCase("pv")) {
                    //startActivity(new Intent(ChatRoomActivity.this, PesticidesVendorStartActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));

                }
            }
        });
        firstAlert.show();
    }

    private void bringPincodeCityData(final String mode) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.BRING_PINCODE_AND_CITIES_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY ","pincode_city"+ response);
                        //JSONArray array;
                        try {
                            //array = new JSONArray(response);
                            //JSONObject object = array.getJSONObject(0);
                            JSONObject object=new JSONObject(response);
                            int error = object.getInt("code");
                            //if (error == 100)
                            if (error == 200)
                            {
                                if (arrayListCity != null) {
                                    arrayListCity.clear();
                                }
                                if (commodityName != null) {
                                    commodityName.clear();
                                }
                                //String data = object.getString("data");
                                //JSONArray jsonArray = new JSONArray(data);
                                JSONObject dataobj=object.getJSONObject("data");
                                JSONArray dataArray=dataobj.getJSONArray("data");
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject jsonObject = dataArray.getJSONObject(i);
                                    if (mode.equalsIgnoreCase("city")) {
                                        String farmer_location = jsonObject.getString("farmer_location");
                                        Log.d("farmer_location", "" + farmer_location);
                                        arrayListCity.add(farmer_location);
                                    } else if (mode.equalsIgnoreCase("commodity")) {
                                        String commodity_name = jsonObject.getString("commodity_name");
                                        Log.d("commodity_name", "" + commodity_name);
                                        commodityName.add(commodity_name);
                                    }
                                }
                            }
                            if (mode.equalsIgnoreCase("city")) {
                                showAlertToEnterCity(arrayListCity);
                            } else if (mode.equalsIgnoreCase("pincode")) {
                                showAlertToEnterPincode();
                            } else if (mode.equalsIgnoreCase("commodity")) {
                                showAlertToEnterCommodity(commodityName);
                            }

                            progressdialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showVolleyError(ChatRoomActivity.this, error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("api_token", mUserProfile.getApiToken());

                if (pref.getUserType().equals("pv")) {
                    params.put("user_type", pref.getUserType());
                    params.put("user_id", "" + pref.getUserId());

                } else if (pref.getUserType().equals("farmer")) {
                    params.put("user_type", pref.getUserType());
                    params.put("user_id", "" + pref.getUserId());

                }
                params.put("mode", "" + mode);
                Log.d("bring commodity params", params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> header = new HashMap<>();
                header.put("api_token", mUserProfile.getApiToken());
                return header;
            }
        };

        //Disabling retry to prevent duplicate messages
        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void showAlertToEnterCity(final ArrayList<String> arrayListCity) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_get_commodity, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alert = dialogBuilder.create();
        TextView textView2 = dialogView.findViewById(R.id.textView2);
        textView2.setText("Enter city...");
        final AutoCompleteTextView autocomplete = dialogView.findViewById(R.id.autoCompleteTextView1);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.select_dialog_item, arrayListCity);
        autocomplete.setThreshold(1);
        autocomplete.setAdapter(adapter);
        dialogView.findViewById(R.id.btnProceed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(autocomplete.getText().toString())) {
                    if (arrayListCity.contains(autocomplete.getText().toString())) {
                        int index = arrayListCity.indexOf(autocomplete.getText().toString());
                        Toast.makeText(ChatRoomActivity.this, "Waiting for REST Api for farmer list", Toast.LENGTH_SHORT).show();
                        fetchMessages();
                        mode2 = "city";
                        city = arrayListCity.get(index);
                        alert.dismiss();
                        firstAlert.dismiss();
                        ChatRoomActivity.this.setTitle(autocomplete.getText().toString() + " Chat Room");
                    } else {
                        autocomplete.setError("No chat room for this city");
                    }
                } else {
                    autocomplete.setError("Can't be blank");
                }

            }
        });
        alert.show();
    }


    private void showAlertToEnterCommodity(final ArrayList<String> commodityName) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_get_commodity, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alert = dialogBuilder.create();
        final AutoCompleteTextView autocomplete = dialogView.findViewById(R.id.autoCompleteTextView1);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.select_dialog_item, commodityName);
        autocomplete.setThreshold(2);
        autocomplete.setAdapter(adapter);
        dialogView.findViewById(R.id.btnProceed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(autocomplete.getText().toString())) {
                    if (commodityName.contains(autocomplete.getText().toString())) {
                        int index = commodityName.indexOf(autocomplete.getText().toString());
                        //Toast.makeText(ChatRoomActivity.this, ""+commodityNameId.get(index), Toast.LENGTH_SHORT).show();
                        fetchMessages();
                        mode2 = "commodity_id";
                        //commodityId=commodityNameId.get(index);
                        commodityId = autocomplete.getText().toString();
                        alert.dismiss();
                        firstAlert.dismiss();
                        ChatRoomActivity.this.setTitle(autocomplete.getText().toString() + " Chat Room");
                    } else {
                        autocomplete.setError("No chat room for this commodity");
                    }
                } else {
                    autocomplete.setError("Can't be blank");
                }

            }
        });
        alert.show();
    }

    private void showAlertToEnterPincode() {
        userPinCode = pref.getPinCode();
        mode2 = "pincode";

        if (userPinCode != null) {
            firstAlert.dismiss();
            fetchMessages();
            ChatRoomActivity.this.setTitle(userPinCode + " Chat Room");
            return;
        }

        UserLocation userLocation = new UserLocation(ChatRoomActivity.this);
        userLocation.getLastKnowLocation(new UserLocation.OnUserLocationResults() {
            @Override
            public void onLocationResult(Location lastLocation) {
                try {
                    Geocoder geocoder = new Geocoder(ChatRoomActivity.this);
                    userPinCode = geocoder.getFromLocation(lastLocation.getLatitude(), lastLocation.getLongitude(), 1)
                            .get(0).getPostalCode();

                    if (userPinCode == null)
                        userPinCode = pref.getPinCode();

                    if (userPinCode == null) {
                        firstAlert.dismiss();
                        Utils.showToast(ChatRoomActivity.this, "failed to find your pin code");
                        ChatRoomActivity.this.finish();
                    } else {
                        firstAlert.dismiss();
                        ChatRoomActivity.this.setTitle(userPinCode + " Chat Room");
                    }

                    fetchMessages();
                } catch (IOException e) {
                    e.printStackTrace();

                    userPinCode = pref.getPinCode();

                    if (userPinCode == null) {
                        firstAlert.dismiss();
                        Utils.showToast(ChatRoomActivity.this, "failed to find your pin code");
                        ChatRoomActivity.this.finish();
                    } else {
                        fetchMessages();
                        firstAlert.dismiss();
                        ChatRoomActivity.this.setTitle(userPinCode + " Chat Room");
                    }
                }
            }

            @Override
            public void onLocationFail() {
                userPinCode = pref.getPinCode();

                if (userPinCode == null) {
                    firstAlert.dismiss();
                    Utils.showToast(ChatRoomActivity.this, "failed to find your pin code");
                    ChatRoomActivity.this.finish();
                } else {
                    firstAlert.dismiss();
                    fetchMessages();
                    ChatRoomActivity.this.setTitle(userPinCode + " Chat Room");
                }
            }
        });

    }
}
