package com.agriiprince.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ListView;

import com.agriiprince.R;
import com.agriiprince.adapter.NotificationAdapter;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.NotificationModel;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity {

    public static List<NotificationModel> listNotification;
    ListView listView;
    String[] listItem = {"NotificationsResp 1", "NotificationsResp 2", "NotificationsResp 3", "NotificationsResp 4",
            "NotificationsResp 5",
            "NotificationsResp 6", "NotificationsResp 7", "NotificationsResp 8", "NotificationsResp 9", "NotificationsResp 10",
            "NotificationsResp 11", "NotificationsResp 12"};
    PrefManager pref;
    private RecyclerView recyclerView;
    private NotificationAdapter mAdapter;
    private ProgressDialog progressDialog;

    private UserProfile mUserProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        //todo rishijay - on click show full NotificationsResp in alert.

        //listView = findViewById(R.id.lvNotification);

        recyclerView = findViewById(R.id.rvNotification);

        mUserProfile = new UserProfile(this);

        pref = new PrefManager(this);

    /*final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
        android.R.layout.simple_list_item_1, android.R.id.text1, listItem);
    listView.setAdapter(adapter);

    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onRssCategoryItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        Toast.makeText(NotificationActivity.this, "NotificationsResp will open in Alert",
            Toast.LENGTH_SHORT)
            .show();
      }
    });*/

        listNotification = new ArrayList<>();
//        mAdapter = new NotificationAdapter(this, listNotification);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager
                (getApplicationContext());
        //RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext()
        //    , 2);
        /*recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);*/

//        fnGetNotification();
    }

    private void fnGetNotification() {
        /*progressDialog.show();

        // URL was "get_notification.php";  // SHOULD BE REPLACED BY api/v1/notifications/get_notifications
        StringRequest strReq = new StringRequest(Method.POST, Config.GET_NOTIFICATION_URL,
                new Listener<String>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        showLog("response get notification " + response);
                        JSONArray array;
                        try {
                            array = new JSONArray(response);

                            JSONObject object = array.getJSONObject(0);
                            int error = object.getInt("error_code");
                            if (error == 100) {

                                String data = object.getString("data");

                                List<NotificationModel> items = new Gson().fromJson(data, new
                                        TypeToken<List<NotificationModel>>() {
                                        }.getType());

                                // adding contacts to contacts list
                                listNotification.clear();
                                listNotification.addAll(items);

                                // refreshing recycler view
                                mAdapter.notifyDataSetChanged();

                                //Toast.makeText(NotificationActivity.this, "Success", Toast.LENGTH_SHORT).show();
                            } else {
                                new Builder(NotificationActivity.this)
                                        .setMessage("No notifications found for you")
                                        .setPositiveButton("Okay", new OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                NotificationActivity.super.onBackPressed();
                                            }
                                        }).setOnCancelListener(new OnCancelListener() {
                                    @Override
                                    public void onCancel(DialogInterface dialog) {
                                        NotificationActivity.super.onBackPressed();
                                    }
                                }).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                if (error instanceof TimeoutError) {
                    showToast(NotificationActivity.this, "Slow internet...Kindly check internet to proceed seamlessly.");
                } else if (error instanceof NoConnectionError) {
                    showToast(NotificationActivity.this, "No internet connection ");
                }
            }
        }) {
            @SuppressLint("LongLogTag")
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());

                params.put("user_id", pref.getUserId());
                showLog("params get notification " + params);
                return params;
            }
        };
        // Adding request to request queue
        strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().cancelPendingRequests(strReq);
        //   Log.d("Volley", "pending request queue cancelled, now adding to request queue");
        AppController.getInstance().addToRequestQueue(strReq);*/
    }
}
