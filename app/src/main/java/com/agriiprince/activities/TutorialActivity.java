package com.agriiprince.activities;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.util.TutorialUtil;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.tutorial.TutorialResponse;
import com.agriiprince.mvvm.retrofit.model.tutorial.SubSection;
import com.agriiprince.mvvm.retrofit.model.tutorial.TutorialSections;
import com.agriiprince.mvvm.retrofit.service.Tutorial;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.agriiprince.appcontroller.AppController.gTutorialSectionsHi;

public class TutorialActivity extends AppCompatActivity {

    private TextToSpeech textToSpeech;
    private ImageView iv;
    private TextView tv;
    private PrefManager mPrefManager;
    private String lang;
    private List<TutorialSections> mTutorialSectionsHi;
    private List<TutorialSections> mTutorialSectionsEn;
    private String mSectionData;
    private StringUtils stringUtils;
    private String mCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setTitle("Disease TutorialRetro");
        //getSupportActionBar().setDisplayOptions(getSupportActionBar().DISPLAY_SHOW_HOME, getSupportActionBar().DISPLAY_SHOW_CUSTOM);
        //getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        stringUtils = AppController.getInstance().getStringUtils();

        mPrefManager = new PrefManager(this);
        lang = mPrefManager.getLocale();

        Intent intent = getIntent();
        mCode =  intent.getStringExtra("code");

        tv = (TextView) findViewById(R.id.textViewContent);
        tv.setText(stringUtils.getLocalizedString(R.string.fetching_data_from_server).toString());
        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int ttsLang;

                    if(mPrefManager.getLocale().equals("hi"))
                        ttsLang = textToSpeech.setLanguage(new Locale("hi","IN"));
                    else
                        ttsLang = textToSpeech.setLanguage(Locale.US);

                    if (ttsLang == TextToSpeech.LANG_MISSING_DATA
                            || ttsLang == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "The Language is not supported!");
                    } else {
                        Log.i("TTS", "Language Supported.");
                    }
                    Log.i("TTS", "Initialization success.");
                } else {
                    Toast.makeText(getApplicationContext(), "TTS Initialization failed!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        iv =  findViewById(R.id.iv_audio);

        iv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                mSectionData = tv.getText().toString();
                Log.i("TTS", "button clicked: " + mSectionData);
                int speechStatus = textToSpeech.speak(mSectionData, TextToSpeech.QUEUE_FLUSH, null);

                if (speechStatus == TextToSpeech.ERROR) {
                    Log.e("TTS", "Error in converting Text to Speech!");
                }
            }

        });

        if(mPrefManager.getLocale().equals("hi"))
            getTutorialHi();
        else
            getTutorialEn();

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (textToSpeech != null) {
            textToSpeech.stop();
        }
    }

    API_Manager api_manager=new API_Manager();
    Tutorial retrofit_interface2 = api_manager.TutorialClient().create(Tutorial.class);

    /*public void getTutorialEn() {

        Call<TutorialResponse> call=retrofit_interface2.tutorial(mPrefManager.getAccessToken(),"en");
        call.enqueue(new Callback<TutorialResponse>() {
            @Override
            public void onResponse(Call<TutorialResponse> call, retrofit2.Response<TutorialResponse> response) {
                Log.e("CHECKSTATUS", "tutorial onResponse "+response);
                try {
                    if (response.body() != null && response.body().getStatus() == 200)
                        Log.e("CHECKSTATUS", "tutorial  status " + response.body().getStatus());
                    Log.e("CHECKSTATUS", "tutorial message " + response.body().getMessage());


                    AppController.getInstance().gTutorialSectionsEn.clear();
                    AppController.getInstance().gTutorialSectionsEn = response.body().getData().getTutorial();


                    Log.e("CHECKSTATUS", "tutorial section_code " + AppController.getInstance().gTutorialSectionsEn.get(7).getSectionCode());
                    Log.e("CHECKSTATUS", "tutorial section_title " + AppController.getInstance().gTutorialSectionsEn.get(7).getSectionTitle());
                    Log.e("CHECKSTATUS", "tutorial section_content " + AppController.getInstance().gTutorialSectionsEn.get(7).getSectionContent());

                    List<SubSection> subSection = AppController.getInstance().gTutorialSectionsEn.get(7).getSubSections();

                    Log.e("CHECKSTATUS", "tutorial  sub_section_code " + subSection.get(1).getSubSectionCode());
                    Log.e("CHECKSTATUS", "tutorial  sub_section_title " + subSection.get(1).getSubSectionTitle());
                    Log.e("CHECKSTATUS", "tutorial  sub_section_content " + subSection.get(1).getSubSectionContent());
                }
                catch
                (Exception e)
                {e.printStackTrace();}

            }
            @Override
            public void onFailure(Call<TutorialResponse> call, Throwable t) {
                Logs.d("CHECKSTATUS","tutorial error "+t.getMessage());
            }
        });
    }*/

    public void getTutorialEn() {

        Call<TutorialResponse> call=retrofit_interface2.tutorial(mPrefManager.getAccessToken(),"en");
        call.enqueue(new Callback<TutorialResponse>() {
            @Override
            public void onResponse(Call<TutorialResponse> call, retrofit2.Response<TutorialResponse> response) {
                Log.e("CHECKSTATUS", "tutorial onResponse "+response);
                try {
                    if (response.body() != null && response.body().getStatus() == 200) {
                        Log.e("CHECKSTATUS", "tutorial  status " + response.body().getStatus());
                        Log.e("CHECKSTATUS", "tutorial message " + response.body().getMessage());

                        //gTutorialSectionsHi.clear();
                        mTutorialSectionsEn = response.body().getData().getTutorial();

                        Log.e("CHECKSTATUS", "tutorial section_code " + mTutorialSectionsEn.get(7).getSectionCode());
                        Log.e("CHECKSTATUS", "tutorial section_title " + mTutorialSectionsEn.get(7).getSectionTitle());
                        Log.e("CHECKSTATUS", "tutorial section_content " + mTutorialSectionsEn.get(7).getSectionContent());

                        List<SubSection> subSection = mTutorialSectionsEn.get(7).getSubSections();

                        try {
                            mSectionData = TutorialUtil.getStringBySectionCode(mCode, "en", mTutorialSectionsEn);
                            tv.setText(mSectionData);
                        } catch (Exception e) {
                            mSectionData = stringUtils.getLocalizedString(R.string.try_again_later).toString();
                        }

                        Log.e("CHECKSTATUS", "tutorial  sub_section_code " + subSection.get(1).getSubSectionCode());
                        Log.e("CHECKSTATUS", "tutorial  sub_section_title " + subSection.get(1).getSubSectionTitle());
                        Log.e("CHECKSTATUS", "tutorial  sub_section_content " + subSection.get(1).getSubSectionContent());
                    }
                    else
                    {
                        mSectionData = stringUtils.getLocalizedString(R.string.try_again_later).toString();
                    }
                }
                catch
                (Exception e)
                {e.printStackTrace();
                    mSectionData = stringUtils.getLocalizedString(R.string.try_again_later).toString();
                }

            }
            @Override
            public void onFailure(Call<TutorialResponse> call, Throwable t) {
                Logs.d("CHECKSTATUS","tutorial error "+t.getMessage());
            }
        });
    }

    public void getTutorialHi() {

        Call<TutorialResponse> call=retrofit_interface2.tutorial(mPrefManager.getAccessToken(),"hi");
        call.enqueue(new Callback<TutorialResponse>() {
            @Override
            public void onResponse(Call<TutorialResponse> call, retrofit2.Response<TutorialResponse> response) {
                Log.e("CHECKSTATUS", "tutorial onResponse "+response);
                try {
                    if (response.body() != null && response.body().getStatus() == 200) {
                        Log.e("CHECKSTATUS", "tutorial  status " + response.body().getStatus());
                        Log.e("CHECKSTATUS", "tutorial message " + response.body().getMessage());

                        //gTutorialSectionsHi.clear();
                        mTutorialSectionsHi = response.body().getData().getTutorial();

                        Log.e("CHECKSTATUS", "tutorial section_code " + mTutorialSectionsHi.get(7).getSectionCode());
                        Log.e("CHECKSTATUS", "tutorial section_title " + mTutorialSectionsHi.get(7).getSectionTitle());
                        Log.e("CHECKSTATUS", "tutorial section_content " + mTutorialSectionsHi.get(7).getSectionContent());

                        List<SubSection> subSection = mTutorialSectionsHi.get(7).getSubSections();

                        try {
                            mSectionData = TutorialUtil.getStringBySectionCode(mCode, "hi", mTutorialSectionsHi);
                            tv.setText(mSectionData);
                        } catch (Exception e) {
                            mSectionData = stringUtils.getLocalizedString(R.string.try_again_later).toString();
                        }

                        Log.e("CHECKSTATUS", "tutorial  sub_section_code " + subSection.get(1).getSubSectionCode());
                        Log.e("CHECKSTATUS", "tutorial  sub_section_title " + subSection.get(1).getSubSectionTitle());
                        Log.e("CHECKSTATUS", "tutorial  sub_section_content " + subSection.get(1).getSubSectionContent());
                    }
                    else
                    {
                        mSectionData = stringUtils.getLocalizedString(R.string.try_again_later).toString();
                    }
                }
                catch
                (Exception e)
                {e.printStackTrace();
                    mSectionData = stringUtils.getLocalizedString(R.string.try_again_later).toString();
                }

            }
            @Override
            public void onFailure(Call<TutorialResponse> call, Throwable t) {
                Logs.d("CHECKSTATUS","tutorial error "+t.getMessage());
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
