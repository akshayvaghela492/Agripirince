package com.agriiprince.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import com.agriiprince.R;
import com.agriiprince.adapter.TrackCropFarmerFragmentAdapter;

import java.util.ArrayList;

public class TrackYourCropFarmer extends FragmentActivity implements View.OnClickListener {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_your_crop_farmer);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.track_crop__f_tabs);
        ViewPager viewPager = (ViewPager) findViewById(R.id.track_crop__f_vp);
        Button addEntry = (Button) findViewById(R.id.add_entry_track_crop_f_btn);
        ArrayList<Fragment> fragmentArrayList = new ArrayList<>();


        TrackCropFarmerFragmentAdapter viewPagerAdapter = new TrackCropFarmerFragmentAdapter(getSupportFragmentManager(), fragmentArrayList);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        addEntry.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        //
    }
}