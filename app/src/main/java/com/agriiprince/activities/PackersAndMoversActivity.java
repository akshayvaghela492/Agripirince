package com.agriiprince.activities;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.agriiprince.R;

public class PackersAndMoversActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_packers_and_movers);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        new Builder(this)
                .setMessage("You have not subscribed to this feature")
                .setPositiveButton("Okay", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PackersAndMoversActivity.super.onBackPressed();
                    }
                }).setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                PackersAndMoversActivity.super.onBackPressed();
            }
        }).show();
    }
}
