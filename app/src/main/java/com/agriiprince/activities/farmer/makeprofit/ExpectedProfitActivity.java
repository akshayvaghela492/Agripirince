package com.agriiprince.activities.farmer.makeprofit;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.util.ArrayMap;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.Utils;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.agriiprince.activities.farmer.makeprofit.MakeProfitActivity.KEY_COMMODITY_GRADE;
import static com.agriiprince.activities.farmer.makeprofit.MakeProfitActivity.KEY_COMMODITY_NAME;
import static com.agriiprince.activities.farmer.makeprofit.MakeProfitActivity.KEY_COMMODITY_QUANTITY;
import static com.agriiprince.activities.farmer.makeprofit.MakeProfitActivity.KEY_COMMODITY_VARIETY;

public class ExpectedProfitActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = ExpectedProfitActivity.class.getName();

    private double SHIPMENT_COST_PER_KM_PER_KG = 0.5d / 100 / 1; // 0.5 Rs per 100 per 1 Kg

    private int SHIPMENT_MOVEMENT_PER_DAY = 400;

    private UserProfile mUserProfile;

    private LatLng originLatLan;

    private String currentCity;

    private GoogleMap mGoogleMap;

    private boolean showGoogleMap = false;

    private String colorRed = "#E74C3C";
    private String colorGreen = "#2ECC71";

    private Map<String, Integer> mMandiDistance;
    private Map<String, LatLng> mMandiLatLng;
    private Map<String, List<String>> mModalPrices;

    private List<String> mCities;

    private AppCompatAutoCompleteTextView acCurrentCity;

    private TableLayout mTableLayout;

    private String cropName, cropVariety, cropGrade;
    private int cropQuantity;

    private int mMaxDistanceForProfit;

    private ProgressDialog mProgressDialog;
    StringUtils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expected_profit);
        utils = AppController.getInstance().getStringUtils();
        mUserProfile = new UserProfile(this);

        PrefManager prefManager = new PrefManager(this);

        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.expected_profit_map);

        mapFragment.getMapAsync(this);

        findViewById(R.id.expected_profit_table_layout).setVisibility(View.INVISIBLE);

        mTableLayout = findViewById(R.id.expected_profit_table);
        acCurrentCity = findViewById(R.id.expected_profit_current_location);

        currentCity = prefManager.getCity();
        acCurrentCity.setText(currentCity);

        getCurrentCityLatLng(currentCity);

        mMandiDistance = new HashMap<>();
        mMandiLatLng = new ArrayMap<>();
        mModalPrices = new HashMap<>();

        Intent intent = getIntent();

        cropName = intent.getStringExtra(KEY_COMMODITY_NAME);
        cropVariety = intent.getStringExtra(KEY_COMMODITY_VARIETY);
        cropGrade = intent.getStringExtra(KEY_COMMODITY_GRADE);

        cropQuantity = intent.getIntExtra(KEY_COMMODITY_QUANTITY, 0);

        acCurrentCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                InputMethodManager imm = (InputMethodManager) getApplicationContext()
                        .getSystemService(Context.INPUT_METHOD_SERVICE);

                assert imm != null;
                imm.hideSoftInputFromWindow(acCurrentCity.getWindowToken(), 0);

                currentCity = acCurrentCity.getText().toString().trim();

                getCurrentCityLatLng(currentCity);
            }
        });

        findViewById(R.id.expected_profit_switch_map).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGoogleMap = !showGoogleMap;
                switchRepresentation();

            }
        });

        fnGetCities();

        fnGetMandi();

    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissProgressDialog();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);

        if (originLatLan != null) {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(originLatLan, 5));

        } else {
            LatLng LatLngNewDelhi = new LatLng(28.7041, 77.1025);
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLngNewDelhi, 5));
        }

        setMapInfoAdapter();

    }

    private void setMapInfoAdapter() {
        mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                LinearLayout info = new LinearLayout(ExpectedProfitActivity.this);
                info.setOrientation(LinearLayout.VERTICAL);

                TextView title = new TextView(ExpectedProfitActivity.this);
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());

                TextView snippet = new TextView(ExpectedProfitActivity.this);
                snippet.setTextColor(Color.GRAY);
                snippet.setText(marker.getSnippet());

                info.addView(title);
                info.addView(snippet);

                return info;
            }
        });
    }

    private void switchRepresentation() {
        if (showGoogleMap) {
            ((ImageView) findViewById(R.id.expected_profit_switch_map)).setImageResource(R.drawable.ic_view_list_black);
            findViewById(R.id.expected_profit_map_layout).setVisibility(View.VISIBLE);
            findViewById(R.id.expected_profit_table_layout).setVisibility(View.INVISIBLE);

        } else {
            ((ImageView) findViewById(R.id.expected_profit_switch_map)).setImageResource(R.drawable.ic_map_black);
            findViewById(R.id.expected_profit_map_layout).setVisibility(View.INVISIBLE);
            findViewById(R.id.expected_profit_table_layout).setVisibility(View.VISIBLE);

        }
    }

    void calculateDistances() {
        showProgressDialog();

        mMandiDistance.clear();

        for (Map.Entry<String, LatLng> entry : mMandiLatLng.entrySet()) {
            findDistance(entry.getKey(), mMandiLatLng.get(entry.getKey()));
        }

        populateData(2000);
    }

    private int expectedArrivalDay(int distance) {
        return distance / SHIPMENT_MOVEMENT_PER_DAY;
    }

    void getCurrentCityLatLng(String city) {
        //String url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + city;
        String url = Config.GOOGLE_MAP + city;
        //+ "&key=" + ;

        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //GeneralUtils.showLog(response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getString("status").equals("OK")) {
                                JSONObject geometry = jsonObject.getJSONArray("results")
                                        .getJSONObject(0).getJSONObject("geometry");

                                JSONObject location = geometry.getJSONObject("location");

                                originLatLan = new LatLng(location.getDouble("lat"),
                                        location.getDouble("lng"));

                                if (mGoogleMap != null)
                                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(originLatLan, 5));

                                calculateDistances();

                                Utils.showLog(originLatLan.toString());

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        AppController.getInstance().addToRequestQueue(request);
    }

    void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.please_wait));
        }

        if (!mProgressDialog.isShowing()) mProgressDialog.show();
    }

    void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    void addTableRow(String mandi, JSONArray jsonArray) {
        TableRow tableRow = new TableRow(this);

        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);

        tableRow.setLayoutParams(layoutParams);

        TextView tvMandi = new TextView(this);
        tvMandi.setText(mandi);
        tvMandi.setPadding(16, 16, 16, 16);
        tvMandi.setTextSize(16);
        tvMandi.setGravity(Gravity.CENTER);

        tableRow.addView(tvMandi);

        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                TextView tvProfit = new TextView(this);
                tvProfit.setText(jsonArray.getString(i));
                tvProfit.setPadding(24, 24, 24, 24);
                tvProfit.setTextSize(16);
                tvProfit.setGravity(Gravity.CENTER);

                tableRow.addView(tvProfit);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (mTableLayout.getChildCount() % 2 == 0) {
            tableRow.setBackgroundColor(getResources().getColor(R.color.btnVik_color_silver));
        }

        mTableLayout.addView(tableRow);
    }

    private void addMarker(String title, String date, String price, String profit, int distance, LatLng latLng) {
        mGoogleMap.addMarker(new MarkerOptions().snippet(utils.getLocalizedString(R.string.shipment_reach_date) + date + "\n" +
                utils.getLocalizedString(R.string.expected_price_kg) + Double.valueOf(price) / 100 + "\n" +
                utils.getLocalizedString(R.string.expected_shipping_cost_kg) + formatDecimal(SHIPMENT_COST_PER_KM_PER_KG * distance) + "\n" +
                utils.getLocalizedString(R.string.expected_profit_kg) + profit + "\n" +
                utils.getLocalizedString(R.string.total_expected_profit) + formatDecimal(Double.valueOf(price) * distance / 100))
                .title(title)
                .position(latLng)
                .icon(getMarkerIcon(Double.valueOf(profit) > 0d ? colorGreen : colorRed)));

    }

    private String formatDecimal(Double value) {
        return String.format(Locale.UK, "%.2f", value);
    }

    private BitmapDescriptor getMarkerIcon(String color) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }

    void populateData(int delayMillis) {

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM", Locale.getDefault());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mTableLayout.removeAllViews();

                mGoogleMap.clear();

                addHeader();


                for (Map.Entry<String, List<String>> entry : mModalPrices.entrySet()) {
                    int distance;
                    try {
                        distance = mMandiDistance.get(entry.getKey());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                        continue;
                    }

                    JSONArray jsonArray = new JSONArray();

                    for (String price : entry.getValue()) {
                        jsonArray.put(calculateProfit(price, distance));
                    }

                    addTableRow(entry.getKey(), jsonArray);


                    Calendar calendar = Calendar.getInstance();

                    int arrivalDay = expectedArrivalDay(distance);
                    calendar.add(Calendar.DATE, arrivalDay);

                    if (arrivalDay >= 5) continue;

                    try {
                        addMarker(entry.getKey(), simpleDateFormat.format(calendar.getTime()),
                                entry.getValue().get(arrivalDay), jsonArray.getString(arrivalDay),
                                distance, mMandiLatLng.get(entry.getKey()));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }

                }
                dismissProgressDialog();
            }
        }, delayMillis);
    }

    void addHeader() {
        JSONArray header = new JSONArray();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM", Locale.getDefault());

        Calendar calendar = Calendar.getInstance();
        header.put(simpleDateFormat.format(calendar.getTime()));

        calendar.add(Calendar.DATE, 1);
        header.put(simpleDateFormat.format(calendar.getTime()));

        calendar.add(Calendar.DATE, 1);
        header.put(simpleDateFormat.format(calendar.getTime()));

        calendar.add(Calendar.DATE, 1);
        header.put(simpleDateFormat.format(calendar.getTime()));

        calendar.add(Calendar.DATE, 1);
        header.put(simpleDateFormat.format(calendar.getTime()));

        addTableRow(utils.getLocalizedString(R.string.mandi_prices_on).toString(), header);
    }

    String calculateProfit(String price, int distance) {

        // price is for 100 kilo so divide by 100 for per KG
        double expectedPrice = Double.valueOf(price) / 100 - SHIPMENT_COST_PER_KM_PER_KG * distance;

        return formatDecimal(expectedPrice);
    }


    private void fnGetCities() {
        try {
            JSONArray jsonArray = new JSONArray(Utils.loadJSONFromAsset(this));
            mCities = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                final JSONObject e = jsonArray.getJSONObject(i);
                String name = e.getString("name");
                mCities.add(name);
            }

            ArrayAdapter<String> citiesAdapter = new ArrayAdapter<>(this,
                    android.R.layout.simple_list_item_1, mCities);

            acCurrentCity.setAdapter(citiesAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    void findDistance(final String mandi, LatLng latLng) {

        if (latLng == null || originLatLan == null) return;

        if (originLatLan.latitude == latLng.latitude && originLatLan.longitude == latLng.longitude) {
            mMandiDistance.put(mandi, 0);
            return;
        }

        String distanceUrl = Config.DISTANCE_URL +
                originLatLan.latitude + "," + originLatLan.longitude +
                "&destinations=" + latLng.latitude + "," + latLng.longitude + "&sensor=false&mode=driving";
        //"&key=" + getString(R.string.google_app_id);

        StringRequest routeRequest = new StringRequest(Request.Method.GET, distanceUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getString("status").equals("OK")) {

                                JSONObject routes = jsonObject.getJSONArray("rows").getJSONObject(0);

                                JSONObject distance = routes.getJSONArray("elements").getJSONObject(0)
                                        .getJSONObject("distance");

                                int value = distance.getInt("value"); // value is in meters

                                mMandiDistance.put(mandi, value / 1000); // convert to KM

                                //GeneralUtils.showLog(mandi + "  :" + String.valueOf(value / 1000) + " KM");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        AppController.getInstance().addToRequestQueue(routeRequest);
    }

    void fnGetMandi() {
        /*showProgressDialog();

        // GET_MANDI_LOCATION_AND_PRICE was "get_mandi_location.php"
        StringRequest request = new StringRequest(Request.Method.POST, Config.GET_MANDI_LOCATION_AND_PRICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            if (jsonObject.getInt("error_code") == 100) {
                                JSONArray mandis = jsonObject.getJSONArray("data");

                                for (int i = 0; i < mandis.length(); i++) {
                                    JSONObject mandi = mandis.getJSONObject(i);

                                    List<String> prices = new ArrayList<>();
                                    prices.add(mandi.getString("list_modal_price"));
                                    prices.add(mandi.getString("list_modal_price"));
                                    prices.add(mandi.getString("list_modal_price"));
                                    prices.add(mandi.getString("list_modal_price"));
                                    prices.add(mandi.getString("list_modal_price"));

                                    mModalPrices.put(mandi.getString("mandi_name_en"), prices);

                                    LatLng latLng = new LatLng(mandi.getDouble("mandi_lat"), mandi.getDouble("mandi_lng"));

                                    mMandiLatLng.put(mandi.getString("mandi_name_en"), latLng);

                                }
                                calculateDistances();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("api_token", mUserProfile.getApiToken());

                params.put("commodity_name", cropName);
                params.put("commodity_variety", cropVariety);

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(request);*/
    }


}
