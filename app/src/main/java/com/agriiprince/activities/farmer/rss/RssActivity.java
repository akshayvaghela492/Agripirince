package com.agriiprince.activities.farmer.rss;

import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.agriiprince.R;
import com.agriiprince.adapter.RssFeedListAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.databinding.ActivityRssFeedBinding;
import com.agriiprince.fragments.rss.RssDetailsFragment;
import com.agriiprince.model.RssFeedModel;
import com.agriiprince.model.RssModel;
import com.agriiprince.utils.DateConvertUtils;
import com.agriiprince.utils.StringUtils;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.agriiprince.utils.Utils.showLog;

public class RssActivity extends AppCompatActivity {

    public static final String RSS_FEED_MODEL = "rss_feed_model";
    public static final String RSS_MODEL = "rss_model";

    ActivityRssFeedBinding binding;
    StringUtils stringUtils;
    DateConvertUtils dateConvertUtils;

    boolean isFeedList = true;

    List<RssFeedModel> mFeedModelList;
    RssFeedListAdapter adapter;
    RssFeedModel rssFeedModel;
    RssModel rssModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stringUtils = AppController.getInstance().getStringUtils();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_rss_feed);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(stringUtils.getLocalizedString(R.string.daily_farming_news_advisory));

        dateConvertUtils = new DateConvertUtils();
        mFeedModelList = new ArrayList<>();

        adapter = new RssFeedListAdapter(this, mFeedModelList);
        adapter.addOnClickListener(onClickRssFeedItemListener);

        binding.rvRssTime.setLayoutManager(new LinearLayoutManager(this));
        binding.rvRssTime.setAdapter(adapter);

        if (getIntent().hasExtra(RSS_FEED_MODEL)) {
            isFeedList = false;
            rssFeedModel = getIntent().getParcelableExtra(RSS_FEED_MODEL);
            startRssDetailsFragment();
        } else if (getIntent().hasExtra(RSS_MODEL)) {
            isFeedList = true;
            rssModel = getIntent().getParcelableExtra(RSS_MODEL);
            new FetchFeedTask(rssModel.rss_feed_description, rssModel.rss_feed_link).execute();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_rss_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void startRssDetailsFragment() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        String date = simpleDateFormat.format(rssFeedModel.getDate());

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        fragmentTransaction.add(binding.rssFrame.getId(),
                RssDetailsFragment.newInstance(
                        rssFeedModel.getSource(),
                        rssFeedModel.getTitle(),
                        rssFeedModel.getLink(),
                        date,
                        rssFeedModel.getImg(),
                        rssFeedModel.getDescription()));

        if (isFeedList)
            fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();
    }


    private void showProgressBar() {
        binding.progress.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        binding.progress.setVisibility(View.GONE);
    }

    private void showEmptyText() {
        showEmptyText(R.string.no_news_available_now);
    }

    private void showEmptyText(@StringRes int stringId) {
        binding.emptyText.setText(stringUtils.getLocalizedString(stringId));
        binding.emptyText.setVisibility(View.VISIBLE);
    }

    private void hideEmptyText() {
        binding.emptyText.setVisibility(View.GONE);

    }

    private class FetchFeedTask extends AsyncTask<Void, Void, List<RssFeedModel>> {

        String TAG = "rss-log";
        String source;

        private String urlLink;

        public FetchFeedTask(String s, String link) {
            source = s;
            urlLink = link;
        }

        @Override
        protected List<RssFeedModel> doInBackground(Void... voids) {
            try {
                if (!urlLink.startsWith("http://") && !urlLink.startsWith("https://"))
                    urlLink = "http://" + urlLink;

                showLog("Fetching feed from " + source);
                URL url = new URL(urlLink);
                InputStream inputStream = url.openConnection().getInputStream();
                showLog("Fetch complete. Begin Parsing");

                List<RssFeedModel> ls = dateConvertUtils.parseFeed(source, inputStream);
                showLog("parsing finsihed. Size of parsed items : " + ls.size());
                if (ls.size() > 0) {
                    for (RssFeedModel model : ls) {
                        model.setTopics(rssModel.categories);
                    }

                    return ls;
                }

                Log.d(TAG, source + "   :   true");

                return null;
            } catch (IOException e) {
                Log.e(TAG, "Error", e);
            } catch (XmlPullParserException e) {
                Log.e(TAG, "Error", e);
            }
            Log.d(TAG, source + "   :   false");
            return null;
        }

        @Override
        protected void onPostExecute(List<RssFeedModel> result) {
            hideProgressBar();
            if (result != null) {
                adapter.addAll(result);
            }
            if (adapter.getItemCount() == 0)
                showEmptyText(R.string.no_news_available_now);
        }
    }

    private RssFeedListAdapter.OnClickRssFeedItemListener onClickRssFeedItemListener = new RssFeedListAdapter.OnClickRssFeedItemListener() {
        @Override
        public void onClickItem(int position) {
            rssFeedModel = adapter.get(position);

            startRssDetailsFragment();
        }
    };

}
