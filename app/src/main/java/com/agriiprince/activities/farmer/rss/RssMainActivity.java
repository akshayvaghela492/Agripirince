package com.agriiprince.activities.farmer.rss;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.agriiprince.R;
import com.agriiprince.activities.TutorialActivity;
import com.agriiprince.adapter.ViewPagerAdapter;
import com.agriiprince.mvvm.applevel.analytics.FarmerRealtimeDBAnalytics;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.databinding.ActivityRssMainBinding;
import com.agriiprince.fragments.rss.RssArrangeByProvider;
import com.agriiprince.fragments.rss.RssArrangeByTime;
import com.agriiprince.fragments.rss.RssSubscribeFragment;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.utils.StringUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class RssMainActivity extends AppCompatActivity implements RssSubscribeFragment.OnRssSubscribeInteractionListener {
    private final String TAG = RssMainActivity.class.getSimpleName();

    private StringUtils utils;

    private ActivityRssMainBinding binding;

    private ViewPagerAdapter adapter;

    UserProfile mUserProfile;
    FirebaseDatabase mFDatabase;
    DatabaseReference mRef;
    DatabaseReference mFarmersRef;
    DatabaseReference mNumRef;
    DatabaseReference mKeyRef;
    long mTimeStart;
    String mUserId;

    ProgressDialog progressDialog;

    @Override
    public void showProgress() {
        showProgressBar();
    }

    @Override
    public void dismissProgress() {
        hideProgressBar();
    }

    @Override
    public void onChangeLabel(String label) {
    }

    @Override
    public void onSuccess() {
        binding.tabLayout.setVisibility(View.VISIBLE);
        setupViewPager();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        utils = AppController.getInstance().getStringUtils();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_rss_main);

        mUserProfile = new UserProfile(this);
        mUserId = mUserProfile.getUserId();
        mFDatabase = FirebaseDatabase.getInstance();
        mRef = mFDatabase.getReference("analytics");
        mFarmersRef = mRef.child("farmers");
        mNumRef = mFarmersRef.child(mUserProfile.getUserMobile());

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(utils.getLocalizedString(R.string.daily_farming_news_advisory));

        ImageView imgview_help =  findViewById(R.id.iv_help);

        imgview_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RssMainActivity.this, TutorialActivity.class);
                intent.putExtra("code", "FARMN");
                startActivity(intent);
            }
        });

        setupViewPager();
        binding.tabLayout.setupWithViewPager(binding.rssViewpager);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0)
            binding.tabLayout.setVisibility(View.VISIBLE);

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_rss_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        } else if (id == R.id.menu_rss_subscription) {
            binding.tabLayout.setVisibility(View.GONE);
            startRssSubscribe();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void startRssSubscribe() {
        getSupportFragmentManager().beginTransaction()
                .add(binding.rssFrameMain.getId(), RssSubscribeFragment.newInstance(), RssSubscribeFragment.class.getSimpleName())
                .addToBackStack(RssSubscribeFragment.class.getSimpleName())
                .commit();
    }

    private void setupViewPager() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(RssArrangeByProvider.newInstance(), utils.getLocalizedString(R.string.arrange_by_provider).toString());
        adapter.addFragment(RssArrangeByTime.newInstance(), utils.getLocalizedString(R.string.arrange_by_time).toString());

        binding.rssViewpager.setAdapter(adapter);
    }

    private void showProgressBar() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle(utils.getLocalizedString(R.string.please_wait));
        }

        progressDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTimeStart = System.currentTimeMillis();
    }

    @Override
    protected void onStop() {
        super.onStop();
        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - mTimeStart;
        final int elapsedSeconds = (int)(tDelta / 1000.0);

        Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    FarmerRealtimeDBAnalytics.saveEventFarmerTimeSpent(
                            dataSnapshot, "time_spent_news", mNumRef, elapsedSeconds);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void hideProgressBar() {
        progressDialog.dismiss();
    }
}
