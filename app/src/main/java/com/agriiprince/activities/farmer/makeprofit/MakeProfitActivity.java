package com.agriiprince.activities.farmer.makeprofit;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.mvvm.applevel.analytics.FarmerRealtimeDBAnalytics;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.dataservice.CropDataService;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.mvvm.model.crop.Crop;
import com.agriiprince.utils.StringUtils;
import com.agriiprince.utils.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class MakeProfitActivity extends AppCompatActivity {

    private static final String TAG = MakeProfitActivity.class.getName();

    public static final String KEY_COMMODITY_NAME = "commodity_name";
    public static final String KEY_COMMODITY_VARIETY = "commodity_variety";
    public static final String KEY_COMMODITY_GRADE = "commodity_grade";
    public static final String KEY_COMMODITY_QUANTITY = "commodity_quantity";

    private List<Crop> crops;

    private AppCompatAutoCompleteTextView etCropName;
    private AppCompatSpinner spCropVariety, spCropGrade, spUnits;

    private EditText etCropQuantity;

    private List<String> mGrade, mUnits;
    //private List<String> mCommodityNamesArray;
    //private Map<String, List<String>> mCommodityVarietyMap;

    private ProgressDialog mProgressDialog;

    UserProfile mUserProfile;
    FirebaseDatabase mFDatabase;
    DatabaseReference mRef;
    DatabaseReference mFarmersRef;
    DatabaseReference mNumRef;
    DatabaseReference mKeyRef;
    long mTimeStart;
    String mUserId;

    StringUtils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_profit);

        mUserProfile = new UserProfile(this);
        mUserId = mUserProfile.getUserId();
        mFDatabase = FirebaseDatabase.getInstance();
        mRef = mFDatabase.getReference("analytics");
        mFarmersRef = mRef.child("farmers");
        mNumRef = mFarmersRef.child(mUserProfile.getUserMobile());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        utils = AppController.getInstance().getStringUtils();

        TextView textView = findViewById(R.id.coming_soon);
        textView.setText(utils.getLocalizedString(R.string.coming_soon_for_premium_subscription));

        mGrade = Utils.getCommodityGrades();
        mUnits = Utils.getWeightUnits();

        etCropName = findViewById(R.id.et_make_profit_crop_name);

        spCropVariety = findViewById(R.id.sp_make_profit_crop_variety);
        spCropGrade = findViewById(R.id.sp_make_profit_crop_grade);
        spUnits = findViewById(R.id.sp_make_profit_quantity_units);

        etCropQuantity = findViewById(R.id.et_make_profit_crop_quantity);

        Button btnCheckPrices = findViewById(R.id.btn_make_profit_check_prices);
        btnCheckPrices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateInputs()) {
                    Intent intent = new Intent(MakeProfitActivity.this, ExpectedPricesActivity.class);

                    intent.putExtra(KEY_COMMODITY_NAME, etCropName.getText().toString());
                    intent.putExtra(KEY_COMMODITY_VARIETY, spCropVariety.getSelectedItem().toString());
                    intent.putExtra(KEY_COMMODITY_GRADE, spCropGrade.getSelectedItem().toString());
                    intent.putExtra(KEY_COMMODITY_QUANTITY, convertQuantity(Integer.valueOf(etCropQuantity.getText().toString())));

                    startActivity(intent);

                    MakeProfitActivity.this.finish();
                }
            }
        });

        etCropName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setCommodityVarietyAdapter(position);
                etCropName.setImeOptions(EditorInfo.IME_ACTION_NEXT);
                MakeProfitActivity.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
        });

        spCropVariety.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setCommodityGradeAdapter();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // TODO remove
        //fnGetCropNames();

        setWeightUnitAdapter();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPause() {
        super.onPause();
        dismissProgressDialog();
    }

    void setCommodityNameAdapter() {
        ArrayAdapter<String> nameAdapter = new ArrayAdapter<>(MakeProfitActivity.this,
                android.R.layout.simple_list_item_1, Crop.getNames(crops));

        etCropName.setAdapter(nameAdapter);
        etCropName.showDropDown();
    }

    void setCommodityVarietyAdapter(int selectedCropPosition) {
        List<String> cropNames = crops.get(selectedCropPosition).getVarietyNames();

        ArrayAdapter<String> varietyAdapter = new ArrayAdapter<>(MakeProfitActivity.this,
                android.R.layout.simple_spinner_item, cropNames);

        varietyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spCropVariety.setAdapter(varietyAdapter);
        spCropVariety.setSelection(0);
    }

    private void setCommodityGradeAdapter() {
        ArrayAdapter<String> gradeAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, mGrade);

        gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spCropGrade.setAdapter(gradeAdapter);
        spCropGrade.setSelection(0);
    }

    private void setWeightUnitAdapter() {
        ArrayAdapter<String> unitsAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, mUnits);

        unitsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spUnits.setAdapter(unitsAdapter);
        spUnits.setSelection(0);

    }

    void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(MakeProfitActivity.this);
            mProgressDialog.setMessage(getString(R.string.please_wait));
        }

        if (!mProgressDialog.isShowing()) {
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
    }

    void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    private int convertQuantity(int value) {
        switch (spUnits.getSelectedItemPosition()) {
            case 0:
                return value; // in kilos no change
            case 1:
                return value * 100; // converting quintal to kilo
            case 2:
                return value * 1000; // ton to kilo
            default:
                return value;
        }
    }

    boolean validateInputs() {
        try {
            if (etCropName.getText().toString().isEmpty()) {
                Utils.showToast(MakeProfitActivity.this, utils.getLocalizedString(R.string.please_select_the_crop_name).toString());
                return false;
            } else if (spCropVariety.getSelectedItem().toString().isEmpty()) {
                Utils.showToast(MakeProfitActivity.this, utils.getLocalizedString(R.string.please_select_the_variety_name).toString());
                return false;
            } else if (spCropGrade.getSelectedItem().toString().isEmpty()) {
                Utils.showToast(MakeProfitActivity.this, utils.getLocalizedString(R.string.please_select_the_crop_grade).toString());
                return false;
            } else if (etCropQuantity.getText().toString().isEmpty() || Integer.parseInt(etCropQuantity.getText().toString()) <= 0) {
                Utils.showToast(MakeProfitActivity.this, utils.getLocalizedString(R.string.please_select_the_quantity).toString());
                return false;
            }

        } catch (NumberFormatException e) {
            e.printStackTrace();
            Utils.showToast(this, utils.getLocalizedString(R.string.please_select_the_quantity).toString());

            return false;
        }

        return true;
    }


    /**
     * make volley string request to web services to fetch cropName names and varieties for each cropName.
     * cropName names are added to the list and used to autocomplete the text input
     * and varieties are used to autocomplete
     */
    void fnGetCropNames() {
        showProgressDialog();

        CropDataService cropDataService = new CropDataService(this);
        cropDataService.getCrops(new CropDataService.OnCropDataRequest() {
            @Override
            public void onCropDataResults(List<Crop> crops) {
                MakeProfitActivity.this.crops = crops;
                setCommodityNameAdapter();
                dismissProgressDialog();
            }

            @Override
            public void onCropDataError() {
                Toast.makeText(MakeProfitActivity.this, utils.getLocalizedString(R.string.data_not_found), Toast.LENGTH_SHORT).show();
                dismissProgressDialog();
            }
        });
/*
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_CROP_LIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray responseArray = new JSONArray(response);
                            JSONObject responseObject = responseArray.getJSONObject(0);

                            if (responseObject.getInt("error_code") == 100) {
                                JSONArray dataArray = responseObject.getJSONArray("data");

                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject userJsonObject = dataArray.getJSONObject(i);

                                    List<String> varieties = new ArrayList<>();

                                    JSONArray varietyArray = userJsonObject.getJSONArray("varietyName");
                                    for (int j = 0; j < varietyArray.length(); j++) {
                                        varieties.add(varietyArray.getString(j));
                                    }

                                    mCommodityVarietyMap.put(userJsonObject.getString("commodity_name"), varieties);
                                    mCommodityNamesArray.add(userJsonObject.getString("commodity_name"));
                                }
                                setCommodityNameAdapter();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();

                        GeneralUtils.showToast(MakeProfitActivity.this, getString(R.string.error_label));
                    }
                });

        AppController.getInstance().addToRequestQueue(stringRequest);*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTimeStart = System.currentTimeMillis();
    }

    @Override
    protected void onStop() {
        super.onStop();
        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - mTimeStart;
        final int elapsedSeconds = (int)(tDelta / 1000.0);

        Query q  = mFarmersRef.orderByChild("_id").equalTo(mUserId);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    FarmerRealtimeDBAnalytics.saveEventFarmerTimeSpent(
                            dataSnapshot, "time_spent_make_profit", mNumRef, elapsedSeconds);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

}
