package com.agriiprince.activities.farmer.makeprofit;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.agriiprince.R;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.utils.StringUtils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static com.agriiprince.activities.farmer.makeprofit.MakeProfitActivity.KEY_COMMODITY_GRADE;
import static com.agriiprince.activities.farmer.makeprofit.MakeProfitActivity.KEY_COMMODITY_NAME;
import static com.agriiprince.activities.farmer.makeprofit.MakeProfitActivity.KEY_COMMODITY_QUANTITY;
import static com.agriiprince.activities.farmer.makeprofit.MakeProfitActivity.KEY_COMMODITY_VARIETY;

public class ExpectedPricesActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static String KEY_MANDI_NAMES = "mandi_names";
    public static String KEY_MANDI_PRICES = "mandi_prices";

    private UserProfile mUserProfile;

    private TableLayout mTableLayout;

    private ProgressDialog mProgressDialog;

    private String cropName, cropVariety, cropGrade;
    private int cropQuantity;
    private StringUtils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expected_prices);
        utils = AppController.getInstance().getStringUtils();

        mUserProfile = new UserProfile(this);

        mTableLayout = findViewById(R.id.expected_prices_table_layout);

        Intent intent = getIntent();

        cropName = intent.getStringExtra(KEY_COMMODITY_NAME);
        cropVariety = intent.getStringExtra(KEY_COMMODITY_VARIETY);
        cropGrade = intent.getStringExtra(KEY_COMMODITY_GRADE);

        cropQuantity = intent.getIntExtra(KEY_COMMODITY_QUANTITY, 0);

        Button btnCheckProfits = findViewById(R.id.expected_prices_btn_check_profits);
        btnCheckProfits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ExpectedPricesActivity.this, ExpectedProfitActivity.class);

                intent.putExtra(KEY_COMMODITY_NAME, cropName);
                intent.putExtra(KEY_COMMODITY_VARIETY, cropVariety);
                intent.putExtra(KEY_COMMODITY_GRADE, cropGrade);
                intent.putExtra(KEY_COMMODITY_QUANTITY, cropQuantity);

                startActivity(intent);

            }
        });

        addHeader();

        populateForTesting(); // remove after implementing web services

        fnGetPredictedPrices();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    void addHeader() {
        JSONArray header = new JSONArray();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM", Locale.getDefault());

        Calendar calendar = Calendar.getInstance();
        header.put(simpleDateFormat.format(calendar.getTime()));

        calendar.add(Calendar.DATE, 1);
        header.put(simpleDateFormat.format(calendar.getTime()));

        calendar.add(Calendar.DATE, 1);
        header.put(simpleDateFormat.format(calendar.getTime()));

        calendar.add(Calendar.DATE, 1);
        header.put(simpleDateFormat.format(calendar.getTime()));

        calendar.add(Calendar.DATE, 1);
        header.put(simpleDateFormat.format(calendar.getTime()));

        addTableRow(utils.getLocalizedString(R.string.mandi_prices_on).toString(), header);
    }


    @Override
    protected void onPause() {
        super.onPause();
        dismissProgressDialog();
    }


    /**
     * //TODO remove this method
     */
    void populateForTesting() {

        /*
        showProgressDialog();

        // GET_MANDI_LOCATION_AND_PRICE was "get_mandi_location.php"
        StringRequest request = new StringRequest(Request.Method.POST, Config.GET_MANDI_LOCATION_AND_PRICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            if (jsonObject.getInt("error_code") == 100) {
                                JSONArray mandis = jsonObject.getJSONArray("data");

                                for (int i = 0; i < mandis.length(); i++) {
                                    JSONObject mandi = mandis.getJSONObject(i);

                                    String price = mandi.getString("list_modal_price");

                                    JSONArray prices = new JSONArray();
                                    prices.put(price);
                                    prices.put(price);
                                    prices.put(price);
                                    prices.put(price);
                                    prices.put(price);

                                    addTableRow(mandi.getString("mandi_name_en"), prices);
                                }
                            }

                            dismissProgressDialog();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            dismissProgressDialog();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                        GeneralUtils.showToast(ExpectedPricesActivity.this, getString(R.string.error_label));
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("api_token", mUserProfile.getApiToken());

                params.put("commodity_name", cropName);
                params.put("commodity_variety", cropVariety);

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(request);*/
    }

    void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.please_wait));
        }

        if (!mProgressDialog.isShowing()) mProgressDialog.show();
    }

    void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    private void populatePrices(JSONArray jsonArray) {
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                String mandi = jsonObject.getString(KEY_MANDI_NAMES);

                addTableRow(mandi, jsonObject.getJSONArray(KEY_MANDI_PRICES));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void addTableRow(String mandi, JSONArray jsonArray) {
        TableRow tableRow = new TableRow(this);

        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);

        tableRow.setLayoutParams(layoutParams);

        TextView tvMandi = new TextView(this);
        tvMandi.setText(mandi);
        tvMandi.setPadding(16, 16, 16, 16);
        tvMandi.setTextSize(16);
        tvMandi.setGravity(Gravity.CENTER);

        tableRow.addView(tvMandi);

        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                TextView tvPrice = new TextView(this);
                tvPrice.setText(jsonArray.getString(i));
                tvPrice.setPadding(24, 24, 24, 24);
                tvPrice.setTextSize(16);
                tvPrice.setGravity(Gravity.CENTER);

                tableRow.addView(tvPrice);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (mTableLayout.getChildCount() % 2 == 0) {
            tableRow.setBackgroundColor(getResources().getColor(R.color.btnVik_color_silver));
        }

        mTableLayout.addView(tableRow);
    }

    private void fnGetPredictedPrices() {

    }
}
