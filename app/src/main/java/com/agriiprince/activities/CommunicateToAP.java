package com.agriiprince.activities;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.adapter.ChatMessageAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.ChatMessage;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.chat.SendMessageResponse;
import com.agriiprince.mvvm.retrofit.service.Chat;
import com.agriiprince.mvvm.util.Logs;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.agriiprince.utils.Utils.showLog;
import static com.agriiprince.utils.Utils.showToast;

public class CommunicateToAP extends AppCompatActivity implements OnClickListener {

    String[] listItem = {"Message 1", "Message 2", "Message 3", "Message 4", "Message 5",
            "Message 6", "Message 7", "Message 8", "Message 9", "Message 10", "Message 11", "Message 12"};

    RecyclerView rvMessages;
    ImageView ivSend;
    EditText etMessage;
    PrefManager pref;
    //Broadcast receiver to receive broadcasts
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    //ArrayList of messages to store the thread messages
    private ArrayList<ChatMessage> messages;

    private UserProfile mUserProfile;

    //This method will return current timestamp
    public static String getTimeStamp() {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(new Date());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communicate_to_ap);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Initializing recycler view
        rvMessages = findViewById(R.id.rvMessages);
        rvMessages.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvMessages.setLayoutManager(layoutManager);

        mUserProfile = new UserProfile(this);
        //Initializing message arraylist
        messages = new ArrayList<>();

        pref = new PrefManager(this);

        //Calling function to fetch the existing messages on the thread
        fetchMessages();

        ivSend = findViewById(R.id.ivSend);

        //initializing button and edittext
        ivSend = findViewById(R.id.ivSend);
        etMessage = findViewById(R.id.etMessage);

        //Adding listener to button
        ivSend.setOnClickListener(this);

        //Creating broadcast receiver
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
/*        if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_SUCCESS)) {

          //When gcm registration is success do something here if you need

        } else if (intent.getAction()
            .equals(GCMRegistrationIntentService.REGISTRATION_TOKEN_SENT)) {

          //When the registration token is sent to ther server displaying a toast
          Toast.makeText(getApplicationContext(), "Chatroom Ready...", Toast.LENGTH_SHORT).show();

          //When we received a notification when the app is in foreground
        } else if (intent.getAction().equals(Constants.PUSH_NOTIFICATION)) {
          //Getting message data
          String name = intent.getStringExtra("name");
          String message = intent.getStringExtra("message");
          String id = intent.getStringExtra("id");

          //processing the message to add it in current thread
          processMessage(name, message, id);
        }*/
                if (intent.getAction().equalsIgnoreCase(Config.NOTIFICATION_TAG)) {
                    showLog("Communicate to AP : " + "inside onReceive");
                    //Getting message data
                    //String name = intent.getStringExtra("name");
                    String message = intent.getStringExtra("message");
                    //String id = intent.getStringExtra("id");

                    //processing the message to add it in current thread
                    processMessage(message);
                }
            }
        };

        //if the google play service is not in the device app won't work
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

        if (ConnectionResult.SUCCESS != resultCode) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(),
                        "Google Play Service is not install/enabled in this device!", Toast.LENGTH_LONG).show();
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());

            } else {
                Toast.makeText(getApplicationContext(),
                        "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
            }
        } else {
            //Intent itent = new Intent(this, GCMRegistrationIntentService.class);
            //startService(itent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //getting data from broad cast of fcm and adding to list
    private void processMessage(String message) {
        ChatMessage chatMessage = new ChatMessage("-1", "AP (0)", mUserProfile.getUserId(), message, getTimeStamp());
        chatMessage.setChat_type("personal");
        messages.add(chatMessage);
        scrollToBottom();
    }

    //This method will fetch all the messages of the thread
    private void fetchMessages() {
        Log.d("fetch msg url", Config.GET_CHAT_MESSAGE_AT_START_UP);
        StringRequest stringRequest = new StringRequest(Method.POST,
                Config.GET_CHAT_MESSAGE_AT_START_UP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY", response);
                        //dialog.dismiss();
                        JSONArray array;
                        try {
                          /*  array = new JSONArray(response);
                            JSONObject object = array.getJSONObject(0);
                            int error = object.getInt("error_code");
                            if (error == 100) {
                                String data = object.getString("data");
                                JSONArray jsonArray = new JSONArray(data);
                                */
                            JSONObject object=new JSONObject(response);
                            //JSONObject responseObject = responseArray.getJSONObject(0);
                            //int errorCode = responseObject.getInt("error_code");
                            int errorCode = object.getInt("code");
                            Log.d("TESTING_VOLLEY" , errorCode+"");
                            //if (errorCode == 100)
                            if (errorCode == 200)
                            {
                                //String data = responseObject.getString("data");
                                JSONObject data=object.getJSONObject("data");
                                JSONArray dataarray=data.getJSONArray("data");
                                for (int i = 0; i < dataarray.length(); i++) {
                                    JSONObject jsonObject = dataarray.getJSONObject(i);
                                    String receiver_id = jsonObject.getString("receiver");
                                    String sender_id = jsonObject.getString("sender");
                                    String message = jsonObject.getString("message");
                                    String time = jsonObject.getString("created_on");
                                    String sender_name;
                                    if (jsonObject.getString("final_name") != null
                                            && jsonObject.getString("final_name") != "null") {
                                        sender_name = jsonObject.getString("final_name");
                                    } else {
                                        sender_name = "AP(0)";
                                    }

                                    //to do Rishijay fetch sender name and send to adapter
                                    //ask swapnil to fetch sender name from user_type and user_id
                                    ChatMessage chatMessage = new ChatMessage("-1", sender_id, receiver_id, message, time,
                                            sender_name);
                                    chatMessage.setChat_type("personal");

                                    messages.add(chatMessage);
                                }

                                adapter = new ChatMessageAdapter(messages, mUserProfile.getUserId());
                                rvMessages.setAdapter(adapter);
                                scrollToBottom();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showToast(CommunicateToAP.this, "Slow internet...Kindly check internet to proceed seamlessly.");
                        } else if (error instanceof NoConnectionError) {
                            showToast(CommunicateToAP.this, "No internet connection ");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());

                params.put("user_type", pref.getUserType());
                params.put("user_id", pref.getUserId());

                Log.d("fetch msg params", params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUserProfile.getApiToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    //This method will send the new message to the thread
    private void sendMessage() {
        final String message = etMessage.getText().toString().trim();
        if (message.equalsIgnoreCase("")) {
            showToast(this, "Can't send empty message");
            return;
        }
        //int userId = AppController.getInstance().getUserId();
        //String name = AppController.getInstance().getUserName();
        final String sentAt = getTimeStamp();

        adapter = new ChatMessageAdapter(messages, mUserProfile.getUserId());
        rvMessages.setAdapter(adapter);

        scrollToBottom();

        etMessage.setText("");

  /*      Log.d("send msg url", Config.SEND_MESSAGE_URL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.
                SEND_MESSAGE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY", "send_msg"+response);
                        //JSONArray array;
                        try {
                            //array = new JSONArray(response);
                            //JSONObject object = array.getJSONObject(0);
                            //int error = object.getInt("error_code");
                            //if (error == 100)
                            JSONObject jsonObject=new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            if(code==200)
                            {
                                ChatMessage m = new ChatMessage("-1", mUserProfile.getUserId(), "AP (0)", message, sentAt,
                                        mUserProfile.getUserName());
                                m.setChat_type("personal");

                                messages.add(m);
                                adapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());

                params.put("sender_type", pref.getUserType());
                params.put("receiver_type", "AP (0)");
                params.put("sender_id", mUserProfile.getUserId());
                params.put("receiver_id", "AP (0)");
                params.put("message", message);
                Log.d("send msg params", params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUserProfile.getApiToken());
                return header;
            }
        };

        //Disabling retry to prevent duplicate messages
        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    */
        API_Manager api_manager = new API_Manager();
        Chat retrofits=api_manager.getClient7().create(Chat.class);
        Call<SendMessageResponse> call=retrofits.send_message(pref.getAccessToken(),message,pref.getUserId(),pref.getUserType(),"AP (0)","AP (0)");
        call.enqueue(new Callback<SendMessageResponse>() {
            @Override
            public void onResponse(Call<SendMessageResponse> call, retrofit2.Response<SendMessageResponse> response) {
                Logs.d("CheckStatus", "onResponse: "+response);
                Logs.d("CheckStatus", response.body().getCode().toString());
                Logs.d("CheckStatus", response.body().getMessage());
                if(response.body()!=null && response.body().getCode() == 200)
                {
                    ChatMessage m = new ChatMessage("-1", mUserProfile.getUserId(), "AP (0)", message, sentAt,
                            mUserProfile.getUserName());
                    m.setChat_type("personal");

                    messages.add(m);
                    adapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onFailure(Call<SendMessageResponse> call, Throwable t) {
                Log.d("CheckStatus"," error "+t.getMessage());
                t.printStackTrace();
            }
        });

    }

    //method to scroll the recyclerview to bottom
    private void scrollToBottom() {
        adapter.notifyDataSetChanged();
        if (adapter.getItemCount() > 1) {
            rvMessages
                    .getLayoutManager().smoothScrollToPosition(rvMessages, null, adapter.getItemCount() - 1);
        }
    }

    //Registering broadcast receivers
    @Override
    protected void onResume() {
        super.onResume();
        Log.w("MainActivity", "onResume");
    /*LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
        new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));
    LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
        new IntentFilter(GCMRegistrationIntentService.REGISTRATION_ERROR));
    LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
        new IntentFilter(GCMRegistrationIntentService.REGISTRATION_TOKEN_SENT));*/
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.NOTIFICATION_TAG));
    }


    //Unregistering receivers
    @Override
    protected void onPause() {
        super.onPause();
        Log.w("MainActivity", "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }


    //Sending message onclick
    @Override
    public void onClick(View v) {
        if (v == ivSend) {
            sendMessage();
        }
    }

    //Creating option menu to add logout feature
  /*@Override
  public boolean onCreateOptionsMenu(Menu menu) {
    //Adding our menu to toolbar
    getMenuInflater().inflate(R.menu.menu, menu);
    return true;
  }*/

    //Adding logout option here
  /*@Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == R.id.menuLogout) {
      AppController.getInstance().logout();
      finish();
      startActivity(new Intent(this, MainActivity.class));
    }
    return super.onOptionsItemSelected(item);
  }*/

}