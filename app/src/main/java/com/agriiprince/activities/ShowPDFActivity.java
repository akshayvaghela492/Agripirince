package com.agriiprince.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.agriiprince.R;
import com.agriiprince.mvvm.ui.splash.SplashActivity;

public class ShowPDFActivity extends AppCompatActivity {

    String strUrl = "";
    String strUrlPrefix = "https://docs.google.com/gview?embedded=true&url=";
    View view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_PROGRESS);

        setContentView(R.layout.activity_show_pdf);

        view = findViewById(R.id.rlPdfA);

        WebView wv = findViewById(R.id.wv);

        //initProgressDialog();

        Intent intent = getIntent();
        if (intent.getStringExtra("url").isEmpty()) {
            Snackbar.make(view, "Something went wrong", Snackbar.LENGTH_INDEFINITE)
                    .setActionTextColor(Color.RED).setAction("Okay", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(ShowPDFActivity.this, SplashActivity.class));
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            }).show();
            //Toast.makeText(PDFActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
        } else {
            strUrl = strUrlPrefix + intent.getStringExtra("url");
            Log.d("pdf url", "" + strUrl);
            wv.loadUrl(strUrl);
        }

        //String doc = "https://docs.google.com/gview?embedded=true&url=http://www.premisafe.com/pdf.pdf";

        /*String doc="<iframe src='http://docs.google.com/viewer?url=http://www.premisafe.com/pdf.pdf&embedded=true' width='100%' height='100%'  style='border: none;'></iframe>";
        WebView wv = (WebView)findViewById(R.id.wv);
        wv.setVisibility(WebView.VISIBLE);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setAllowFileAccess(true);
        wv.getSettings().setPluginState(WebSettings.PluginState.ON);
        //wv.setWebViewClient(new Callback());
        wv.loadData(doc, "text/html", "UTF-8");*/

        // Let's display the progress in the activity title bar, like the
        // browser app does.
        wv.getSettings().setJavaScriptEnabled(true);

        final Activity activity = this;
        wv.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                activity.setProgress(progress * 1000);
                if (progress == 100) {
                    //pDialog.hide();
                }
            }
        });
        wv.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description,
                                        String failingUrl) {
                Snackbar.make(view, "Something went wrong", Snackbar.LENGTH_INDEFINITE).setActionTextColor(
                        Color.RED).setAction("Okay", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(ShowPDFActivity.this, SplashActivity.class));
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }
                }).show();
                //Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
                // pDialog.hide();
            }
        });
    }
}
