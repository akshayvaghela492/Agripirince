package com.agriiprince.activities;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.agriiprince.R;
import com.agriiprince.adapter.ChatMessageAdapter;
import com.agriiprince.appcontroller.AppController;
import com.agriiprince.mvvm.applevel.constants.Config;
import com.agriiprince.mvvm.data.prefs.PrefManager;
import com.agriiprince.mvvm.data.prefs.UserProfile;
import com.agriiprince.model.ChatMessage;
import com.agriiprince.mvvm.retrofit.API_Manager;
import com.agriiprince.mvvm.retrofit.dto.chat.MessageStatus;
import com.agriiprince.mvvm.retrofit.service.Chat;
import com.agriiprince.mvvm.util.Logs;
import com.agriiprince.utils.Utils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.agriiprince.utils.Utils.showLog;
import static com.agriiprince.utils.Utils.showToast;

public class ChatOneToOneActivity extends AppCompatActivity {

    private RecyclerView rvMessages;
    ImageView ivSend;
    EditText etMessage;
    PrefManager pref;
    String receiverId, receiverType;
    //Broadcast receiver to receive broadcasts
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ChatMessageAdapter mChatAdapter;
    //ArrayList of messages to store the thread messages
    private List<ChatMessage> messages;
    private boolean firstLaunch;
    private ArrayList<String> farmer_id;

    private UserProfile mUserProfile;

    private boolean isKeyboardOpen;

    //This method will return current timestamp
    public static String getTimeStamp() {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(new Date());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_one_to_one);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        isKeyboardOpen = false;
        mUserProfile = new UserProfile(this);
        pref = new PrefManager(this);
        try {
            if (getIntent().getBooleanExtra("call_from_send", false)) {
                farmer_id = getIntent().getStringArrayListExtra("sub_farmer_id");

            } else {
                Bundle extras = getIntent().getExtras();
                this.setTitle(extras.getString("receiver_name"));
                String call_from = extras.getString("call_from");
                if (call_from.equalsIgnoreCase("farmer")) {
                    receiverId = extras.getString("receiver_id");
                    receiverType = extras.getString("receiver_type", "farmer");
                } else if (call_from.equalsIgnoreCase("pv")) {
                    receiverId = extras.getString("receiver_id");
                    receiverType = "pv";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        messages = new ArrayList<>();
        firstLaunch = true;

        rvMessages = findViewById(R.id.rvMessages);
        rvMessages.setHasFixedSize(true);
        rvMessages.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mChatAdapter = new ChatMessageAdapter(messages, mUserProfile.getUserId());
        rvMessages.setAdapter(mChatAdapter);

        fetchMessages();

        etMessage = findViewById(R.id.etMessage);
        ivSend = findViewById(R.id.ivSend);
        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
/*        if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_SUCCESS)) {

          //When gcm registration is success do something here if you need

        } else if (intent.getAction()
            .equals(GCMRegistrationIntentService.REGISTRATION_TOKEN_SENT)) {

          //When the registration token is sent to ther server displaying a toast
          Toast.makeText(getApplicationContext(), "Chatroom Ready...", Toast.LENGTH_SHORT).show();

          //When we received a notification when the app is in foreground
        } else if (intent.getAction().equals(Constants.PUSH_NOTIFICATION)) {
          //Getting message data
          String name = intent.getStringExtra("name");
          String message = intent.getStringExtra("message");
          String id = intent.getStringExtra("id");

          //processing the message to add it in current thread
          processMessage(name, message, id);
        }*/
                if (intent.getAction().equalsIgnoreCase(Config.NOTIFICATION_TAG2)) {
                    showLog("ChatOneToOne : " + "inside onReceive");
                    //String name = intent.getStringExtra("name");
                    String message = intent.getStringExtra("message");
                    String message_id = intent.getStringExtra("message_id");
                    //String id = intent.getStringExtra("id");
                    //processing the message to add it in current thread
                    processMessage(message_id, message);
                }
            }
        };

        //if the google play service is not in the device app won't work
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (ConnectionResult.SUCCESS != resultCode) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(),
                        "Google Play Service is not install/enabled in this device!", Toast.LENGTH_LONG).show();
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());

            } else {
                Toast.makeText(getApplicationContext(),
                        "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
            }
        } else {
            //Intent itent = new Intent(this, GCMRegistrationIntentService.class);
            //startService(itent);
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //getting data from broad cast of fcm and adding to list
    private void processMessage(String message_id, String message) {
        Log.d("process one2one:     ", message);
        ChatMessage chatMessage = new ChatMessage(message_id, "" + receiverId, mUserProfile.getUserId(), message, getTimeStamp(), ChatOneToOneActivity.this.getTitle().toString());
        chatMessage.setChat_type("personal");
        chatMessage.setRead_status("N");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        String date = dateFormat.format(Calendar.getInstance().getTime());

        updateReadStatus(message_id, "no_mode", date);
        mChatAdapter.addData(chatMessage);
        scrollToBottom();
    }

    //This method will fetch all the messages of the thread
    private void fetchMessages() {
        Log.d("one2one fetch msg url", Config.GET_FARMER_ONE_TO_ONE_CHAT_MESSAGE_AT_START_UP);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.GET_FARMER_ONE_TO_ONE_CHAT_MESSAGE_AT_START_UP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY", response);
                        //dialog.dismiss();
                        //JSONArray array;
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            int error = jsonObject.getInt("code");
                            if (error == 200) {
                                messages.clear();
                                //String data = jsonObject.getString("data");
                                JSONObject dataobj=jsonObject.getJSONObject("data");
                                JSONArray dataArray=dataobj.getJSONArray("data");
                                //JSONArray jsonArray = new JSONArray(data);
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject jsonObject1 = dataArray.getJSONObject(i);
                                    String message_id = jsonObject1.getString("id");
                                    String receiver_id = jsonObject1.getString("receiver");
                                    String sender_id = jsonObject1.getString("sender");
                                    String message = jsonObject1.getString("message");
                                    String time = jsonObject1.getString("created_on");
                                    String sender_name;
                                    if (jsonObject1.getString("final_name") != null
                                            && jsonObject1.getString("final_name") != "null") {
                                        sender_name = jsonObject1.getString("final_name");
                                    } else {
                                        sender_name = "AP(0)";
                                    }

                                    //to do Rishijay fetch sender name and send to adapter
                                    //ask swapnil to fetch sender name from user_type and user_id
                                    ChatMessage chatMessage = new ChatMessage(message_id, sender_id, receiver_id, message, time, sender_name);
                                    chatMessage.setRead_status(jsonObject1.getString("read_status"));
                                    chatMessage.setChat_type("personal");

                                    messages.add(chatMessage);
                                }

                                mChatAdapter.setData(messages);
                                //scrollToBottom(jsonArray.length());
                                if (mChatAdapter.getItemCount() > 0) {
                                    if (firstLaunch) {
                                        rvMessages.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                rvMessages.smoothScrollToPosition(mChatAdapter.getItemCount() - 1);
                                            }
                                        });
                                        firstLaunch = false;
                                    } else {
                                        rvMessages.scrollToPosition(mChatAdapter.getItemCount() - 1);
                                    }

                                    updateReadStatus();
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showVolleyError(ChatOneToOneActivity.this, error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", mUserProfile.getApiToken());

                params.put("sender_type", pref.getUserType());
                if (pref.getUserType().equalsIgnoreCase("ca")) {
                    params.put("user_id", pref.getUserId());
                } else if (pref.getUserType().equalsIgnoreCase("farmer")) {
                    params.put("sender_id", pref.getUserId());
                    params.put("receiver_id", receiverId);
                    params.put("receiver_type", receiverType);
                    if (receiverType.equalsIgnoreCase("pv")) {
                        params.put("mode1", "pv_farmer");
                    } else {
                        params.put("mode1", "farmer_farmer");
                    }
                    params.put("mode2", "");
                } else if (pref.getUserType().equalsIgnoreCase("oe")) {
                    params.put("user_id", pref.getUserId());
                } else if (pref.getUserType().equalsIgnoreCase("fa")) {
                    params.put("user_id", pref.getUserId());
                } else if (pref.getUserType().equalsIgnoreCase("trucker")) {
                    params.put("user_id", pref.getUserId());
                } else if (pref.getUserType().equalsIgnoreCase("trader")) {
                    params.put("user_id", pref.getUserId());
                } else if (pref.getUserType().equalsIgnoreCase("pv")) {
                    params.put("sender_id", pref.getUserId());
                    params.put("receiver_id", receiverId);
                    params.put("receiver_type", receiverType);
                    params.put("mode1", "pv_farmer");
                    params.put("mode2", "");
                }
                Log.d("fetch msg params", params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUserProfile.getApiToken());
                return header;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void updateReadStatus() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        String date = dateFormat.format(Calendar.getInstance().getTime());

        String userId = mUserProfile.getUserId();

        for (ChatMessage chatMessage : messages) {
            Log.d("ONe to OnE Chat  ", chatMessage.getRead_status());

            if (chatMessage.getSender_id().equalsIgnoreCase(userId))
                continue; // don't update read status for your own message

            if (!chatMessage.getRead_status().equalsIgnoreCase("Y")) {
                updateReadStatus(chatMessage.getMessage_id(), "read", date);
            }
        }
    }

    private void updateReadStatus(final String messageId, final String mode, final String dateTime) {
        StringRequest request = new StringRequest(Request.Method.POST, Config.UPDATE_READ_STATUS_OF_CHAT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY", "update chat status "+response);
                        // response ignored
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("api_token", mUserProfile.getApiToken());
                params.put("user_id", mUserProfile.getUserId());
                params.put("user_type", mUserProfile.getUserType());
                params.put("message_id", messageId);
                params.put("mode", mode);
                params.put("date_time", dateTime);

                return params;
            }
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUserProfile.getApiToken());
                return header;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }

    //This method will send the new message to the thread
    private void sendMessage() {
        final String message = etMessage.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            showToast(this, "Can't send empty message");
            return;
        }
        final String sentAt = getTimeStamp();


        etMessage.setText("");

   /*     Log.d("send msg url", Config.INSERT_FARMER_ONE_TO_ONE_CHAT_MESSAGE_AT_START_UP);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.INSERT_FARMER_ONE_TO_ONE_CHAT_MESSAGE_AT_START_UP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TESTING_VOLLEY", "insert_msg"+response);
                        //JSONArray array;
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            if(code==200) {
                                ChatMessage m = new ChatMessage("-1", mUserProfile.getUserId(), "AP (0)", message, sentAt, mUserProfile.getUserName());

                                m.setChat_type("personal");

                                mChatAdapter.addData(m);
                                rvMessages.scrollToPosition(mChatAdapter.getItemCount() - 1);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("api_token", mUserProfile.getApiToken());

                params.put("sender_type", pref.getUserType());
                params.put("receiver_type", receiverType);
                if (pref.getUserType().equalsIgnoreCase("farmer")) {
                    params.put("sender_id", pref.getUserId());
                } else if (pref.getUserType().equalsIgnoreCase("pv")) {
                    params.put("sender_id", pref.getUserId());
                }

                params.put("receiver_id", receiverId);
                params.put("message", message);
                if (pref.getUserType().equalsIgnoreCase("farmer") &&
                        receiverType.equalsIgnoreCase("farmer")) {
                    params.put("mode1", "farmer_farmer");
                } else if (pref.getUserType().equalsIgnoreCase("pv") &&
                        receiverType.equalsIgnoreCase("farmer")) {
                    params.put("mode1", "pv_farmer");
                } else if (pref.getUserType().equalsIgnoreCase("farmer") &&
                        receiverType.equalsIgnoreCase("pv")) {
                    params.put("mode1", "pv_farmer");
                }
                params.put("mode2", "no_mode"); //added by V1k
                params.put("name", "" + mUserProfile.getUserName());
                Log.d("send msg params", params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> header = new HashMap<>();
                header.put("Authentication",mUserProfile.getApiToken());
                return header;
            }
        };

        //Disabling retry to prevent duplicate messages
        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
        */
        API_Manager api_manager = new API_Manager();
        Chat retrofits=api_manager.getClient7().create(Chat.class);

        HashMap<String, String> params = new HashMap<>();
        params.put("sender_type", pref.getUserType());
        params.put("receiver_type", receiverType);
        if (pref.getUserType().equalsIgnoreCase("farmer")) {
            params.put("sender_id", pref.getUserId());
        } else if (pref.getUserType().equalsIgnoreCase("pv")) {
            params.put("sender_id", pref.getUserId());
        }
        params.put("receiver_id", receiverId);
        params.put("message", message);
        if (pref.getUserType().equalsIgnoreCase("farmer") &&
                receiverType.equalsIgnoreCase("farmer")) {
            params.put("mode1", "farmer_farmer");
        } else if (pref.getUserType().equalsIgnoreCase("pv") &&
                receiverType.equalsIgnoreCase("farmer")) {
            params.put("mode1", "pv_farmer");
        } else if (pref.getUserType().equalsIgnoreCase("farmer") &&
                receiverType.equalsIgnoreCase("pv")) {
            params.put("mode1", "pv_farmer");
        }
        params.put("mode2", "no_mode"); //added by V1k
        params.put("name", "" + mUserProfile.getUserName());

            Call<MessageStatus> call=retrofits.insert_chat_message(mUserProfile.getApiToken(),params);
            call.enqueue(new Callback<MessageStatus>() {
                @Override
                public void onResponse(Call<MessageStatus> call, retrofit2.Response<MessageStatus> response) {
                    Logs.d("CheckStatus", "onResponse: insert one to one "+response);
                    try {
                        if(response.body()!=null && response.body().getStatus()== 200) {
                            Logs.d("CheckStatus", "insert one to one "+response.body().getStatus());
                            ChatMessage m = new ChatMessage("-1", mUserProfile.getUserId(), "AP (0)", message, sentAt, mUserProfile.getUserName());

                            m.setChat_type("personal");

                            mChatAdapter.addData(m);
                            rvMessages.scrollToPosition(mChatAdapter.getItemCount() - 1);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onFailure(Call<MessageStatus> call, Throwable t) {
                    Log.d("CheckStatus","insert one to one error "+t.getMessage());
                    t.printStackTrace();
                }
            });
    }

    //method to scroll the recyclerview to bottom
    private void scrollToBottom() {
        mChatAdapter.notifyDataSetChanged();
        if (mChatAdapter.getItemCount() > 1) {
            rvMessages.getLayoutManager().smoothScrollToPosition(rvMessages, null, mChatAdapter.getItemCount() - 1);
        }
    }

    //Registering broadcast receivers
    @Override
    protected void onResume() {
        super.onResume();
        Log.w("MainActivity", "onResume");
    /*LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
        new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));
    LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
        new IntentFilter(GCMRegistrationIntentService.REGISTRATION_ERROR));
    LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
        new IntentFilter(GCMRegistrationIntentService.REGISTRATION_TOKEN_SENT));*/
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.NOTIFICATION_TAG2));

        AppController.getInstance().setChatIdentifier(receiverId);
    }


    //Unregistering receivers
    @Override
    protected void onPause() {
        super.onPause();
        Log.w("MainActivity", "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

        AppController.getInstance().removeChatIdentifier();
    }

    //Creating option menu to add logout feature
  /*@Override
  public boolean onCreateOptionsMenu(Menu menu) {
    //Adding our menu to toolbar
    getMenuInflater().inflate(R.menu.menu, menu);
    return true;
  }*/

    //Adding logout option here
  /*@Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == R.id.menuLogout) {
      AppController.getInstance().logout();
      finish();
      startActivity(new Intent(this, MainActivity.class));
    }
    return super.onOptionsItemSelected(item);
  }*/



    private void showAlertToSelectWhomToChat() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);
        alert.setMessage("Select your choice to chat with...");
        alert.setPositiveButton("Farmer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showAlertToSelectChoiceOfFarmer();
            }
        });
        alert.setNegativeButton("Pesticide Vender", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }

    private void showAlertToSelectChoiceOfFarmer() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(true);
        alert.setMessage("Select your farmer category to chat with...");
        alert.setPositiveButton("Farmers From Same Pincode", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.setNegativeButton("Farmers From Same City", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.setNeutralButton("Farmers For Particular Commodity",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }
}
