package com.agriiprince;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.agriiprince.activities.SplashActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@RunWith(AndroidJUnit4.class)
    @LargeTest
    public class Test1 {

        @Rule
        public ActivityTestRule<SplashActivity> mActivityTestRule = new ActivityTestRule<>(SplashActivity.class);


    @Test
        public void Test1() {
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            onView(
                    allOf(withId(R.id.farmer_home_commission_agent),withText("Commission Agents Directory")));
                  onView(        allOf(withId(R.id.farmer_home_check_price),withText("Check and Compare Prices")));
        onView( allOf(withId(R.id.farmer_home_make_profit),withText("Make profit")));
        onView (allOf(withId(R.id.farmer_home_weather),withText("WeatherForecast")));
        onView( allOf(withId(R.id.farmer_home_pesticides),withText("Pesticides & Crop Diseases")));
        onView (allOf(withId(R.id.farmer_home_warehouse),withText("Search Cold Storage around you")));


    }
    }
